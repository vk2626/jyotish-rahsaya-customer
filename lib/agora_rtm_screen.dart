// // ignore_for_file: unnecessary_brace_in_string_interps

// import 'dart:convert';
// import 'dart:math';

// import 'package:agora_rtm/agora_rtm.dart';

// import 'package:flutter/material.dart';

// import 'Core/logger_helper.dart';

// class AgoraRtmScreen extends StatefulWidget {
//   const AgoraRtmScreen({super.key});

//   @override
//   State<AgoraRtmScreen> createState() => _AgoraRtmScreenState();
// }

// class _AgoraRtmScreenState extends State<AgoraRtmScreen> {
//   final _userId = '929';
//   final appId = '2b2b29ac0c8b40e88eae4516659054ed';
//   final token =
//       '007eJxSYIhYPudDUaRIhOoDqZe8ln9MJzHzH3/bsnN62RWL0/83pborMBglGSUZWSYmGyRbJJkYpFpYpCammpgampmZWhqYmqSmqNz3Snfw9mHYLFzJxMTAyADCID4LmGRmKCrJZWawNLIEiTNCxSyNLAEBAAD//0imIhk=';
//   final channelName = 'rtm';
//   late AgoraRtmClient _client;
//   Color myColor = Colors.black;
//   late AgoraRtmChannel _channel;
//   List<Offset?> points = [];
  
//   @override
//   void initState() {
//     super.initState();
//     initializeAgoraRTM();
//   }

//   initializeAgoraRTM() async {
//     try {
//       // Create rtm client
//       _client = await AgoraRtmClient.createInstance(appId);
//       _client.onMessageReceived = (RtmMessage message, String peerId) {
//         TLoggerHelper.info("Private Message from $peerId: ${message.text}");
//         // setState(() {
//         //   myColor = Color(int.parse(message.text));
//         // });
//         final decodedData = jsonDecode(message.text);
//         if (decodedData['action'] == 'draw') {
//           final point = decodedData['point'];
//           if (point != null) {
//             setState(() {
//               points.add(Offset(point['x'], point['y']));
//             });
//           } else {
//             setState(() {
//               points.add(null); // Handle line breaks
//             });
//           }
//         }
//       };
//       _client.onConnectionStateChanged = (int state, int reason) {
//         TLoggerHelper.info(
//             'Connection state changed: ${state}, reason: ${reason}');
//         if (state == 5) {
//           _client.logout();
//           TLoggerHelper.info('Logout.');
//         }
//       };
//     } catch (e) {
//       TLoggerHelper.error('Initialize failed!:${e}');
//     }
//   }

//   void _login(BuildContext context) async {
//     //Change this only after the changes need to be done or needed !!
//     String userId = _userId;
//     if (userId.isEmpty) {
//       TLoggerHelper.info('Please input your user id to login.');
//       return;
//     }

//     try {
//       await _client.login(token, userId);
//       TLoggerHelper.info('Login success: $userId');
//       _joinChannel(context);
//     } on AgoraRtmClientException catch (errorCode) {
//       TLoggerHelper.error('Login error: $errorCode');
//     } catch (errorCode) {
//       TLoggerHelper.error('Login error: $errorCode');
//     }
//   }

//   void _joinChannel(BuildContext context) async {
//     //Change the channel name if required !!
//     String channelId = channelName;
//     if (channelId.isEmpty) {
//       TLoggerHelper.info('Please input channel id to join.');
//       return;
//     }

//     try {
//       _channel = await _createChannel(channelId);
//       await _channel.join();
//       TLoggerHelper.info('Join channel success.');
//     } catch (errorCode) {
//       TLoggerHelper.error('Join channel error: $errorCode');
//     }
//   }

//   Future<AgoraRtmChannel> _createChannel(String name) async {
//     AgoraRtmChannel? channel = await _client.createChannel(name);
//     channel!.onMemberJoined = (AgoraRtmMember member) {
//       TLoggerHelper.info(
//           "Member joined: " + member.userId + ', channel: ' + member.channelId);
//     };
//     channel.onMemberLeft = (AgoraRtmMember member) {
//       TLoggerHelper.info(
//           "Member left: " + member.userId + ', channel: ' + member.channelId);
//     };
//     channel.onMessageReceived =
//         (AgoraRtmMessage message, AgoraRtmMember member) {
//       TLoggerHelper.info(
//           "Public Message from ${member.userId}: ${message.text}");
//     };
//     return channel;
//   }

//   void _sendPoint(Offset? point) async {
//     final data = {
//       'action': 'draw',
//       'point': point != null
//           ? {'x': point.dx, 'y': point.dy}
//           : null, // Send null for breaks
//     };

//     try {
      
//       await _client.sendMessageToPeer2(
//         "1521", // Replace with recipient's ID, or use channel broadcast
//         RtmMessage.fromText(jsonEncode(data)),
//       );
//     } catch (e) {
//       TLoggerHelper.error("Failed to send point: $e");
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: RepaintBoundary(
//           child: GestureDetector(
//             onPanUpdate: (details) {
//               final localPosition = details.localPosition;
//               setState(() {
//                 points.add(localPosition);
//               });
//               _sendPoint(localPosition);
//             },
//             onPanEnd: (_) {
//               setState(() {
//                 points.add(null); // Add a break in the line
//               });
//               _sendPoint(null);
//             },
//             child: CustomPaint(
//               painter: SignaturePainter(points),
//               size: Size.infinite,
//             ),
//           ),
//         ),
//         floatingActionButton: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           children: [
//             FloatingActionButton(
//               onPressed: () {
//                 setState(() {
//                   points.clear();
//                 });
//               },
//               child: Icon(Icons.clear),
//             ),
//             FloatingActionButton(
//               onPressed: () {
//                 _login(context);
//               },
//               child: Icon(Icons.add),
//             ),
//           ],
//         ));
//   }
// }

// class SignaturePainter extends CustomPainter {
//   final List<Offset?> points;

//   SignaturePainter(this.points);

//   @override
//   void paint(Canvas canvas, Size size) {
//     final paint = Paint()
//       ..color = Colors.black
//       ..strokeCap = StrokeCap.round
//       ..strokeWidth = 3.0;

//     for (int i = 0; i < points.length - 1; i++) {
//       if (points[i] != null && points[i + 1] != null) {
//         canvas.drawLine(points[i]!, points[i + 1]!, paint);
//       } else if (points[i] != null && points[i + 1] == null) {
//         // Draw point if it's the end of a stroke
//         canvas.drawCircle(points[i]!, 3.0, paint);
//       }
//     }
//   }

//   @override
//   bool shouldRepaint(covariant CustomPainter oldDelegate) {
//     return oldDelegate != this;
//   }
// }
