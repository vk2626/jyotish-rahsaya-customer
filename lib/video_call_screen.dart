// import 'dart:async';
// import 'package:agora_rtc_engine/agora_rtc_engine.dart';
// import 'package:agora_uikit/agora_uikit.dart';
// import 'package:dio/dio.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../core/logger_helper.dart';
// import 'Core/Api/Constants.dart';
// import 'Core/Provider/list.of.api.provider.dart';

// class VideoCallScreen extends StatefulWidget {
//   final String sessionId;
//   final String token;
//   final String channelName;
//   final String liveId;

//   const VideoCallScreen({
//     Key? key,
//     required this.token,
//     required this.channelName,
//     required this.sessionId,
//     required this.liveId,
//   }) : super(key: key);

//   @override
//   State<VideoCallScreen> createState() => _VideoCallScreenState();
// }

// class _VideoCallScreenState extends State<VideoCallScreen>
//     with WidgetsBindingObserver {
//   final String appId = "2b2b29ac0c8b40e88eae4516659054ed";

//   late String liveId;
//   ScrollController scrollController = ScrollController();
//   int _elapsedSeconds = 0;
//   late RtcEngine agoraEngine;
//   SharedPreferences? prefs;
//   late final String token;
//   late final String channelName;
//   bool _isJoined = false;
//   late Timer _timer;
//   final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
//       GlobalKey<ScaffoldMessengerState>();
//   int? localUIDs;
//   final int callTimeoutSeconds = 30;
//   int? remoteUIDs;
//   int minutesCompleted = 0;
//   bool isAttended = false;

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) {
//     super.didChangeAppLifecycleState(state);
//     if (state == AppLifecycleState.paused) {
//       // leave();
//     }
//   }

//   @override
//   void initState() {
//     super.initState();
//     WidgetsBinding.instance.addObserver(this);
//     getPrefs();
//     liveId = widget.liveId;
//     // token = widget.token;
//     // channelName = widget.channelName;
//     token =
//         "007eJxTYNhxKD56hV31MvWs3xt8lroJHDep8OiITmiZqH6o+vBF/nQFBqMkILRMTDZItkgyMUi1sEhNTDUxNTQzM7U0MDVJTUmVLk1rCGRkmP2xkJWRAQJBfCGGssyU1Hzd5MScHN3kjMS8vNQcBgYAEt4kOw==";
//     channelName = "video-call-channel";
//     TLoggerHelper.info(
//         "Token: ${widget.token}, Channel: ${widget.channelName}, Live: ${widget.liveId}");
//     agoraEngine = createAgoraRtcEngine();
//     initAgora();
//   }

//   Future<void> getPrefs() async {
//     prefs = await SharedPreferences.getInstance();
//   }

//   initAgora() async {
//     setupVideoSDKEngine();
//   }

//   Future<void> setupVideoSDKEngine() async {
//     await [Permission.microphone, Permission.camera].request();
//     if (await Permission.microphone.isDenied ||
//         await Permission.camera.isDenied) {
//       showPermissionSnackbar(context);
//     }
//     try {
//       await agoraEngine.initialize(RtcEngineContext(appId: appId));

//       agoraEngine.registerEventHandler(
//         RtcEngineEventHandler(
//           onJoinChannelSuccess: (connection, elapsed) {
//             TLoggerHelper.info("Local user joined the channel");
//           },
//           onUserJoined: (connection, remoteUid, elapsed) {
//             TLoggerHelper.info("Joined user uid:$remoteUid joined the channel");

//             setState(() {
//               remoteUIDs = remoteUid;
//               isAttended = true;
//             });
//             _startTimer();
//           },
//           onUserOffline: (connection, remoteUid, reason) async {
//             leave();
//             TLoggerHelper.warning("Remote user left the channel");
//             await prefs?.remove(Constants.isCalling);
//           },
//           onError: (err, msg) {
//             TLoggerHelper.error(err.name);
//             TLoggerHelper.error(msg);
//           },
//         ),
//       );
//       join();
//     } on AgoraRtcException catch (e) {
//       TLoggerHelper.error(e.message.toString());
//     }
//   }

//   void showMessage(String message) {
//     scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
//       content: Text(message),
//     ));
//   }

//   Future join() async {
//     await agoraEngine.startPreview();
//     await agoraEngine.enableVideo();
//     // Set channel options including the client role and channel profile
//     ChannelMediaOptions options = const ChannelMediaOptions(
//       clientRoleType: ClientRoleType.clientRoleBroadcaster,
//       channelProfile: ChannelProfileType.channelProfileCommunication,
//     );

//     await agoraEngine.joinChannel(
//       token: token,
//       // channelId: channelName,
//       channelId: channelName,
//       options: options,
//       uid: 182,
//     );
//   }

//   void showPermissionSnackbar(BuildContext context) {
//     ScaffoldMessenger.of(context).showSnackBar(
//       SnackBar(
//         content: const Text('Microphone and Camera permission are required.'),
//         action: SnackBarAction(
//           label: 'Settings',
//           onPressed: () {
//             openAppSettings();
//           },
//         ),
//       ),
//     );
//   }

//   Future leave() async {
//     TLoggerHelper.warning("Leave Working!!");
//     setState(() {
//       _isJoined = false;
//       remoteUIDs = null;
//     });
//     agoraEngine.release();
//     agoraEngine.leaveChannel();
//     if (_timer.isActive) {
//       _timer.cancel();
//     }
//     if (!isAttended) {
//       // rejectCall(liveId);
//     }
//     if (Navigator.of(context).canPop()) {
//       Navigator.of(context).pop();
//     }
//     if (Navigator.of(context).canPop()) {
//       Navigator.of(context).pop();
//     }
//   }

//   void _startTimer() {
//     TLoggerHelper.debug("Timer Started");
//     _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
//       setState(() {
//         _elapsedSeconds++;
//         minutesCompleted = (_elapsedSeconds / 60).ceil();
//         if (remoteUIDs != null) {
//           localUIDs = remoteUIDs;
//           TLoggerHelper.debug("Remote local id:- ${localUIDs}");
//         } else {
//           TLoggerHelper.debug("Remote local id off the loop:- ${localUIDs}");
//         }
//       });
//     });
//   }

//   Future<bool> showExitPopup() async {
//     Navigator.of(context).pop();
//     return false;
//   }

//   Future<void> showConfirmationDialog(
//       BuildContext context, String title) async {
//     return await showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: Text(title,
//               textAlign: TextAlign.center,
//               style: const TextStyle(fontSize: 15, color: Colors.black)),
//           actions: [
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 ElevatedButton(
//                   style: ButtonStyle(
//                     backgroundColor: MaterialStateProperty.all(Colors.grey),
//                     shape: MaterialStateProperty.all(
//                       RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(5),
//                       ),
//                     ),
//                   ),
//                   onPressed: () {
//                     Navigator.of(context).pop(true);
//                   },
//                   child: const Text('No',
//                       style: TextStyle(
//                           fontWeight: FontWeight.bold, color: Colors.black)),
//                 ),
//                 ElevatedButton(
//                   style: ButtonStyle(
//                     backgroundColor: MaterialStateProperty.all(Colors.yellow),
//                     shape: MaterialStateProperty.all(
//                       RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(5),
//                       ),
//                     ),
//                   ),
//                   onPressed: () {
//                     leave();
//                   },
//                   child: const Text(
//                     'Yes',
//                     style: TextStyle(
//                         fontWeight: FontWeight.bold, color: Colors.black),
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         );
//       },
//     );
//   }

//   static Future<void> rejectCall(String communicationId) async {
//     var endPointUrl = "reject-call";
//     final Map<String, String> body = {'communication_id': communicationId};
//     final dio = Dio();
//     await dio
//         .post("${Constants.baseUrl}$endPointUrl", data: body)
//         .then((val) => TLoggerHelper.info("Completed Successfully!!"));
//   }

//   @override
//   Widget build(BuildContext context) {
//     TLoggerHelper.debug("Remote local id ${localUIDs}");
//     return PopScope(
//       canPop: false,
//       onPopInvoked: (didPop) async {
//         if (didPop) {
//           return;
//         }
//         showConfirmationDialog(
//             context, "Do you really want to leave the live??");
//       },
//       child: SafeArea(
//         child: Scaffold(
//           body: Stack(
//             children: [
//               _localPreview(),
//               Align(alignment: Alignment.bottomRight, child: _remotePreview())
//             ],
//           ),
//           floatingActionButtonLocation:
//               FloatingActionButtonLocation.centerDocked,
//           floatingActionButton: InkWell(
//             onTap: () async {
//               await Provider.of<ListOfApisProvider>(context, listen: false)
//                   .sendCallDurationProvider(
//                       communicationID: liveId, duration: minutesCompleted)
//                   .whenComplete(() async {
//                 leave();
//                 await prefs?.remove(Constants.isCalling);
//               });
//             },
//             child: Padding(
//               padding: const EdgeInsets.only(bottom: 18.0),
//               child: CircleAvatar(
//                 radius: 32,
//                 backgroundColor: Colors.red,
//                 child: Icon(
//                   _isJoined ? Icons.call_end : Icons.call,
//                   color: Colors.white,
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget _localPreview() {
//     return AgoraVideoView(
//       controller: VideoViewController(
//         rtcEngine: agoraEngine,
//         canvas: const VideoCanvas(uid: 0),
//       ),
//     );
//   }

//   Widget _remotePreview() {
//     return Padding(
//       padding: const EdgeInsets.all(16.0),
//       child: ClipRRect(
//         borderRadius: BorderRadius.circular(12),
//         child: Container(
//           alignment: Alignment.center,
//           decoration: BoxDecoration(
//               color: localUIDs == null
//                   ? Colors.blueGrey.withOpacity(.85)
//                   : Colors.transparent),
//           height: MediaQuery.of(context).size.height * .2,
//           width: MediaQuery.of(context).size.width * .3,
//           child:
//               //  remoteUIds == null
//               //     ? Text(
//               //         "Ringing",
//               //         style: GoogleFonts.poppins(color: Colors.white),
//               //       )
//               //     :
//               AgoraVideoView(
//             controller: VideoViewController.remote(
//                 rtcEngine: agoraEngine,
//                 canvas: VideoCanvas(uid: 183),
//                 connection: RtcConnection(channelId: channelName)),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class RemotePreview extends StatefulWidget {
//   final String? localUIDs;
//   final RtcEngine agoraEngine;
//   final String channelName;
//   const RemotePreview(
//       {super.key,
//       required this.localUIDs,
//       required this.agoraEngine,
//       required this.channelName});

//   @override
//   State<RemotePreview> createState() => _RemotePreviewState();
// }

// class _RemotePreviewState extends State<RemotePreview> {
//   String? localPreviewID;
//   @override
//   Widget build(BuildContext context) {
//     TLoggerHelper.debug(
//         "Local ID from the remote preview printing !!! ----> ${widget.localUIDs}");
//     setState(() {
//       // if (localPreviewID != "null") {
//       localPreviewID = widget.localUIDs;
//       // }
//     });
//     return Padding(
//       padding: const EdgeInsets.all(16.0),
//       child: ClipRRect(
//         borderRadius: BorderRadius.circular(12),
//         child: Container(
//           alignment: Alignment.center,
//           decoration: BoxDecoration(
//               color: localPreviewID == "null"
//                   ? Colors.blueGrey.withOpacity(.85)
//                   : Colors.transparent),
//           height: MediaQuery.of(context).size.height * .2,
//           width: MediaQuery.of(context).size.width * .3,
//           child: localPreviewID == "null"
//               ? Text(
//                   "Ringing ${localPreviewID}",
//                   style: GoogleFonts.poppins(color: Colors.white),
//                 )
//               : AgoraVideoView(
//                   controller: VideoViewController.remote(
//                       rtcEngine: widget.agoraEngine,
//                       canvas:
//                           VideoCanvas(uid: int.parse(localPreviewID ?? "0")),
//                       connection: RtcConnection(channelId: widget.channelName)),
//                 ),
//         ),
//       ),
//     );
//   }
// }
