import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';

class ForegroundServiceManager {
  ForegroundServiceManager() {
    _initializeForegroundTask();
  }

  void _initializeForegroundTask() {
    FlutterForegroundTask.init(
      androidNotificationOptions: AndroidNotificationOptions(
          channelId: 'foreground_service_channel',
          channelName: 'Foreground Service',
          channelDescription:
              'This notification appears when the app is running in the background.',
          channelImportance: NotificationChannelImportance.LOW,
          priority: NotificationPriority.HIGH,
          visibility: NotificationVisibility.VISIBILITY_PUBLIC),
      iosNotificationOptions: const IOSNotificationOptions(),
      foregroundTaskOptions: ForegroundTaskOptions(
        eventAction: ForegroundTaskEventAction.once(),
        autoRunOnBoot: true,
        allowWifiLock: true,
      ),
    );
  }

  Future<void> startForegroundService({required VoidCallback callback}) async {
    await FlutterForegroundTask.startService(
      notificationTitle: 'Running in background',
      notificationText: 'Tap to return to the app',
      callback: callback
    );
  }

  Future<void> stopForegroundService() async {
    await FlutterForegroundTask.stopService();
  }
}
