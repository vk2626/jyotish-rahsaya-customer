import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import '../Core/Model/book.pooja.model.dart';
import '../Core/logger_helper.dart';
import '../router_constants.dart';
import '../Core/formatter.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/list.of.api.provider.dart';
import '../utils/AppColor.dart';
import '../utils/custom_circular_progress_indicator.dart';

class BookPoojaScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BookPoojaState();
  }
}

class _BookPoojaState extends State<BookPoojaScreen>
    with AutomaticKeepAliveClientMixin {
  bool _isLoading = true; // Track loading state
  List<PoojaListItem> bookPooja = []; // To store fetched book pooja data

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isLoading) {
      getData();
    }
  }

  // Fetch data only once
  getData() async {
    setState(() {
      _isLoading = true;
    });

    // Get the data from the provider
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getBookPoojaProvider(context: context);

    // Get the updated list from the provider's state
    var poojaData = Provider.of<ListOfApisProvider>(context, listen: false)
            .poojaModel
            .data ??
        [];

    setState(() {
      _isLoading = false;
      bookPooja = poojaData; // Update the list
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: _isLoading
            ? Center(
                child: CustomCircularProgressIndicator(),
              )
            : bookPooja.isEmpty
                ? Center(child: Text("No Data Available"))
                : Column(
                    children: <Widget>[
                      SizedBox(height: 10),
                      Expanded(
                        child: ListView.builder(
                          itemCount: bookPooja.length,
                          itemBuilder: (BuildContext context, int pIndex) {
                            final DateTime dateTime = bookPooja[pIndex].time!;
                            final String formattedDate =
                                DateFormat('dd').format(dateTime);
                            final String formattedMonth =
                                DateFormat('MMM').format(dateTime);
                            final String formattedYear =
                                DateFormat('yyyy').format(dateTime);

                            return Container(
                              height: 280,
                              margin: EdgeInsets.only(
                                  bottom: 15, left: 10, right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Card(
                                        color: Colors.white,
                                        elevation: 2,
                                        shape: CircleBorder(
                                            side:
                                                BorderSide(color: Colors.grey)),
                                        child: SizedBox(
                                          width: 30,
                                          height: 30,
                                          child: Center(
                                            child: Text(
                                              formattedDate,
                                              style: GoogleFonts.poppins(
                                                  color: AppColor.darkGrey,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        formattedMonth,
                                        style: GoogleFonts.poppins(
                                            fontSize: 14, color: Colors.black),
                                      ),
                                      SizedBox(height: 4),
                                      Expanded(
                                        child: VerticalDivider(
                                          width: 2,
                                          thickness: 1,
                                          color: Colors.grey[500],
                                        ),
                                      )
                                    ],
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        TLoggerHelper.debug(
                                            bookPooja[pIndex].astrologerId);
                                        context.pushNamed(
                                            RouteConstants.poojaScreen,
                                            queryParameters: {
                                              'poojaId': bookPooja[pIndex]
                                                  .id
                                                  .toString(),
                                              'isDirectPooja': "1",
                                              'astrologerId':
                                                  bookPooja[pIndex].astrologerId
                                            });
                                      },
                                      child: Container(
                                        child: Card(
                                          color: Colors.white,
                                          elevation: 2,
                                          clipBehavior:
                                              Clip.antiAliasWithSaveLayer,
                                          child: Stack(
                                            children: <Widget>[
                                              Positioned(
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                      "${bookPooja[pIndex].image}",
                                                  fit: BoxFit.cover,
                                                ),
                                                right: 0,
                                                left: 0,
                                                top: 0,
                                                bottom: 40,
                                              ),
                                              Positioned(
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 10),
                                                  height: 50,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text(
                                                          TFormatter.capitalizeSentence(
                                                              bookPooja[pIndex]
                                                                  .title
                                                                  .toString()),
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      16)),
                                                      Text(
                                                        TFormatter
                                                            .capitalizeSentence(
                                                                bookPooja[
                                                                        pIndex]
                                                                    .shortDescription
                                                                    .toString()),
                                                        style:
                                                            GoogleFonts.poppins(
                                                          color: Colors.white,
                                                          fontSize: 14,
                                                        ),
                                                        maxLines: 1,
                                                      ),
                                                    ],
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                  ),
                                                  color:
                                                      AppColor.transparentBlack,
                                                ),
                                                right: 0,
                                                left: 0,
                                                bottom: 40,
                                              ),
                                              Positioned(
                                                child: Divider(
                                                    color: Colors.white,
                                                    thickness: 2),
                                                right: 0,
                                                left: 0,
                                                bottom: 82,
                                              ),
                                              Positioned(
                                                child: Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 10),
                                                    height: 40,
                                                    child: Row(
                                                        children: [
                                                          Text(
                                                              "${formattedDate}-${formattedMonth}-${formattedYear}",
                                                              style: GoogleFonts.poppins(
                                                                  color: Colors
                                                                      .grey
                                                                      .shade600,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  fontSize:
                                                                      14)),
                                                          Row(
                                                            children: [
                                                              Text(
                                                                  "Book Now"
                                                                      .tr(),
                                                                  style: GoogleFonts.poppins(
                                                                      color: Colors
                                                                          .black,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      fontSize:
                                                                          16)),
                                                              SizedBox(
                                                                  width: 5),
                                                              Icon(Icons
                                                                  .arrow_forward)
                                                            ],
                                                          ),
                                                        ],
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween)),
                                                right: 0,
                                                left: 0,
                                                bottom: 0,
                                              ),
                                              Positioned(
                                                  right: 10,
                                                  top: 10,
                                                  child: CircleAvatar(
                                                    radius: 33,
                                                    backgroundColor:
                                                        AppColor.appColor,
                                                    child: CircleAvatar(
                                                      radius: 32,
                                                      backgroundImage:
                                                          CachedNetworkImageProvider(
                                                              bookPooja[pIndex]
                                                                  .astrologerImage
                                                                  .toString()),
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                        height: 280,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
