import 'dart:convert';
import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Core/Key/app.key.dart';
import '../Core/logger_helper.dart';
import '../utils/snack_bar.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../utils/AppColor.dart';

class YoutubeScreen extends StatefulWidget {
  final String youtubeUrlId;

  const YoutubeScreen({
    Key? key,
    required this.youtubeUrlId,
  }) : super(key: key);

  @override
  State<YoutubeScreen> createState() => _YoutubeScreenState();
}

class _YoutubeScreenState extends State<YoutubeScreen> {
  late YoutubePlayerController _controller;

  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  double _volume = 100;
  bool _muted = false;
  bool _isPlayerReady = false;
  String videoTitle = "";
  String userProfile = "";
  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: "${widget.youtubeUrlId}",
      flags: const YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        loop: false,
        isLive: false,
        forceHD: false,
      ),
    )..addListener(listener);
    _videoMetaData = YoutubeMetaData();
    _playerState = PlayerState.unknown;
    futurefuns();
  }

  futurefuns() async {
    userProfile =
        await fetchChannelProfileImage("UCijB8Lc-EHpUryeCkPgq1Lw") ?? "";
  }

  Future<String?> fetchChannelProfileImage(String channelId) async {
    final url = Uri.https('www.googleapis.com', '/youtube/v3/channels', {
      'part': 'snippet',
      'id': channelId,
      'key': AppKey.youtubeAPIKey,
    });
    TLoggerHelper.debug(url.toString());
    final response = await http.get(url);

    if (response.statusCode == 200) {
      Map<String, dynamic> data = jsonDecode(response.body);
      String? profileImageUrl =
          data['items'][0]['snippet']['thumbnails']['default']['url'];
      return profileImageUrl;
    } else {
      TLoggerHelper.error(
          'Failed to load channel profile image: ${response.statusCode}');
      return null;
    }
  }

  void listener() {
    if (_controller.value.isReady) {
      setState(() {
        _isPlayerReady = true;
      });
    }
    if (_isPlayerReady && mounted) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
    if (_controller.value.playerState == PlayerState.ended) {
      Navigator.of(context).pop();
      showSnackBarWidget(context, "Video Completed !!", isTrue: true);
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.appColor,
        title: Text("Our Celebrity Customer"),
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          YoutubePlayerBuilder(
            player: YoutubePlayer(
              controller: _controller,
              showVideoProgressIndicator: true,
              progressIndicatorColor: Colors.amber,
              progressColors: const ProgressBarColors(
                  playedColor: Colors.amber, handleColor: Colors.amberAccent),
              controlsTimeOut: Duration(hours: 1),
              topActions: [
                CircleAvatar(
                  radius: 24,
                  backgroundImage: CachedNetworkImageProvider(userProfile),
                ),
                SizedBox(
                  width: 12,
                ),
                Expanded(
                    child: Text(_videoMetaData.title,
                        style: GoogleFonts.poppins(
                            color: Colors.white, fontSize: 18))),
                GestureDetector(
                    onTap: () async {
                      var url =
                          "https://www.youtube.com/watch?v=${_controller.initialVideoId}";
                      await launchUrl(Uri.parse(url));
                    },
                    child:
                        Icon(Icons.screen_share_rounded, color: Colors.white))
              ],
            ),
            builder: (context, player) {
              return Column(
                children: [Expanded(child: player)],
              );
            },
          ),
        ],
      ),
    );
  }
}
