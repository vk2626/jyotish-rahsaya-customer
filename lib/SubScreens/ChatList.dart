// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:jyotish_rahsaya/Core/Provider/go_live_provider.dart';
import 'package:jyotish_rahsaya/Core/helper_functions.dart';
import 'package:jyotish_rahsaya/Screens/AstrologerProfileNew.dart';
import 'package:jyotish_rahsaya/router_constants.dart';
import 'package:jyotish_rahsaya/utils/custom_circular_progress_indicator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Api/list.of.apis.dart';
import '../Core/Model/LeaveChatModel.dart';
import '../Core/Model/astrologer.category.list.model.dart';
import '../Core/Model/astrologer.list.model.dart';
import '../Core/Model/astrologer.slider.category.model.dart';
import '../Core/Model/recharge.amount.model.dart';
import '../Core/Model/wallet.transection.model.dart';
import '../Core/Provider/FilterNotifier.dart';
import '../Core/Provider/WaitListNotifier.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Core/formatter.dart';
import '../Core/langConstant/language.constant.dart';
import '../Core/logger_helper.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../Screens/ChatIntakeForm.dart';
import '../Screens/LoginScreen.dart';
import '../Screens/ProfileScreen.dart';
import '../Screens/payment.info.dart';
import '../dialog/showLoaderDialog.dart';
import '../utils/AppColor.dart';
import '../utils/DashSeparator.dart';

class ChatList extends StatefulWidget {
  List<String>? selectedSortBy;
  List<String>? selectedCountry;
  List<String>? selectedGender;
  List<String>? selectedLng;
  List<String>? selectedSkills;
  ChatList(
      {Key? key,
      this.selectedSortBy,
      this.selectedCountry,
      this.selectedGender,
      this.selectedLng,
      this.selectedSkills})
      : super(key: key);

  static ValueNotifier<bool> refreshFunction = ValueNotifier(false);
  @override
  State<StatefulWidget> createState() {
    return _ChatListState();
  }
}

class _ChatListState extends State<ChatList>
    with AutomaticKeepAliveClientMixin {
  int selectedPos = 0;
  String astroID = "1";
  int customerID = 0;
  String customerName = "";
  String profileImage = "";
  String walletAmount = "0";
  String waitChatID = "";
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  String astroName = "";
  String price = "";
  static const _pageSize = 20;
  String waitTime = "";
  final PagingController<int, AstrologerElement> _pagingController =
      PagingController(firstPageKey: 20);

  // int? free_chat;
  Set<String> pagedUsersIDs = {};
  DioClient? client;
  var isRefresh = true;
  var filteredList = false;
  List<AstrologerElement> localresAstroList = [];

  AstrologerCategoryListModel? resAstroCategoryList;
  SliderAstrologerCategoryModel? resSliderCategoryData;
  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });

    ChatList.refreshFunction.addListener(() {
      if (ChatList.refreshFunction.value) {
        refreshlist().whenComplete(() {
          ChatList.refreshFunction.value = false;
        });
      } else {}
    });

    // Provider.of<WaitListNotifier>(context, listen: false)
    //     .checkWaitList(context, customerID);
    getUserId();

    runProviders();
  }

  void runProviders() async {
    Provider.of<ListOfApisProvider>(context, listen: false)
        .getAstrologerCategoryListProvider()
        .then((value) {
      if (value != null) {
        setState(() {
          resAstroCategoryList = value;
        });
      }
    });
    Provider.of<ListOfApisProvider>(context, listen: false)
        .getSliderAstrologerCategoryListProvider(id: astroID)
        .then((value) {
      if (value != null) {
        setState(() {
          resSliderCategoryData = value;
        });
      }
    });
  }

  Future<void> _fetchPage(int pageKey) async {
    // try {
    TLoggerHelper.info(
        (pageKey / 20).toString() + " This is the page key <<<<<<---->>>>>");

    List<AstrologerElement> astrologerElement = [];

    // Fetch from both APIs for the first page
    if ((pageKey / 20).toInt() == 1) {
      var firstApiData =
          await ListOfApi().getAstrologerWithOrderList(type: "chat");
      if (firstApiData != null && firstApiData.astrologers != null) {
        astrologerElement.addAll(firstApiData.astrologers!.data);
      }
    }

    // Fetch from the second API for all pages, including the first one
    var secondApiData = await ListOfApi().getAstrologerList(
      customerID,
      "chat",
      (pageKey / 20).toInt(),
      pagedUsersIDs,
      selectedSortBy: widget.selectedSortBy ?? [],
      selectedCountry: widget.selectedCountry ?? [],
      selectedGender: widget.selectedGender ?? [],
      selectedLng: widget.selectedLng ?? [],
      selectedSkills: widget.selectedSkills ?? [],
    );

    if (secondApiData != null && secondApiData.astrologers != null) {
      astrologerElement.addAll(secondApiData.astrologers!.data);
    }

    // Save the result locally if it's the first page
    if ((pageKey / 20).toInt() == 1) {
      localresAstroList = astrologerElement;
    }

    // Check if this is the last page
    final isLastPage = astrologerElement.length < _pageSize;
    if (isLastPage) {
      _pagingController.appendLastPage(astrologerElement);
    } else {
      final nextPageKey = pageKey + astrologerElement.length;
      _pagingController.appendPage(astrologerElement, nextPageKey);
    }
    // } catch (error) {
    //   _pagingController.error = error;
    // }
  }

  Future refreshlist() async {
    setState(() {
      isRefresh = true;
      filteredList = false;
    });
    runProviders();
    _pagingController.refresh();

    getWalletAmount();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    TLoggerHelper.debug("REBUILDING WIDGET");
    return SafeArea(
        child: Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: RefreshIndicator(
          onRefresh: refreshlist,
          color: Colors.white,
          backgroundColor: AppColor.appColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              /********************* Chip Category Section ************************/
              if (resAstroCategoryList != null)
                Container(
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  width: double.infinity,
                  height: 40,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: resAstroCategoryList!.data.length,
                    itemBuilder: (BuildContext context, int i) {
                      return Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 40,
                        child: Card(
                          color: Colors.white,
                          elevation: 2,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25.0)),
                              side: BorderSide(
                                  color: selectedPos == i
                                      ? TFormatter.hexToColor(
                                          resAstroCategoryList!.data[i].color)
                                      : Colors.white)),
                          child: GestureDetector(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 10),
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        image: DecorationImage(
                                            image: CachedNetworkImageProvider(
                                                "${resAstroCategoryList!.data[i].image}"))),
                                  ),
                                ),
                                SizedBox(width: 5),
                                Text(
                                  "${resAstroCategoryList!.data[i].name}",
                                  style:
                                      GoogleFonts.poppins(color: Colors.black),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(width: 10),
                              ],
                            ),
                            onTap: () {
                              setState(() {
                                selectedPos = i;
                                astroID = resAstroCategoryList!.data[i].id!;
                                Provider.of<ListOfApisProvider>(context,
                                        listen: false)
                                    .getSliderAstrologerCategoryListProvider(
                                        id: astroID)
                                    .then((value) {
                                  if (value != null) {
                                    setState(() {
                                      resSliderCategoryData = value;
                                    });
                                  }
                                });
                                Provider.of<ListOfApisProvider>(context,
                                        listen: false)
                                    .getAstrologerListProvider(
                                        context: context,
                                        UserId: double.parse(astroID).toInt(),
                                        type: 'chat',
                                        pageNumber: 1);

                                print("Sele ID ---> $astroID");
                              });
                            },
                          ),
                        ),
                      );
                    },
                  ),
                ),
              /************* Carousel Slider *********************/
              if (resSliderCategoryData != null && resAstroCategoryList != null)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Container(
                      width: double.infinity,
                      child: CarouselSlider.builder(
                        itemCount: resSliderCategoryData!.data.length,
                        itemBuilder: (BuildContext context, int itemIndex,
                            int pageViewIndex) {
                          return Container(
                            width: double.infinity,
                            margin: EdgeInsets.symmetric(horizontal: 6),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  Container(
                                    width: double.infinity,
                                    height: double.infinity,
                                    alignment: Alignment.centerLeft,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8),
                                    color: TFormatter.hexToColor(
                                        resAstroCategoryList!
                                            .data[selectedPos].color),
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          .5,
                                      child: Text(
                                        "Q. " +
                                                resSliderCategoryData!
                                                    .data[itemIndex].title
                                                    .toString() ??
                                            "",
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        style: GoogleFonts.poppins(
                                            color: Colors.white, fontSize: 16),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      bottom: -30,
                                      right: -40,
                                      child: CircleAvatar(
                                        radius: 64,
                                      )),
                                  Positioned(
                                      bottom: -25,
                                      right: -50,
                                      child: CircleAvatar(
                                        radius: 64,
                                        backgroundColor: Colors.white,
                                      )),
                                  Positioned(
                                    bottom: 5,
                                    right: 15,
                                    child: SvgPicture.network(
                                      resSliderCategoryData!
                                          .data[itemIndex].image!,
                                      height: 40,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                        options: CarouselOptions(
                          autoPlay: true,
                          height: 50,
                          viewportFraction: 1,
                          reverse: false,
                          enableInfiniteScroll: false,
                          enlargeCenterPage: false,
                          initialPage: selectedPos,
                        ),
                      )),
                ),
              /**************** Astrologer List Section **********************/
              Expanded(
                child: PagedListView<int, AstrologerElement>(
                  pagingController: _pagingController,
                  shrinkWrap: true,
                  builderDelegate: PagedChildBuilderDelegate<AstrologerElement>(
                      firstPageProgressIndicatorBuilder: (context) => Center(
                            child: CustomCircularProgressIndicator(),
                          ),
                      newPageProgressIndicatorBuilder: (context) => Center(
                            child: CustomCircularProgressIndicator(),
                          ),
                      itemBuilder: (context, resAstroList, i) {
                        pagedUsersIDs.addAll(_pagingController.itemList!
                            .map((element) => element.astrologer.userId)
                            .toSet());

                        bool isFreeChat = resAstroList.astrologer.freeChat ==
                                1 &&
                            ((resAstroList.isFreeChatCompleted == 0 &&
                                    resAstroList.astrologer.freeChat == 1) ||
                                customerID == 0);
                        return GestureDetector(
                          onTap: () {
                            context.pushNamed(
                              RouteConstants.astrologerProfile,
                              queryParameters: {
                                'astrologerId': resAstroList.astrologer.userId,
                                'type': 'chat',
                                'isAstrologerOnline':
                                    resAstroList.isAstrologerOnline.toString()
                              },
                            );
                            print("pos---> $i");
                            TLoggerHelper.debug(
                                resAstroList.astrologer.freeChat.toString());
                          },
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 10, right: 10, top: 2),
                            child: Card(
                              color: Colors.white,
                              elevation: 2,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              child: Stack(
                                children: [
                                  Row(
                                    children: [
                                      SizedBox(width: 10),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            SizedBox(height: 10),
                                            Stack(
                                              children: [
                                                Center(
                                                  child: Card(
                                                    clipBehavior: Clip
                                                        .antiAliasWithSaveLayer,
                                                    child: CircleAvatar(
                                                      radius: 40,
                                                      backgroundImage:
                                                          CachedNetworkImageProvider(
                                                              resAstroList
                                                                  .avatar),
                                                    ),
                                                    shape: CircleBorder(
                                                      side: BorderSide(
                                                          color:
                                                              AppColor.appColor,
                                                          width: 2),
                                                    ),
                                                  ),
                                                ),
                                                if (resAstroList.label != 0)
                                                  Positioned(
                                                    bottom: -4,
                                                    left: 0,
                                                    right: 0,
                                                    child: Center(
                                                      child: Card(
                                                        color: Colors.amber,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          10.0),
                                                                ),
                                                                side: BorderSide(
                                                                    color: Colors
                                                                        .yellow)),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 12,
                                                                  right: 12,
                                                                  bottom: 3,
                                                                  top: 2),
                                                          child: Text(
                                                              TFormatter.capitalizeSentence(
                                                                  resAstroList
                                                                      .astrologer
                                                                      .labelText),
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .fade,
                                                              style: GoogleFonts.poppins(
                                                                  fontSize: 8,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                Positioned(
                                                  bottom: 10,
                                                  right: 0,
                                                  left: 64,
                                                  child: CircleAvatar(
                                                    radius: 7,
                                                    child: CircleAvatar(
                                                      radius: 5,
                                                      backgroundColor: resAstroList
                                                                  .isAstrologerOnline ==
                                                              1
                                                          ? Colors.green
                                                          : Colors
                                                              .grey, // Green for online, grey for offline
                                                      child: Tooltip(
                                                        message: resAstroList
                                                                    .isAstrologerOnline ==
                                                                1
                                                            ? 'Online'
                                                            : 'Offline',
                                                        child: Container(
                                                          height: 20,
                                                          width: 20,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            RatingBarIndicator(
                                                rating: resAstroList.rating,
                                                itemCount: 5,
                                                itemSize: 15.0,
                                                itemBuilder: (context, _) =>
                                                    const Icon(
                                                      Icons.star,
                                                      color: Colors.amber,
                                                    )),
                                            Text(
                                                "${resAstroList.astrologer.orderCount} " +
                                                    "orders".tr(),
                                                style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                    fontSize: 10),
                                                textAlign: TextAlign.center),
                                            SizedBox(height: 10),
                                          ],
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      Expanded(
                                        // flex: 3,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(height: 10),
                                            Text(
                                                "${TFormatter.capitalizeSentence(resAstroList.name)}",
                                                style: GoogleFonts.poppins(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                            SizedBox(
                                              height: 2,
                                            ),
                                            Text(
                                              "${resAstroList.astrologer.allSkillName}",
                                              style: GoogleFonts.poppins(
                                                color: Colors.grey[600],
                                              ),
                                              maxLines: 2,
                                            ),
                                            Text(
                                              "${resAstroList.astrologer.languageName}",
                                              style: GoogleFonts.poppins(
                                                color: Colors.grey[600],
                                              ),
                                              maxLines: 2,
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "Exp".tr() +
                                                  " : ${resAstroList.astrologer.experienceYear} " +
                                                  "yr".tr(),
                                              style: GoogleFonts.poppins(
                                                color: Colors.grey[600],
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text: "₹",
                                                    style: GoogleFonts.poppins(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey[600],
                                                      // Add line through original price
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        "${resAstroList.astrologer.chatPrice == 0 ? "0" : resAstroList.astrologer.chatPrice}",
                                                    style: GoogleFonts.poppins(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey[600],
                                                      decoration: resAstroList
                                                                  .astrologer
                                                                  .chatDiscountPrice ==
                                                              0
                                                          ? !isFreeChat
                                                              ? TextDecoration
                                                                  .none
                                                              : TextDecoration
                                                                  .lineThrough
                                                          : TextDecoration
                                                              .lineThrough, // Add line through original price
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text: isFreeChat
                                                        ? " Free"
                                                        : " ${resAstroList.astrologer.chatDiscountPrice == 0 ? '' : resAstroList.astrologer.chatDiscountPrice}",
                                                    style: GoogleFonts.poppins(
                                                      color: Colors.red,
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            SizedBox(height: 10),
                                          ],
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      Expanded(
                                        // flex: 2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Icon(Icons.verified,
                                                color: Colors.green),
                                            SizedBox(height: 24),
                                            SizedBox(
                                              height: 35,
                                              child: TextButton(
                                                onPressed: () {
                                                  if (resAstroList.astrologer
                                                          .statusChat ==
                                                      1) {
                                                    if (resAstroList
                                                            .astrologer
                                                            .nextonlines
                                                            .chatNODate
                                                            .isEmpty &&
                                                        resAstroList
                                                            .astrologer
                                                            .nextonlines
                                                            .chatNOTime
                                                            .isEmpty) {
                                                      TLoggerHelper.info(
                                                          customerName
                                                                  .toString() +
                                                              "   <------>    " +
                                                              customerID
                                                                  .toString());
                                                      if (customerID > 0 &&
                                                          customerName != "") {
                                                        var data = resAstroList
                                                            .astrologer;
                                                        int realPrice = (data
                                                                        .chatDiscountPrice <=
                                                                    data
                                                                        .chatPrice) &&
                                                                (data.chatDiscountPrice !=
                                                                    0)
                                                            ? data
                                                                .chatDiscountPrice
                                                            : data.chatPrice;
                                                        if ((double.parse(
                                                                    walletAmount) >=
                                                                realPrice *
                                                                    5) &&
                                                            (walletAmount !=
                                                                "0") &&
                                                            (!isFreeChat)) {
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (BuildContext
                                                                      context) {
                                                                return Dialog(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .white,
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20.0)),
                                                                  child:
                                                                      Padding(
                                                                    padding:
                                                                        const EdgeInsets
                                                                            .all(
                                                                            5.0),
                                                                    child:
                                                                        Column(
                                                                      mainAxisSize:
                                                                          MainAxisSize
                                                                              .min,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .center,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        Align(
                                                                          alignment:
                                                                              Alignment.topRight,
                                                                          child:
                                                                              IconButton(
                                                                            onPressed:
                                                                                () {
                                                                              Navigator.pop(context);
                                                                            },
                                                                            icon:
                                                                                Icon(
                                                                              Icons.close,
                                                                              size: 30,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Card(
                                                                          clipBehavior:
                                                                              Clip.antiAliasWithSaveLayer,
                                                                          child:
                                                                              CircleAvatar(
                                                                            radius:
                                                                                60,
                                                                            backgroundImage:
                                                                                CachedNetworkImageProvider("${resAstroList.avatar}"),
                                                                          ),
                                                                          shape:
                                                                              CircleBorder(side: BorderSide(color: AppColor.appColor, width: 2)),
                                                                        ),
                                                                        Text(
                                                                          TFormatter.capitalizeSentence(
                                                                              resAstroList.name),
                                                                          style: GoogleFonts.poppins(
                                                                              color: Colors.black,
                                                                              fontSize: 16,
                                                                              fontWeight: FontWeight.w600),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                10),
                                                                        Text(
                                                                          "Start a paid chat to get detailed insights from the astrologer."
                                                                              .tr(),
                                                                          style: GoogleFonts.poppins(
                                                                              color: Colors.black,
                                                                              fontSize: 15),
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                10),
                                                                        TextButton(
                                                                          onPressed:
                                                                              () {
                                                                            Navigator.pop(context);

                                                                            print("resultChatlist");
                                                                            sendAndGetResult(
                                                                                resAstroList.name.toString(),
                                                                                resAstroList.id,
                                                                                resAstroList.avatar.toString(),
                                                                                context,
                                                                                resAstroList.astrologer.userId.toString(),
                                                                                isFreeChat ? 1 : 0);
                                                                          },
                                                                          style: TextButton.styleFrom(
                                                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                                                                              backgroundColor: AppColor.appColor),
                                                                          child:
                                                                              Padding(
                                                                            padding:
                                                                                EdgeInsets.symmetric(horizontal: 20),
                                                                            child:
                                                                                Text(
                                                                              isFreeChat != 1 ? "Start Chat".tr() + " @ ${Constants.currency} ${resAstroList.astrologer!.chatDiscountPrice == 0 ? resAstroList.astrologer!.chatPrice : resAstroList.astrologer!.chatDiscountPrice}/" + "min".tr() : "Start Chat".tr() + " @ Free".tr(),
                                                                              style: GoogleFonts.poppins(color: Colors.black),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );
                                                              });
                                                        } else if (isFreeChat) {
                                                          showDialog(
                                                              barrierDismissible:
                                                                  false,
                                                              context: context,
                                                              builder:
                                                                  (BuildContext
                                                                      context) {
                                                                return Dialog(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .white,
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20.0)),
                                                                  child:
                                                                      Container(
                                                                    height: (MediaQuery.of(context).size.height /
                                                                            2) -
                                                                        50,
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                          .all(
                                                                          5.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.center,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                Alignment.topRight,
                                                                            child:
                                                                                IconButton(
                                                                              onPressed: () {
                                                                                Navigator.pop(context);
                                                                              },
                                                                              icon: Icon(Icons.close, size: 30),
                                                                            ),
                                                                          ),
                                                                          Card(
                                                                            clipBehavior:
                                                                                Clip.antiAliasWithSaveLayer,
                                                                            child:
                                                                                CircleAvatar(
                                                                              radius: 70,
                                                                              backgroundImage: CachedNetworkImageProvider("${resAstroList.avatar}"),
                                                                            ),
                                                                            shape:
                                                                                CircleBorder(side: BorderSide(color: AppColor.appColor, width: 2)),
                                                                          ),
                                                                          Text(
                                                                            "${resAstroList.name}",
                                                                            style: GoogleFonts.poppins(
                                                                                color: Colors.black,
                                                                                fontSize: 16,
                                                                                fontWeight: FontWeight.w600),
                                                                          ),
                                                                          SizedBox(
                                                                              height: 10),
                                                                          Text(
                                                                            "This offer is free for 3 min only. Astrologer will try to answer at least one question.",
                                                                            style:
                                                                                GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                                                                            textAlign:
                                                                                TextAlign.center,
                                                                          ),
                                                                          SizedBox(
                                                                              height: 10),
                                                                          TextButton(
                                                                            onPressed:
                                                                                () {
                                                                              Navigator.pop(context);

                                                                              print("resultChatlist");
                                                                              sendAndGetResult(resAstroList.name.toString(), resAstroList.id, resAstroList.avatar.toString(), context, resAstroList.astrologer!.userId.toString(), resAstroList.astrologer!.freeChat);
                                                                            },
                                                                            style:
                                                                                TextButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)), backgroundColor: AppColor.appColor),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsets.symmetric(horizontal: 20),
                                                                              child: Text(
                                                                                "Start Free Chat".tr(),
                                                                                style: GoogleFonts.poppins(color: Colors.black),
                                                                              ),
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                );
                                                              });
                                                        } else {
                                                          _rechargeDialog(
                                                                  context,
                                                                  resAstroList
                                                                      .name
                                                                      .toString(),
                                                                  resAstroList.id
                                                                      .toString(),
                                                                  resAstroList.astrologer
                                                                              .chatDiscountPrice ==
                                                                          0
                                                                      ? resAstroList
                                                                          .astrologer
                                                                          .chatPrice
                                                                      : resAstroList
                                                                          .astrologer
                                                                          .chatDiscountPrice)
                                                              .whenComplete(
                                                            () {
                                                              getWalletAmount();
                                                            },
                                                          );
                                                        }
                                                      } else if (customerID >
                                                              0 &&
                                                          customerName == "") {
                                                        Navigator.of(context)
                                                            .push(SlideRightRoute(
                                                                page:
                                                                    ProfileScreen()))
                                                            .then((value) =>
                                                                getUserId());
                                                      } else {
                                                        context.pushNamed(
                                                            RouteConstants
                                                                .authScreen);
                                                      }
                                                    } else {
                                                      nextOnlineDialog(
                                                          astrologerName:
                                                              resAstroList.name,
                                                          astrologerImage:
                                                              resAstroList
                                                                  .avatar,
                                                          nextOnlineTime:
                                                              resAstroList
                                                                  .astrologer
                                                                  .nextonlines
                                                                  .chatNOTime,
                                                          nextOnlineDate:
                                                              resAstroList
                                                                  .astrologer
                                                                  .nextonlines
                                                                  .chatNODate);
                                                    }
                                                    print(
                                                        "Chat List Customer -> $customerID");
                                                  } else {
                                                    Fluttertoast.showToast(
                                                        msg:
                                                            "${resAstroList.astrologer.displayName} Chat is Offline");
                                                  }
                                                },
                                                style: TextButton.styleFrom(
                                                    side: BorderSide(
                                                        color: resAstroList
                                                                        .astrologer
                                                                        .waitTime ==
                                                                    0 &&
                                                                resAstroList
                                                                        .astrologer
                                                                        .statusChat ==
                                                                    1
                                                            ? Colors.greenAccent
                                                            : Colors.red),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            side: BorderSide(
                                                                width: 2),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5))),
                                                child: Text(
                                                  resAstroList.astrologer
                                                              .statusChat ==
                                                          0
                                                      ? "Chat Offline".tr()
                                                      : !isFreeChat
                                                          ? "Chat".tr()
                                                          : "Free Chat".tr(),
                                                  style: GoogleFonts.poppins(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: resAstroList
                                                                      .astrologer
                                                                      .waitTime! ==
                                                                  0 &&
                                                              resAstroList
                                                                      .astrologer
                                                                      .statusChat ==
                                                                  1
                                                          ? Colors.greenAccent
                                                          : Colors.red),
                                                ),
                                              ),
                                            ),
                                            Visibility(
                                              child: Text(
                                                  "Wait" +
                                                      " ~ ${resAstroList.astrologer.waitTime} /" +
                                                      "mins".tr(),
                                                  style: GoogleFonts.poppins(
                                                      color: Colors.red[600],
                                                      fontSize: 10,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                              visible: resAstroList.astrologer
                                                          .waitTime! !=
                                                      0 &&
                                                  resAstroList.astrologer
                                                          .statusChat ==
                                                      1,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                  // if (resAstroList.label != 0)
                                  //   Positioned(
                                  //     left:
                                  //         -25, // Adjust the value to avoid overlapping
                                  //     top:
                                  //         15, // Adjust the value for proper positioning

                                  //     child: Transform.rotate(
                                  //       angle: -math.pi / 3.5,
                                  //       child: Container(
                                  //         decoration: BoxDecoration(
                                  //           gradient: LinearGradient(
                                  //             colors: [
                                  //               Color(
                                  //                   0xFF3D3D3D), // Dark gray (closer to black)
                                  //               Color(
                                  //                   0xFF1C1C1C), // Almost black
                                  //               Color(0xFF000000), // Pure black
                                  //               Color(
                                  //                   0xFF434343), // Slightly lighter for a metallic effect
                                  //             ],
                                  //             begin: Alignment.topCenter,
                                  //             end: Alignment.bottomCenter,
                                  //           ),
                                  //         ),
                                  //         width: 100,
                                  //         alignment: Alignment.center,
                                  //         padding: EdgeInsets.symmetric(
                                  //           vertical: 2,
                                  //         ),
                                  //         child: Text(
                                  //           resAstroList.astrologer.labelText,
                                  //           textAlign: TextAlign.center,
                                  //           style: GoogleFonts.poppins(
                                  //             color: AppColor.whiteColor,
                                  //             fontSize: 8,
                                  //           ),
                                  //         ),
                                  //       ),
                                  //     ),
                                  //   ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
              // Visibility(
              //   child: Expanded(
              //     child: Center(
              //       child: Text("No Astrologer found"),
              //     ),
              //   ),
              //   visible: resAstroList.length == 0,
              // ),
            ],
          )),
    ));
  }

  /********************* Recharge Dialog ************************/
  Future<void> _rechargeDialog(BuildContext context, String name,
      String astroID, int amountPerMinute) async {
    int? amID = 0;
    String? amount = "";

    // context
    //     .read<ListOfApisProvider>()
    //     .getRechargeAmountProvider(context: context);
    await showModalBottomSheet<void>(
      context: context,
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: Consumer<ListOfApisProvider>(builder: (context, provider, _) {
            return FutureBuilder(
                future: provider.getRechargeAmountPProvider(context: context),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CustomCircularProgressIndicator(),
                    );
                  } else if (snapshot.hasData) {
                    RechargeAmountModel rechargeAmountModel = snapshot.data;
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topRight,
                              child: CloseButton()),
                          Text(
                            "Minimum balance of 5 minutes".tr() +
                                " (INR ${amountPerMinute * 5}.0) " +
                                "is required to start chat with".tr() +
                                " ($name)",
                            textAlign: TextAlign.start,
                            style: GoogleFonts.poppins(
                                fontSize: 14, color: Colors.redAccent),
                          ),
                          SizedBox(height: 10),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Recharge Now'.tr(),
                              style: GoogleFonts.poppins(fontSize: 17),
                            ),
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.light_mode_rounded,
                                color: AppColor.appColor,
                              ),
                              Text("Tip: 90% user recharge for 10 mins or more"
                                  .tr())
                            ],
                          ),
                          SizedBox(height: 10),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: rechargeAmountModel.recharge!.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4,
                                        childAspectRatio: 6 / 2,
                                        mainAxisSpacing: 10,
                                        crossAxisSpacing: 10),
                                itemBuilder: (context, index) {
                                  if (index == 0) {
                                    price = rechargeAmountModel
                                        .recharge![index].price!;
                                    print(rechargeAmountModel
                                        .recharge![index].price);
                                    amID =
                                        rechargeAmountModel.recharge![index].id;
                                    amount = rechargeAmountModel
                                        .recharge![index].price;
                                  }
                                  return InkWell(
                                    onTap: () {
                                      if (customerID == 0) {
                                        Navigator.of(context).push(
                                            SlideRightRoute(
                                                page: LoginScreen()));
                                      } else {
                                        Navigator.of(context)
                                            .push(
                                              SlideRightRoute(
                                                page: ListenableProvider(
                                                  create: (context) =>
                                                      ListOfApisProvider(),
                                                  child:
                                                      PaymentInformationScreen(
                                                    userInfoId: "",
                                                    astroName: customerName,
                                                    price: rechargeAmountModel
                                                        .recharge![index]
                                                        .price!,
                                                    amountID:
                                                        rechargeAmountModel
                                                            .recharge![index]
                                                            .id!,
                                                    from: 'wallet',
                                                    title:
                                                        "Recharge Amount".tr(),
                                                    astroId: astroID,
                                                  ),
                                                ),
                                              ),
                                            )
                                            .then((value) =>
                                                Navigator.maybePop(context));
                                      }
                                      price = rechargeAmountModel
                                          .recharge![index].price!;
                                      print(rechargeAmountModel
                                          .recharge![index].price);
                                      amID = rechargeAmountModel
                                          .recharge![index].id;
                                      amount = rechargeAmountModel
                                          .recharge![index].price;
                                    },
                                    child: Container(
                                        height: 25,
                                        width: 90,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            border: Border.all(
                                                color: index == 0
                                                    ? AppColor.appColor
                                                    : AppColor.borderColor)),
                                        child: Stack(children: <Widget>[
                                          Positioned(
                                              top: 7,
                                              right: 50.5,
                                              child: Transform.rotate(
                                                  angle: -math.pi / 3.5,
                                                  child: Container(
                                                      height: 15,
                                                      width: 50,
                                                      decoration: BoxDecoration(
                                                          color: Colors.green,
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius.circular(
                                                                      5))),
                                                      child: Center(
                                                          child: Text(
                                                              "${rechargeAmountModel.recharge![index].discountPercentage}% Extra",
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      fontSize:
                                                                          8)))))),
                                          Center(
                                              child: Text(
                                                  "${rechargeAmountModel.recharge![index].price}"))
                                        ])),
                                  );
                                }),
                          ),
                          SizedBox(height: 20),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              style: TextButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  backgroundColor: AppColor.appColor,
                                  textStyle:
                                      Theme.of(context).textTheme.labelLarge),
                              child: Text(
                                'Proceed to Pay'.tr(),
                                style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              ),
                              onPressed: () {
                                if (customerID == 0) {
                                  Navigator.of(context).push(
                                      SlideRightRoute(page: LoginScreen()));
                                } else {
                                  Navigator.of(context)
                                      .push(
                                        SlideRightRoute(
                                          page: PaymentInformationScreen(
                                            userInfoId: "",
                                            astroName: customerName,
                                            price: price,
                                            title: "Recharge Amount".tr(),
                                            amountID: amID!,
                                            from: 'wallet',
                                            astroId: astroID,
                                          ),
                                        ),
                                      )
                                      .then((value) =>
                                          Navigator.maybePop(context));
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return SizedBox();
                  }
                });
          }),
        );
      },
    );
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;
      profileImage = prefs.getString(Constants.profileImage)!;
      print("CHat List Customer -> $customerID");
      getWalletAmount();
      if (prefs.getString(Constants.fullName) != null) {
        customerName = prefs.getString(Constants.fullName)!;
      }
    }
    setState(() {});
  }

  getWalletAmount() {
    THelperFunctions.getWalletAmount().then((value) {
      setState(() {
        walletAmount = value.toString();
      });
    });
  }

  nextOnlineDialog(
      {required String astrologerName,
      required String nextOnlineTime,
      required String astrologerImage,
      required String nextOnlineDate}) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        CircleAvatar(
                          radius: 35,
                          backgroundImage:
                              CachedNetworkImageProvider(astrologerImage),
                        ),
                        Wrap(
                          children: [
                            Text(
                              TFormatter.capitalizeSentence(astrologerName),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Text(
                        "You will be connecting with" +
                            " $astrologerName " +
                            "in a while.".tr(),
                        style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Next Online Time of the Astrologer is".tr() +
                          " :- $nextOnlineDate $nextOnlineTime",
                      style: GoogleFonts.poppins(
                          color: Colors.black54, fontSize: 15),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          // showCancelBookingDialog(waitChatID);
                        },
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            backgroundColor: AppColor.appColor),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "OK",
                            style: GoogleFonts.poppins(color: Colors.black),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }

  showConfirmationDialog(String name, String userImage) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      Positioned(
                        child: DashSeparator(
                          color: Colors.grey,
                          height: 2.0,
                        ),
                        right: 50,
                        left: 50,
                        top: 40,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 100,
                            height: 150,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Card(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  child: CircleAvatar(
                                    radius: 35,
                                    backgroundImage: CachedNetworkImageProvider(
                                        "$profileImage"),
                                  ),
                                  shape: CircleBorder(
                                      side: BorderSide(
                                          color: AppColor.appColor, width: 2)),
                                ),
                                Wrap(
                                  children: [
                                    Text(
                                      "${customerName}",
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          SizedBox(
                            width: 100,
                            height: 150,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Card(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  child: CircleAvatar(
                                    radius: 35,
                                    backgroundImage: CachedNetworkImageProvider(
                                        "$userImage"),
                                  ),
                                  shape: CircleBorder(
                                      side: BorderSide(
                                          color: AppColor.appColor, width: 2)),
                                ),
                                Text(
                                  "${name}",
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Text(
                      "You will be connecting with" +
                          " $name " +
                          "in a while.".tr(),
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w700),
                      textAlign: TextAlign.center),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "While you wait for" +
                        " $name " +
                        "you may also explore other astrologers and join their wishlist.",
                    style: GoogleFonts.poppins(
                        color: Colors.black54, fontSize: 15),
                  ),
                  SizedBox(height: 20),
                  SizedBox(
                    width: double.infinity,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        // showCancelBookingDialog(waitChatID);
                      },
                      style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          backgroundColor: AppColor.appColor),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          "OK",
                          style: GoogleFonts.poppins(color: Colors.black),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  void sendAndGetResult(String name, int? id, String avatar,
      BuildContext context, String astroID, int? free_chat) async {
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MultiProvider(providers: [
              ListenableProvider(create: (context) => ListOfApisProvider()),
            ], child: ChatIntakeForm(name, id, astroID, "Chat", free_chat))));
    print("resultdata${result.toString()}");
    if (result == "true") {
      showConfirmationDialog(name, avatar);
    }
  }

  @override
  bool get wantKeepAlive => true;

  // showCancelBookingDialog(String id) {
  //   showModalBottomSheet<void>(
  //     backgroundColor: Colors.white,
  //     context: context,
  //     isDismissible: false,
  //     enableDrag: false,
  //     builder: (BuildContext ctx) {
  //       return PopScope(
  //           canPop: false,
  //           onPopInvoked: (didPop) {
  //             // logic
  //           },
  //           child: ListenableProvider(
  //             create: (context) => ListOfApisProvider(),
  //             child: SizedBox(
  //               height: 400,
  //               child: Padding(
  //                 padding: EdgeInsets.all(10),
  //                 child: Center(
  //                   child: Column(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: <Widget>[
  //                       Align(
  //                           alignment: Alignment.topRight,
  //                           child: IconButton(
  //                               onPressed: () {
  //                                 Navigator.pop(context);
  //                               },
  //                               icon: Icon(Icons.close, size: 30))),
  //                       Text('Leave waitlist of astrologer',
  //                           style: GoogleFonts.poppins(
  //                               fontWeight: FontWeight.bold, fontSize: 14)),
  //                       SizedBox(height: 5),
  //                       Text(
  //                         'Instead of leaving the waitlist, you may also join the waitlist of these trending astrologers',
  //                         textAlign: TextAlign.center,
  //                       ),
  //                       SizedBox(height: 10),
  //                       Container(
  //                         child: Column(
  //                           children: [
  //                             Container(
  //                               width: double.infinity,
  //                               height: 180,
  //                               child: ListView.builder(
  //                                 scrollDirection: Axis.horizontal,
  //                                 shrinkWrap: true,
  //                                 itemCount: localresAstroList != null
  //                                     ? localresAstroList.length
  //                                     : 0,
  //                                 itemBuilder: (BuildContext context, int i) {
  //                                   return GestureDetector(
  //                                     onTap: () {
  //                                       Navigator.of(context)
  //                                           .push(SlideRightRoute(
  //                                               page: MultiProvider(
  //                                         providers: [
  //                                           ListenableProvider(
  //                                               create: (context) =>
  //                                                   ListOfApisProvider())
  //                                         ],
  //                                         child: AstrologerProfile(
  //                                           type: 'chat',
  //                                           uName:
  //                                               "${localresAstroList[i].name}",
  //                                           urExp:
  //                                               "${localresAstroList[i].astrologer!.experienceYear}",
  //                                           uProfile:
  //                                               "${localresAstroList[i].avatar}",
  //                                           uBio:
  //                                               "${localresAstroList[i].astrologer!.bio}",
  //                                           uPrice: localresAstroList[i]
  //                                               .astrologer!
  //                                               .chatPrice,
  //                                           uDiscount: localresAstroList[i]
  //                                               .astrologer!
  //                                               .chatDiscountPrice,
  //                                           gallery: localresAstroList[i]
  //                                               .astrologer!
  //                                               .gallery,
  //                                           uID:
  //                                               "${localresAstroList[i].id.toString()}",
  //                                           isOnline: localresAstroList[i]
  //                                               .astrologer!
  //                                               .isOnline,
  //                                           freeChat: localresAstroList[i]
  //                                               .astrologer!
  //                                               .freeChat,
  //                                           orderCount: localresAstroList[i]
  //                                               .astrologer!
  //                                               .orderCount,
  //                                           primarySkillId: localresAstroList[i]
  //                                               .astrologer!
  //                                               .primarySkillId,
  //                                           consultantGallery: [],
  //                                           waitTime: localresAstroList[i]
  //                                               .astrologer
  //                                               .waitTime!,
  //                                         ),
  //                                       )));
  //                                     },
  //                                     child: Container(
  //                                       margin: EdgeInsets.only(left: 10),
  //                                       width: 120,
  //                                       child: Card(
  //                                         color: Colors.white,
  //                                         elevation: 1,
  //                                         clipBehavior:
  //                                             Clip.antiAliasWithSaveLayer,
  //                                         shape: RoundedRectangleBorder(
  //                                             borderRadius: BorderRadius.all(
  //                                                 Radius.circular(6.0))),
  //                                         child: Column(
  //                                           mainAxisAlignment:
  //                                               MainAxisAlignment.center,
  //                                           children: <Widget>[
  //                                             Center(
  //                                               child: CircleAvatar(
  //                                                 radius: 35,
  //                                                 backgroundImage:
  //                                                     CachedNetworkImageProvider(
  //                                                   "${localresAstroList[i].avatar}",
  //                                                 ),
  //                                               ),
  //                                             ),
  //                                             SizedBox(height: 10),
  //                                             Text(
  //                                                 "${TFormatter.capitalize(localresAstroList[i].name.toString())}",
  //                                                 textAlign: TextAlign.center,
  //                                                 style: GoogleFonts.poppins(
  //                                                     fontSize: 14),
  //                                                 maxLines: 2,
  //                                                 overflow: TextOverflow.fade),
  //                                             Padding(
  //                                               padding: EdgeInsets.symmetric(
  //                                                   horizontal: 5),
  //                                               child: Text(
  //                                                   "₹ ${localresAstroList[i].astrologer!.chatPrice == 0 ? '0' : localresAstroList[i].astrologer!.chatPrice}/"+"min",
  //                                                   style: GoogleFonts.poppins(
  //                                                       fontSize: 12,
  //                                                       color:
  //                                                           Colors.grey[800])),
  //                                             ),
  //                                             Card(
  //                                               shape: RoundedRectangleBorder(
  //                                                   borderRadius:
  //                                                       BorderRadius.all(
  //                                                     Radius.circular(10.0),
  //                                                   ),
  //                                                   side: BorderSide(
  //                                                       color: Colors.green)),
  //                                               child: Padding(
  //                                                 padding: EdgeInsets.only(
  //                                                     left: 25,
  //                                                     right: 25,
  //                                                     bottom: 3,
  //                                                     top: 2),
  //                                                 child: Text(
  //                                                     "chat,
  //                                                     style:
  //                                                         GoogleFonts.poppins(
  //                                                             fontSize: 12,
  //                                                             color: Colors
  //                                                                 .green)),
  //                                               ),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ),
  //                                   );
  //                                 },
  //                               ),
  //                             ),
  //                             SizedBox(height: 10),
  //                           ],
  //                         ),
  //                       ),
  //                       Row(
  //                         children: <Widget>[
  //                           Expanded(
  //                             child: ElevatedButton(
  //                               child: Text('Go Back',
  //                                   style: GoogleFonts.poppins(
  //                                       color: Colors.black)),
  //                               onPressed: () => Navigator.pop(context),
  //                             ),
  //                           ),
  //                           SizedBox(width: 10),
  //                           Expanded(
  //                               child: ElevatedButton(
  //                             child: Text(
  //                               'Leave',
  //                               style: GoogleFonts.poppins(color: Colors.black),
  //                             ),
  //                             onPressed: () async {
  //                               FlutterCallkitIncoming.endAllCalls();
  //                               await Provider.of<ListOfApisProvider>(context,
  //                                       listen: false)
  //                                   .callDismiss(communicationId: id)
  //                                   .whenComplete(
  //                                 () {
  //                                   leaveWaitList(id);
  //                                 },
  //                               );
  //                             },
  //                             style: ButtonStyle(
  //                               backgroundColor:
  //                                   MaterialStateProperty.all<Color>(
  //                                       AppColor.appColor),
  //                             ),
  //                           ))
  //                         ],
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ));
  //     },
  //   );
  // }

  // leaveWaitList(String id) async {
  //   showLoaderDialog(context);
  //   client = DioClient();
  //   Dio dio = await client!.getClient();

  //   var params = jsonEncode({'id': id});
  //   String api = Constants.cancelChatRequest;

  //   LeaveChatModel? walletTransactionModel =
  //       await client?.leaveChat(dio, api, params);
  //   Fluttertoast.showToast(msg: "Leaved Successfully !!");
  //   if (Navigator.of(context).canPop()) {
  //     Navigator.pop(context);
  //   }
  //   if (Navigator.of(context).canPop()) {
  //     Navigator.pop(context);
  //   }
  // }
}
