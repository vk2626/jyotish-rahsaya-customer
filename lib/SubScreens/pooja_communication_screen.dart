// import 'dart:io';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_chat_ui/flutter_chat_ui.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
// import 'package:image_picker/image_picker.dart';
// import '../utils/AppColor.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:swipe_to/swipe_to.dart';
// import 'package:uuid/uuid.dart';
// import 'package:http/http.dart' as http;

// import '../Core/Provider/chatting_provider.dart';
// import '../Core/logger_helper.dart';

// class PoojaCommunicationScreen extends StatefulWidget {
//   final String toId;
//   final String fromId;
//   final String customerName;
//   final String orderId;
//   final String poojaName;

//   const PoojaCommunicationScreen(
//       {super.key,
//       required this.toId,
//       required this.fromId,
//       required this.customerName,
//       required this.orderId,
//       required this.poojaName});

//   @override
//   State<PoojaCommunicationScreen> createState() =>
//       _PoojaCommunicationScreenState();
// }

// class _PoojaCommunicationScreenState extends State<PoojaCommunicationScreen> {
//   List<types.Message> _messages = [];
//   String? referenceId;
//   String imageUrlToSend = "";
//   String messageToSend = "";
//   late types.User user;
//   late types.User otherUser;
//   int pageNumber = 1;
//   SharedPreferences? prefs;
//   File? file;
//   bool isFirstFetched = false;
//   late TextEditingController messageController;

//   @override
//   void initState() {
//     super.initState();
//     messageController = TextEditingController();
//     user = types.User(id: widget.fromId);
//     otherUser = types.User(id: widget.toId, firstName: widget.customerName);
//     refreshMessages();
//     getPrefs();
//     _loadMessages();
//   }

//   getPrefs() async {
//     prefs = await SharedPreferences.getInstance();
//     // await prefs!.remove(TPrefsConstants.isCalling);
//   }

//   refreshMessages() async {
//     FirebaseMessaging.onMessage.listen(handleMessage);
//     FirebaseMessaging.onMessageOpenedApp.listen(handleMessage);
//   }

//   void handleMessage(RemoteMessage message) async {
//     //TODO: Check why not working
//     if (message.data['type'] == 'customer') {
//       await _handleCustomerMessage(message);
//     }
//     TLoggerHelper.debug(message.data.entries.toString());
//   }

//   Future<void> _handleCustomerMessage(RemoteMessage message) async {
//     var localMessage;
//     if (message.data['chat-image'].toString().isNotEmpty) {
//       await Provider.of<ChattingProvider>(context, listen: false)
//           .getMessages(
//         context: context,
//         infoId: widget.orderId,
//         toId: widget.toId,
//         fromId: widget.fromId,
//         pageNumber: 0,
//       )
//           .then((value) {
//         if (value!.status) {
//           var val = value.messages.data.last;
//           TLoggerHelper.debug(val.body.toString());
//           TLoggerHelper.debug(val.attachment.toString());
//           localMessage = types.ImageMessage(
//             author: otherUser,
//             showStatus: true,
//             id: val.id,
//             name: val.attachment!,
//             remoteId: message.data['chat-referenceId'],
//             status: types.Status.delivered,
//             size: 1440,
//             uri: val.attachment!,
//           );
//           setState(() {
//             _messages.insert(0, localMessage);
//           });
//         }
//       });
//     } else {
//       localMessage = types.TextMessage(
//         author: otherUser,
//         remoteId: message.data['chat-referenceId'],
//         id: message.data['chat-id'].toString(),
//         status: types.Status.delivered,
//         text: message.notification!.body!,
//       );
//       setState(() {
//         _messages.insert(0, localMessage);
//       });
//     }
//   }

//   void _handleAttachmentPressed() {
//     showModalBottomSheet<void>(
//       context: context,
//       shape: const BeveledRectangleBorder(borderRadius: BorderRadius.zero),
//       builder: (BuildContext context) => SafeArea(
//         child: SizedBox(
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: <Widget>[
//               TextButton(
//                 onPressed: () {
//                   Navigator.pop(context);
//                   _handleImageSelection();
//                 },
//                 child: const Align(
//                   alignment: AlignmentDirectional.centerStart,
//                   child: Text('Photo'),
//                 ),
//               ),
//               TextButton(
//                 onPressed: () => Navigator.pop(context),
//                 child: const Align(
//                   alignment: AlignmentDirectional.centerStart,
//                   child: Text('Cancel'),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   void _addMessage(types.Message message) {
//     setState(() {
//       _messages.insert(0, message);
//     });
//   }

//   void _handleImageSelection() async {
//     final result = await ImagePicker().pickImage(
//       imageQuality: 70,
//       maxWidth: 1440,
//       source: ImageSource.gallery,
//     );
//     setState(() {
//       messageToSend = "";
//       imageUrlToSend = "";
//     });
//     if (result != null) {
//       file = File(result!.path);
//       final bytes = await result.readAsBytes();
//       final image = await decodeImageFromList(bytes);
//       var message = types.ImageMessage(
//           author: user,
//           createdAt: DateTime.now().millisecondsSinceEpoch,
//           height: image.height.toDouble(),
//           id: const Uuid().v4(),
//           name: result.name,
//           size: bytes.length,
//           remoteId: referenceId,
//           uri: result.path,
//           width: image.width.toDouble(),
//           showStatus: true,
//           status: types.Status.sending);
//       _addMessage(message);
//       await Provider.of<ChattingProvider>(context, listen: false)
//           .sendPoojaMessage(
//               context: context,
//               textBody: "",
//               toId: widget.toId,
//               orderId: widget.orderId,
//               imagePath: result.path,
//               referenceChatId: referenceId,
//               fromId: widget.fromId)
//           .then((response) async {
//         if (response.entries.first.key) {
//           await Provider.of<ChattingProvider>(context, listen: false)
//               .getMessages(
//                   context: context,
//                   toId: widget.toId,
//                   fromId: widget.fromId,
//                   infoId: widget.orderId,
//                   pageNumber: 0)
//               .then((value) {
//             if (value!.status) {
//               var val = value.messages.data[value.messages.data.length - 1];
//               TLoggerHelper.debug(val.body.toString());
//               TLoggerHelper.debug(val.attachment.toString());

//               message = types.ImageMessage(
//                   author: user,
//                   createdAt: DateTime.now().millisecondsSinceEpoch,
//                   height: image.height.toDouble(),
//                   id: response.entries.first.value,
//                   name: result.name,
//                   remoteId: referenceId,
//                   size: bytes.length,
//                   uri: val.attachment!,
//                   width: image.width.toDouble(),
//                   showStatus: true,
//                   status: types.Status.delivered);
//             }
//           });
//         } else {
//           message = types.ImageMessage(
//               author: user,
//               createdAt: DateTime.now().millisecondsSinceEpoch,
//               height: image.height.toDouble(),
//               id: const Uuid().v4(),
//               name: result.name,
//               remoteId: referenceId,
//               size: bytes.length,
//               uri: result.path,
//               width: image.width.toDouble(),
//               showStatus: true,
//               status: types.Status.error);
//         }
//       });

//       setState(() {
//         _messages[0] = message;
//         referenceId = null;
//         messageToSend = "";
//         imageUrlToSend = "";
//       });
//     }
//   }

//   void _handlePreviewDataFetched(
//     types.TextMessage message,
//     types.PreviewData previewData,
//   ) {
//     final index = _messages.indexWhere((element) => element.id == message.id);
//     final updatedMessage = (_messages[index] as types.TextMessage)
//         .copyWith(previewData: previewData);

//     setState(() {
//       _messages[index] = updatedMessage;
//     });
//   }

//   void _handleSendPressed(types.PartialText message) async {
//     if (message.text.isNotEmpty) {
//       setState(() {
//         messageToSend = "";
//         imageUrlToSend = "";
//         messageController.clear();
//       });
//       var textMessage = types.TextMessage(
//           author: user,
//           createdAt: DateTime.now().millisecondsSinceEpoch,
//           id: const Uuid().v4(),
//           text: message.text,
//           remoteId: referenceId,
//           showStatus: true,
//           status: types.Status.sending);

//       _addMessage(textMessage);
//       await Provider.of<ChattingProvider>(context, listen: false)
//           .sendPoojaMessage(
//               context: context,
//               textBody: message.text,
//               toId: widget.toId,
//               referenceChatId: referenceId,
//               orderId: widget.orderId,
//               imagePath: "",
//               fromId: widget.fromId)
//           .then((response) async {
//         if (response.entries.first.key) {
//           textMessage = types.TextMessage(
//               author: user,
//               createdAt: DateTime.now().millisecondsSinceEpoch,
//               id: response.entries.first.value,
//               text: message.text,
//               remoteId: referenceId,
//               showStatus: true,
//               status: types.Status.delivered);
//           setState(() {
//             isFirstFetched = true;
//           });
//         } else {
//           textMessage = types.TextMessage(
//               author: user,
//               createdAt: DateTime.now().millisecondsSinceEpoch,
//               id: const Uuid().v4(),
//               text: message.text,
//               remoteId: referenceId,
//               showStatus: true,
//               status: types.Status.error);
//         }

//         setState(() {
//           _messages[0] = textMessage;
//           referenceId = null;
//           messageToSend = "";
//           imageUrlToSend = "";
//           isFirstFetched = true;
//         });
//       });
//     }
//   }

//   Future<void> _loadMessages() async {
//     // await Future.delayed(const Duration(seconds: 8));
//     var textMessage;
//     List<types.Message> localMessageList = [];
//     await Provider.of<ChattingProvider>(context, listen: false)
//         .getPoojaMessages(
//             context: context,
//             toId: widget.toId,
//             fromId: widget.fromId,
//             orderId: widget.orderId,
//             pageNumber: pageNumber)
//         .then((value) {
//       if (value != null) {
//         if (value.status == true) {
//           TLoggerHelper.info(value.messages.data[0].body!);
//           for (int index = 0; index < value.messages.data.length; index++) {
//             var val = value.messages.data[index];
//             if (widget.fromId == val.fromId.toString()) {
//               if (val.attachment!.isEmpty) {
//                 textMessage = types.TextMessage(
//                   author: user,
//                   createdAt: val.createdAt.millisecondsSinceEpoch,
//                   id: val.id,
//                   showStatus: true,
//                   remoteId: val.referenceChatId,
//                   status: types.Status.delivered,
//                   text: val.body ?? "",
//                 );
//               } else {
//                 textMessage = types.ImageMessage(
//                   author: user,
//                   createdAt: val.createdAt.millisecondsSinceEpoch,
//                   id: val.id,
//                   showStatus: true,
//                   status: types.Status.delivered,
//                   uri: val.attachment!,
//                   remoteId: val.referenceChatId,
//                   name: val.attachment!,
//                   size: 1440,
//                 );
//               }
//             } else {
//               if (val.attachment!.isEmpty) {
//                 textMessage = types.TextMessage(
//                   author: otherUser,
//                   createdAt: val.createdAt.millisecondsSinceEpoch,
//                   id: val.id,
//                   text: val.body ?? "",
//                   showStatus: true,
//                   remoteId: val.referenceChatId,
//                   status: types.Status.delivered,
//                 );
//               } else {
//                 textMessage = types.ImageMessage(
//                   author: otherUser,
//                   createdAt: val.createdAt.millisecondsSinceEpoch,
//                   id: val.id,
//                   showStatus: true,
//                   status: types.Status.delivered,
//                   uri: val.attachment!,
//                   name: val.attachment!,
//                   remoteId: val.referenceChatId,
//                   size: 1440,
//                 );
//               }
//             }

//             if (pageNumber == 1) {
//               _messages.add(textMessage);
//             } else {
//               localMessageList.add(textMessage);
//             }
//           }
//           setState(() {
//             if (pageNumber == 1) {
//               _messages = _messages.reversed.toList();
//             } else {
//               localMessageList = localMessageList.reversed.toList();
//               _messages.addAll(localMessageList);
//             }
//             pageNumber++;
//           });
//           _messages.forEach((element) {
//             if (element.remoteId != null) {
//               TLoggerHelper.debug(element.type.name);
//             }
//           });
//         }
//       }
//     });

//     _messages.forEach((element) {
//       if (element.remoteId != null) {
//         if (!_messages.any((test) => test.id == element.remoteId)) {
//           _loadMessages();
//         }
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     Size mediaQuery = MediaQuery.of(context).size;
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(kToolbarHeight + mediaQuery.height * .1),
//         child: Container(
//           color: Colors.white,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: [
//               AppBar(
//                 title: Text(
//                   "Astrologer",
//                   style: GoogleFonts.poppins(),
//                 ),
//                 actions: [
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 12.0),
//                     child: Icon(Icons.video_camera_front_rounded),
//                   ),
//                   GestureDetector(
//                     onTap: () {
//                       showDialog(
//                           context: context,
//                           builder: (ctx) {
//                             return Dialog(
//                                 child: Container(
//                               width: mediaQuery.width * .8,
//                               padding: EdgeInsets.symmetric(
//                                   horizontal: 12, vertical: 12),
//                               decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.circular(12)),
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                   Row(
//                                     children: [
//                                       Radio.adaptive(
//                                           value: true,
//                                           fillColor: WidgetStatePropertyAll(
//                                               AppColor.appColor),
//                                           groupValue: [],
//                                           onChanged: (value) {}),
//                                       Text(
//                                         "VOIP Video Call",
//                                         style:
//                                             GoogleFonts.poppins(fontSize: 16),
//                                       )
//                                     ],
//                                   ),
//                                 ],
//                               ),
//                             ));
//                           });
//                     },
//                     child: Padding(
//                       padding: EdgeInsets.symmetric(horizontal: 12.0),
//                       child: Icon(Icons.phone),
//                     ),
//                   )
//                 ],
//               ),
//               Container(
//                 padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
//                 child: Column(
//                   children: [
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceAround,
//                       children: [
//                         Text(
//                           "U: ${widget.customerName}(${widget.toId})",
//                           style: GoogleFonts.poppins(
//                               fontSize: 14,
//                               color: Colors.green,
//                               fontWeight: FontWeight.w600),
//                         ),
//                         Text(
//                           widget.poojaName,
//                           style: GoogleFonts.poppins(
//                               fontSize: 14,
//                               color: Colors.green,
//                               fontWeight: FontWeight.w600),
//                         ),
//                       ],
//                     ),
//                     SizedBox(height: 8),
//                     Text(
//                       "This is a free group chat to sevice Astromall orders. It is not billed on a per min basis.",
//                       style: GoogleFonts.poppins(),
//                       textAlign: TextAlign.center,
//                     )
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//       body: Stack(
//         fit: StackFit.expand,
//         children: [
//           Image.asset(
//             'assets/images/rectangle.png',
//             fit: BoxFit.cover, // Cover the entire screen
//           ),
//           Chat(
//             messages: _messages,
//             onPreviewDataFetched: _handlePreviewDataFetched,
//             onSendPressed: _handleSendPressed,
//             slidableMessageBuilder: (p0, msgWidget) {
//               return SwipeTo(
//                 child: msgWidget,
//                 iconOnLeftSwipe: Icons.arrow_forward,
//                 iconOnRightSwipe: Icons.arrow_back,
//                 onRightSwipe: (details) {
//                   // if (isDelivered) {
//                   var index = _messages.indexOf(p0);

//                   if (_messages[index].type == types.MessageType.text &&
//                       _messages[index].status == types.Status.delivered) {
//                     types.TextMessage textMessage =
//                         _messages[index] as types.TextMessage;
//                     TLoggerHelper.info(p0.id);
//                     setState(() {
//                       // replyingTextMap = {p0.id: textMessage.text};
//                       // replyingImageUrlMap = {};
//                       referenceId = p0.id;
//                       messageToSend = textMessage.text;
//                       imageUrlToSend = "";
//                     });
//                   } else if (_messages[index].type == types.MessageType.image &&
//                       _messages[index].status == types.Status.delivered) {
//                     types.ImageMessage imageMessage =
//                         _messages[index] as types.ImageMessage;
//                     TLoggerHelper.info(p0.id);
//                     setState(() {
//                       // replyingImageUrlMap = {p0.id: imageMessage.uri};
//                       // replyingTextMap = {};
//                       referenceId = p0.id;
//                       messageToSend = "";
//                       imageUrlToSend = imageMessage.uri;
//                     });
//                   }
//                   // } else {
//                   //   showSnackBarWidget(
//                   //       context, "Previouse Message has to be delivered !!");
//                   // }
//                   // messageTextController.text = ;
//                 },
//                 swipeSensitivity: 5,
//                 offsetDx: 0.1,
//               );
//             },
//             onEndReached: _loadMessages,
//             showUserAvatars: true,
//             textMessageBuilder: (p0,
//                 {required messageWidth, required showName}) {
//               final message;
//               dynamic repliedMessage;
//               if (p0.remoteId != null) {
//                 message = _messages
//                     .firstWhere((element) => p0.remoteId == element.id);

//                 if (_messages
//                         .firstWhere((element) => p0.remoteId == element.id)
//                         .type
//                         .name ==
//                     'text') {
//                   repliedMessage = message as types.TextMessage;
//                   return TextRepliedMessageWidget(
//                     messageText: p0.text,
//                     widget: widget,
//                     repliedMessage: repliedMessage.text,
//                     repliedImageUrl: "",
//                   );
//                 } else {
//                   repliedMessage = message as types.ImageMessage;
//                   return TextRepliedMessageWidget(
//                     messageText: p0.text,
//                     widget: widget,
//                     repliedMessage: "",
//                     repliedImageUrl: repliedMessage.uri,
//                   );
//                 }
//               } else {
//                 return Container(
//                   padding: const EdgeInsets.all(10),
//                   color: Colors.white,
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       if (p0.author.firstName != null)
//                         Text(
//                           p0.author.firstName.toString(),
//                           style: GoogleFonts.poppins(
//                               fontSize: 12,
//                               fontWeight: FontWeight.bold,
//                               color: Colors.blueGrey),
//                         ),
//                       Text(
//                         p0.text,
//                         style: GoogleFonts.poppins(fontSize: 14),
//                       ),
//                     ],
//                   ),
//                 );
//               }
//             },
//             theme: DefaultChatTheme(
//                 messageBorderRadius: 12,
//                 backgroundColor: Colors.transparent,
//                 deliveredIcon: const Icon(
//                   Icons.done_all,
//                   color: Colors.black,
//                   size: 18,
//                 ),
//                 errorIcon: const Icon(
//                   Icons.error,
//                   color: Colors.red,
//                 ),
//                 sendingIcon: const Icon(
//                   Icons.timelapse_rounded,
//                   color: Colors.black,
//                   size: 18,
//                 ),
//                 inputBackgroundColor: Colors.grey.shade100,
//                 attachmentButtonIcon: const Icon(Icons.attachment),
//                 inputTextStyle: GoogleFonts.poppins(color: Colors.black),
//                 inputTextColor: Colors.black,
//                 inputBorderRadius: BorderRadius.zero),
//             inputOptions: InputOptions(
//               textEditingController: messageController,
//             ),
//             customBottomWidget: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 if (imageUrlToSend.isNotEmpty || messageToSend.isNotEmpty)
//                   IntrinsicHeight(
//                     child: Container(
//                       decoration: BoxDecoration(
//                           color: Colors.grey.shade100,
//                           borderRadius: const BorderRadius.only(
//                               topLeft: Radius.circular(12),
//                               topRight: Radius.circular(12))),
//                       width: double.infinity,
//                       padding: const EdgeInsets.only(top: 8, right: 8, left: 8),
//                       child: Container(
//                         width: MediaQuery.of(context).size.width * .9,
//                         decoration: BoxDecoration(
//                             color: Colors.white70,
//                             borderRadius: BorderRadius.circular(6)),
//                         padding: const EdgeInsets.all(8),
//                         child: Row(
//                           children: [
//                             Container(
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(6),
//                                   color: Colors.amber),
//                               width: 4,
//                             ),
//                             const SizedBox(
//                               width: 8,
//                             ),
//                             Expanded(
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                   Row(
//                                     mainAxisAlignment:
//                                         MainAxisAlignment.spaceBetween,
//                                     children: [
//                                       Text(
//                                         widget.customerName,
//                                         style: GoogleFonts.poppins(
//                                             fontWeight: FontWeight.w600,
//                                             color: Colors.black87),
//                                       ),
//                                       GestureDetector(
//                                           onTap: () => setState(() {
//                                                 referenceId = null;
//                                                 messageToSend = "";
//                                                 imageUrlToSend = "";
//                                               }),
//                                           child: const Icon(Icons.close,
//                                               color: Colors.grey))
//                                     ],
//                                   ),
//                                   const SizedBox(
//                                     height: 8,
//                                   ),
//                                   if (messageToSend.isNotEmpty)
//                                     Text(
//                                       messageToSend,
//                                       maxLines: 4,
//                                       overflow: TextOverflow.ellipsis,
//                                       textAlign: TextAlign.start,
//                                       style: GoogleFonts.poppins(
//                                           color: Colors.grey),
//                                     ),
//                                   if (imageUrlToSend.isNotEmpty)
//                                     Container(
//                                       width: MediaQuery.of(context).size.width *
//                                           .3,
//                                       height:
//                                           MediaQuery.of(context).size.height *
//                                               .08,
//                                       decoration: BoxDecoration(
//                                           borderRadius:
//                                               BorderRadius.circular(8),
//                                           image: DecorationImage(
//                                               fit: BoxFit.cover,
//                                               image: CachedNetworkImageProvider(
//                                                   imageUrlToSend))),
//                                     )
//                                 ],
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                   ),
//                 // Input(
//                 //   onSendPressed: _handleSendPressed,
//                 //   onAttachmentPressed: _handleAttachmentPressed,
//                 //   options: InputOptions(
//                 //     textEditingController: messageTextController,
//                 //   ),
//                 // ),
//                 Container(
//                   height: MediaQuery.of(context).size.height * .12,
//                   child: Column(
//                     children: [
//                       Container(
//                         color: Colors.white,
//                         padding: EdgeInsets.symmetric(horizontal: 12),
//                         width: MediaQuery.of(context).size.width,
//                         height: MediaQuery.of(context).size.height * .07,
//                         child: Row(
//                           children: [
//                             GestureDetector(
//                               onTap: () {
//                                 _handleAttachmentPressed();
//                               },
//                               child: Icon(
//                                 Icons.attachment_rounded,
//                                 size: 28,
//                               ),
//                             ),
//                             SizedBox(
//                               width: 12,
//                             ),
//                             Expanded(
//                               child: Padding(
//                                 padding: EdgeInsets.symmetric(horizontal: 8),
//                                 child: TextField(
//                                   controller: messageController,
//                                   maxLines: 5,
//                                   minLines: 1,
//                                   decoration: InputDecoration(
//                                     border: OutlineInputBorder(
//                                       borderSide: BorderSide.none,
//                                     ),
//                                   ),
//                                   textCapitalization:
//                                       TextCapitalization.sentences,
//                                 ),
//                               ),
//                             ),
//                             GestureDetector(
//                                 onTap: () {
//                                   _handleSendPressed(types.PartialText(
//                                       text: messageController.text));
//                                 },
//                                 child: Icon(Icons.send))
//                           ],
//                         ),
//                       ),
//                       Container(
//                         width: MediaQuery.of(context).size.width,
//                         padding: const EdgeInsets.symmetric(horizontal: 12),
//                         height: MediaQuery.of(context).size.height * .05,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceAround,
//                           children: [
//                             GestureDetector(
//                                 onTap: () {
//                                   _handleImageSelection();
//                                 },
//                                 child: Icon(Icons.camera_alt, size: 24)),
//                             GestureDetector(
//                               onTap: () {
//                                 // Navigator.of(context).push(MaterialPageRoute(
//                                 //     builder: (context) => ListenableProvider(
//                                 //           create: (context) => KundliProvider(),
//                                 //           child: ViewKundliScreen(
//                                 //               dob: DateFormat('dd/MM/yyyy')
//                                 //                   .format(DateTime.parse(
//                                 //                       widget.dob)),
//                                 //               tob: TFormatter
//                                 //                   .convertTo24HourFormat(
//                                 //                       widget.tob),
//                                 //               lat: widget.lat,
//                                 //               pob: "",
//                                 //               userName: widget.customerName,
//                                 //               lon: widget.lon),
//                                 //         )));
//                               },
//                               child: Image.asset(
//                                   "assets/images/kundli_icon.png",
//                                   height: 32),
//                             ),
//                             GestureDetector(
//                               onTap: () {
//                                 // Navigator.of(context).push(MaterialPageRoute(
//                                 //   builder: (context) => ListenableProvider(
//                                 //     create: (context) =>
//                                 //         SuggestRemedyProvider(),
//                                 //     child: SuggestNextRemedy(
//                                 //       orderId: widget.orderId,
//                                 //       userId: widget.toId,
//                                 //     ),
//                                 //   ),
//                                 // ));
//                               },
//                               child: Image.asset("assets/images/namaste.png",
//                                   height: 32),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 )
//               ],
//             ),
//             onMessageTap: (context, p1) {},
//             imageMessageBuilder: (p0, {required messageWidth}) {
//               final message;
//               dynamic repliedMessage;
//               if (p0.remoteId != null) {
//                 message = _messages
//                     .firstWhere((element) => p0.remoteId == element.id);

//                 if (_messages
//                         .firstWhere((element) => p0.remoteId == element.id)
//                         .type
//                         .name ==
//                     'image') {
//                   repliedMessage = message as types.ImageMessage;
//                   return ImageRepliedMessageWidget(
//                     imageUrl: p0.uri,
//                     widget: widget,
//                     repliedMessage: "",
//                     repliedImageUrl: repliedMessage.uri,
//                   );
//                 } else {
//                   repliedMessage = message as types.TextMessage;
//                   return ImageRepliedMessageWidget(
//                     imageUrl: p0.uri,
//                     widget: widget,
//                     repliedMessage: repliedMessage.text,
//                     repliedImageUrl: "",
//                   );
//                 }
//                 //else {
//                 //   repliedMessage = (message as types.TextMessage);

//                 //   return TextRepliedMessageWidget(
//                 //       messageText: repliedMessage.text,
//                 //       widget: widget,
//                 //       repliedMessage: repliedMessage);
//                 // }
//                 return const SizedBox();
//               } else {
//                 return Container(
//                   padding: const EdgeInsets.all(8),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       if (p0.author.firstName != null)
//                         Text(
//                           p0.author.firstName.toString(),
//                           style: GoogleFonts.poppins(
//                             fontSize: 12,
//                             fontWeight: FontWeight.bold,
//                             color: Colors.blueGrey,
//                           ),
//                         ),
//                       SizedBox(
//                         child: Container(
//                           width: messageWidth.toDouble(),
//                           constraints: BoxConstraints(
//                               maxHeight:
//                                   MediaQuery.of(context).size.height * .5),
//                           decoration: BoxDecoration(
//                             color: Colors.white,
//                             borderRadius: BorderRadius.circular(8),
//                           ),
//                           child: p0.uri.contains(
//                                   "https://jyotish.techsaga.live/storage/attachments")
//                               ? CachedNetworkImage(
//                                   imageUrl: p0.uri, fit: BoxFit.fitWidth)
//                               : Image.file(file!),
//                         ),
//                       )
//                     ],
//                   ),
//                 );
//               }
//             },
//             showUserNames: true,
//             user: user,
//           ),
//         ],
//       ),
//     );
//   }
// }

// class TextRepliedMessageWidget extends StatelessWidget {
//   const TextRepliedMessageWidget(
//       {super.key,
//       required this.messageText,
//       required this.widget,
//       required this.repliedImageUrl,
//       required this.repliedMessage});
//   final String messageText;
//   final PoojaCommunicationScreen widget;
//   final String repliedImageUrl;
//   final String repliedMessage;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.all(8),
//       decoration: const BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.only(
//             topLeft: Radius.circular(12),
//             topRight: Radius.circular(12),
//             bottomLeft: Radius.circular(12),
//           )),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           IntrinsicHeight(
//             child: Container(
//               decoration: BoxDecoration(
//                   color: Colors.blueGrey.withOpacity(.1),
//                   borderRadius: BorderRadius.circular(6)),
//               padding: const EdgeInsets.all(8),
//               child: Row(
//                 children: [
//                   Container(
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(6),
//                         color: Colors.amber),
//                     width: 4,
//                   ),
//                   const SizedBox(
//                     width: 8,
//                   ),
//                   Expanded(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text(
//                               widget.customerName,
//                               style: GoogleFonts.poppins(
//                                   fontWeight: FontWeight.w600,
//                                   color: Colors.black87),
//                             ),
//                           ],
//                         ),
//                         const SizedBox(
//                           height: 6,
//                         ),
//                         if (repliedImageUrl.isNotEmpty)
//                           Container(
//                             width: MediaQuery.of(context).size.width * .3,
//                             height: MediaQuery.of(context).size.height * .08,
//                             decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.circular(8),
//                                 image: DecorationImage(
//                                     fit: BoxFit.cover,
//                                     image: CachedNetworkImageProvider(
//                                         repliedImageUrl))),
//                           ),
//                         if (repliedMessage.isNotEmpty)
//                           Text(
//                             repliedMessage,
//                             maxLines: 4,
//                             overflow: TextOverflow.ellipsis,
//                             textAlign: TextAlign.start,
//                             style: GoogleFonts.poppins(color: Colors.grey),
//                           ),
//                       ],
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           const SizedBox(height: 8),
//           Text(
//             messageText,
//             style: GoogleFonts.poppins(fontSize: 14),
//           )
//         ],
//       ),
//     );
//   }
// }

// class ImageRepliedMessageWidget extends StatelessWidget {
//   const ImageRepliedMessageWidget(
//       {super.key,
//       required this.imageUrl,
//       required this.widget,
//       required this.repliedMessage,
//       required this.repliedImageUrl});
//   final String imageUrl;
//   final PoojaCommunicationScreen widget;
//   final String repliedMessage;
//   final String repliedImageUrl;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.all(8),
//       decoration: const BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.only(
//             topLeft: Radius.circular(12),
//             topRight: Radius.circular(12),
//             bottomLeft: Radius.circular(12),
//           )),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           IntrinsicHeight(
//             child: Container(
//               decoration: BoxDecoration(
//                   color: Colors.blueGrey.withOpacity(.1),
//                   borderRadius: BorderRadius.circular(6)),
//               padding: const EdgeInsets.all(8),
//               child: InkWell(
//                 onTap: () {},
//                 child: Row(
//                   children: [
//                     Container(
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(6),
//                           color: Colors.amber),
//                       width: 4,
//                     ),
//                     const SizedBox(
//                       width: 8,
//                     ),
//                     Expanded(
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Text(
//                                 widget.customerName,
//                                 style: GoogleFonts.poppins(
//                                     fontWeight: FontWeight.w600,
//                                     color: Colors.black87),
//                               ),
//                             ],
//                           ),
//                           const SizedBox(
//                             height: 6,
//                           ),
//                           if (repliedImageUrl.isNotEmpty)
//                             Container(
//                               width: MediaQuery.of(context).size.width * .3,
//                               height: MediaQuery.of(context).size.height * .08,
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(8),
//                                   image: DecorationImage(
//                                       fit: BoxFit.cover,
//                                       image: CachedNetworkImageProvider(
//                                           repliedImageUrl))),
//                             ),
//                           if (repliedMessage.isNotEmpty)
//                             Text(
//                               repliedMessage,
//                               maxLines: 4,
//                               overflow: TextOverflow.ellipsis,
//                               textAlign: TextAlign.start,
//                               style: GoogleFonts.poppins(color: Colors.grey),
//                             ),
//                           // if (replyingImageUrlMap.isNotEmpty)
//                           //   Container(
//                           //     width:
//                           //         MediaQuery.of(context).size.width * .3,
//                           //     height: MediaQuery.of(context).size.height *
//                           //         .08,
//                           //     decoration: BoxDecoration(
//                           //         borderRadius: BorderRadius.circular(8),
//                           //         image: DecorationImage(
//                           //             fit: BoxFit.cover,
//                           //             image: CachedNetworkImageProvider(
//                           //                 replyingImageUrlMap
//                           //                     .entries.first.value))),
//                           //   )
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           const SizedBox(height: 8),
//           Container(
//             height: MediaQuery.of(context).size.height * .3,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(8),
//                 image: DecorationImage(
//                     fit: BoxFit.cover,
//                     image: CachedNetworkImageProvider(imageUrl))),
//           )
//         ],
//       ),
//     );
//   }
// }
