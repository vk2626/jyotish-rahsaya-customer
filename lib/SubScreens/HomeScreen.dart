import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/Model/celebrity.customer.model.dart';
import '../Core/Model/recharge.amount.model.dart';
import '../Core/Provider/chatting_provider.dart';
import '../Core/helper_functions.dart';
import '../Screens/ProfileScreen.dart';
import '../Screens/mallScreens/astromall_allitem_screen.dart';
import '../router_constants.dart';
import '../Core/Model/banner.model.dart';
import '../Core/Model/blog.model.dart';
import '../Core/Model/jyotish.mall.model.dart';
import '../Core/Provider/kundli_provider.dart';
import '../Screens/LoginScreen.dart';
import '../Screens/horoScopeScreens/kundliScreens/create_new_kundli_matching_screen.dart';
import '../Screens/payment.info.dart';
import '../Core/Model/OrderHistoryModel.dart';
import '../Screens/flutter_chat_screen.dart';
import '../Screens/order.history.dart';
import '../Core/Provider/go_live_provider.dart';
import '../Core/formatter.dart';
import '../Core/logger_helper.dart';
import '../Screens/ChatIntakeForm.dart';
import '../Screens/go_live_screen.dart';
import '../screens/flutter_chat_screen_new.dart';
import '../utils/custom_circular_progress_indicator.dart';
import 'youtube_screen.dart';
import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Api/HttpClient.dart';
import '../Core/Model/astrologer.list.model.dart';
import '../Screens/blogScreens/latest.blog.viewall.dart';
import '../Screens/horoScopeScreens/daily.horoscope.screen.dart';
import '../Screens/mallScreens/mall.item.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:transparent_image/transparent_image.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../Screens/blogScreens/blog.details.screen.dart';
import '../Screens/horoScopeScreens/kundliScreens/free_kundli_screen.dart';
import '../Screens/search.screen.dart';
import '../utils/DashSeparator.dart';
import 'ChatList.dart';

class HomeScreen extends StatefulWidget {
  final IntCallback onChangedItem;

  HomeScreen({required this.onChangedItem});

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

typedef void IntCallback(int id);

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin {
  // NotificationHandler notificationHandler = NotificationHandler();

  int userId = 0;
  String customerName = "";
  String walletAmount = "0";
  String profileImage = "";
  String waitChatID = "";
  DioClient? client;
  String price = "";
  int customerID = 0;
  bool isLoading = true;

  late List<dynamic> liveAstrolgerlist = [];
  List<OrderHistory> orderHistoryList = [];
  bool isOrderHistoryListLoading = true;
  List<AstrologerElement> resAstroList = [];
  BlogModel? blogData;
  CelebrityCustomerModel? celebrityList;
  JyotishMallModel? mallData;
  BannerModel? bannerData;
  @override
  void initState() {
    super.initState();
    // notificationHandler.initializeNotification();
    getUserId();

    runProviders();
    getWalletAmount();
    getOrders();
    // requestNotification();
    // messagingService.init(context);
    // messagingService.addListener(() => mounted ? setState(() {}) : null);
  }

  getOrders() async {
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getMyOrderList()
        .then((value) {
      if (value != null) {
        TLoggerHelper.warning("OrderHistory Working !!");
        orderHistoryList = value;
      }
    });
  }

  void runProviders() async {
    await _liveAstrologerList();
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getBannerProvider()
        .then((value) {
      if (value != null) {
        bannerData = value;
      }
    });
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getAstrologerListNew()
        .then((value) {
      if (value != null) {
        resAstroList = value.astrologers!.data;
      }
    });
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getBlogNew()
        .then((value) {
      if (value != null) {
        blogData = value;
      }
    });
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getCelebrityCustomerNew()
        .then((value) {
      if (value != null) {
        celebrityList = value;
      }
    });
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getJyotishMallProvider()
        .then((value) {
      if (value != null) {
        mallData = value;
      }
    });
    setState(() {
      isLoading = false;
    });
  }

  // requestNotification() async {
  //   await FlutterCallkitIncoming.requestNotificationPermission({
  //     "rationaleMessagePermission":
  //         "Notification permission is required, to show notification.",
  //     "postNotificationMessageRequired":
  //         "Notification permission is required, Please allow notification permission from setting."
  //   }).then((value) => TLoggerHelper.info("Permission Granted!!"));
  // }
  void sendAndGetResult(String name, int? id, String avatar,
      BuildContext context, String astroID, int? free_chat) async {
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MultiProvider(providers: [
              ListenableProvider(create: (context) => ListOfApisProvider()),
              // ListenableProvider(create: (context) => WaitListNotifier()),
            ], child: ChatIntakeForm(name, id, astroID, "Chat", free_chat))));
    print("resultdata${result.toString()}");
    if (result == "true") {
      showConfirmationDialog(name, avatar);
    }
  }

  getWalletAmount() {
    THelperFunctions.getWalletAmount().then((value) {
      setState(() {
        walletAmount = value.toString();
      });
    });
  }

  nextOnlineDialog(
      {required String astrologerName,
      required String astrologerImage,
      required String nextOnlineTime,
      required String nextOnlineDate}) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // Stack(
                    //   children: [
                    //     Positioned(
                    //       child: DashSeparator(
                    //         color: Colors.grey,
                    //         height: 2.0,
                    //       ),
                    //       right: 50,
                    //       left: 50,
                    //       top: 40,
                    //     ),
                    //     Row(
                    //       children: [

                    //         Spacer(),
                    //         SizedBox(
                    //           width: 100,
                    //           height: 150,
                    //           child: Column(
                    //             crossAxisAlignment: CrossAxisAlignment.center,
                    //             children: [
                    //               Card(
                    //                 clipBehavior: Clip.antiAliasWithSaveLayer,
                    //                 child: CircleAvatar(
                    //                   radius: 35,
                    //                   backgroundImage:
                    //                       CachedNetworkImageProvider(
                    //                           "$userImage"),
                    //                 ),
                    //                 shape: CircleBorder(
                    //                     side: BorderSide(
                    //                         color: AppColor.appColor,
                    //                         width: 2)),
                    //               ),
                    //               Text(
                    //                 "${name}",
                    //                 textAlign: TextAlign.center,
                    //                 overflow: TextOverflow.ellipsis,
                    //                 style: GoogleFonts.poppins(
                    //                   color: Colors.black,
                    //                   fontSize: 14,
                    //                   fontWeight: FontWeight.w600,
                    //                 ),
                    //               )
                    //             ],
                    //           ),
                    //         )
                    //       ],
                    //     ),
                    //   ],
                    // ),
                    Column(
                      children: [
                        CircleAvatar(
                          radius: 35,
                          backgroundImage:
                              CachedNetworkImageProvider(astrologerImage),
                        ),
                        Wrap(
                          children: [
                            Text(
                              TFormatter.capitalizeSentence(astrologerName),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Text(
                        "You will be connecting with".tr() +
                            " $astrologerName in a while.",
                        style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Next Online Time of the Astrologer is".tr() +
                          " :- $nextOnlineDate $nextOnlineTime",
                      style: GoogleFonts.poppins(
                          color: Colors.black54, fontSize: 15),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          // showCancelBookingDialog(waitChatID);
                        },
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            backgroundColor: AppColor.appColor),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "OK",
                            style: GoogleFonts.poppins(color: Colors.black),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }

  showConfirmationDialog(String name, String userImage) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return PopScope(
              canPop: false,
              onPopInvoked: (didPop) {
                // logic
              },
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(20.0)), //this right here
                child: Container(
                  height: (MediaQuery.of(context).size.height / 2) - 50,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Stack(
                          children: [
                            Positioned(
                              child: DashSeparator(
                                color: Colors.grey,
                                height: 2.0,
                              ),
                              right: 50,
                              left: 50,
                              top: 40,
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: 100,
                                  height: 150,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Card(
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        child: CircleAvatar(
                                          radius: 35,
                                          backgroundImage:
                                              CachedNetworkImageProvider(
                                                  "$profileImage"),
                                        ),
                                        shape: CircleBorder(
                                            side: BorderSide(
                                                color: AppColor.appColor,
                                                width: 2)),
                                      ),
                                      Wrap(
                                        children: [
                                          Text(
                                            "${customerName}",
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Spacer(),
                                SizedBox(
                                  width: 100,
                                  height: 150,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Card(
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        child: CircleAvatar(
                                          radius: 35,
                                          backgroundImage:
                                              CachedNetworkImageProvider(
                                                  "$userImage"),
                                        ),
                                        shape: CircleBorder(
                                            side: BorderSide(
                                                color: AppColor.appColor,
                                                width: 2)),
                                      ),
                                      Text(
                                        "${name}",
                                        textAlign: TextAlign.center,
                                        overflow: TextOverflow.fade,
                                        style: GoogleFonts.poppins(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        Text("You will be connecting with $name in a while.",
                            style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.w700),
                            textAlign: TextAlign.center),
                        SizedBox(height: 10),
                        Text(
                          "While you wait for $name you may also explore other astrologers and join their wishlist.",
                          style: GoogleFonts.poppins(
                              color: Colors.black54, fontSize: 15),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                              showCancelBookingDialog(waitChatID);
                            },
                            style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                backgroundColor: AppColor.appColor),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                "OK",
                                style: GoogleFonts.poppins(color: Colors.black),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // log("Rebuilding Home Screen");
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey.shade100,
        body: RefreshIndicator(
          color: Colors.white,
          backgroundColor: AppColor.appColor,
          onRefresh: () async {
            runProviders();
          },
          child: isLoading
              ? Center(child: CustomCircularProgressIndicator())
              : Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          // Padding(
                          // padding:
                          //     EdgeInsets.only(left: 10, right: 10, top: 10),
                          //   child: InkWell(
                          //     onTap: () {
                          //       Navigator.of(context).push(
                          //           SlideRightRoute(page: SearchScreen()));
                          //       print("Search");
                          //     },
                          //     child: Card(
                          //       color: Colors.white,
                          //       elevation: 5,
                          //       child: Row(
                          //         children: <Widget>[
                          //           SizedBox(height: 40, width: 20),
                          //           Icon(Icons.search),
                          //           SizedBox(width: 10),
                          //           Text("Search Astrologers",style: Googlefont,),
                          //         ],
                          //       ),
                          //     ),
                          //   ),
                          // ),

                          Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 12, vertical: 8),
                            child: Container(
                              height: MediaQuery.of(context).size.height * .05,
                              decoration: BoxDecoration(boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.shade200,
                                    blurRadius: 1,
                                    spreadRadius: 3,
                                    offset: Offset(1, 1))
                              ], borderRadius: BorderRadius.circular(8)),
                              child: InkWell(
                                onTap: () =>
                                    Navigator.of(context).push(SlideRightRoute(
                                        page: ListenableProvider(
                                  create: (context) => ListOfApisProvider(),
                                  child: SearchScreen(),
                                ))),
                                child: TextField(
                                  decoration: InputDecoration(
                                      filled: true,
                                      contentPadding: EdgeInsets.zero,
                                      fillColor: Colors.white,
                                      hintText:
                                          "Search Astrologers, Astromall Products"
                                              .tr(),
                                      hintStyle:
                                          GoogleFonts.poppins(fontSize: 14),
                                      prefixIcon: Icon(
                                        Icons.search,
                                        color: Colors.grey,
                                      ),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      enabled: false),
                                ),
                              ),
                            ),
                          ),
                          if (bannerData != null)
                            CarouselSlider.builder(
                              itemCount: bannerData!.data.length,
                              itemBuilder: (BuildContext context, int itemIndex,
                                      int pageViewIndex) =>
                                  InkWell(
                                onTap: () {
                                  widget.onChangedItem(1);
                                },
                                child: Card(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0))),
                                  child: CachedNetworkImage(
                                    imageUrl: bannerData!.data.length != 0
                                        ? bannerData!.data[itemIndex].image
                                            .toString()
                                        : 'https://admin.jyotishrahsya.com/uploads/banner/2024022211270109.jpg',

                                    // image: ,
                                    fit: BoxFit.fill,
                                    width: double.infinity,
                                    placeholder: (context, url) {
                                      return CachedNetworkImage(
                                        imageUrl:
                                            'https://admin.jyotishrahsya.com/uploads/banner/2024022211270109.jpg',
                                        fit: BoxFit.fill,
                                        width: double.infinity,
                                      );
                                    },
                                  ),
                                ),
                              ),
                              options: CarouselOptions(
                                height:
                                    MediaQuery.of(context).size.height * .225,
                                autoPlay: bannerData!.data.length > 1,
                                enlargeCenterPage: true,
                                initialPage: 0,
                              ),
                            ),
                          SizedBox(height: 4),
                          Container(
                            height: 150,
                            width: double.infinity,
                            decoration: BoxDecoration(color: Colors.white70),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(
                                          SlideRightRoute(page: HoroscopeScreen(
                                        onChangedItem: (int id) {
                                          setState(() {
                                            widget.onChangedItem(99);
                                          });
                                        },
                                      )));
                                    },
                                    child: horoScopeWidget(
                                        "asset/images/sunrise_new.png",
                                        // "asset/images/sunrise.png",
                                        "Daily".tr() + "\n" + "Horoscope".tr()),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () async {
                                      if ((await THelperFunctions.getUserId() !=
                                          "0")) {
                                        // Navigate to the FreeKundliScreen if authenticated
                                        Navigator.of(context).push(
                                          SlideRightRoute(
                                            page: ListenableProvider(
                                              create: (context) =>
                                                  KundliProvider(),
                                              child: FreeKundliScreen(),
                                            ),
                                          ),
                                        );
                                      } else {
                                        context
                                            .goNamed(RouteConstants.authScreen);
                                        Fluttertoast.showToast(
                                            msg:
                                                "You must login before using free kundli !!");
                                      }
                                    },
                                    child: horoScopeWidget(
                                        "asset/images/virgo_new.png",
                                        // "asset/images/virgo.png",
                                        "Free".tr() + "\n" + "Kundli".tr()),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () async {
                                      if ((await THelperFunctions.getUserId() ==
                                          "0")) {
                                        context
                                            .goNamed(RouteConstants.authScreen);
                                      } else {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                          builder: (context) =>
                                              CreateNewKundliMatchingScreen(),
                                        ));
                                      }
                                    },
                                    child: horoScopeWidget(
                                        "asset/images/wedding_rings_new.png",
                                        // "asset/images/wedding-rings.png",
                                        "Kundli".tr() + "\n" + "Matching".tr()),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      widget.onChangedItem(1);
                                    },
                                    child: horoScopeWidget(
                                        "asset/images/free_new.png",
                                        // "asset/images/free.png",
                                        "Free".tr() + "\n" + "Chat".tr()),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          //My Orders Section

                          liveAstrolgerlist.isNotEmpty
                              ? Container(
                                  color: Colors.white,
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(height: 10),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Row(
                                          // mainAxisAlignment:
                                          //     MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "Live Astrologers".tr(),
                                              style: GoogleFonts.poppins(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            // InkWell(
                                            //   onTap: () {
                                            //     widget.onChangedItem(99);
                                            //   },
                                            //   child: Text(
                                            //     liveAstrolgerlist.length > 5
                                            //         ? "View All".tr()
                                            //         : "",
                                            //     style: GoogleFonts.poppins(
                                            //         fontSize: 14,
                                            //         fontWeight:
                                            //             FontWeight.w500),
                                            //   ),
                                            // )
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      ChangeNotifierProvider(
                                        create: (context) => GoLiveProvider(),
                                        child: Container(
                                          width: double.infinity,
                                          height: 160,
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount: liveAstrolgerlist.length,
                                            itemBuilder:
                                                (BuildContext context, int i) {
                                              return InkWell(
                                                onTap: () async {
                                                  // SharedPreferences prefs =
                                                  //     await SharedPreferences
                                                  //         .getInstance();
                                                  await Provider.of<
                                                              GoLiveProvider>(
                                                          context,
                                                          listen: false)
                                                      .goLive(
                                                    context: context,
                                                  )
                                                      .then((val) {
                                                    TLoggerHelper.debug(
                                                        "Going To Live Screen ${val.toString()}");

                                                    // Navigator.of(context)
                                                    //     .push(MaterialPageRoute(
                                                    //   builder: (context) => MultiProvider(
                                                    //       providers: [
                                                    //         ListenableProvider(
                                                    //             create: (context) =>
                                                    //                 GoLiveProvider())
                                                    //       ],
                                                    //       child: AgoraLiveScreen(
                                                    //           token:
                                                    //               liveAstrolgerlist[
                                                    //                       i]
                                                    //                   .live_token,
                                                    //           channelName:
                                                    //               liveAstrolgerlist[
                                                    //                       i]
                                                    //                   .channel_name,
                                                    //           sessionId: "",
                                                    //           liveId:
                                                    //               liveAstrolgerlist[
                                                    //                       i]
                                                    //                   .live_id,
                                                    //           userId:
                                                    //               liveAstrolgerlist[
                                                    //                       i]
                                                    //                   .id)),
                                                    // ));

                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ListenableProvider(
                                                                  create: (context) =>
                                                                      GoLiveProvider(),
                                                                  child:
                                                                      GoLiveScreen(
                                                                    token: val
                                                                        .first,
                                                                    channelName:
                                                                        val[1],
                                                                    liveId:
                                                                        val[2],
                                                                  ),
                                                                )));
                                                  });
                                                },
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  height: 160,
                                                  width: 120,
                                                  child: Card(
                                                    elevation: 2,
                                                    clipBehavior: Clip
                                                        .antiAliasWithSaveLayer,
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    10.0))),
                                                    child: Stack(
                                                      children: [
                                                        Positioned(
                                                          right: 0,
                                                          left: 0,
                                                          top: 0,
                                                          bottom: 0,
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl:
                                                                liveAstrolgerlist[
                                                                        i]
                                                                    ["avatar"],
                                                            fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                        Positioned(
                                                          child: Text(
                                                            liveAstrolgerlist[i]
                                                                ['name'],
                                                            style: GoogleFonts
                                                                .poppins(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                          right: 5,
                                                          left: 5,
                                                          bottom: 5,
                                                        ),
                                                        Positioned(
                                                          child: Column(
                                                            children: <Widget>[
                                                              Visibility(
                                                                child:
                                                                    IconButton(
                                                                  visualDensity: VisualDensity(
                                                                      horizontal:
                                                                          -4.0,
                                                                      vertical:
                                                                          -4.0),
                                                                  padding:
                                                                      EdgeInsets
                                                                          .zero,
                                                                  icon: Icon(
                                                                    Icons
                                                                        .videocam,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                  onPressed:
                                                                      () {},
                                                                ),
                                                                visible: false,
                                                              ),
                                                              Visibility(
                                                                child:
                                                                    IconButton(
                                                                  visualDensity: VisualDensity(
                                                                      horizontal:
                                                                          -4.0,
                                                                      vertical:
                                                                          -4.0),
                                                                  padding:
                                                                      EdgeInsets
                                                                          .zero,
                                                                  icon: Icon(
                                                                    Icons.call,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                  onPressed:
                                                                      () {},
                                                                ),
                                                                visible: true,
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                )
                              : SizedBox(),

                          if (orderHistoryList.isNotEmpty)
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "My Orders".tr(),
                                          style: GoogleFonts.poppins(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700),
                                        ),
                                        GestureDetector(
                                          child: Text(
                                            "View All".tr(),
                                            style: GoogleFonts.poppins(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          onTap: () {
                                            // Ensure widget.onChangedItem is correctly implemented
                                            // widget.onChangedItem(1);
                                            Navigator.of(context).push(
                                                SlideRightRoute(
                                                    page: OrderHistoryScreen(
                                                        selected_index: 1)));
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        .15,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: orderHistoryList.length > 3
                                          ? 3
                                          : orderHistoryList.length,
                                      itemBuilder: (context, index) => Card(
                                        shadowColor: Colors.blueGrey,
                                        shape: BeveledRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                        color: Colors.grey.shade50,
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 10.0,
                                          ),
                                          child: Row(
                                            children: [
                                              Stack(
                                                children: [
                                                  Card(
                                                    clipBehavior: Clip
                                                        .antiAliasWithSaveLayer,
                                                    child: CircleAvatar(
                                                      radius: 40,
                                                      backgroundImage:
                                                          CachedNetworkImageProvider(
                                                              orderHistoryList[
                                                                      index]
                                                                  .user
                                                                  .avatar),
                                                    ),
                                                    shape: CircleBorder(
                                                      side: BorderSide(
                                                          color:
                                                              AppColor.appColor,
                                                          width: 2),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    bottom: 8,
                                                    right: 8,
                                                    child: CircleAvatar(
                                                      radius: 7,
                                                      child: CircleAvatar(
                                                        radius: 5,
                                                        backgroundColor: orderHistoryList[
                                                                        index]
                                                                    .user
                                                                    .isAstrologerOnline ==
                                                                1
                                                            ? Colors.green
                                                            : Colors
                                                                .grey, // Green for online, grey for offline
                                                        child: Tooltip(
                                                          message: orderHistoryList[
                                                                          index]
                                                                      .user
                                                                      .isAstrologerOnline ==
                                                                  1
                                                              ? 'Online'.tr()
                                                              : 'Offline'.tr(),
                                                          child: Container(
                                                            height: 20,
                                                            width: 20,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(width: 18),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    TFormatter
                                                        .capitalizeSentence(
                                                            orderHistoryList[
                                                                    index]
                                                                .user
                                                                .name),
                                                    style: GoogleFonts.poppins(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 16),
                                                  ),
                                                  Text(
                                                    TFormatter.formatDateOnly(
                                                        orderHistoryList[index]
                                                            .endTime!),
                                                    style: GoogleFonts.poppins(
                                                        fontSize: 14),
                                                  ),
                                                  SizedBox(height: 6),
                                                  Row(
                                                    children: [
                                                      GestureDetector(
                                                        onTap: () {
                                                          var data =
                                                              orderHistoryList[
                                                                  index];
                                                          TLoggerHelper.warning(data
                                                              .user
                                                              .isAstrologerOnline
                                                              .toString());
                                                          Navigator.of(context)
                                                              .push(SlideRightRoute(
                                                                  page: MultiProvider(
                                                            providers: [
                                                              ListenableProvider(
                                                                create: (context) =>
                                                                    ChattingProvider(),
                                                              ),
                                                              ListenableProvider(
                                                                create: (context) =>
                                                                    ListOfApisProvider(),
                                                              ),
                                                            ],
                                                            child:
                                                                FlutterChatScreen(
                                                              dob: "",
                                                              lat: "",
                                                              lon: "",
                                                              tob: "",
                                                              // addNavigation:
                                                              //     () {
                                                              //   var data =
                                                              //       orderHistoryList[
                                                              //           index];

                                                              //   context
                                                              //       .pushNamed(
                                                              //     RouteConstants
                                                              //         .astrologerProfile,
                                                              //     queryParameters: {
                                                              //       'astrologerId': data
                                                              //           .user.id
                                                              //           .toString(),
                                                              //       'type':
                                                              //           'chat',
                                                              //     },
                                                              //   );
                                                              // },
                                                              reviewId: data
                                                                      .review
                                                                      .id ??
                                                                  -1,
                                                              initialRating:
                                                                  int.parse(data
                                                                          .review
                                                                          .rate ??
                                                                      "0"),
                                                              initialReview: data
                                                                      .review
                                                                      .description ??
                                                                  "",
                                                              orderID:
                                                                  data.orderId,
                                                              cummID: data.id
                                                                  .toString(),
                                                              chat_price: 0,
                                                              fromId:
                                                                  data.userId,
                                                              toId: data
                                                                  .astrologerId,
                                                              timerDuration:
                                                                  Duration.zero,
                                                              astrologerName: TFormatter
                                                                  .capitalizeSentence(
                                                                      orderHistoryList[
                                                                              index]
                                                                          .user
                                                                          .name),
                                                              astrologerImage:
                                                                  data.user
                                                                      .avatar,
                                                              messageInfo: {},
                                                              infoId:
                                                                  data.infoId,
                                                              isToView: true,
                                                            ),
                                                          )))
                                                              .whenComplete(() {
                                                            THelperFunctions
                                                                .checkWaitList();
                                                          });
                                                        },
                                                        child: orderHistoryList[
                                                                        index]
                                                                    .type ==
                                                                "chat"
                                                            ? Container(
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                        horizontal:
                                                                            24,
                                                                        vertical:
                                                                            8),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  border: Border.all(
                                                                      color: AppColor
                                                                          .appColor),
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                child: Text(
                                                                  "View".tr() +
                                                                      " " +
                                                                      "Chat"
                                                                          .tr(),
                                                                  style: GoogleFonts.poppins(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      fontSize:
                                                                          16),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                      ),
                                                      SizedBox(width: 8),
                                                      GestureDetector(
                                                        onTap: () {
                                                          var data =
                                                              orderHistoryList[
                                                                  index];

                                                          context.pushNamed(
                                                            RouteConstants
                                                                .astrologerProfile,
                                                            queryParameters: {
                                                              'astrologerId': data
                                                                  .user.id
                                                                  .toString(),
                                                              'type': 'chat',
                                                            },
                                                          );
                                                        },
                                                        child: Container(
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      24,
                                                                  vertical: 8),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: AppColor
                                                                .appColor,
                                                          ),
                                                          child: Text(
                                                            "${TFormatter.capitalize(orderHistoryList[index].type).tr()}" +
                                                                " " +
                                                                "Again".tr(),
                                                            style: GoogleFonts
                                                                .poppins(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                        .white),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                          /*Astrologers Section*/
                          SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: Column(
                              children: [
                                SizedBox(height: 10),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Astrologers".tr(),
                                        style: GoogleFonts.poppins(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      GestureDetector(
                                        child: Text(
                                          "View All".tr(),
                                          style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        onTap: () {
                                          widget.onChangedItem(1);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 10),
                                Container(
                                  width: double.infinity,
                                  height: 210,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    itemCount: resAstroList != null
                                        ? (resAstroList.length > 5
                                            ? 5
                                            : resAstroList.length)
                                        : 0,
                                    itemBuilder: (BuildContext context, int i) {
                                      return Container(
                                        margin: EdgeInsets.only(left: 10),
                                        color: Colors.white,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .4,
                                        child: GestureDetector(
                                          onTap: () {
                                            context.pushNamed(
                                              RouteConstants.astrologerProfile,
                                              queryParameters: {
                                                'astrologerId': resAstroList[i]
                                                    .id
                                                    .toString(),
                                                'type': 'chat',
                                                'isAstrologerOnline':
                                                    resAstroList[i]
                                                        .isAstrologerOnline
                                                        .toString()
                                              },
                                            );
                                          },
                                          child: Card(
                                            color: Colors.white,
                                            elevation: 2,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10.0))),
                                            child: Stack(
                                              children: [
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Stack(
                                                      children: [
                                                        Center(
                                                          child: Card(
                                                            clipBehavior: Clip
                                                                .antiAliasWithSaveLayer,
                                                            child: CircleAvatar(
                                                              radius: 40,
                                                              backgroundImage:
                                                                  CachedNetworkImageProvider(
                                                                      resAstroList[
                                                                              i]
                                                                          .avatar),
                                                            ),
                                                            shape: CircleBorder(
                                                              side: BorderSide(
                                                                  color: AppColor
                                                                      .appColor,
                                                                  width: 2),
                                                            ),
                                                          ),
                                                        ),
                                                        if (resAstroList[i]
                                                                .label !=
                                                            0)
                                                          Positioned(
                                                            bottom: -4,
                                                            left: 0,
                                                            right: 0,
                                                            child: Center(
                                                              child: Card(
                                                                color: Colors
                                                                    .amber,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius
                                                                                .all(
                                                                          Radius.circular(
                                                                              10.0),
                                                                        ),
                                                                        side: BorderSide(
                                                                            color:
                                                                                Colors.yellow)),
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              12,
                                                                          right:
                                                                              12,
                                                                          bottom:
                                                                              3,
                                                                          top:
                                                                              2),
                                                                  child: Text(
                                                                      TFormatter.capitalizeSentence(resAstroList[i]
                                                                          .astrologer
                                                                          .labelText),
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .fade,
                                                                      style: GoogleFonts.poppins(
                                                                          fontSize:
                                                                              8,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          color:
                                                                              Colors.white)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        Positioned(
                                                          bottom: 10,
                                                          right: 0,
                                                          left: 64,
                                                          child: CircleAvatar(
                                                            radius: 7,
                                                            backgroundColor:
                                                                Colors.white,
                                                            child: CircleAvatar(
                                                              radius: 5,
                                                              backgroundColor:
                                                                  resAstroList[i]
                                                                              .isAstrologerOnline ==
                                                                          1
                                                                      ? Colors
                                                                          .green
                                                                      : Colors
                                                                          .grey, // Green for online, grey for offline
                                                              child: Tooltip(
                                                                message: resAstroList[i]
                                                                            .isAstrologerOnline ==
                                                                        1
                                                                    ? 'Online'
                                                                        .tr()
                                                                    : 'Offline'
                                                                        .tr(),
                                                                child:
                                                                    Container(
                                                                  height: 20,
                                                                  width: 20,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 4,
                                                    ),
                                                    Text(
                                                        TFormatter
                                                            .capitalizeSentence(
                                                                resAstroList[i]
                                                                    .name),
                                                        textAlign:
                                                            TextAlign.center,
                                                        style:
                                                            GoogleFonts.poppins(
                                                                fontSize: 15,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                        maxLines: 2,
                                                        overflow:
                                                            TextOverflow.fade),
                                                    SizedBox(
                                                      height: 4,
                                                    ),
                                                    Text(
                                                        "₹ ${resAstroList[i].astrologer.chatPrice == 0 ? resAstroList[i].astrologer.chatDiscountPrice == 0 ? '' : resAstroList[i].astrologer.chatDiscountPrice : resAstroList[i].astrologer.chatPrice} /" +
                                                            "min",
                                                        style:
                                                            GoogleFonts.poppins(
                                                                fontSize: 14,
                                                                color:
                                                                    Colors.grey[
                                                                        800])),
                                                    SizedBox(
                                                      height: 4,
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        print("Chat Intake");
                                                        if (resAstroList[i]
                                                                .astrologer
                                                                .nextonlines
                                                                .chatNODate
                                                                .isEmpty &&
                                                            resAstroList[i]
                                                                .astrologer
                                                                .nextonlines
                                                                .chatNOTime
                                                                .isEmpty) {
                                                          if (userId > 0 &&
                                                              customerName !=
                                                                  "") {
                                                            {
                                                              var data =
                                                                  resAstroList[
                                                                          i]
                                                                      .astrologer;

                                                              int realPrice = (data.chatDiscountPrice <=
                                                                          data
                                                                              .chatPrice) &&
                                                                      data.chatDiscountPrice !=
                                                                          0
                                                                  ? data
                                                                      .chatDiscountPrice
                                                                  : data
                                                                      .chatPrice;
                                                              TLoggerHelper.debug(
                                                                  "Real Price:--  " +
                                                                      realPrice
                                                                          .toString() +
                                                                      " with Chat price:- ${data.chatPrice} and with Chat Discount Price:- ${data.chatDiscountPrice}");
                                                              if ((double.parse(
                                                                          walletAmount) >=
                                                                      realPrice *
                                                                          5) &&
                                                                  walletAmount !=
                                                                      "0") {
                                                                showDialog(
                                                                    barrierDismissible:
                                                                        false,
                                                                    context:
                                                                        context,
                                                                    builder:
                                                                        (BuildContext
                                                                            context) {
                                                                      return Dialog(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(20.0)),
                                                                        child:
                                                                            Container(
                                                                          height:
                                                                              (MediaQuery.of(context).size.height / 2) - 50,
                                                                          child:
                                                                              Padding(
                                                                            padding:
                                                                                const EdgeInsets.all(5.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: Alignment.topRight,
                                                                                  child: IconButton(
                                                                                    onPressed: () {
                                                                                      Navigator.pop(context);
                                                                                    },
                                                                                    icon: Icon(
                                                                                      Icons.close,
                                                                                      size: 30,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                Card(
                                                                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                                                                  child: CircleAvatar(
                                                                                    radius: 70,
                                                                                    backgroundImage: CachedNetworkImageProvider("${resAstroList[i].avatar}"),
                                                                                  ),
                                                                                  shape: CircleBorder(side: BorderSide(color: AppColor.appColor, width: 2)),
                                                                                ),
                                                                                Text(
                                                                                  TFormatter.capitalizeSentence(resAstroList[i].name),
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
                                                                                ),
                                                                                SizedBox(height: 10),
                                                                                Text(
                                                                                  "This offer at".tr() + " ${Constants.currency} ${resAstroList[i].astrologer.chatDiscountPrice == 0 ? resAstroList[i].astrologer.chatPrice : resAstroList[i].astrologer.chatDiscountPrice}/" + "min for 5 mins only. Astrologer will try to answer at least one question.".tr(),
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                                                                                  textAlign: TextAlign.center,
                                                                                ),
                                                                                SizedBox(height: 10),
                                                                                TextButton(
                                                                                  onPressed: () {
                                                                                    Navigator.pop(context);

                                                                                    print("resultChatlist");
                                                                                    sendAndGetResult(resAstroList[i].name.toString(), resAstroList[i].id, resAstroList[i].avatar.toString(), context, resAstroList[i].astrologer.userId.toString(), (resAstroList[i].astrologer.freeChat == 1 && resAstroList[i].isFreeChatCompleted == 0) ? 1 : 0);
                                                                                  },
                                                                                  style: TextButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)), backgroundColor: AppColor.appColor),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.symmetric(horizontal: 20),
                                                                                    child: Text(
                                                                                      "Start Chat".tr() + " @ ${Constants.currency} ${resAstroList[i].astrologer.chatDiscountPrice == 0 ? resAstroList[i].astrologer.chatPrice : resAstroList[i].astrologer.chatDiscountPrice}/" + "min",
                                                                                      style: GoogleFonts.poppins(color: Colors.black),
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      );
                                                                    });
                                                              } else if (resAstroList[
                                                                              i]
                                                                          .isFreeChatCompleted ==
                                                                      0 &&
                                                                  resAstroList[
                                                                              i]
                                                                          .astrologer
                                                                          .freeChat ==
                                                                      1) {
                                                                showDialog(
                                                                    barrierDismissible:
                                                                        false,
                                                                    context:
                                                                        context,
                                                                    builder:
                                                                        (BuildContext
                                                                            context) {
                                                                      return Dialog(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(20.0)),
                                                                        child:
                                                                            Container(
                                                                          height:
                                                                              (MediaQuery.of(context).size.height / 2) - 50,
                                                                          child:
                                                                              Padding(
                                                                            padding:
                                                                                const EdgeInsets.all(5.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: Alignment.topRight,
                                                                                  child: IconButton(
                                                                                    onPressed: () {
                                                                                      Navigator.pop(context);
                                                                                    },
                                                                                    icon: Icon(
                                                                                      Icons.close,
                                                                                      size: 30,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                Card(
                                                                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                                                                  child: CircleAvatar(
                                                                                    radius: 70,
                                                                                    backgroundImage: CachedNetworkImageProvider("${resAstroList[i].avatar}"),
                                                                                  ),
                                                                                  shape: CircleBorder(side: BorderSide(color: AppColor.appColor, width: 2)),
                                                                                ),
                                                                                Text(
                                                                                  "${resAstroList[i].name}",
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
                                                                                ),
                                                                                SizedBox(height: 10),
                                                                                Text(
                                                                                  "This offer at".tr() + " ${Constants.currency} ${resAstroList[i].astrologer.chatDiscountPrice == 0 ? resAstroList[i].astrologer.chatPrice : resAstroList[i].astrologer.chatDiscountPrice}/" + "min for 5 mins only. Astrologer will try to answer at least one question.".tr(),
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                                                                                  textAlign: TextAlign.center,
                                                                                ),
                                                                                SizedBox(height: 10),
                                                                                TextButton(
                                                                                  onPressed: () {
                                                                                    Navigator.pop(context);

                                                                                    print("resultChatlist");
                                                                                    sendAndGetResult(resAstroList[i].name.toString(), resAstroList[i].id, resAstroList[i].avatar.toString(), context, resAstroList[i].astrologer.userId.toString(), (resAstroList[i].astrologer.freeChat == 1 && resAstroList[i].isFreeChatCompleted == 0) ? 1 : 0);
                                                                                  },
                                                                                  style: TextButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)), backgroundColor: AppColor.appColor),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.symmetric(horizontal: 20),
                                                                                    child: Text(
                                                                                      "Start Chat".tr() + " @ ${Constants.currency} ${resAstroList[i].astrologer.chatDiscountPrice == 0 ? resAstroList[i].astrologer.chatPrice : resAstroList[i].astrologer.chatDiscountPrice}/" + "min",
                                                                                      style: GoogleFonts.poppins(color: Colors.black),
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      );
                                                                    });
                                                              } else {
                                                                // showSnackBarWidget(
                                                                //     context,
                                                                //     "Please recharge your wallet");
                                                                _rechargeDialog(
                                                                        context,
                                                                        resAstroList[i]
                                                                            .name
                                                                            .toString(),
                                                                        resAstroList[i]
                                                                            .id
                                                                            .toString(),
                                                                        resAstroList[i]
                                                                            .astrologer
                                                                            .chatPrice)
                                                                    .whenComplete(
                                                                  () {
                                                                    getWalletAmount();
                                                                  },
                                                                );
                                                              }
                                                            }
                                                          } else if (customerID >
                                                                  0 &&
                                                              customerName
                                                                  .isEmpty) {
                                                            Navigator.of(
                                                                    context)
                                                                .push(SlideRightRoute(
                                                                    page:
                                                                        ProfileScreen()));
                                                          } else {
                                                            context.pushNamed(
                                                                RouteConstants
                                                                    .authScreen);
                                                          }
                                                          // Navigator.of(context)
                                                          //     .push(
                                                          //     SlideRightRoute(
                                                          //         page: ChatIntakeForm(
                                                          //             resAstroList[i]
                                                          //                 .name
                                                          //                 .toString(),
                                                          //             userId
                                                          //                 .toString(),
                                                          //             resAstroList[i]
                                                          //                 .id
                                                          //                 .toString(),
                                                          //             "chat")));
                                                        } else {
                                                          nextOnlineDialog(
                                                              astrologerName:
                                                                  resAstroList[
                                                                          i]
                                                                      .name,
                                                              astrologerImage:
                                                                  resAstroList[
                                                                          i]
                                                                      .avatar,
                                                              nextOnlineTime:
                                                                  resAstroList[
                                                                          i]
                                                                      .astrologer
                                                                      .nextonlines
                                                                      .chatNOTime,
                                                              nextOnlineDate:
                                                                  resAstroList[
                                                                          i]
                                                                      .astrologer
                                                                      .nextonlines
                                                                      .chatNODate);
                                                        }
                                                      },
                                                      child: Card(
                                                        color: Colors.green,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          10.0),
                                                                ),
                                                                side: BorderSide(
                                                                    color: Colors
                                                                        .green)),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 25,
                                                                  right: 25,
                                                                  bottom: 3,
                                                                  top: 2),
                                                          child: Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            children: [
                                                              Icon(
                                                                Icons.message,
                                                                size: 12,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              SizedBox(
                                                                  width: 4),
                                                              Text("Chat".tr(),
                                                                  style: GoogleFonts.poppins(
                                                                      fontSize:
                                                                          14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      color: Colors
                                                                          .white)),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 8,
                                                    ),
                                                  ],
                                                ),
                                                // if (resAstroList[i].label != 0)
                                                //   Positioned(
                                                //     left:
                                                //         -25, // Adjust the value to avoid overlapping
                                                //     top:
                                                //         15, // Adjust the value for proper positioning

                                                //     child: Transform.rotate(
                                                //       angle: -math.pi / 3.5,
                                                //       child: Container(
                                                //         decoration: BoxDecoration(
                                                //           gradient:
                                                //               LinearGradient(
                                                //             colors: [
                                                //               Color(
                                                //                   0xFF3D3D3D), // Dark gray (closer to black)
                                                //               Color(
                                                //                   0xFF1C1C1C), // Almost black
                                                //               Color(
                                                //                   0xFF000000), // Pure black
                                                //               Color(
                                                //                   0xFF434343), // Slightly lighter for a metallic effect
                                                //             ],
                                                //             begin: Alignment
                                                //                 .topCenter,
                                                //             end: Alignment
                                                //                 .bottomCenter,
                                                //           ),
                                                //         ),
                                                //         width: 100,
                                                //         alignment:
                                                //             Alignment.center,
                                                //         padding:
                                                //             EdgeInsets.symmetric(
                                                //           vertical: 2,
                                                //         ),
                                                //         child: Text(
                                                //           resAstroList[i]
                                                //               .astrologer
                                                //               .labelText,
                                                //           textAlign:
                                                //               TextAlign.center,
                                                //           style:
                                                //               GoogleFonts.poppins(
                                                //             color: AppColor
                                                //                 .whiteColor,
                                                //             fontSize: 10,
                                                //           ),
                                                //         ),
                                                //       ),
                                                //     ),
                                                //   ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                                SizedBox(height: 10),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          /*Latest Blog Astrologers*/
                          Container(
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 10),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Latest From Blog".tr(),
                                        style: GoogleFonts.poppins(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      ListenableProvider(
                                                        create: (context) =>
                                                            ListOfApisProvider(),
                                                        child:
                                                            LatestBlogViewAllScreen(),
                                                      )));
                                        },
                                        child: Text(
                                          "View All".tr(),
                                          style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(height: 10),
                                if (blogData != null)
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        .275,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemCount: blogData!.data.length > 5
                                          ? 5
                                          : blogData!.data.length,
                                      itemBuilder:
                                          (BuildContext context, int bIndex) {
                                        return InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (_) =>
                                                        BlogDetailsScreen(
                                                          bloggerName:
                                                              "${blogData!.data[bIndex].bloggerName.toString()}",
                                                          date:
                                                              "${blogData!.data[bIndex].createTime.toString()}",
                                                          id: "${blogData!.data[bIndex].id.toString()}",
                                                          img:
                                                              "${blogData!.data[bIndex].image.toString()}",
                                                          title:
                                                              "${blogData!.data[bIndex].title.toString()}",
                                                          description:
                                                              '${blogData!.data[bIndex].longDescription.toString()}',
                                                        )));
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(left: 10),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .7,
                                            child: Card(
                                              color: Colors.white,
                                              elevation: 2,
                                              clipBehavior:
                                                  Clip.antiAliasWithSaveLayer,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10.0))),
                                              child: Stack(
                                                children: [
                                                  Column(
                                                    children: [
                                                      Container(
                                                        height: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .height *
                                                            .15,
                                                        decoration:
                                                            BoxDecoration(
                                                                image:
                                                                    DecorationImage(
                                                          image: CachedNetworkImageProvider(
                                                              blogData!
                                                                  .data[bIndex]
                                                                  .image
                                                                  .toString()),
                                                          fit: BoxFit.cover,
                                                        )),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Text(
                                                          blogData!.data[bIndex]
                                                              .title
                                                              .toString(),
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                          maxLines: 2,
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                                horizontal:
                                                                    8.0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Text(
                                                              TFormatter.capitalize(
                                                                  blogData!
                                                                      .data[
                                                                          bIndex]
                                                                      .bloggerName
                                                                      .toString()),
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      fontSize:
                                                                          12,
                                                                      color: Colors
                                                                          .grey),
                                                              maxLines: 2,
                                                            ),
                                                            Text(
                                                              "${DateFormat("MMM dd yyyy").format(DateTime.parse(blogData!.data[bIndex].createTime.toString()))}",
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      fontSize:
                                                                          12,
                                                                      color: Colors
                                                                          .grey),
                                                              maxLines: 2,
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: SizedBox(
                                                      height: 30,
                                                      child: Card(
                                                        color: Colors.white,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                                  left: 8.0,
                                                                  right: 8.0),
                                                          child: Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons
                                                                    .remove_red_eye,
                                                                size: 15,
                                                              ),
                                                              SizedBox(
                                                                width: 5,
                                                              ),
                                                              Text(
                                                                "${blogData!.data[bIndex].view.toString()}",
                                                                style:
                                                                    GoogleFonts
                                                                        .poppins(
                                                                  fontSize: 15,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                SizedBox(height: 10),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          /*Shop at AstroMall*/
                          Container(
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 10),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Shop at Jyotish Mall".tr(),
                                        style: GoogleFonts.poppins(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          // Navigator.of(context)
                                          //     .push(SlideRightRoute(
                                          //         page: ListenableProvider(
                                          //   create: (_) =>
                                          //       ListOfApisProvider(),
                                          //   child: AstroMallViewAllScreen(
                                          //     onChangedItem: (int id) {
                                          //       // setState(() {
                                          //       widget.onChangedItem(id);
                                          //       // });
                                          //     },
                                          //   ),
                                          // )));
                                          Navigator.of(context)
                                              .push(SlideRightRoute(
                                                  page: ListenableProvider(
                                            create: (context) =>
                                                ListOfApisProvider(),
                                            child: AstroMallAllItemScreen(
                                              onChangedItem: (value) {
                                                setState(() {
                                                  widget.onChangedItem(value);
                                                });
                                              },
                                            ),
                                          )));
                                        },
                                        child: Text(
                                          "View All".tr(),
                                          style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(height: 10),
                                if (mallData != null)
                                  Container(
                                    width: double.infinity,
                                    height: 200,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemCount: mallData!.data.length > 5
                                          ? 5
                                          : mallData!.data.length,
                                      itemBuilder: (BuildContext context,
                                          int mallIndex) {
                                        return InkWell(
                                          onTap: () {
                                            mallData!.data[mallIndex].name ==
                                                    "Pooja"
                                                ? widget.onChangedItem(4)
                                                : Navigator.of(context).push(
                                                    SlideRightRoute(
                                                        page:
                                                            ListenableProvider(
                                                                create: (context) =>
                                                                    ListOfApisProvider(),
                                                                child:
                                                                    MallItemsScreen(
                                                                  categoryId:
                                                                      "${mallData!.data[mallIndex].id}",
                                                                  suggestedType:
                                                                      mallData!
                                                                          .data[
                                                                              mallIndex]
                                                                          .suggestedType,
                                                                  productImg:
                                                                      '${mallData!.data[mallIndex].image}',
                                                                  productName:
                                                                      '${mallData!.data[mallIndex].name}',
                                                                ))));
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 200,
                                            width: 200,
                                            child: Card(
                                              elevation: 2,
                                              clipBehavior:
                                                  Clip.antiAliasWithSaveLayer,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10.0))),
                                              child: Stack(
                                                children: <Widget>[
                                                  Positioned(
                                                    child: CachedNetworkImage(
                                                      fit: BoxFit.cover,
                                                      imageUrl:
                                                          "${mallData!.data[mallIndex].image}",
                                                    ),
                                                    right: 0,
                                                    left: 0,
                                                    top: 0,
                                                    bottom: 0,
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 120),
                                                    width: double.infinity,
                                                    height: 100,
                                                    child: ClipRect(
                                                      child: BackdropFilter(
                                                        filter:
                                                            ImageFilter.blur(
                                                                sigmaX: 1.0,
                                                                sigmaY: 1.0),
                                                        child: Container(
                                                          decoration: BoxDecoration(
                                                              gradient: LinearGradient(
                                                                  begin: Alignment
                                                                      .bottomCenter,
                                                                  end: Alignment.topCenter,
                                                                  colors: [
                                                                Colors.black54
                                                                    .withOpacity(
                                                                        1),
                                                                Colors.white70
                                                                    .withOpacity(
                                                                        00)
                                                              ])),
                                                          height: 40,
                                                          child: Center(
                                                            child: Text(
                                                              "${mallData!.data[mallIndex].name}",
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      color: Colors
                                                                          .white),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                SizedBox(height: 10),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          /*********************** Celebrity Section *********************/
                          if (celebrityList != null)
                            Container(
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(height: 10),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      "Our Celebrity Customer".tr(),
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  // SingleChildScrollView(
                                  //   scrollDirection: Axis.horizontal,
                                  //   child: Row(
                                  //       children: List.generate(
                                  //           celebrityList!.data.length, (index) {
                                  //     return InkWell(
                                  //       onTap: () {
                                  //         Navigator.of(context)
                                  //             .push(MaterialPageRoute(
                                  //                 builder: (context) => YoutubeScreen(
                                  //                       youtubeUrlId: TFormatter
                                  //                           .getYouTubeVideoId(
                                  //                               celebrityList!
                                  //                                   .data[index]
                                  //                                   .url!)!,
                                  //                     )));
                                  //       },
                                  //       child: Container(
                                  //         height: 200,
                                  //         width:
                                  //             MediaQuery.of(context).size.width * .7,
                                  //         margin: EdgeInsets.symmetric(
                                  //             horizontal: 12, vertical: 8),
                                  //         decoration: BoxDecoration(
                                  //             borderRadius:
                                  //                 BorderRadius.circular(12)),
                                  //         child: Stack(
                                  //             fit: StackFit.expand,
                                  //             children: <Widget>[
                                  //               ClipRRect(
                                  //                 borderRadius:
                                  //                     BorderRadius.circular(6),
                                  //                 child: CachedNetworkImage(
                                  //                     imageUrl:
                                  //                         "https://img.youtube.com/vi/${TFormatter.getYouTubeVideoId(celebrityList!.data[index].url!)}/0.jpg",
                                  //                     fit: BoxFit.cover),
                                  //               ),
                                  //               Container(
                                  //                   margin: EdgeInsets.only(top: 150),
                                  //                   width: double.infinity,
                                  //                   child: ClipRect(
                                  //                       child: Container(
                                  //                           height: 40,
                                  //                           child: Center(
                                  //                               child: Text(
                                  //                             "${celebrityList!.data[index].title}",
                                  //                             maxLines: 2,
                                  //                             overflow: TextOverflow
                                  //                                 .ellipsis,
                                  //                             style:
                                  //                                 GoogleFonts.poppins(
                                  //                                     fontWeight:
                                  //                                         FontWeight
                                  //                                             .bold,
                                  //                                     fontSize: 20,
                                  //                                     color: Colors
                                  //                                         .white),
                                  //                           ))))),
                                  //               Center(
                                  //                   child: Image.asset(
                                  //                 "asset/images/Youtube.png",
                                  //                 height: 50,
                                  //               ))
                                  //             ]),
                                  //       ),
                                  //     );
                                  //   })),
                                  // ),
                                  ListView.builder(
                                    // scrollDirection: Axis.vertical,
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: celebrityList!.data.length,
                                    itemBuilder:
                                        (BuildContext context, int celeIndex) {
                                      return InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      YoutubeScreen(
                                                        youtubeUrlId: TFormatter
                                                            .getYouTubeVideoId(
                                                                celebrityList!
                                                                    .data[
                                                                        celeIndex]
                                                                    .url!)!,
                                                      )));
                                        },
                                        child: Container(
                                          height: 200,
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 12, vertical: 8),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12)),
                                          child: Stack(
                                              fit: StackFit.expand,
                                              children: <Widget>[
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  child: CachedNetworkImage(
                                                      imageUrl:
                                                          "https://img.youtube.com/vi/${TFormatter.getYouTubeVideoId(celebrityList!.data[celeIndex].url!)}/0.jpg",
                                                      fit: BoxFit.cover),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        top: 150),
                                                    width: double.infinity,
                                                    child: ClipRect(
                                                        child: Container(
                                                            height: 40,
                                                            child: Center(
                                                                child: Text(
                                                              "${celebrityList!.data[celeIndex].title}",
                                                              maxLines: 2,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              style: GoogleFonts.poppins(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  fontSize: 18,
                                                                  color: Colors
                                                                      .white),
                                                            ))))),
                                                Center(
                                                    child: Image.asset(
                                                  "asset/images/Youtube.png",
                                                  height: 50,
                                                ))
                                              ]),
                                        ),
                                      );
                                    },
                                  ),
                                  SizedBox(height: 10),
                                ],
                              ),
                            ),
                          /*********************** Secur Section *******************/
                          SizedBox(height: 10),
                          // Container(
                          //   color: Colors.white,
                          //   child: Column(
                          //     children: <Widget>[
                          //       SizedBox(height: 10),
                          //       Padding(
                          //         padding: EdgeInsets.symmetric(horizontal: 20),
                          //         child: Row(
                          //           mainAxisAlignment:
                          //               MainAxisAlignment.spaceBetween,
                          //           children: [
                          //             Text(
                          //               "astrotalk_in_news,
                          //               style: GoogleFonts.poppins(
                          //                   fontSize: 16,
                          //                   fontWeight: FontWeight.w700),
                          //             ),
                          //             InkWell(
                          //               onTap: () {
                          //                 Navigator.push(
                          //                     context,
                          //                     MaterialPageRoute(
                          //                         builder: (_) =>
                          //                             AstrotalkNewsViewAllScreen()));
                          //               },
                          //               child: Text(
                          //                 "view_all,
                          //                 style: GoogleFonts.poppins(
                          //                     fontSize: 14,
                          //                     fontWeight: FontWeight.w500),
                          //               ),
                          //             )
                          //           ],
                          //         ),
                          //       ),
                          //       SizedBox(height: 10),
                          //       Container(
                          //         width: double.infinity,
                          //         height: 200,
                          //         child: ListView.builder(
                          //           scrollDirection: Axis.horizontal,
                          //           shrinkWrap: true,
                          //           itemCount: 10,
                          //           itemBuilder: (BuildContext context, int i) {
                          //             return Container(
                          //               margin: EdgeInsets.only(left: 10),
                          //               height: 150,
                          //               width: 250,
                          //               child: Card(
                          //                 elevation: 2,
                          //                 clipBehavior:
                          //                     Clip.antiAliasWithSaveLayer,
                          //                 shape: RoundedRectangleBorder(
                          //                     borderRadius: BorderRadius.all(
                          //                         Radius.circular(10.0))),
                          //                 child: Stack(
                          //                   children: [
                          //                     Positioned(
                          //                       child: Image.network(
                          //                         fit: BoxFit.cover,
                          //                         'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=1760&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                          //                       ),
                          //                       right: 0,
                          //                       left: 0,
                          //                       top: 0,
                          //                       bottom: 0,
                          //                     ),
                          //                     Container(
                          //                       margin:
                          //                           EdgeInsets.only(top: 125),
                          //                       color: Colors.white,
                          //                       height: 70,
                          //                       width: 250,
                          //                       child: Column(
                          //                         children: <Widget>[
                          //                           Text("News Title",
                          //                               maxLines: 2,
                          //                               overflow: TextOverflow
                          //                                   .ellipsis,
                          //                               style: GoogleFonts.poppins(
                          //                                   fontSize: 19,
                          //                                   color: Colors.black,
                          //                                   fontWeight:
                          //                                       FontWeight
                          //                                           .bold)),
                          //                           Padding(
                          //                             padding:
                          //                                 const EdgeInsets.only(
                          //                                     left: 8.0,
                          //                                     right: 8.0),
                          //                             child: Row(
                          //                               mainAxisAlignment:
                          //                                   MainAxisAlignment
                          //                                       .spaceBetween,
                          //                               children: <Widget>[
                          //                                 Text(
                          //                                   "Writer Name",
                          //                                   maxLines: 1,
                          //                                   overflow:
                          //                                       TextOverflow
                          //                                           .ellipsis,
                          //                                   style: GoogleFonts.poppins(
                          //                                       color: Colors
                          //                                           .black38),
                          //                                   // textAlign: TextAlign.center,
                          //                                 ),
                          //                                 Text(
                          //                                   "Date",
                          //                                   maxLines: 1,
                          //                                   overflow:
                          //                                       TextOverflow
                          //                                           .ellipsis,
                          //                                   style: GoogleFonts.poppins(
                          //                                       color: Colors
                          //                                           .black38),
                          //                                   // textAlign: TextAlign.center,
                          //                                 ),
                          //                               ],
                          //                             ),
                          //                           )
                          //                         ],
                          //                       ),
                          //                     ),
                          //                   ],
                          //                 ),
                          //               ),
                          //             );
                          //           },
                          //         ),
                          //       ),
                          //       SizedBox(height: 10),
                          //     ],
                          //   ),
                          // ),
                          SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                          child: secureSectionWidget(
                                              "Private & Confidential".tr(),
                                              "asset/images/Secure.png")),
                                      Expanded(
                                          child: secureSectionWidget(
                                              "Verified Astrologers".tr(),
                                              "asset/images/Verify.png")),
                                      Expanded(
                                          child: secureSectionWidget(
                                              "Secure Payments".tr(),
                                              "asset/images/money.png"))
                                    ],
                                  ),
                                ),
                                SizedBox(height: 10),
                                SizedBox(height: 100),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                    // Positioned(
                    //   right: 8,
                    //   left: 8,
                    //   bottom: 20,
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: <Widget>[
                    //       Expanded(
                    //           child: GestureDetector(
                    //         onTap: () {
                    //           widget.onChangedItem(1);
                    //         },
                    //         child: Container(
                    //           padding: EdgeInsets.symmetric(
                    //               horizontal: 8, vertical: 12),
                    //           decoration: BoxDecoration(
                    //               color: AppColor.appColor,
                    //               borderRadius: BorderRadius.circular(30)),
                    //           child: Row(
                    //             mainAxisAlignment: MainAxisAlignment.center,
                    //             crossAxisAlignment: CrossAxisAlignment.center,
                    //             children: [
                    //               Image.asset("asset/images/chat.png",
                    //                   height: 15, width: 15),
                    //               SizedBox(width: 5),
                    //               Expanded(
                    //                   child: Text(
                    //                 "Chat with Astrologer".tr(),
                    //                 textAlign: TextAlign.center,
                    //                 style: GoogleFonts.poppins(
                    //                     fontWeight: FontWeight.w500),
                    //               ))
                    //             ],
                    //           ),
                    //         ),
                    //       )),
                    //       SizedBox(
                    //         width: 8,
                    //       ),
                    //       Expanded(
                    //           child: GestureDetector(
                    //         onTap: () {
                    //           widget.onChangedItem(3);
                    //         },
                    //         child: Container(
                    //           padding: EdgeInsets.symmetric(
                    //               horizontal: 8, vertical: 12),
                    //           decoration: BoxDecoration(
                    //               color: AppColor.appColor,
                    //               borderRadius: BorderRadius.circular(30)),
                    //           child: Row(
                    //             mainAxisAlignment: MainAxisAlignment.center,
                    //             crossAxisAlignment: CrossAxisAlignment.center,
                    //             children: [
                    //               Image.asset("asset/images/call.png",
                    //                   height: 15, width: 15),
                    //               SizedBox(width: 5),
                    //               Expanded(
                    //                   child: Text(
                    //                 "Call with Astrologer".tr(),
                    //                 textAlign: TextAlign.center,
                    //                 style: GoogleFonts.poppins(
                    //                     fontWeight: FontWeight.w500),
                    //               ))
                    //             ],
                    //           ),
                    //         ),
                    //       )),
                    //     ],
                    //   ),
                    // )
                  ],
                ),
        ),
        // bottomNavigationBar: FloatingActionButton(onPressed: () async{
        //   int i = 0;
        //   while(i<=50){

        //   await notificationHandler.showNormalNotification();
        //   i++;
        //   }
        //   },child: Icon(Icons.notification_add),),
      ),
    );
  }

  // Future<void> getOrderList() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   var userId = prefs.getInt(Constants.userID).toString();

  //   client = DioClient();
  //   Dio dio = await client!.getClient();

  //   String api = Constants.order;

  //   var params = jsonEncode({'user_id': userId});
  //   TLoggerHelper.debug(params);
  //   OrderHistoryModel? orderHistoryModel =
  //       await client?.orderHistory(dio, api, params);
  //   if (!mounted) return;
  //   Navigator.of(context).pop();

  //   if (orderHistoryModel!.status == true) {
  //     orderHistoryList
  //         .addAll(orderHistoryModel.orderHistory as Iterable<OrderHistory>);

  //     isOrderHistoryListLoading = false;
  //   }
  // }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.getInt("id") != null) {
      userId = prefs.getInt("id")!;
      customerID = userId;
      profileImage = prefs.getString(Constants.profileImage)!;
      if (prefs.getString(Constants.fullName) != null) {
        customerName = prefs.getString(Constants.fullName)!;
      }
    }
  }

  Future<void> _liveAstrologerList() async {
    final httpClient = HttpClient();
    try {
      var response = await httpClient.getData(UrlConstants.astrologerLive);

      if (response!["status"] == true) {
        liveAstrolgerlist = response["is_online"];
        print("liveAstrolgerlist---> $liveAstrolgerlist");
      }
    } catch (e) {
      print('Error fetching posts: $e');
    }
  }

  /********************* Recharge Dialog ************************/
  Future<void> _rechargeDialog(BuildContext context, String name,
      String astroID, int amountPerMinute) async {
    int? amID = 0;
    String? amount = "";

    // context
    //     .read<ListOfApisProvider>()
    //     .getRechargeAmountProvider(context: context);
    await showModalBottomSheet<void>(
      context: context,
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: Consumer<ListOfApisProvider>(builder: (context, provider, _) {
            return FutureBuilder(
                future: provider.getRechargeAmountPProvider(context: context),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(child: CustomCircularProgressIndicator());
                  } else if (snapshot.hasData) {
                    RechargeAmountModel rechargeAmountModel = snapshot.data;
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topRight,
                              child: CloseButton()),
                          Text(
                            "Minimum balance of 5 minutes".tr() +
                                " (INR ${amountPerMinute * 5}.0) " +
                                "is required to start chat with".tr() +
                                " ($name)",
                            textAlign: TextAlign.start,
                            style: GoogleFonts.poppins(
                                fontSize: 14, color: Colors.redAccent),
                          ),
                          SizedBox(height: 10),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Recharge Now'.tr(),
                              style: GoogleFonts.poppins(fontSize: 17),
                            ),
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.light_mode_rounded,
                                color: AppColor.appColor,
                              ),
                              Text("Tip: 90% user recharge for 10 mins or more"
                                  .tr())
                            ],
                          ),
                          SizedBox(height: 10),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: rechargeAmountModel.recharge!.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4,
                                        childAspectRatio: 6 / 2,
                                        mainAxisSpacing: 10,
                                        crossAxisSpacing: 10),
                                itemBuilder: (context, index) {
                                  if (index == 0) {
                                    price = rechargeAmountModel
                                        .recharge![index].price!;
                                    print(rechargeAmountModel
                                        .recharge![index].price);
                                    amID =
                                        rechargeAmountModel.recharge![index].id;
                                    amount = rechargeAmountModel
                                        .recharge![index].price;
                                  }
                                  return InkWell(
                                    onTap: () {
                                      if (customerID == 0) {
                                        Navigator.of(context).push(
                                            SlideRightRoute(
                                                page: LoginScreen()));
                                      } else {
                                        Navigator.of(context)
                                            .push(
                                              SlideRightRoute(
                                                page: ListenableProvider(
                                                  create: (context) =>
                                                      ListOfApisProvider(),
                                                  child:
                                                      PaymentInformationScreen(
                                                    userInfoId: "",
                                                    astroName: customerName,
                                                    title:
                                                        "Recharge Amount".tr(),
                                                    price: rechargeAmountModel
                                                        .recharge![index]
                                                        .price!,
                                                    amountID:
                                                        rechargeAmountModel
                                                            .recharge![index]
                                                            .id!,
                                                    from: 'wallet',
                                                    astroId: astroID,
                                                  ),
                                                ),
                                              ),
                                            )
                                            .then((value) =>
                                                Navigator.maybePop(context));
                                      }
                                      price = rechargeAmountModel
                                          .recharge![index].price!;
                                      print(rechargeAmountModel
                                          .recharge![index].price);
                                      amID = rechargeAmountModel
                                          .recharge![index].id;
                                      amount = rechargeAmountModel
                                          .recharge![index].price;
                                    },
                                    child: Container(
                                        height: 25,
                                        width: 90,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            border: Border.all(
                                                color: index == 0
                                                    ? AppColor.appColor
                                                    : AppColor.borderColor)),
                                        child: Stack(children: <Widget>[
                                          Positioned(
                                              top: 7,
                                              right: 50.5,
                                              child: Transform.rotate(
                                                  angle: -math.pi / 3.5,
                                                  child: Container(
                                                      height: 15,
                                                      width: 50,
                                                      decoration: BoxDecoration(
                                                          color: Colors.green,
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius.circular(
                                                                      5))),
                                                      child: Center(
                                                          child: Text(
                                                              "${rechargeAmountModel.recharge![index].discountPercentage}% Extra",
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      fontSize:
                                                                          8)))))),
                                          Center(
                                              child: Text(
                                                  "${rechargeAmountModel.recharge![index].price}"))
                                        ])),
                                  );
                                }),
                          ),
                          SizedBox(height: 20),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              style: TextButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  backgroundColor: AppColor.appColor,
                                  textStyle:
                                      Theme.of(context).textTheme.labelLarge),
                              child: Text(
                                'Proceed to Pay'.tr(),
                                style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              ),
                              onPressed: () {
                                if (customerID == 0) {
                                  Navigator.of(context).push(
                                      SlideRightRoute(page: LoginScreen()));
                                } else {
                                  Navigator.of(context)
                                      .push(
                                        SlideRightRoute(
                                          page: ListenableProvider(
                                            create: (context) =>
                                                ListOfApisProvider(),
                                            child: PaymentInformationScreen(
                                              userInfoId: "",
                                              astroName: customerName,
                                              title: "Recharge Amount".tr(),
                                              price: price,
                                              amountID: amID!,
                                              from: 'wallet',
                                              astroId: astroID,
                                            ),
                                          ),
                                        ),
                                      )
                                      .then((value) =>
                                          Navigator.maybePop(context));
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return SizedBox();
                  }
                });
          }),
        );
      },
    );
  }

  showCancelBookingDialog(String id) {
    showModalBottomSheet<void>(
      backgroundColor: Colors.white,
      context: context,
      isDismissible: false,
      enableDrag: false,
      builder: (BuildContext context) {
        return PopScope(
            canPop: false,
            onPopInvoked: (didPop) {
              // logic
            },
            child: SizedBox(
              height: 400,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Icon(
                              Icons.close,
                              size: 30,
                            ),
                          )),
                      Text(
                        'Leave waitlist of astrologer',
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                      SizedBox(height: 5),
                      Text(
                        'Instead of leaving the waitlist, you may also join the waitlist of these trending astrologers',
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 10),
                      Container(
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              height: 180,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: resAstroList != null
                                    ? resAstroList.length
                                    : 0,
                                itemBuilder: (BuildContext context, int i) {
                                  return GestureDetector(
                                    onTap: () {
                                      context.pushNamed(
                                        RouteConstants.astrologerProfile,
                                        queryParameters: {
                                          'astrologerId':
                                              resAstroList[i].id.toString(),
                                          'type': 'chat',
                                          'isAstrologerOnline': resAstroList[i]
                                              .isAstrologerOnline
                                              .toString()
                                        },
                                      );
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      width: 120,
                                      child: Card(
                                        color: Colors.white,
                                        elevation: 1,
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(6.0))),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Stack(
                                              children: [
                                                Center(
                                                  child: Card(
                                                    clipBehavior: Clip
                                                        .antiAliasWithSaveLayer,
                                                    child: CircleAvatar(
                                                      radius: 40,
                                                      backgroundImage:
                                                          CachedNetworkImageProvider(
                                                              resAstroList[i]
                                                                  .avatar),
                                                    ),
                                                    shape: CircleBorder(
                                                      side: BorderSide(
                                                          color:
                                                              AppColor.appColor,
                                                          width: 2),
                                                    ),
                                                  ),
                                                ),
                                                if (resAstroList[i].label != 0)
                                                  Positioned(
                                                    bottom: -4,
                                                    left: 0,
                                                    right: 0,
                                                    child: Center(
                                                      child: Card(
                                                        color: Colors.amber,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          10.0),
                                                                ),
                                                                side: BorderSide(
                                                                    color: Colors
                                                                        .yellow)),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 12,
                                                                  right: 12,
                                                                  bottom: 3,
                                                                  top: 2),
                                                          child: Text(
                                                              TFormatter.capitalizeSentence(
                                                                  resAstroList[
                                                                          i]
                                                                      .astrologer
                                                                      .labelText),
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .fade,
                                                              style: GoogleFonts.poppins(
                                                                  fontSize: 8,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                Positioned(
                                                  bottom: 10,
                                                  right: 0,
                                                  left: 64,
                                                  child: CircleAvatar(
                                                    radius: 7,
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: CircleAvatar(
                                                      radius: 5,
                                                      backgroundColor: resAstroList[
                                                                      i]
                                                                  .isAstrologerOnline ==
                                                              1
                                                          ? Colors.green
                                                          : Colors
                                                              .grey, // Green for online, grey for offline
                                                      child: Tooltip(
                                                        message: resAstroList[i]
                                                                    .isAstrologerOnline ==
                                                                1
                                                            ? 'Online'.tr()
                                                            : 'Offline'.tr(),
                                                        child: Container(
                                                          height: 20,
                                                          width: 20,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 10),
                                            Text(
                                                "${TFormatter.capitalize(resAstroList[i].name.toString())}",
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.poppins(
                                                    fontSize: 14),
                                                maxLines: 2,
                                                overflow: TextOverflow.fade),
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 5),
                                              child: Text(
                                                  "₹ ${resAstroList[i].astrologer.chatPrice == 0 ? '0' : resAstroList[i].astrologer.chatPrice}/" +
                                                      "min",
                                                  style: GoogleFonts.poppins(
                                                      fontSize: 12,
                                                      color: Colors.grey[800])),
                                            ),
                                            Card(
                                              color: Colors.green,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(10.0),
                                                  ),
                                                  side: BorderSide(
                                                      color: Colors.green)),
                                              child: Padding(
                                                padding: EdgeInsets.only(
                                                    left: 25,
                                                    right: 25,
                                                    bottom: 3,
                                                    top: 2),
                                                child: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Icon(
                                                      Icons.message,
                                                      size: 12,
                                                      color: Colors.white,
                                                    ),
                                                    SizedBox(width: 4),
                                                    Text("Chat".tr(),
                                                        style:
                                                            GoogleFonts.poppins(
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                color: Colors
                                                                    .white)),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              child: Text('Go Back',
                                  style:
                                      GoogleFonts.poppins(color: Colors.black)),
                              onPressed: () => Navigator.pop(context),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: ElevatedButton(
                            child: Text(
                              'Leave',
                              style: GoogleFonts.poppins(color: Colors.black),
                            ),
                            onPressed: () async {
                              FlutterCallkitIncoming.endAllCalls();
                              await Provider.of<ListOfApisProvider>(context,
                                      listen: false)
                                  .callDismiss(communicationId: id)
                                  .whenComplete(
                                () {
                                  //TODO: Yaha kuch karna hai Check kare
                                  // leaveWaitList(id);
                                },
                              );
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  AppColor.appColor),
                            ),
                          ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
      },
    );
  }

  @override
  bool get wantKeepAlive => true;

  // leaveWaitList(String id) async {
  //   showLoaderDialog(context);
  //   client = DioClient();
  //   Dio dio = await client!.getClient();

  //   var params = jsonEncode({'id': id});
  //   String api = Constants.cancelChatRequest;

  //   LeaveChatModel? walletTransactionModel =
  //       await client?.leaveChat(dio, api, params);
  //   Navigator.pop(context);

  //   Provider.of<WaitListNotifier>(context, listen: false)
  //       .checkWaitList(context, userId);
  //   WidgetsBinding.instance.addPostFrameCallback((_) {
  //     Navigator.pop(context);
  //   });
  // }
}

Widget horoScopeWidget(String img, String name) {
  return Container(
    decoration: BoxDecoration(border: Border.all(color: Colors.grey.shade200)),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Image.asset(
            img,
          ),
        ),
        // CircleAvatar(
        //   radius: 32,
        //   backgroundColor: AppColor.whiteColor,
        //   // backgroundColor: AppColor.appColor,
        //   child: Padding(
        //     padding: const EdgeInsets.all(12.0),
        //     child: Image.asset(
        //       img,
        //     ),
        //   ),
        // ),
        // SizedBox(height: 8),
        Text(
          name,
          style: GoogleFonts.poppins(
            fontSize: 14,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    ),
  );
}

secureSectionWidget(String name, String img) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Center(
        child: Container(
          height: 70,
          width: 70,
          decoration: BoxDecoration(
              color: Colors.grey.shade300,
              image: DecorationImage(
                image: AssetImage(img),
              ),
              borderRadius: BorderRadius.circular(50)),
        ),
      ),
      SizedBox(height: 10),
      Text(
        name,
        style: GoogleFonts.poppins(fontSize: 15),
        textAlign: TextAlign.center,
      ),
    ],
  );
}
