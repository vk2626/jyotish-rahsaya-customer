import 'dart:convert';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/Model/pooja_order_model.dart';
import '../Core/Model/pooja_suggested_model.dart';
import '../Core/Provider/chatting_provider.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Core/formatter.dart';
import '../Core/helper_functions.dart';
import '../Core/logger_helper.dart';
import '../Screens/jyotish_mall/live_pooja_screen.dart';
import '../Screens/jyotish_mall/pooja_details_screen_new.dart';
import '../pooja_screens/pooja_chat_screen.dart';
import '../utils/AppColor.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../router_constants.dart';
import '../utils/custom_circular_progress_indicator.dart';

class OrderPoojaListScreen extends StatefulWidget {
  const OrderPoojaListScreen({super.key});

  @override
  State<OrderPoojaListScreen> createState() => _OrderPoojaListScreenState();
}

class _OrderPoojaListScreenState extends State<OrderPoojaListScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  bool isLoading = true;
  List<PoojaList> purchasedPoojaList = [];
  List<Customer> suggestedPoojaList = [];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(_handleTabChange);
    _loadData2();
    _loadData1();
    THelperFunctions.logScreenNameEvent(screenName: "order_pooja_list_screen");
  }

  // Load data into the lists for both tabs
  Future<void> _loadData2() async {
    // try {
    var poojaOrderList = await getPoojaOrderList();
    setState(() {
      purchasedPoojaList = poojaOrderList
          // .where((pooja) => pooja.status == 'purchased') // Assuming "purchased" is the criteria
          .toList();
      // suggestedPoojaList = poojaOrderList
      //     .where((pooja) => pooja.status == 'suggested') // Assuming "suggested" is the criteria
      //     .toList();
      isLoading = false;
    });
    // } catch (e) {
    //   TLoggerHelper.error(e.toString());
    //   setState(() {
    //     isLoading = false;
    //   });
    // }
  }

  Future<void> _loadData1() async {
    try {
      var suggestedPoojaListData = await getSuggestedPoojaOrderList();
      setState(() {
        suggestedPoojaList = suggestedPoojaListData
            // .where((pooja) => pooja.status == 'purchased') // Assuming "purchased" is the criteria
            .toList();
        // suggestedPoojaList = poojaOrderList
        //     .where((pooja) => pooja.status == 'suggested') // Assuming "suggested" is the criteria
        //     .toList();
        isLoading = false;
      });
    } catch (e) {
      TLoggerHelper.error(e.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

// Handle tab change
  void _handleTabChange() {
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {
                    _tabController.animateTo(0);
                    _loadData1();
                  },
                  child: Chip(
                    label: Text('Suggested'),
                    backgroundColor: _tabController.index == 0
                        ? AppColor.appColor
                        : Colors.grey.shade300,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _tabController.animateTo(1);
                    _loadData2();
                  },
                  child: Chip(
                    label: Text('Purchased'),
                    backgroundColor: _tabController.index == 1
                        ? AppColor.appColor
                        : Colors.grey.shade300,
                  ),
                ),
              ],
            ),
            isLoading
                ? Center(child: CustomCircularProgressIndicator())
                : Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        _buildSuggestedPoojaList(
                            suggestedPoojaList), // Suggested tab content
                        _buildPoojaList(
                            purchasedPoojaList), // Purchased tab content
                      ],
                    ),
                  )
          ])),
    );
  }

  Widget _buildPoojaList(List<PoojaList> poojaList) {
    if (poojaList.isEmpty) {
      return Center(child: Text("No data available.".tr()));
    }

    return ListView.builder(
      itemCount: poojaList.length,
      itemBuilder: (context, index) {
        return Container(
          width: double.infinity,
          margin: EdgeInsets.only(bottom: 8),
          padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RichText(
                text: TextSpan(
                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Order ID'.tr() + ': ',
                      style: GoogleFonts.poppins(
                          fontSize: 14.0, fontWeight: FontWeight.w600),
                    ),
                    TextSpan(
                      text: poojaList[index].orderPoojaId,
                      style: GoogleFonts.poppins(fontSize: 12),
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Name'.tr() + ': ',
                      style: GoogleFonts.poppins(
                          fontSize: 14.0, fontWeight: FontWeight.w600),
                    ),
                    TextSpan(
                      text: poojaList[index].name,
                      style: GoogleFonts.poppins(fontSize: 12),
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Product Name'.tr() + ': ',
                      style: GoogleFonts.poppins(
                          fontSize: 14.0, fontWeight: FontWeight.w600),
                    ),
                    TextSpan(
                      text: poojaList[index].title,
                      style: GoogleFonts.poppins(fontSize: 12),
                    ),
                  ],
                ),
              ),
              if (poojaList[index].poojaTime != null)
                RichText(
                  text: TextSpan(
                    style:
                        GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Pooja Timing'.tr() + ': ',
                        style: GoogleFonts.poppins(
                            fontSize: 14.0, fontWeight: FontWeight.w600),
                      ),
                      TextSpan(
                        text: TFormatter.formatDateTime(
                            poojaList[index].poojaTime ?? DateTime.now()),
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                    ],
                  ),
                ),
              Row(
                children: [
                  if (poojaList[index].orderPoojaStatus >= 1)
                    ElevatedButton(
                      onPressed: () async {
                        String userId = await THelperFunctions.getUserId();
                        Navigator.of(context)
                            .push(
                              MaterialPageRoute(
                                builder: (context) => ListenableProvider(
                                  create: (context) => ChattingProvider(),
                                  child: PoojaChatScreen(
                                    astrologerName: poojaList[index].name,
                                    fromId: userId,
                                    toId: poojaList[index].userId,
                                    isToViewOnly:
                                        poojaList[index].orderPoojaStatus != 1,
                                    orderId: poojaList[index]
                                        .orderPoojaId
                                        .toString(),
                                    poojaName: poojaList[index].title,
                                  ),
                                ),
                              ),
                            )
                            .whenComplete(
                              () => _loadData2(),
                            );
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(AppColor.appColor),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5))),
                      ),
                      child: Text(
                        'Chat'.tr(),
                        style: GoogleFonts.poppins(
                            color: Colors.black, fontWeight: FontWeight.w600),
                      ),
                    ),
                  if (poojaList[index].connectToken.isNotEmpty ||
                      poojaList[index].channelName.isNotEmpty)
                    SizedBox(width: 18),
                  if ((poojaList[index].connectToken.isNotEmpty ||
                          poojaList[index].channelName.isNotEmpty) &&
                      poojaList[index].orderPoojaStatus != 3)
                    ElevatedButton(
                      onPressed: () async {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => LivePoojaScreen(
                              token: poojaList[index].connectToken,
                              channelName: poojaList[index].channelName,
                              astrologerName: poojaList[index].name,
                              astrologerImage: poojaList[index].avatar,
                              poojaId: poojaList[index].poojaId.toString(),
                              astrologerId: poojaList[index].astrologerId,
                            ),
                          ),
                        );
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(AppColor.appColor),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5))),
                      ),
                      child: Text(
                        'Connect to Live'.tr(),
                        style: GoogleFonts.poppins(
                            color: Colors.black, fontWeight: FontWeight.w600),
                      ),
                    ),
                ],
              ),
              if (poojaList[index].orderPoojaStatus > 1)
                Column(
                  children: [
                    SizedBox(height: 8),
                    Container(
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade200,
                          border: Border.all(),
                          borderRadius: BorderRadius.circular(8)),
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Order Completed"),
                          SizedBox(
                            width: 4,
                          ),
                          Icon(Icons.check_circle_rounded)
                        ],
                      ),
                    ),
                  ],
                )
            ],
          ),
        );
      },
    );
  }

  Widget _buildSuggestedPoojaList(List<Customer> poojaList) {
    if (poojaList.isEmpty) {
      return Center(child: Text("No data available.".tr()));
    }

    return ListView.builder(
      itemCount: poojaList.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            context.pushNamed(RouteConstants.poojaScreen, queryParameters: {
              'poojaId': poojaList[index].poojaId.toString(),
              'isDirectPooja': "0",
              'astrologerId': poojaList[index].astrologerId.toString(),
              "user_info_id": poojaList[index].userInfoId
            }).whenComplete(() => _loadData1());
            // Navigator.of(context).push(MaterialPageRoute(
            //   builder: (context) => ListenableProvider(
            //     create: (context) => ListOfApisProvider(),
            //     child: PoojaDetailsScreenNew(
            //         poojaId: poojaList[index].poojaId.toString()),
            //   ),
            // ));
          },
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.only(bottom: 8),
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    style:
                        GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Order ID'.tr() + ': ',
                        style: GoogleFonts.poppins(
                            fontSize: 14.0, fontWeight: FontWeight.w600),
                      ),
                      TextSpan(
                        text: poojaList[index].orderId,
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    style:
                        GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Name'.tr() + ': ',
                        style: GoogleFonts.poppins(
                            fontSize: 14.0, fontWeight: FontWeight.w600),
                      ),
                      TextSpan(
                        text: TFormatter.capitalizeSentence(
                            poojaList[index].suggestedAstroName),
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    style:
                        GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Product Name'.tr() + ': ',
                        style: GoogleFonts.poppins(
                            fontSize: 14.0, fontWeight: FontWeight.w600),
                      ),
                      TextSpan(
                        text: TFormatter.capitalizeSentence(
                            poojaList[index].productTitle),
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    style:
                        GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Pooja Timing'.tr() + ': ',
                        style: GoogleFonts.poppins(
                            fontSize: 14.0, fontWeight: FontWeight.w600),
                      ),
                      TextSpan(
                        text: TFormatter.formatDateTime(
                            poojaList[index].currentDateTime),
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                // Row(
                //   children: [
                //     if (poojaList[index]
                //         .currentDateTime
                //         .isAfter(DateTime.now()))
                // ElevatedButton(
                //   onPressed: () async {
                // String userId = await THelperFunctions.getUserId();
                // Navigator.of(context).push(
                //   MaterialPageRoute(
                //     builder: (context) => ListenableProvider(
                //       create: (context) => ChattingProvider(),
                //       child: PoojaChatScreen(
                //         astrologerName: poojaList[index].name,
                //         fromId: poojaList[index].customerDId,
                //         toId: poojaList[index].userId,
                //         orderId: poojaList[index].orderPoojaId,
                //         poojaName: poojaList[index].title,
                //       ),
                //     ),
                //   ),
                // );
                //   },
                //   style: ButtonStyle(
                //     backgroundColor:
                //         MaterialStateProperty.all(AppColor.appColor),
                //     shape: MaterialStateProperty.all(
                //         RoundedRectangleBorder(
                //             borderRadius: BorderRadius.circular(5))),
                //   ),
                //   child: Text(
                //     'Chat'.tr(),
                //     style: GoogleFonts.poppins(
                //         color: Colors.black, fontWeight: FontWeight.w600),
                //   ),
                // ),
                // if (poojaList[index].connectToken.isNotEmpty ||
                //     poojaList[index].channelName.isNotEmpty)
                //   SizedBox(width: 18),
                // if (poojaList[index].connectToken.isNotEmpty ||
                //     poojaList[index].channelName.isNotEmpty)
                //   ElevatedButton(
                //     onPressed: () async {
                //       Navigator.of(context).push(
                //         MaterialPageRoute(
                //           builder: (context) => LivePoojaScreen(
                //             token: poojaList[index].connectToken,
                //             channelName: poojaList[index].channelName,
                //             astrologerName: poojaList[index].name,
                //             astrologerImage: poojaList[index].avatar,
                //             poojaId: poojaList[index].poojaId.toString(),
                //             astrologerId: poojaList[index].astrologerId,
                //           ),
                //         ),
                //       );
                //     },
                //     style: ButtonStyle(
                //       backgroundColor:
                //           MaterialStateProperty.all(AppColor.appColor),
                //       shape: MaterialStateProperty.all(RoundedRectangleBorder(
                //           borderRadius: BorderRadius.circular(5))),
                //     ),
                //     child: Text(
                //       'Connect to Live'.tr(),
                //       style: GoogleFonts.poppins(
                //           color: Colors.black, fontWeight: FontWeight.w600),
                //     ),
                //   ),
                //   ],
                // ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<List<PoojaList>> getPoojaOrderList() async {
    // try {
    var endPointUrl = UrlConstants.getPoojaOrderList;
    Map<String, String> body = {
      "user_id": await THelperFunctions.getUserId(),
    };
    TLoggerHelper.info(body.entries.toString());
    var getPoojaOrderList = await THelperFunctions.httpPostHandler(
      client: http.Client(),
      endPointUrl: endPointUrl,
      body: body,
    );
    Map<String, dynamic> parseData = await jsonDecode(getPoojaOrderList);
    TLoggerHelper.warning(parseData.entries.toString());
    PoojaOrderModel poojaOrderModel = PoojaOrderModel.fromJson(parseData);
    return poojaOrderModel.poojaList;
    // } catch (e) {
    //   TLoggerHelper.error(e.toString());
    //   return [];
    // }
  }

  Future<List<Customer>> getSuggestedPoojaOrderList() async {
    try {
      var endPointUrl = UrlConstants.getSuggestedPoojaOrderList;
      Map<String, String> body = {
        "user_id": await THelperFunctions.getUserId(),
      };
      TLoggerHelper.info(body.entries.toString());
      var getPoojaOrderList = await THelperFunctions.httpPostHandler(
        client: http.Client(),
        endPointUrl: endPointUrl,
        body: body,
      );
      Map<String, dynamic> parseData = await jsonDecode(getPoojaOrderList);
      // TLoggerHelper.warning(parseData.entries.toString());
      PoojaSuggestedModel poojaOrderModel =
          PoojaSuggestedModel.fromJson(parseData);
      return poojaOrderModel.customer;
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return [];
    }
  }
}
