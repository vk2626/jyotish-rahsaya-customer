import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;
// import 'dart:nativewrappers/_internal/vm/lib/ffi_allocation_patch.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:jyotish_rahsaya/Core/Api/list.of.apis.dart';
import 'package:jyotish_rahsaya/Core/Provider/go_live_provider.dart';
import 'package:jyotish_rahsaya/Core/formatter.dart';
import 'package:jyotish_rahsaya/Core/helper_functions.dart';
import 'package:jyotish_rahsaya/Screens/AstrologerProfileNew.dart';
import 'package:jyotish_rahsaya/dialog/showLoaderDialog.dart';
import 'package:jyotish_rahsaya/router_constants.dart';
import '../Core/Model/astrologer.category.list.model.dart';
import '../Core/Model/astrologer.slider.category.model.dart';
import '../Core/Model/recharge.amount.model.dart';
import '../Core/logger_helper.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../Screens/payment.info.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Model/astrologer.list.model.dart';
import '../Core/Model/wallet.transection.model.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Screens/ChatIntakeForm.dart';
import '../Screens/LoginScreen.dart';
import '../Screens/ProfileScreen.dart';
import '../utils/DashSeparator.dart';
import '../utils/custom_circular_progress_indicator.dart';

class CallScreen extends StatefulWidget {
  List<String>? selectedSortBy;
  List<String>? selectedCountry;
  List<String>? selectedGender;
  List<String>? selectedLng;
  List<String>? selectedSkills;
  CallScreen({
    Key? key,
    this.selectedSortBy,
    this.selectedCountry,
    this.selectedGender,
    this.selectedLng,
    this.selectedSkills,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _CallScreenState();
  }
}

class _CallScreenState extends State<CallScreen>
    with AutomaticKeepAliveClientMixin {
  int selectedPos = 0;
  String astroID = "1";
  String astroName = "";
  int customerID = 0;
  String customerName = "";
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  var isRefresh = true;
  String profileImage = "";
  String waitChatID = "";
  String walletAmount = "";
  List<AstrologerElement> localresAstroList = [];
  String price = "";
  var filteredList = false;
  String waitTime = "";
  Set<String> pagedUsersIDs = {};
  final PagingController<int, AstrologerElement> _pagingController =
      PagingController(firstPageKey: 20);
  static const _pageSize = 20;
  AstrologerCategoryListModel? resAstroCategoryList;
  SliderAstrologerCategoryModel? resSliderCategoryData;
  @override
  void initState() {
    // context
    //     .read<ListOfApisProvider>()
    //     .getAstrologerCategoryListProvider(context: context);
    //
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    // Provider.of<WaitListNotifier>(context, listen: false)
    //     .checkWaitList(context, customerID);
    getUserId();
    runProviders();
    super.initState();
  }

  void runProviders() async {
    Provider.of<ListOfApisProvider>(context, listen: false)
        .getAstrologerCategoryListProvider()
        .then((value) {
      if (value != null) {
        setState(() {
          resAstroCategoryList = value;
        });
      }
    });
    Provider.of<ListOfApisProvider>(context, listen: false)
        .getSliderAstrologerCategoryListProvider(id: "1")
        .then((value) {
      if (value != null) {
        setState(() {
          resSliderCategoryData = value;
        });
      }
    });
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      TLoggerHelper.info(
          (pageKey / 20).toString() + " This is the page key <<<<<<---->>>>>");

      List<AstrologerElement> astrologerElement = [];

      // Fetch from both APIs for the first page
      if ((pageKey / 20).toInt() == 1) {
        var firstApiData =
            await ListOfApi().getAstrologerWithOrderList(type: "call");
        if (firstApiData != null && firstApiData.astrologers != null) {
          astrologerElement.addAll(firstApiData.astrologers!.data);
        }
      }

      // Fetch from the second API for all pages, including the first one
      var secondApiData = await ListOfApi().getAstrologerList(
        customerID,
        "call",
        (pageKey / 20).toInt(),
        pagedUsersIDs,
        selectedSortBy: widget.selectedSortBy ?? [],
        selectedCountry: widget.selectedCountry ?? [],
        selectedGender: widget.selectedGender ?? [],
        selectedLng: widget.selectedLng ?? [],
        selectedSkills: widget.selectedSkills ?? [],
      );

      if (secondApiData != null && secondApiData.astrologers != null) {
        astrologerElement.addAll(secondApiData.astrologers!.data);
      }

      // Save the result locally if it's the first page
      if ((pageKey / 20).toInt() == 1) {
        localresAstroList = astrologerElement;
      }

      // Check if this is the last page
      final isLastPage = astrologerElement.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(astrologerElement);
      } else {
        final nextPageKey = pageKey + astrologerElement.length;
        _pagingController.appendPage(astrologerElement, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: RefreshIndicator(
            color: Colors.white,
            backgroundColor: AppColor.appColor,
            onRefresh: () async {
              setState(() {
                isRefresh = true;
                filteredList = false;
              });
              _pagingController.refresh();
              runProviders();
              getWalletAmount();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                /********************* Chip Category Section ************************/
                if (resAstroCategoryList != null)
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    width: double.infinity,
                    height: 40,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: resAstroCategoryList!.data.length,
                      itemBuilder: (BuildContext context, int i) {
                        return Container(
                          margin: EdgeInsets.only(left: 10),
                          height: 40,
                          child: Card(
                            color: Colors.white,
                            elevation: 2,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25.0)),
                                side: BorderSide(
                                    color: selectedPos == i
                                        ? AppColor.appColor
                                        : Colors.white)),
                            child: GestureDetector(
                              child: Row(
                                children: <Widget>[
                                  SizedBox(width: 10),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          image: DecorationImage(
                                              image: CachedNetworkImageProvider(
                                                  "${resAstroCategoryList!.data[i].image}"))),
                                    ),
                                  ),
                                  SizedBox(width: 5),
                                  Text("${resAstroCategoryList!.data[i].name}",
                                      style: GoogleFonts.poppins(
                                          color: Colors.black),
                                      textAlign: TextAlign.center),
                                  SizedBox(width: 10),
                                ],
                              ),
                              onTap: () {
                                setState(() {
                                  selectedPos = i;
                                  astroID = resAstroCategoryList!.data[i].id!;
                                  Provider.of<ListOfApisProvider>(context,
                                          listen: false)
                                      .getSliderAstrologerCategoryListProvider(
                                          id: astroID)
                                      .then((value) {
                                    if (value != null) {
                                      setState(() {
                                        resSliderCategoryData = value;
                                      });
                                    }
                                  });
                                  Provider.of<ListOfApisProvider>(context,
                                          listen: false)
                                      .getAstrologerListProvider(
                                          context: context,
                                          UserId: double.parse(astroID).toInt(),
                                          type: 'call',
                                          pageNumber: 1);

                                  print("Sele ID ---> $astroID");
                                });
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  ),

                /****************** Carousel Slider *********************/
                if (resSliderCategoryData != null &&
                    resAstroCategoryList != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Container(
                        width: double.infinity,
                        child: CarouselSlider.builder(
                          itemCount: resSliderCategoryData!.data.length,
                          itemBuilder: (BuildContext context, int itemIndex,
                              int pageViewIndex) {
                            return Container(
                              width: double.infinity,
                              margin: EdgeInsets.symmetric(horizontal: 6),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(6),
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      alignment: Alignment.centerLeft,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      color: TFormatter.hexToColor(
                                          resAstroCategoryList!
                                              .data[selectedPos].color),
                                      child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .5,
                                        child: Text(
                                          "Q. " +
                                                  resSliderCategoryData!
                                                      .data[itemIndex].title
                                                      .toString() ??
                                              "",
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: GoogleFonts.poppins(
                                              color: Colors.white,
                                              fontSize: 16),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                        bottom: -30,
                                        right: -40,
                                        child: CircleAvatar(
                                          radius: 64,
                                        )),
                                    Positioned(
                                        bottom: -25,
                                        right: -50,
                                        child: CircleAvatar(
                                          radius: 64,
                                          backgroundColor: Colors.white,
                                        )),
                                    Positioned(
                                      bottom: 5,
                                      right: 15,
                                      child: SvgPicture.network(
                                        resSliderCategoryData!
                                            .data[itemIndex].image!,
                                        height: 40,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          options: CarouselOptions(
                            autoPlay: true,
                            height: 50,
                            viewportFraction: 1,
                            reverse: false,
                            enableInfiniteScroll: false,
                            enlargeCenterPage: false,
                            initialPage: selectedPos,
                          ),
                        )),
                  ),
                /**************** Astrologer List Section **********************/
                Expanded(
                  child: PagedListView<int, AstrologerElement>(
                      pagingController: _pagingController,
                      shrinkWrap: true,
                      builderDelegate: PagedChildBuilderDelegate<
                              AstrologerElement>(
                          firstPageProgressIndicatorBuilder: (context) =>
                              Center(
                                child: CustomCircularProgressIndicator(),
                              ),
                          newPageProgressIndicatorBuilder: (context) => Center(
                                child: CustomCircularProgressIndicator(),
                              ),
                          itemBuilder: (context, resAstroList, i) {
                            pagedUsersIDs.addAll(_pagingController.itemList!
                                .map((element) => element.astrologer.userId)
                                .toSet());

                            // TLoggerHelper.debug(
                            //     "Astrologers IDs:- $pagedUsersIDs");
                            return GestureDetector(
                              onTap: () {
                                context.pushNamed(
                                  RouteConstants.astrologerProfile,
                                  queryParameters: {
                                    'astrologerId':
                                        resAstroList.astrologer.userId,
                                    'type': 'call',
                                    'isAstrologerOnline': resAstroList
                                        .isAstrologerOnline
                                        .toString()
                                  },
                                );
                                print("pos---> $i");
                              },
                              child: Container(
                                margin: EdgeInsets.only(
                                    left: 10, right: 10, top: 2),
                                child: Card(
                                  color: Colors.white,
                                  elevation: 1,
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(6.0)),
                                  ),
                                  child: Stack(
                                    children: [
                                      Row(
                                        children: [
                                          SizedBox(width: 20),
                                          Column(
                                            children: <Widget>[
                                              SizedBox(height: 10),
                                              Stack(
                                                children: [
                                                  Center(
                                                    child: Card(
                                                      clipBehavior: Clip
                                                          .antiAliasWithSaveLayer,
                                                      child: CircleAvatar(
                                                        radius: 40,
                                                        backgroundImage:
                                                            CachedNetworkImageProvider(
                                                                resAstroList
                                                                    .avatar),
                                                      ),
                                                      shape: CircleBorder(
                                                        side: BorderSide(
                                                            color: AppColor
                                                                .appColor,
                                                            width: 2),
                                                      ),
                                                    ),
                                                  ),
                                                  if (resAstroList.label != 0)
                                                    Positioned(
                                                      bottom: -4,
                                                      left: 0,
                                                      right: 0,
                                                      child: Center(
                                                        child: Card(
                                                          color: Colors.amber,
                                                          shape:
                                                              RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .all(
                                                                    Radius.circular(
                                                                        10.0),
                                                                  ),
                                                                  side: BorderSide(
                                                                      color: Colors
                                                                          .yellow)),
                                                          child: Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 12,
                                                                    right: 12,
                                                                    bottom: 3,
                                                                    top: 2),
                                                            child: Text(
                                                                TFormatter.capitalizeSentence(
                                                                    resAstroList
                                                                        .astrologer
                                                                        .labelText),
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .fade,
                                                                style: GoogleFonts.poppins(
                                                                    fontSize: 8,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    color: Colors
                                                                        .white)),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  Positioned(
                                                    bottom: 10,
                                                    right: 0,
                                                    left: 64,
                                                    child: CircleAvatar(
                                                      radius: 7,
                                                      child: CircleAvatar(
                                                        radius: 5,
                                                        backgroundColor: resAstroList
                                                                    .isAstrologerOnline ==
                                                                1
                                                            ? Colors.green
                                                            : Colors
                                                                .grey, // Green for online, grey for offline
                                                        child: Tooltip(
                                                          message: resAstroList
                                                                      .isAstrologerOnline ==
                                                                  1
                                                              ? 'Online'
                                                              : 'Offline',
                                                          child: Container(
                                                            height: 20,
                                                            width: 20,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              RatingBarIndicator(
                                                  rating: resAstroList.rating,
                                                  itemCount: 5,
                                                  itemSize: 15.0,
                                                  itemBuilder: (context, _) =>
                                                      const Icon(
                                                        Icons.star,
                                                        color: Colors.amber,
                                                      )),
                                              Text(
                                                "${resAstroList.astrologer!.orderCount} " +
                                                    "orders".tr(),
                                                style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                                textAlign: TextAlign.center,
                                              ),
                                              SizedBox(height: 10),
                                            ],
                                          ),
                                          SizedBox(width: 10),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "${TFormatter.capitalizeSentence(resAstroList.name)}",
                                                  style: GoogleFonts.poppins(
                                                      color: Colors.black,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                SizedBox(
                                                  height: 2,
                                                ),
                                                Text(
                                                  "${resAstroList.astrologer.allSkillName}",
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                  ),
                                                  maxLines: 2,
                                                ),
                                                Text(
                                                  "${resAstroList.astrologer.languageName}",
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                  ),
                                                  maxLines: 2,
                                                ),
                                                SizedBox(
                                                  height: 2,
                                                ),
                                                Text(
                                                  "Exp".tr() +
                                                      " : ${resAstroList.astrologer.experienceYear} " +
                                                      "yr".tr(),
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                                SizedBox(
                                                  height: 4,
                                                ),
                                                Text.rich(
                                                    TextSpan(
                                                      children: [
                                                        TextSpan(
                                                          text: "₹",
                                                          style: GoogleFonts
                                                              .poppins(
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors
                                                                .grey[600],
                                                            // Add line through original price
                                                          ),
                                                        ),
                                                        TextSpan(
                                                          text:
                                                              "${resAstroList.astrologer!.callPrice == 0 ? "0" : resAstroList.astrologer!.callPrice}",
                                                          style: GoogleFonts
                                                              .poppins(
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors
                                                                .grey[600],
                                                            decoration: resAstroList
                                                                        .astrologer!
                                                                        .callDiscountPrice ==
                                                                    0
                                                                ? TextDecoration
                                                                    .none
                                                                : TextDecoration
                                                                    .lineThrough, // Add line through original price
                                                          ),
                                                        ),
                                                        TextSpan(
                                                          text:
                                                              " ${resAstroList.astrologer!.callDiscountPrice == 0 ? '' : resAstroList.astrologer!.callDiscountPrice}",
                                                          style: GoogleFonts
                                                              .poppins(
                                                            color: Colors.red,
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    textAlign:
                                                        TextAlign.center),
                                                SizedBox(height: 10),
                                              ],
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.verified,
                                                  color: Colors.green,
                                                ),
                                                SizedBox(height: 24),
                                                SizedBox(
                                                  height: 35,
                                                  child: TextButton(
                                                    onPressed: () {
                                                      if (resAstroList
                                                              .astrologer
                                                              .statusCall ==
                                                          1) {
                                                        if (resAstroList
                                                                .astrologer
                                                                .nextonlines
                                                                .callNOTime
                                                                .isEmpty &&
                                                            resAstroList
                                                                .astrologer
                                                                .nextonlines
                                                                .callNODate
                                                                .isEmpty) {
                                                          if (customerID > 0 &&
                                                              customerName !=
                                                                  "") {
                                                            var data =
                                                                resAstroList
                                                                    .astrologer;
                                                            int realPrice = (data
                                                                            .callDiscountPrice <=
                                                                        data
                                                                            .callPrice) &&
                                                                    (data.callDiscountPrice !=
                                                                        0)
                                                                ? data
                                                                    .callDiscountPrice
                                                                : data
                                                                    .callPrice;
                                                            TLoggerHelper.debug(
                                                                "Real Price:- ${realPrice} and Wallet Amount:- ${double.parse(walletAmount)}");
                                                            if ((double.parse(
                                                                        walletAmount) >=
                                                                    realPrice *
                                                                        5) &&
                                                                walletAmount !=
                                                                    "0") {
                                                              showDialog(
                                                                  context:
                                                                      context,
                                                                  builder:
                                                                      (BuildContext
                                                                          context) {
                                                                    return Dialog(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .white,
                                                                      shape: RoundedRectangleBorder(
                                                                          borderRadius:
                                                                              BorderRadius.circular(20.0)),
                                                                      //this right here
                                                                      child:
                                                                          Padding(
                                                                        padding: const EdgeInsets
                                                                            .all(
                                                                            5.0),
                                                                        child:
                                                                            Column(
                                                                          mainAxisSize:
                                                                              MainAxisSize.min,
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Align(
                                                                              alignment: Alignment.topRight,
                                                                              child: IconButton(
                                                                                onPressed: () {
                                                                                  Navigator.pop(context);
                                                                                },
                                                                                icon: Icon(
                                                                                  Icons.close,
                                                                                  size: 30,
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Card(
                                                                              clipBehavior: Clip.antiAliasWithSaveLayer,
                                                                              child: CircleAvatar(
                                                                                radius: 60,
                                                                                backgroundImage: CachedNetworkImageProvider("${resAstroList.avatar}"),
                                                                              ),
                                                                              shape: CircleBorder(side: BorderSide(color: AppColor.appColor, width: 2)),
                                                                            ),
                                                                            Text(
                                                                              TFormatter.capitalizeSentence(resAstroList.name),
                                                                              style: GoogleFonts.poppins(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
                                                                            ),
                                                                            SizedBox(height: 10),
                                                                            Text(
                                                                              resAstroList.astrologer.freeChat != 0 ? "This offer at free for 3 min only. Astrologer will try to answer at least one question." : "This offer at".tr() + " ${Constants.currency} ${resAstroList.astrologer!.callDiscountPrice == 0 ? resAstroList.astrologer!.callPrice : resAstroList.astrologer!.callDiscountPrice}/" + "min for 5 mins only. Astrologer will try to answer at least one question.".tr(),
                                                                              style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                            SizedBox(height: 10),
                                                                            TextButton(
                                                                              onPressed: () {
                                                                                Navigator.pop(context);
                                                                                sendAndGetResult(resAstroList.name.toString(), resAstroList.id, resAstroList.avatar.toString(), context, resAstroList.astrologer!.userId.toString(), resAstroList.astrologer!.freeChat);
                                                                              },
                                                                              style: TextButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)), backgroundColor: AppColor.appColor),
                                                                              child: Padding(
                                                                                padding: EdgeInsets.symmetric(horizontal: 20),
                                                                                child: Text(
                                                                                  "Start Call".tr() + " @ ${Constants.currency} ${resAstroList.astrologer!.callDiscountPrice == 0 ? resAstroList.astrologer!.callPrice : resAstroList.astrologer!.callDiscountPrice}/" + "min".tr(),
                                                                                  style: GoogleFonts.poppins(color: Colors.black),
                                                                                ),
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    );
                                                                  });
                                                            } else {
                                                              _rechargeDialog(
                                                                      context,
                                                                      resAstroList
                                                                          .name
                                                                          .toString(),
                                                                      resAstroList
                                                                          .id
                                                                          .toString(),
                                                                      resAstroList.astrologer.callDiscountPrice == 0
                                                                          ? resAstroList
                                                                              .astrologer
                                                                              .callPrice
                                                                          : resAstroList
                                                                              .astrologer
                                                                              .callDiscountPrice)
                                                                  .whenComplete(
                                                                () {
                                                                  getWalletAmount();
                                                                },
                                                              );
                                                            }
                                                          } else if (customerID >
                                                                  0 &&
                                                              customerName ==
                                                                  "") {
                                                            Navigator.of(
                                                                    context)
                                                                .push(SlideRightRoute(
                                                                    page:
                                                                        ProfileScreen()))
                                                                .then((value) =>
                                                                    getUserId());
                                                          } else {
                                                            context.pushNamed(
                                                                RouteConstants
                                                                    .authScreen);
                                                          }
                                                        } else {
                                                          nextOnlineDialog(
                                                              astrologerName:
                                                                  resAstroList
                                                                      .name,
                                                              astrologerImage:
                                                                  resAstroList
                                                                      .avatar,
                                                              nextOnlineTime:
                                                                  resAstroList
                                                                      .astrologer
                                                                      .nextonlines
                                                                      .callNOTime,
                                                              nextOnlineDate:
                                                                  resAstroList
                                                                      .astrologer
                                                                      .nextonlines
                                                                      .callNODate);
                                                        }
                                                      } else {
                                                        Fluttertoast.showToast(
                                                            msg:
                                                                "${resAstroList.astrologer.displayName} Call is Offline");
                                                      }
                                                    },
                                                    style: TextButton.styleFrom(
                                                        side: BorderSide(
                                                            color: resAstroList
                                                                            .astrologer
                                                                            .waitTime! ==
                                                                        0 &&
                                                                    resAstroList
                                                                            .astrologer
                                                                            .statusCall ==
                                                                        1
                                                                ? Colors
                                                                    .greenAccent
                                                                : Colors.red),
                                                        shape: RoundedRectangleBorder(
                                                            side: BorderSide(
                                                                width: 2),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5))),
                                                    child: Text(
                                                      resAstroList.astrologer
                                                                  .statusCall ==
                                                              1
                                                          ? "Call".tr()
                                                          : "Call Offline".tr(),
                                                      style: GoogleFonts.poppins(
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color: resAstroList
                                                                          .astrologer
                                                                          .waitTime! ==
                                                                      0 &&
                                                                  resAstroList
                                                                          .astrologer
                                                                          .statusCall ==
                                                                      1
                                                              ? Colors
                                                                  .greenAccent
                                                              : Colors.red),
                                                    ),
                                                  ),
                                                ),
                                                Visibility(
                                                  child: Text(
                                                      "Wait ~ ${resAstroList.astrologer.waitTime} /mins",
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color: Colors
                                                                  .red[600],
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                  visible: resAstroList
                                                              .astrologer
                                                              .waitTime! !=
                                                          0 &&
                                                      resAstroList.astrologer
                                                              .statusCall ==
                                                          1,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                      // if (resAstroList.label != 0)
                                      //   Positioned(
                                      //     left:
                                      //         -25, // Adjust the value to avoid overlapping
                                      //     top:
                                      //         15, // Adjust the value for proper positioning

                                      //     child: Transform.rotate(
                                      //       angle: -math.pi / 3.5,
                                      //       child: Container(
                                      //         decoration: BoxDecoration(
                                      //           gradient: LinearGradient(
                                      //             colors: [
                                      //               Color(
                                      //                   0xFF3D3D3D), // Dark gray (closer to black)
                                      //               Color(
                                      //                   0xFF1C1C1C), // Almost black
                                      //               Color(
                                      //                   0xFF000000), // Pure black
                                      //               Color(
                                      //                   0xFF434343), // Slightly lighter for a metallic effect
                                      //             ],
                                      //             begin: Alignment.topCenter,
                                      //             end: Alignment.bottomCenter,
                                      //           ),
                                      //         ),
                                      //         width: 100,
                                      //         alignment: Alignment.center,
                                      //         padding: EdgeInsets.symmetric(
                                      //           vertical: 2,
                                      //         ),
                                      //         child: Text(
                                      //           resAstroList
                                      //               .astrologer.labelText,
                                      //           textAlign: TextAlign.center,
                                      //           style: GoogleFonts.poppins(
                                      //             color: AppColor.whiteColor,
                                      //             fontSize: 8,
                                      //           ),
                                      //         ),
                                      //       ),
                                      //     ),
                                      //   ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          })),
                ),
                // Visibility(
                //   child: Expanded(
                //     child: Center(
                //       child: Text("No Astrologer found"),
                //     ),
                //   ),
                //   visible: resAstroList.length == 0,
                // ),
              ],
            )),
      ),
    );
  }

  /********************* Recharge Dialog ************************/
  Future<void> _rechargeDialog(BuildContext context, String name,
      String astroID, int amountPerMinute) async {
    int? amID = 0;
    String? amount = "";

    await showModalBottomSheet<void>(
      context: context,
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: Consumer<ListOfApisProvider>(builder: (context, provider, _) {
            return FutureBuilder(
                future: provider.getRechargeAmountPProvider(context: context),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(child: CustomCircularProgressIndicator());
                  } else if (snapshot.hasData) {
                    RechargeAmountModel rechargeAmountModel = snapshot.data;
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topRight,
                              child: CloseButton()),
                          Text(
                            "Minimum balance of 5 minutes".tr() +
                                " (INR ${(amountPerMinute * 5)}.0) " +
                                "is required to start Call with".tr() +
                                " ($name)",
                            textAlign: TextAlign.start,
                            style: GoogleFonts.poppins(
                                fontSize: 14, color: Colors.redAccent),
                          ),
                          SizedBox(height: 10),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Recharge Now'.tr(),
                              style: GoogleFonts.poppins(fontSize: 17),
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.light_mode_rounded,
                                color: AppColor.appColor,
                              ),
                              Text("Tip: 90% user recharge for 10 mins or more"
                                  .tr())
                            ],
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: rechargeAmountModel.recharge!.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4,
                                        childAspectRatio: 6 / 2,
                                        mainAxisSpacing: 10,
                                        crossAxisSpacing: 10),
                                itemBuilder: (context, index) {
                                  if (index == 0) {
                                    price = rechargeAmountModel
                                        .recharge![index].price!;
                                    print(rechargeAmountModel
                                        .recharge![index].price);
                                    amID =
                                        rechargeAmountModel.recharge![index].id;
                                    amount = rechargeAmountModel
                                        .recharge![index].price;
                                  }
                                  return InkWell(
                                    onTap: () {
                                      if (customerID == 0) {
                                        Navigator.of(context).push(
                                            SlideRightRoute(
                                                page: LoginScreen()));
                                      } else {
                                        Navigator.of(context)
                                            .push(
                                              SlideRightRoute(
                                                  page: ListenableProvider(
                                                create: (context) =>
                                                    ListOfApisProvider(),
                                                child: PaymentInformationScreen(
                                                    userInfoId: "",
                                                    astroName: customerName,
                                                    title:
                                                        "Recharge Amount".tr(),
                                                    price: rechargeAmountModel
                                                        .recharge![index]
                                                        .price!,
                                                    amountID:
                                                        rechargeAmountModel
                                                            .recharge![index]
                                                            .id!,
                                                    from: 'wallet',
                                                    astroId: astroID),
                                              )),
                                            )
                                            .then((value) =>
                                                Navigator.maybePop(context));
                                      }

                                      price = rechargeAmountModel
                                          .recharge![index].price!;
                                      amID = rechargeAmountModel
                                          .recharge![index].id;
                                      amount = rechargeAmountModel
                                          .recharge![index].price;
                                    },
                                    child: Container(
                                        height: 25,
                                        width: 90,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            border: Border.all(
                                                color: index == 0
                                                    ? AppColor.appColor
                                                    : AppColor.borderColor)),
                                        child: Stack(children: <Widget>[
                                          Positioned(
                                              top: 7,
                                              right: 50.5,
                                              child: Transform.rotate(
                                                  angle: -math.pi / 3.5,
                                                  child: Container(
                                                      height: 15,
                                                      width: 50,
                                                      decoration: BoxDecoration(
                                                          color: Colors.green,
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius.circular(
                                                                      5))),
                                                      child: Center(
                                                          child: Text(
                                                              "${rechargeAmountModel.recharge![index].discountPercentage}% Extra",
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      fontSize:
                                                                          8)))))),
                                          Center(
                                              child: Text(
                                                  "${rechargeAmountModel.recharge![index].price}"))
                                        ])),
                                  );
                                }),
                          ),
                          SizedBox(height: 20),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                backgroundColor: AppColor.appColor,
                                textStyle:
                                    Theme.of(context).textTheme.labelLarge,
                              ),
                              child: Text(
                                'Proceed to Pay'.tr(),
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black),
                              ),
                              onPressed: () {
                                if (customerID == 0) {
                                  Navigator.of(context).push(
                                      SlideRightRoute(page: LoginScreen()));
                                } else {
                                  Navigator.of(context)
                                      .push(
                                        SlideRightRoute(
                                          page: ListenableProvider(
                                            create: (context) =>
                                                ListOfApisProvider(),
                                            child: PaymentInformationScreen(
                                              userInfoId: "",
                                              astroName: customerName,
                                              price: price,
                                              title: "Recharge Amount".tr(),
                                              amountID: amID!,
                                              from: 'wallet',
                                              astroId: astroID,
                                            ),
                                          ),
                                        ),
                                      )
                                      .then((value) =>
                                          Navigator.maybePop(context));
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return SizedBox();
                  }
                });
          }),
        );
      },
    );
  }

  nextOnlineDialog(
      {required String astrologerName,
      required String nextOnlineTime,
      required String astrologerImage,
      required String nextOnlineDate}) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        CircleAvatar(
                          radius: 35,
                          backgroundImage:
                              CachedNetworkImageProvider(astrologerImage),
                        ),
                        Wrap(
                          children: [
                            Text(
                              TFormatter.capitalizeSentence(astrologerName),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Text(
                        "You will be connecting with".tr() +
                            " $astrologerName " +
                            "in a while.".tr(),
                        style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Next Online Time of the Astrologer is".tr() +
                          " :- $nextOnlineDate $nextOnlineTime",
                      style: GoogleFonts.poppins(
                          color: Colors.black54, fontSize: 15),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          // showCancelBookingDialog(waitChatID);
                        },
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            backgroundColor: AppColor.appColor),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "OK",
                            style: GoogleFonts.poppins(color: Colors.black),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;
      profileImage = prefs.getString(Constants.profileImage)!;
      getWalletAmount();
      if (prefs.getString(Constants.fullName) != null) {
        customerName = prefs.getString(Constants.fullName)!;
      }
    }
    setState(() {});
  }

  DioClient? client;

  getWalletAmount() {
    THelperFunctions.getWalletAmount().then((value) {
      setState(() {
        walletAmount = value.toString();
      });
    });
  }

  void sendAndGetResult(String name, int? id, String avatar,
      BuildContext context, String astroId, int? free_chat) async {
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MultiProvider(
              providers: [
                ListenableProvider(create: (context) => ListOfApisProvider()),
              ],
              child: ChatIntakeForm(name, id, astroId, "call", 0),
            )));
    if (result == "true") {
      showConfirmationDialog(name, avatar);
    }
  }

  showConfirmationDialog(String name, String userImage) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      Positioned(
                        child: DashSeparator(
                          color: Colors.grey,
                          height: 2.0,
                        ),
                        right: 50,
                        left: 50,
                        top: 40,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 100,
                            height: 150,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Card(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  child: CircleAvatar(
                                    radius: 35,
                                    backgroundImage:
                                        NetworkImage("$profileImage"),
                                  ),
                                  shape: CircleBorder(
                                      side: BorderSide(
                                          color: AppColor.appColor, width: 2)),
                                ),
                                Wrap(
                                  children: [
                                    Text(
                                      TFormatter.capitalizeSentence(
                                          customerName),
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          SizedBox(
                            width: 100,
                            height: 150,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Card(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  child: CircleAvatar(
                                    radius: 35,
                                    backgroundImage: NetworkImage("$userImage"),
                                  ),
                                  shape: CircleBorder(
                                      side: BorderSide(
                                          color: AppColor.appColor, width: 2)),
                                ),
                                Text(
                                  "${name}",
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.fade,
                                  style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Text(
                      "You will be connecting with" +
                          " ${TFormatter.capitalizeSentence(name)} " +
                          "in a while.".tr(),
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w700),
                      textAlign: TextAlign.center),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "While you wait for ${TFormatter.capitalizeSentence(name)} you may also explore other astrologers and join their wishlist.",
                    style: GoogleFonts.poppins(
                        color: Colors.black54, fontSize: 15),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        // showCancelBookingDialog(waitChatID);
                      },
                      style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          backgroundColor: AppColor.appColor),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          "OK",
                          style: GoogleFonts.poppins(color: Colors.black),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  bool get wantKeepAlive => true;
}
