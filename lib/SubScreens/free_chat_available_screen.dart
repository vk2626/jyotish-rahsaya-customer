import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/helper_functions.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/list.of.api.provider.dart';
import '../Screens/ChatIntakeForm.dart';
import '../utils/custom_floating_action_button.dart';

class FreeChatAvailableScreen extends StatefulWidget {
  const FreeChatAvailableScreen({super.key});

  @override
  State<FreeChatAvailableScreen> createState() =>
      _FreeChatAvailableScreenState();
}

class _FreeChatAvailableScreenState extends State<FreeChatAvailableScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.1,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: [
                CircleAvatar(
                  radius: MediaQuery.of(context).size.width * 0.2,
                  backgroundColor: Colors.yellow,
                ),
                Positioned(
                  bottom: 2,
                  right: 2,
                  child: CircleAvatar(
                    radius: MediaQuery.of(context).size.width * 0.2,
                    backgroundColor: Colors.yellow,
                  ),
                ),
                Image.asset("asset/images/testimonial.png"),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "Congratulations!\n",
                    style: GoogleFonts.satisfy(
                      fontSize: 32,
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ),
                  TextSpan(
                    text: "You got a Free Chat!\n",
                    style: GoogleFonts.poppins(
                      fontSize: 18,
                      color: Colors.black54,
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(
                  "Enjoy your free session with our expert astrologers.",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    color: Colors.black87,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: CustomFloatingButton(   
        onPressed: () async {
          String userId = await THelperFunctions.getUserId();
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => MultiProvider(
                      providers: [
                        ListenableProvider(
                            create: (context) => ListOfApisProvider()),
                      ],
                      child: ChatIntakeForm(
                        "Astrologer",
                        userId,
                        userId,
                        "chat",
                        1,
                        isRandomChat: true,
                      ))));
        },
      ),
    );
  }
}
