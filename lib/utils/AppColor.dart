import 'package:flutter/material.dart';

Color hexToColor(String hex) {
  assert(RegExp(r'^#([0-9a-fA-F]{6})|([0-9a-fA-F]{8})$').hasMatch(hex),
      'hex color must be #rrggbb or #rrggbbaa');

  return Color(
    int.parse(hex.substring(1), radix: 16) +
        (hex.length == 7 ? 0xff000000 : 0x00000000),
  );
}

class AppColor {
  static const Color blackColor = Color(0xFF000000);
  static const Color whiteColor = Color(0xFFFEFEFE);
  static const Color appColor = Color(0xfff0df20);
  static const Color appiconColor = Color(0xffE2B81E);
  static const Color appDarkColor = Color(0xffe8d704);
  static const Color appBackgroundLightGrey = Color(0xFFF5F4F4);
  static const Color btnActiveColorLight = Color(0xfffff4b1);
  static const Color darkGrey = Color(0xFF4C4C4C);
  static const Color borderColor = Color(0xFF989898);
  static const Color lightGrey = Color(0xFFCBC9C9);
  static const Color ltGrey = Color(0xFFECEBEB);
  static const Color transparentBlack = Color(0x80000000);
  static const Color grey = Color(0xFFE0E0E0);
}
