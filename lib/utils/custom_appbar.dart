import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'AppColor.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool isAtHomePage;
  final Widget? actionIcon;
  const CustomAppBar(
      {super.key,
      required this.title,
      this.isAtHomePage = false,
      this.actionIcon});

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          AppBar(
            title: Text(
              title,
              style: GoogleFonts.poppins(
                fontSize: 22.0,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.transparent,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 18.0),
                child: (actionIcon == null) ? const SizedBox() : actionIcon!,
              )
            ],
          ),
          DividerTheme(
            data: DividerThemeData(space: 0),
            child: Divider(),
          )
        ],
      ),
    );
  }
}
