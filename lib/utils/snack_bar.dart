import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

showSnackBarWidget(context, String message, {bool isTrue = false}) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(
      message,
      style:
          GoogleFonts.poppins(fontWeight: FontWeight.w600, color: Colors.white),
    ),
    backgroundColor: isTrue ? Colors.green : Colors.red,
  ));
}
