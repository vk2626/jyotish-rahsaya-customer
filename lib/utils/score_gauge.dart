import 'package:flutter/material.dart';
import 'dart:math' as math;

class ScoreGauge extends StatelessWidget {
  final double score; // current score
  final double maxScore; // maximum score

  const ScoreGauge({Key? key, required this.score, required this.maxScore})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;

    // Calculate the needle rotation angle (0 is left, and 180 degrees is right)
    double percentage = score / maxScore;
    double angle = percentage *
        math.pi; // Converts percentage to radians (max is π radians for 180 degrees)

    return SizedBox(
      height: mediaQuery.height * .15,
      child: Stack(
        fit: StackFit.expand,
        children: [
          // Background compatibility chart
          Image.asset(
            "asset/images/compatibility_chart.png",
            height: mediaQuery.height * .15,
            width: mediaQuery.width,
            fit: BoxFit.cover,
          ),
          // Needle (rotate based on score)
          Positioned(
            bottom: -24,
            left: mediaQuery.width * 0.25, // adjust position as needed
            child: Transform.rotate(
              angle: angle,
              alignment: Alignment.center,
              child: Image.asset(
                "asset/images/needle.png",
                height: mediaQuery.height * .08,
                width: mediaQuery.width * 0.5, // Adjust width to needle size
              ),
            ),
          ),
        ],
      ),
    );
  }
}
