import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/formatter.dart';
import 'package:jyotish_rahsaya/Core/helper_functions.dart';

import '../Core/Model/astrologer_details_model.dart';

class ReviewBottomSheet extends StatelessWidget {
  final String astrologerName;
  List<Review> reviews;
  String rating;
  ReviewBottomSheet(
      {super.key,
      required this.reviews,
      required this.astrologerName,
      required this.rating});

  @override
  Widget build(BuildContext context) {
    // Count occurrences of each rating (1-5)
    Map<int, int> ratingCount = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};

    for (var review in reviews) {
      int rate = int.tryParse(review.rate) ?? 0;
      if (ratingCount.containsKey(rate)) {
        ratingCount[rate] = ratingCount[rate]! + 1;
      }
    }

    // Get total reviews to calculate percentage
    int totalReviews = reviews.length > 0 ? reviews.length : 1;
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16))),
      width: double.infinity,
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.arrow_back_rounded),
              SizedBox(
                width: 16,
              ),
              Text(
                "Rating & Reviews",
                style: GoogleFonts.poppins(fontSize: 18),
              ),
            ],
          ),
          SizedBox(height: 12),
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.shade300,
                  blurRadius: 10,
                  spreadRadius: 2,
                  offset: Offset(0, 4),
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text(
                      rating,
                      style: GoogleFonts.poppins(fontSize: 22),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        5,
                        (index) => Icon(
                          Icons.star,
                          color: index + 1 < double.parse(rating)
                              ? Colors.yellow
                              : Colors.grey,
                          size: 14,
                        ),
                      ),
                    ),
                    Text(
                      "Based on \nRatings & Reviews",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(fontSize: 12),
                    ),
                  ],
                ),
                Column(
                  children: List.generate(
                    5,
                    (index) {
                      int star = 5 - index;
                      double progress = ratingCount[star]! / totalReviews;

                      return Row(
                        children: [
                          Text(
                            star.toString(),
                            style: GoogleFonts.poppins(fontSize: 20),
                          ),
                          SizedBox(width: 8),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.35,
                            child: LinearProgressIndicator(
                              value: progress,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                              minHeight: 16,
                              backgroundColor: Colors.grey.shade300,
                              valueColor: AlwaysStoppedAnimation(Colors.green),
                            ),
                          ),
                          SizedBox(width: 8),
                          Text(
                            '${ratingCount[star]}', // Show count of reviews
                            style: GoogleFonts.poppins(fontSize: 12),
                          ),
                        ],
                      );
                    },
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 12),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: List.generate(
                  reviews.length,
                  (index) => ReviewTile(
                    review: reviews[index],
                    astrologerName: astrologerName,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ReviewTile extends StatelessWidget {
  final Review review;
  final String astrologerName;
  const ReviewTile({
    super.key,
    required this.review,
    required this.astrologerName,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(12)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade300,
            blurRadius: 10,
            spreadRadius: 2,
            offset: Offset(0, 4),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundImage:
                    CachedNetworkImageProvider(review.customerImage),
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                TFormatter.capitalizeSentence(review.customerName),
                style: GoogleFonts.poppins(fontSize: 16),
              ),
            ],
          ),
          SizedBox(height: 4),
          Row(
            children: List.generate(
              5,
              (index) => Icon(
                Icons.star,
                color: index < int.parse(review.rate)
                    ? Colors.yellow
                    : Colors.grey,
                size: 14,
              ),
            ),
          ),
          SizedBox(height: 4),
          Text(
            TFormatter.capitalize(review.description),
            maxLines: 4,
            overflow: TextOverflow.ellipsis,
            style: GoogleFonts.poppins(fontSize: 14),
          ),
          SizedBox(height: 4),
          if (review.reply.isNotEmpty)
            Container(
              margin: EdgeInsets.only(left: 12),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: BorderRadius.circular(6)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    TFormatter.capitalizeSentence(astrologerName),
                    style: GoogleFonts.poppins(
                        color: Colors.black, fontWeight: FontWeight.w500),
                  ),
                  Text(
                    TFormatter.capitalize(review.reply),
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.poppins(fontSize: 12),
                  ),
                ],
              ),
            )
        ],
      ),
    );
  }
}
