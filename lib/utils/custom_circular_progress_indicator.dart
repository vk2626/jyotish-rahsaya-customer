
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'AppColor.dart';

class CustomCircularProgressIndicator extends StatelessWidget {
  const CustomCircularProgressIndicator({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(padding: EdgeInsets.all(6),decoration: BoxDecoration(shape: BoxShape.circle,color: Colors.white),
      child: CircularProgressIndicator(
      color: AppColor.whiteColor,backgroundColor: AppColor.appColor,
                      ),
    );
  }
}
