import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ClockWidget extends StatelessWidget {
  const ClockWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Stream.periodic(const Duration(seconds: 1)),
      builder: (context, snapshot) {
        return Text(
            DateFormat('hh:mm:ss a').format(DateTime.now()),
            style: const TextStyle(
                color:Color.fromARGB(255, 0,46,98),
                fontSize: 16)
        );
      },
    );
  }
}