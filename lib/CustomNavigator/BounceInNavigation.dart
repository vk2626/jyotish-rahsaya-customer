import 'package:flutter/material.dart';
class BounceInNavigation extends PageRouteBuilder {
  final Widget widget;

  BounceInNavigation({required this.widget})
      : super(
      transitionDuration: Duration(milliseconds: 270),
      transitionsBuilder: (BuildContext con, Animation<double> animation,
          Animation<double> secAnimation, Widget child) {
        animation = CurvedAnimation(
            parent: animation, curve: Curves.easeInOutBack);
        return ScaleTransition(
          scale: animation,
          alignment: Alignment.topCenter,
          child: child,
        );

      },
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secanimation) {
        return widget;
      });
}