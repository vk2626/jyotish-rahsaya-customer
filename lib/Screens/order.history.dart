import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Core/Provider/list.of.api.provider.dart';
import 'package:jyotish_rahsaya/SubScreens/order_pooja_list_screen.dart';
import 'package:jyotish_rahsaya/dialog/showLoaderDialog.dart';
import 'package:provider/provider.dart';
import 'reports.screen.dart';
import 'walletScreens/wallet.transaction.screen.dart';

import 'OrderScreen/OrderListScreen.dart';
import 'mallScreens/remedies_screen.dart';

class OrderHistoryScreen extends StatefulWidget {
  int selected_index = 0;

  OrderHistoryScreen({required this.selected_index});

  @override
  State<OrderHistoryScreen> createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  var currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 5);
    setState(() {
      print(widget.selected_index);
      currentIndex = widget.selected_index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.selected_index,
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Order History".tr(),
            style: GoogleFonts.poppins(fontSize: 18),
          ),
          bottom: TabBar(
              physics: NeverScrollableScrollPhysics(),
              isScrollable: true,
              labelStyle: GoogleFonts.poppins(
                  fontSize: 14, // Adjust font size
                  fontWeight: FontWeight.w500), // Adjust font weight),
              labelColor: Colors.black,
              unselectedLabelColor: Colors.grey.shade600,
              indicatorColor: Colors.black,
              controller: _tabController!..index = widget.selected_index,
              tabAlignment: TabAlignment.start,
              tabs: [
                Tab(text: "Wallet".tr()),
                Tab(text: "Order".tr()),
                Tab(text: "Remedies".tr()),
                Tab(text: "Poojas".tr()),
                Tab(text: "Report".tr()),
              ]),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListenableProvider(
                  create: (context) => ListOfApisProvider(),
                  child: WalletTransactionScreen()),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: OrderListScreen(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: RemediesScreen(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: OrderPoojaListScreen(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ReportScreen(),
            ),
          ],
        ),
      ),
    );
  }
}
