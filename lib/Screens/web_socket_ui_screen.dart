// import 'dart:developer';

// import 'package:flutter/material.dart';
// import 'package:jyotish_rahsaya/Core/logger_helper.dart';
// import 'package:jyotish_rahsaya/handler_classes/web_socket_handler.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';
// import 'dart:convert';

// class WebSocketUiScreen extends StatefulWidget {
//   const WebSocketUiScreen({super.key});

//   @override
//   State<WebSocketUiScreen> createState() => _WebSocketUiScreenState();
// }

// class _WebSocketUiScreenState extends State<WebSocketUiScreen> {
//   // final String webSocketURL =
//   //     "wss://jyotish.techsaga.live:8080/app/abqvak4gc8wme3xjjq0a?protocol=7&client=js&version=8.4.0-rc2&flash=false";
//   final List<Message> messages = [];
//   late WebSocketHandler webSocketHandler;
//   // var channel;
//   @override
//   void initState() {
//     super.initState();
//     webSocketHandler = WebSocketHandler();
//     webSocketHandler.initializeWebSocket().catchError((error) {
//       // Handle any errors during initialization
//       print("Error initializing WebSocket: $error");
//     });
//     // channel = WebSocketChannel.connect(
//     //   Uri.parse(
//     //       "wss://jyotish.techsaga.live:8080/app/abqvak4gc8wme3xjjq0a?protocol=7&client=js&version=8.4.0-rc2&flash=false"),
//     // );
//   }

//   @override
//   void dispose() {
//     webSocketHandler.closeConnection(); // Close WebSocket connection
//     super.dispose();
//   }

//   void _sendMessage(String text) {
//     if (text.isNotEmpty) {
//       // Create a new message object
//       Message newMessage = Message(text: text, isSent: true);
//       setState(() {
//         messages.add(newMessage); // Add the message to the list
//       });
//       // Send the message through WebSocket
//       // webSocketHandler.sendMessage(text, "1521");
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         appBar: AppBar(
//           title: Text('Chat'),
//         ),
//         body: Column(
//           children: [
//             Expanded(
//                 child: StreamBuilder(
//               stream: webSocketHandler.uiResponder,
//               builder: (context, snapshot) {
//                 if (snapshot.hasData) {
//                   // Process incoming messages
//                   final message = snapshot.data as String;
//                   processIncomingMessage(message);
//                   // Use Future.microtask to defer the state update
//                   // Future.microtask(() {
//                   log(message);
//                   // setState(() {
//                   //   messages.add(Message(text: message, isSent: false));
//                   // });
//                   // });
//                 }

//                 return ListView.builder(
//                   itemCount: messages.length,
//                   itemBuilder: (context, index) {
//                     final message = messages[index];
//                     return Align(
//                       alignment: message.isSent
//                           ? Alignment.centerRight
//                           : Alignment.centerLeft,
//                       child: Container(
//                         margin:
//                             EdgeInsets.symmetric(vertical: 5, horizontal: 10),
//                         padding: EdgeInsets.all(10),
//                         decoration: BoxDecoration(
//                           color: message.isSent
//                               ? Colors.blue
//                               : Colors.grey.shade300,
//                           borderRadius: BorderRadius.circular(10),
//                         ),
//                         child: Text(
//                           message.text,
//                           style: TextStyle(
//                             color: message.isSent ? Colors.white : Colors.black,
//                           ),
//                         ),
//                       ),
//                     );
//                   },
//                 );
//               },
//             )),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: Row(
//                 children: [
//                   Expanded(
//                     child: TextField(
//                       onSubmitted: _sendMessage,
//                       decoration: InputDecoration(
//                         hintText: 'Type a message',
//                         border: OutlineInputBorder(),
//                       ),
//                     ),
//                   ),
//                   // IconButton(
//                   //   icon: Icon(Icons.send),
//                   //   onPressed: _se,
//                   // ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   // Function to process the incoming WebSocket message
//   void processIncomingMessage(String message) async {
//     try {
//       // Decode the incoming message
//       final decodedMessage = jsonDecode(message);

//       // Check if the event type matches what you are interested in
//       // if (decodedMessage['event'] == "App\\Events\\MessageSent2") {
//       // Decode the message data which is a JSON string
//       final messageData = jsonDecode(decodedMessage['data']);

//       // Check for a valid status and message presence

//       TLoggerHelper.debug("Something is received!! $decodedMessage");

//       // Create a new Message object
//       final newMessage = Message(
//         text: messageData['message']['message'].toString(),
//         isSent: false, // Assume received messages are not sent by this user
//       );

//       // Use setState to add the new message to the list

//       setState(() {
//         messages.add(newMessage);
//       });
//       // }
//     } catch (e) {
//       TLoggerHelper.error("Error processing incoming message: $e");
//     }
//   }
// }

// class Message {
//   final String text;
//   final bool isSent;

//   Message({required this.text, required this.isSent});
// }
