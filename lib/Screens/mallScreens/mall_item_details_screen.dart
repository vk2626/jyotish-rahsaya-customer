import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_markdown/flutter_markdown.dart';
import '../../Core/Api/list.of.apis.dart';
import '../../Core/Model/product_details_model_new.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../Core/formatter.dart';
import '../../Core/logger_helper.dart';
import '../../Core/nav/SlideRightRoute.dart';
import 'select.mall.items.screen.dart';
import '../../dialog/showLoaderDialog.dart';
import '../../utils/AppColor.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../../utils/custom_circular_progress_indicator.dart';

class MallItemDetailsScreen extends StatefulWidget {
  final String remedyId;
  final String productName;
  const MallItemDetailsScreen(
      {super.key, required this.remedyId, required this.productName});

  @override
  State<MallItemDetailsScreen> createState() => _MallItemDetailsScreenState();
}

class _MallItemDetailsScreenState extends State<MallItemDetailsScreen> {
  late String remedyId;
  late String productName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          TFormatter.capitalizeSentence(widget.productName),
          style: GoogleFonts.poppins(),
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          if (remedyId.isNotEmpty && productName.isNotEmpty) {
            Navigator.push(
              context,
              SlideRightRoute(
                page: SelectMallItems(
                  name: productName,
                  productId: remedyId,
                ),
              ),
            );
          }
        },
        child: Container(
          color: AppColor.appColor,
          height: 60,
          width: double.infinity,
          child: Center(
            child: Text(
              "Book Now".tr(),
              style: GoogleFonts.poppins(
                  fontSize: 16, fontWeight: FontWeight.w600),
            ),
          ),
        ),
      ),
      body: FutureBuilder(
        future: Provider.of<ListOfApisProvider>(context, listen: false)
            .getProductDetailsNew(productId: widget.remedyId),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CustomCircularProgressIndicator());
          } else if (snapshot.hasData) {
            ProductDetailsModelNew product = snapshot.data;
            var data = product.data.first;

            // Store the productId and productName
            remedyId = data.id.toString();
            productName = data.title;
            TLoggerHelper.debug("$remedyId Product Name:-- $productName");
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 220,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        border: Border.all(color: AppColor.darkGrey),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(7.5),
                        child: CachedNetworkImage(
                          imageUrl: data.image,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      data.title,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 15.5,
                          color: Colors.black),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          data.shortDescription,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.poppins(
                              color: Colors.grey.shade700, fontSize: 14.5),
                        ),
                        SizedBox(height: 24),
                        Text(
                          "Starting from".tr() + ":₹ ${data.price}/-",
                          style: GoogleFonts.poppins(
                              color: AppColor.blackColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        SizedBox(height: 15),
                        ElevatedButton(
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side: const BorderSide(
                                            color: AppColor.darkGrey))),
                                backgroundColor:
                                    MaterialStatePropertyAll<Color>(
                                        AppColor.darkGrey)),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SelectMallItems(
                                          name: data.title,
                                          productId: remedyId.toString())));
                            },
                            child: Text(
                              "Book Now".tr(),
                              style: GoogleFonts.poppins(
                                  color: Colors.white, fontSize: 12),
                            )),
                        SizedBox(height: 40),
                        Visibility(
                          visible: true,
                          child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: data.heading.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(8),
                                  margin: EdgeInsets.symmetric(vertical: 4),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border:
                                        Border.all(color: Colors.grey.shade400),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        data.heading[index].heading,
                                        style: GoogleFonts.poppins(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                      SizedBox(height: 1),
                                      MarkdownBody(
                                          data: html2md.convert(data
                                              .heading[index].description
                                              .toString()))
                                    ],
                                  ),
                                );
                              }),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                ],
              ),
            );
          } else {
            return SizedBox.shrink();
          }
        },
      ),
    );
  }
}
