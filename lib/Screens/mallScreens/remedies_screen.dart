import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../../Core/Provider/chatting_provider.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../Core/helper_functions.dart';
import '../../Core/nav/SlideRightRoute.dart';
import 'mall_item_details_screen.dart';
import '../payment.info.dart';
import '../../dialog/showLoaderDialog.dart';
import '../../pooja_screens/pooja_chat_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../Core/Api/ApiServices.dart';
import '../../Core/Api/Constants.dart';
import '../../Core/Model/AstroMallOrdersModel.dart';
import '../../Core/Model/customer_remedy_model.dart';
import '../../Core/Model/remedy_purchased_model.dart';
import '../../utils/AppColor.dart';
import '../../utils/custom_circular_progress_indicator.dart';

class RemediesScreen extends StatefulWidget {
  const RemediesScreen({super.key});

  @override
  State createState() => _RemediesScreenState();
}

class _RemediesScreenState extends State<RemediesScreen> {
  bool visibleOrder = false;
  int currentIndex = 0;
  int customerId = 0;
  List list = [];
  List customerList = [];
  List customerPurchasedList = [];
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    getUserId();
    THelperFunctions.logScreenNameEvent(screenName: "remedies_screen");
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    customerId = prefs.getInt(Constants.userID) ?? 0;
    print("customerId--> $customerId");
    getOrderList();
    getRemedyList();
    getCustomerPurchasedList();
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildButton("All".tr(), 0),
                buildButton("Suggested".tr(), 1),
                buildButton("Purchased".tr(), 2),
              ],
            ),
            SizedBox(height: 10),
            if (isLoading)
              Expanded(child: Center(child: CustomCircularProgressIndicator()))
            else
              Expanded(child: buildContent()),
          ],
        ),
      ),
    );
  }

  Widget buildButton(String title, int index) {
    return SizedBox(
      height: 30,
      child: ElevatedButton(
        style: ButtonStyle(
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
                side: BorderSide(
                    color: currentIndex == index
                        ? AppColor.appColor
                        : AppColor.borderColor)),
          ),
          backgroundColor: WidgetStateProperty.all(
              currentIndex == index ? AppColor.appColor : AppColor.ltGrey),
        ),
        onPressed: () {
          setState(() {
            visibleOrder = index == 0;
            currentIndex = index;
          });
          if (currentIndex == 0) {
            getOrderList();
          } else if (currentIndex == 1) {
            getRemedyList();
          } else if (currentIndex == 2) {
            getCustomerPurchasedList();
          }
        },
        child: Text(title, style: GoogleFonts.poppins(color: Colors.black)),
      ),
    );
  }

  Widget buildContent() {
    switch (currentIndex) {
      case 0:
        return buildOrderList();
      case 1:
        return buildSuggestedList();
      case 2:
        return buildPurchasedList();
      default:
        return Container();
    }
  }

  Widget buildOrderList() {
    if (list.isEmpty) {
      return Center(
          child: Text("No order found".tr(),
              style: GoogleFonts.poppins(
                  fontWeight: FontWeight.bold, fontSize: 16)));
    }
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: List.generate(
          list.length,
          (i) => buildOrderCard(list[i]),
        ),
      ),
    );
  }

  Widget buildSuggestedList() {
    if (customerList.isEmpty) {
      return Center(
          child: Text("No Remedies Available".tr(),
              style: GoogleFonts.poppins(
                  fontWeight: FontWeight.bold, fontSize: 16)));
    }
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: List.generate(
          customerList.length,
          (ic) => buildSuggestedCard(customerList[ic]),
        ),
      ),
    );
  }

  Widget buildPurchasedList() {
    if (customerPurchasedList.isEmpty) {
      return Center(
          child: Text("No Purchased Items".tr(),
              style: GoogleFonts.poppins(
                  fontWeight: FontWeight.bold, fontSize: 16)));
    }
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: List.generate(
          customerPurchasedList.length,
          (indx) => buildPurchasedCard(customerPurchasedList[indx]),
        ),
      ),
    );
  }

  Widget buildOrderCard(order) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(SlideRightRoute(
            page: ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: MallItemDetailsScreen(
            remedyId: order!.remedyId.toString(),
            // remedyId: order!.remedyId.toString(),
            productName: order.productTitle,
          ),
        )));
      },
      child: Card(
        color: Colors.white,
        elevation: 2,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 35,
                backgroundImage: CachedNetworkImageProvider(order.productImage),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("OrderId".tr() + " : #${order.id}",
                        style: GoogleFonts.poppins(
                            fontSize: 12, fontWeight: FontWeight.bold)),
                    Text("Product".tr() + " : ${order.productTitle}",
                        style: GoogleFonts.poppins(fontSize: 14)),
                    Text("Astrologer".tr() + " : ${order.suggestedAstroName}",
                        style: GoogleFonts.poppins(fontSize: 14)),
                    Text(
                        "Price".tr() +
                            " : ₹ ${order.amount} (GST. ${order.gstAmount})",
                        style: GoogleFonts.poppins(
                            fontSize: 14, color: Colors.grey[800])),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSuggestedCard(suggested) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(SlideRightRoute(
            page: ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: MallItemDetailsScreen(
            remedyId: suggested.remedyId.toString(),
            productName: suggested.productTitle,
          ),
        )));
      },
      child: Card(
        color: Colors.white,
        elevation: 2,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 35,
                backgroundImage:
                    CachedNetworkImageProvider(suggested.productImage),
              ),
              SizedBox(width: 12),
              SizedBox(
                width: MediaQuery.of(context).size.width * .35,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("OrderId".tr() + " : #${suggested.orderId}",
                        style: GoogleFonts.poppins(
                            fontSize: 12, fontWeight: FontWeight.bold)),
                    Text("Category".tr() + " : ${suggested.categoryName}",
                        style: GoogleFonts.poppins(fontSize: 14)),
                    Text("Product".tr() + " : ${suggested.productTitle}",
                        style: GoogleFonts.poppins(fontSize: 14)),
                    Text(
                        "Performed by".tr() +
                            " : ${suggested.suggestedAstroName}",
                        style: GoogleFonts.poppins(fontSize: 14)),
                    Text("Price".tr() + " : ₹ ${suggested.amount}",
                        style: GoogleFonts.poppins(
                            fontSize: 14, color: Colors.grey[800])),
                  ],
                ),
              ),
              if (suggested.type != "free own") Spacer(),
              if (suggested.type != "free own")
                ElevatedButton(
                  onPressed: () async {
                    Navigator.of(context)
                        .push(
                          SlideRightRoute(
                              page: ListenableProvider(
                                  create: (context) => ListOfApisProvider(),
                                  child: PaymentInformationScreen(
                                      userInfoId: suggested.infoId.toString(),
                                      title: suggested.productTitle,
                                      astroId: suggested.astrologerId,
                                      astroName:
                                          await THelperFunctions.getUserName(),
                                      price: suggested.amount,
                                      amountID: 0,
                                      from: 'mall',
                                      isProductOrPooja: true,
                                      remedy_id: suggested.remedyId.toString(),
                                      suggested_astro_id:
                                          suggested.suggestedAstroId,
                                      originalProductId:
                                          suggested.id.toString()))),
                        )
                        .whenComplete(() => getRemedyList());
                  },
                  child: Text(
                    "Book Now".tr(),
                    style: GoogleFonts.poppins(
                        color: Colors.black, fontWeight: FontWeight.w600),
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(AppColor.appColor),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPurchasedCard(purchased) {
    return Card(
      color: Colors.white,
      elevation: 2,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 35,
                  backgroundImage:
                      CachedNetworkImageProvider(purchased.productImage),
                ),
                SizedBox(
                  width: 12,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("OrderId".tr() + " : #${purchased.remedyOrderId}",
                          style: GoogleFonts.poppins(
                              fontSize: 12, fontWeight: FontWeight.bold)),
                      Text("Category".tr() + " : ${purchased.categoryName}",
                          style: GoogleFonts.poppins(fontSize: 14)),
                      Text("Product".tr() + " : ${purchased.productTitle}",
                          style: GoogleFonts.poppins(fontSize: 14)),
                      Text(
                          "Performed by".tr() +
                              " : ${purchased.suggestedAstroName}",
                          style: GoogleFonts.poppins(fontSize: 14)),
                      Text(
                          "Price".tr() +
                              " : ₹ ${purchased.amount} (GST. ${purchased.gstAmount})",
                          style: GoogleFonts.poppins(
                              fontSize: 14, color: Colors.grey[800])),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                if (purchased.orderStatus >= 1)
                  ElevatedButton(
                    onPressed: () async {
                      Navigator.of(context)
                          .push(MaterialPageRoute(
                              builder: (context) => ListenableProvider(
                                  create: (context) => ChattingProvider(),
                                  child: PoojaChatScreen(
                                      astrologerName:
                                          purchased.suggestedAstroName,
                                      fromId: purchased.userId,
                                      toId: purchased.suggestedAstroId,
                                      isToViewOnly: purchased.orderStatus != 1,
                                      orderId:
                                          purchased.remedyOrderId.toString(),
                                      isPooja: false,
                                      poojaName: purchased.productTitle))))
                          .whenComplete(() => getCustomerPurchasedList());
                    },
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(AppColor.appColor),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                    child: Text(
                      'Chat'.tr(),
                      style: GoogleFonts.poppins(
                          color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                  ),
              ],
            ),
            if (purchased.orderStatus == 2 || purchased.orderStatus == 3)
              Column(
                children: [
                  SizedBox(height: 8),
                  Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: Colors.grey.shade200,
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(8)),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Order Completed"),
                        SizedBox(
                          width: 4,
                        ),
                        Icon(Icons.check_circle_rounded)
                      ],
                    ),
                  ),
                ],
              )
          ],
        ),
      ),
    );
  }

  Future<void> getOrderList() async {
    DioClient client = DioClient();
    Dio dio = await client.getClient();
    var params = jsonEncode({'user_id': customerId});
    AstroMallOrdersModel? astroMallOrdersModel = await client.astroMallOrders(
        dio, UrlConstants.baseUrl + UrlConstants.astroMallOrder, params);
    if (astroMallOrdersModel.status == true) {
      setState(() {
        list = astroMallOrdersModel.productList;
      });
    }
  }

  Future<void> getRemedyList() async {
    DioClient client = DioClient();
    Dio dio = await client.getClient();
    var params = jsonEncode({'user_id': customerId});
    CustomerRemedyModel? customerRemedyModel = await client.customerRemedy(
        dio, UrlConstants.baseUrl + UrlConstants.remedyDetails, params);
    if (customerRemedyModel.status == true) {
      setState(() {
        customerList = customerRemedyModel.customer;
      });
    }
  }

  Future<void> getCustomerPurchasedList() async {
    DioClient client = DioClient();
    Dio dio = await client.getClient();
    var params = jsonEncode({'user_id': customerId});
    CustomerRemedyPurchasedListModel? customerPurchasedRemedyModel =
        await client.customerPurchasedRemedy(dio,
            UrlConstants.baseUrl + UrlConstants.remedyPurchasedDetails, params);
    if (customerPurchasedRemedyModel.status == true) {
      setState(() {
        customerPurchasedList = customerPurchasedRemedyModel.productList;
      });
    }
  }
}
