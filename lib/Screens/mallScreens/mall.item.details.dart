// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'package:google_fonts/google_fonts.dart';
// import '../../Core/Model/RecentSearchModel.dart' as recentSearch;
// import '../../Core/Model/SearchModel.dart' as Search;
// import '../../Core/Model/jyotish.mall.detail.model.dart';
// import '../../CustomNavigator/SlideRightRoute.dart';
// import 'select.mall.items.screen.dart';
// import '../../utils/AppColor.dart';
// import 'package:html2md/html2md.dart' as html2md;
// import 'package:flutter_markdown/flutter_markdown.dart';

// class MallProductDetails extends StatefulWidget {
//   const MallProductDetails({
//     super.key,
//     required this.id,
//     this.headingList,
//     required this.name,
//     required this.img,
//     required this.price,
//     required this.shortDetail,
//     this.headingListRecentSearch,
//     this.headingSearch,
//   });

//   final String id;
//   final List<Heading>? headingList;
//   final List<recentSearch.Heading>? headingListRecentSearch;
//   final List<Search.Heading>? headingSearch;

//   final String name;
//   final String img;
//   final String price;
//   final String shortDetail;

//   @override
//   State<MallProductDetails> createState() => _MallProductDetailsState();
// }

// class _MallProductDetailsState extends State<MallProductDetails> {
//   ScrollController scrollViewController = ScrollController();

//   bool isVisible = false;

//   @override
//   void initState() {
//     super.initState();
//     scrollViewController.addListener(() {
//       setState(() {
//         isVisible =
//             scrollViewController.offset > 395; // Adjust this value as needed
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context0) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         title: Text(widget.name),
//       ),
//       bottomNavigationBar: GestureDetector(
//         onTap: () {
//           Navigator.of(context).push(SlideRightRoute(
//               page: SelectMallItems(
//                   name: widget.name.toString(),
//                   productId: widget.id.toString())));
//           print("Book Now");
//         },
//         child: Visibility(
//           visible: isVisible,
//           child: Container(
//               color: AppColor.appColor,
//               height: 50,
//               width: double.infinity,
//               child: Center(
//                   child: Text(
//                 "Book Now",
//                 style: GoogleFonts.poppins(
//                     fontSize: 14, fontWeight: FontWeight.bold),
//               ))),
//         ),
//       ),
//       body: NotificationListener<ScrollNotification>(
//         onNotification: (ScrollNotification scrollInfo) {
//           scrollViewController..notifyListeners();
//           return false;
//         },
//         child: LayoutBuilder(builder: (context, BoxConstraints constraints) {
//           return SingleChildScrollView(
//             controller: scrollViewController,
//             child: Column(
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Container(
//                     height: 220,
//                     width: double.infinity,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.all(Radius.circular(14)),
//                       border: Border.all(color: AppColor.darkGrey),
//                     ),
//                     child: Padding(
//                       padding: const EdgeInsets.all(7.5),
//                       child: CachedNetworkImage(
//                         imageUrl: "${widget.img.toString()}",
//                         fit: BoxFit.cover,
//                       ),
//                     ),
//                   ),
//                 ),
//                 ListTile(
//                   title: Text(
//                     "${widget.name.toString()}",
//                     style: GoogleFonts.poppins(
//                         fontWeight: FontWeight.bold,
//                         fontSize: 15.5,
//                         color: Colors.black),
//                   ),
//                   subtitle: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         "${widget.shortDetail.toString()}",
//                         maxLines: 2,
//                         overflow: TextOverflow.ellipsis,
//                         style: GoogleFonts.poppins(
//                             color: Colors.grey.shade700, fontSize: 14.5),
//                       ),
//                       SizedBox(height: 24),
//                       Text(
//                         "Starting from:₹ ${widget.price.toString()}/-",
//                         style: GoogleFonts.poppins(
//                             color: AppColor.blackColor,
//                             fontWeight: FontWeight.bold,
//                             fontSize: 14),
//                       ),
//                       SizedBox(height: 15),
//                       ElevatedButton(
//                           style: ButtonStyle(
//                               shape: MaterialStateProperty.all<
//                                       RoundedRectangleBorder>(
//                                   RoundedRectangleBorder(
//                                       borderRadius: BorderRadius.circular(10.0),
//                                       side: const BorderSide(
//                                           color: AppColor.darkGrey))),
//                               backgroundColor: MaterialStatePropertyAll<Color>(
//                                   AppColor.darkGrey)),
//                           onPressed: () {
//                             Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) => SelectMallItems(
//                                         name: widget.name.toString(),
//                                         productId: widget.id.toString())));

//                             setState(() {});
//                           },
//                           child: Text(
//                             "Book Now",
//                             style: GoogleFonts.poppins(
//                                 color: Colors.white, fontSize: 12),
//                           )),
//                       SizedBox(height: 40),
//                       Visibility(
//                         visible: widget.headingList!.isNotEmpty,
//                         child: ListView.builder(
//                             physics: NeverScrollableScrollPhysics(),
//                             shrinkWrap: true,
//                             itemCount: widget.headingList!.length,
//                             itemBuilder: (context, index) {
//                               return Container(
//                                 width: double.infinity,
//                                 padding: EdgeInsets.all(8),
//                                 margin: EdgeInsets.symmetric(vertical: 4),
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border:
//                                       Border.all(color: Colors.grey.shade400),
//                                 ),
//                                 child: Column(
//                                   mainAxisAlignment: MainAxisAlignment.start,
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: <Widget>[
//                                     Text(
//                                       "${widget.headingList![index].heading.toString()}",
//                                       style: GoogleFonts.poppins(
//                                           color: Colors.black,
//                                           fontWeight: FontWeight.bold,
//                                           fontSize: 14),
//                                     ),
//                                     SizedBox(height: 1),
//                                     MarkdownBody(
//                                         data: html2md.convert(widget
//                                             .headingList![index].description
//                                             .toString()))
//                                   ],
//                                 ),
//                               );
//                             }),
//                       ),
//                       Visibility(
//                         visible: widget.headingListRecentSearch!.isNotEmpty,
//                         child: ListView.builder(
//                             physics: NeverScrollableScrollPhysics(),
//                             shrinkWrap: true,
//                             itemCount: widget.headingListRecentSearch!.length,
//                             itemBuilder: (context, index) {
//                               return Container(
//                                 width: double.infinity,
//                                 padding: EdgeInsets.all(8),
//                                 margin: EdgeInsets.symmetric(vertical: 4),
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border:
//                                       Border.all(color: Colors.grey.shade400),
//                                 ),
//                                 child: Column(
//                                   mainAxisAlignment: MainAxisAlignment.start,
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: <Widget>[
//                                     Text(
//                                       "${widget.headingListRecentSearch![index].heading.toString()}",
//                                       style: GoogleFonts.poppins(
//                                           color: Colors.black,
//                                           fontWeight: FontWeight.bold,
//                                           fontSize: 14),
//                                     ),
//                                     SizedBox(height: 1),
//                                     MarkdownBody(
//                                         data: html2md.convert(widget
//                                             .headingListRecentSearch![index]
//                                             .description
//                                             .toString()))
//                                   ],
//                                 ),
//                               );
//                             }),
//                       ),
//                       Visibility(
//                         visible: widget.headingSearch!.isNotEmpty,
//                         child: ListView.builder(
//                             physics: NeverScrollableScrollPhysics(),
//                             shrinkWrap: true,
//                             itemCount: widget.headingSearch!.length,
//                             itemBuilder: (context, index) {
//                               return Container(
//                                 width: double.infinity,
//                                 padding: EdgeInsets.all(8),
//                                 margin: EdgeInsets.symmetric(vertical: 4),
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border:
//                                       Border.all(color: Colors.grey.shade400),
//                                 ),
//                                 child: Column(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   children: <Widget>[
//                                     Text(
//                                       "${widget.headingSearch![index].heading.toString()}",
//                                       style: GoogleFonts.poppins(
//                                           color: Colors.black,
//                                           fontWeight: FontWeight.bold,
//                                           fontSize: 17),
//                                     ),
//                                     SizedBox(height: 1),
//                                     MarkdownBody(
//                                         data: html2md.convert(widget
//                                             .headingSearch![index].description
//                                             .toString()))
//                                   ],
//                                 ),
//                               );
//                             }),
//                       ),
//                       SizedBox(height: 20),
//                       // Center(
//                       //   child: Text(
//                       //     "Customer Testimonial",
//                       //     style: GoogleFonts.poppins(
//                       //         fontWeight: FontWeight.bold, fontSize: 14.5),
//                       //   ),
//                       // ),
//                       // SizedBox(height: 10),
//                       // ListView.builder(
//                       //     physics: NeverScrollableScrollPhysics(),
//                       //     shrinkWrap: true,
//                       //     itemCount: 2,
//                       //     itemBuilder: (context, index) {
//                       //       return Card(
//                       //         child: Container(
//                       //           child: ListTile(
//                       //             title: Row(
//                       //               mainAxisAlignment:
//                       //                   MainAxisAlignment.spaceBetween,
//                       //               children: <Widget>[
//                       //                 customUserList(
//                       //                     "asset/images/person.png", "Akash"),
//                       //                 Icon(Icons.more_vert_rounded),
//                       //               ],
//                       //             ),
//                       //             subtitle: Column(
//                       //               children: <Widget>[
//                       //                 Row(
//                       //                   children: <Widget>[
//                       //                     RatingBarIndicator(
//                       //                         itemCount: 5,
//                       //                         itemSize: 15.0,
//                       //                         itemBuilder: (context, _) =>
//                       //                             const Icon(
//                       //                               Icons.star,
//                       //                             )),
//                       //                     SizedBox(width: 5),
//                       //                     Text("08 Jan 2024")
//                       //                   ],
//                       //                 ),
//                       //                 Text(
//                       //                     "During a typical development cycle, you test an app using flutter run at the command line, or by using the Run and Debug options in your IDE. By default, Flutter builds a debug version of your app.When you’re ready to prepare a release version of your app, for example to publish to the Google Play Store, this page can help. Before publishing, you might want to put some finishing touches on your app. This page covers the following topics:"),
//                       //               ],
//                       //             ),
//                       //           ),
//                       //         ),
//                       //       );
//                       //     }),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           );
//         }),
//       ),
//     );
//   }

//   Widget customUserList(String imgVal, String text) {
//     return Container(
//       child: Row(
//         children: <Widget>[
//           Padding(
//             padding: const EdgeInsets.all(2.0),
//             child: CircleAvatar(
//               radius: 20.0,
//               backgroundImage: AssetImage(
//                 imgVal,
//               ),
//             ),
//           ),
//           SizedBox(width: 10),
//           Text(
//             text,
//             style: GoogleFonts.poppins(
//               fontSize: 19.0,
//               color: Colors.black,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
