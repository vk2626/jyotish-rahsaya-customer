import 'dart:developer';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../Core/formatter.dart';
import 'mall_item_details_screen.dart';
import '../../dialog/showLoaderDialog.dart';
import '../../Core/logger_helper.dart';
import '../../Core/Model/jyotish.mall.detail.model.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../CustomNavigator/SlideRightRoute.dart';
import 'package:provider/provider.dart';

import '../../utils/custom_circular_progress_indicator.dart';
import 'mall.item.details.dart';

class MallItemsScreen extends StatefulWidget {
  final String suggestedType;
  final String categoryId;
  final String productName;
  final String productImg;

  const MallItemsScreen(
      {super.key,
      required this.suggestedType,
      required this.categoryId,
      required this.productImg,
      required this.productName});

  @override
  State<MallItemsScreen> createState() => _MallItemsScreenState();
}

class _MallItemsScreenState extends State<MallItemsScreen> {
  @override
  void initState() {
    Provider.of<ListOfApisProvider>(context, listen: false)
        .getMallProductSliderProvider(context: context, id: widget.categoryId);
    // WidgetsBinding.instance.addPostFrameCallback((_) {});4
    super.initState();
  }

  List<Datum> filterMallDetailData = [];
  List<Datum> mallDetailData = [];

  TextEditingController jyotishMallItemTextController = TextEditingController();
  var filled = false;

  @override
  Widget build(BuildContext context) {
    print("Build Here With Category ID ---> ${widget.categoryId}");
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            title: Text(
          TFormatter.capitalizeSentence(widget.productName),
        )),
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                /* Slider Data Consumer */
                Consumer<ListOfApisProvider>(
                    builder: (context, sliderState, _) {
                  var mallProductSliderData = sliderState.mallPSlider.data;

                  if (mallProductSliderData == null) {
                    return Text("");
                  }
                  print(
                      "Mall Product Slider Data ID1 --------> ${mallProductSliderData.length}");
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 200,
                      width: 400,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10)),
                      child: CarouselSlider.builder(
                        itemCount: mallProductSliderData.isNotEmpty
                            ? mallProductSliderData.length
                            : 0,
                        itemBuilder: (BuildContext context, int itemIndex,
                                int pageViewIndex) =>
                            Card(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          child: CachedNetworkImage(
                            imageUrl:
                                "${mallProductSliderData[itemIndex].image.toString()}",
                            fit: BoxFit.fill,
                            width: double.infinity,
                          ),
                        ),
                        options: CarouselOptions(
                          autoPlay: true,
                          enlargeCenterPage: true,
                          initialPage: 0,
                        ),
                      ),
                    ),
                  );
                }),
                /* Mall Detail Consumer */
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.shade600.withOpacity(.5),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(2, 2))
                      ],
                      borderRadius: BorderRadius.circular(12)),
                  child: TextField(
                    controller: jyotishMallItemTextController,
                    onChanged: (value) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(12)),
                        prefixIcon: Icon(Icons.search),
                        filled: true,
                        fillColor: Colors.white,
                        hintText: "Search product items".tr()+"...",
                        hintStyle: GoogleFonts.poppins(),
                        contentPadding: EdgeInsets.zero),
                  ),
                ),
                SizedBox(height: 10),
                Consumer<ListOfApisProvider>(
                  builder: (context, provider, child) {
                    return FutureBuilder(
                      future: provider.getJyotishMallDetailProvider(
                          context: context, categoryId: widget.categoryId,suggestedType: widget.suggestedType),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CustomCircularProgressIndicator(),
                          );
                        } else if (snapshot.hasData) {
                          if (snapshot.data!.data!.isEmpty) {
                            return Center(
                              child: Text("No Data Available".tr()),
                            );
                          } else {
                            TLoggerHelper.debug("Coming here !!");
                            JyotishMallDetailModel jyotishMallDetail =
                                snapshot.data!;
                            mallDetailData = jyotishMallDetail.data!;
                            filterMallDetailData = mallDetailData
                                .where((element) => element.title!
                                    .toLowerCase()
                                    .contains(jyotishMallItemTextController.text
                                        .toLowerCase()))
                                .toList();
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GridView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: filterMallDetailData.length,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          mainAxisSpacing: 0,
                                          crossAxisSpacing: 0),
                                  itemBuilder: (context, index) {
                                    log("This is to check the mall.item screen gridview which isn't updating at all " +
                                        mallDetailData.length.toString());
                                    return InkWell(
                                      onTap: () {
                                        Navigator.of(context)
                                            .push(SlideRightRoute(
                                                page: ListenableProvider(
                                          create: (context) =>
                                              ListOfApisProvider(),
                                          child: MallItemDetailsScreen(
                                            remedyId: mallDetailData[index]
                                                .id
                                                .toString(),
                                            productName: mallDetailData[index]
                                                .title
                                                .toString(),
                                          ),
                                        )));
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        height: 200,
                                        width: 200,
                                        child: Card(
                                          elevation: 2,
                                          clipBehavior:
                                              Clip.antiAliasWithSaveLayer,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0))),
                                          child: Stack(
                                            children: [
                                              Positioned(
                                                child: CachedNetworkImage(
                                                  fit: BoxFit.cover,
                                                  imageUrl:
                                                      "${filterMallDetailData[index].image}",
                                                ),
                                                right: 0,
                                                left: 0,
                                                top: 0,
                                                bottom: 0,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 120),
                                                width: double.infinity,
                                                height: 100,
                                                child: ClipRect(
                                                  child: BackdropFilter(
                                                    filter: ImageFilter.blur(
                                                        sigmaX: 1.0,
                                                        sigmaY: 1.0),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                          gradient: LinearGradient(
                                                              begin: Alignment
                                                                  .bottomCenter,
                                                              end: Alignment
                                                                  .topCenter,
                                                              colors: [
                                                            Colors.black54
                                                                .withOpacity(1),
                                                            Colors.white70
                                                                .withOpacity(00)
                                                          ])),
                                                      height: 40,
                                                      child: Center(
                                                        child: Text(
                                                          "${filterMallDetailData[index].title}",
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  color: Colors
                                                                      .white),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  }),
                            );
                          }
                        } else {
                          TLoggerHelper.error("Error building widget");
                          return SizedBox();
                        }
                      },
                    );
                  },
                )
              ],
            ),
          ),
        ));
  }
}
