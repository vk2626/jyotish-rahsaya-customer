import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../Core/formatter.dart';
import '../../utils/AppColor.dart';
import 'package:provider/provider.dart';
import '../../Core/Model/JyotishMallAstrologersModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Core/Api/Constants.dart';
import '../../Core/Api/list.of.apis.dart';
import '../../CustomNavigator/SlideRightRoute.dart';
import '../../dialog/showLoaderDialog.dart';
import '../payment.info.dart';

class SelectMallItems extends StatefulWidget {
  const SelectMallItems({
    super.key,
    required this.name,
    required this.productId,
  });

  final String name;
  final String productId;

  @override
  State<SelectMallItems> createState() => _SelectMallItemsState();
}

class _SelectMallItemsState extends State<SelectMallItems> {
  final ListOfApi listOfApi = ListOfApi();
  List<Astrologers> astrologersList = [];
  int customerID = 0;
  @override
  void initState() {
    super.initState();
    getUserId();
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;
    }
    getProductName();
  }

  void getProductName() async {
    showLoaderDialog(context);

    try {
      var userData = await listOfApi.getProductList(widget.productId);
      print(userData);
      var response = JyotishMallAstrologersModel.fromJson(jsonDecode(userData));
      setState(() {
        Navigator.pop(context);
        bool status = response.status;
        if (status) {
          setState(() {
            astrologersList = response.astrologers;
          });
          if (astrologersList.isEmpty) {
            Fluttertoast.showToast(msg: "No Astrologer");
            Navigator.of(context).pop();
          }
        } else {
          setState(() {
            astrologersList = [];
          });
        }
      });
    } on SocketException catch (_) {
      setState(() {});
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          TFormatter.capitalizeSentence(widget.name),
        ),
      ),
      body: ListView.builder(
          addAutomaticKeepAlives: true,
          cacheExtent: 99999,
          itemCount: astrologersList.length,
          itemBuilder: (context, index) {
            var resAstroList = astrologersList[index];
            return Card(
              color: Colors.white,
              elevation: 2,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Stack(
                children: [
                  Row(
                    children: [
                      SizedBox(width: 25),
                      Column(
                        children: [
                          SizedBox(height: 10),
                          Stack(
                            children: [
                              Card(
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: CircleAvatar(
                                  radius: 32,
                                  backgroundImage: CachedNetworkImageProvider(
                                      resAstroList.avatar),
                                ),
                                shape: CircleBorder(
                                  side: BorderSide(
                                      color: AppColor.appColor, width: 2),
                                ),
                              ),
                            ],
                          ),
                          RatingBarIndicator(
                              rating: resAstroList.astrologer.rating.toDouble(),
                              itemCount: 5,
                              itemSize: 15.0,
                              itemBuilder: (context, _) => const Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  )),
                          Text(
                              "${resAstroList.astrologer.orderCount} " +
                                  "orders".tr(),
                              style: GoogleFonts.poppins(
                                  color: Colors.grey[600], fontSize: 10),
                              textAlign: TextAlign.center),
                          SizedBox(height: 10),
                        ],
                      ),
                      SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 10),
                          Text(
                              "${TFormatter.capitalizeSentence(resAstroList.name)}",
                              style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600)),
                          Text(
                            "${resAstroList.astrologer.allSkillName}",
                            style: GoogleFonts.poppins(
                                color: Colors.grey[600], fontSize: 12),
                            maxLines: 2,
                          ),
                          Text(
                            resAstroList.astrologer.languageName,
                            style: GoogleFonts.poppins(
                                color: Colors.grey[600], fontSize: 12),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            "Exp".tr() +
                                " : ${resAstroList.astrologer.experienceYear} " +
                                "yr".tr(),
                            style: GoogleFonts.poppins(
                                color: Colors.grey[600], fontSize: 12),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "₹",
                                  style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[600],
                                    // Add line through original price
                                  ),
                                ),
                                TextSpan(
                                  text: (num.parse(resAstroList
                                                  .astrologer.discountPrice) <
                                              num.parse(resAstroList
                                                  .astrologer.price) &&
                                          (num.parse(resAstroList
                                                  .astrologer.discountPrice) !=
                                              0))
                                      ? resAstroList.astrologer.discountPrice
                                      : resAstroList.astrologer.price,
                                  style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[600],
                                  ),
                                ),
                              ],
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(height: 10),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).push(
                                  SlideRightRoute(
                                      page: ListenableProvider(
                                          create: (context) =>
                                              ListOfApisProvider(),
                                          child: PaymentInformationScreen(
                                            userInfoId: "",
                                              title: widget.name,
                                              astroName: astrologersList[index]
                                                  .name
                                                  .toString(),
                                              price: (num.parse(resAstroList.astrologer.discountPrice) <
                                                          num.parse(resAstroList
                                                              .astrologer
                                                              .price) &&
                                                      (num.parse(resAstroList
                                                              .astrologer
                                                              .discountPrice) !=
                                                          0))
                                                  ? resAstroList
                                                      .astrologer.discountPrice
                                                  : resAstroList.astrologer.price,
                                              amountID: 0,
                                              productID: widget.productId,
                                              isProductOrPooja: true,
                                              originalProductId: widget.productId,
                                              from: "mall",
                                              remedy_id: widget.productId,
                                              astroId: astrologersList[index].id.toString()))),
                                );
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18, vertical: 8),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(4),
                                    border: Border.all(color: Colors.green)),
                                child: Text(
                                  "Select",
                                  style:
                                      GoogleFonts.poppins(color: Colors.green),
                                ),
                              ),
                            )
                            // Padding(
                            //   child: ElevatedButton(
                            //     onPressed: () {
                            // Navigator.of(context).push(
                            //   SlideRightRoute(
                            //       page: ListenableProvider(
                            //           create: (context) =>
                            //               ListOfApisProvider(),
                            //           child: PaymentInformationScreen(
                            //               title: widget.name,
                            //               astroName:
                            //                   astrologersList[index]
                            //                       .name
                            //                       .toString(),
                            //               price: astrologersList[index]
                            //                   .astrologer!
                            //                   .price
                            //                   .toString(),
                            //               amountID: 0,
                            //               isProductOrPooja: true,
                            //               userID: "$customerID",
                            //               from: "mall",
                            //               astroId:
                            //                   astrologersList[index]
                            //                       .id
                            //                       .toString()))),
                            // );
                            //     },
                            //     style: ElevatedButton.styleFrom(
                            //       backgroundColor: Colors.white,
                            //       shape: RoundedRectangleBorder(
                            //         side: BorderSide(
                            //             color: Colors.greenAccent.shade700),
                            //         borderRadius: BorderRadius.circular(4.0),
                            //       ),
                            //     ),
                            //     child: Text(
                            //       "Select",
                            //       style: TextStyle(
                            //           color: Colors.greenAccent.shade700),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  if (resAstroList.label != 0)
                    Positioned(
                      left: -25, // Adjust the value to avoid overlapping
                      top: 15, // Adjust the value for proper positioning

                      child: Transform.rotate(
                        angle: -math.pi / 3.5,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color(
                                    0xFF3D3D3D), // Dark gray (closer to black)
                                Color(0xFF1C1C1C), // Almost black
                                Color(0xFF000000), // Pure black
                                Color(
                                    0xFF434343), // Slightly lighter for a metallic effect
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                            ),
                          ),
                          width: 100,
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                            vertical: 2,
                          ),
                          child: Text(
                            // resAstroList.astrologer.labelText,
                            "Celebrity",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: AppColor.whiteColor,
                              fontSize: 8,
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            );
          }),
    );
  }
}
