// import 'dart:developer' as developer;
// import 'dart:ui';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter/material.dart';
// import '../../Core/Model/jyotish.mall.model.dart';
// import '../../Core/Provider/list.of.api.provider.dart';
// import '../../CustomNavigator/SlideRightRoute.dart';
// import 'mall.item.dart';
// import 'mall.item.details.dart';
// import '../jyotish_mall/pooja_product_detail_screen.dart';
// import '../../utils/AppColor.dart';
// import 'package:provider/provider.dart';
// import 'package:transparent_image/transparent_image.dart';

// class AstroMallViewAllScreen extends StatefulWidget {
//   final IntCallback? onChangedItem;

//   AstroMallViewAllScreen({super.key, this.onChangedItem});

//   @override
//   State<AstroMallViewAllScreen> createState() => _AstroMallViewAllScreenState();
// }

// typedef void IntCallback(int value);

// class _AstroMallViewAllScreenState extends State<AstroMallViewAllScreen> {
//   List<Datum> mallData = [];
//   List<Datum> filteredMallData = [];
//   var filled = false;

//   TextEditingController jyotishMallText = TextEditingController();

//   @override
//   void initState() {
//     super.initState();
//     Provider.of<ListOfApisProvider>(context, listen: false)
//         .getJyotishMallProvider(context: context, customText: "");
//     Provider.of<ListOfApisProvider>(context, listen: false)
//         .getSpellSliderProvider(context: context, id: '0');

//     Provider.of<ListOfApisProvider>(context, listen: false)
//         .getNewlyLaunchedProvider(context: context);
//   }

//   @override
//   Widget build(BuildContext context) {
//     developer.log("Build is running in speed !!!!");
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Jyotish Mall"),
//       ),
//       body:
//           Consumer3<ListOfApisProvider, ListOfApisProvider, ListOfApisProvider>(
//               builder: (context, state, state1, state2, _) {
//         if (state.isLoading || state1.isLoading || state2.isLoading) {
//           return Center(child: CustomCircularProgressIndicator());
//         }

//         mallData = state.jyotishModel.data ?? [];

//         if (filled == false) {
//           filteredMallData = mallData;
//           filled = true;
//         }
//         var spellSliderData = state1.spellSlider.data ?? [];

//         var newlyLaunchedData = state2.newLaunchModel.data;
//         var newLaunchStatus = state2.newLaunchModel.status;
//         print("Mall Data --------> ${mallData.length}");
//         print("Spell Slider Data --------> ${spellSliderData.length}");
//         print("Newly Data --> ${newlyLaunchedData?.length}");

//         return LayoutBuilder(builder: (context, BoxConstraints constraints) {
//           return SingleChildScrollView(
//             child: Container(
//               width: double.infinity,
//               child: Column(
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Container(
//                       height: 200,
//                       width: 400,
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(10)),
//                       child: CarouselSlider.builder(
//                         itemCount: spellSliderData.isNotEmpty
//                             ? spellSliderData.length
//                             : 0,
//                         itemBuilder: (BuildContext context, int itemIndex,
//                                 int pageViewIndex) =>
//                             Card(
//                           clipBehavior: Clip.antiAliasWithSaveLayer,
//                           shape: RoundedRectangleBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(10.0))),
//                           child: FadeInImage.memoryNetwork(
//                             placeholder: kTransparentImage,
//                             image: spellSliderData.length != 0
//                                 ? spellSliderData[itemIndex].image.toString()
//                                 : "https://jyotish.techsaga.live/uploads/banner/2024022211270109.jpg",
//                             fit: BoxFit.fill,
//                             width: double.infinity,
//                           ),
//                         ),
//                         options: CarouselOptions(
//                           autoPlay: true,
//                           enlargeCenterPage: true,
//                           initialPage: 0,
//                         ),
//                       ),
//                     ),
//                   ),
//                   /*Search Section*/
//                   Container(
//                     margin: EdgeInsets.only(left: 10, right: 10, top: 10),
//                     decoration: BoxDecoration(
//                         color: Colors.white,
//                         boxShadow: [
//                           BoxShadow(
//                               color: Colors.grey.shade600.withOpacity(.5),
//                               spreadRadius: 1,
//                               blurRadius: 2,
//                               offset: Offset(2, 2))
//                         ],
//                         borderRadius: BorderRadius.circular(12)),
//                     child: TextField(
//                       controller: jyotishMallText,
//                       onChanged: (value) {
//                         setState(() {
//                           if (value.isEmpty) {
//                             filteredMallData =
//                                 mallData; // Reset to mallData when search text is empty
//                           } else {
//                             filteredMallData = mallData
//                                 .where((element) => element.name!
//                                     .toLowerCase()
//                                     .contains(value.toLowerCase()))
//                                 .toList();
//                           }
//                           filteredMallData.forEach(
//                             (element) => developer.log(element.name.toString()),
//                           );
//                           // log(filteredMallData.length.toString());
//                         });
//                       },
//                       decoration: InputDecoration(
//                           border: OutlineInputBorder(
//                               borderSide: BorderSide.none,
//                               borderRadius: BorderRadius.circular(12)),
//                           prefixIcon: Icon(Icons.search),
//                           filled: true,
//                           fillColor: Colors.white,
//                           hintText: "Lets find what you're looking for ...",
//                           contentPadding: EdgeInsets.zero),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   /*Product Grid View Screen*/
//                   filteredMallData.isNotEmpty
//                       ? Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: GridView.builder(
//                               physics: NeverScrollableScrollPhysics(),
//                               shrinkWrap: true,
//                               itemCount: filteredMallData.length,
//                               gridDelegate:
//                                   SliverGridDelegateWithFixedCrossAxisCount(
//                                       crossAxisCount: 2,
//                                       mainAxisSpacing: 10,
//                                       crossAxisSpacing: 10),
//                               itemBuilder: (context, index) {
//                                 developer.log(
//                                     "This is to check the astro mall.view all screen gridview which isn't updating at all " +
//                                         filteredMallData.length.toString());
//                                 return InkWell(
//                                   onTap: () {
//                                     if (mallData[index].name == "Pooja") {
//                                       setState(() {
//                                         widget.onChangedItem!(3);
//                                       });
//                                       Navigator.of(context).pop();
//                                     } else {
//                                       Navigator.of(context)
//                                           .push(SlideRightRoute(
//                                               page: MallItemsScreen(
//                                         productId: filteredMallData[index]
//                                             .id
//                                             .toString(),
//                                         productImg: filteredMallData[index]
//                                             .image
//                                             .toString(),
//                                         productName: filteredMallData[index]
//                                             .name
//                                             .toString(),
//                                       )))
//                                           .then((value) {
//                                         setState(() {});
//                                       });
//                                       print("Enter");
//                                     }
//                                   },
//                                   child: Container(
//                                     margin: EdgeInsets.only(left: 10),
//                                     height: 200,
//                                     width: 200,
//                                     child: Card(
//                                       elevation: 2,
//                                       clipBehavior: Clip.antiAliasWithSaveLayer,
//                                       shape: RoundedRectangleBorder(
//                                           borderRadius: BorderRadius.all(
//                                               Radius.circular(10.0))),
//                                       child: Stack(
//                                         children: <Widget>[
//                                           Positioned(
//                                             child: Image.network(
//                                               fit: BoxFit.cover,
//                                               "${filteredMallData[index].image}",
//                                             ),
//                                             right: 0,
//                                             left: 0,
//                                             top: 0,
//                                             bottom: 0,
//                                           ),
//                                           Container(
//                                             margin: EdgeInsets.only(top: 120),
//                                             width: double.infinity,
//                                             height: 100,
//                                             child: ClipRect(
//                                               child: BackdropFilter(
//                                                 filter: ImageFilter.blur(
//                                                     sigmaX: 1.0, sigmaY: 1.0),
//                                                 child: Container(
//                                                   decoration: BoxDecoration(
//                                                       gradient: LinearGradient(
//                                                           begin: Alignment
//                                                               .bottomCenter,
//                                                           end: Alignment
//                                                               .topCenter,
//                                                           colors: [
//                                                         Colors.black54
//                                                             .withOpacity(1),
//                                                         Colors.white70
//                                                             .withOpacity(00)
//                                                       ])),
//                                                   height: 40,
//                                                   child: Center(
//                                                     child: Text(
//                                                       "${filteredMallData[index].name}",
//                                                       style: TextStyle(
//                                                           color: Colors.white),
//                                                     ),
//                                                   ),
//                                                 ),
//                                               ),
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ),
//                                 );
//                               }),
//                         )
//                       : SizedBox(
//                           height: 250,
//                           child: Center(child: Text("No Data Found"))),
//                   SizedBox(height: 20),
//                   /* New Launch */
//                   newLaunchStatus == true
//                       ? Container(
//                           height: 200,
//                           width: double.infinity,
//                           color: AppColor.appColor.withOpacity(.4),
//                           child: Column(
//                             children: <Widget>[
//                               Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: Text(
//                                   "Newly Launched",
//                                   style: TextStyle(fontSize: 16),
//                                 ),
//                               ),
//                               Expanded(
//                                 child: ListView.builder(
//                                     scrollDirection: Axis.horizontal,
//                                     shrinkWrap: true,
//                                     itemCount: newlyLaunchedData?.length,
//                                     itemBuilder: (context, index) {
//                                       return InkWell(
//                                         onTap: () {
//                                           //TODO: Change rating and the astrologer Display name
//                                           newlyLaunchedData?[index].activity ==
//                                                   "E-Pooja"
//                                               ? Navigator.of(context).push(SlideRightRoute(
//                                                   page: PoojaDetailScreen(
//                                                       astrologer_id:
//                                                           "${newlyLaunchedData?[index].astrologerId.toString()}",
//                                                       id:
//                                                           "${newlyLaunchedData?[index].id.toString()}",
//                                                       title:
//                                                           "${newlyLaunchedData?[index].title.toString()}",
//                                                       shortDes:
//                                                           "${newlyLaunchedData?[index].shortDescription.toString()}",
//                                                       urName:
//                                                           "${newlyLaunchedData?[index].astrologerName.toString()}",
//                                                       date: newlyLaunchedData![
//                                                               index]
//                                                           .time!,
//                                                       description:
//                                                           "${newlyLaunchedData?[index].description.toString()}",
//                                                       uProfile:
//                                                           "${newlyLaunchedData[index].astrologerImage.toString()}",
//                                                       price: [],
//                                                       nprice:
//                                                           newlyLaunchedData?[
//                                                                   index]
//                                                               .price,
//                                                       astrologerDisplayName: "",
//                                                       rating: "0",
//                                                       pImage:
//                                                           "${newlyLaunchedData?[index].image.toString()}",
//                                                       uBio:
//                                                           "${newlyLaunchedData?[index].astrologerBio.toString()}")))
//                                               : Navigator.of(context)
//                                                   .push(SlideRightRoute(
//                                                       page: MallProductDetails(
//                                                   id: '${newlyLaunchedData?[index].id.toString()}',
//                                                   shortDetail:
//                                                       '${newlyLaunchedData?[index].shortDescription.toString()}',
//                                                   headingList: [],
//                                                   name:
//                                                       '${newlyLaunchedData?[index].title.toString()}',
//                                                   img:
//                                                       '${newlyLaunchedData?[index].image.toString()}',
//                                                   price:
//                                                       '${newlyLaunchedData?[index].price}',
//                                                   headingListRecentSearch: [],
//                                                   headingSearch: [],
//                                                 )));
//                                         },
//                                         child: Padding(
//                                           padding: const EdgeInsets.all(8.0),
//                                           child: Column(
//                                             children: <Widget>[
//                                               CircleAvatar(
//                                                 radius: 50,
//                                                 backgroundImage:
//                                                     CachedNetworkImageProvider(
//                                                         "${newlyLaunchedData?[index].image}"),
//                                               ),
//                                               Text(
//                                                 "${newlyLaunchedData?[index].title}",
//                                                 textAlign: TextAlign.center,
//                                                 style: TextStyle(fontSize: 20),
//                                               )
//                                             ],
//                                           ),
//                                         ),
//                                       );
//                                     }),
//                               ),
//                             ],
//                           ),
//                         )
//                       : Text(""),
//                 ],
//               ),
//             ),
//           );
//         });
//       }),
//     );
//   }
// }
