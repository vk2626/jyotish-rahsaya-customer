import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../Core/Model/jyotish.mall.model.dart';
import '../../Core/Model/jyotish_mall_model_new.dart';
import '../../Core/Model/newly_launched_model_new.dart';
import '../../Core/Model/spell_slider_model.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../Core/formatter.dart';
import '../../Core/logger_helper.dart';
import '../../Core/nav/SlideRightRoute.dart';
import 'mall.item.dart';
import 'mall_item_details_screen.dart';
import '../../dialog/showLoaderDialog.dart';
import '../../router_constants.dart';
import '../../utils/AppColor.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../utils/custom_circular_progress_indicator.dart';

class AstroMallAllItemScreen extends StatefulWidget {
  final IntCallback onChangedItem;
  const AstroMallAllItemScreen({super.key, required this.onChangedItem});

  @override
  State<AstroMallAllItemScreen> createState() => _AstroMallAllItemScreenState();
}

typedef void IntCallback(int value);

class _AstroMallAllItemScreenState extends State<AstroMallAllItemScreen> {
  TextEditingController jyotishMallText = TextEditingController();
  // late JyotishMallModelNew? jyotishMallModel;
  List<ProductItem> productItemsList = [];
  List<ProductItem> filterProductItemsList = [];
  NewlyLauchedModelNew? newlyLauchedModel;
  late SpellSliderModelNew? spellSliderModelNew;
  bool isCategoriesLoading = true;
  bool isNewLaunchLoading = true;
  bool isSliderLoading = true;

  @override
  void initState() {
    super.initState();
    getJyotishMallCategories();
    getNewlyLaunchedItems();
    getSpellSliderItems();
  }

  getJyotishMallCategories() async {
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getJyotishMallProviderNew()
        .then((value) {
      if (value != null) {
        setState(() {
          productItemsList = value.data;
          filterProductItemsList = value.data;
          isCategoriesLoading = false;
        });
      }
    });
  }

  getNewlyLaunchedItems() async {
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getNewlyLauchedProviderNew()
        .then((value) {
      if (value != null) {
        setState(() {
          newlyLauchedModel = value;
          isNewLaunchLoading = false;
        });
      } else {
        setState(() {
          isNewLaunchLoading = false;
        });
      }
    });
  }

  getSpellSliderItems() async {
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .getSpellSliderProviderNew(id: "0")
        .then((value) {
      if (value != null) {
        setState(() {
          spellSliderModelNew = value;
          isSliderLoading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Jyotish Mall".tr(),
          style: GoogleFonts.poppins(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 200,
                width: 400,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: isSliderLoading
                    ? Center(child: CustomCircularProgressIndicator())
                    : CarouselSlider.builder(
                        itemCount: spellSliderModelNew!.data.length,
                        itemBuilder: (BuildContext context, int itemIndex,
                                int pageViewIndex) =>
                            Card(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          child: FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            image: spellSliderModelNew!.data[itemIndex].image,
                            fit: BoxFit.fill,
                            width: double.infinity,
                          ),
                        ),
                        options: CarouselOptions(
                          autoPlay: true,
                          enlargeCenterPage: true,
                          initialPage: 0,
                        ),
                      ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.shade600.withOpacity(.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(2, 2))
                  ],
                  borderRadius: BorderRadius.circular(12)),
              child: TextField(
                controller: jyotishMallText,
                onChanged: (value) {
                  setState(() {
                    filterProductItemsList = productItemsList
                        .where((element) => element.name
                            .toLowerCase()
                            .contains(value.toLowerCase()))
                        .toList();
                  });
                },
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(12)),
                    prefixIcon: Icon(Icons.search),
                    filled: true,
                    fillColor: Colors.white,
                    hintText:
                        "Let's find what you're looking for".tr() + " ...",
                    hintStyle: GoogleFonts.poppins(),
                    contentPadding: EdgeInsets.zero),
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: isCategoriesLoading
                  ? SizedBox(
                      height: MediaQuery.of(context).size.height * .8,
                      child: Center(child: CustomCircularProgressIndicator()),
                    )
                  : GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: filterProductItemsList.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 0,
                        crossAxisSpacing: 0,
                      ),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            if (filterProductItemsList[index]
                                    .name
                                    .toLowerCase() ==
                                "pooja") {
                              setState(() {
                                widget.onChangedItem(4);
                              });
                              Navigator.of(context).pop();
                            } else {
                              Navigator.of(context).push(SlideRightRoute(
                                  page: ListenableProvider(
                                      create: (context) => ListOfApisProvider(),
                                      child: MallItemsScreen(
                                          categoryId:
                                              filterProductItemsList[index]
                                                  .id
                                                  .toString(),
                                          productImg:
                                              filterProductItemsList[index]
                                                  .image,
                                          suggestedType:
                                              filterProductItemsList[index]
                                                  .suggestedType,
                                          productName:
                                              filterProductItemsList[index]
                                                  .name))));
                            }
                          },
                          child: Container(
                            height: 200,
                            width: 200,
                            child: Card(
                              elevation: 2,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: Stack(
                                children: <Widget>[
                                  Positioned.fill(
                                    child: Image.network(
                                      fit: BoxFit.cover,
                                      filterProductItemsList[index].image,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 120),
                                    width: double.infinity,
                                    height: 100,
                                    child: ClipRect(
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 1.0, sigmaY: 1.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  begin: Alignment.bottomCenter,
                                                  end: Alignment.topCenter,
                                                  colors: [
                                                Colors.black54.withOpacity(1),
                                                Colors.white70.withOpacity(0),
                                              ])),
                                          height: 40,
                                          child: Center(
                                            child: Text(
                                              TFormatter.capitalizeSentence(
                                                  filterProductItemsList[index]
                                                      .name),
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
            ),
            SizedBox(height: 10),
            if ((newlyLauchedModel != null))
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 12),
                decoration:
                    BoxDecoration(color: AppColor.appColor.withOpacity(.4)),
                child: Column(
                  children: [
                    Text(
                      "Newly Lauched".tr(),
                      style: GoogleFonts.poppins(
                          fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 8),
                    isNewLaunchLoading
                        ? SizedBox(
                            height: MediaQuery.of(context).size.height * .15,
                            child: Center(
                                child: CustomCircularProgressIndicator()),
                          )
                        : SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: List.generate(
                                (newlyLauchedModel != null)
                                    ? newlyLauchedModel!.data.length
                                    : 0,
                                (index) => InkWell(
                                  onTap: () {
                                    if (newlyLauchedModel!
                                            .data[index].activity ==
                                        "E-Pooja") {
                                      context.pushNamed(
                                          RouteConstants.poojaScreen,
                                          queryParameters: {
                                            'poojaId': newlyLauchedModel!
                                                .data[index].id
                                                .toString(),
                                            'isDirectPooja': "1"
                                          });
                                    } else {
                                      Navigator.of(context)
                                          .push(SlideRightRoute(
                                              page: ListenableProvider(
                                        create: (context) =>
                                            ListOfApisProvider(),
                                        child: MallItemDetailsScreen(
                                          remedyId: newlyLauchedModel!
                                              .data[index].id
                                              .toString(),
                                          productName: newlyLauchedModel!
                                              .data[index].title,
                                        ),
                                      )));
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Column(
                                      children: [
                                        CircleAvatar(
                                          radius: 48,
                                          backgroundImage:
                                              CachedNetworkImageProvider(
                                                  newlyLauchedModel!
                                                      .data[index].image),
                                        ),
                                        SizedBox(height: 4),
                                        SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .3,
                                            child: Text(
                                              TFormatter.capitalizeSentence(
                                                  newlyLauchedModel!
                                                      .data[index].title),
                                              textAlign: TextAlign.center,
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            SizedBox(height: 12)
          ],
        ),
      ),
    );
  }
}
