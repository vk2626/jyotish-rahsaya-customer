// import 'package:flutter/material.dart';

// import '../Core/Model/SimilarConsultantsResp.dart';
// import '../Core/Model/astrologer.list.model.dart';

// class ImagePopupViewWidget extends StatefulWidget {
//   const ImagePopupViewWidget({super.key, this.link, this.consultantGallery});

//   final List<Gallery>? link;
//   final List<ConsultantsGallery>? consultantGallery;

//   @override
//   State<ImagePopupViewWidget> createState() => _ImagePopupViewWidgetState();
// }

// class _ImagePopupViewWidgetState extends State<ImagePopupViewWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       child: Container(
//         height: 300,
//         width: 400,
//         decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
//         child: PageView.builder(
//             scrollDirection: Axis.horizontal,
//             itemCount: widget.link!.length,
//             itemBuilder: (context, index) {
//               return InteractiveViewer(
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.circular(10),
//                   child: Image.network(
//                     widget.link!.isNotEmpty?widget.link![index].image.toString() : widget.consultantGallery![index].image.toString(),
//                     height: 100,
//                     fit: BoxFit.contain,
//                   ),
//                 ),
//               );
//             }),
//       ),
//     );
//   }
// }
