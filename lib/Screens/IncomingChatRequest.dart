import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/formatter.dart';
import '../router_constants.dart';
import '../utils/snack_bar.dart';
import '../Core/Provider/chatting_provider.dart';
import '../Core/helper_functions.dart';
import '../Core/logger_helper.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/Constants.dart';
import '../Core/Api/list.of.apis.dart';
import '../Core/nav/SlideRightRoute.dart';
import '../utils/AppColor.dart';
import 'flutter_chat_screen.dart';

class IncomingChatRequest extends StatefulWidget {
  String communicationId;
  String imageurl;
  String username;
  String astrologer_id;
  String chat_price;
  String order_id;
  bool isFreeChat;
  String chatDuration;
  final String infoId;
  final String lat;
  final String lon;
  final String dob;
  final String tob;
  final Map<String, String> messageInfo;
  IncomingChatRequest(this.communicationId, this.imageurl, this.username,
      this.astrologer_id, this.chat_price, this.order_id,
      {required this.isFreeChat,
      required this.chatDuration,
      required this.messageInfo,
      required this.infoId,
      required this.lat,
      required this.lon,
      required this.dob,
      required this.tob});

  @override
  State<IncomingChatRequest> createState() => _IncomingChatRequestState();
}

class _IncomingChatRequestState extends State<IncomingChatRequest> {
  ListOfApi listOfApi = ListOfApi();
  bool isLoading = false;

  String customerName = "";
  int customerID = 0;
  late Timer _callTimeoutTimer;
  static const int callTimeoutSeconds = 30;
  @override
  void initState() {
    super.initState();
    _startCallTimeoutTimer();
    getUserId();
    THelperFunctions.logScreenNameEvent(screenName: "incoming_call");
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;

      print("CustomerID -> $customerID");

      if (prefs.getString(Constants.fullName) != null) {
        customerName = prefs.getString(Constants.fullName)!;
      }
    }
    await prefs.remove(Constants.isCalling);
    setState(() {});
  }

  void _startCallTimeoutTimer() async {
    _callTimeoutTimer = Timer(const Duration(seconds: callTimeoutSeconds), () {
      Navigator.of(context).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    TLoggerHelper.debug("Chat Price ${widget.chat_price}");
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: Container(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 60),
                  Text(
                    'Incoming Chat request from',
                    style: GoogleFonts.poppins(
                        fontSize: 16, fontWeight: FontWeight.w400),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            color: AppColor.appColor,
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image:
                                    AssetImage('asset/images/white_logo.png'),
                                fit: BoxFit.fitWidth)),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text(
                        'Jyotish Rahsaya'.tr(),
                        style: GoogleFonts.poppins(fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ],
              ),
            )),
            Expanded(
                child: Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColor.appColor, width: 1),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(
                                '${widget.imageurl}'),
                            fit: BoxFit.fitWidth)),
                  ),
                  SizedBox(height: 10),
                  Text(
                    TFormatter.capitalizeSentence(widget.username),
                    style: GoogleFonts.poppins(
                        fontSize: 18, fontWeight: FontWeight.w500),
                  )
                ],
              ),
            )),
            Expanded(
                child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      if (!isLoading) {
                        accept_startChat();
                      } else {
                        Fluttertoast.showToast(
                            msg: "We're connecting your chat !!");
                      }
                    },
                    child: Container(
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: 45),
                      padding:
                          EdgeInsets.symmetric(horizontal: 70, vertical: 18),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          shape: BoxShape.rectangle,
                          color: Colors.green.shade300),
                      child: Center(
                          child: isLoading
                              ? CircularProgressIndicator(color: Colors.white)
                              : Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Icon(
                                      Icons.chat,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      'Start Chat'.tr(),
                                      style: GoogleFonts.poppins(
                                        color: Colors.white,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ],
                                )),
                    ),
                  ),
                  SizedBox(height: 20),
                  GestureDetector(
                    onTap: () async {
                      await rejectCallFunction(
                              communicationId: widget.communicationId)
                          .whenComplete(() => Navigator.of(context).pop());
                    },
                    child: Text(
                      "Reject Chat Request".tr(),
                      style: GoogleFonts.poppins(color: Colors.red),
                    ),
                  )
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }

  void accept_startChat() async {
    try {
      setState(() {
        isLoading = true;
        _callTimeoutTimer.cancel();
      });
      var userData = await listOfApi.acceptCustomerStartChat(
          communicationid: widget.communicationId.toString());
      print("communication_id${widget.communicationId.toString()}");
      print("user_data${userData}");
      final Map<String, dynamic> parseData = await jsonDecode(userData);
      bool isAuthenticated = parseData['status'];
      // TLoggerHelper.debug("${parseData['communication']['duration_time']}");

      if (isAuthenticated) {
        TLoggerHelper.debug("${parseData['communication']['duration_time']}");
        // Navigator.of(context).pushReplacement(SlideRightRoute(
        //     page: FlutterChatScreen(
        //   orderID: widget.order_id,
        //   dob: widget.dob,
        //   lat: widget.lat,
        //   lon: widget.lon,
        //   tob: widget.tob,
        //   infoId: widget.infoId,
        //   cummID: widget.communicationId,
        //   timerDuration: Duration(
        //     minutes: int.parse(parseData['communication']['duration_time']),
        //   ),
        //   chat_price: int.parse(widget.chat_price.toString()),
        //   fromId: customerID.toString(),
        //   customerImage: widget.imageurl,
        //   customerName: widget.username,
        //   toId: widget.astrologer_id,
        //   isFreeChat: widget.isFreeChat,
        //   messageInfo: widget.messageInfo,
        //   addNavigation: () {},
        // )));
        context.pushReplacementNamed(RouteConstants.chatScreen);
      } else {
        TLoggerHelper.debug(parseData['message'].toString());
        Fluttertoast.showToast(
            msg: "Communication Request Declined by Astrologer");
        if (context.canPop()) {
          context.pop();
        }
      }

      // print(list);
    } on SocketException catch (_) {
      //showSnackBar(context, "Oops No You Need A Good Internet Connection");
    } catch (e) {
      print("error-${e}");
    }
  }

  static Future<void> rejectCallFunction(
      {required String communicationId}) async {
    var endPointUrl = UrlConstants.rejectCall;
    final Map<String, String> body = {'communication_id': communicationId};
    TLoggerHelper.debug(body.entries.toString());
    THelperFunctions.clearCallingPrefs();
    await THelperFunctions.httpPostHandler(
        client: http.Client(), endPointUrl: endPointUrl, body: body);
  }
}
