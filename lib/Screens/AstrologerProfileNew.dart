import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import 'package:jyotish_rahsaya/Core/notification/local_notice_service.dart';
import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Model/LeaveChatModel.dart';
import '../Core/Model/astrologer_details_model.dart';
import '../Core/Model/gift.api.model.dart';
import '../Core/Model/recharge.amount.model.dart';
import '../Core/Model/similar_consultant_model.dart';
import '../Core/Model/wait_list_model.dart';
import '../Core/Provider/chatting_provider.dart';
import '../Core/Provider/go_live_provider.dart';
import '../Core/formatter.dart';
import '../Core/helper_functions.dart';
import 'dart:math' as math;
import '../Core/logger_helper.dart';
import '../Core/nav/SlideRightRoute.dart';
import '../utils/review_bottom_sheet.dart';
import 'ChatIntakeForm.dart';
import 'ProfileScreen.dart';
import 'flutter_chat_screen.dart';
import 'payment.info.dart';
import '../dialog/showLoaderDialog.dart';
import '../router_constants.dart';
import '../utils/AppColor.dart';
import '../utils/DashSeparator.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

import '../Core/Provider/list.of.api.provider.dart';
import '../utils/custom_circular_progress_indicator.dart';

class AstrologerProfileNew extends StatefulWidget {
  final String astrologerId;
  String type;
  final bool isAstrologerOnline;

  AstrologerProfileNew(
      {super.key,
      required this.astrologerId,
      this.type = "chat",
      required this.isAstrologerOnline});

  @override
  State<AstrologerProfileNew> createState() => _AstrologerProfileNewState();
}

class _AstrologerProfileNewState extends State<AstrologerProfileNew> {
  AstrologerDetailsModel? astrologerDetailsModel;
  late AstrologerElement astrologer;
  bool isLoading = true;
  bool readMore = false;
  GiftApiModel giftApiModel = GiftApiModel();
  int? selectedIdx;
  String customerId = "0";
  bool isSelected = false;
  bool isGiftLoading = true;
  bool isSimilarConsultantsLoading = true;
  String giftID = "";
  String giftAmount = "";
  bool isGiftSelected = false;
  String walletAmount = "0";
  String giftSelectedPrice = "0";
  SimilarConsultantsModel? similarConsultantsModel;
  bool isAstrologerFollowed = false;
  List<String> popupMenu = [];
  bool isBlockedUser = false;

  @override
  void initState() {
    super.initState();
    _fetchAstrologerDetails();
  }

  getGiftData() async {
    Provider.of<GoLiveProvider>(context, listen: false)
        .getGiftListData(context: context)
        .then((val) {
      if (val != null) {
        setState(() {
          giftApiModel = val;
          isGiftLoading = false;
        });
      }
    });
    getWalletAmount();
  }

  getSimilarConsultantsList({required String primarySkillIds}) async {
    Provider.of<ListOfApisProvider>(context, listen: false)
        .getSimilarConsulants(primarySkillsList: primarySkillIds)
        .then((val) {
      if (val != null) {
        setState(() {
          similarConsultantsModel = val;
        });
      }
      setState(() {
        isSimilarConsultantsLoading = false;
      });
    });
    getWalletAmount();
  }

  getWalletAmount() {
    THelperFunctions.getWalletAmount().then((value) {
      setState(() {
        walletAmount = value.toString();
        print("wallet amount -- >${walletAmount}");
      });
    });
  }

  Future<void> _fetchAstrologerDetails() async {
    await THelperFunctions.getUserId().then((value) {
      setState(() {
        customerId = value;
      });
      TLoggerHelper.debug("Customer ID: $customerId");
    });
    final fetchedDetails =
        await Provider.of<ListOfApisProvider>(context, listen: false)
            .getAstrologerDetailsNew(
                astrologerId: widget.astrologerId, type: widget.type);
    if (fetchedDetails != null) {
      getGiftData();
      setState(() {
        astrologerDetailsModel = fetchedDetails;
        astrologer = astrologerDetailsModel!.astrologers.first;

        isAstrologerFollowed = astrologer.astrologer.followingUser;
        isBlockedUser = astrologer.astrologer.blockedUser;

        isLoading = false;
        if (astrologer.astrologer.blockedUser) {
          popupMenu = ["UnBlock".tr()];
        } else {
          popupMenu = ["Report & Block".tr()];
        }
      });
      TLoggerHelper.debug(astrologer.astrologer.followingUser.toString());
      getSimilarConsultantsList(
          primarySkillIds: astrologer.astrologer.primarySkillId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profile".tr(),
          style: GoogleFonts.poppins(
              fontWeight: FontWeight.w500, color: Colors.white),
        ),
        backgroundColor: AppColor.appColor,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: <Widget>[
          GestureDetector(
            child: Container(
                margin: EdgeInsets.only(right: 10),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                child: Row(children: <Widget>[
                  Icon(Icons.share, size: 20),
                  SizedBox(width: 5),
                  Text("Share".tr(),
                      style: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.white))
                ]),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    shape: BoxShape.rectangle,
                    border: Border.all(color: Colors.white, width: 2))),
            onTap: () {},
          )
        ],
      ),
      backgroundColor: Colors.grey.shade50,

      body: isLoading
          ? Center(child: CustomCircularProgressIndicator())
          : Container(
              margin: EdgeInsets.all(8),
              child: RefreshIndicator(
                color: Colors.white,
                backgroundColor: AppColor.appColor,
                strokeWidth: 2.0,
                onRefresh: () async {
                  _fetchAstrologerDetails();
                  getWalletAmount();
                },
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        color: Colors.white,
                        elevation: 3,
                        child: Stack(
                          children: [
                            Stack(
                              children: [
                                Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(width: 15),
                                        Column(
                                          children: [
                                            SizedBox(height: 10),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Column(
                                                  children: <Widget>[
                                                    SizedBox(height: 10),
                                                    Stack(
                                                      children: [
                                                        Center(
                                                          child: Card(
                                                            clipBehavior: Clip
                                                                .antiAliasWithSaveLayer,
                                                            child: CircleAvatar(
                                                              radius: 40,
                                                              backgroundImage:
                                                                  CachedNetworkImageProvider(
                                                                      astrologer
                                                                          .avatar),
                                                            ),
                                                            shape: CircleBorder(
                                                              side: BorderSide(
                                                                  color: AppColor
                                                                      .appColor,
                                                                  width: 2),
                                                            ),
                                                          ),
                                                        ),
                                                        if (astrologer.label !=
                                                            0)
                                                          Positioned(
                                                            bottom: -4,
                                                            left: 0,
                                                            right: 0,
                                                            child: Center(
                                                              child: Card(
                                                                color: Colors
                                                                    .amber,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius
                                                                                .all(
                                                                          Radius.circular(
                                                                              10.0),
                                                                        ),
                                                                        side: BorderSide(
                                                                            color:
                                                                                Colors.yellow)),
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              12,
                                                                          right:
                                                                              12,
                                                                          bottom:
                                                                              3,
                                                                          top:
                                                                              2),
                                                                  child: Text(
                                                                      TFormatter.capitalizeSentence(astrologer
                                                                          .astrologer
                                                                          .labelText),
                                                                      style: GoogleFonts.poppins(
                                                                          fontSize:
                                                                              8,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          color:
                                                                              Colors.white)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        Positioned(
                                                          bottom: 10,
                                                          right: 0,
                                                          left: 64,
                                                          child: CircleAvatar(
                                                            radius: 7,
                                                            child: CircleAvatar(
                                                              radius: 5,
                                                              backgroundColor: widget
                                                                      .isAstrologerOnline
                                                                  ? Colors.green
                                                                  : Colors
                                                                      .grey, // Green for online, grey for offline
                                                              child: Tooltip(
                                                                message: widget
                                                                        .isAstrologerOnline
                                                                    ? 'Online'
                                                                        .tr()
                                                                    : 'Offline'
                                                                        .tr(),
                                                                child:
                                                                    Container(
                                                                  height: 20,
                                                                  width: 20,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    RatingBarIndicator(
                                                        rating: astrologer
                                                            .astrologer.rating
                                                            .toDouble(),
                                                        itemCount: 5,
                                                        itemSize: 15.0,
                                                        itemBuilder: (context,
                                                                _) =>
                                                            const Icon(
                                                              Icons.star,
                                                              color:
                                                                  Colors.amber,
                                                            )),
                                                    Text(
                                                      "${astrologer.astrologer.orderCount} " +
                                                          "Orders".tr(),
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color: Colors
                                                                  .grey[600],
                                                              fontSize: 12),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(width: 5),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(height: 25),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .3,
                                              child: Text(
                                                TFormatter.capitalizeSentence(
                                                    astrologer.name),
                                                maxLines: 3,
                                                style: GoogleFonts.poppins(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .3,
                                              child: Text(
                                                TFormatter.capitalizeSentence(
                                                    astrologer.astrologer
                                                        .allSkillName),
                                                style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                                textAlign: TextAlign.start,
                                                maxLines: 3,
                                              ),
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .3,
                                              child: Text(
                                                TFormatter.capitalizeSentence(
                                                    astrologer.astrologer
                                                        .languageName),
                                                style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                                textAlign: TextAlign.start,
                                                maxLines: 3,
                                              ),
                                            ),
                                            Text(
                                              "Exp".tr() +
                                                  " : ${astrologer.astrologer.experienceYear} " +
                                                  "Yrs".tr(),
                                              style: GoogleFonts.poppins(
                                                  color: Colors.grey[600],
                                                  fontSize: 12),
                                              maxLines: 3,
                                            ),
                                            Text.rich(
                                              TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(text: "₹ "),
                                                  // Original Price with condition for strike-through
                                                  if (widget.type == "chat")
                                                    TextSpan(
                                                      text: astrologer
                                                          .astrologer.chatPrice,
                                                      style:
                                                          GoogleFonts.poppins(
                                                        fontSize: 15,
                                                        color: Colors.grey[600],
                                                        decoration: (astrologer
                                                                        .astrologer
                                                                        .chatDiscountPrice
                                                                        .isNotEmpty &&
                                                                    astrologer.astrologer
                                                                            .chatDiscountPrice !=
                                                                        "0") ||
                                                                (astrologer.astrologer
                                                                            .freeChat ==
                                                                        1 &&
                                                                    astrologer
                                                                            .isFreeChatCompleted ==
                                                                        0) ||
                                                                (customerId ==
                                                                        "0" &&
                                                                    widget.type ==
                                                                        "chat" &&
                                                                    astrologer.astrologer
                                                                            .freeChat ==
                                                                        1)
                                                            ? TextDecoration
                                                                .lineThrough
                                                            : TextDecoration
                                                                .none,
                                                      ),
                                                    ),
                                                  if (widget.type == "call")
                                                    TextSpan(
                                                      text: astrologer
                                                          .astrologer.callPrice,
                                                      style:
                                                          GoogleFonts.poppins(
                                                        fontSize: 15,
                                                        color: Colors.grey[600],
                                                        decoration: (astrologer
                                                                    .astrologer
                                                                    .callDiscountPrice
                                                                    .isEmpty ||
                                                                astrologer
                                                                        .astrologer
                                                                        .callDiscountPrice ==
                                                                    "0")
                                                            ? TextDecoration
                                                                .none
                                                            : TextDecoration
                                                                .lineThrough,
                                                      ),
                                                    ),
                                                  // Discounted Price if it exists and isn't "0"
                                                  if ((widget.type == "chat" &&
                                                          astrologer
                                                              .astrologer
                                                              .chatDiscountPrice
                                                              .isNotEmpty &&
                                                          astrologer.astrologer
                                                                  .chatDiscountPrice !=
                                                              "0") ||
                                                      (astrologer.astrologer
                                                                  .freeChat ==
                                                              1) &&
                                                          (astrologer
                                                                  .isFreeChatCompleted ==
                                                              0) ||
                                                      (customerId == "0" &&
                                                          widget.type ==
                                                              "chat" &&
                                                          astrologer.astrologer
                                                                  .freeChat ==
                                                              1))
                                                    TextSpan(
                                                      text: (astrologer.astrologer
                                                                          .freeChat ==
                                                                      1) &&
                                                                  (astrologer
                                                                          .isFreeChatCompleted ==
                                                                      0) ||
                                                              (customerId ==
                                                                      "0" &&
                                                                  widget.type ==
                                                                      "chat" &&
                                                                  astrologer
                                                                          .astrologer
                                                                          .freeChat ==
                                                                      1)
                                                          ? "Free"
                                                          : "  ${astrologer.astrologer.chatDiscountPrice}",
                                                      style:
                                                          GoogleFonts.poppins(
                                                        color: Colors.red,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  if (widget.type == "call" &&
                                                      astrologer
                                                          .astrologer
                                                          .callDiscountPrice
                                                          .isNotEmpty &&
                                                      astrologer.astrologer
                                                              .callDiscountPrice !=
                                                          "0")
                                                    TextSpan(
                                                      text:
                                                          "  ${astrologer.astrologer.callDiscountPrice}",
                                                      style:
                                                          GoogleFonts.poppins(
                                                        color: Colors.red,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 10),
                                          ],
                                        ),
                                        Spacer(),
                                        SizedBox(
                                          height: 120,
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  TextButton(
                                                    onPressed: () async {
                                                      await Provider.of<
                                                                  ListOfApisProvider>(
                                                              context,
                                                              listen: false)
                                                          .followerNew(
                                                              astrologerID:
                                                                  astrologer
                                                                      .astrologer
                                                                      .userId
                                                                      .toString(),
                                                              status:
                                                                  !isAstrologerFollowed)
                                                          .then((value) {
                                                        if (value) {
                                                          setState(() {
                                                            isAstrologerFollowed =
                                                                !isAstrologerFollowed;
                                                          });
                                                        }
                                                      });
                                                    },
                                                    style: TextButton.styleFrom(
                                                      backgroundColor: AppColor
                                                          .btnActiveColorLight,
                                                      minimumSize: Size.zero,
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10),
                                                      tapTargetSize:
                                                          MaterialTapTargetSize
                                                              .shrinkWrap,
                                                    ),
                                                    child: Text(
                                                        isAstrologerFollowed
                                                            ? "Followed".tr()
                                                            : "Follow".tr(),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style:
                                                            GoogleFonts.poppins(
                                                                color: Colors
                                                                    .black)),
                                                  ),
                                                  MenuAnchor(
                                                    style: MenuStyle(
                                                        backgroundColor:
                                                            WidgetStatePropertyAll(
                                                                Colors.white)),
                                                    builder:
                                                        (BuildContext context,
                                                            MenuController
                                                                controller,
                                                            Widget? child) {
                                                      return IconButton(
                                                        onPressed: () {
                                                          if (controller
                                                              .isOpen) {
                                                            controller.close();
                                                          } else {
                                                            controller.open();
                                                          }
                                                        },
                                                        icon: const Icon(Icons
                                                            .more_vert_sharp),
                                                        tooltip:
                                                            'Show menu'.tr(),
                                                      );
                                                    },
                                                    menuChildren: List<
                                                        MenuItemButton>.generate(
                                                      popupMenu.length,
                                                      (index) => MenuItemButton(
                                                          onPressed: () {
                                                            if (isBlockedUser) {
                                                              Provider.of<ListOfApisProvider>(
                                                                      context,
                                                                      listen:
                                                                          false)
                                                                  .submitBlockReport(
                                                                      astrologerId: astrologer
                                                                          .astrologer
                                                                          .userId,
                                                                      reason:
                                                                          "",
                                                                      blockStatus:
                                                                          false)
                                                                  .then(
                                                                (val) {
                                                                  if (val) {
                                                                    setState(
                                                                        () {
                                                                      popupMenu =
                                                                          [
                                                                        "Report & Block"
                                                                            .tr()
                                                                      ];
                                                                      isBlockedUser =
                                                                          !isBlockedUser;
                                                                    });
                                                                    Fluttertoast.showToast(
                                                                        msg: "${TFormatter.capitalizeSentence(astrologer.name)} " +
                                                                            "UnBlocked".tr());
                                                                  } else {
                                                                    Fluttertoast.showToast(
                                                                        msg: "Some Error occured".tr() +
                                                                            " !!");
                                                                  }
                                                                },
                                                              );
                                                            } else {
                                                              reportAndBlock();
                                                            }
                                                          },
                                                          child: Text(
                                                              popupMenu[index],
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                      fontSize:
                                                                          12,
                                                                      color: Colors
                                                                          .black))),
                                                    ).toList(),
                                                  ),
                                                ],
                                              ),
                                              Spacer(),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      child: Divider(thickness: 1),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          right: 10, left: 10, bottom: 5),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Image.asset(
                                                "asset/images/chat.png",
                                                height: 15,
                                                width: 15,
                                                color: Colors.grey[600],
                                              ),
                                              SizedBox(width: 10),
                                              Text(
                                                astrologer.astrologer
                                                    .totalDurationChat,
                                                style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    color: Colors.grey[600]),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 30,
                                            child: VerticalDivider(
                                              width: 2,
                                              thickness: 1,
                                              color: Colors.grey,
                                            ),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Image.asset(
                                                "asset/images/call.png",
                                                height: 15,
                                                width: 15,
                                                color: Colors.grey[600],
                                              ),
                                              SizedBox(width: 10),
                                              Text(
                                                astrologer.astrologer
                                                    .totalDurationCall,
                                                style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    color: Colors.grey[600]),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                // if (astrologer.label != 0)
                                //   Positioned(
                                //     left: -25,
                                //     top: 15,
                                //     child: Transform.rotate(
                                //       angle: -math.pi / 3.5,
                                //       child: Container(
                                //         decoration: BoxDecoration(
                                //           gradient: LinearGradient(
                                //             colors: [
                                //               Color(
                                //                   0xFF3D3D3D), // Dark gray (closer to black)
                                //               Color(0xFF1C1C1C), // Almost black
                                //               Color(0xFF000000), // Pure black
                                //               Color(
                                //                   0xFF434343), // Slightly lighter for a metallic effect
                                //             ],
                                //             begin: Alignment.topCenter,
                                //             end: Alignment.bottomCenter,
                                //           ),
                                //         ),
                                //         width: 100,
                                //         alignment: Alignment.center,
                                //         padding: EdgeInsets.symmetric(
                                //           vertical: 2,
                                //         ),
                                //         child: Text(
                                //           astrologer.astrologer.labelText,
                                //           textAlign: TextAlign.center,
                                //           style: GoogleFonts.poppins(
                                //             color: AppColor.whiteColor,
                                //             fontSize: 10,
                                //           ),
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      if (similarConsultantsModel != null ||
                          isSimilarConsultantsLoading)
                        Card(
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "Check Similar Consultants".tr(),
                                      style: GoogleFonts.poppins(fontSize: 16),
                                    ),
                                    IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.info))
                                  ],
                                ),
                              ),
                              isSimilarConsultantsLoading
                                  ? SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              .15,
                                      child: Center(
                                        child:
                                            CustomCircularProgressIndicator(),
                                      ),
                                    )
                                  : GridView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: 3,
                                      shrinkWrap: true,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 3,
                                              mainAxisSpacing: 5,
                                              childAspectRatio: 0.6,
                                              mainAxisExtent: 176,
                                              crossAxisSpacing: 3),
                                      itemBuilder: (ctx, i) {
                                        return GestureDetector(
                                            onTap: () {
                                              context.pushNamed(
                                                RouteConstants
                                                    .astrologerProfile,
                                                queryParameters: {
                                                  'astrologerId':
                                                      similarConsultantsModel!
                                                          .users[i].id
                                                          .toString(),
                                                  'type': 'chat',
                                                },
                                              );
                                            },
                                            child: Card(
                                              elevation: 3,
                                              color: Colors.white,
                                              child: Stack(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Center(
                                                        child: Stack(
                                                          children: [
                                                            Center(
                                                              child: Card(
                                                                clipBehavior: Clip
                                                                    .antiAliasWithSaveLayer,
                                                                child:
                                                                    CircleAvatar(
                                                                  radius: 40,
                                                                  backgroundImage:
                                                                      CachedNetworkImageProvider(similarConsultantsModel!
                                                                          .users[
                                                                              i]
                                                                          .avatar),
                                                                ),
                                                                shape:
                                                                    CircleBorder(
                                                                  side: BorderSide(
                                                                      color: AppColor
                                                                          .appColor,
                                                                      width: 2),
                                                                ),
                                                              ),
                                                            ),
                                                            if (similarConsultantsModel!
                                                                    .users[i]
                                                                    .label !=
                                                                0)
                                                              Positioned(
                                                                bottom: -4,
                                                                left: 0,
                                                                right: 0,
                                                                child: Center(
                                                                  child: Card(
                                                                    color: Colors
                                                                        .amber,
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius: BorderRadius.all(
                                                                          Radius.circular(
                                                                              10.0),
                                                                        ),
                                                                        side: BorderSide(color: Colors.yellow)),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              12,
                                                                          right:
                                                                              12,
                                                                          bottom:
                                                                              3,
                                                                          top:
                                                                              2),
                                                                      child: Text(
                                                                          TFormatter.capitalizeSentence(similarConsultantsModel!
                                                                              .users[
                                                                                  i]
                                                                              .astrologer
                                                                              .labelText),
                                                                          maxLines:
                                                                              1,
                                                                          overflow: TextOverflow
                                                                              .fade,
                                                                          style: GoogleFonts.poppins(
                                                                              fontSize: 8,
                                                                              fontWeight: FontWeight.w600,
                                                                              color: Colors.white)),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            Positioned(
                                                              bottom: 10,
                                                              right: 0,
                                                              left: 64,
                                                              child:
                                                                  CircleAvatar(
                                                                radius: 7,
                                                                child:
                                                                    CircleAvatar(
                                                                  radius: 5,
                                                                  backgroundColor: similarConsultantsModel!
                                                                              .users[
                                                                                  i]
                                                                              .isOnlineAstrologer ==
                                                                          1
                                                                      ? Colors
                                                                          .green
                                                                      : Colors
                                                                          .grey, // Green for online, grey for offline
                                                                  child:
                                                                      Tooltip(
                                                                    message: similarConsultantsModel!.users[i].isOnlineAstrologer ==
                                                                            1
                                                                        ? 'Online'
                                                                            .tr()
                                                                        : 'Offline'
                                                                            .tr(),
                                                                    child:
                                                                        Container(
                                                                      height:
                                                                          20,
                                                                      width: 20,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(height: 10),
                                                      Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 5),
                                                        child: Text(
                                                          TFormatter
                                                              .capitalizeSentence(
                                                                  similarConsultantsModel!
                                                                      .users[i]
                                                                      .name),
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 5),
                                                        child: Text(
                                                            Constants.currency +
                                                                "${similarConsultantsModel!.users[i].astrologer.chatPrice}/" +
                                                                "min".tr(),
                                                            style: GoogleFonts
                                                                .poppins(
                                                                    fontSize:
                                                                        14,
                                                                    color: Colors
                                                                            .grey[
                                                                        800])),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ));
                                      },
                                    ),
                            ],
                          ),
                        ),
                      Card(
                        color: Colors.white,
                        elevation: 3,
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Wrap(
                            children: <Widget>[
                              Text(
                                TFormatter.capitalize(
                                    astrologer.astrologer.bio),
                                maxLines: readMore ? 100 : 2,
                                overflow: TextOverflow.ellipsis,
                                style: GoogleFonts.poppins(fontSize: 16),
                              ),
                              Container(
                                alignment: Alignment.bottomRight,
                                padding: EdgeInsets.all(6),
                                child: GestureDetector(
                                  child: Text(
                                    readMore
                                        ? "Show less".tr()
                                        : "Show more".tr(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.green),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      readMore = !readMore;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (astrologer.astrologer.gallery.isNotEmpty)
                        Container(
                          width: double.infinity,
                          height: 150,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: astrologer.astrologer.gallery.length,
                            itemBuilder: (BuildContext context, int i) {
                              return Container(
                                height: 150,
                                width: 120,
                                child: Card(
                                  elevation: 2,
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0))),
                                  child: InkWell(
                                    onTap: () {
                                      // showDialog(
                                      //   context: context,
                                      //   builder: (_) =>
                                      //       ImagePopupViewWidget(
                                      //           link: astrologer.astrologer.gallery[i].image),
                                      // );
                                      THelperFunctions.showFullImage(
                                          context,
                                          astrologer
                                              .astrologer.gallery[i].image);
                                    },
                                    child: FadeInImage.memoryNetwork(
                                      placeholder: kTransparentImage,
                                      fit: BoxFit.cover,
                                      image: astrologer
                                          .astrologer.gallery[i].image,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      // : astrologer.astrologer.
                      //     ? Container(
                      //         width: double.infinity,
                      //         height: 150,
                      //         child: ListView.builder(
                      //           scrollDirection: Axis.horizontal,
                      //           shrinkWrap: true,
                      //           itemCount: widget.consultantGallery!.length,
                      //           itemBuilder: (BuildContext context, int i) {
                      //             return Container(
                      //               height: 150,
                      //               width: 120,
                      //               child: Card(
                      //                 elevation: 2,
                      //                 clipBehavior:
                      //                     Clip.antiAliasWithSaveLayer,
                      //                 shape: RoundedRectangleBorder(
                      //                     borderRadius: BorderRadius.all(
                      //                         Radius.circular(10.0))),
                      //                 child: InkWell(
                      //                   onTap: () {
                      //                     showDialog(
                      //                       context: context,
                      //                       builder: (_) =>
                      //                           ImagePopupViewWidget(
                      //                               consultantGallery: widget
                      //                                   .consultantGallery),
                      //                     );
                      //                   },
                      //                   child: FadeInImage.memoryNetwork(
                      //                     placeholder: kTransparentImage,
                      //                     fit: BoxFit.cover,
                      //                     image:
                      //                         "${widget.consultantGallery![i].image == null ? "" : widget.consultantGallery![i].image}",
                      //                   ),
                      //                 ),
                      //               ),
                      //             );
                      //           },
                      //         ),
                      //       )
                      //     : SizedBox(),
                      ,
                      SizedBox(height: 10),
                      Visibility(
                          visible: astrologer.astrologer.reviews.length > 0,
                          child: Card(
                            color: Colors.white,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 8),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "User Reviews".tr(),
                                        style: GoogleFonts.poppins(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      GestureDetector(
                                        child: Text(
                                          astrologer.astrologer.reviews.length >
                                                  5
                                              ? "View All".tr()
                                              : "",
                                          style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              color: Colors.grey[700]),
                                        ),
                                        onTap: () {
                                          showModalBottomSheet(
                                              isScrollControlled: true,
                                              constraints: BoxConstraints(
                                                  maxHeight: MediaQuery.of(
                                                              context)
                                                          .size
                                                          .height *
                                                      0.8,
                                                  minHeight:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .height *
                                                          0.8),
                                              context: context,
                                              builder: (context) =>
                                                  ReviewBottomSheet(
                                                      rating: astrologer
                                                          .astrologer.rating
                                                          .toStringAsFixed(2),
                                                      astrologerName:
                                                          astrologer.name,
                                                      reviews: astrologer
                                                          .astrologer.reviews));
                                        },
                                      )
                                    ],
                                  ),
                                  ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount:
                                          astrologer.astrologer.reviews.length >
                                                  5
                                              ? 5
                                              : astrologer
                                                  .astrologer.reviews.length,
                                      itemBuilder:
                                          (BuildContext context, int i) {
                                        return Container(
                                          child: Card(
                                            color: Colors.white,
                                            elevation: 2,
                                            clipBehavior:
                                                Clip.antiAliasWithSaveLayer,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                            ),
                                            child: Container(
                                              margin: EdgeInsets.all(5),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 8, vertical: 8),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Card(
                                                        clipBehavior: Clip
                                                            .antiAliasWithSaveLayer,
                                                        child: Image.network(
                                                          width: 40,
                                                          height: 40,
                                                          fit: BoxFit.cover,
                                                          astrologer
                                                              .astrologer
                                                              .reviews[i]
                                                              .customerImage,
                                                        ),
                                                        shape: CircleBorder(
                                                            side: BorderSide(
                                                                color: AppColor
                                                                    .appColor,
                                                                width: 2)),
                                                      ),
                                                      SizedBox(width: 8),
                                                      Expanded(
                                                          child: Text(
                                                        TFormatter
                                                            .capitalizeSentence(
                                                                astrologer
                                                                    .astrologer
                                                                    .reviews[i]
                                                                    .customerName),
                                                        style:
                                                            GoogleFonts.poppins(
                                                          color: Colors.black,
                                                          fontSize: 16,
                                                        ),
                                                      )),
                                                      MenuAnchor(
                                                        builder: (BuildContext
                                                                context,
                                                            MenuController
                                                                controller,
                                                            Widget? child) {
                                                          return IconButton(
                                                            onPressed: () {
                                                              if (controller
                                                                  .isOpen) {
                                                                controller
                                                                    .close();
                                                              } else {
                                                                controller
                                                                    .open();
                                                              }
                                                            },
                                                            icon: const Icon(Icons
                                                                .more_vert_sharp),
                                                            tooltip: 'Show menu'
                                                                .tr(),
                                                          );
                                                        },
                                                        // menuChildren: List<
                                                        //     MenuItemButton>.generate(
                                                        //   popupMenu.length,
                                                        //   (index) =>
                                                        //       MenuItemButton(
                                                        //           child: Text(
                                                        //     popupMenu[index],
                                                        //     style: GoogleFonts
                                                        //         .poppins(),
                                                        //   )),
                                                        // ).toList(),
                                                        menuChildren: [],
                                                        // menuChildren: List<
                                                        //     MenuItemButton>.generate(
                                                        //   2,
                                                        //   (int index) =>
                                                        //       MenuItemButton(
                                                        //     onPressed: () => setState(
                                                        //         () => selectedMenu =
                                                        //             index),
                                                        //     child: index == 0
                                                        //         ? Text(
                                                        //             '${popUpMenu2[index]}')
                                                        //         : Text(
                                                        //             '${popUpMenu2[index]}',
                                                        //             style: GoogleFonts
                                                        //                 .poppins(
                                                        //                     color: Colors
                                                        //                         .red),
                                                        //           ),
                                                        //   ),
                                                        // ),
                                                      )
                                                    ],
                                                  ),
                                                  RatingBarIndicator(
                                                      rating: double.parse(
                                                          astrologer.astrologer
                                                              .reviews[i].rate),
                                                      itemCount: 5,
                                                      itemSize: 15.0,
                                                      itemBuilder: (context,
                                                              _) =>
                                                          const Icon(
                                                            Icons.star,
                                                            color: Colors.amber,
                                                          )),
                                                  Wrap(children: <Widget>[
                                                    Text(
                                                      TFormatter.capitalize(
                                                          astrologer
                                                              .astrologer
                                                              .reviews[i]
                                                              .description),
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color: AppColor
                                                                  .darkGrey,
                                                              fontSize: 14),
                                                    )
                                                  ]),
                                                  // SizedBox(height: 10),
                                                  // Card(
                                                  //   child: Padding(
                                                  //     padding: EdgeInsets.all(5),
                                                  //     child: Column(
                                                  //       mainAxisAlignment:
                                                  //           MainAxisAlignment.start,
                                                  //       crossAxisAlignment:
                                                  //           CrossAxisAlignment.start,
                                                  //       children: <Widget>[
                                                  //         Text(
                                                  //           "Astrologer Name",
                                                  //           style:
                                                  //               GoogleFonts.poppins(
                                                  //             color: Colors.black,
                                                  //             fontSize: 16,
                                                  //           ),
                                                  //         ),
                                                  //         SizedBox(height: 3),
                                                  //         Text(
                                                  //           "Astrologer Reply----> Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                                                  //           style:
                                                  //               GoogleFonts.poppins(
                                                  //             color:
                                                  //                 AppColor.darkGrey,
                                                  //             fontSize: 14,
                                                  //           ),
                                                  //         )
                                                  //       ],
                                                  //     ),
                                                  //   ),
                                                  // )
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      })
                                ],
                              ),
                            ),
                          )),
                      Card(
                        color: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Icon(Icons.wallet_giftcard_rounded,
                                      color: AppColor.borderColor, size: 25),
                                  SizedBox(width: 5),
                                  Expanded(
                                      child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          "Send Gift to".tr() +
                                              " ${TFormatter.capitalizeSentence(astrologer.name)}",
                                          overflow: TextOverflow.fade,
                                          style: GoogleFonts.poppins(
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 5),
                                      Icon(
                                        Icons.info_outline_rounded,
                                        color: AppColor.borderColor,
                                      ),
                                    ],
                                  )),
                                  SizedBox(width: 15),
                                  Text(
                                    "${Constants.currency}${walletAmount}",
                                    style: GoogleFonts.poppins(fontSize: 16),
                                  )
                                ],
                              ),
                              isGiftLoading
                                  ? SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              .2,
                                      child: Center(
                                          child:
                                              CustomCircularProgressIndicator()),
                                    )
                                  : GridView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: giftApiModel.data!.length,
                                      shrinkWrap: true,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 4,
                                              childAspectRatio: 8 / 16,
                                              mainAxisSpacing: 2,
                                              crossAxisSpacing: 3),
                                      itemBuilder: (context, giftIdx) {
                                        var giftData = giftApiModel.data!;
                                        return InkWell(
                                          onTap: () {
                                            setState(() {
                                              selectedIdx = giftIdx;
                                              isSelected = true;
                                            });
                                            giftID =
                                                giftData[giftIdx].id.toString();
                                            giftAmount = giftData[giftIdx]
                                                .price
                                                .toString();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: giftIdx == selectedIdx
                                                  ? AppColor.borderColor
                                                      .withOpacity(.3)
                                                  : Colors.transparent,
                                            ),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 70,
                                                  width: 50,
                                                  child: CachedNetworkImage(
                                                      imageUrl:
                                                          giftData[giftIdx]
                                                              .image!),
                                                ),
                                                Column(
                                                  children: [
                                                    Text(
                                                      "${giftData[giftIdx].title}",
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    Text(
                                                        "₹ ${giftData[giftIdx].price}")
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                              Visibility(
                                visible: isSelected ?? false,
                                child: Container(
                                  alignment: Alignment.bottomRight,
                                  width: double.infinity,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      TextButton(
                                        style: TextButton.styleFrom(
                                            fixedSize: Size.fromHeight(10.0),
                                            backgroundColor: AppColor.appColor,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5))),
                                        onPressed: () async {
                                          if (await THelperFunctions
                                                  .getUserId() ==
                                              "0") {
                                            context.pushNamed(
                                                RouteConstants.authScreen);
                                          } else {
                                            _rechargeDialog(
                                                    context, "wallet", 0,
                                                    isGift: true)
                                                .whenComplete(() {
                                              getWalletAmount();
                                            });
                                          }
                                          // context
                                          //     .read<
                                          //         ListOfApisProvider>()
                                          //     .giftWalletProvider(
                                          //         customerID: customerID
                                          //             .toString(),
                                          //         astrologerID: widget
                                          //             .uID
                                          //             .toString(),
                                          //         giftID: giftID
                                          //             .toString());
                                        },
                                        child: Text(
                                          "Recharge",
                                          style:
                                              GoogleFonts.poppins(fontSize: 14),
                                        ),
                                      ),
                                      SizedBox(width: 5),
                                      TextButton(
                                        style: TextButton.styleFrom(
                                            backgroundColor: AppColor.appColor,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5))),
                                        onPressed: () async {
                                          if (await THelperFunctions
                                                  .getUserId() ==
                                              "0") {
                                            context.pushNamed(
                                                RouteConstants.authScreen);
                                          } else {
                                            if (double.parse(walletAmount) <
                                                double.parse(giftAmount!)) {
                                              Fluttertoast.showToast(
                                                  msg:
                                                      "Insufficient amount. Please recharge your wallet."
                                                          .tr());
                                              return;
                                            }

                                            showLoaderDialog(context);
                                            await Provider.of<
                                                        ListOfApisProvider>(
                                                    context,
                                                    listen: false)
                                                .giftWalletProvider(
                                                    customerID:
                                                        await THelperFunctions
                                                            .getUserId(),
                                                    context: context,
                                                    astrologerID: widget
                                                        .astrologerId
                                                        .toString(),
                                                    giftID: giftID.toString())
                                                .whenComplete(
                                              () {
                                                getWalletAmount();
                                                setState(() {
                                                  giftID = "";
                                                  giftAmount = "";
                                                  isSelected = false;
                                                  selectedIdx = null;
                                                });
                                              },
                                            );
                                          }
                                        },
                                        child: Text(
                                          "Send".tr(),
                                          style:
                                              GoogleFonts.poppins(fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * .1),
                    ],
                  ),
                ),
              ),
            ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     NotificationHandler()
      //         .showNormalNotification(title: "title", desc: "desc");
      //   },
      // ),
      bottomNavigationBar: FutureBuilder<WaitListModel?>(
        future: THelperFunctions.checkWaitList(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // return Container(
            //   padding: EdgeInsets.all(16),
            //   child: CustomCircularProgressIndicator(),
            // );
            return SizedBox.shrink();
          } else if (snapshot.hasError) {
            return SizedBox.shrink();
            // return Container(
            //   padding: EdgeInsets.all(16),
            //   child: Text('Error: ${snapshot.error}'),
            // );
          } else if (snapshot.hasData && snapshot.data != null) {
            WaitListModel waitListModel = snapshot.data!;
            return IntrinsicHeight(
              child: Container(
                color: AppColor.btnActiveColorLight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: CircleBorder(
                          side: BorderSide(color: AppColor.appColor, width: 2),
                        ),
                        child: CircleAvatar(
                          radius: 32,
                          backgroundImage: CachedNetworkImageProvider(
                              waitListModel.users.avatar ?? ''),
                        ),
                      ),
                      SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            TFormatter.capitalizeSentence(
                                waitListModel.users.name),
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "₹",
                                  style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[600],
                                  ),
                                ),
                                TextSpan(
                                  text: (waitListModel.users.astrologer.type
                                                  .toLowerCase() ==
                                              'chat'
                                          ? "${waitListModel.users.astrologer.chatPrice}"
                                          : "${waitListModel.users.astrologer.callPrice}") +
                                      "/min (${TFormatter.capitalize(waitListModel.users.astrologer.type)})",
                                  style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    color: Colors.grey[600],
                                  ),
                                ),
                              ],
                            ),
                            textAlign: TextAlign.center,
                          ),
                          if (waitListModel.users.astrologer.status == 0)
                            Text(
                              "Wait".tr() +
                                  " ~ ${waitListModel.users.astrologer.waitTime} " +
                                  "mins".tr(),
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                          if (waitListModel.users.astrologer.status == 2)
                            Text(
                              "${TFormatter.capitalize(waitListModel.users.astrologer.type).tr()} in Progress",
                              style: GoogleFonts.poppins(
                                color: Colors.blue,
                                fontSize: 14,
                              ),
                            ),
                        ],
                      ),
                      Spacer(),
                      Column(
                        mainAxisAlignment:
                            waitListModel.users.astrologer.status == 2
                                ? MainAxisAlignment.spaceAround
                                : MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          if (waitListModel.users.astrologer.status == 0)
                            CircleAvatar(
                              radius: 12,
                              backgroundColor: Colors.grey[300],
                              child: InkWell(
                                onTap: () {
                                  showCancelBookingDialog(
                                      "${waitListModel.users.astrologer.chatRequestId}");
                                },
                                child: Icon(
                                  Icons.close,
                                  color: Colors.black54,
                                  size: 14,
                                ),
                              ),
                            ),
                          if (waitListModel.users.astrologer.status == 2)
                            InkWell(
                              onTap: () async {
                                Navigator.of(context)
                                    .push(SlideRightRoute(
                                        page: ListenableProvider(
                                  create: (context) => ChattingProvider(),
                                  child: FlutterChatScreen(
                                    orderID: waitListModel
                                        .users.astrologer.chatRequestId
                                        .toString(),
                                    cummID: waitListModel
                                        .users.astrologer.chatRequestId
                                        .toString(),
                                    chat_price: int.parse(waitListModel
                                        .users.astrologer.chatPrice),
                                    fromId: await THelperFunctions.getUserId(),
                                    toId: waitListModel.users.astrologer.userId,
                                    // timerDuration: Duration(
                                    //     minutes: int.parse(waitListModel
                                    //         .users
                                    //         .astrologer
                                    //         .durationTime)),
                                    timerDuration: Duration(
                                            minutes: int.parse(waitListModel
                                                .users
                                                .astrologer
                                                .durationTime)) -
                                        DateTime.now().difference(
                                            DateTime.parse(waitListModel
                                                .users.astrologer.startTime)),
                                    astrologerName:
                                        TFormatter.capitalizeSentence(
                                            waitListModel.users.name),
                                    astrologerImage: waitListModel.users.avatar,
                                    dob: waitListModel.users.astrologer.dob,
                                    tob: waitListModel.users.astrologer.dobTime,
                                    messageInfo: {},
                                    infoId:
                                        waitListModel.users.astrologer.infoId,
                                    lat: waitListModel.users.astrologer.lat,
                                    lon: waitListModel.users.astrologer.lon,
                                    // addNavigation: () {},
                                  ),
                                )))
                                    .then((value) {
                                  setState(() {});
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 4),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(4)),
                                child: Text(
                                  "View".tr() +
                                      " ${TFormatter.capitalize(waitListModel.users.astrologer.type).tr()}",
                                  style: GoogleFonts.poppins(),
                                ),
                              ),
                            ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8),
              child: isLoading
                  ? SizedBox()
                  : Row(
                      children: <Widget>[
                        Expanded(
                          child: InkWell(
                            onTap: () async {
                              bool isFreeChat =
                                  astrologer.isFreeChatCompleted == 0 &&
                                      astrologer.astrologer.freeChat == 1;
                              if (astrologer.astrologer.statusChat == 1) {
                                if (astrologer.astrologer.nextonlines.chatNODate
                                        .isEmpty &&
                                    astrologer.astrologer.nextonlines.chatNOTime
                                        .isEmpty) {
                                  String customerName =
                                      await THelperFunctions.getUserName();
                                  String? userId =
                                      await THelperFunctions.getUserId();

                                  TLoggerHelper.debug("User Id is:-- $userId");

                                  // Use && to ensure both conditions are true
                                  if (userId != "0") {
                                    int customerID = int.parse(userId);

                                    // int freeChat =
                                    //     astrologer.a;

                                    int realPrice = (int.parse(astrologer
                                                    .astrologer
                                                    .chatDiscountPrice) <=
                                                int.parse(astrologer
                                                    .astrologer.chatPrice)) &&
                                            (int.parse(astrologer.astrologer
                                                    .chatDiscountPrice) >
                                                0)
                                        ? int.parse(astrologer
                                            .astrologer.chatDiscountPrice)
                                        : int.parse(
                                            astrologer.astrologer.chatPrice);

                                    if (customerID > 0 &&
                                        customerName.isNotEmpty) {
                                      if (isFreeChat) {
                                        sendToChatIntakeForm(
                                            "chat", "free", true);
                                      } else {
                                        if ((double.parse(walletAmount) >=
                                                realPrice * 5) &&
                                            walletAmount != "0") {
                                          sendToChatIntakeForm("chat",
                                              realPrice.toString(), false);
                                        } else {
                                          _rechargeDialog(
                                                  context, "wallet", realPrice)
                                              .whenComplete(() {
                                            getWalletAmount();
                                          });
                                        }
                                      }
                                    } else if (customerID > 0 &&
                                        customerName.isEmpty) {
                                      Navigator.of(context).push(
                                          SlideRightRoute(
                                              page: ProfileScreen()));
                                    }
                                  } else {
                                    context
                                        .pushNamed(RouteConstants.authScreen)
                                        .whenComplete(
                                      () {
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  }
                                } else {
                                  nextOnlineDialog(
                                      astrologerName: astrologer.name,
                                      astrologerImage: astrologer.avatar,
                                      nextOnlineTime: astrologer
                                          .astrologer.nextonlines.chatNOTime,
                                      nextOnlineDate: astrologer
                                          .astrologer.nextonlines.chatNODate);
                                }
                              } else {
                                Fluttertoast.showToast(
                                    msg: "${astrologer.name} " +
                                        "Chat is Offline".tr() +
                                        " !!");
                              }
                              // _rechargeDialog(context,"chat");
                            },
                            child: Card(
                              color: Colors.white,
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    Image.asset(
                                      "asset/images/chat.png",
                                      height: 15,
                                      width: 15,
                                      color:
                                          astrologer.astrologer.waitTime == 0 &&
                                                  astrologer.astrologer
                                                          .statusChat ==
                                                      1
                                              ? Colors.green[600]
                                              : Colors.red[600],
                                    ),
                                    SizedBox(width: 5),
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            astrologer.astrologer.statusChat ==
                                                    1
                                                ? "Chat".tr()
                                                : "Chat Offline".tr(),
                                            style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                                color: astrologer.astrologer
                                                                .waitTime ==
                                                            0 &&
                                                        astrologer.astrologer
                                                                .statusChat ==
                                                            1
                                                    ? Colors.green[600]
                                                    : Colors.red[600]),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Visibility(
                                            child: Text(
                                                !widget.isAstrologerOnline
                                                    ? "Currently Offline"
                                                    : "Wait".tr() +
                                                        " ~ ${astrologer.astrologer.waitTime}/" +
                                                        "mins".tr(),
                                                style: GoogleFonts.poppins(
                                                    color: Colors.red[600],
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                            visible: astrologer
                                                        .astrologer.waitTime !=
                                                    0 &&
                                                astrologer.astrologer
                                                        .statusChat ==
                                                    1,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.center,
                                ),
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                              ),
                              elevation: 5,
                            ),
                          ),
                        ),
                        SizedBox(width: 5),
                        Expanded(
                          child: InkWell(
                            onTap: () async {
                              if (astrologer.astrologer.statusCall == 1) {
                                if (astrologer.astrologer.nextonlines.callNODate
                                        .isEmpty &&
                                    astrologer.astrologer.nextonlines.callNOTime
                                        .isEmpty) {
                                  String customerName =
                                      await THelperFunctions.getUserName();
                                  String userId =
                                      await THelperFunctions.getUserId();

                                  // Check if userId is valid and not empty
                                  int customerID =
                                      userId.isNotEmpty ? int.parse(userId) : 0;

                                  // Determine the real price based on discount
                                  int realPrice = (int.parse(astrologer
                                                  .astrologer
                                                  .callDiscountPrice) <=
                                              int.parse(astrologer
                                                  .astrologer.callPrice)) &&
                                          (int.parse(astrologer.astrologer
                                                  .callDiscountPrice) >
                                              0)
                                      ? int.parse(astrologer
                                          .astrologer.callDiscountPrice)
                                      : int.parse(
                                          astrologer.astrologer.callPrice);

                                  if (customerID > 0 &&
                                      customerName.isNotEmpty) {
                                    if ((double.parse(walletAmount) >=
                                            realPrice * 5) &&
                                        walletAmount != "0") {
                                      sendToChatIntakeForm(
                                          "call", realPrice.toString(), false);
                                    } else {
                                      _rechargeDialog(
                                              context, "wallet", realPrice)
                                          .whenComplete(() {
                                        getWalletAmount();
                                      });
                                    }
                                  } else if (customerID > 0 &&
                                      customerName.isEmpty) {
                                    Navigator.of(context).push(
                                        SlideRightRoute(page: ProfileScreen()));
                                  } else {
                                    context
                                        .pushNamed(RouteConstants.authScreen)
                                        .whenComplete(
                                      () {
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  }
                                } else {
                                  nextOnlineDialog(
                                      astrologerName: astrologer.name,
                                      astrologerImage: astrologer.avatar,
                                      nextOnlineTime: astrologer
                                          .astrologer.nextonlines.callNOTime,
                                      nextOnlineDate: astrologer
                                          .astrologer.nextonlines.callNODate);
                                }
                              } else {
                                Fluttertoast.showToast(
                                    msg: "${astrologer.name} " +
                                        "Call is Offline" +
                                        " !!");
                              }
                            },
                            child: Card(
                              color: Colors.white,
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    Image.asset("asset/images/call.png",
                                        height: 15,
                                        width: 15,
                                        color: astrologer.astrologer.waitTime ==
                                                    0 &&
                                                astrologer.astrologer
                                                        .statusCall ==
                                                    1
                                            ? Colors.green[600]
                                            : Colors.red[600]),
                                    SizedBox(width: 10),
                                    Expanded(
                                        child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          astrologer.astrologer.statusCall == 1
                                              ? "Call".tr()
                                              : "Call Offline".tr(),
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: astrologer.astrologer
                                                              .waitTime ==
                                                          0 &&
                                                      astrologer.astrologer
                                                              .statusCall ==
                                                          1
                                                  ? Colors.green[600]
                                                  : Colors.red[600]),
                                        ),
                                        SizedBox(width: 5),
                                        Visibility(
                                          child: Text(
                                              !widget.isAstrologerOnline
                                                  ? "Currently Offline".tr()
                                                  : "Wait".tr() +
                                                      " ~ ${astrologer.astrologer.waitTime} /" +
                                                      "mins".tr(),
                                              style: GoogleFonts.poppins(
                                                  color: Colors.red[600],
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.w500)),
                                          visible:
                                              astrologer.astrologer.waitTime !=
                                                      0 &&
                                                  astrologer.astrologer
                                                          .statusCall ==
                                                      1,
                                        ),
                                      ],
                                    ))
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.center,
                                ),
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                              ),
                              elevation: 5,
                            ),
                          ),
                        ),
                      ],
                    ),
            );
          }
        },
      ),
      // bottomNavigationBar: Padding(
      //   padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8),
      //   child: isLoading
      //       ? SizedBox()
      //       : Row(
      //           children: <Widget>[
      //             Expanded(
      //               child: InkWell(
      //                 onTap: () async {
      //                   if (astrologer.astrologer.statusChat == 1) {
      //                     if (astrologer
      //                             .astrologer.nextonlines.chatNODate.isEmpty &&
      //                         astrologer
      //                             .astrologer.nextonlines.chatNOTime.isEmpty) {
      //                       String customerName =
      //                           await THelperFunctions.getUserName();
      //                       String? userId = await THelperFunctions.getUserId();

      //                       TLoggerHelper.debug("User Id is:-- $userId");

      //                       // Use && to ensure both conditions are true
      //                       if (userId != null && userId.isNotEmpty) {
      //                         int customerID = int.parse(userId);
      //                         int freeChat = astrologer.astrologer.freeChat;

      //                         int realPrice = (int.parse(astrologer
      //                                         .astrologer.chatDiscountPrice) <=
      //                                     int.parse(astrologer
      //                                         .astrologer.chatPrice)) &&
      //                                 (int.parse(astrologer
      //                                         .astrologer.chatDiscountPrice) >
      //                                     0)
      //                             ? int.parse(
      //                                 astrologer.astrologer.chatDiscountPrice)
      //                             : int.parse(astrologer.astrologer.chatPrice);

      //                         if (customerID > 0 && customerName.isNotEmpty) {
      //                           if (freeChat == 1) {
      //                             sendToChatIntakeForm("chat", "free");
      //                           } else {
      //                             if ((double.parse(walletAmount) >=
      //                                     realPrice * 5) &&
      //                                 walletAmount != "0") {
      //                               sendToChatIntakeForm(
      //                                   "chat", realPrice.toString());
      //                             } else {
      //                               _rechargeDialog(
      //                                       context, "wallet", realPrice)
      //                                   .whenComplete(() {
      //                                 getWalletAmount();
      //                               });
      //                             }
      //                           }
      //                         } else if (customerID > 0 &&
      //                             customerName.isEmpty) {
      //                           Navigator.of(context).push(
      //                               SlideRightRoute(page: ProfileScreen()));
      //                         }
      //                       } else {
      //                         context
      //                             .pushNamed(RouteConstants.authScreen)
      //                             .whenComplete(
      //                           () {
      //                             Navigator.of(context).pop();
      //                           },
      //                         );
      //                       }
      //                     } else {
      //                       nextOnlineDialog(
      //                           astrologerName: astrologer.name,
      //                           astrologerImage: astrologer.avatar,
      //                           nextOnlineTime: astrologer
      //                               .astrologer.nextonlines.chatNOTime,
      //                           nextOnlineDate: astrologer
      //                               .astrologer.nextonlines.chatNODate);
      //                     }
      //                   } else {
      //                     Fluttertoast.showToast(
      //                         msg: "${astrologer.name} " +
      //                             "Chat is Offline".tr() +
      //                             " !!");
      //                   }
      //                   // _rechargeDialog(context,"chat");
      //                 },
      //                 child: Card(
      //                   color: Colors.white,
      //                   child: Container(
      //                     child: Row(
      //                       children: <Widget>[
      //                         Image.asset(
      //                           "asset/images/chat.png",
      //                           height: 15,
      //                           width: 15,
      //                           color: astrologer.astrologer.waitTime == 0 &&
      //                                   astrologer.astrologer.statusChat == 1
      //                               ? Colors.green[600]
      //                               : Colors.red[600],
      //                         ),
      //                         SizedBox(width: 5),
      //                         Expanded(
      //                           child: Row(
      //                             mainAxisAlignment: MainAxisAlignment.center,
      //                             children: <Widget>[
      //                               Text(
      //                                 astrologer.astrologer.statusChat == 1
      //                                     ? "Chat".tr()
      //                                     : "Chat Offline".tr(),
      //                                 style: GoogleFonts.poppins(
      //                                     fontWeight: FontWeight.w500,
      //                                     fontSize: 12,
      //                                     color:
      //                                         astrologer.astrologer.waitTime ==
      //                                                     0 &&
      //                                                 astrologer.astrologer
      //                                                         .statusChat ==
      //                                                     1
      //                                             ? Colors.green[600]
      //                                             : Colors.red[600]),
      //                               ),
      //                               SizedBox(
      //                                 width: 5,
      //                               ),
      //                               Visibility(
      //                                 child: Text(
      //                                     astrologer.isOnlineAstrologer == 0
      //                                         ? "Currently Offline"
      //                                         : "Wait".tr() +
      //                                             " ~ ${astrologer.astrologer.waitTime}/" +
      //                                             "mins".tr(),
      //                                     style: GoogleFonts.poppins(
      //                                         color: Colors.red[600],
      //                                         fontSize: 12,
      //                                         fontWeight: FontWeight.w500)),
      //                                 visible: astrologer.astrologer.waitTime !=
      //                                         0 &&
      //                                     astrologer.astrologer.statusChat == 1,
      //                               ),
      //                             ],
      //                           ),
      //                         )
      //                       ],
      //                       mainAxisAlignment: MainAxisAlignment.center,
      //                     ),
      //                     margin: EdgeInsets.symmetric(
      //                         horizontal: 10, vertical: 10),
      //                   ),
      //                   elevation: 5,
      //                 ),
      //               ),
      //             ),
      //             SizedBox(width: 5),
      //             Expanded(
      //               child: InkWell(
      //                 onTap: () async {
      //                   if (astrologer.astrologer.statusCall == 1) {
      //                     if (astrologer
      //                             .astrologer.nextonlines.callNODate.isEmpty &&
      //                         astrologer
      //                             .astrologer.nextonlines.callNOTime.isEmpty) {
      //                       String customerName =
      //                           await THelperFunctions.getUserName();
      //                       String userId = await THelperFunctions.getUserId();

      //                       // Check if userId is valid and not empty
      //                       int customerID =
      //                           userId.isNotEmpty ? int.parse(userId) : 0;

      //                       // Determine the real price based on discount
      //                       int realPrice = (int.parse(astrologer
      //                                       .astrologer.callDiscountPrice) <=
      //                                   int.parse(
      //                                       astrologer.astrologer.callPrice)) &&
      //                               (int.parse(astrologer
      //                                       .astrologer.callDiscountPrice) >
      //                                   0)
      //                           ? int.parse(
      //                               astrologer.astrologer.callDiscountPrice)
      //                           : int.parse(astrologer.astrologer.callPrice);

      //                       if (customerID > 0 && customerName.isNotEmpty) {
      //                         if ((double.parse(walletAmount) >=
      //                                 realPrice * 5) &&
      //                             walletAmount != "0") {
      //                           sendToChatIntakeForm(
      //                               "call", realPrice.toString());
      //                         } else {
      //                           _rechargeDialog(context, "wallet", realPrice)
      //                               .whenComplete(() {
      //                             getWalletAmount();
      //                           });
      //                         }
      //                       } else if (customerID > 0 && customerName.isEmpty) {
      //                         Navigator.of(context)
      //                             .push(SlideRightRoute(page: ProfileScreen()));
      //                       } else {
      //                         context
      //                             .pushNamed(RouteConstants.authScreen)
      //                             .whenComplete(
      //                           () {
      //                             Navigator.of(context).pop();
      //                           },
      //                         );
      //                       }
      //                     } else {
      //                       nextOnlineDialog(
      //                           astrologerName: astrologer.name,
      //                           astrologerImage: astrologer.avatar,
      //                           nextOnlineTime: astrologer
      //                               .astrologer.nextonlines.callNOTime,
      //                           nextOnlineDate: astrologer
      //                               .astrologer.nextonlines.callNODate);
      //                     }
      //                   } else {
      //                     Fluttertoast.showToast(
      //                         msg: "${astrologer.name} " +
      //                             "Call is Offline" +
      //                             " !!");
      //                   }
      //                 },
      //                 child: Card(
      //                   color: Colors.white,
      //                   child: Container(
      //                     child: Row(
      //                       children: <Widget>[
      //                         Image.asset("asset/images/call.png",
      //                             height: 15,
      //                             width: 15,
      //                             color: astrologer.astrologer.waitTime == 0 &&
      //                                     astrologer.astrologer.statusCall == 1
      //                                 ? Colors.green[600]
      //                                 : Colors.red[600]),
      //                         SizedBox(width: 10),
      //                         Expanded(
      //                             child: Row(
      //                           mainAxisAlignment: MainAxisAlignment.center,
      //                           children: <Widget>[
      //                             Text(
      //                               astrologer.astrologer.statusCall == 1
      //                                   ? "Call".tr()
      //                                   : "Call Offline".tr(),
      //                               style: GoogleFonts.poppins(
      //                                   fontWeight: FontWeight.w500,
      //                                   fontSize: 12,
      //                                   color: astrologer.astrologer.waitTime ==
      //                                               0 &&
      //                                           astrologer.astrologer
      //                                                   .statusCall ==
      //                                               1
      //                                       ? Colors.green[600]
      //                                       : Colors.red[600]),
      //                             ),
      //                             SizedBox(width: 5),
      //                             Visibility(
      //                               child: Text(
      //                                   astrologer.isOnlineAstrologer == 0
      //                                       ? "Currently Offline".tr()
      //                                       : "Wait".tr() +
      //                                           " ~ ${astrologer.astrologer.waitTime} /" +
      //                                           "mins".tr(),
      //                                   style: GoogleFonts.poppins(
      //                                       color: Colors.red[600],
      //                                       fontSize: 10,
      //                                       fontWeight: FontWeight.w500)),
      //                               visible: astrologer.astrologer.waitTime !=
      //                                       0 &&
      //                                   astrologer.astrologer.statusCall == 1,
      //                             ),
      //                           ],
      //                         ))
      //                       ],
      //                       mainAxisAlignment: MainAxisAlignment.center,
      //                     ),
      //                     margin: EdgeInsets.symmetric(
      //                         horizontal: 10, vertical: 10),
      //                   ),
      //                   elevation: 5,
      //                 ),
      //               ),
      //             ),
      //           ],
      //         ),
      // ),
    );
  }

  void sendToChatIntakeForm(String type, String rate, bool isFreeChat) {
    showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return ListenableProvider(
            create: (context) => ListOfApisProvider(),
            child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)), //this right here
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.close,
                          size: 30,
                        ),
                      ),
                    ),
                    Card(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: CircleAvatar(
                        radius: 70,
                        backgroundImage:
                            CachedNetworkImageProvider(astrologer.avatar),
                      ),
                      shape: CircleBorder(
                          side: BorderSide(color: AppColor.appColor, width: 2)),
                    ),
                    Text(
                      TFormatter.capitalizeSentence(astrologer.name),
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      rate == "free"
                          ? "This offer at free for 3 min only. Astrologer will try to answer at least one question."
                              .tr()
                              .tr()
                          : "This offer at".tr() +
                              " ${Constants.currency} $rate/" +
                              "min for 5 mins only. Astrologer will try to answer at least one question."
                                  .tr(),
                      style: GoogleFonts.poppins(
                          color: Colors.black, fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        sendAndGetResult(
                            astrologer.name.toString(),
                            int.parse(astrologer.astrologer.userId.toString()),
                            astrologer.avatar,
                            context,
                            astrologer.astrologer.userId.toString(),
                            type,
                            isFreeChat ? 1 : 0);
                      },
                      style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          backgroundColor: AppColor.appColor),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          rate == "free"
                              ? "Start Chat".tr() + " @ Free".tr()
                              : "Start".tr() +
                                  " ${TFormatter.capitalize(type).tr()} @ ${Constants.currency} $rate/" +
                                  "min".tr(),
                          style: GoogleFonts.poppins(color: Colors.black),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void sendAndGetResult(String name, int? id, String avatar,
      BuildContext context, String astroId, String type, int? free_chat) async {
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MultiProvider(providers: [
              ListenableProvider(create: (context) => ListOfApisProvider()),
              // ListenableProvider(create: (context) => WaitListNotifier()),
            ], child: ChatIntakeForm(name, id, astroId, type, free_chat))));
    TLoggerHelper.warning(free_chat.toString());
    if (result == "true") {
      setState(() {});
      showConfirmationDialog(name, avatar);
    }
  }

  void reportAndBlock() {
    final TextEditingController controller = TextEditingController();
    ValueNotifier downloadProgressNotifier = ValueNotifier(0);
    showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return ListenableProvider(
            create: (context) => ListOfApisProvider(),
            child: Dialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.close, size: 20),
                      ),
                    ),
                    Text(
                      "Report & Block".tr(),
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 5),
                    Card(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: CircleAvatar(
                        radius: 30,
                        backgroundImage:
                            CachedNetworkImageProvider(astrologer.avatar),
                      ),
                      shape: CircleBorder(
                          side: BorderSide(color: AppColor.appColor, width: 2)),
                    ),
                    SizedBox(height: 5),
                    Text(
                      TFormatter.capitalizeSentence(astrologer.name),
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 5),
                    Text.rich(
                      TextSpan(
                        children: <TextSpan>[
                          TextSpan(text: "Reason for blocking".tr()),
                          TextSpan(
                            text: "*",
                            style: GoogleFonts.poppins(
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    Container(
                        height: 100,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            shape: BoxShape.rectangle,
                            border: Border.all(color: Colors.black, width: 1)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          child: TextFormField(
                              onChanged: (value) {
                                if (value.isNotEmpty)
                                  downloadProgressNotifier.value = 1;
                                else
                                  downloadProgressNotifier.value = 0;
                              },
                              keyboardType: TextInputType.multiline,
                              controller: controller,
                              minLines: 1,
                              //Normal textInputField will be displayed
                              maxLines: 5,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: "Write your reason...".tr(),
                                labelStyle: GoogleFonts.poppins(
                                    color: AppColor.blackColor, fontSize: 14),
                                hintStyle: GoogleFonts.poppins(fontSize: 14),
                              )),
                        )),
                    SizedBox(height: 10),
                    ValueListenableBuilder(
                        valueListenable: downloadProgressNotifier,
                        builder: (context, value, snapshot) {
                          return SizedBox(
                            child: TextButton(
                              onPressed: () {
                                if (downloadProgressNotifier.value > 0) {
                                  Navigator.pop(context);
                                  Provider.of<ListOfApisProvider>(context,
                                          listen: false)
                                      .submitBlockReport(
                                          astrologerId:
                                              astrologer.astrologer.userId,
                                          reason: controller.text,
                                          blockStatus: true)
                                      .then(
                                    (val) {
                                      if (val) {
                                        setState(() {
                                          popupMenu = ['UnBlock'];
                                          isBlockedUser = !isBlockedUser;
                                        });
                                        Fluttertoast.showToast(
                                            msg:
                                                "${TFormatter.capitalizeSentence(astrologer.name)} Blocked");
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: "Some error occured".tr() +
                                                " !!");
                                      }
                                    },
                                  );
                                }
                              },
                              style: TextButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  backgroundColor:
                                      downloadProgressNotifier.value == 0
                                          ? AppColor.lightGrey
                                          : AppColor.appColor),
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Text(
                                  "Submit".tr(),
                                  style: GoogleFonts.poppins(
                                      color: AppColor.darkGrey),
                                ),
                              ),
                            ),
                          );
                        })
                  ],
                ),
              ),
            ),
          );
        });
  }

  showConfirmationDialog(String astrologerName, String astrologerImage) async {
    String userName = await THelperFunctions.getUserName();
    String userAvatar = await THelperFunctions.getUserAvatar();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: (MediaQuery.of(context).size.height / 2) - 50,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Column(
                          children: [
                            Card(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: CircleAvatar(
                                radius: 30,
                                backgroundImage:
                                    CachedNetworkImageProvider(userAvatar),
                              ),
                              shape: CircleBorder(
                                  side: BorderSide(
                                      color: AppColor.appColor, width: 2)),
                            ),
                            Text(
                              userName,
                              style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        Expanded(
                            child: DashSeparator(
                          color: Colors.grey,
                          height: 2.0,
                        )),
                        Column(
                          children: [
                            Card(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: CircleAvatar(
                                radius: 30,
                                backgroundImage: CachedNetworkImageProvider(
                                    "$astrologerImage"),
                              ),
                              shape: CircleBorder(
                                  side: BorderSide(
                                      color: AppColor.appColor, width: 2)),
                            ),
                            Text(
                              "${astrologerName}",
                              style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                        "You will be connecting with".tr() +
                            " $astrologerName " +
                            "in a while.".tr(),
                        style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "While you wait for".tr() +
                          " $astrologerName" +
                          " you may also explore other astrologers and join their wishlist."
                              .tr(),
                      style: GoogleFonts.poppins(
                          color: Colors.black54, fontSize: 15),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            backgroundColor: AppColor.appColor),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "OK".tr(),
                            style: GoogleFonts.poppins(color: Colors.black),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<void> _rechargeDialog(
      BuildContext context, String s, int amountPerMinute,
      {bool isGift = false}) async {
    int? amID = 0;
    String? amount = "";

    await showModalBottomSheet<void>(
      context: context,
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: Consumer<ListOfApisProvider>(builder: (context, provider, _) {
            // if (state.isLoading) {
            //   return Center(
            //     child: CustomCircularProgressIndicator(),
            //   );
            // }
            // var rechargeData = state.rechargeAmountModel.recharge ?? [];
            // log("Recharge Am --> ${rechargeData.length}");
            // ;
            return FutureBuilder(
              future: provider.getRechargeAmountPProvider(context: context),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CustomCircularProgressIndicator(),
                  );
                } else if (snapshot.hasData) {
                  RechargeAmountModel rechargeAmountModel = snapshot.data;
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topRight,
                            child: CloseButton()),
                        if (!isGift)
                          Text(
                            "Minimum balance of 5 minutes".tr() +
                                " (INR ${amountPerMinute * 5}.0) " +
                                "is required to start chat with".tr() +
                                " (${TFormatter.capitalizeSentence(astrologer.name)})",
                            textAlign: TextAlign.start,
                            style: GoogleFonts.poppins(
                                fontSize: 14, color: Colors.redAccent),
                          ),
                        SizedBox(height: 10),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Recharge Now'.tr(),
                            style: GoogleFonts.poppins(fontSize: 17),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.light_mode_rounded,
                              color: AppColor.appColor,
                            ),
                            Text("Tip: 90% user recharge for 10 mins or more"
                                .tr())
                          ],
                        ),
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: GridView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: rechargeAmountModel.recharge!.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 4,
                                      childAspectRatio: 6 / 2,
                                      mainAxisSpacing: 10,
                                      crossAxisSpacing: 10),
                              itemBuilder: (context, index) {
                                if (index == 0) {
                                  amID =
                                      rechargeAmountModel.recharge![index].id;
                                  amount = rechargeAmountModel
                                      .recharge![index].price;
                                }
                                return InkWell(
                                  onTap: () async {
                                    int customerID = int.parse(
                                        await THelperFunctions.getUserId());
                                    if (customerID == 0) {
                                      // Navigator.of(context).push(
                                      //     SlideRightRoute(page: LoginScreen()));
                                      context
                                          .pushNamed(RouteConstants.authScreen);
                                    } else {
                                      Navigator.of(context)
                                          .push(
                                            SlideRightRoute(
                                              page: ListenableProvider(
                                                create: (context) =>
                                                    ListOfApisProvider(),
                                                child: PaymentInformationScreen(
                                                    userInfoId: "",
                                                    title:
                                                        "Recharge Amount".tr(),
                                                    astroName: astrologer.name,
                                                    price: rechargeAmountModel
                                                        .recharge![index]
                                                        .price!,
                                                    amountID:
                                                        rechargeAmountModel
                                                            .recharge![index]
                                                            .id!,
                                                    from: s,
                                                    astroId: astrologer
                                                        .astrologer.userId
                                                        .toString()),
                                              ),
                                            ),
                                          )
                                          .then((value) => context
                                              .read<ListOfApisProvider>()
                                              .giftDataProvider(
                                                context: context,
                                                customerID:
                                                    customerID.toString(),
                                              ));
                                      Navigator.maybePop(context);
                                    }
                                    amID =
                                        rechargeAmountModel.recharge![index].id;
                                    amount = rechargeAmountModel
                                        .recharge![index].price;
                                  },
                                  child: Container(
                                      height: 25,
                                      width: 90,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: AppColor.appColor)),
                                      child: Stack(children: <Widget>[
                                        Positioned(
                                            top: 7,
                                            right: 50.5,
                                            child: Transform.rotate(
                                                angle: -math.pi / 3.5,
                                                child: Container(
                                                    height: 15,
                                                    width: 50,
                                                    decoration: BoxDecoration(
                                                        color: Colors.green,
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    5))),
                                                    child: Center(
                                                        child: Text(
                                                            "${rechargeAmountModel.recharge![index].discountPercentage}% " +
                                                                "Extra".tr(),
                                                            style:
                                                                GoogleFonts.poppins(
                                                                    fontSize:
                                                                        8)))))),
                                        Center(
                                            child: Text(
                                                "${rechargeAmountModel.recharge![index].price}"))
                                      ])),
                                );
                              }),
                        ),
                        SizedBox(height: 20),
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton(
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              backgroundColor: AppColor.appColor,
                              textStyle: Theme.of(context).textTheme.labelLarge,
                            ),
                            child: Text(
                              'Proceed to Pay'.tr(),
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black),
                            ),
                            onPressed: () async {
                              String customerID =
                                  await THelperFunctions.getUserId();
                              String customerName =
                                  await THelperFunctions.getUserName();
                              Navigator.of(context)
                                  .push(
                                    SlideRightRoute(
                                        page: ListenableProvider(
                                      create: (context) => ListOfApisProvider(),
                                      child: PaymentInformationScreen(
                                          userInfoId: "",
                                          title: "Recharge Amount".tr(),
                                          astroName: customerName,
                                          price: amount.toString(),
                                          amountID: amID!,
                                          from: s,
                                          astroId: astrologer.astrologer.userId
                                              .toString()),
                                    )),
                                  )
                                  .then((value) => Navigator.maybePop(context));
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return SizedBox();
                }
              },
            );
          }),
        );
      },
    );
  }

  showCancelBookingDialog(String id) {
    showModalBottomSheet<void>(
      backgroundColor: Colors.white,
      context: context,
      isDismissible: false,
      enableDrag: false,
      builder: (BuildContext ctx) {
        return PopScope(
            canPop: false,
            onPopInvoked: (didPop) {
              // logic
            },
            child: ListenableProvider(
              create: (context) => ListOfApisProvider(),
              child: SizedBox(
                height: 400,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topRight,
                            child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.close, size: 30))),
                        Text('Leave Waitlist of Astrologer'.tr(),
                            style: GoogleFonts.poppins(
                                fontWeight: FontWeight.bold, fontSize: 14)),
                        SizedBox(height: 5),
                        Text(
                          'Instead of leaving the waitlist, you may also join the waitlist of these trending astrologers'
                              .tr(),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        FutureBuilder(
                          future: ListOfApisProvider().getAstrologerListNew(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return SizedBox(
                                  height: 180,
                                  child: Center(
                                      child:
                                          CustomCircularProgressIndicator()));
                            } else if (!snapshot.hasData) {
                              return SizedBox.shrink();
                            } else {
                              var astrologerData = snapshot.data!.astrologers;
                              return Container(
                                child: Column(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: 180,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: 5,
                                        itemBuilder:
                                            (BuildContext context, int i) {
                                          return GestureDetector(
                                            onTap: () {
                                              context.pushNamed(
                                                RouteConstants
                                                    .astrologerProfile,
                                                queryParameters: {
                                                  'astrologerId': astrologerData
                                                      .data[i].id
                                                      .toString(),
                                                  'type': 'chat',
                                                },
                                              );
                                              // Navigator.of(context)
                                              //     .push(SlideRightRoute(
                                              //         page: MultiProvider(
                                              //   providers: [
                                              //     ListenableProvider(
                                              //         create: (context) =>
                                              //             ListOfApisProvider())
                                              //   ],
                                              //   child: AstrologerProfile(
                                              //     type: 'chat',
                                              //     uName:
                                              //         "${astrologerData!.data[i].name}",
                                              //     urExp:
                                              //         "${astrologerData.data[i].astrologer.experienceYear}",
                                              //     uProfile:
                                              //         "${astrologerData.data[i].avatar}",
                                              //     uBio:
                                              //         "${astrologerData.data[i].astrologer.bio}",
                                              //     uPrice: astrologerData.data[i]
                                              //         .astrologer.chatPrice,
                                              //     uDiscount: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .chatDiscountPrice,
                                              //     gallery: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .gallery,
                                              //     uID:
                                              //         "${astrologerData.data[i].id.toString()}",
                                              //     isOnline: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .isOnline,
                                              //     freeChat: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .freeChat,
                                              //     orderCount: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .orderCount,
                                              //     primarySkillId: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .primarySkillId,
                                              //     consultantGallery: [],
                                              //     waitTime: astrologerData
                                              //         .data[i]
                                              //         .astrologer
                                              //         .waitTime!,
                                              //   ),
                                              // )));
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              width: 120,
                                              child: Card(
                                                color: Colors.white,
                                                elevation: 1,
                                                clipBehavior:
                                                    Clip.antiAliasWithSaveLayer,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                6.0))),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Stack(
                                                      children: [
                                                        Center(
                                                          child: Card(
                                                            clipBehavior: Clip
                                                                .antiAliasWithSaveLayer,
                                                            child: CircleAvatar(
                                                              radius: 40,
                                                              backgroundImage:
                                                                  CachedNetworkImageProvider(
                                                                      astrologerData!
                                                                          .data[
                                                                              i]
                                                                          .avatar),
                                                            ),
                                                            shape: CircleBorder(
                                                              side: BorderSide(
                                                                  color: AppColor
                                                                      .appColor,
                                                                  width: 2),
                                                            ),
                                                          ),
                                                        ),
                                                        if (astrologerData!
                                                                .data[i]
                                                                .label !=
                                                            0)
                                                          Positioned(
                                                            bottom: -4,
                                                            left: 0,
                                                            right: 0,
                                                            child: Center(
                                                              child: Card(
                                                                color: Colors
                                                                    .amber,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius
                                                                                .all(
                                                                          Radius.circular(
                                                                              10.0),
                                                                        ),
                                                                        side: BorderSide(
                                                                            color:
                                                                                Colors.yellow)),
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              12,
                                                                          right:
                                                                              12,
                                                                          bottom:
                                                                              3,
                                                                          top:
                                                                              2),
                                                                  child: Text(
                                                                      TFormatter.capitalizeSentence(astrologerData!
                                                                          .data[
                                                                              i]
                                                                          .astrologer
                                                                          .labelText),
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .fade,
                                                                      style: GoogleFonts.poppins(
                                                                          fontSize:
                                                                              8,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          color:
                                                                              Colors.white)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        Positioned(
                                                          bottom: 10,
                                                          right: 0,
                                                          left: 64,
                                                          child: CircleAvatar(
                                                            radius: 7,
                                                            backgroundColor:
                                                                Colors.white,
                                                            child: CircleAvatar(
                                                              radius: 5,
                                                              backgroundColor: widget
                                                                      .isAstrologerOnline
                                                                  ? Colors.green
                                                                  : Colors
                                                                      .grey, // Green for online, grey for offline
                                                              child: Tooltip(
                                                                message: widget
                                                                        .isAstrologerOnline
                                                                    ? 'Online'
                                                                        .tr()
                                                                    : 'Offline'
                                                                        .tr(),
                                                                child:
                                                                    Container(
                                                                  height: 20,
                                                                  width: 20,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(height: 10),
                                                    Text(
                                                        TFormatter.capitalize(
                                                            astrologerData
                                                                .data[i].name),
                                                        textAlign:
                                                            TextAlign.center,
                                                        style:
                                                            GoogleFonts.poppins(
                                                                fontSize: 14),
                                                        maxLines: 2,
                                                        overflow:
                                                            TextOverflow.fade),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 5),
                                                      child: Text(
                                                          "₹ ${astrologerData.data[i].astrologer.chatPrice == 0 ? '0' : astrologerData.data[i].astrologer.chatPrice}/" +
                                                              "min".tr(),
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  fontSize: 12,
                                                                  color: Colors
                                                                          .grey[
                                                                      800])),
                                                    ),
                                                    Card(
                                                      color: Colors.green,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .all(
                                                                Radius.circular(
                                                                    10.0),
                                                              ),
                                                              side: BorderSide(
                                                                  color: Colors
                                                                      .green)),
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 25,
                                                                right: 25,
                                                                bottom: 3,
                                                                top: 2),
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          children: [
                                                            Icon(
                                                              Icons.message,
                                                              size: 12,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            SizedBox(width: 4),
                                                            Text("Chat".tr(),
                                                                style: GoogleFonts.poppins(
                                                                    fontSize:
                                                                        14,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    color: Colors
                                                                        .white)),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                ),
                              );
                            }
                          },
                        ),
                        // AstrologerListWidget(),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: ElevatedButton(
                                child: Text('Go Back'.tr(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.black)),
                                onPressed: () => Navigator.pop(context),
                              ),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                                child: ElevatedButton(
                              child: Text(
                                'Leave'.tr(),
                                style: GoogleFonts.poppins(color: Colors.black),
                              ),
                              onPressed: () async {
                                FlutterCallkitIncoming.endAllCalls();
                                await Provider.of<ListOfApisProvider>(context,
                                        listen: false)
                                    .callDismiss(communicationId: id)
                                    .whenComplete(
                                  () {
                                    leaveWaitList(id);
                                  },
                                );
                                Navigator.of(context).pop();
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppColor.appColor),
                              ),
                            ))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  nextOnlineDialog(
      {required String astrologerName,
      required String nextOnlineTime,
      required String astrologerImage,
      required String nextOnlineDate}) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        CircleAvatar(
                          radius: 35,
                          backgroundImage:
                              CachedNetworkImageProvider(astrologerImage),
                        ),
                        Wrap(
                          children: [
                            Text(
                              TFormatter.capitalizeSentence(astrologerName),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Text(
                        "You will be connecting with".tr() +
                            " $astrologerName " +
                            "in a while.".tr(),
                        style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Next Online Time of the Astrologer is".tr() +
                          " :- $nextOnlineDate $nextOnlineTime",
                      style: GoogleFonts.poppins(
                          color: Colors.black54, fontSize: 15),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          // showCancelBookingDialog(waitChatID);
                        },
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            backgroundColor: AppColor.appColor),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "OK".tr(),
                            style: GoogleFonts.poppins(color: Colors.black),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ));
  }

  leaveWaitList(String id) async {
    showLoaderDialog(context);
    Navigator.pop(context);
    var client = DioClient();
    Dio dio = await client.getClient();

    var params = jsonEncode({'id': id});

    LeaveChatModel? walletTransactionModel = await client.leaveChat(
        dio, UrlConstants.baseUrl + UrlConstants.cancelChatRequest, params);

    Fluttertoast.showToast(msg: "Leaved Successfully".tr() + " !!");
    setState(() {});
  }
}
