import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import '../main.dart';
import '../Core/Api/Constants.dart';
import '../Core/Model/login.model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Provider/authentication.provider.dart';
import '../Core/langConstant/language.constant.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../dialog/showLoaderDialog.dart';
import '../screens/OTPScreen.dart';
import '../utils/AppColor.dart';
import 'NavigationScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final otpFormKey = GlobalKey<FormState>();
  TextEditingController mobileController = TextEditingController();
  String otp = "", authStatus = "", deviceToken = "";
  String verificationId = "";

  bool getOTP = false;

  // Future<void> verifyPhoneNumberr(BuildContext context) async {
  //   await FirebaseAuth.instance.verifyPhoneNumber(
  //     phoneNumber: "+91" + mobileController.text,
  //     timeout: const Duration(seconds: 15),
  //     verificationCompleted: (AuthCredential authCredential) {
  //       setState(() {
  //         authStatus = "Your account is successfully verified";
  //       });
  //     },
  //     verificationFailed: (FirebaseAuthException authException) {
  //       setState(() {
  //         authStatus = "Authentication failed";
  //       });
  //     },
  //     codeSent: (String verId, [int? forceCodeResent]) {
  //       verificationId = verId;
  //       setState(() {
  //         authStatus = "OTP has been successfully send";
  //       });
  //       if (!mounted) return;
  //       Navigator.of(context).pop();
  //       Navigator.of(context).push(SlideRightRoute(
  //           page: OTPScreen(mobileController.text, verificationId)));
  //
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(content: Text(authStatus)),
  //       );
  //       //otpDialogBox(context).then((value) {});
  //     },
  //     codeAutoRetrievalTimeout: (String verId) {
  //       verificationId = verId;
  //       setState(() {
  //         authStatus = "TIMEOUT";
  //       });
  //     },
  //   );
  // }

  late AuthenticationProvider authenticationProvider;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await FirebaseMessaging.instance.getToken().then((token) {
        setState(() {
          deviceToken = token!;
        });
      });
    });
  }

  loginPhoneNumber(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      showLoaderDialog(context);
      authenticationProvider.login(
          context: context,
          mobileNum: mobileController.text.trim(),
          token: deviceToken);
    }
  }

  @override
  Widget build(BuildContext context) {
    authenticationProvider = Provider.of<AuthenticationProvider>(context);

    return Scaffold(
      backgroundColor: AppColor.appColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        context.go('/home');
                        if (Navigator.of(context).canPop()) {
                          Navigator.of(context).pop();
                        }
                      },
                    ),
                  ],
                ),
              ),
              Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 280,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: Image.asset('asset/images/blue_logo.png'),
                  )),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 40),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          // The validator receives the text that the user has entered.
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          controller: mobileController,
                          maxLength: 10,
                          maxLines: 1,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            errorStyle:
                                GoogleFonts.poppins(color: Colors.white),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                borderSide: BorderSide(
                                  color: AppColor.appColor,
                                  width: 1.5,
                                )),
                            filled: true,
                            fillColor: Colors.white,
                            prefixIcon: Container(
                              margin: const EdgeInsets.only(left: 10),
                              width: 80,
                              child: Row(
                                children: [
                                  Container(
                                    height: 24,
                                    width: 24,
                                    child: Image.asset('asset/images/flag.png'),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "+91 - ",
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                            hintText: "Enter Phone Number".tr(),
                            labelStyle: GoogleFonts.poppins(
                                color: AppColor.appColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                            hintStyle: GoogleFonts.poppins(fontSize: 14),
                          ),
                          validator: (value) {
                            if (value == null) {
                              return null;
                            } else if (value.isNotEmpty) {
                              bool mobileValid =
                                  RegExp(r'^[0-9]{10}$').hasMatch(value);
                              return mobileValid
                                  ? null
                                  : "Invalid mobile number".tr();
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 20),
                        ElevatedButton(
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                                minimumSize: MaterialStateProperty.all(
                                    const Size(150, 50)),
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.black),
                                // elevation: MaterialStateProperty.all(3),
                                shadowColor: MaterialStateProperty.all(
                                    Colors.transparent)),
                            onPressed: () {
                              loginPhoneNumber(context);
                              // verifyPhoneNumberr(context);
                              //userSendOTP();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Spacer(),
                                Text("Send OTP".tr(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.white)),
                                Spacer(),
                                Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                )
                              ],
                            )),
                        const SizedBox(height: 20),
                        ElevatedButton(
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                                minimumSize: MaterialStateProperty.all(
                                    const Size(150, 50)),
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.black),
                                // elevation: MaterialStateProperty.all(3),
                                shadowColor: MaterialStateProperty.all(
                                    Colors.transparent)),
                            onPressed: () {
                              context.go('/home');

                              //SendOTP();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Spacer(),
                                Text("SKIP".tr(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.white)),
                                Spacer(),
                                Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                )
                              ],
                            )),
                        const SizedBox(
                          height: 40,
                        ),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "By signing up, you agree to our".tr() + " ",
                            style: GoogleFonts.poppins(
                              color: Colors.white,
                              fontSize: 14,
                            ),
                            /*defining default style is optional */
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Terms of Use'.tr(),
                                  style: GoogleFonts.poppins(
                                      color: Colors.black,
                                      decoration: TextDecoration.underline),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      print("Terms of Use");
                                    }),
                              TextSpan(
                                text: '  ' + "and".tr() + '  ',
                                style: GoogleFonts.poppins(color: Colors.white),
                              ),
                              TextSpan(
                                  text: 'Privacy Policy'.tr(),
                                  style: GoogleFonts.poppins(
                                      color: Colors.black,
                                      decoration: TextDecoration.underline),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      print("Privacy policy");
                                    }),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // DioClient? client;

  // Future<void> userSendOTP() async {
  //   client = DioClient();
  //   Dio dio = await client!.getClient();
  //   var params = jsonEncode(
  //       {'mobile': mobileController.text, "device_token": deviceToken});
  //   LoginModel? generateOtpRes =
  //       await client?.loginModel(dio, Constants.generateOTP, params);
  //   final pref = await SharedPreferences.getInstance();
  //   if (generateOtpRes!.status == true) {
  //     loginPhoneNumber(context);
  //     verifyPhoneNumberr(context);
  //
  //     // if (mobileController.text == "9528079788") ;
  //     // pref.setString(
  //     //     Constants.userID, (generateOtpRes.data!.customer!.userId).toString());
  //     // pref.setString(Constants.userID, (generateOtpRes.data!.name).toString());
  //     // Navigator.of(context).pus
  //   } else {
  //     SnackBar(content: Text("Unable To Login"));
  //   }
  // }

  // Future<void> SendOTP() async {
  //   client = DioClient();
  //   Dio dio = await client!.getClient();
  //   var params = jsonEncode({
  //     'mobile': mobileController.text,
  //     "type": "login",
  //     "device_token": deviceToken
  //   });
  //   /*LoginModel? generateOtpRes = await client?.loginModel(dio,Constants.generateOTP,params);
  //   final prefs = await SharedPreferences.getInstance();
  //   if(generateOtpRes!.status == true){
  //     verifyPhoneNumber(context);
  //     prefs.setString(Constants.userID, (generateOtpRes.data!.customer!.id).toString());
  //   }
  //   else{
  //     if (!mounted) return;
  //     Navigator.of(context).pop();
  //     Navigator.of(context).push(SlideRightRoute(page: PersonalDetails(mobileController.text)));
  //   }*/
  // }
}
