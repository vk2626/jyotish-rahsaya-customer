import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../dialog/showLoaderDialog.dart';

import '../../utils/AppColor.dart';
import 'astrologers.assistant.dart';
import 'customer.support.dart';

class ChatSupportScreen extends StatefulWidget {
  const ChatSupportScreen({super.key});

  @override
  State<ChatSupportScreen> createState() => _ChatSupportScreenState();
}

class _ChatSupportScreenState extends State<ChatSupportScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    setState(() {
      // print(widget.selected_index);
      // currentIndex = widget.selected_index;
    });
  }

  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            title: Text("Support Chat".tr()),
            bottom: TabBar(
                onTap: (value) {
                  setState(() {
                    _tabController!.animateTo(value);
                  });
                },
                controller: _tabController!,
                tabs: [
                  Tab(text: "Customer Support".tr()),
                  Tab(text: "Astrologer's Assistant".tr()),
                ]),
          ),
          body: TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              CustomerSupport(),
              AstrologerAssistant(),
            ],
          )),
    );
  }
}
