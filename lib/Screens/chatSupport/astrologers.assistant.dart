import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../dialog/showLoaderDialog.dart';

import '../../utils/AppColor.dart';

class AstrologerAssistant extends StatefulWidget {
  const AstrologerAssistant({super.key});

  @override
  State<AstrologerAssistant> createState() => _AstrologerAssistantState();
}

class _AstrologerAssistantState extends State<AstrologerAssistant>
    with SingleTickerProviderStateMixin {
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: GestureDetector(
          onTap: () {
            print("Chat With Astrologer Assistant");
          },
          child: Container(
              color: AppColor.appColor,
              height: 50,
              width: double.infinity,
              child: Center(
                  child: Text(
                "Chat With Astrologer Assistant".tr(),
                style: TextStyle(fontSize: 15),
              ))),
        ),
        body: Center(
            child: Text("You Have Not Text Any Astrologers Assistant Yet.".tr())));
  }
}
