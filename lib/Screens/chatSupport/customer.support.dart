import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../dialog/showLoaderDialog.dart';

import '../../utils/AppColor.dart';

class CustomerSupport extends StatefulWidget {
  const CustomerSupport({super.key});

  @override
  State<CustomerSupport> createState() => _CustomerSupportState();
}

class _CustomerSupportState extends State<CustomerSupport>
    with SingleTickerProviderStateMixin {
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: GestureDetector(
          onTap: () {
            print("Chat With Astrologer Assistant");
          },
          child: Container(
              color: AppColor.appColor,
              height: 50,
              width: double.infinity,
              child: Center(
                  child: Text(
                "Chat with Customer Support".tr(),
                style: TextStyle(fontSize: 15),
              )))),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(child: Text("No Ticket Available".tr())),
        ),
      ),
    );
  }
}
