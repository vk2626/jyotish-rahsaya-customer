import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:uuid/uuid.dart';

class SupportChatScreen extends StatefulWidget {
  const SupportChatScreen({super.key});

  @override
  State<SupportChatScreen> createState() => _SupportChatScreenState();
  static TextEditingController messageTextController = TextEditingController();
}

class _SupportChatScreenState extends State<SupportChatScreen> {
  types.User bot = types.User(id: '1');
  types.User customer = types.User(id: '5582');

  List<types.Message> _messages = [];
  int index = 0;
  void _handleSendPressed(types.PartialText message) async {
    final newMessage = types.TextMessage(
        author: customer, id: index.toString(), text: message.text);
    setState(() {
      index++;
      _messages.insert(0, newMessage);
    });
    await Future.delayed(const Duration(milliseconds: 500));

    _addMessage(types.TextMessage(
        author: bot,
        createdAt: DateTime.now().millisecondsSinceEpoch,
        id: const Uuid().v4(),
        text: "Please Select Your Issue",
        metadata: {"message_type": "chat"},
        showStatus: true,
        status: types.Status.delivered));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _addMessage(types.TextMessage(
          author: bot,
          createdAt: DateTime.now().millisecondsSinceEpoch,
          id: const Uuid().v4(),
          text: "Please Select Your Issue",
          metadata: {"message_type": "chat"},
          showStatus: true,
          status: types.Status.delivered));
    });
  }

  void _addMessage(types.Message message) {
    setState(() {
      _messages.insert(0, message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Support Chat",
            style: GoogleFonts.poppins(fontSize: 18),
          ),
        ),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              'asset/images/rectangle.png',
              fit: BoxFit.cover, // Cover the entire screen
            ),
            Chat(
              messages: _messages,
              onSendPressed: _handleSendPressed,
              user: customer,
              customBottomWidget: SizedBox.shrink(),
              theme: DefaultChatTheme(
                  messageBorderRadius: 12,
                  typingIndicatorTheme: TypingIndicatorTheme(
                      animatedCirclesColor: Colors.blueAccent,
                      animatedCircleSize: 6,
                      bubbleBorder: BorderRadius.zero,
                      bubbleColor: Colors.transparent,
                      countAvatarColor: Colors.white,
                      countTextColor: Colors.white,
                      multipleUserTextStyle: TextStyle()),
                  backgroundColor: Colors.transparent,
                  seenIcon: const Icon(
                    Icons.done_all,
                    color: Colors.blue,
                    size: 18,
                  ),
                  deliveredIcon: const Icon(
                    Icons.done_all,
                    color: Colors.black,
                    size: 18,
                  ),
                  errorIcon: const Icon(
                    Icons.error,
                    color: Colors.red,
                  ),
                  sendingIcon: const Icon(Icons.timelapse_rounded,
                      color: Colors.black, size: 18),
                  inputBackgroundColor: Colors.grey.shade100,
                  attachmentButtonIcon: const Icon(Icons.attachment),
                  inputTextStyle: GoogleFonts.poppins(color: Colors.black),
                  inputTextColor: Colors.black,
                  inputBorderRadius: BorderRadius.zero),
              inputOptions: InputOptions(
                textEditingController: SupportChatScreen.messageTextController,
                enabled: false,
              ),
              textMessageBuilder: (p0,
                  {required messageWidth, required showName}) {
                if (p0.author.id == bot.id) {
                  return BotQuestionMessage(
                      isLatestMessage: _messages.indexOf(p0) == 0,
                      selectedMessage: p0.text,
                      selectedQueryFun: (selectedQuery) {
                        var textMessage = types.TextMessage(
                            author: customer,
                            createdAt: DateTime.now().millisecondsSinceEpoch,
                            id: const Uuid().v4(),
                            text: selectedQuery,
                            remoteId: p0.id,
                            metadata: {"message_type": "chat"},
                            showStatus: true,
                            status: types.Status.delivered);
                        _handleSendPressed(
                            types.PartialText(text: textMessage.text));
                      });
                } else {
                  return TextRepliedMessageWidget(
                      myColor: Colors.amber.shade50,
                      messageText: p0.text,
                      widget: widget,
                      repliedMessage: p0.text);
                }
              },
            )
          ],
        ));
  }
}

class TextRepliedMessageWidget extends StatelessWidget {
  const TextRepliedMessageWidget(
      {super.key,
      required this.myColor,
      required this.messageText,
      required this.widget,
      required this.repliedMessage});
  final Color myColor;
  final String messageText;
  final SupportChatScreen widget;

  final String repliedMessage;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: myColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
            bottomLeft: Radius.circular(12),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IntrinsicHeight(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(6)),
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.amber),
                    width: 4,
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Admin",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black87,
                                  fontSize: 12),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          "Please Select Your Issue",
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.start,
                          style: GoogleFonts.poppins(
                              color: Colors.grey, fontSize: 12),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 8),
          Text(
            messageText,
            style: GoogleFonts.poppins(fontSize: 14),
          )
        ],
      ),
    );
  }
}

class BotQuestionMessage extends StatelessWidget {
  final String selectedMessage;
  final bool isLatestMessage;
  final Function(String) selectedQueryFun;
  const BotQuestionMessage(
      {super.key,
      required this.selectedMessage,
      required this.isLatestMessage,
      required this.selectedQueryFun});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.7),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(12),
            child: Text(
              selectedMessage,
              style: GoogleFonts.poppins(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Choose One",
                  style: GoogleFonts.poppins(fontSize: 12, color: Colors.grey),
                ),
                Text(
                  "12:50 PM",
                  style: GoogleFonts.poppins(fontSize: 12, color: Colors.grey),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          ...List.generate(
            7,
            (index) => InkWell(
              onTap: () {
                if (isLatestMessage) {
                  selectedQueryFun([
                    "Change the language",
                    "Reset my password",
                    "How to use the app",
                    "Report a bug",
                    "Contact support",
                    "Upgrade my plan",
                    "Other issues"
                  ][index]);
                } else {
                  print("Not the latest message");
                }
              },
              child: Column(
                children: [
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Icon(
                          Icons.forward_outlined,
                          color: isLatestMessage
                              ? Colors.blue
                              : Colors.blue.shade100,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          [
                            "Change the language",
                            "Reset my password",
                            "How to use the app",
                            "Report a bug",
                            "Contact support",
                            "Upgrade my plan",
                            "Other issues"
                          ][index],
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            color: isLatestMessage
                                ? Colors.blue
                                : Colors.blue.shade100,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Column(
            children: [
              Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.settings_backup_restore,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text("More",
                        style: GoogleFonts.poppins(
                            fontSize: 14, color: Colors.grey)),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}
