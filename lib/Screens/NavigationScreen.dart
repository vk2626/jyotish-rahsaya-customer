import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import 'package:jyotish_rahsaya/SubScreens/free_chat_available_screen.dart';
import '../Core/Model/LeaveChatModel.dart';
import '../Core/Provider/authentication.provider.dart';
import '../Core/Provider/chatting_provider.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Core/formatter.dart';
import '../Core/helper_functions.dart';
import 'IncomingCallRequest.dart';
import 'flutter_chat_screen.dart';
import 'mallScreens/astromall_allitem_screen.dart';
import '../dialog/showLoaderDialog.dart';
import '../router_constants.dart';
import 'package:url_launcher/url_launcher.dart';
import '../Core/Model/wait_list_model.dart';
import '../Core/Provider/go_live_provider.dart';
import '../Core/logger_helper.dart';
import '../handler_classes/secure_storage.dart';
import '../utils/custom_circular_progress_indicator.dart';
import 'liveScreen/live_reel/live_reel_screen.dart';
import 'package:restart_app/restart_app.dart';
import '../Core/Model/LanguageModel.dart' as lang;
import '../Core/Model/SkillsModel.dart';
import 'LoginScreen.dart';
import 'my.following.dart';
import 'order.history.dart';
import 'search.screen.dart';
import 'setting.screen.dart';
import 'walletScreens/wallet.screen.dart';
import '../SubScreens/BookPoojaScreen.dart';
import '../SubScreens/CallScreen.dart';
import '../SubScreens/ChatList.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../SubScreens/HomeScreen.dart';
import 'ProfileScreen.dart';
import 'blogScreens/latest.blog.viewall.dart';
import 'chatSupport/chat.support.screen.dart';

ValueNotifier<bool> isApiCallInProgress = ValueNotifier(false);

class NavigationScreen extends StatefulWidget {
  const NavigationScreen({required this.pageIndex});

  final int pageIndex;

  @override
  State<StatefulWidget> createState() {
    return _NavigationScreenState();
  }
}

class DrawerItem {
  DrawerItem(this.title, this.icon);

  String icon;
  String title;
}

Widget SocialMedialIcon(String icon) {
  return Column(
      children: [InkWell(onTap: () {}, child: Image.asset(icon, height: 30))]);
}

class BottomItems {
  BottomItems(this.title, this.icon);

  String icon;
  String title;
}

class _NavigationScreenState extends State<NavigationScreen> {
  final PageController _pageController = PageController(initialPage: 0);
  List<String> SkillsArrayList = [];
  List<Widget> body = [];
  DioClient? client;
  int customerId = 0;
  late List<DrawerItem> drawerItems;
  late List<Widget> drawerScreensList;
  late final SharedPreferences prefs;
  String firstLetter = "";
  var generatedColor = math.Random().nextInt(Colors.primaries.length);
  final iconList = [
    BottomItems("Home".tr(), "asset/images/home_fill.png"),
    BottomItems("Chat".tr(), "asset/images/chat.png"),
    BottomItems("Live".tr(), "asset/images/live.png"),
    BottomItems("Call".tr(), "asset/images/call.png"),
    BottomItems("Pooja".tr(), "asset/images/pooja.png"),
  ];
  final List<String> iconTitleList = [
    "Home",
    "Chat",
    "Live",
    "Call",
    "Pooja",
  ];

  bool isDrawerLoading = true;
  List<String> langArrayList = [];
  List<lang.Data> langModelList = [];
  String mobileNumber = "";
  String name = "";
  String profileImage = "";
  List<String> selectedCountry = [];
  List<String> selectedGender = [];
  bool selectedIndex = false;
  List<String> selectedLng = [];
  List<String> selectedSkills = [];
  List<bool> selectedSortBy = [];
  List<String> selectedSortByOption = [];
  bool showAllAttendance = false;
  List<Data> skillModelList = [];
  List<String> sorFilterList = [
    "Sort by".tr(),
    "Skill".tr(),
    "Language".tr(),
    "Gender".tr(),
    "Country".tr(),
    "Offer".tr()
  ];

  String title = "Jyotish Rahsaya";
  String userId = "";
  WaitListModel? waitListModel;

  int _bottomNavIndex = 0;
  List<List<String>> _filterScreen = [
    [
      'Experience : High to Low',
      'Experience : Low to High',
      'Orders : High to Low',
      'Orders : Low to High',
      'Price : High to Low',
      'Price : Low to High',
      'Rating : High to Low'
    ],
    [
      'Experience : High to Low',
      'Experience : Low to High',
      'Orders : High to Low',
      'Orders : Low to High',
      'Price : High to Low',
      'Price : Low to High',
      'Rating : High to Low'
    ],
    [
      'Experience : High to Low',
      'Experience : Low to High',
      'Orders : High to Low',
      'Orders : Low to High',
      'Price : High to Low',
      'Price : Low to High',
      'Rating : High to Low'
    ],
    [
      'Experience : High to Low',
      'Experience : Low to High',
      'Orders : High to Low',
      'Orders : Low to High',
      'Price : High to Low',
      'Price : Low to High',
      'Rating : High to Low'
    ],
    [
      'Experience : High to Low',
      'Experience : Low to High',
      'Orders : High to Low',
      'Orders : Low to High',
      'Price : High to Low',
      'Price : Low to High',
      'Rating : High to Low'
    ],
  ];

  int _savePrevIndex = 0;
  int _selectedDrawerIndex = 0;
  int _selectedIndex = 0;
  /************************* Filter Section **********************/
  /* Filter Check Box */
  List<String> _selectedItems = [];

  // notificationInitialize() async {
  //   NotificationHandlerLocal.initializeNotifications(context);

  //   await FirebaseMessaging.instance
  //       .getToken()
  //       .then((value) => TLoggerHelper.info("FCM Token:- ${value!}"));
  // }

  @override
  initState() {
    super.initState();
    getUserId();
    getSkillArrayList();
    // notificationInitialize();

    drawerScreensList = [
      SizedBox(),
      SizedBox(),
      SizedBox(),
      SizedBox(),
      ChatSupportScreen(),
      OrderHistoryScreen(selected_index: 0),
      OrderHistoryScreen(selected_index: 1),
      ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: LatestBlogViewAllScreen()),
      ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: AstroMallAllItemScreen(onChangedItem: (value) {})),
      // TODO: Free Services needs to be removed below or added
      ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: AstroMallAllItemScreen(onChangedItem: (value) {})),
      ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: MyFollowingScreen()),
      // BuyMemberShip(),
      SettingScreen(),

      MultiProvider(providers: [
        ListenableProvider(create: (context) => ListOfApisProvider()),
        ListenableProvider(create: (context) => AuthenticationProvider())
      ], child: LoginScreen())
    ];
    initializeDrawer();
    startPolling();
    initprefs();
  }

  int isFreeFlag = 1;
  initprefs() async {
    prefs = await SharedPreferences.getInstance();
    String userId = await THelperFunctions.getUserId();
    int? value = prefs.getInt(Constants.isFreeFlag);
    // int? value = 0;
    if ((value != null && value == 0) && userId != "0") {
      showModalBottomSheet(
          context: context,
          isDismissible: false,
          isScrollControlled: true,
          enableDrag: false,
          builder: (context) => FreeChatAvailableScreen()).then((value) {
        Navigator.of(context).pop();
      });
    }
  }

  void startPolling() async {
    while (mounted) {
      await waitListApi();
      await Future.delayed(Duration(seconds: 10));
    }
  }

  Future<void> waitListApi() async {
    if (isApiCallInProgress.value) return; // Prevent multiple calls

    isApiCallInProgress.value = true;

    if ((await THelperFunctions.getUserId() == "0")) {
      isApiCallInProgress.value = false;
      return;
    }

    try {
      final value = await THelperFunctions.checkWaitList();
      if (mounted) {
        setState(() {
          waitListModel = value;
        });
      }
    } catch (error) {
      print("Error fetching waitlist: $error");
    } finally {
      isApiCallInProgress.value = false;
    }
  }

  initializeDrawer() async {
    log("Initializing Everything again");
    drawerItems = [
      DrawerItem("Home".tr(), "asset/images/home_fill.png"),
      DrawerItem("Chat with Astrologer".tr(), "asset/images/chat.png"),
      DrawerItem("Call with Astrologer".tr(), "asset/images/call.png"),
      DrawerItem("Book a Pooja".tr(), "asset/images/pooja.png"),
      DrawerItem("Customer Support Chat".tr(), "asset/images/support.png"),
      DrawerItem("Wallet Transaction".tr(), "asset/images/wallet.png"),
      DrawerItem("Order History".tr(), "asset/images/order_history.png"),
      DrawerItem("Astrology Blog".tr(), "asset/images/blog.png"),
      DrawerItem("AstroMall".tr(), "asset/images/shopping_bag.png"),
      DrawerItem("Free Services".tr(), "asset/images/services.png"),
      DrawerItem("My Following".tr(), "asset/images/following.png"),
      // DrawerItem("Buy Membership".tr(), "asset/images/premium.png"),
      DrawerItem("Setting".tr(), "asset/images/settings.png"),
      DrawerItem(
        ((await THelperFunctions.getUserId() == "0"))
            ? ("Login".tr())
            : ("Logout".tr()),
        "asset/images/login.png",
      ),
    ];
    setState(() {
      isDrawerLoading = false;
    });
    body = [
      MultiProvider(
        providers: [
          ListenableProvider(create: (context) => GoLiveProvider()),
          ListenableProvider(create: (context) => ListOfApisProvider())
        ],
        child: HomeScreen(
          onChangedItem: (int id) {
            setState(() {
              _bottomNavIndex = id;
              _pageController.jumpToPage(_bottomNavIndex);
            });
          },
        ),
      ),
      ChatList(
          selectedSkills: selectedSkills,
          selectedCountry: selectedCountry,
          selectedGender: selectedGender,
          selectedLng: selectedLng,
          selectedSortBy: selectedSortByOption),
      SizedBox(),
      CallScreen(
          selectedSkills: selectedSkills,
          selectedCountry: selectedCountry,
          selectedGender: selectedGender,
          selectedLng: selectedLng,
          selectedSortBy: selectedSortByOption),
      BookPoojaScreen()
    ];
  }

  showCancelBookingDialog(String id) {
    showModalBottomSheet<void>(
      backgroundColor: Colors.white,
      context: context,
      isDismissible: false,
      enableDrag: false,
      builder: (BuildContext ctx) {
        return PopScope(
            canPop: false,
            onPopInvoked: (didPop) {
              // logic
            },
            child: ListenableProvider(
              create: (context) => ListOfApisProvider(),
              child: SizedBox(
                height: 400,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topRight,
                            child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.close, size: 30))),
                        Text("Leave Waitlist of Astrologer".tr(),
                            style: GoogleFonts.poppins(
                                fontWeight: FontWeight.bold, fontSize: 14)),
                        SizedBox(height: 5),
                        Text(
                          "Instead of leaving the waitlist, you may also join the waitlist of these trending astrologers",
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        FutureBuilder(
                          future: ListOfApisProvider().getAstrologerListNew(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return SizedBox(
                                  height: 180,
                                  child: Center(
                                      child:
                                          CustomCircularProgressIndicator()));
                            } else if (!snapshot.hasData) {
                              return SizedBox.shrink();
                            } else {
                              var astrologerData = snapshot.data!.astrologers;
                              return Container(
                                child: Column(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: 180,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: 5,
                                        itemBuilder:
                                            (BuildContext context, int i) {
                                          return GestureDetector(
                                            onTap: () {
                                              context.pushNamed(
                                                RouteConstants
                                                    .astrologerProfile,
                                                queryParameters: {
                                                  'astrologerId': astrologerData
                                                      .data[i].id
                                                      .toString(),
                                                  'type': 'chat',
                                                },
                                              );
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              width: 120,
                                              child: Card(
                                                color: Colors.white,
                                                elevation: 1,
                                                clipBehavior:
                                                    Clip.antiAliasWithSaveLayer,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                6.0))),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Stack(
                                                      children: [
                                                        Center(
                                                          child: Card(
                                                            clipBehavior: Clip
                                                                .antiAliasWithSaveLayer,
                                                            child: CircleAvatar(
                                                              radius: 40,
                                                              backgroundImage:
                                                                  CachedNetworkImageProvider(
                                                                      astrologerData!
                                                                          .data[
                                                                              i]
                                                                          .avatar),
                                                            ),
                                                            shape: CircleBorder(
                                                              side: BorderSide(
                                                                  color: AppColor
                                                                      .appColor,
                                                                  width: 2),
                                                            ),
                                                          ),
                                                        ),
                                                        if (astrologerData
                                                                .data[i]
                                                                .label !=
                                                            0)
                                                          Positioned(
                                                            bottom: -4,
                                                            left: 0,
                                                            right: 0,
                                                            child: Center(
                                                              child: Card(
                                                                color: Colors
                                                                    .amber,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius
                                                                                .all(
                                                                          Radius.circular(
                                                                              10.0),
                                                                        ),
                                                                        side: BorderSide(
                                                                            color:
                                                                                Colors.yellow)),
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              12,
                                                                          right:
                                                                              12,
                                                                          bottom:
                                                                              3,
                                                                          top:
                                                                              2),
                                                                  child: Text(
                                                                      TFormatter.capitalizeSentence(astrologerData
                                                                          .data[
                                                                              i]
                                                                          .astrologer
                                                                          .labelText),
                                                                      style: GoogleFonts.poppins(
                                                                          fontSize:
                                                                              8,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          color:
                                                                              Colors.white)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        Positioned(
                                                          bottom: 10,
                                                          right: 0,
                                                          left: 64,
                                                          child: CircleAvatar(
                                                            radius: 7,
                                                            child: CircleAvatar(
                                                              radius: 5,
                                                              backgroundColor: astrologerData
                                                                          .data[
                                                                              i]
                                                                          .isAstrologerOnline ==
                                                                      1
                                                                  ? Colors.green
                                                                  : Colors
                                                                      .grey, // Green for online, grey for offline
                                                              child: Tooltip(
                                                                message: astrologerData
                                                                            .data[
                                                                                i]
                                                                            .isAstrologerOnline ==
                                                                        1
                                                                    ? 'Online'
                                                                        .tr()
                                                                    : 'Offline'
                                                                        .tr(),
                                                                child:
                                                                    Container(
                                                                  height: 20,
                                                                  width: 20,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(height: 10),
                                                    Text(
                                                        TFormatter.capitalize(
                                                            astrologerData!
                                                                .data[i].name),
                                                        textAlign:
                                                            TextAlign.center,
                                                        style:
                                                            GoogleFonts.poppins(
                                                                fontSize: 14),
                                                        maxLines: 2,
                                                        overflow:
                                                            TextOverflow.fade),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 5),
                                                      child: Text(
                                                          "₹ ${astrologerData.data[i].astrologer.chatPrice == 0 ? '0' : astrologerData.data[i].astrologer.chatPrice}/" +
                                                              "min".tr(),
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  fontSize: 12,
                                                                  color: Colors
                                                                          .grey[
                                                                      800])),
                                                    ),
                                                    Card(
                                                      color: Colors.green,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .all(
                                                                Radius.circular(
                                                                    10.0),
                                                              ),
                                                              side: BorderSide(
                                                                  color: Colors
                                                                      .green)),
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 25,
                                                                right: 25,
                                                                bottom: 3,
                                                                top: 2),
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          children: [
                                                            Icon(
                                                              Icons.message,
                                                              size: 12,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            SizedBox(width: 4),
                                                            Text("Chat".tr(),
                                                                style: GoogleFonts.poppins(
                                                                    fontSize:
                                                                        14,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    color: Colors
                                                                        .white)),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                ),
                              );
                            }
                          },
                        ),
                        // AstrologerListWidget(),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: ElevatedButton(
                                child: Text("Go Back".tr(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.black)),
                                onPressed: () => Navigator.pop(context),
                              ),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                                child: ElevatedButton(
                              child: Text(
                                "Leave".tr(),
                                style: GoogleFonts.poppins(color: Colors.black),
                              ),
                              onPressed: () async {
                                FlutterCallkitIncoming.endAllCalls();
                                await Provider.of<ListOfApisProvider>(context,
                                        listen: false)
                                    .callDismiss(communicationId: id)
                                    .whenComplete(
                                  () {
                                    leaveWaitList(id);
                                  },
                                );
                                Navigator.of(context).pop();
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppColor.appColor),
                              ),
                            ))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  leaveWaitList(String id) async {
    showLoaderDialog(context);
    if (Navigator.of(context).canPop()) {
      Navigator.pop(context);
    }
    client = DioClient();
    Dio dio = await client!.getClient();

    var params = jsonEncode({'id': id});
    String api = UrlConstants.cancelChatRequest;
    final String url = UrlConstants.baseUrl + api;
    LeaveChatModel? walletTransactionModel =
        await client?.leaveChat(dio, url, params);

    Fluttertoast.showToast(msg: "Leaved Successfully !!");
    setState(() {});
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    // bool checkValue = prefs.containsKey(Constants.userID);
    if (prefs.getInt(Constants.userID) != null) {
      userId = await THelperFunctions.getUserId();
      customerId = int.parse(await THelperFunctions.getUserId());
      TLoggerHelper.info("UserID:- ${userId} CustomerID:- ${customerId}");
      if (prefs.getString(Constants.fullName) != null ||
          prefs.getString(Constants.profileImage) != null) {
        name = prefs.getString(Constants.fullName) ?? "JR User";
        profileImage = prefs.getString(Constants.profileImage) ?? "";

        firstLetter = name.isNotEmpty ? "${name[0]}" : "J";
      } else {
        name = "JR User";
        firstLetter = "J";
      }
    } else {
      setState(() {
        userId = userId;
        firstLetter = "G";
        name = "Hello Guest User".tr();
      });
    }
    setState(() async {
      userId = await THelperFunctions.getUserId();
      print("******************** UserId:-- ${userId}");
      drawerItems = drawerItems;
    });
    // _getDrawerItemWidget(widget.page);
  }

  void showLogoutDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false, // User must tap a button to close the dialog

      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
          title: Text(
            'Do you want to logout?'.tr(),
            style: GoogleFonts.poppins(color: Colors.black, fontSize: 14),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
                // Handle the "No" action here if needed
              },
              child: Text(
                'No'.tr(),
                style: GoogleFonts.poppins(color: AppColor.appColor),
              ),
            ),
            TextButton(
              onPressed: () async {
                final prefs = await SharedPreferences.getInstance();
                await prefs.clear();
                await SecureStorage().clearAll();
                // Navigate to the login screen
                // context.goNamed(RouteConstants.mainhomescreen);
                // if (Navigator.of(context).canPop()) {
                //   Navigator.of(context).pop();
                // }
                THelperFunctions.restartApp();
              },
              child: Text(
                'Yes'.tr(),
                style: GoogleFonts.poppins(color: AppColor.appColor),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> getSkillArrayList() async {
    client = DioClient();
    Dio dio = await client!.getClient();

    SkillsModel? skillsModel =
        await client?.skills(dio, UrlConstants.baseUrl + UrlConstants.skills);
    if (skillsModel!.status == true) {
      skillModelList = skillsModel.data!;
      for (var i = 0; i < skillModelList.length; i++) {
        SkillsArrayList.add(skillModelList[i].skill.toString());
        selectedSortBy.add(false);
      }
      _filterScreen.insert(1, SkillsArrayList);
    }
    getLanguages();
  }

  Future<void> getLanguages() async {
    client = DioClient();
    Dio dio = await client!.getClient();

    lang.LanguageModel? languageModel = await client?.languages(
        dio, UrlConstants.baseUrl + UrlConstants.language);
    setState(() {
      if (languageModel!.status == true) {
        langModelList = languageModel.data!;
        for (var i = 0; i < langModelList.length; i++) {
          langArrayList.add(langModelList[i].language.toString());
        }
        //sorFilterList
        _filterScreen.insert(2, langArrayList);
        // print("_filterScreen1---> $_filterScreen");
        WidgetsBinding.instance.addPostFrameCallback((_) {
          _filterScreen.insert(3, ['Male', 'Female']);
          _filterScreen.insert(4, ['India', 'Outside India']);
          _filterScreen.insert(5, ['Active', 'Not Active']);
        });
        // print("_filterScreen2---> $_filterScreen");
      }
    });
  }

  void sendToLogin() async {
    final prefs = await SharedPreferences.getInstance();
    client = DioClient();
    Dio dio = await client!.getClient();
    var params =
        jsonEncode({'id': await prefs.getInt(Constants.userID).toString()});
    // String api = "${UrlConstants.baseUrl}user-logout";
    Response response = await dio
        .post(UrlConstants.baseUrl + UrlConstants.userLogout, data: params);
    final Map<String, dynamic> parseData = response.data;
    if (parseData['status']) {
      prefs.clear();
      // prefs.remove(Constants.fullName);
      // prefs.remove(Constants.profileImage);
      setState(() {
        _selectedDrawerIndex = 0;
        _bottomNavIndex = 0;
        // _getDrawerItemWidget(0);
        getUserId();
      });

      await Restart.restartApp();
    }
  }

  Future<void> _filterBottomSheet(BuildContext context) async {
    await showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
      ),
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, StateSetter setState) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Sort & Filter",
                      style: GoogleFonts.poppins(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: CloseButton(),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: [
                            ...List.generate(
                              sorFilterList.length,
                              (i) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _selectedIndex = i;
                                    });
                                  },
                                  child: Container(
                                    width: 150,
                                    color: _selectedIndex == i
                                        ? Colors.white
                                        : Colors.grey[300],
                                    height: 40,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: _selectedIndex == i
                                                    ? Colors.black
                                                    : AppColor.grey,
                                                width: 3),
                                          ),
                                        ),
                                        SizedBox(width: 5),
                                        Text(
                                          sorFilterList[i],
                                          style: GoogleFonts.poppins(),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            )
                          ],
                        ),
                        const VerticalDivider(thickness: 1, width: 2),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ...List.generate(
                                _filterScreen[_selectedIndex].length,
                                (i) {
                                  return Padding(
                                    padding: EdgeInsets.all(5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          child: Checkbox(
                                            materialTapTargetSize:
                                                MaterialTapTargetSize
                                                    .shrinkWrap,
                                            value: _selectedIndex == 0
                                                ? selectedSortBy[i]
                                                : _selectedItems.contains(
                                                    _filterScreen[
                                                        _selectedIndex][i]),
                                            onChanged: (bool? value) {
                                              setState(() {
                                                if (_selectedIndex == 0) {
                                                  for (var j = 0;
                                                      j < selectedSortBy.length;
                                                      j++) {
                                                    selectedSortBy[j] = false;
                                                  }
                                                  selectedSortBy[i] = value!;

                                                  selectedSortByOption.clear();
                                                  selectedSortByOption.add(
                                                      _filterScreen[
                                                          _selectedIndex][i]);
                                                } else {
                                                  if (value!) {
                                                    _selectedItems.add(
                                                        _filterScreen[
                                                            _selectedIndex][i]);
                                                    if (_selectedIndex == 1) {
                                                      selectedSkills.add(
                                                          skillModelList[i]
                                                              .id
                                                              .toString());
                                                    }
                                                    if (_selectedIndex == 2) {
                                                      selectedLng.add(
                                                          langModelList[i]
                                                              .id
                                                              .toString());
                                                    }
                                                    if (_selectedIndex == 3) {
                                                      selectedGender.add(
                                                          _filterScreen[
                                                                  _selectedIndex]
                                                              [i]);
                                                    }
                                                    if (_selectedIndex == 4) {
                                                      selectedCountry.add(
                                                          _filterScreen[
                                                                  _selectedIndex]
                                                              [i]);
                                                    }
                                                  } else {
                                                    _selectedItems.remove(
                                                        _filterScreen[
                                                            _selectedIndex][i]);

                                                    if (_selectedIndex == 1) {
                                                      selectedSkills.remove(
                                                          skillModelList[i]
                                                              .id
                                                              .toString());
                                                    }
                                                    if (_selectedIndex == 2) {
                                                      selectedLng.remove(
                                                          langModelList[i]
                                                              .id
                                                              .toString());
                                                    }
                                                    if (_selectedIndex == 3) {
                                                      selectedGender.remove(
                                                          _filterScreen[
                                                                  _selectedIndex]
                                                              [i]);
                                                    }
                                                    if (_selectedIndex == 4) {
                                                      selectedCountry.remove(
                                                          _filterScreen[
                                                                  _selectedIndex]
                                                              [i]);
                                                    }
                                                  }
                                                }
                                              });
                                            },
                                            side: const BorderSide(
                                                color: Colors.black),
                                            fillColor: MaterialStateProperty
                                                .resolveWith<Color?>(
                                              (Set<MaterialState> states) {
                                                if (states.contains(
                                                    MaterialState.selected)) {
                                                  return AppColor.appColor;
                                                }
                                                return const Color(0xffE6E6E6);
                                              },
                                            ),
                                          ),
                                          height: 24,
                                          width: 24,
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          _filterScreen[_selectedIndex][i],
                                          style: GoogleFonts.poppins(),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              )
                            ])
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          selectedSkills.clear();
                          selectedCountry.clear();
                          selectedGender.clear();
                          selectedLng.clear();
                          selectedSortByOption.clear();
                          ChatList.refreshFunction.value = true;
                        });
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.all(6),
                        padding: EdgeInsets.symmetric(vertical: 14),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: AppColor.lightGrey.withOpacity(.5),
                            borderRadius: BorderRadius.circular(8)),
                        child: Text(
                          "Clear".tr(),
                          style:
                              GoogleFonts.poppins(fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          ChatList.refreshFunction.value = true;
                        });

                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.all(6),
                        padding: EdgeInsets.symmetric(vertical: 14),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: AppColor.appColor,
                            borderRadius: BorderRadius.circular(8)),
                        child: Text(
                          "Apply".tr(),
                          style:
                              GoogleFonts.poppins(fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // log("Rebuilding Home Screen");
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Jyotish Rahsaya".tr(),
            style: GoogleFonts.poppins(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),
          ),
          backgroundColor: AppColor.appColor,
          iconTheme: const IconThemeData(color: Colors.white),
          actions: [
            if (_bottomNavIndex == 0 || _bottomNavIndex == 4)
              IconButton(
                visualDensity: VisualDensity(horizontal: -2.0, vertical: -4.0),
                padding: EdgeInsets.zero,
                onPressed: () {
                  Navigator.of(context).push(SlideRightRoute(
                      page: ListenableProvider(
                    create: (context) => ListOfApisProvider(),
                    child: WalletScreen(),
                  )));
                },
                icon: Image.asset(
                  "asset/images/wallet.png",
                  height: 20,
                  width: 20,
                  color: Colors.white,
                ),
              ),
            if (_bottomNavIndex == 0 || _bottomNavIndex == 4)
              IconButton(
                visualDensity: VisualDensity(horizontal: -2.0, vertical: -4.0),
                padding: EdgeInsets.zero,
                onPressed: () async {
                  // Navigator.of(context).push(SlideRightRoute(
                  //     page: ListenableProvider(
                  //   create: (context) => ListOfApisProvider(),
                  //   child: ChatSupportScreen(),
                  // )));
                  var url = "https://wa.me/message/BDSLKTDWYG7WN1";
                  await launchUrl(Uri.parse(url));
                  THelperFunctions.logEvent(eventName: "chat_support_event");
                },
                icon: Image.asset(
                  "asset/images/customer_care_outline.png",
                  height: 25,
                  color: Colors.white,
                ),
              ),
            if (_bottomNavIndex == 1 || _bottomNavIndex == 3)
              IconButton(
                visualDensity: VisualDensity(horizontal: -2.0, vertical: -4.0),
                padding: EdgeInsets.zero,
                onPressed: () {
                  Navigator.of(context).push(SlideRightRoute(
                      page: ListenableProvider(
                    create: (context) => ListOfApisProvider(),
                    child: SearchScreen(),
                  )));
                },
                icon: Icon(Icons.search),
              ),
            if (_bottomNavIndex == 1)
              IconButton(
                visualDensity: VisualDensity(horizontal: -2.0, vertical: -4.0),
                padding: EdgeInsets.zero,
                onPressed: () {
                  _filterBottomSheet(context);
                },
                icon: Icon(Icons.filter_alt_sharp),
              ),
            SizedBox(width: 10)
          ],
        ),
        drawer: isDrawerLoading
            ? CustomCircularProgressIndicator()
            : Drawer(
                backgroundColor: Colors.white,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      DrawerHeader(
                        margin: EdgeInsets.zero,
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            // Close button to dismiss the drawer
                            GestureDetector(
                              onTap: () => Navigator.of(context).pop(),
                              child: const Icon(
                                Icons.close,
                                size: 25,
                                color: Color.fromARGB(255, 0, 46, 98),
                              ),
                            ),
                            const SizedBox(height: 10),
                            Row(
                              children: [
                                // Profile Image with condition for displaying placeholder or user image
                                CircleAvatar(
                                  radius: 40,
                                  backgroundImage: profileImage.isNotEmpty
                                      ? CachedNetworkImageProvider(profileImage)
                                      : null,
                                  backgroundColor:
                                      Colors.primaries[generatedColor],
                                  child: profileImage.isEmpty
                                      ? Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            firstLetter,
                                            style: GoogleFonts.poppins(
                                              color: Colors.white,
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        )
                                      : null,
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      // User name and edit button
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              name,
                                              style: GoogleFonts.poppins(
                                                color: Colors.black,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          if (userId != "0")
                                            IconButton(
                                              padding: EdgeInsets.zero,
                                              onPressed: () {
                                                Navigator.pop(context);
                                                WidgetsBinding.instance
                                                    .addPostFrameCallback((_) {
                                                  Navigator.of(context)
                                                      .push(SlideRightRoute(
                                                          page:
                                                              ProfileScreen()))
                                                      .then((value) =>
                                                          getUserId());
                                                });
                                              },
                                              icon: const Icon(Icons.edit,
                                                  size: 18),
                                            ),
                                        ],
                                      ),
                                      if (userId.isNotEmpty)
                                        Text(
                                          mobileNumber,
                                          style: GoogleFonts.poppins(
                                            color: Colors.black,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Column(
                          children: List.generate(
                        drawerItems.length,
                        (index) => ListTile(
                          horizontalTitleGap: 12,
                          dense: true,
                          leading: Image.asset(
                            drawerItems[index].icon,
                            color: selectedIndex
                                ? AppColor.appColor
                                : const Color.fromARGB(255, 96, 96, 96),
                            height: 20,
                            width: 20,
                          ),
                          title: Text(drawerItems[index].title,
                              style: GoogleFonts.poppins(
                                  color: selectedIndex
                                      ? AppColor.appColor
                                      : const Color.fromARGB(255, 96, 96, 96),
                                  fontSize: 15,
                                  fontWeight: selectedIndex
                                      ? FontWeight.w700
                                      : FontWeight.w400)),
                          selected: index == _selectedDrawerIndex,
                          onTap: () async {
                            Navigator.of(context).pop();
                            if (index > 4 && index != 12 && index != 4) {
                              Navigator.of(context).push(SlideRightRoute(
                                  page: drawerScreensList[index]));
                            } else if (index == 12) {
                              if ((await THelperFunctions.getUserId() == "0")) {
                                context.goNamed(RouteConstants.authScreen);
                              } else {
                                showLogoutDialog(context);
                              }
                            } else if (index == 4) {
                              var url = "https://wa.me/message/BDSLKTDWYG7WN1";
                              await launchUrl(Uri.parse(url));
                              THelperFunctions.logEvent(
                                  eventName: "chat_support_event");
                            } else {
                              setState(() {
                                if (index < 2) {
                                  _bottomNavIndex = index;
                                } else if (index == 2) {
                                  _bottomNavIndex = 3;
                                } else if (index == 3) {
                                  _bottomNavIndex = 4;
                                }
                                _pageController.jumpToPage(_bottomNavIndex);
                              });
                            }
                          },
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text("Also Available On".tr())),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, top: 6, bottom: 12),
                        child: Row(
                          children: [
                            SocialMedialIcon("asset/images/instagram.png"),
                            SocialMedialIcon("asset/images/fb.png"),
                            SocialMedialIcon("asset/images/linkedIn.png"),
                            SocialMedialIcon("asset/images/AppStore.png"),
                            SocialMedialIcon("asset/images/website.png"),
                            SocialMedialIcon("asset/images/Youtube.png"),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
        // bottomSheet: _bottomNavIndex == 1 || _bottomNavIndex == 3
        //     ? FutureBuilder<WaitListModel?>(
        //         future: Provider.of<ListOfApisProvider>(context, listen: false)
        //             .checkWaitList(),
        //         builder: (context, snapshot) {
        //           if (snapshot.connectionState == ConnectionState.waiting) {
        //             // return Container(
        //             //   padding: EdgeInsets.all(16),
        //             //   child: CustomCircularProgressIndicator(),
        //             // )
        //             return SizedBox.shrink();
        //           } else if (snapshot.hasError) {
        //             return SizedBox.shrink();
        //             // return Container(
        //             //   padding: EdgeInsets.all(16),
        //             //   child: Text('Error: ${snapshot.error}'),
        //             // );
        //           } else if (snapshot.hasData && snapshot.data != null) {
        //             WaitListModel waitListModel = snapshot.data!;
        //             return ;
        //           } else {
        //             return SizedBox
        //                 .shrink(); // Empty state when no data is available
        //           }
        //         },
        //       )
        //     : SizedBox.shrink(),
        bottomSheet: (waitListModel != null)
            ? IntrinsicHeight(
                child: Container(
                  color: AppColor.btnActiveColorLight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          shape: CircleBorder(
                            side:
                                BorderSide(color: AppColor.appColor, width: 2),
                          ),
                          child: CircleAvatar(
                            radius: 32,
                            backgroundImage: CachedNetworkImageProvider(
                                waitListModel?.users.avatar ?? ''),
                          ),
                        ),
                        SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              TFormatter.capitalizeSentence(
                                  waitListModel!.users.name),
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  TextSpan(
                                    text: "₹",
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[600],
                                    ),
                                  ),
                                  TextSpan(
                                    text: (waitListModel!.users.astrologer.type
                                                    .toLowerCase() ==
                                                'chat'
                                            ? "${waitListModel!.users.astrologer.chatPrice}"
                                            : "${waitListModel!.users.astrologer.callPrice}") +
                                        "/min (${TFormatter.capitalize(waitListModel!.users.astrologer.type).tr()})",
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      color: Colors.grey[600],
                                    ),
                                  ),
                                ],
                              ),
                              textAlign: TextAlign.center,
                            ),
                            if (waitListModel!.users.astrologer.status == 0)
                              Text(
                                "Wait ~ ${waitListModel!.users.astrologer.waitTime} mins",
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 14,
                                ),
                              ),
                            if (waitListModel!.users.astrologer.status == 2)
                              Text(
                                "${TFormatter.capitalize(waitListModel!.users.astrologer.type).tr()} in Progress",
                                style: GoogleFonts.poppins(
                                  color: Colors.blue,
                                  fontSize: 14,
                                ),
                              ),
                          ],
                        ),
                        Spacer(),
                        Column(
                          mainAxisAlignment:
                              waitListModel!.users.astrologer.status == 2
                                  ? MainAxisAlignment.spaceAround
                                  : MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            if (waitListModel!.users.astrologer.status == 0)
                              CircleAvatar(
                                radius: 12,
                                backgroundColor: Colors.grey[300],
                                child: InkWell(
                                  onTap: () {
                                    showCancelBookingDialog(
                                        "${waitListModel!.users.astrologer.chatRequestId}");
                                  },
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.black54,
                                    size: 14,
                                  ),
                                ),
                              ),
                            if (waitListModel!.users.astrologer.status == 2)
                              InkWell(
                                onTap: () async {
                                  if (waitListModel!.users.astrologer.type ==
                                      "chat") {
                                    Navigator.of(context).push(SlideRightRoute(
                                        page: ListenableProvider(
                                      create: (context) => ChattingProvider(),
                                      child: FlutterChatScreen(
                                        orderID: waitListModel!
                                            .users.astrologer.chatRequestId
                                            .toString(),
                                        cummID: waitListModel!
                                            .users.astrologer.chatRequestId
                                            .toString(),
                                        chat_price: int.parse(waitListModel!
                                            .users.astrologer.chatPrice),
                                        fromId:
                                            await THelperFunctions.getUserId(),
                                        toId: waitListModel!
                                            .users.astrologer.userId,
                                        // timerDuration: Duration(
                                        //     minutes: int.parse(waitListModel
                                        //         .users
                                        //         .astrologer
                                        //         .durationTime)),
                                        timerDuration: Duration(
                                                minutes: int.parse(
                                                    waitListModel!
                                                        .users
                                                        .astrologer
                                                        .durationTime)) -
                                            DateTime.now().difference(
                                                DateTime.parse(waitListModel!
                                                    .users
                                                    .astrologer
                                                    .startTime)),
                                        astrologerName:
                                            TFormatter.capitalizeSentence(
                                                waitListModel!.users.name),
                                        astrologerImage:
                                            waitListModel!.users.avatar,
                                        dob:
                                            waitListModel!.users.astrologer.dob,
                                        tob: waitListModel!
                                            .users.astrologer.dobTime,
                                        messageInfo: {},
                                        infoId: waitListModel!
                                            .users.astrologer.infoId,
                                        lat:
                                            waitListModel!.users.astrologer.lat,
                                        lon:
                                            waitListModel!.users.astrologer.lon,
                                        // addNavigation: () {},
                                      ),
                                    )));
                                  } else if (waitListModel!
                                          .users.astrologer.type ==
                                      'call') {
                                    Navigator.of(context)
                                        .push(SlideRightRoute(
                                            page: ListenableProvider(
                                                create: (context) =>
                                                    ListOfApisProvider(),
                                                child: IncomingCallRequest(
                                                    token: "",
                                                    communication_id:
                                                        waitListModel!
                                                            .users
                                                            .astrologer
                                                            .chatRequestId
                                                            .toString(),
                                                    channelName: "",
                                                    userName: waitListModel!
                                                        .users.name,
                                                    isAttended: true,
                                                    astrologerId: waitListModel!
                                                        .users.id
                                                        .toString(),
                                                    imageUrl: waitListModel!
                                                        .users.avatar,
                                                    totalCallDuration:
                                                        Duration(minutes: int.parse(waitListModel!.users.astrologer.durationTime)) -
                                                            DateTime.now().difference(
                                                                DateTime.parse(waitListModel!.users.astrologer.startTime))))))
                                        .whenComplete(() {
                                      setState(() {});
                                    });
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 4),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.green),
                                      borderRadius: BorderRadius.circular(4)),
                                  child: Text(
                                    "View".tr() +
                                        " " +
                                        TFormatter.capitalize(waitListModel!
                                                .users.astrologer.type)
                                            .tr(),
                                    style: GoogleFonts.poppins(),
                                  ),
                                ),
                              ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : SizedBox.shrink(),
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.white,
            type: BottomNavigationBarType.fixed,
            showSelectedLabels: true,
            showUnselectedLabels: true,
            currentIndex: _bottomNavIndex,
            onTap: (index) {
              if (index == 2) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MultiProvider(
                            providers: [
                              ListenableProvider(
                                  create: (context) => ListOfApisProvider()),
                              ListenableProvider(
                                create: (context) => GoLiveProvider(),
                              ),
                            ],
                            child: LiveReelScreen(),
                          )),
                );
              } else {
                setState(() {
                  _bottomNavIndex = index;
                  _selectedDrawerIndex = index;
                  _pageController.jumpToPage(_bottomNavIndex);
                });
              }
              THelperFunctions.logScreenNameEvent(
                  screenName: iconTitleList[index]);
            },
            selectedItemColor: AppColor.appColor,
            selectedLabelStyle: GoogleFonts.poppins(),
            unselectedLabelStyle: GoogleFonts.poppins(),
            unselectedItemColor: Colors.grey.shade600,
            items: List.generate(
              iconList.length,
              (index) => BottomNavigationBarItem(
                  icon: Image.asset(
                    iconList[index].icon,
                    height: 20,
                    width: 20,
                    color: _bottomNavIndex == index
                        ? AppColor.appColor
                        : Colors.grey.shade600,
                  ),
                  label: iconList[index].title),
            )),
        // body: body.isEmpty
        //     ? Center(child: CustomCircularProgressIndicator())
        //     : IndexedStack(
        //         index: _bottomNavIndex,
        //         children: body,
        //      ),
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: body,
          onPageChanged: (index) {
            setState(() {
              _bottomNavIndex = index;
              _pageController.jumpToPage(_bottomNavIndex);
            });
          },
        ));
  }
}

// class AstrologerListWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ListenableProvider(
//       create: (context) => ListOfApisProvider(),
//       child: Consumer<ListOfApisProvider>(
//         builder: (context, listOfApisProvider, child) {
//           return FutureBuilder<void>(
//             future: listOfApisProvider.getAstrologerListProvider(
//               context: context,
//               UserId: 1, // Replace with the actual user ID
//               type: 'chat',
//               pageNumber: 1,
//             ),
//             builder: (context, snapshot) {
//               if (snapshot.connectionState == ConnectionState.waiting) {
//                 return Center(
//                   child: CustomCircularProgressIndicator(),
//                 );
//               } else if (snapshot.hasError) {
//                 return Center(
//                   child: Text("Error: ${snapshot.error}"),
//                 );
//               } else if (listOfApisProvider
//                   .astrologerList.astrologers!.data.isEmpty) {
//                 return Center(
//                   child: Text("No astrologers found."),
//                 );
//               }
//               var astrologerListModel = listOfApisProvider.astrologerList;
//               return Container(
//                 width: double.infinity,
//                 height: 180,
//                 child: ListView.builder(
//                   scrollDirection: Axis.horizontal,
//                   shrinkWrap: true,
//                   // itemCount: astrologerListModel.astrologers.length.clamp(0, 5),
//                   itemCount:
//                       astrologerListModel.astrologers!.data.length.clamp(0, 5),
//                   itemBuilder: (BuildContext context, int i) {
//                     var astrologer = astrologerListModel.astrologers!.data[i];
//                     return GestureDetector(
//                       onTap: () {
//                         Navigator.of(context).push(
//                           SlideRightRoute(
//                             page: MultiProvider(
//                               providers: [
//                                 ListenableProvider(
//                                     create: (context) => ListOfApisProvider())
//                               ],
//                               child: AstrologerProfile(
//                                 type: 'chat',
//                                 uName: astrologer.name,
//                                 urExp: "${astrologer}",
//                                 uProfile: astrologer.avatar,
//                                 uBio: astrologer.astrologer.bio,
//                                 uPrice: astrologer.astrologer.chatPrice,
//                                 uDiscount:
//                                     astrologer.astrologer.chatDiscountPrice,
//                                 gallery: astrologer.astrologer.gallery,
//                                 uID: "${astrologer.id.toString()}",
//                                 isOnline: astrologer.astrologer.isOnline,
//                                 freeChat: astrologer.astrologer.freeChat,
//                                 orderCount: astrologer.astrologer.orderCount,
//                                 primarySkillId:
//                                     astrologer.astrologer.primarySkillId,
//                                 consultantGallery: [],
//                                 waitTime: astrologer.astrologer.waitTime!,
//                               ),
//                             ),
//                           ),
//                         );
//                       },
//                       child: Container(
//                         margin: EdgeInsets.only(left: 10),
//                         width: 120,
//                         child: Card(
//                           color: Colors.white,
//                           elevation: 1,
//                           clipBehavior: Clip.antiAliasWithSaveLayer,
//                           shape: RoundedRectangleBorder(
//                             borderRadius:
//                                 BorderRadius.all(Radius.circular(6.0)),
//                           ),
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: <Widget>[
//                               Center(
//                                 child: CircleAvatar(
//                                   radius: 35,
//                                   backgroundImage: CachedNetworkImageProvider(
//                                     astrologer.avatar,
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(height: 10),
//                               Text(astrologer.name,
//                                   textAlign: TextAlign.center,
//                                   style: GoogleFonts.poppins(fontSize: 14),
//                                   maxLines: 2,
//                                   overflow: TextOverflow.fade),
//                               Padding(
//                                 padding: EdgeInsets.symmetric(horizontal: 5),
//                                 child: Text(
//                                     "₹${astrologer.astrologer.chatPrice}/"+"min",
//                                     style: GoogleFonts.poppins(
//                                         fontSize: 12, color: Colors.grey[800])),
//                               ),
//                               Card(
//                                 shape: RoundedRectangleBorder(
//                                   borderRadius:
//                                       BorderRadius.all(Radius.circular(10.0)),
//                                   side: BorderSide(color: Colors.green),
//                                 ),
//                                 child: Padding(
//                                   padding: EdgeInsets.only(
//                                       left: 25, right: 25, bottom: 3, top: 2),
//                                   child: Text("Chat",
//                                       style: GoogleFonts.poppins(
//                                           fontSize: 12, color: Colors.green)),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     );
//                   },
//                 ),
//               );
//             },
//           );
//         },
//       ),
//     );
//   }
// }
