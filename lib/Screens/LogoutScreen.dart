import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../Core/langConstant/language.constant.dart';
import '../dialog/showLoaderDialog.dart';
import '../utils/AppColor.dart';

class LogoutScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _LogoutScreenState();
  }
}

class _LogoutScreenState extends State<LogoutScreen> {
  @override
  void initState() {
    super.initState();
    logout();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColor.appColor,
      statusBarBrightness: Brightness.dark,
    ));

    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "logout",
              style: TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            ),
            backgroundColor: AppColor.appColor,
            iconTheme: const IconThemeData(color: Colors.white),
          ),
        )
    );
  }

  void logout() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog<String>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 300,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                        child:  Image.asset(
                          'asset/icons/app_icon.png',width: 100,
                        )
                    ),
                    SizedBox(height: 20,),
                    Center(
                      child:  Text(
                          "logoutText",
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            color: AppColor.blackColor,
                          )
                      ),
                    ),

                    SizedBox(height: 20,),

                    Row(
                      children: [
                        SizedBox(width: 10,),
                        Expanded(child: ElevatedButton(
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  side: BorderSide(color: AppColor.appColor)
                              ),
                            ),
                            backgroundColor: MaterialStateProperty.all(Colors.white), // elevation: MaterialStateProperty.all(3),
                          ),
                          onPressed:(){
                            Navigator.pop(context);
                            Future.delayed(Duration.zero, () {
                              Navigator.pop(context);
                            });
                          },
                          child:  Text(
                            "cancel",
                            style: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                              color: AppColor.blackColor,

                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),),
                        SizedBox(width: 20,),
                        Expanded(child: ElevatedButton(
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  side: BorderSide(color: AppColor.appColor)
                              ),
                            ),
                            backgroundColor: MaterialStateProperty.all(AppColor.appColor), // elevation: MaterialStateProperty.all(3),
                          ),
                          onPressed:() async {
                            showLoaderDialog(context);
                            // sendDataLogout();
                          },
                          child:  Text(
                            "logout",
                            style:TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                              color: AppColor.whiteColor,

                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        ),
                        SizedBox(width: 10,),


                      ],

                    )

                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }

}