import 'dart:async';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import '../Core/Model/golive_messages_model.dart';
import '../Core/Provider/go_live_provider.dart';
import '../core/logger_helper.dart';

class GoLiveScreen extends StatefulWidget {
  final String token;
  final String channelName;
  final String liveId;

  const GoLiveScreen(
      {super.key,
      required this.token,
      required this.channelName,
      required this.liveId});

  @override
  State<GoLiveScreen> createState() => _GoLiveScreenState();
}

class _GoLiveScreenState extends State<GoLiveScreen> {
  final String appId = "2b2b29ac0c8b40e88eae4516659054ed";
  late final String channelName;

  // List<int> remoteUIds = [];
  late final String token;

  // late final AgoraClient client;

  late RtcEngine agoraEngine;
  bool _isJoined = true;
  int? _remoteUid;
  late Timer _timer;
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  bool muted = true;
  bool videomuted = false;
  List<Comment> commentList = [];
  late String liveId;
  TextEditingController comment = TextEditingController();

  @override
  void initState() {
    super.initState();
    token = widget.token;
    channelName = widget.channelName;
    liveId = widget.liveId;
    TLoggerHelper.info(
        "This is the token --> $token and This is the channel ----> $channelName with Live as ---> ${widget.liveId}");
    // client = AgoraClient(
    //   agoraConnectionData: AgoraConnectionData(
    //     appId: "2b2b29ac0c8b40e88eae4516659054ed",
    //     channelName: widget.channelName,
    //     tempToken: widget.token,
    //     uid: 0,
    //   ),
    // );
    _timer = Timer.periodic(Duration(seconds: 3), (timer) {
      getListData();
    });
    agoraEngine = createAgoraRtcEngine();
    initAgora();
  }

  getListData() async {
    await Provider.of<GoLiveProvider>(context, listen: false)
        .getAllMessageList(context: context, liveId: liveId.toString())
        .then((value) {
      if (value != null) {
        GoLiveMessagesModel goLiveMessagesModel = value;
        if (value.status) {
          setState(() {
            if (commentList.isEmpty) {
              commentList = goLiveMessagesModel.comments;
            } else {
              if (commentList[commentList.length - 1].commentId !=
                  value.comments[value.comments.length - 1].commentId) {
                commentList.add(value.comments[value.comments.length - 1]);
                // scroll();
              }
            }
          });
        }
      }
    });
  }

  initAgora() async {
    // await client.initialize();

    await setupVideoSDKEngine();
  }

  showMessage(String message) {
    scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  void join() async {
    await agoraEngine.startPreview();

    // Set channel options including the client role and channel profile
    ChannelMediaOptions options = const ChannelMediaOptions(
      clientRoleType: ClientRoleType.clientRoleAudience,
      channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
    );

    await agoraEngine.joinChannel(
      token: token,
      // channelId: channelName,
      channelId: channelName,
      options: options,
      uid: 0,
    );
  }

  Future<void> setupVideoSDKEngine() async {
    try {
      // SharedPreferences prefs = await SharedPreferences.getInstance();
      // prefs.setString(Constants.CurrentScreen, "Call");
      // retrieve or request camera and microphone permissions
      await [Permission.microphone, Permission.camera].request();

      //create an instance of the Agora engine

      await agoraEngine.initialize(
          const RtcEngineContext(appId: "2b2b29ac0c8b40e88eae4516659054ed"));

      await agoraEngine.enableVideo();

      // Register the event handler
      agoraEngine.registerEventHandler(
        RtcEngineEventHandler(
          onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
            showMessage(
                "Local user uid:${connection.localUid} joined the channel");
            TLoggerHelper.info(
                "Local user uid:${connection.localUid} joined the channel");
            setState(() {
              _isJoined = true;
            });
          },
          onUserJoined:
              (RtcConnection connection, int remoteUidd, int elapsed) {
            showMessage("Remote user uid:$remoteUidd joined the channel");
            TLoggerHelper.info(
                "Joined user uid:${remoteUidd} joined the channel");
            setState(() {
              _remoteUid = remoteUidd;
              TLoggerHelper.info(
                  "Yeh hai remote uid jo ke user joined hone ke baad milta hai ${remoteUidd}");
            });
          },
          onUserOffline: (RtcConnection connection, int remoteUid,
              UserOfflineReasonType reason) {
            showMessage("Remote user uid:$remoteUid left the channel");
            setState(() {
              _remoteUid = null;
              leave();
            });
          },
        ),
      );

      join();
    } on AgoraRtcException catch (e) {
      TLoggerHelper.error(e.message.toString());
    }
  }

  void leave() {
    setState(() {
      _isJoined = false;
      _remoteUid = 0;
    });
    agoraEngine.leaveChannel();
    Navigator.of(context).pop();
  }

  @override
  void dispose() async {
    await agoraEngine.leaveChannel();
    agoraEngine.release();
    super.dispose();
  }

  /// Toolbar layout
  Widget _toolbar() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RawMaterialButton(
            onPressed: _onToggleMute,
            shape: const CircleBorder(),
            elevation: 2.0,
            fillColor: muted ? Colors.blueAccent : Colors.white,
            padding: const EdgeInsets.all(12.0),
            child: Icon(
              muted ? Icons.mic : Icons.mic_off,
              color: muted ? Colors.white : Colors.blueAccent,
              size: 20.0,
            ),
          ),
          const SizedBox(height: 20),
          RawMaterialButton(
            onPressed: _onSwitchCamera,
            shape: const CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.white,
            padding: const EdgeInsets.all(12.0),
            child: const Icon(
              Icons.switch_camera,
              color: Colors.blueAccent,
              size: 20.0,
            ),
          ),
          const SizedBox(height: 20),
          RawMaterialButton(
            onPressed: _onToggleVideoMute,
            shape: const CircleBorder(),
            elevation: 2.0,
            fillColor: videomuted ? Colors.blueAccent : Colors.white,
            padding: const EdgeInsets.all(12.0),
            child: Icon(
              videomuted ? Icons.videocam_off : Icons.videocam,
              color: videomuted ? Colors.white : Colors.blueAccent,
              size: 20.0,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          RawMaterialButton(
            onPressed: () => _onCallEnd(context),
            shape: const CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.redAccent,
            padding: const EdgeInsets.all(15.0),
            child: const Icon(
              Icons.call_end,
              color: Colors.white,
              size: 20.0,
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> showExitPopup() async {
    Navigator.of(context).pop();
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: showExitPopup,
        child: SafeArea(
          child: Scaffold(
            // backgroundColor: Colors.black,
            body: Stack(
              children: [
                Center(child: _remotePreview()),
                Positioned(
                    right: 18,
                    top: 18,
                    child: Row(
                      children: [
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                          decoration: BoxDecoration(
                              color: Colors.yellow.shade600,
                              borderRadius: BorderRadius.circular(8)),
                          child: Text(
                            "LIVE",
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        InkWell(
                          onTap: leave,
                          child: Icon(
                            Icons.cancel,
                            size: 36,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    )),
                Positioned(
                  left: 18,
                  bottom: 18,
                  child: SizedBox(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height * .4,
                          width: MediaQuery.of(context).size.width * .9,
                          child: ListView.separated(
                              itemCount: commentList.length,
                              reverse: true,
                              separatorBuilder: (context, index) => SizedBox(
                                    height: 8,
                                  ),
                              itemBuilder: (context, index) {
                                final reversedIndex =
                                    commentList.length - 1 - index;
                                return Row(
                                  children: [
                                    CircleAvatar(
                                      radius: 18,
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                              commentList[reversedIndex]
                                                  .userImage),
                                    ),
                                    SizedBox(
                                      width: 12,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          commentList[reversedIndex].userName,
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .7,
                                          child: Flexible(
                                            child: Text(
                                              commentList[reversedIndex]
                                                  .comment,
                                              maxLines: 4,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              })),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            height: 42,
                            width: MediaQuery.of(context).size.width * .7,
                            child: TextField(
                              controller: comment,
                              expands: false,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                fillColor: Colors.grey.withOpacity(.3),
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 28,
                          ),
                          CircleAvatar(
                            backgroundColor: Colors.blue,
                            // Change color according to your theme
                            child: IconButton(
                              icon: const Icon(
                                Icons.send,
                                color: Colors.white,
                              ),
                              onPressed: () async {
                                if (comment.text.isNotEmpty) {
                                  await Provider.of<GoLiveProvider>(context,
                                          listen: false)
                                      .sendLiveComment(
                                          context: context,
                                          liveId: liveId.toString(),
                                          comment: comment.text)
                                      .then((val) {
                                    if (val) {
                                      setState(() {
                                        comment.clear();
                                      });
                                    }
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  )),
                )
              ],
            ),
          ),
        ));
  }

  Widget _remotePreview() {
    if (_isJoined) {
      TLoggerHelper.info(_isJoined.toString());
      return AgoraVideoView(
        controller: VideoViewController(
          // useAndroidSurfaceView: true,
          // useFlutterTexture: true,
          rtcEngine: agoraEngine,
          canvas: VideoCanvas(uid: _remoteUid),
        ),
      );
    } else {
      return const Text(
        'Join a channel',
        textAlign: TextAlign.center,
      );
    }
  }

  void _onCallEnd(BuildContext context) {
    Navigator.of(context).pop();
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    agoraEngine.enableLocalAudio(muted);
  }

  void _onToggleVideoMute() {
    setState(() {
      videomuted = !videomuted;
    });
    agoraEngine.muteLocalVideoStream(videomuted);
  }

  void _onSwitchCamera() {
    agoraEngine.switchCamera();
  }
}
