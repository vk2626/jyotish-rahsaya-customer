// import 'package:flutter/material.dart';

// class AstrotalkNewsViewAllScreen extends StatefulWidget {
//   const AstrotalkNewsViewAllScreen({super.key});

//   @override
//   State<AstrotalkNewsViewAllScreen> createState() =>
//       _AstrotalkNewsViewAllScreenState();
// }

// class _AstrotalkNewsViewAllScreenState
//     extends State<AstrotalkNewsViewAllScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Astrotalk in News"),
//       ),
//       body: LayoutBuilder(builder: (context, BoxConstraints constraints) {
//         return SingleChildScrollView(
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Column(
//               children: <Widget>[
//                 ListView.builder(
//                   physics: NeverScrollableScrollPhysics(),
//                   shrinkWrap: true,
//                   itemCount: 20,
//                   itemBuilder: (BuildContext context, int i) {
//                     return Column(
//                       children: [
//                         Container(
//                           height: 300,
//                           child: Card(
//                             elevation: 2,
//                             clipBehavior: Clip.antiAliasWithSaveLayer,
//                             shape: RoundedRectangleBorder(
//                                 borderRadius:
//                                     BorderRadius.all(Radius.circular(10.0))),
//                             child: Stack(
//                               children: [
//                                 Positioned(
//                                   child: Image.network(
//                                     fit: BoxFit.cover,
//                                     'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=1760&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
//                                   ),
//                                   right: 0,
//                                   left: 0,
//                                   top: 0,
//                                   bottom: 0,
//                                 ),
//                                 Container(
//                                   margin: EdgeInsets.only(top: 230),
//                                   color: Colors.white,
//                                   height: 70,
//                                   width: double.infinity,
//                                   child: Column(
//                                     children: <Widget>[
//                                       Text("News Title",
//                                           maxLines: 2,
//                                           overflow: TextOverflow.ellipsis,
//                                           style: TextStyle(
//                                               fontSize: 19,
//                                               color: Colors.black,
//                                               fontWeight: FontWeight.bold)),
//                                       Padding(
//                                         padding: const EdgeInsets.only(
//                                             left: 8.0, right: 8.0),
//                                         child: Row(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.spaceBetween,
//                                           children: <Widget>[
//                                             Text(
//                                               "Writer Name",
//                                               maxLines: 1,
//                                               overflow: TextOverflow.ellipsis,
//                                               style: TextStyle(
//                                                   color: Colors.black38),
//                                               // textAlign: TextAlign.center,
//                                             ),
//                                             Text(
//                                               "Date",
//                                               maxLines: 1,
//                                               overflow: TextOverflow.ellipsis,
//                                               style: TextStyle(
//                                                   color: Colors.black38),
//                                               // textAlign: TextAlign.center,
//                                             ),
//                                           ],
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       ],
//                     );
//                   },
//                 ),
//               ],
//             ),
//           ),
//         );
//       }),
//     );
//   }
// }
