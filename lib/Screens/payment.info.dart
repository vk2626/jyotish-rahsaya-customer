import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/Model/create_order_pooja_model.dart';
import '../Core/formatter.dart';
import '../Core/helper_functions.dart';
import '../router_constants.dart';
import '../Core/logger_helper.dart';

import '../Core/Model/createOrderModel.dart';

import '../Core/Model/walletPaymentOrderCreate.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Core/Services/payment.services.dart';
import '../utils/custom_circular_progress_indicator.dart';

class PaymentInformationScreen extends StatefulWidget {
  PaymentInformationScreen({
    super.key,
    required this.title,
    required this.price,
    required this.amountID,
    required this.astroName,
    required this.from,
    required this.userInfoId,
    this.astroId,
    this.productID,
    this.originalProductId,
    // this.orderID,
    this.remedy_id,
    this.suggested_astro_id,
    this.isProductOrPooja = false,
  });
  final String title;
  final String astroName;
  final String price;
  final int amountID;
  final String from;
  final String userInfoId;
  String? originalProductId;
  String? astroId;
  String? productID;
  String? orderID;
  String? remedy_id;
  String? suggested_astro_id;
  bool isProductOrPooja;

  @override
  State<PaymentInformationScreen> createState() =>
      _PaymentInformationScreenState();
}

class _PaymentInformationScreenState extends State<PaymentInformationScreen> {
  final TextEditingController _cpnController = TextEditingController();
  late PaymentServices paymentServices;

  // Razorpay razorpay = Razorpay();

  // /*************************** ERROR HANDLING / SUCCESS HANDLING *******************************/
  // initializeRazorPay({required BuildContext context}) {
  //   razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS,
  //       (response) => _handlePaymentSuccess(response, context));
  //   razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
  // }

  // void _handlePaymentSuccess(PaymentSuccessResponse response, context) {
  //   Fluttertoast.showToast(msg: "SUCCESS: " + response.paymentId!)
  //       .whenComplete(() {
  //     Provider.of<ListOfApisProvider>(context, listen: false)
  //         .getSuccessProvider(
  //             context: context,
  //             amountID: widget.amountID.toString(),
  //             userID: widget.userID,
  //             transactionID: "${response.paymentId.toString()}");
  //   });
  // }

  // void _handlePaymentError(PaymentFailureResponse response) {
  //   TLoggerHelper.error(
  //       "ERROR: " + response.code.toString() + " - " + response.message!);
  //   Fluttertoast.showToast(
  //       msg: "ERROR: " + response.code.toString() + " - " + response.message!);
  // }

  // /********************************* PROCESS OF PAYMENT ************************************/
  // Future checkMeOUT(
  //     {required BuildContext context,
  //     required int rechargeAmount,
  //     required String astroName}) async {
  //   initializeRazorPay(context: context);

  //   var options = {
  //     'key': AppKey.razorKey,
  //     'amount': rechargeAmount * 100,
  //     'name': astroName,
  //     'description': "Payment Done",
  //     'prefill': {'contact': '1234567890', 'email': 'test@razorpay.com'}
  //   };
  //   try {
  //     razorpay.open(options);
  //   } catch (e) {
  //     debugPrint("RazorPay Error---->${e.toString()}");
  //   }
  // }

  // disposeRazorPay() {
  //   razorpay.clear();
  // }

  int? selectPay;

  /********************************** Pay List *****************************/
  List<String> _payList = [
    "asset/icons/bhim.png",
    "asset/icons/gpay.png",
    "asset/icons/paytm.png",
    "asset/icons/phonepe.png",
  ];
  List<String> _payNameList = [
    "Bhim",
    "Gpay",
    "Paytm",
    "PhonePe",
  ];
  List<String> _otherPayNameList = [
    "Paytm",
    "Credit/Debit Card",
    "Net Banking",
    "CRED PAY",
  ];
  List<String> _otherPayList = [
    "asset/icons/paytm.png",
    "asset/icons/credit.png",
    "asset/icons/bank.png",
    "asset/icons/cred.png",
  ];
  num walletAmount = 0;
  @override
  void initState() {
    super.initState();

    context.read<ListOfApisProvider>().getTotalRechargeAmountProvider(
        context: context, amountID: widget.price);
    THelperFunctions.getWalletAmount().then((val) {
      setState(() {
        walletAmount = val;
      });
    });
    TLoggerHelper.debug("UserInfoId:--   " + widget.userInfoId);
    THelperFunctions.logScreenNameEvent(screenName: "recharge_payment");
  }

  double? totalPriceWithGst;
  int selectPaymentMethod = -1;
  bool isWallet = false;
  var totalAmount;
  num cashback = 0;
  num cashbackDiscount = 0;
  @override
  Widget build(BuildContext context) {
    TLoggerHelper.debug(" Order ID ${widget.orderID.toString()}");
    TLoggerHelper.debug(
        " Suggested ID ${widget.suggested_astro_id.toString()}");
    TLoggerHelper.debug("Remedy OrderID ${widget.remedy_id.toString()}");

    return Scaffold(
      backgroundColor: AppColor.appBackgroundLightGrey,
      bottomNavigationBar: SizedBox(
        height: 60,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              backgroundColor: AppColor.appColor,
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Text(
              'Proceed to Pay'.tr(),
              style: GoogleFonts.poppins(
                  color: AppColor.blackColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),
            ),
            onPressed: () {
              generateOrder();
            },
          ),
        ),
      ),
      appBar: AppBar(
        title: Text(TFormatter.capitalizeSentence(widget.astroName),
            style: GoogleFonts.poppins()),
      ),
      body: Consumer<ListOfApisProvider>(builder: (context, state, _) {
        totalAmount = state.totalRechargeAmountModel.recharge;
        totalPriceWithGst = totalAmount?.totalPriceWithGst;
        if (state.totalRechargeAmountModel.recharge != null) {
          cashback = state.totalRechargeAmountModel.recharge!.cashback;
          cashbackDiscount = int.parse(
              state.totalRechargeAmountModel.recharge!.discountPercentage);
        }
        // TLoggerHelper.debug(cashback.toString() + " This is the Cashback.....");
        if ((state.isLoading || totalAmount == null) &&
            selectPaymentMethod == -1) {
          return Center(
            child: CustomCircularProgressIndicator(),
          );
        }

        return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                color: AppColor.whiteColor,
                padding: EdgeInsets.all(12),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          TFormatter.capitalizeSentence(widget.title),
                          style: GoogleFonts.poppins(fontSize: 14),
                        ),
                        Text(
                          "₹ ${widget.price}",
                          style: GoogleFonts.poppins(fontSize: 14),
                        )
                      ],
                    ),
                    Divider(color: Colors.grey.shade300, thickness: 0.5),
                    if (isWallet)
                      Column(
                        children: [
                          // Divider(color: Colors.grey.shade300, thickness: 0.5),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Your total wallet balance".tr(),
                                  style: GoogleFonts.poppins(fontSize: 14)),
                              Text("₹ ${walletAmount}",
                                  style: GoogleFonts.poppins(fontSize: 14)),
                            ],
                          ),
                          SizedBox(height: 8),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Wallet balance deducted",
                                  style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600)),
                              Text(
                                  "₹ ${(walletAmount <= num.parse(widget.price.toString())) ? walletAmount : widget.price}",
                                  style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600)),
                            ],
                          ),
                          Divider(color: Colors.grey.shade300, thickness: 0.5),
                        ],
                      ),
                    SizedBox(height: 4),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Payable Amount".tr(),
                                style: GoogleFonts.poppins(
                                    fontSize: 14, fontWeight: FontWeight.w600)),
                            Text(
                                isWallet
                                    ? "₹ ${walletAmount <= num.parse(widget.price.toString()) ? num.parse(widget.price.toString()) - walletAmount : widget.price}"
                                    : "₹ ${num.parse(totalAmount!.price.toString())}",
                                style: GoogleFonts.poppins(
                                    fontSize: 14, fontWeight: FontWeight.w600)),
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "GST (18%)",
                              style: GoogleFonts.poppins(fontSize: 14),
                            ),
                            Text(
                              (walletAmount <=
                                          num.parse(widget.price.toString())) ||
                                      (!isWallet)
                                  ? "₹" +
                                      "${totalAmount!.gstAmount.toStringAsFixed(2)}"
                                  : "0",
                              style: GoogleFonts.poppins(fontSize: 14),
                            )
                          ],
                        ),
                        SizedBox(height: 4),
                      ],
                    ),
                    Divider(color: Colors.grey.shade300, thickness: 0.5),
                    // SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Total Payable Amount".tr(),
                            style: GoogleFonts.poppins(
                                fontSize: 14, fontWeight: FontWeight.w600)),
                        Text(
                            isWallet
                                ? "₹ ${(walletAmount <= num.parse(widget.price.toString())) ? (num.parse(widget.price.toString()) - walletAmount) + num.parse((totalAmount.gstAmount.toStringAsFixed(2))) : widget.price}"
                                : "₹ ${num.parse(totalAmount.totalPriceWithGst.toString())}",
                            style: GoogleFonts.poppins(
                                fontSize: 14, fontWeight: FontWeight.w600)),
                      ],
                    ),
                    SizedBox(height: 20),
                    if (widget.isProductOrPooja)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Wallet".tr() + " ($walletAmount)",
                              style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.grey)),
                          SizedBox(
                            height: 15,
                            width: 15,
                            child: Checkbox(
                              value: isWallet,
                              onChanged: (val) {
                                if (walletAmount != 0) {
                                  setState(() {
                                    isWallet = val ?? false;
                                  });

                                  if (isWallet) {
                                    // Wallet selected: Deduct walletAmount from the price
                                    var newAmount = (walletAmount <=
                                                num.parse(
                                                    widget.price.toString())
                                            ? num.parse(
                                                    widget.price.toString()) -
                                                walletAmount
                                            : widget.price)
                                        .toString();

                                    context
                                        .read<ListOfApisProvider>()
                                        .getTotalRechargeAmountProvider(
                                            context: context,
                                            amountID: newAmount);

                                    // Update local state to reflect the new total with GST after wallet deduction
                                    // setState(() {
                                    //   totalPriceWithGst =
                                    //       totalAmount.totalPriceWithGst -
                                    //           walletAmount.toDouble();
                                    // });

                                    TLoggerHelper.debug(
                                        "Amount needs to be deducted:-- " +
                                            newAmount);
                                  } else {
                                    // Wallet not selected: Use the original widget.price
                                    context
                                        .read<ListOfApisProvider>()
                                        .getTotalRechargeAmountProvider(
                                            context: context,
                                            amountID: widget.price);

                                    // Reset to the original totalPriceWithGst without wallet deduction
                                    // setState(() {
                                    //   totalPriceWithGst =
                                    //       totalAmount.totalPriceWithGst;
                                    // });

                                    TLoggerHelper.debug(
                                        "Amount without wallet deduction:-- " +
                                            widget.price);
                                  }
                                } else if (walletAmount == 0) {
                                  Fluttertoast.showToast(
                                      msg: "You won't have enough wallet amount"
                                          .tr());
                                }
                              },
                            ),
                          )
                        ],
                      ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              if (cashback != 0)
                Container(
                    padding: EdgeInsets.all(8),
                    color: AppColor.whiteColor,
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 2),
                        // if (cashback == 0)
                        //   TextField(
                        //     controller: _cpnController,
                        //     decoration: InputDecoration(
                        //       hintText: "Enter Coupon Code",
                        //       hintStyle: GoogleFonts.poppins(color: Colors.grey),
                        //       border: OutlineInputBorder(
                        //         borderRadius: BorderRadius.circular(10),
                        //         borderSide: BorderSide(
                        //           color: Colors.black,
                        //           width: 1,
                        //           style: BorderStyle.solid,
                        //         ),
                        //       ),
                        //       suffixIcon: Container(
                        //         margin: EdgeInsets.all(8),
                        //         child: ElevatedButton(
                        //           style: ElevatedButton.styleFrom(
                        //             minimumSize: Size(100, 30),
                        //             backgroundColor: AppColor.appColor,
                        //             shape: new RoundedRectangleBorder(
                        //               borderRadius:
                        //                   new BorderRadius.circular(10.0),
                        //             ),
                        //           ),
                        //           child: Text(
                        //             "Apply",
                        //             style:
                        //                 GoogleFonts.poppins(color: Colors.black),
                        //           ),
                        //           onPressed: () {},
                        //         ),
                        //       ),
                        //     ),
                        //   ),

                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.green.shade200),
                              borderRadius: BorderRadius.circular(8)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.all(12),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(8),
                                        topRight: Radius.circular(8)),
                                    color: Colors.green.shade200),
                                child: Row(children: [
                                  Text(
                                    "${cashbackDiscount}% " +
                                        "extra on recharge of".tr() +
                                        " ${Constants.currency} ${widget.price}",
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  // Icon(Icons.cancel, color: Colors.blueGrey)
                                ]),
                              ),
                              SizedBox(height: 8),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  width:
                                      MediaQuery.of(context).size.width * 0.8,
                                  child: Row(
                                    children: [
                                      Icon(Icons.check_circle,
                                          color: Colors.green),
                                      const SizedBox(width: 12),
                                      Expanded(
                                        child: Text(
                                          "${Constants.currency} ${cashback} " +
                                              "Cashback in Jyotish Rahsaya wallet with this recharge"
                                                  .tr(),
                                          maxLines: 2,
                                          style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              color: Colors.blueGrey),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),

                        SizedBox(height: 2),
                      ],
                    )),
              if (cashback != 0) SizedBox(height: 10),
              Container(
                  padding: EdgeInsets.all(8),
                  color: AppColor.whiteColor,
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Pay directly with your favourite UPI apps".tr(),
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                      SizedBox(height: 8),
                      Row(
                          children: List.generate(
                              _payNameList.length,
                              (index) => Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            generateOrder();
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              height: 45,
                                              width: 45,
                                              decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: AppColor
                                                            .appBackgroundLightGrey)
                                                  ],
                                                  image: DecorationImage(
                                                      fit: BoxFit.contain,
                                                      image: AssetImage(
                                                          "${_payList[index]}")),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "${_payNameList[index]}",
                                          style:
                                              GoogleFonts.poppins(fontSize: 10),
                                        )
                                      ],
                                    ),
                                  ))),
                      SizedBox(
                        height: 8,
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  generateOrder();
                                },
                                child: Container(
                                    height: 45,
                                    width: 45,
                                    decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                              color: AppColor
                                                  .appBackgroundLightGrey)
                                        ],
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "asset/icons/bhim1.png")),
                                        borderRadius:
                                            BorderRadius.circular(10))),
                              ),
                              SizedBox(width: 5),
                              Text("Pay with other UPI apps".tr(),
                                  style: GoogleFonts.poppins(
                                      color: Colors.black, fontSize: 14)),
                            ],
                          ),
                          Icon(
                            Icons.arrow_forward_ios_rounded,
                            color: Colors.grey.shade600,
                            size: 14,
                          ),
                        ],
                      )
                    ],
                  )),
              SizedBox(height: 12),
              Container(
                  padding: EdgeInsets.all(8),
                  color: AppColor.whiteColor,
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Other Payments Methods".tr(),
                        style: GoogleFonts.poppins(fontSize: 12),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        height: 250,
                        width: double.infinity,
                        child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: _otherPayNameList.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 45,
                                              width: 45,
                                              decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: AppColor
                                                            .appBackgroundLightGrey)
                                                  ],
                                                  image: DecorationImage(
                                                      fit: BoxFit.contain,
                                                      image: AssetImage(
                                                          "${_otherPayList[index]}")),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 15.0),
                                            child: Text(
                                              "${_otherPayNameList[index]}",
                                              style: GoogleFonts.poppins(
                                                  fontSize: 15,
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          )
                                        ],
                                      ),
                                      Checkbox(
                                          value: selectPay == index,
                                          onChanged: (value) {
                                            setState(() {
                                              selectPaymentMethod = 1;
                                              selectPay = index;
                                            });
                                          }),
                                    ],
                                  )
                                ],
                              );
                            }),
                      ),
                      // DropdownButton(items: [], onChanged: (value) {}),
                    ],
                  )),
            ],
          ),
        );
      }),
    );
  }

  DioClient? client;
  bool isProcessing = false; // Declare this at the class level

  Future<void> generateOrder() async {
    if (isProcessing) {
      Fluttertoast.showToast(msg: "Razorpay is preparing...");
      return;
    }

    isProcessing = true; // Prevent multiple clicks
    Fluttertoast.showToast(msg: "Generating order...");

    client = DioClient();
    Dio dio = await client!.getClient();

    if (await THelperFunctions.getUserId() != "") {
      Map<String, String> bodyWithAstroID = {
        'user_id': await THelperFunctions.getUserId(),
        'astrologer_id': widget.astroId.toString(),
        "amount": totalAmount.totalPriceWithGst.toString(),
        "remedy_price": widget.price,
        "is_wallet": isWallet == true ? "1" : "0",
        "user_info_id": widget.userInfoId
      };
      Map<String, dynamic> bodyWithSuggestedID = {
        'user_id': await THelperFunctions.getUserId(),
        "amount": totalAmount.totalPriceWithGst.toString(),
        "suggested_astro_id": widget.suggested_astro_id,
        "astrologer_id": widget.astroId.toString(),
        "is_wallet": isWallet == true ? "1" : "0",
        "remedy_price": widget.price,
        "remedy_id": widget.remedy_id ?? "",
        "user_info_id": widget.userInfoId
      };
      var params;
      if (widget.originalProductId != null) {
        params = bodyWithSuggestedID;
      } else {
        params = bodyWithAstroID;
      }

      // var params = widget.suggested_astro_id != null &&
      //         widget.suggested_astro_id!.isNotEmpty
      //     ? bodyWithSuggestedID
      //     : bodyWithAstroID;

      String api =
          widget.from == "wallet" ?UrlConstants.baseUrl+ UrlConstants.payment : UrlConstants.baseUrl+ UrlConstants.createOrderProduct;

      print("api---> $api");
      if (widget.from == "wallet") {
        WalletPaymentOrderCreate? walletPaymentOrderCreate =
            await client?.createWalletOrder(dio, api, params);
        if (walletPaymentOrderCreate!.status == true) {
          try {
            setState(() {
              paymentServices = PaymentServices(
                  context: context,

                  // amountID:
                  //     walletPaymentOrderCreate.payment!.totalValue.toString(),
                  orderId: walletPaymentOrderCreate.payment!.orderId.toString(),
                  razorpayId: walletPaymentOrderCreate.payment!.razorpayId,
                  razorpaySecretId:
                      walletPaymentOrderCreate.payment!.razorpaySecretKey);

              paymentServices.checkMeOUT(
                  context: context,
                  astroName: widget.astroName,
                  rechargeAmount: totalPriceWithGst.toString(),
                  orderId: walletPaymentOrderCreate.payment!.orderId.toString(),
                  razorypayDesc: widget.from);
              print("Ra---${totalPriceWithGst.runtimeType}");
            });
          } catch (e) {
            TLoggerHelper.error(e.toString());
          }
        }
      } else if (widget.from == "mall") {
        // try {
        CreateOrderModel? createOrderModel =
            await client?.createOrder(dio, api, params);

        if (createOrderModel!.status == true) {
          TLoggerHelper.warning("Very First Block ${widget.productID}");
          bool isWalletAmountLesser =
              walletAmount < (double.parse(widget.price));
          if (isWalletAmountLesser || !isWallet) {
            TLoggerHelper.warning(
                "${widget.orderID ?? createOrderModel.payment.orderId.toString()} ${widget.originalProductId}");
            setState(() {
              paymentServices = PaymentServices(
                  context: context,
                  orderId: createOrderModel.payment.orderId.toString(),
                  remedy_id: createOrderModel.payment.astroRemedyId,
                  razorpayId: createOrderModel.payment.razorpayId,
                  razorpaySecretId: createOrderModel.payment.razorpaySecretKey,
                  suggestedAstrologerId: null);

              paymentServices.checkMeOUT(
                context: context,
                astroName: widget.astroName,
                rechargeAmount: isWallet
                    ? (((double.parse(widget.price) - walletAmount) +
                            num.parse(
                                (totalAmount.gstAmount.toStringAsFixed(2))))
                        .toString())
                    : ((double.parse(widget.price) +
                            (double.parse(widget.price) * 18) / 100)
                        .toString()),
                orderId: widget.orderID ??
                    createOrderModel.payment.orderId.toString(),
                razorypayDesc: widget.from,
              );
              print("Order Create Payment---${totalPriceWithGst.runtimeType}");
            });
          } else {
            TLoggerHelper.warning("Second Block");
            Provider.of<ListOfApisProvider>(context, listen: false)
                .getSuccessProvider(
                    context: context,
                    orderId: createOrderModel.payment.orderId.toString(),
                    razorpayId: createOrderModel.payment.razorpayId,
                    remedy_id: createOrderModel.payment.astroRemedyId,
                    razorpaySecretId:
                        createOrderModel.payment.razorpaySecretKey,
                    astroProductId: widget.originalProductId,
                    transactionID:
                        THelperFunctions.generateWalletTransactionId())
                .whenComplete(
              () {
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                Fluttertoast.showToast(
                    msg: TFormatter.capitalizeSentence(widget.title) +
                        "Booked Successfully".tr() +
                        " !!");
              },
            );
          }
        }
        // } catch (e) {
        //   TLoggerHelper.error(e.toString());
        // }
      } else {
        TLoggerHelper.warning("Pooja Block");
        // try {
        var orderId = "";
        String astroPoojaId = "";
        CreateOrderPoojaModel? createOrderPoojaModel;
        TLoggerHelper.debug(
            "Check the Pooja OrderID Here: $orderId && Suggested Astro Id is ${widget.suggested_astro_id} Price ${widget.price}");
        if (widget.isProductOrPooja) {
          await Provider.of<ListOfApisProvider>(context, listen: false)
              .createOrderPoojaListProvider(
            context: context,
            userInfoId: widget.userInfoId,
            realPricePoojaWithoutGST: widget.price,
            suggestedAstrologerId: widget.suggested_astro_id ?? "",
            astrologerID: widget.astroId!,
            productID: widget.productID!,
            isWallet: isWallet == true ? "1" : "0",
            productPrice: [widget.price],
            amount: totalPriceWithGst.toString(),
          )
              .then((val) {
            if (val != null) {
              createOrderPoojaModel = val;
              orderId = val.payment!.orderId!;
              astroPoojaId = val.payment!.orderPoojaId!;
            }
          });
        }
        bool isWalletAmountLesser = walletAmount <
            (double.parse(widget.price) +
                (double.parse(widget.price) * 18) / 100);
        TLoggerHelper.info(
            "Is Wallet Amount Lesser ${isWalletAmountLesser} , ${(double.parse(widget.price) + (double.parse(widget.price) * 18) / 100)} and ${walletAmount}");
        // if () {
        // TLoggerHelper.debug(
        //     "${widget.remedy_order_id} ${widget.suggested_astro_id} ${widget.originalProductId}");
        //TODO:Change RazoryPay Key Below down
        setState(() {
          TLoggerHelper.info("Pooja If");
          if (((walletAmount - (double.parse(widget.price))) <= 0) ||
              !isWallet) {
            TLoggerHelper.info("Pooja If");
            paymentServices = PaymentServices(
                context: context,
                // amountID: isWallet
                //     ? (((double.parse(widget.price) - walletAmount) +
                //             num.parse(
                //                 (totalAmount.gstAmount.toStringAsFixed(2))))
                //         .toString())
                //     : ((double.parse(widget.price) +
                //             (double.parse(widget.price) * 18) / 100)
                //         .toString()),
                orderId: orderId,
                razorpayId: createOrderPoojaModel!.payment!.razorpayId,
                razorpaySecretId:
                    createOrderPoojaModel!.payment!.razorpaySecretKey,
                suggestedAstrologerId: widget.suggested_astro_id,
                astroPoojaId: astroPoojaId);
            paymentServices.checkMeOUT(
                context: context,
                rechargeAmount: isWallet
                    ? (((double.parse(widget.price) - walletAmount) +
                            num.parse(
                                (totalAmount.gstAmount.toStringAsFixed(2))))
                        .toString())
                    : ((double.parse(widget.price) +
                            (double.parse(widget.price) * 18) / 100)
                        .toString()),
                astroName: widget.astroName,
                orderId: orderId,
                razorypayDesc: "");
            // TLoggerHelper.debug(
            //     "isWalletAmountGreater Status:- $isWalletAmountLesser with orderId of ${isWalletAmountLesser ? "hemant" : orderId} SuggestedAstroID: ${widget.suggested_astro_id} Remedy OrderId:-- ${widget.remedy_order_id}");
          } else {
            TLoggerHelper.info("Pooja Else");
            Provider.of<ListOfApisProvider>(context, listen: false)
                .getSuccessProvider(
                    context: context,

                    // amountID: isWallet
                    //     ? (((double.parse(widget.price) - walletAmount) +
                    //             num.parse(
                    //                 (totalAmount.gstAmount.toStringAsFixed(2))))
                    //         .toString())
                    //     : ((double.parse(widget.price) +
                    //             (double.parse(widget.price) * 18) / 100)
                    //         .toString()),
                    orderId: orderId,
                    razorpayId: createOrderPoojaModel!.payment!.razorpayId,
                    razorpaySecretId:
                        createOrderPoojaModel!.payment!.razorpaySecretKey,
                    astroProductId: astroPoojaId,
                    suggestedAstrologerId: widget.suggested_astro_id,
                    transactionID:
                        THelperFunctions.generateWalletTransactionId())
                .whenComplete(
              () {
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                Fluttertoast.showToast(
                    msg: TFormatter.capitalizeSentence(widget.title) +
                        " Booked Successfully !!");
              },
            );
          }
        });
        // TLoggerHelper.debug("Check Pooja OrderID Get :$orderId");
        // }
        // } catch (e) {
        //   TLoggerHelper.error("Pooja Order Payment Error -->${e.toString()}");
        // }
      }
    } else {
      Fluttertoast.showToast(msg: "Login to Recharge".tr());
      context.pushNamed(RouteConstants.authScreen);
    }
  }
}
