import 'dart:convert';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places_hoc081098/flutter_google_places_hoc081098.dart';
import 'package:flutter_google_places_hoc081098/google_maps_webservice_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/formatter.dart';
import '../Core/logger_helper.dart';
import '../Core/Model/ChatIntakeFromResponse.dart';
import '../Core/Model/UserList.dart';
import '../Core/Provider/WaitListNotifier.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../dialog/showLoaderDialog.dart';
import '../utils/AppColor.dart';
import 'ProfileScreen.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ChatIntakeForm extends StatefulWidget {
  var name;
  var id;
  var astroId;
  var type;
  int? free_chat;
  bool isLive;
  bool isRandomChat;

  ChatIntakeForm(this.name, this.id, this.astroId, this.type, this.free_chat,
      {this.isLive = false, this.isRandomChat = false});

  @override
  State<StatefulWidget> createState() {
    return _ChatIntakeFormState();
  }
}

class _ChatIntakeFormState extends State<ChatIntakeForm> {
  List<int> generatedColor = [];
  int selectedPos = 0;
  var selected = false;
  String _lat = "0.0";
  String _lon = "0.0";
  final formKey = GlobalKey<FormState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController occupationController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController tobController = TextEditingController();
  TextEditingController placeOfBirthController = TextEditingController();

  List<String> relationshipItems = [
    'Single',
    'Married',
    'Divorced',
    'Separated',
    'Widowed'
  ];
  List<String> concernTopicsItems = [
    'Career and Business',
    'Marriage',
    'Love and Relationship',
    'Wealth and Property',
    'Education',
    'Legal Matters',
    'Child Name Consultation',
    'Business Name Consultation',
    'Gem Stone Consultation',
    'Commodity Trading Consultation',
    'Match Making',
    'Birth Time Rectification',
    'Name Correction Consultation',
    'Travel Abroad Consultation',
    'Health Consultation',
    'Others'
  ];
  String chooseRelationshipValue = "";
  String concernsTopicValue = "";
  int selectedGenderTile = 1;
  String dob = "";
  String selectedDob = "";
  String timeOfBirth = "";
  String id = "";

  bool isChecked = false;
  DioClient? client;
  List<UserListInfo> userArrayList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 3,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconButton(
            onPressed: () {
              Navigator.pop(context, "false");
            },
            icon: Icon(Icons.arrow_back),
          ),
        ),
        title: Text(
          widget.type == "call"
              ? "Call Intake Form".tr()
              : "Chat Intake Form".tr(),
          style: GoogleFonts.poppins(fontSize: 16),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 15),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Recent Kundali".tr()),
                    SizedBox(height: 12),
                    SizedBox(
                      height: 70,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: userArrayList.length,
                        itemBuilder: (BuildContext context, int i) {
                          // Ensure generatedColor list has enough entries
                          if (i >= generatedColor.length) {
                            generatedColor
                                .add(Random().nextInt(Colors.primaries.length));
                          }

                          return Padding(
                            padding: EdgeInsets.only(right: 20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                  radius: 20,
                                  backgroundColor: i == 0
                                      ? Colors
                                          .grey // Set a default color for "New" item
                                      : Colors.primaries[generatedColor[i]],
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        selected = true;
                                        selectedPos = i;
                                        setData(i);
                                      });
                                      TLoggerHelper.info(
                                          "Selected $selectedPos");
                                    },
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: i == 0
                                          ? Icon(
                                              Icons.add,
                                              color: Colors.white,
                                            )
                                          : Text(
                                              TFormatter.capitalize(
                                                  userArrayList[i]
                                                      .firstName
                                                      .toString()[0]),
                                              style: GoogleFonts.poppins(
                                                color: Colors.white,
                                                fontSize: selectedPos == i
                                                    ? 18.0
                                                    : 10.0,
                                                fontWeight: selectedPos == i
                                                    ? FontWeight.w500
                                                    : FontWeight.bold,
                                              ),
                                            ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  i == 0
                                      ? "New".tr()
                                      : TFormatter.capitalize(userArrayList[i]
                                          .firstName
                                          .toString()),
                                  style: GoogleFonts.poppins(
                                    fontSize: selectedPos == i ? 16.0 : 12.0,
                                    color: Colors.black87,
                                    fontWeight: selectedPos == i
                                        ? FontWeight.normal
                                        : FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(height: 20),
                    Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "First Name".tr() + " *",
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            // The validator receives the text that the user has entered.
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: firstNameController,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(0),
                              isDense: true,
                              hintText: "Enter First Name".tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: AppColor.appColor, fontSize: 16),
                              hintStyle: GoogleFonts.poppins(fontSize: 14),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: AppColor.appColor),
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Name required".tr();
                              }
                              if (RegExp(r'\d').hasMatch(value)) {
                                return "Name cannot contain numbers".tr();
                              }
                              if (value.length < 2) {
                                return "Name should be at least 2 characters long"
                                    .tr();
                              }

                              return null;
                            },
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Last Name".tr(),
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            // The validator receives the text that the user has entered.
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: lastNameController,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(0),
                              isDense: true,
                              hintText: "Enter Last Name".tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: AppColor.appColor, fontSize: 16),
                              hintStyle: GoogleFonts.poppins(fontSize: 14),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: AppColor.appColor),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Text(
                                "Gender".tr() + " *",
                                style: GoogleFonts.poppins(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: ListTileTheme(
                                  horizontalTitleGap: 1,
                                  child: RadioListTile(
                                    contentPadding: EdgeInsets.all(0),
                                    value: 1,
                                    groupValue: selectedGenderTile,
                                    title: Text("Male".tr()),
                                    onChanged: (val) {
                                      print("Radio Tile pressed $val");
                                      setSelectedRadioTile(val!);
                                    },
                                    activeColor: AppColor.appColor,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: ListTileTheme(
                                  horizontalTitleGap: 1,
                                  child: RadioListTile(
                                    contentPadding: EdgeInsets.all(0),
                                    value: 2,
                                    groupValue: selectedGenderTile,
                                    title: Text("Female".tr()),
                                    onChanged: (val) {
                                      print("Radio Tile pressed $val");
                                      setSelectedRadioTile(val!);
                                    },
                                    activeColor: AppColor.appColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Date of Birth".tr() + " *",
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            onTap: () async {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              _selectDate(context, "1");
                            },
                            // The validator receives the text that the user has entered.
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: dobController..text = dob,
                            maxLines: 1,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(0),
                              isDense: true,
                              hintText: "Enter DOB".tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: AppColor.appColor, fontSize: 16),
                              hintStyle: GoogleFonts.poppins(fontSize: 14),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: AppColor.appColor),
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Date of birth required".tr();
                              }

                              return null;
                            },
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Time of Birth".tr() + " *",
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            onTap: () async {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              TimeOfDay? time = await getTime(
                                context: context,
                                title: "Select Time of Birth".tr(),
                              );

                              setState(() {
                                String min = "";
                                if (time!.minute.toString().length < 2) {
                                  min = "0${time?.minute}";
                                } else {
                                  min = "${time?.minute}";
                                }
                                timeOfBirth =
                                    "${time.hourOfPeriod}:$min ${time.period == DayPeriod.am ? 'AM' : 'PM'}";
                              });
                            },
                            // The validator receives the text that the user has entered.
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: tobController..text = timeOfBirth,
                            maxLines: 1,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(0),
                              isDense: true,
                              hintText: "Enter time of birth".tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: AppColor.appColor, fontSize: 16),
                              hintStyle: GoogleFonts.poppins(fontSize: 14),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: AppColor.appColor),
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value == "") {
                                return "Time of birth required".tr();
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                child: Checkbox(
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  value: isChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      isChecked = value ?? false;
                                      if (isChecked == true) {
                                        timeOfBirth = "12:00 AM";
                                      } else {
                                        timeOfBirth = "";
                                      }
                                    });
                                  },
                                  side: const BorderSide(color: Colors.black),
                                  fillColor:
                                      MaterialStateProperty.resolveWith<Color?>(
                                    (Set<MaterialState> states) {
                                      if (states
                                          .contains(MaterialState.selected)) {
                                        return AppColor.appColor;
                                      }
                                      return const Color(0xffE6E6E6);
                                    },
                                  ),
                                ),
                                height: 24,
                                width: 24,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                "Don't know my exact time of birth".tr(),
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          Text(
                            "Note: Without time of birth, we can still achieve upto 80% accurate predictions."
                                .tr(),
                            style: GoogleFonts.poppins(
                                fontSize: 12,
                                color: Colors.black54,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Place of Birth".tr() + " *",
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            onTap: () async {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              Prediction? p = await PlacesAutocomplete.show(
                                offset: 0,
                                radius: 1000,
                                types: [],
                                strictbounds: false,
                                region: "in",
                                onError: onError,
                                context: context,
                                apiKey:
                                    "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                                mode: Mode
                                    .overlay, // Mode.fullscreen language: "en", onError: (value) { Utils.displayToast(context, value.errorMessage!); }, components: [Component(Component.country, "in")] );
                              );
                              displayPrediction(p!);
                              //cityController..text = p.description!;
                              final _places = GoogleMapsPlaces(
                                apiKey:
                                    "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                                apiHeaders:
                                    await const GoogleApiHeaders().getHeaders(),
                              );

                              final detail =
                                  await _places.getDetailsByPlaceId(p.placeId!);
                              final geometry = detail.result.geometry!;
                              final lat = geometry.location.lat;
                              final lng = geometry.location.lng;
                              _getAddressFromLatLng(lat, lng,
                                  placeOfBirthController, p.description!);
                            },
                            // The validator receives the text that the user has entered.
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: placeOfBirthController,
                            maxLines: 1,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(0),
                              isDense: true,
                              hintText: "Enter Place of birth".tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: AppColor.appColor, fontSize: 16),
                              hintStyle: GoogleFonts.poppins(fontSize: 14),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: AppColor.appColor),
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value == "") {
                                return "Place of birth required".tr();
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Relationship Status".tr(),
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          DropdownButtonFormField(
                              items: relationshipItems
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14),
                                  ),
                                );
                              }).toList(),
                              value: chooseRelationshipValue.isNotEmpty
                                  ? chooseRelationshipValue
                                  : null,
                              decoration: InputDecoration(
                                hintText: "Select Relationship Status".tr(),
                                hintStyle: GoogleFonts.poppins(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 4),
                                isDense: true,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColor.appColor),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black38),
                                ),
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  chooseRelationshipValue = newValue!;
                                });
                              }),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Occupation".tr(),
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            // The validator receives the text that the user has entered.
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            controller: occupationController,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 4),
                              isDense: true,
                              hintText: "Enter Occupation".tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: AppColor.appColor, fontSize: 16),
                              hintStyle: GoogleFonts.poppins(fontSize: 14),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: AppColor.appColor),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Topic of Concern".tr(),
                            style: GoogleFonts.poppins(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          DropdownButtonFormField(
                              items: concernTopicsItems
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14),
                                  ),
                                );
                              }).toList(),
                              value: concernsTopicValue.isNotEmpty
                                  ? concernsTopicValue
                                  : null,
                              decoration: InputDecoration(
                                hintText: "Select Topic of Concern".tr(),
                                hintStyle: GoogleFonts.poppins(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 4),
                                isDense: true,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColor.appColor),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black38),
                                ),
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  concernsTopicValue = newValue!;
                                });
                              }),
                          SizedBox(
                            height: 25,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              if (formKey.currentState!.validate()) {
                submitData();
                TLoggerHelper.debug(
                    "Checking Free: ${widget.free_chat.toString()}");
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
              width: double.infinity,
              color: AppColor.appColor,
              child: Center(
                child: Text(
                  "Start".tr() +
                      " ${TFormatter.capitalize(widget.type == "call" ? "call" : "chat").tr()} " +
                      "with".tr() +
                      " ${TFormatter.capitalize(widget.name)}",
                  style: GoogleFonts.poppins(
                      fontSize: 16, fontWeight: FontWeight.w500),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getUsers();
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedGenderTile = val;
    });
  }

  _selectDate(BuildContext context, String type) async {
    print("DatePickerDialog");
    DateTime selectedDate = DateTime.now();
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        // Refer step 1
        firstDate: DateTime(1950),
        lastDate: DateTime.now(),
        helpText: 'Select date range',
        // Can be used as title
        cancelText: 'Not Now',
        confirmText: 'Confirm');
    if (picked != null && picked != selectedDate) {
      setState(() {
        String formattedDate = DateFormat('dd-MMMM-yyyy').format(picked);
        selectedDob = DateFormat('yyyy/MM/dd').format(picked);
        dob = formattedDate;
      });
    }
  }

  Future<TimeOfDay?> getTime({
    required BuildContext context,
    String? title,
    TimeOfDay? initialTime,
    String? cancelText,
    String? confirmText,
  }) async {
    TimeOfDay? time = await showTimePicker(
      initialEntryMode: TimePickerEntryMode.dial,
      context: context,
      initialTime: initialTime ?? TimeOfDay.now(),
      cancelText: cancelText ?? "Cancel",
      confirmText: confirmText ?? "Save",
      helpText: title ?? "Select time",
      builder: (context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child!,
        );
      },
    );

    return time;
  }

  Future<void> _getAddressFromLatLng(double lat, double lng,
      TextEditingController cityController, String description) async {
    await placemarkFromCoordinates(lat, lng).then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      print(place);
      setState(() {
        _lat = lat.toString();
        _lon = lng.toString();
        cityController
          ..text = place.locality! +
              ", " +
              place.administrativeArea! +
              ", " +
              place.country!;
        //zipController..text = place.postalCode!;
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  Future<void> submitData() async {
    final prefs = await SharedPreferences.getInstance();
    var userId = await prefs.getInt(Constants.userID).toString();
    Future.delayed(Duration.zero, () {
      showLoaderDialog(context);
    });

    client = DioClient();
    Dio dio = await client!.getClient();

    var params = id == ""
        ? jsonEncode({
            'user_id': userId,
            'first_name': firstNameController.text,
            'last_name': lastNameController.text,
            'gender': selectedGenderTile == 1 ? "Male" : "Female",
            'dob': selectedDob,
            'dob_time': timeOfBirth,
            'dob_place': placeOfBirthController.text,
            'relationship_status': chooseRelationshipValue,
            'occupation': occupationController.text,
            'topic_concern': concernsTopicValue,
            'astrologer_id': widget.astroId,
            'type': widget.type.toString().toLowerCase(),
            'free_chat': widget.type.toString().toLowerCase() == "chat"
                ? widget.free_chat
                : "0",
            'not_exact_dob_time': isChecked == true ? 1 : 0,
            'lat': _lat,
            'lon': _lon
          })
        : jsonEncode({
            'user_id': userId,
            'id': id,
            'first_name': firstNameController.text,
            'last_name': lastNameController.text,
            'gender': selectedGenderTile == 1 ? "Male" : "Female",
            'dob': selectedDob,
            'dob_time': timeOfBirth,
            'dob_place': placeOfBirthController.text,
            'relationship_status': chooseRelationshipValue,
            'occupation': occupationController.text,
            'topic_concern': concernsTopicValue,
            'astrologer_id': widget.astroId,
            'type': widget.type.toString().toLowerCase(),
            'free_chat': widget.type.toString().toLowerCase() == "chat"
                ? widget.free_chat
                : "0",
            'not_exact_dob_time': isChecked == true ? 1 : 0,
            'lat': _lat,
            'lon': _lon
          });
    String api = widget.isRandomChat
        ? UrlConstants.submitChatIntakeFormRandom
        : widget.isLive
            ? UrlConstants.submitChatIntakeFormLive
            : UrlConstants.submitChatIntakeForm;
    TLoggerHelper.debug("Info ID : ----   ${params}");
    final String uri = UrlConstants.baseUrl + api;
    ChatIntakeFromResponse? chatIntakeFromResponse =
        await client?.chatIntakeForm(dio, uri, params);
    if (widget.isRandomChat) {
      if (Navigator.of(context).canPop()) {
        Navigator.pop(context);
      }
      if (Navigator.of(context).canPop()) {
        Navigator.pop(context);
      }
    } else {
      if (Navigator.of(context).canPop()) {
        Navigator.pop(context);
      }
    }

    if (chatIntakeFromResponse!.status == true) {
      // Provider.of<WaitListNotifier>(context, listen: false)
      //     .checkWaitList(context, int.parse(userId));
      if (widget.isRandomChat) {
        if (Navigator.of(context).canPop()) {
          Navigator.pop(context, "true");
        }
        if (Navigator.of(context).canPop()) {
          Navigator.pop(context, "true");
        }
      } else {
        if (Navigator.of(context).canPop()) {
          Navigator.pop(context, "true");
        }
      }
    } else {
      Fluttertoast.showToast(msg: chatIntakeFromResponse.message!);
      //SnackBar(content: Text("Please try again"));
    }
  }

  Future<void> getUsers() async {
    final prefs = await SharedPreferences.getInstance();
    var userId = prefs.getInt(Constants.userID).toString();
    Future.delayed(Duration.zero, () {
      showLoaderDialog(context);
    });

    client = DioClient();
    Dio dio = await client!.getClient();

    var params = jsonEncode({'user_id': userId});

    String api = UrlConstants.submitChatIntakeFormUsers;
    final String uri = UrlConstants.baseUrl + api;
    UserList? userList = await client?.chatIntakeFormUsers(dio, uri, params);
    Navigator.pop(context);

    userArrayList.add(UserListInfo(
        id: 0,
        userId: "",
        firstName: "",
        lastName: "",
        image: "",
        gender: "",
        dob: "",
        dobPlace: "",
        dobTime: "",
        notExactDobTime: 0,
        occupation: "",
        topicConcern: "",
        otherText: "",
        relationshipStatus: "",
        createdAt: "",
        updatedAt: "",
        lat: "0.0",
        lon: "0.0"));
    if (userList!.userListInfo!.length > 0) {
      userArrayList.addAll(userList.userListInfo!);
      setData(1);
      selectedPos = 1;
    }

    setState(() {});
  }

  void setData(int i) {
    if (i > 0) {
      firstNameController
        ..text = TFormatter.capitalize(userArrayList[i].firstName.toString());
      lastNameController
        ..text = TFormatter.capitalize(
            userArrayList[i].lastName.toString().replaceAll("null", ""));
      dob = userArrayList[i].dob.toString();
      selectedDob = userArrayList[i].dob.toString();
      timeOfBirth = userArrayList[i].dobTime.toString();
      placeOfBirthController..text = userArrayList[i].dobPlace.toString();
      chooseRelationshipValue = userArrayList[i].relationshipStatus.toString();
      concernsTopicValue = userArrayList[i].topicConcern.toString();
      occupationController
        ..text = TFormatter.capitalize(
            userArrayList[i].occupation.toString().replaceAll("null", ""));
      id = userArrayList[i].id!.toString();
      _lat = userArrayList[i].lat.toString();
      _lon = userArrayList[i].lon.toString();
      if (userArrayList[i].gender.toString() == "Male")
        selectedGenderTile = 1;
      else {
        selectedGenderTile = 2;
      }

      if (userArrayList[i].notExactDobTime.toString() == "0")
        isChecked = false;
      else {
        isChecked = true;
      }
    } else {
      id = "";
      firstNameController..text = "";
      lastNameController..text = "";
      dob = "";
      selectedDob = "";
      timeOfBirth = "";
      placeOfBirthController..text = "";
      chooseRelationshipValue = "";
      concernsTopicValue = "";
      occupationController..text = "";
      selectedGenderTile = 1;
      isChecked = false;
    }
  }
}
