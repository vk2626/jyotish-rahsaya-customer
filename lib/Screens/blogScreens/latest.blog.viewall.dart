import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../utils/AppColor.dart';
import '../../Core/Model/blog.model.dart';
import '../../CustomNavigator/SlideRightRoute.dart';
import 'blog.details.screen.dart';
import 'package:provider/provider.dart';

import '../../Core/Provider/list.of.api.provider.dart';

class LatestBlogViewAllScreen extends StatefulWidget {
  const LatestBlogViewAllScreen({
    super.key,
  });

  @override
  State<LatestBlogViewAllScreen> createState() =>
      _LatestBlogViewAllScreenState();
}

class _LatestBlogViewAllScreenState extends State<LatestBlogViewAllScreen> {
  BlogModel? blogData;
  @override
  void initState() {
    super.initState();
    Provider.of<ListOfApisProvider>(context, listen: false).getBlogNew();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Astrology Blog".tr()),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.search))],
      ),
      body: blogData == null
          ? Center(
              child: CircularProgressIndicator(
                color: AppColor.appColor,
              ),
            )
          : SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: blogData!.data.length,
                      itemBuilder: (BuildContext context, int i) {
                        return Column(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.of(context).push(SlideRightRoute(
                                    page: BlogDetailsScreen(
                                  bloggerName:
                                      "${blogData!.data[i].bloggerName.toString()}",
                                  date:
                                      "${blogData!.data[i].createTime.toString()}",
                                  id: "${blogData!.data[i].id.toString()}",
                                  img: "${blogData!.data[i].image.toString()}",
                                  title:
                                      "${blogData!.data[i].title.toString()}",
                                  description:
                                      '${blogData!.data[i].longDescription.toString()}',
                                )));
                              },
                              child: Card(
                                elevation: 2,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0))),
                                child: Stack(
                                  children: <Widget>[
                                    Positioned(
                                      child: CachedNetworkImage(
                                        fit: BoxFit.fill,
                                        imageUrl:
                                            "${blogData!.data[i].image.toString()}",
                                      ),
                                      right: 0,
                                      left: 0,
                                      top: 0,
                                      bottom: 0,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 230),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 12, vertical: 4),
                                      color: Colors.white,
                                      width: double.infinity,
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                              "${blogData!.data[i].title.toString()}",
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "${blogData!.data[i].bloggerName!.toUpperCase().toString()}",
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.black38),
                                                // textAlign: TextAlign.center,
                                              ),
                                              Text(
                                                "${DateFormat("MMM dd yyyy").format(DateTime.parse(blogData!.data[i].createTime.toString()))}",
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.black38),
                                                // textAlign: TextAlign.center,
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: SizedBox(
                                        width: 65,
                                        height: 30,
                                        child: Card(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, right: 8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.remove_red_eye,
                                                  size: 15,
                                                ),
                                                Text(
                                                  "${blogData!.data[i].view.toString()}",
                                                  style:
                                                      TextStyle(fontSize: 15),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
