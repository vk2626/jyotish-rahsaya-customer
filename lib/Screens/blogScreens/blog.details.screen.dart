import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../utils/AppColor.dart';

class BlogDetailsScreen extends StatefulWidget {
  const BlogDetailsScreen(
      {super.key,
      required this.date,
      required this.id,
      required this.img,
      required this.bloggerName,
      required this.description,
      required this.title});

  final String date;
  final String id;
  final String img;
  final String description;
  final String title;
  final String bloggerName;

  @override
  State<BlogDetailsScreen> createState() => _BlogDetailsScreenState();
}

class _BlogDetailsScreenState extends State<BlogDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    final DateTime dateTime =
        DateFormat("dd-MM-yyyy").parse(widget.date.toString());
    final String formattedDate = DateFormat('dd').format(dateTime);
    final String formattedMonth = DateFormat('MM').format(dateTime);
    final String formattedYear = DateFormat('yyyy').format(dateTime);
    // final String formattedTime = DateFormat('hh:mm a').format(dateTime);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Astrology Blog".tr(),
          style: GoogleFonts.poppins(),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Container(
              height: 40,
              width: 110,
              child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              side:
                                  const BorderSide(color: AppColor.darkGrey))),
                      backgroundColor: const MaterialStatePropertyAll<Color>(
                          AppColor.appColor)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Image.asset(
                        "asset/images/whatsapp.png",
                        height: 20,
                      ),
                      Text(
                        "Share".tr(),
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
            ),
          ),
        ],
      ),
      body: LayoutBuilder(builder: (context, BoxConstraints constraints) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 200,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.green,
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                              "${widget.img.toString()}"),
                          fit: BoxFit.cover)),
                ),
                SizedBox(height: 10),
                Text(
                  "${widget.title.toString()}",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Author",
                    style: TextStyle(fontSize: 14, color: AppColor.borderColor),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "${widget.bloggerName.toUpperCase().toString()}",
                      style: TextStyle(fontSize: 14),
                    ),
                    Text(
                      "${formattedDate.toString()}-${formattedMonth.toString()}-${formattedYear.toString()}",
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
                Html(data: "${widget.description.toString()}"),
              ],
            ),
          ),
        );
      }),
    );
  }
}
