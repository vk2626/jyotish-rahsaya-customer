// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:jyotish_rahsaya/Core/Provider/kundli_provider.dart';
// import 'package:jyotish_rahsaya/Core/logger_helper.dart';
// import 'package:provider/provider.dart';
// import 'kundli_tabs/astakvarga_tab.dart';
// import 'kundli_tabs/dasha_tab.dart';
// import 'kundli_tabs/kp_tab.dart';
// import 'kundli_tabs/kundli_basic_tab.dart';
// import 'kundli_tabs/kundli_chart_tab.dart';
// import 'kundli_tabs/kundli_report_tab.dart';
// import '../../../utils/custom_appbar.dart';

// class ViewKundliScreen extends StatefulWidget {
//   final String dob;
//   final String tob;
//   final String lat;
//   final String lon;
//   final String userName;
//   final String pob;
//   bool isEnglish;
//   ViewKundliScreen(
//       {super.key,
//       required this.dob,
//       required this.tob,
//       required this.lat,
//       required this.pob,
//       required this.userName,
//       required this.lon,
//       this.isEnglish = true});

//   @override
//   _ViewKundliScreenState createState() => _ViewKundliScreenState();
// }

// class _ViewKundliScreenState extends State<ViewKundliScreen> {
//   // Initialize language state
//   late bool isEnglish;
//   @override
//   void initState() {
//     super.initState();
//     isEnglish = widget.isEnglish;
//   }

//   @override
//   Widget build(BuildContext context) {
//     TLoggerHelper.info(
//         "DOB: ${widget.dob} TOB: ${widget.tob} Lat: ${widget.lat} Lon: ${widget.lon} userName: ${widget.userName} POB: ${widget.pob}");

//     return SafeArea(
//       child: Scaffold(
//         appBar: AppBar(
//           title: Text(isEnglish ? "Kundli" : "कुंडली"),
//           actions: [
//             Row(
//               children: [
//                 Text(
//                   isEnglish ? "English" : "हिंदी",
//                   style: GoogleFonts.poppins(),
//                 ),
//                 SizedBox(width: 12),
//                 Switch(
//                   value: !isEnglish,
//                   onChanged: (value) {
//                     setState(() {
//                       isEnglish = !value; // Toggle language
//                     });
                    
//                     Navigator.of(context).pushReplacement(MaterialPageRoute(
//                       builder: (context) => ListenableProvider(
//                         create: (context) => KundliProvider(),
//                         child: ViewKundliScreen(
//                             dob: widget.dob,
//                             tob: widget.tob,
//                             lat: widget.lat,
//                             pob: widget.pob,
//                             userName: widget.userName,
//                             lon: widget.lon,
//                             isEnglish: isEnglish),
//                       ),
//                     ));
//                   },
//                 ),
//               ],
//             ),
//             const SizedBox(width: 16),
//           ],
//         ),
//         backgroundColor: Colors.white,
//         body: Padding(
//           padding: const EdgeInsets.only(bottom: 12),
//           child: DefaultTabController(
//             length: 6,
//             child: Column(
//               children: [
//                 TabBar(
//                   labelStyle: const TextStyle(
//                       fontSize: 16, fontWeight: FontWeight.bold),
//                   labelColor: Colors.black,
//                   unselectedLabelColor: Colors.grey,
//                   indicatorColor: Colors.black,
//                   isScrollable: true,
//                   tabs: [
//                     Tab(text: isEnglish ? "Basic" : "बुनियादी"),
//                     Tab(text: isEnglish ? "Charts" : "चार्ट"),
//                     Tab(text: isEnglish ? "KP" : "केपी"),
//                     Tab(text: isEnglish ? "Ashtakvarga" : "अष्टकवर्ग"),
//                     Tab(text: isEnglish ? "Dasha" : "दशा"),
//                     Tab(text: isEnglish ? "Report" : "रिपोर्ट"),
//                   ],
//                 ),
//                 const SizedBox(height: 16),
//                 Expanded(
//                   child: TabBarView(
//                     children: [
//                       KundliBasicTab(
//                         dob: widget.dob,
//                         tob: widget.tob,
//                         lat: widget.lat,
//                         lon: widget.lon,
//                         pob: widget.pob,
//                         userName: widget.userName,
//                         lang: isEnglish ? "en" : "hi",
//                       ),
//                       KundliChartTab(
//                           dob: widget.dob,
//                           tob: widget.tob,
//                           lat: widget.lat,
//                           lang: isEnglish ? "en" : "hi",
//                           lon: widget.lon),
//                       KpTab(
//                           dob: widget.dob,
//                           tob: widget.tob,
//                           lat: widget.lat,
//                           lang: isEnglish ? "en" : "hi",
//                           lon: widget.lon),
//                       AstakVargaTab(
//                         dob: widget.dob,
//                         tob: widget.tob,
//                         lat: widget.lat,
//                         lang: isEnglish ? "en" : "hi",
//                         lon: widget.lon,
//                       ),
//                       DashaTab(
//                         lat: widget.lat,
//                         lon: widget.lon,
//                         dob: widget.dob,
//                         lang: isEnglish ? "en" : "hi",
//                         tob: widget.tob,
//                       ),
//                       KundliReportTab(
//                         dob: widget.dob,
//                         tob: widget.tob,
//                         lang: isEnglish ? "en" : "hi",
//                         lat: widget.lat,
//                         lon: widget.lon,
//                         userName: widget.userName,
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
