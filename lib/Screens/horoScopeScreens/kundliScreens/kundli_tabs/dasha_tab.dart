// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:jyotish_rahsaya/Core/Model/dasha_model.dart';
// import 'package:jyotish_rahsaya/Core/Provider/kundli_provider.dart';
// import 'package:jyotish_rahsaya/Core/formatter.dart';
// import 'package:jyotish_rahsaya/Core/helper_functions.dart';
// import 'package:jyotish_rahsaya/utils/AppColor.dart';
// import 'package:provider/provider.dart';

// class DashaTab extends StatefulWidget {
//   final String dob;
//   final String tob;
//   final String lat;
//   final String lon;
//   final String lang;
//   const DashaTab({
//     super.key,
//     required this.lat,
//     required this.lon,
//     required this.lang,
//     required this.dob,
//     required this.tob,
//   });

//   @override
//   State<DashaTab> createState() => _DashaTabState();
// }

// class _DashaTabState extends State<DashaTab>
//     with AutomaticKeepAliveClientMixin {
//   List<DashaPeriod> dashaPeriods = [];
//   List<DashaPeriod>? nestedDashaPeriods;
//   List<String> currentDashaPath = [];
//   @override
//   void initState() {
//     super.initState();
//     getData();
//     currentDashaPath = widget.lang == "en" ? ["Mahadasha"] : ["महादशा"];
//   }

//   // Fetch the Dasha data and populate the table
//   getData() async {
//     String newDateTime =
//         THelperFunctions.formatDateTimeProKerala(widget.dob, widget.tob);
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getDashaData(
//             lat: widget.lat,
//             lon: widget.lon,
//             dateTime: newDateTime,
//             lang: widget.lang)
//         .then((value) {
//       if (value != null) {
//         setState(() {
//           dashaPeriods = value;
//         });
//       }
//     });
//   }

//   // Update the table when sub-periods are clicked
//   void updateDashaPeriods(List<DashaPeriod>? subPeriods, String level) {
//     if (subPeriods != null && subPeriods.isNotEmpty) {
//       setState(() {
//         nestedDashaPeriods = subPeriods;
//         currentDashaPath.add(level);
//       });
//     }
//   }

//   // Function to reset the Dasha level (Go back to a specific point in the path)
//   void resetToLevel(int index) {
//     setState(() {
//       if (index == 0) {
//         // Reset to Mahadasha
//         nestedDashaPeriods = null;
//         currentDashaPath = widget.lang == "en" ? ["Mahadasha"] : ["महादशा"];
//       } else if (index == 1) {
//         // Reset to Antardasha
//         nestedDashaPeriods = dashaPeriods[index - 1].antardasha;
//         currentDashaPath = currentDashaPath.sublist(0, 2);
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     List<String> columnsList = widget.lang == "en"
//         ? ["Planets", "Start Date", "End Date"]
//         : ["ग्रह", "प्रारंभ तिथि", "अंत तिथि"];

//     List<DashaPeriod> periodsToShow = nestedDashaPeriods ?? dashaPeriods;

//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 18),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Padding(
//             padding: const EdgeInsets.only(right: 8.0),
//             child: Chip(
//               label: Text(
//                 widget.lang == "en" ? "Vimshottari" : "विंशोत्तरी",
//                 style: GoogleFonts.poppins(fontWeight: FontWeight.w500),
//               ),
//               backgroundColor: AppColor.appColor.withOpacity(.75),
//             ),
//           ),
//           SizedBox(height: 8),

//           // Display the current dasha path as breadcrumb
//           SingleChildScrollView(
//             scrollDirection: Axis.horizontal,
//             child: Row(
//               children: List.generate(
//                 currentDashaPath.length,
//                 (index) => GestureDetector(
//                   onTap: () {
//                     resetToLevel(index);
//                   },
//                   child: Padding(
//                     padding: const EdgeInsets.symmetric(horizontal: 8.0),
//                     child: Row(
//                       children: [
//                         Text(
//                           currentDashaPath[index],
//                           style: GoogleFonts.poppins(
//                             fontSize: 16,
//                             color: index == currentDashaPath.length - 1
//                                 ? Colors.black
//                                 : Colors.blue,
//                           ),
//                         ),
//                         if (index != currentDashaPath.length - 1)
//                           const Icon(Icons.chevron_right, color: Colors.grey),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),

//           SizedBox(height: 8),

//           Expanded(
//             child: DataTable(
//               border: TableBorder.all(
//                 borderRadius: BorderRadius.circular(12),
//                 color: Colors.grey.withOpacity(.2),
//                 width: 2,
//               ),
//               headingRowColor:
//                   MaterialStateProperty.all(AppColor.appColor.withOpacity(.4)),
//               dataTextStyle: GoogleFonts.poppins(fontSize: 13),
//               columns: List.generate(
//                 columnsList.length,
//                 (index) => DataColumn(
//                   label: Container(
//                     alignment: Alignment.center,
//                     child: Text(
//                       TFormatter.capitalizeSentence(columnsList[index]),
//                     ),
//                   ),
//                 ),
//               ),
//               rows: periodsToShow.map((period) {
//                 bool hasNestedDasha = (period.antardasha != null &&
//                         period.antardasha!.isNotEmpty) ||
//                     (period.pratyantardasha != null &&
//                         period.pratyantardasha!.isNotEmpty);

//                 return DataRow(
//                   cells: [
//                     DataCell(
//                       Container(
//                         alignment: Alignment.center,
//                         child: Text(period.name),
//                       ),
//                     ),
//                     DataCell(
//                       Container(
//                         alignment: Alignment.center,
//                         child: Text(TFormatter.formatDate(period.start)),
//                       ),
//                     ),
//                     DataCell(
//                       GestureDetector(
//                         onTap: () {
//                           // Check for Antardasha or Pratyantardasha
//                           if (hasNestedDasha) {
//                             if (period.antardasha != null &&
//                                 period.antardasha!.isNotEmpty) {
//                               updateDashaPeriods(
//                                   period.antardasha,
//                                   widget.lang == "en"
//                                       ? "Antardasha"
//                                       : "अन्तर्दशा");
//                             } else if (period.pratyantardasha != null &&
//                                 period.pratyantardasha!.isNotEmpty) {
//                               updateDashaPeriods(
//                                   period.pratyantardasha,
//                                   widget.lang == "en"
//                                       ? "Pratyantardasha"
//                                       : "प्रत्यान्तर्दशा");
//                             }
//                           }
//                         },
//                         child: Container(
//                           alignment: Alignment.center,
//                           child: Text(
//                             hasNestedDasha
//                                 ? TFormatter.formatDate(period.end) + ">"
//                                 : TFormatter.formatDate(period.end),
//                             style: TextStyle(
//                               color:
//                                   hasNestedDasha ? Colors.blue : Colors.black,
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 );
//               }).toList(),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   @override
//   // TODO: implement wantKeepAlive
//   bool get wantKeepAlive => true;
// }
