// import 'dart:developer';

// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:jyotish_rahsaya/Core/helper_functions.dart';
// import 'package:jyotish_rahsaya/Core/logger_helper.dart';
// import '../../../../Core/Provider/kundli_provider.dart';
// import '../../../../Core/formatter.dart';
// import 'package:provider/provider.dart';

// import '../../../../utils/AppColor.dart';

// class KundliChartTab extends StatefulWidget {
//   final String dob;
//   final String tob;
//   final String lat;
//   final String lon;
//   final String lang;
//   const KundliChartTab(
//       {super.key,
//       required this.dob,
//       required this.tob,
//       required this.lat,
//       required this.lon,
//       required this.lang});

//   @override
//   State<KundliChartTab> createState() => _KundliChartTabState();
// }

// class _KundliChartTabState extends State<KundliChartTab>
//     with AutomaticKeepAliveClientMixin {
//   ScrollController _scrollController = ScrollController();
//   List<Map<String, String>> data = [];
//   String lagnaChartNorthIndian = "";
//   String lagnaChartSouthIndian = "";
//   String navasmaChartNorthIndian = "";
//   String navasmaChartSouthIndian = "";

//   bool isLoading = true;
//   // @override
//   // void initState() {
//   //   super.initState();
//   //   getData();
//   // }
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     getData();
//   }

//   getData() async {
//     String newDateTime =
//         THelperFunctions.formatDateTimeProKerala(widget.dob, widget.tob);
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getPlanetDetails(
//             dob: widget.dob,
//             tob: widget.tob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             data = value;
//             isLoading = false;
//           });
//         }
//       },
//     );
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getChart(
//             lat: widget.lat,
//             lon: widget.lon,
//             dateTime: newDateTime,
//             lang: widget.lang,
//             chartType: "lagna")
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             lagnaChartNorthIndian = value;
//             log(lagnaChartNorthIndian);
//           });
//         }
//       },
//     );
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getChart(
//             lat: widget.lat,
//             lon: widget.lon,
//             dateTime: newDateTime,
//             chartType: "lagna",
//             lang: widget.lang,
//             isSouthIndian: true)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             lagnaChartSouthIndian = value;
//           });
//         }
//       },
//     );
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getChart(
//             lat: widget.lat,
//             lon: widget.lon,
//             dateTime: newDateTime,
//             lang: widget.lang,
//             chartType: "navamsa")
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             navasmaChartNorthIndian = value;
//           });
//         }
//       },
//     );
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getChart(
//             lat: widget.lat,
//             lon: widget.lon,
//             dateTime: newDateTime,
//             chartType: "navamsa",
//             lang: widget.lang,
//             isSouthIndian: true)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             navasmaChartSouthIndian = value;
//           });
//         }
//       },
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     List<String> selectionsList = widget.lang == "en" ? ["Sign"] : ["संकेत"];
//     return isLoading
//         ? Center(
//             child: CustomCircularProgressIndicator(),
//           )
//         : SingleChildScrollView(
//             child: Container(
//               padding: const EdgeInsets.symmetric(horizontal: 18),
//               width: MediaQuery.of(context).size.width,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   DefaultTabController(
//                     length: 2, // Lagna and Navasma tabs
//                     child: Column(
//                       children: [
//                         TabBar(
//                           tabs: [
//                             Tab(
//                                 child: Text(widget.lang == "en"
//                                     ? "Lagna" // English Label
//                                     : "लग्न")), // Hindi Label
//                             Tab(
//                                 child: Text(widget.lang == "en"
//                                     ? "Navasma" // English Label
//                                     : "नवमांश")), // Hindi Label
//                           ],
//                           labelStyle: GoogleFonts.poppins(fontSize: 16),
//                           labelColor: Colors.black,
//                           unselectedLabelColor: Colors.grey,
//                           indicatorSize: TabBarIndicatorSize.tab,
//                           indicatorColor: AppColor.appColor,
//                         ),
//                         SizedBox(height: 12),
//                         SizedBox(
//                           height: MediaQuery.of(context).size.height * .58,
//                           child: TabBarView(
//                             physics: NeverScrollableScrollPhysics(),
//                             children: [
//                               // Lagna Tab Content
//                               DefaultTabController(
//                                 length: 2,
//                                 child: Column(
//                                   children: [
//                                     Container(
//                                       height: kToolbarHeight - 8.0,
//                                       child: TabBar(
//                                         labelStyle:
//                                             GoogleFonts.poppins(fontSize: 16),
//                                         labelColor: Colors.black,
//                                         unselectedLabelColor: Colors.grey,
//                                         indicatorSize: TabBarIndicatorSize.tab,
//                                         indicator: BoxDecoration(
//                                           borderRadius:
//                                               BorderRadius.circular(8.0),
//                                           color: AppColor.appColor,
//                                         ),
//                                         dividerColor: Colors.transparent,
//                                         padding: EdgeInsets.symmetric(
//                                             horizontal: 18, vertical: 6),
//                                         tabs: [
//                                           Tab(
//                                               child: Text(widget.lang == "en"
//                                                   ? "North Indian" // English Label
//                                                   : "उत्तर भारतीय")), // Hindi Label
//                                           Tab(
//                                               child: Text(widget.lang == "en"
//                                                   ? "South Indian" // English Label
//                                                   : "दक्षिण भारतीय")), // Hindi Label
//                                         ],
//                                       ),
//                                     ),
//                                     SizedBox(
//                                       height:
//                                           MediaQuery.of(context).size.height *
//                                               .5,
//                                       child: TabBarView(
//                                         physics: NeverScrollableScrollPhysics(),
//                                         children: [
//                                           Center(
//                                             child: lagnaChartNorthIndian.isEmpty
//                                                 ? CustomCircularProgressIndicator()
//                                                 : SvgPicture.string(
//                                                     lagnaChartNorthIndian),
//                                           ),
//                                           Center(
//                                             child: lagnaChartSouthIndian.isEmpty
//                                                 ? CustomCircularProgressIndicator()
//                                                 : SvgPicture.string(
//                                                     lagnaChartSouthIndian),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               // Navasma Tab Content
//                               DefaultTabController(
//                                 length: 2,
//                                 child: Column(
//                                   children: [
//                                     // Navasma-specific content can be added here
//                                     Container(
//                                       height: kToolbarHeight - 8.0,
//                                       child: TabBar(
//                                         labelStyle:
//                                             GoogleFonts.poppins(fontSize: 16),
//                                         labelColor: Colors.black,
//                                         unselectedLabelColor: Colors.grey,
//                                         indicatorSize: TabBarIndicatorSize.tab,
//                                         indicator: BoxDecoration(
//                                           borderRadius:
//                                               BorderRadius.circular(8.0),
//                                           color: AppColor.appColor,
//                                         ),
//                                         dividerColor: Colors.transparent,
//                                         padding: EdgeInsets.symmetric(
//                                             horizontal: 18, vertical: 6),
//                                         tabs: [
//                                           Tab(
//                                               child: Text(widget.lang == "en"
//                                                   ? "North Indian" // English Label
//                                                   : "उत्तर भारतीय")), // Hindi Label
//                                           Tab(
//                                               child: Text(widget.lang == "en"
//                                                   ? "South Indian" // English Label
//                                                   : "दक्षिण भारतीय")), // Hindi Label
//                                         ],
//                                       ),
//                                     ),
//                                     SizedBox(
//                                       height:
//                                           MediaQuery.of(context).size.height *
//                                               .5,
//                                       child: TabBarView(
//                                         physics: NeverScrollableScrollPhysics(),
//                                         children: [
//                                           Center(
//                                             child: navasmaChartNorthIndian
//                                                     .isEmpty
//                                                 ? CustomCircularProgressIndicator()
//                                                 : SvgPicture.string(
//                                                     navasmaChartNorthIndian),
//                                           ),
//                                           Center(
//                                             child: navasmaChartSouthIndian
//                                                     .isEmpty
//                                                 ? CustomCircularProgressIndicator()
//                                                 : SvgPicture.string(
//                                                     navasmaChartSouthIndian),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               )
//                             ],
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   SingleChildScrollView(
//                     scrollDirection: Axis.horizontal,
//                     child: Row(
//                       children: List.generate(
//                         selectionsList.length,
//                         (index) => Padding(
//                           padding: const EdgeInsets.only(right: 8.0),
//                           child: Chip(
//                             label: Text(
//                               selectionsList[index],
//                               style: GoogleFonts.poppins(
//                                   fontWeight: FontWeight.w500),
//                             ),
//                             backgroundColor: index == 0
//                                 ? AppColor.appColor.withOpacity(.75)
//                                 : Colors.transparent,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                   const SizedBox(height: 12),
//                   Scrollbar(
//                     thickness: 8,
//                     thumbVisibility: true,
//                     controller:
//                         _scrollController, // Provide the ScrollController here
//                     child: Container(
//                       decoration: BoxDecoration(
//                         border: Border.all(
//                             color: Colors.grey.withOpacity(.2), width: 2),
//                         borderRadius: BorderRadius.circular(12),
//                       ),
//                       child: SingleChildScrollView(
//                         controller: _scrollController,
//                         scrollDirection: Axis.horizontal,
//                         child: DataTable(
                          
//                           columnSpacing: 8,
//                           headingRowColor: MaterialStateProperty.all(
//                               AppColor.appColor.withOpacity(.4)),
//                           dataTextStyle: GoogleFonts.poppins(fontSize: 14),
//                           columns: data.first.keys.map((key) {
//                             return DataColumn(
//                               label: Flexible(
//                                 child: Container(
//                                   alignment: Alignment.center,
//                                   child: Text(
//                                     TFormatter.capitalizeSentence(key),
//                                   ),
//                                 ),
//                               ),
//                             );
//                           }).toList(),
//                           rows: data.map((row) {
//                             return DataRow(
//                               cells: row.keys.map((key) {
//                                 return DataCell(
//                                   Container(
//                                     alignment: Alignment.center,
//                                     child: Text(
//                                       row[key]!,
//                                       textAlign: TextAlign.center,
//                                     ),
//                                   ),
//                                 );
//                               }).toList(),
//                             );
//                           }).toList(),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           );
//   }

//   @override
//   bool get wantKeepAlive => true;
// }
