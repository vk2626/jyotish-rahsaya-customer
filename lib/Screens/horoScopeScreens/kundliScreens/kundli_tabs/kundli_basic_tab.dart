// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import '../../../../Core/Provider/kundli_provider.dart';
// import '../../../../Core/formatter.dart';
// import '../../../../utils/AppColor.dart';
// import 'package:provider/provider.dart';

// class KundliBasicTab extends StatefulWidget {
//   final String userName;
//   final String dob;
//   final String tob;
//   final String lat;
//   final String lon;
//   final String pob;
//   final String lang;
//   const KundliBasicTab(
//       {super.key,
//       required this.userName,
//       required this.dob,
//       required this.pob,
//       required this.tob,
//       required this.lat,
//       required this.lon,
//       required this.lang});

//   @override
//   State<KundliBasicTab> createState() => _KundliBasicTabState();
// }

// class _KundliBasicTabState extends State<KundliBasicTab>
//     with AutomaticKeepAliveClientMixin {
//   Map<String, String> data = {};
//   bool isLoading = true;
//   // @override
//   // void initState() {
//   //   super.initState();
//   // }
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     getData();
//   }

//   getData() async {
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getPanchangDetails(
//             date: widget.dob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang,
//             time: widget.tob)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             data = value;
//             isLoading = false;
//           });
//         }
//       },
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     // English and Hindi versions of titles based on the selected language
//     List<String> titlesList = widget.lang == "en"
//         ? [
//             "Name", // Name in English
//             "Date", // Date in English
//             "Time", // Time in English
//             "Place", // Place in English
//             "Latitude", // Latitude in English
//             "Longitude", // Longitude in English
//             "Timezone", // Timezone in English
//           ]
//         : [
//             "नाम", // Name in Hindi
//             "तिथि", // Date in Hindi
//             "समय", // Time in Hindi
//             "स्थान", // Place in Hindi
//             "अक्षांश", // Latitude in Hindi
//             "देशांतर", // Longitude in Hindi
//             "समय क्षेत्र", // Timezone in Hindi
//           ];
//     List<String> ansList = [
//       widget.userName,
//       widget.dob,
//       TFormatter.convertTo12HourFormat(widget.tob),
//       widget.pob,
//       widget.lat,
//       widget.lon,
//       "GMT+5.5",
//     ];
//     return isLoading
//         ? Center(
//             child: CustomCircularProgressIndicator(),
//           )
//         : SingleChildScrollView(
//             child: Container(
//               padding: const EdgeInsets.symmetric(horizontal: 18),
//               width: MediaQuery.of(context).size.width,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Text(
//                     widget.lang == "en"
//                         ? "Kundli Basic Details"
//                         : "कुंडली मूल विवरण",
//                     style: GoogleFonts.poppins(
//                         fontSize: 18, fontWeight: FontWeight.w500),
//                   ),
//                   SizedBox(height: 12),
//                   Container(
//                     decoration: BoxDecoration(
//                         border: Border.all(
//                             color: Colors.grey.withOpacity(.2), width: 2),
//                         borderRadius: BorderRadius.circular(12)),
//                     child: Column(
//                       children: List.generate(
//                           titlesList.length,
//                           (index) => Container(
//                                 padding: const EdgeInsets.all(10.0),
//                                 decoration: BoxDecoration(
//                                     color: index % 2 == 0
//                                         ? AppColor.appColor.withOpacity(.3)
//                                         : Colors.transparent),
//                                 child: Row(
//                                   children: [
//                                     Expanded(
//                                         child: Text(
//                                       titlesList[index],
//                                       style: GoogleFonts.poppins(
//                                           fontSize: 16,
//                                           fontWeight: FontWeight.w500),
//                                     )),
//                                     Expanded(
//                                         child: Text(
//                                       ansList[index],
//                                       style: GoogleFonts.poppins(
//                                           fontSize: 16,
//                                           fontWeight: FontWeight.w500),
//                                     )),
//                                   ],
//                                 ),
//                               )),
//                     ),
//                   ),
//                   SizedBox(height: 18),
//                   Text(
//                     widget.lang == "en" ? "Panchang Details" : "पंचांग विवरण",
//                     style: GoogleFonts.poppins(
//                         fontSize: 18, fontWeight: FontWeight.w500),
//                   ),
//                   SizedBox(height: 12),
//                   Container(
//                     decoration: BoxDecoration(
//                         border: Border.all(
//                             color: Colors.grey.withOpacity(.2), width: 2),
//                         borderRadius: BorderRadius.circular(12)),
//                     child: Column(
//                       children: List.generate(
//                           data.length,
//                           (index) => Container(
//                                 padding: const EdgeInsets.all(8.0),
//                                 decoration: BoxDecoration(
//                                     color: index % 2 == 0
//                                         ? AppColor.appColor.withOpacity(.3)
//                                         : Colors.transparent),
//                                 child: Row(
//                                   children: [
//                                     Expanded(
//                                         child: Text(
//                                       TFormatter.capitalize(
//                                           data.keys.elementAt(index)),
//                                       style: GoogleFonts.poppins(
//                                           fontSize: 16,
//                                           fontWeight: FontWeight.w500),
//                                     )),
//                                     Expanded(
//                                         child: Text(
//                                       TFormatter.capitalize(
//                                           data.values.elementAt(index)),
//                                       style: GoogleFonts.poppins(
//                                           fontSize: 16,
//                                           fontWeight: FontWeight.w500),
//                                     )),
//                                   ],
//                                 ),
//                               )),
//                     ),
//                   ),
//                   SizedBox(height: 18),
//                 ],
//               ),
//             ),
//           );
//   }

//   @override
//   bool get wantKeepAlive => true;
// }
