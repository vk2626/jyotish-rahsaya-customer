// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import '../../../../Core/Provider/kundli_provider.dart';
// import '../../../../Core/formatter.dart';
// import '../../../../utils/AppColor.dart';
// import 'package:provider/provider.dart';

// import '../../../../Core/Model/sadesatidetailsmodel.dart';

// class KundliReportTab extends StatefulWidget {
//   final String dob;
//   final String tob;
//   final String lat;
//   final String lon;
//   final String userName;
//   final String lang;
//   const KundliReportTab(
//       {super.key,
//       required this.dob,
//       required this.tob,
//       required this.lat,
//       required this.lang,
//       required this.lon,
//       required this.userName});

//   @override
//   State<KundliReportTab> createState() => _KundliReportTabState();
// }

// class _KundliReportTabState extends State<KundliReportTab>
//     with AutomaticKeepAliveClientMixin {
//   ScrollController _scrollController = ScrollController();
//   Map<String, String> manglinkAnalysis = {};
//   Map<String, String> kalsarpaAnalysis = {};
//   List<SadesatiTableList> sadeSatiTable = [];
//   bool isLoading = true;
//   @override
//   void initState() {
//     super.initState();
//     futures();
//   }

//   futures() async {
//     var res1 = getDataManglikAnalysis();
//     var res2 = getDataKalsarpaAnalysis();
//     var res3 = getSadeSatiDetails();
//     await Future.wait([res1, res2, res3]).whenComplete(() {
//       setState(() {
//         isLoading = false;
//       });
//     });
//   }

//   Future getDataManglikAnalysis() async {
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getManglikAnalysis(
//             dob: widget.dob,
//             tob: widget.tob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang)
//         .then((val) {
//       if (val != null) {
//         setState(() {
//           manglinkAnalysis = val;
//         });
//       }
//     });
//   }

//   Future getDataKalsarpaAnalysis() async {
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getKalsarpaAnalysis(
//             dob: widget.dob,
//             tob: widget.tob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             kalsarpaAnalysis = value;
//           });
//         }
//       },
//     );
//   }

//   Future getSadeSatiDetails() async {
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getSadeSatiDetails(
//             dob: widget.dob,
//             tob: widget.tob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             sadeSatiTable = value;
//           });
//         }
//       },
//     );
//   }

//   int selectionIndex = 0;
//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     List<String> sadeSatiTableColumnsList = widget.lang == "en"
//         ? ["Start", "End", "Sign Name", "Type"]
//         : ["प्रारंभ", "अंत", "चिह्न नाम", "प्रकार"];
//     List<String> selectionList = widget.lang == 'en'
//         ? ["Manglik", "Kalsarpa", "Sadesati"]
//         : ["मांगलिक", "कालसर्प", "साढ़ेसाती"];
//     return isLoading
//         ? Center(
//             child: CustomCircularProgressIndicator(),
//           )
//         : SingleChildScrollView(
//             child: Container(
//               padding: const EdgeInsets.symmetric(horizontal: 18),
//               width: MediaQuery.of(context).size.width,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   SingleChildScrollView(
//                     scrollDirection: Axis.horizontal,
//                     child: Row(
//                         children: List.generate(
//                       selectionList.length,
//                       (index) => Container(
//                         margin: const EdgeInsets.only(right: 8.0),
//                         child: InkWell(
//                           onTap: () {
//                             setState(() {
//                               selectionIndex = index;
//                             });
//                           },
//                           child: Chip(
//                             label: Text(selectionList[index],
//                                 style: GoogleFonts.poppins(
//                                     fontWeight: FontWeight.w500)),
//                             color: WidgetStatePropertyAll(
//                                 index == selectionIndex
//                                     ? AppColor.appColor.withOpacity(.75)
//                                     : Colors.transparent),
//                           ),
//                         ),
//                       ),
//                     )),
//                   ),
//                   const SizedBox(height: 12),
//                   Text(
//                     "${selectionList[selectionIndex]} " +
//                         (widget.lang == "en" ? "Analysis" : "विश्लेषण"),
//                     style: GoogleFonts.poppins(
//                         fontSize: 18, fontWeight: FontWeight.w500),
//                   ),
//                   const SizedBox(height: 12),
//                   Container(
//                     padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
//                     decoration: BoxDecoration(
//                         border: Border.all(color: Colors.green, width: 1.5),
//                         borderRadius: BorderRadius.circular(8)),
//                     child: Row(
//                       children: [
//                         CircleAvatar(
//                           radius: 36,
//                           backgroundColor: Colors.green,
//                           child: Text(
//                             selectionIndex == 0
//                                 ? manglinkAnalysis.entries.first.key
//                                 : selectionIndex == 1
//                                     ? kalsarpaAnalysis.entries.first.key
//                                     : getSadeSatiStatus(),
//                             style: GoogleFonts.poppins(
//                                 fontSize: 20,
//                                 color: Colors.white,
//                                 fontWeight: FontWeight.w500),
//                           ),
//                         ),
//                         SizedBox(width: 12),
//                         Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Text(
//                               widget.userName,
//                               style: GoogleFonts.poppins(
//                                   fontWeight: FontWeight.w600, fontSize: 16),
//                             ),
//                             SizedBox(height: 4),
//                             SizedBox(
//                               width: MediaQuery.of(context).size.width * .6,
//                               child: Text(
//                                 selectionIndex == 0
//                                     ? manglinkAnalysis.entries.first.value
//                                     : selectionIndex == 1
//                                         ? kalsarpaAnalysis.entries.first.value
//                                         : widget.lang == "en"
//                                             ? "Your next Sadesati starts from"
//                                             : "आपकी अगली साढ़ेसाती कब से शुरू होगी" +
//                                                 " ${getNextSadesatiStartDate()}",
//                                 maxLines: null,
//                                 style: GoogleFonts.poppins(
//                                     fontWeight: FontWeight.w500, fontSize: 15),
//                               ),
//                             ),
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//                   const SizedBox(height: 12),
//                   if (selectionIndex == 0)
//                     Text(
//                       widget.lang == "en"
//                           ? "This is a computer generated result. Please consult an Astrologer to confirm & understand this in detail."
//                           : "यह एक कंप्यूटर जनित परिणाम है। इसकी पुष्टि करने और विस्तार से समझने के लिए कृपया किसी ज्योतिषी से परामर्श लें।",
//                       maxLines: 3,
//                       style: GoogleFonts.poppins(
//                           fontWeight: FontWeight.w500,
//                           fontSize: 13,
//                           color: Colors.grey),
//                     ),
//                   if (selectionIndex == 2)
//                     Scrollbar(
//                       thickness: 8,
//                       thumbVisibility: true,
//                       controller:
//                           _scrollController, // Provide the ScrollController here
//                       child: Container(
//                         decoration: BoxDecoration(
//                             border: Border.all(
//                                 color: Colors.grey.withOpacity(.2), width: 2),
//                             borderRadius: BorderRadius.circular(12)),
//                         child: SingleChildScrollView(
//                           scrollDirection: Axis.horizontal,
//                           controller: _scrollController,
//                           child: DataTable(
//                             columnSpacing: 8,
//                             horizontalMargin: 8,
//                             headingTextStyle: GoogleFonts.poppins(
//                               fontSize: 16,
//                               fontWeight: FontWeight.w500,
//                             ),
//                             dataTextStyle: GoogleFonts.poppins(
//                               fontSize: 14,
//                             ),
//                             headingRowColor: WidgetStatePropertyAll(
//                                 AppColor.appColor.withOpacity(.4)),
//                             columns: sadeSatiTableColumnsList.map((item) {
//                               return DataColumn(
//                                 label: Flexible(
//                                   child: Container(
//                                     alignment: Alignment.center,
//                                     child: Text(
//                                         TFormatter.capitalizeSentence(item)),
//                                   ),
//                                 ),
//                               );
//                             }).toList(),
//                             rows: sadeSatiTable.map((row) {
//                               return DataRow(
//                                 cells: [
//                                   DataCell(
//                                     Container(
//                                       alignment: Alignment.center,
//                                       child: Text(row.startDate,
//                                           textAlign: TextAlign.center),
//                                     ),
//                                   ),
//                                   DataCell(
//                                     Container(
//                                       alignment: Alignment.center,
//                                       child: Text(row.endDate,
//                                           textAlign: TextAlign.center),
//                                     ),
//                                   ),
//                                   DataCell(
//                                     Container(
//                                       alignment: Alignment.center,
//                                       child: Text(row.zodiac,
//                                           textAlign: TextAlign.center),
//                                     ),
//                                   ),
//                                   DataCell(
//                                     Container(
//                                       alignment: Alignment.center,
//                                       child: Text(
//                                           row.direction == "N/A"
//                                               ? "-"
//                                               : row.direction,
//                                           textAlign: TextAlign.center),
//                                     ),
//                                   ),
//                                 ],
//                               );
//                             }).toList(),
//                           ),
//                         ),
//                       ),
//                     ),
//                 ],
//               ),
//             ),
//           );
//   }

//   String getSadeSatiStatus() {
//     // Function to determine if Sade Sati has started or not
//     if (sadeSatiTable.isEmpty) {
//       return widget.lang == "en"
//           ? "No"
//           : "नहीं"; // Sade Sati data not available
//     }

//     // Get current date
//     DateTime currentDate = DateTime.now();

//     // Iterate through sadeSatiTable to check if current date is within any Sade Sati period
//     for (var sadeSati in sadeSatiTable) {
//       // Parse start date and end date strings into DateTime objects
//       DateTime startDate = parseDateString(sadeSati.startDate);
//       DateTime endDate = parseDateString(sadeSati.endDate);

//       if (currentDate.isAfter(startDate) && currentDate.isBefore(endDate)) {
//         return widget.lang == "en" ? "Yes" : "हाँ"; // Sade Sati has started
//       }
//     }

//     return widget.lang == "en" ? "No" : "नहीं"; // Sade Sati has not started
//   }

//   DateTime parseDateString(String dateString) {
//     // Example dateString format: "Tue Aug 05 2149"
//     // You may need to adjust the parsing logic based on the exact format of your dateString
//     try {
//       // Split dateString into parts and construct DateTime
//       List<String> parts = dateString.split(" ");
//       String month = parts[1];
//       int day = int.parse(parts[2]);
//       int year = int.parse(parts[3]);
//       return DateTime(year, _getMonthNumber(month), day);
//     } catch (e) {
//       print("Error parsing date: $e");
//       return DateTime.now(); // Return current date as fallback
//     }
//   }

//   int _getMonthNumber(String month) {
//     switch (month.toLowerCase()) {
//       case "jan":
//         return 1;
//       case "feb":
//         return 2;
//       case "mar":
//         return 3;
//       case "apr":
//         return 4;
//       case "may":
//         return 5;
//       case "jun":
//         return 6;
//       case "jul":
//         return 7;
//       case "aug":
//         return 8;
//       case "sep":
//         return 9;
//       case "oct":
//         return 10;
//       case "nov":
//         return 11;
//       case "dec":
//         return 12;
//       default:
//         return 1;
//     }
//   }

//   String getNextSadesatiStartDate() {
//     if (sadeSatiTable.isEmpty) {
//       return "Unknown";
//     }

//     DateTime currentDate = DateTime.now();
//     DateTime? nextStartDate;

//     for (var sadeSati in sadeSatiTable) {
//       DateTime startDate = parseDateString(sadeSati.startDate);

//       if (startDate.isAfter(currentDate)) {
//         if (nextStartDate == null || startDate.isBefore(nextStartDate)) {
//           nextStartDate = startDate;
//         }
//       }
//     }

//     if (nextStartDate != null) {
//       return TFormatter.formatDate(nextStartDate);
//     } else {
//       return "Unknown";
//     }
//   }

//   @override
//   bool get wantKeepAlive => true;
// }
