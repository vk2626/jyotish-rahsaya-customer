// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import '../../../../Core/Provider/kundli_provider.dart';
// import '../../../../Core/formatter.dart';
// import '../../../../utils/AppColor.dart';
// import 'package:provider/provider.dart';

// class KpTab extends StatefulWidget {
//   final String dob;
//   final String tob;
//   final String lat;
//   final String lon;
//   final String lang;
//   const KpTab(
//       {super.key,
//       required this.dob,
//       required this.tob,
//       required this.lat,
//       required this.lon,
//       required this.lang});

//   @override
//   State<KpTab> createState() => _KpTabState();
// }

// class _KpTabState extends State<KpTab> with AutomaticKeepAliveClientMixin {
//   ScrollController _scrollControllerPlanets = ScrollController();
//   ScrollController _scrollControllerCusps = ScrollController();
//   List<Map<String, String>> planetsKP = [];
//   List<Map<String, String>> cuspsKP = [];
//   bool isLoading = true;
//   @override
//   void initState() {
//     super.initState();
//     futures();
//   }

//   futures() async {
//     var res1 = getPlanetsData();
//     var res2 = getCuspsData();
//     await Future.wait([res1, res2]).whenComplete(() {
//       setState(() {
//         isLoading = false;
//       });
//     });
//   }

//   Future getPlanetsData() async {
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getKPPlanetsDetails(
//             dob: widget.dob,
//             tob: widget.tob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             planetsKP = value;
//           });
//         }
//       },
//     );
//   }

//   Future getCuspsData() async {
//     await Provider.of<KundliProvider>(context, listen: false)
//         .getKPCuspsDetails(
//             dob: widget.dob,
//             tob: widget.tob,
//             lat: widget.lat,
//             lon: widget.lon,
//             lang: widget.lang)
//         .then(
//       (value) {
//         if (value != null) {
//           setState(() {
//             cuspsKP = value;
//           });
//         }
//       },
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     return isLoading
//         ? Center(
//             child: CustomCircularProgressIndicator(),
//           )
//         : SingleChildScrollView(
//             child: Container(
//               padding: const EdgeInsets.symmetric(horizontal: 18),
//               width: MediaQuery.of(context).size.width,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Text(
//                     widget.lang == "en" ? "Planets" : "ग्रह",
//                     style: GoogleFonts.poppins(
//                         fontSize: 18, fontWeight: FontWeight.w500),
//                   ),
//                   SizedBox(height: 12),
//                   Scrollbar(
//                     thickness: 8,
//                     thumbVisibility: true,
//                     controller:
//                         _scrollControllerPlanets, // Provide the ScrollController here
//                     child: Container(
//                       decoration: BoxDecoration(
//                           border: Border.all(
//                               color: Colors.grey.withOpacity(.2), width: 2),
//                           borderRadius: BorderRadius.circular(12)),
//                       child: SingleChildScrollView(
//                         scrollDirection: Axis.horizontal,
//                         controller: _scrollControllerPlanets,
//                         child: DataTable(
//                           columnSpacing: 8,
//                           // horizontalMargin: 8,
//                           headingTextStyle: GoogleFonts.poppins(
//                             fontSize: 16,
//                             fontWeight: FontWeight.w500,
//                           ),
//                           dataTextStyle: GoogleFonts.poppins(
//                             fontSize: 14,
//                           ),
//                           headingRowColor: WidgetStatePropertyAll(
//                               AppColor.appColor.withOpacity(.4)),
//                           columns: planetsKP.first.keys.map((key) {
//                             return DataColumn(
//                               label: Flexible(
//                                 child: Container(
//                                   alignment: Alignment.center,
//                                   child:
//                                       Text(TFormatter.capitalizeSentence(key)),
//                                 ),
//                               ),
//                             );
//                           }).toList(),
//                           rows: planetsKP.map((row) {
//                             return DataRow(
//                               cells: row.keys.map((key) {
//                                 return DataCell(
//                                   Container(
//                                     alignment: Alignment.center,
//                                     child: Text(row[key]!,
//                                         textAlign: TextAlign.center),
//                                   ),
//                                 );
//                               }).toList(),
//                             );
//                           }).toList(),
//                         ),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 18),
//                   Text(
//                     widget.lang == "en" ? "Cusps" : "कस्प्स",
//                     style: GoogleFonts.poppins(
//                         fontSize: 18, fontWeight: FontWeight.w500),
//                   ),
//                   SizedBox(height: 12),
//                   Scrollbar(
//                     thickness: 8,
//                     thumbVisibility: true,
//                     controller:
//                         _scrollControllerCusps, // Provide the ScrollController here
//                     child: Container(
//                       decoration: BoxDecoration(
//                           border: Border.all(
//                               color: Colors.grey.withOpacity(.2), width: 2),
//                           borderRadius: BorderRadius.circular(12)),
//                       child: SingleChildScrollView(
//                         controller: _scrollControllerCusps,
//                         scrollDirection: Axis.horizontal,
//                         child: DataTable(
//                           columnSpacing: 8,
//                           horizontalMargin: 8,
//                           headingTextStyle: GoogleFonts.poppins(
//                             fontSize: 16,
//                             fontWeight: FontWeight.w500,
//                           ),
//                           dataTextStyle: GoogleFonts.poppins(
//                             fontSize: 14,
//                           ),
//                           headingRowColor: WidgetStatePropertyAll(
//                               AppColor.appColor.withOpacity(.4)),
//                           columns: cuspsKP.first.keys.map((key) {
//                             return DataColumn(
//                               label: Flexible(
//                                 child: Container(
//                                   alignment: Alignment.center,
//                                   child:
//                                       Text(TFormatter.capitalizeSentence(key)),
//                                 ),
//                               ),
//                             );
//                           }).toList(),
//                           rows: cuspsKP.map((row) {
//                             return DataRow(
//                               cells: row.keys.map((key) {
//                                 return DataCell(
//                                   Container(
//                                     alignment: Alignment.center,
//                                     child: Text(row[key]!,
//                                         textAlign: TextAlign.center),
//                                   ),
//                                 );
//                               }).toList(),
//                             );
//                           }).toList(),
//                         ),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 12),
//                 ],
//               ),
//             ),
//           );
//   }

//   @override
//   bool get wantKeepAlive => true;
// }
