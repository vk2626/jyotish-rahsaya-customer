import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../utils/score_gauge.dart';
import 'package:provider/provider.dart';
import '../../../Core/Provider/kundli_provider_new.dart';
import '../../../Core/formatter.dart';
import '../../../utils/custom_circular_progress_indicator.dart';
import '../../../Core/helper_functions.dart';
import '../../../Core/logger_helper.dart';
import 'dart:math' as math;
import '../../../kundali_testing_tabs/kundli_testing.dart';
import '../../../utils/AppColor.dart';

class MatchMakingResultScreen extends StatefulWidget {
  final String boyName;
  final String boyAvatar;
  final DateTime boyDOB;
  final TimeOfDay boyTOB;
  final String boyLat;
  final String boyLong;
  final String girlName;
  final String girlAvatar;
  final DateTime girlDOB;
  final TimeOfDay girlTOB;
  final String girlLat;
  final String girlLong;
  const MatchMakingResultScreen({
    super.key,
    required this.boyName,
    required this.boyAvatar,
    required this.boyDOB,
    required this.boyTOB,
    required this.boyLat,
    required this.boyLong,
    required this.girlName,
    required this.girlAvatar,
    required this.girlDOB,
    required this.girlTOB,
    required this.girlLat,
    required this.girlLong,
  });

  @override
  State<MatchMakingResultScreen> createState() =>
      _MatchMakingResultScreenState();
}

class _MatchMakingResultScreenState extends State<MatchMakingResultScreen> {
  Map<String, dynamic> matchMakingReport = {};
  bool isMatchMakingLoading = true;
  List<Color> colorList = [
    Colors.orange.shade200,
    Colors.red.shade200,
    Colors.purple.shade200,
    Colors.deepPurple.shade200,
    Colors.indigo.shade200,
    Colors.blue.shade200,
    Colors.lightBlue.shade200,
    Colors.lightGreen.shade200,
  ];
  getMatchMakingResult() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getMatchMakingResult(
            boyDOB: widget.boyDOB,
            boyTOB: widget.boyTOB,
            boyLat: widget.boyLat,
            boyLong: widget.boyLong,
            girlDOB: widget.girlDOB,
            girlTOB: widget.girlTOB,
            girlLat: widget.girlLat,
            girlLong: widget.girlLong)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          matchMakingReport = value;
          isMatchMakingLoading = false;
        });
      }
    });
  }

  List<Map<String, String>> detailsAstrologer = [
    {
      "name": "Compatibility",
      "hindi_name": "varna",
      "desc":
          "Varna refers to the mental compatibility of the two persons involved. It holds nominal effect in the matters of marriage compatibility."
    },
    {
      "name": "Love",
      "hindi_name": "bhakut",
      "desc":
          "Bhakut is related to the couple's joys and sorrows together and assessess the wealth and health after their wedding."
    },
    {
      "name": "Mental Compatibility",
      "hindi_name": "maitri",
      "desc":
          "Maitri assesess the mental compatibility and mutual love between the partners to be married."
    },
    {
      "name": "Health",
      "hindi_name": "nadi",
      "desc":
          "Nadi is related to the health compatibility of the couple. Matters of childbirth and progeny are also determined with this Guna."
    },
    {
      "name": "Dominance",
      "hindi_name": "vashya",
      "desc":
          "Vashya indicates the bride and the groom's tendency to dominate or influence each other in a marriage."
    },
    {
      "name": "Temperament",
      "hindi_name": "gan",
      "desc":
          "Gana is the indicator of the behaviour, character and temperament of the potential bride and groom towards each other."
    },
    {
      "name": "Destiny",
      "hindi_name": "tara",
      "desc":
          "Tara is the indicator of the birth star compatibility of the bride and the groom. It also indicates the fortune of the couple."
    },
    {
      "name": "Physical Compatibility",
      "hindi_name": "yoni",
      "desc":
          "Yoni is the indicator of the sexual or physical compatibility between the bride and the groom in question."
    },
  ];

  @override
  void initState() {
    super.initState();
    // Map<String, dynamic> jsonData = widget.toJson();
    // String jsonString = jsonEncode(jsonData);
    // TLoggerHelper.info('Match Making Parameters: $jsonString');
    getMatchMakingResult();
  }

  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        extendBody: true,
        body: isMatchMakingLoading
            ? Center(child: CustomCircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: mediaQuery.height * .45,
                      child: Stack(
                        children: [
                          Container(
                            height: mediaQuery.height * .4,
                            width: mediaQuery.width,
                            decoration: BoxDecoration(
                              color: Colors.black87,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(48),
                                  bottomRight: Radius.circular(48)),
                            ),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    BackButton(color: Colors.white),
                                    Text(
                                      "Kundli Matching",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white, fontSize: 16),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: mediaQuery.height * .03,
                                ),
                                Text(
                                  "Compatibility Score",
                                  style: GoogleFonts.poppins(
                                      color: Colors.white, fontSize: 28),
                                ),
                                SizedBox(
                                  height: 18,
                                ),
                                SizedBox(
                                  height: mediaQuery.height * .15,
                                  child: ClipRRect(
                                    child: Stack(
                                      fit: StackFit.expand,
                                      children: [
                                        Transform.flip(
                                          flipX: true,
                                          child: Image.asset(
                                            "asset/images/compatibility_chart.png",
                                            height: mediaQuery.height * .15,
                                            width: mediaQuery.width,
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Transform.rotate(
                                            angle: (matchMakingReport[
                                                                'ashtakoota']
                                                            ['total']
                                                        ['received_points'] /
                                                    36) *
                                                math.pi,
                                            // angle: (36 / 36) * math.pi,
                                            child: Container(
                                              width: mediaQuery.width * .35,
                                              height: 6,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                                // ScoreGauge(maxScore: 36, score: 3)
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 12),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.green),
                                  borderRadius: BorderRadius.circular(8)),
                              child: Text(
                                "${matchMakingReport['ashtakoota']['total']['received_points']} / 36",
                                style: GoogleFonts.poppins(
                                    fontSize: 28, fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: mediaQuery.height * .03,
                    ),
                    Text(
                      "Details",
                      style: GoogleFonts.poppins(
                          fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: Column(
                        children:
                            List.generate(detailsAstrologer.length, (index) {
                          final Map<String, dynamic> dataPointsMatchMaking =
                              matchMakingReport['ashtakoota'][
                                  detailsAstrologer[index]['hindi_name']
                                      .toString()
                                      .toLowerCase()];

                          return Container(
                              margin: EdgeInsets.only(bottom: 12),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 12),
                              decoration: BoxDecoration(
                                  color: colorList[index].withOpacity(.4),
                                  border:
                                      Border.all(color: Colors.grey.shade400),
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            "${detailsAstrologer[index].entries.first.value} ",
                                            style: GoogleFonts.poppins(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          Text(
                                            "(${TFormatter.capitalize(detailsAstrologer[index]['hindi_name'].toString() == "grahamaitri" ? "Maitri" : detailsAstrologer[index]['hindi_name'].toString())})",
                                            style: GoogleFonts.poppins(
                                                fontSize: 16,
                                                color: Colors.grey.shade500,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 6,
                                      ),
                                      SizedBox(
                                        width: mediaQuery.width * .55,
                                        child: Text(
                                          detailsAstrologer[index]['desc']
                                              .toString(),
                                          maxLines: 6,
                                          style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.grey.shade700),
                                        ),
                                      ),
                                    ],
                                  ),
                                  CircleAvatar(
                                    radius: 52,
                                    backgroundColor: colorList[index],
                                    child: CircleAvatar(
                                      radius: 46,
                                      backgroundColor: Colors.white,
                                      child: Text(
                                        dataPointsMatchMaking['received_points']
                                                .toString() +
                                            "/" +
                                            dataPointsMatchMaking[
                                                    'total_points']
                                                .toString(),
                                        style: GoogleFonts.poppins(
                                            fontSize: 24,
                                            color: colorList[index],
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                  )
                                ],
                              ));
                        }),
                      ),
                    ),
                    Text(
                      "Manglik Report",
                      style: GoogleFonts.poppins(
                          fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              CircleAvatar(
                                radius: 40,
                                backgroundColor: AppColor.appColor,
                                child: CircleAvatar(
                                  radius: 36,
                                  backgroundImage: CachedNetworkImageProvider(
                                      widget.boyAvatar),
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                TFormatter.capitalizeSentence(widget.boyName),
                                style: GoogleFonts.poppins(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              // Text(
                              //   "Non Manglik",
                              //   style: GoogleFonts.poppins(
                              //       fontSize: 16, color: Colors.green.shade600),
                              // ),
                              InkWell(
                                onTap: () async {
                                  bool isEnglish = await THelperFunctions
                                          .getLocalizationCode() ==
                                      'en';
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ListenableProvider(
                                            create: (context) =>
                                                KundliProviderNew(),
                                            child: KundliTesting(
                                                lat: widget.boyLat,
                                                long: widget.boyLong,
                                                isEnglish: isEnglish,
                                                dateTime: DateTime(
                                                    widget.boyDOB.year,
                                                    widget.boyDOB.month,
                                                    widget.boyDOB.day,
                                                    widget.boyTOB.hour,
                                                    widget.boyTOB.minute)),
                                          )));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(top: 12),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8),
                                  decoration: BoxDecoration(
                                      color: AppColor.appColor,
                                      borderRadius: BorderRadius.circular(18)),
                                  child: Text(
                                    "View Kundli",
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 18),
                                child: Image.asset(
                                  "asset/images/couple_ring.png",
                                  height: mediaQuery.height * .05,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              CircleAvatar(
                                radius: 40,
                                backgroundColor: AppColor.appColor,
                                child: CircleAvatar(
                                  radius: 36,
                                  backgroundImage: CachedNetworkImageProvider(
                                      widget.girlAvatar),
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                TFormatter.capitalizeSentence(widget.girlName),
                                style: GoogleFonts.poppins(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              // Text(
                              //   "Non Manglik",
                              //   style: GoogleFonts.poppins(
                              //       fontSize: 16, color: Colors.green.shade600),
                              // ),
                              InkWell(
                                onTap: () async {
                                  bool isEnglish = await THelperFunctions
                                          .getLocalizationCode() ==
                                      'en';
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ListenableProvider(
                                            create: (context) =>
                                                KundliProviderNew(),
                                            child: KundliTesting(
                                                lat: widget.girlLat,
                                                long: widget.girlLong,
                                                isEnglish: isEnglish,
                                                dateTime: DateTime(
                                                    widget.girlDOB.year,
                                                    widget.girlDOB.month,
                                                    widget.girlDOB.day,
                                                    widget.girlTOB.hour,
                                                    widget.girlTOB.minute)),
                                          )));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(top: 12),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8),
                                  decoration: BoxDecoration(
                                      color: AppColor.appColor,
                                      borderRadius: BorderRadius.circular(18)),
                                  child: Text(
                                    "View Kundli",
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: mediaQuery.height * .03,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 12),
                      padding:
                          EdgeInsets.symmetric(horizontal: 12, vertical: 18),
                      width: mediaQuery.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border:
                              Border.all(color: AppColor.appColor, width: 2),
                          gradient: LinearGradient(
                              colors: [AppColor.appColor, Colors.white],
                              begin: Alignment.topCenter,
                              end: Alignment.center)),
                      child: Column(
                        children: [
                          Text(
                            "Jyotish Rahsaya Conclusion",
                            style: GoogleFonts.poppins(
                                fontSize: 20, fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Text(
                            matchMakingReport['ashtakoota']['conclusion']
                                    ['report'] +
                                "\n" +
                                matchMakingReport['conclusion']['match_report'],
                            style: GoogleFonts.poppins(
                                fontSize: 16, color: Colors.grey.shade700),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 12),
                            padding: EdgeInsets.symmetric(
                                horizontal: 48, vertical: 8),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      spreadRadius: 1,
                                      offset: Offset(0, 1),
                                      blurRadius: 2)
                                ],
                                color: AppColor.whiteColor,
                                borderRadius: BorderRadius.circular(18)),
                            child: Text(
                              "Chat with Astrologer",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          SizedBox(height: 24),
                          Image.asset("asset/images/match_making.png",
                              height: mediaQuery.height * .2),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: mediaQuery.height * .03,
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
