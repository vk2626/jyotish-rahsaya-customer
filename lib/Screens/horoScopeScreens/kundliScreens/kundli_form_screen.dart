import 'package:dots_indicator/dots_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_google_places_hoc081098/flutter_google_places_hoc081098.dart';
import 'package:flutter_google_places_hoc081098/google_maps_webservice_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import '../../../Core/Provider/kundli_provider.dart';
import '../../../Core/formatter.dart';
import '../../../utils/AppColor.dart';
import '../../../utils/custom_appbar.dart';
import '../../../utils/snack_bar.dart';
import 'package:provider/provider.dart';
import 'package:scroll_date_picker/scroll_date_picker.dart';

import '../../ProfileScreen.dart';

class KundliFormScreen extends StatefulWidget {
  final String? kundliId;
  final String? userName;
  final String? gender;
  final DateTime? birthDate;
  final TimeOfDay? birthTime;
  final String? placeOfBirth;
  final double? lat;
  final double? lon;

  const KundliFormScreen(
      {super.key,
      this.kundliId,
      this.userName,
      this.gender,
      this.birthDate,
      this.birthTime,
      this.lat,
      this.lon,
      this.placeOfBirth});

  @override
  State<KundliFormScreen> createState() => _KundliFormScreenState();
}

class _KundliFormScreenState extends State<KundliFormScreen> {
  final PageController _pageController = PageController();
  int selectedIndex = 0;
  TextEditingController nameController = TextEditingController();
  String? gender;
  DateTime? birthDate;
  TimeOfDay? birthTime;
  TextEditingController placeOfBirthController = TextEditingController();
  bool isCheckBoxSelected = false;
  List<String> genderImages = [
    "asset/images/male.png",
    "asset/images/female.png",
    "asset/images/couple-gender.png",
  ];
  List<String> titleQuestionsList = [
    "Hey there!".tr() + "\n" + "What is your name?".tr(),
    "What is your gender?".tr(),
    "Enter your birth date".tr(),
    "Enter your birth time".tr(),
    "Where were you born?".tr(),
  ];

  @override
  void initState() {
    super.initState();
    if (widget.kundliId != null) {
      nameController.text = widget.userName!;
      gender = widget.gender;
      birthDate = widget.birthDate;
      birthTime = widget.birthTime;
      placeOfBirthController.text = widget.placeOfBirth!;
      _lat = widget.lat!;
      _lon = widget.lon!;
    }
  }

  double _lat = 0;
  double _lon = 0;
  Future<void> _getAddressFromLatLng(double lat, double lng,
      TextEditingController cityController, String description) async {
    await placemarkFromCoordinates(lat, lng).then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      print(place);
      setState(() {
        _lat = lat;
        _lon = lng;
        cityController
          ..text = place.locality! +
              ", " +
              place.administrativeArea! +
              ", " +
              place.country!;
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  List<String> genderNamesList = ["male".tr(), "female".tr(), "others".tr()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber.shade50,
      appBar: CustomAppBar(title: "Kundli".tr()),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DotsIndicator(
                dotsCount: 5,
                position: selectedIndex,
                decorator: DotsDecorator(activeSize: Size(18, 18)),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                titleQuestionsList[selectedIndex],
                style: GoogleFonts.poppins(
                    fontSize: 24, color: Colors.grey.shade700),
              ),
              SizedBox(
                height: 32,
              ),
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  children: [
                    TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Enter Name".tr(),
                          hintStyle: GoogleFonts.poppins(),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(18),
                              borderSide:
                                  BorderSide(color: Colors.grey.shade100))),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: List.generate(
                              3,
                              (index) => GestureDetector(
                                onTap: () {
                                  setState(() {
                                    gender = genderNamesList[index];
                                    selectedIndex++;
                                    _pageController.animateToPage(
                                      selectedIndex,
                                      duration: Duration(milliseconds: 400),
                                      curve: Curves.ease,
                                    );
                                  });
                                },
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(12),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                          border: Border.all(
                                              color: AppColor.appColor)),
                                      child: Image.asset(
                                        genderImages[index],
                                        height: 48,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      TFormatter.capitalize(
                                          genderNamesList[index]),
                                      style: GoogleFonts.poppins(fontSize: 14),
                                    )
                                  ],
                                ),
                              ),
                            )),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          height: 150,
                          child: ScrollDatePicker(
                            selectedDate: birthDate ?? DateTime.now(),
                            locale: Locale('en'),
                            options: DatePickerOptions(
                                backgroundColor: Colors.amber.shade50),
                            onDateTimeChanged: (DateTime value) {
                              setState(() {
                                birthDate = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        InkWell(
                          onTap: () async {
                            final selectedTime = await showTimePicker(
                              context: context,
                              initialTime: TimeOfDay(hour: 0, minute: 0),
                            );
                            if (selectedTime != null) {
                              setState(() {
                                birthTime = selectedTime;
                              });
                            }
                          },
                          child: TextField(
                            controller: TextEditingController(
                              text: birthTime != null
                                  ? TFormatter.formatTimeOfDay(birthTime!)
                                  : "",
                            ),
                            enabled: false,
                            style: GoogleFonts.poppins(color: Colors.black),
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                hintText: "Enter Time".tr(),
                                hintStyle: GoogleFonts.poppins(),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    borderSide: BorderSide(
                                        color: Colors.grey.shade100))),
                          ),
                        ),
                        Row(
                          children: [
                            Checkbox(
                              value: isCheckBoxSelected,
                              onChanged: (value) {
                                setState(() {
                                  isCheckBoxSelected = !isCheckBoxSelected;
                                  if (isCheckBoxSelected) {
                                    birthTime = TimeOfDay(hour: 0, minute: 0);
                                  }
                                });
                              },
                              activeColor: AppColor.appColor,
                              visualDensity: VisualDensity.standard,
                            ),
                            Text(
                              "Don't know my exact time of birth".tr(),
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        Text(
                          "Note: Without time of birth, we can still achieve upto 80% accurate predictions."
                              .tr(),
                          style: GoogleFonts.poppins(),
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () async {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        Prediction? p = await PlacesAutocomplete.show(
                          offset: 0,
                          radius: 1000,
                          types: [],
                          strictbounds: false,
                          region: "in",
                          onError: onError,
                          context: context,
                          apiKey: "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                          mode: Mode
                              .overlay, // Mode.fullscreen language: "en", onError: (value) { Utils.displayToast(context, value.errorMessage!); }, components: [Component(Component.country, "in")] );
                        );
                        displayPrediction(p!);
                        //cityController..text = p.description!;
                        final _places = GoogleMapsPlaces(
                          apiKey: "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                          apiHeaders:
                              await const GoogleApiHeaders().getHeaders(),
                        );

                        final detail =
                            await _places.getDetailsByPlaceId(p.placeId!);
                        final geometry = detail.result.geometry!;
                        final lat = geometry.location.lat;
                        final lng = geometry.location.lng;

                        _getAddressFromLatLng(
                            lat, lng, placeOfBirthController, p.description!);
                      },
                      child: TextField(
                        enabled: false,
                        controller: placeOfBirthController,
                        style: GoogleFonts.poppins(color: Colors.black),
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabled: false,
                          hintText: "New Delhi, Delhi, India",
                          hintStyle: GoogleFonts.poppins(),
                          suffixIcon: Icon(Icons.search),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(18),
                            borderSide: BorderSide(color: Colors.grey.shade100),
                          ),
                        ),
                      ),
                    ),
                  ],
                  onPageChanged: (value) {
                    setState(() {
                      selectedIndex = value;
                    });
                  },
                ),
              ),
              SizedBox(
                height: 24,
              ),
              if (selectedIndex != 1)
                InkWell(
                  onTap: () async {
                    if (selectedIndex < 4) {
                      switch (selectedIndex) {
                        case 0:
                          if (nameController.text.isNotEmpty) {
                            selectedIndex++;
                            _animateToNextPage();
                          } else {
                            showSnackBarWidget(
                                context, "Name must be filled".tr() + " !!");
                          }
                          break;
                        case 1:
                          if (gender != null) {
                            selectedIndex++;
                            _animateToNextPage();
                          } else {
                            showSnackBarWidget(context, "Select Gender".tr());
                          }
                          break;
                        case 2:
                          if (birthDate != null) {
                            selectedIndex++;
                            _animateToNextPage();
                          } else {
                            showSnackBarWidget(context,
                                "Birth Date must be selected".tr() + " !!");
                          }
                          break;
                        case 3:
                          if (birthTime != null) {
                            selectedIndex++;
                            _animateToNextPage();
                          } else {
                            showSnackBarWidget(context,
                                "Birth Time must be selected".tr() + " !!");
                          }
                          break;
                        default:
                          break;
                      }
                    } else {
                      if (nameController.text.isNotEmpty &&
                          gender != null &&
                          birthDate != null &&
                          birthTime != null &&
                          placeOfBirthController.text.isNotEmpty) {
                        try {
                          await Provider.of<KundliProvider>(context,
                                  listen: false)
                              .addKundliDetails(
                            id: widget.kundliId ?? "",
                            name: nameController.text,
                            gender: gender!,
                            dob: TFormatter.formatDateOnlyKundli(birthDate!),
                            dobTime: TFormatter.formatTimeOfDay(birthTime!),
                            pob: placeOfBirthController.text,
                            lat: _lat.toString(),
                            lon: _lon.toString(),
                          )
                              .then(
                            (value) {
                              if (value) {
                                showSnackBarWidget(context,
                                    "Kundli Added Successfully".tr() + " !!",
                                    isTrue: true);
                              } else {
                                showSnackBarWidget(
                                    context, "Some Error Occured".tr() + " !!");
                              }
                              Navigator.of(context).pop();
                            },
                          );
                          // After successful submission, you can navigate or show success message
                        } catch (e) {
                          // Handle errors if any
                          print("Error: $e");
                        }
                      }
                    }
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: AppColor.appColor,
                          borderRadius: BorderRadius.circular(18),
                          border: Border.all(color: Colors.grey.shade400)),
                      padding: EdgeInsets.symmetric(vertical: 16),
                      alignment: Alignment.center,
                      child: Text(
                        selectedIndex != 4 ? "Next".tr() : "Submit".tr(),
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      )),
                )
            ]),
      ),
    );
  }

  void _animateToNextPage() {
    _pageController.animateToPage(
      selectedIndex,
      duration: Duration(milliseconds: 400),
      curve: Curves.ease,
    );
  }
}
