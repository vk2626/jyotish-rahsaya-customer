// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places_hoc081098/flutter_google_places_hoc081098.dart';
import 'package:flutter_google_places_hoc081098/google_maps_webservice_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'package:jyotish_rahsaya/Core/logger_helper.dart';
import 'package:jyotish_rahsaya/dialog/showLoaderDialog.dart';
import '../../../utils/custom_circular_progress_indicator.dart';

import '../../../Core/Model/kundli_list_model.dart';
import '../../../Core/Provider/kundli_provider.dart';
import '../../../Core/Provider/kundli_provider_new.dart';
import '../../../Core/formatter.dart';
import '../../../Core/helper_functions.dart';
import '../../../kundali_testing_tabs/kundli_testing.dart';
import '../../../utils/AppColor.dart';
import '../../../utils/custom_appbar.dart';
import '../../ProfileScreen.dart';
import 'kundli_form_screen.dart';
import 'match_making_result_screen.dart';

class CreateNewKundliMatchingScreen extends StatefulWidget {
  const CreateNewKundliMatchingScreen({super.key});

  @override
  State<CreateNewKundliMatchingScreen> createState() =>
      _CreateNewKundliMatchingScreenState();
}

class _CreateNewKundliMatchingScreenState
    extends State<CreateNewKundliMatchingScreen> {
  int index = 0;
  TextEditingController boyNameController = TextEditingController();
  TextEditingController girlNameController = TextEditingController();
  TextEditingController boyBirthPlaceController = TextEditingController();
  TextEditingController girlBirthPlaceController = TextEditingController();
  DateTime? birthDateBoy;
  DateTime? birthDateGirl;
  TimeOfDay? birthTimeBoy;
  TimeOfDay? birthTimeGirl;
  double latBoy = 0;
  double lonBoy = 0;
  double latGirl = 0;
  double lonGirl = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "Kundli".tr()),
      resizeToAvoidBottomInset: false,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'asset/images/rectangle.png',
            fit: BoxFit.cover,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            child: Column(
              children: [
                Row(children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () => setState(() => index = 0),
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 12),
                        decoration: BoxDecoration(
                            color: index == 0
                                ? AppColor.appColor
                                : Colors.grey.shade100,
                            borderRadius: BorderRadius.circular(8)),
                        alignment: Alignment.center,
                        child: Text(
                          "Kundli".tr(),
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 12),
                  Expanded(
                    child: GestureDetector(
                      onTap: () => setState(() => index = 1),
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 12),
                        decoration: BoxDecoration(
                            color: index == 1
                                ? AppColor.appColor
                                : Colors.grey.shade100,
                            borderRadius: BorderRadius.circular(8)),
                        alignment: Alignment.center,
                        child: Text(
                          "Create New Kundli".tr(),
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ]),
                Expanded(
                    child: SingleChildScrollView(
                  child: IndexedStack(children: [
                    ListenableProvider(
                      create: (context) => KundliProvider(),
                      child: KundliSectionMatch(
                        getKundliInformationList: (p0) {
                          if (p0.gender.toString().toLowerCase() == "male") {
                            setState(() {
                              boyNameController.text =
                                  TFormatter.capitalizeSentence(p0.name);
                              boyBirthPlaceController.text =
                                  TFormatter.capitalize(p0.pob);
                              birthDateBoy = DateTime.parse(p0.dob);
                              birthTimeBoy =
                                  TFormatter.parseDobTime(p0.dobTime);
                              latBoy = double.parse(p0.lat);
                              lonBoy = double.parse(p0.lng);
                              index = 1;
                            });
                          } else if (p0.gender.toString().toLowerCase() ==
                              "female") {
                            setState(() {
                              girlNameController.text =
                                  TFormatter.capitalizeSentence(p0.name);
                              girlBirthPlaceController.text =
                                  TFormatter.capitalize(p0.pob);
                              birthDateGirl = DateTime.parse(p0.dob);
                              birthTimeGirl =
                                  TFormatter.parseDobTime(p0.dobTime);
                              latGirl = double.parse(p0.lat);
                              lonGirl = double.parse(p0.lng);
                              index = 1;
                            });
                          }
                        },
                      ),
                    ),
                    Column(
                      children: [
                        SizedBox(height: 18),
                        NewMatchingSectionKundli(
                          title: "Boy".tr(),
                          nameController: boyNameController,
                          birthPlaceController: boyBirthPlaceController,
                          selectedDate: birthDateBoy,
                          selectedTime: birthTimeBoy,
                          onBirthDateSelected: (date) =>
                              setState(() => birthDateBoy = date),
                          onBirthTimeSelected: (time) =>
                              setState(() => birthTimeBoy = time),
                          onLatLonSelected: (lat, lon) {
                            setState(() {
                              latBoy = lat;
                              lonBoy = lon;
                            });
                          },
                        ),
                        NewMatchingSectionKundli(
                          title: "Girl".tr(),
                          nameController: girlNameController,
                          birthPlaceController: girlBirthPlaceController,
                          selectedDate: birthDateGirl,
                          selectedTime: birthTimeGirl,
                          onBirthDateSelected: (date) =>
                              setState(() => birthDateGirl = date),
                          onBirthTimeSelected: (time) =>
                              setState(() => birthTimeGirl = time),
                          onLatLonSelected: (lat, lon) {
                            setState(() {
                              latGirl = lat;
                              lonGirl = lon;
                            });
                          },
                        ),
                      ],
                    )
                  ], index: index),
                ))
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: (index == 0)
          ? null
          : InkWell(
              onTap: () {
                // Logic for matching horoscope
                // Use the controllers and selected dates/times here
              },
              child: InkWell(
                onTap: () {
                  if (index == 0) {
                  } else {
                    // Navigator.of(context).push(MaterialPageRoute(
                    //   builder: (context) => ListenableProvider(
                    //     create: (context) => KundliProviderNew(),
                    //     child: MatchMakingResultScreen(
                    // boyName: "Rinku",
                    // boyAvatar:
                    //     "https://static.vecteezy.com/system/resources/thumbnails/029/364/941/small/3d-carton-of-boy-going-to-school-ai-photo.jpg",
                    //         boyDOB: DateTime(2002, 5, 20),
                    //         boyTOB: TimeOfDay(hour: 18, minute: 45),
                    //         boyLat: "19.0760",
                    //         boyLong: "72.8777",
                    // girlName: "Rinki",
                    // girlAvatar:
                    //     "https://t4.ftcdn.net/jpg/03/85/50/01/360_F_385500115_T8QiYsPeliQ5tE3npwOuJNUfunqFBo1U.jpg",
                    //         girlDOB: DateTime(2002, 5, 20),
                    //         girlTOB: TimeOfDay(hour: 18, minute: 45),
                    //         girlLat: "19.0760",
                    //         girlLong: "72.8777"),
                    //   ),
                    // ));
                    // Navigator.of(context).push(MaterialPageRoute(
                    //   builder: (context) => ListenableProvider(
                    //     create: (context) => KundliProviderNew(),
                    //     child: MatchMakingResultScreen(
                    //         boyName: "Rinku",
                    //         boyAvatar:
                    //             "https://static.vecteezy.com/system/resources/thumbnails/029/364/941/small/3d-carton-of-boy-going-to-school-ai-photo.jpg",
                    //         boyDOB: DateTime(2000, 1, 15),
                    //         boyTOB: TimeOfDay(hour: 10, minute: 30),
                    //         boyLat: "28.7041",
                    //         boyLong: "77.1025",
                    //         girlName: "Rinki",
                    //         girlAvatar:
                    //             "https://t4.ftcdn.net/jpg/03/85/50/01/360_F_385500115_T8QiYsPeliQ5tE3npwOuJNUfunqFBo1U.jpg",
                    //         girlDOB: DateTime(2002, 5, 20),
                    //         girlTOB: TimeOfDay(hour: 18, minute: 45),
                    //         girlLat: "19.0760",
                    //         girlLong: "72.8777"),
                    //   ),
                    // ));
    
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ListenableProvider(
                              create: (context) => KundliProviderNew(),
                              child: MatchMakingResultScreen(
                                  boyName: boyNameController.text,
                                  boyAvatar:
                                      "https://static.vecteezy.com/system/resources/thumbnails/029/364/941/small/3d-carton-of-boy-going-to-school-ai-photo.jpg",
                                  boyDOB: birthDateBoy!,
                                  boyTOB: birthTimeBoy!,
                                  boyLat: latBoy.toString(),
                                  boyLong: lonBoy.toString(),
                                  girlName: girlNameController.text,
                                  girlAvatar:
                                      "https://t4.ftcdn.net/jpg/03/85/50/01/360_F_385500115_T8QiYsPeliQ5tE3npwOuJNUfunqFBo1U.jpg",
                                  girlDOB: birthDateGirl!,
                                  girlTOB: birthTimeGirl!,
                                  girlLat: latGirl.toString(),
                                  girlLong: lonGirl.toString()),
                            )));
                  }
                },
                child: Container(
                  height: kBottomNavigationBarHeight,
                  color: AppColor.appColor,
                  child: Center(
                    child: Text(
                      index == 0
                          ? "Generate Horoscope".tr()
                          : "Match Horoscope".tr(),
                      style: GoogleFonts.poppins(
                          fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}

class NewMatchingSectionKundli extends StatefulWidget {
  final String title;
  final TextEditingController nameController;
  final TextEditingController birthPlaceController;
  final DateTime? selectedDate;
  final TimeOfDay? selectedTime;
  final ValueChanged<DateTime?> onBirthDateSelected;
  final ValueChanged<TimeOfDay?> onBirthTimeSelected;
  final Function(double, double) onLatLonSelected; // New callback for lat/lon

  const NewMatchingSectionKundli({
    super.key,
    required this.title,
    required this.nameController,
    required this.birthPlaceController,
    required this.selectedDate,
    required this.selectedTime,
    required this.onBirthDateSelected,
    required this.onBirthTimeSelected,
    required this.onLatLonSelected,
  });

  @override
  State<NewMatchingSectionKundli> createState() =>
      _NewMatchingSectionKundliState();
}

class _NewMatchingSectionKundliState extends State<NewMatchingSectionKundli> {
  Future<void> _getAddressFromLatLng(double lat, double lng,
      TextEditingController cityController, String description) async {
    await placemarkFromCoordinates(lat, lng).then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      print(place);
      setState(() {
        cityController
          ..text = place.locality! +
              ", " +
              place.administrativeArea! +
              ", " +
              place.country!;
        widget.onLatLonSelected(lat, lng); // Update lat/lon callback
        TLoggerHelper.info("lat $lat, long $lng");
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: widget.selectedDate ?? DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != widget.selectedDate) {
      widget.onBirthDateSelected(picked);
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: widget.selectedTime ?? TimeOfDay.now(),
    );
    if (picked != null && picked != widget.selectedTime) {
      widget.onBirthTimeSelected(picked);
    }
  }

  void onError(PlacesAutocompleteResponse response) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(response.errorMessage!)));
  }

  Future<void> displayPrediction(Prediction p) async {
    final _places = GoogleMapsPlaces(
      apiKey: "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
      apiHeaders: await const GoogleApiHeaders().getHeaders(),
    );

    final detail = await _places.getDetailsByPlaceId(p.placeId!);
    final geometry = detail.result.geometry!;
    final lat = geometry.location.lat;
    final lng = geometry.location.lng;
    _getAddressFromLatLng(
        lat, lng, widget.birthPlaceController, p.description!);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 18),
          decoration: BoxDecoration(
              color: Colors.grey.shade100,
              border: Border.all(color: AppColor.ltGrey, width: 1.5),
              borderRadius: BorderRadius.circular(8)),
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "${widget.title}'s Details",
                    style: GoogleFonts.poppins(
                        fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              KundliEditFieldWidget(
                title: "Name".tr(),
                hintText: "Enter Name".tr(),
                controller: widget.nameController,
                prefixIcon: Image.asset(
                  "asset/images/kundli_profile_icon.png",
                  height: 18,
                ),
              ),
              GestureDetector(
                onTap: () => _selectDate(context),
                child: AbsorbPointer(
                  child: KundliEditFieldWidget(
                    title: "Birth Date".tr(),
                    hintText: widget.selectedDate != null
                        ? "${widget.selectedDate!.day}-${widget.selectedDate!.month}-${widget.selectedDate!.year}"
                        : "Select Date".tr(),
                    prefixIcon: Image.asset(
                      "asset/images/kundli_calendar_icon.png",
                      height: 18,
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => _selectTime(context),
                child: AbsorbPointer(
                  child: KundliEditFieldWidget(
                    title: "Birth Time".tr(),
                    hintText: widget.selectedTime != null
                        ? "${widget.selectedTime!.format(context)}"
                        : "Select Time".tr(),
                    prefixIcon: Image.asset(
                      "asset/images/kundli_time_icon.png",
                      height: 18,
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  Prediction? p = await PlacesAutocomplete.show(
                    offset: 0,
                    radius: 1000,
                    types: [],
                    strictbounds: false,
                    region: "in",
                    onError: onError,
                    context: context,
                    apiKey: "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                    mode: Mode.overlay,
                    language: "en",
                    components: [Component(Component.country, "in")],
                  );
                  if (p != null) {
                    displayPrediction(p);
                  }
                },
                child: KundliEditFieldWidget(
                  isEnabled: false,
                  title: "Birth Place".tr(),
                  hintText: "Enter Birth Place".tr(),
                  controller: widget.birthPlaceController,
                  prefixIcon: Image.asset(
                    "asset/images/kundli_location_icon.png",
                    height: 18,
                  ),
                ),
              ),
              const SizedBox(height: 18)
            ],
          ),
        ),
        const SizedBox(
          height: 18,
        ),
      ],
    );
  }
}

class KundliSectionMatch extends StatefulWidget {
  Function(KundliInformationList) getKundliInformationList;
  KundliSectionMatch({
    Key? key,
    required this.getKundliInformationList,
  }) : super(key: key);

  @override
  State<KundliSectionMatch> createState() => _KundliSectionMatchState();
}

class _KundliSectionMatchState extends State<KundliSectionMatch>
    with AutomaticKeepAliveClientMixin {
  List<KundliInformationList> kundliInformationList = [];
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    getKundliListData();
  }

  getKundliListData() async {
    await Provider.of<KundliProvider>(context, listen: false)
        .getAllKundliesList()
        .then(
      (value) {
        if (value != null) {
          setState(() {
            kundliInformationList = value;
            isLoading = false;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return isLoading
        ? Center(child: CustomCircularProgressIndicator())
        : RefreshIndicator(
          color: Colors.white,
        backgroundColor: AppColor.appColor,
            onRefresh: () async {
              getKundliListData();
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: Column(
                children: List.generate(
                  kundliInformationList.length,
                  (index) => InkWell(
                    onTap: () {
                      setState(() {
                        widget.getKundliInformationList(
                            kundliInformationList[index]);
                      });
                    },
                    child: IntrinsicHeight(
                      child: Container(
                          margin: EdgeInsets.only(bottom: 12),
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey.withOpacity(.6)),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.shade200,
                                    blurRadius: 1,
                                    offset: Offset(0, 1),
                                    spreadRadius: 2)
                              ],
                              borderRadius: BorderRadius.circular(8)),
                          child: Row(
                            children: [
                              CircleAvatar(
                                radius: 28,
                                backgroundColor: Colors.green,
                                child: Text(
                                  kundliInformationList[index].name[0],
                                  style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 24,
                                      color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    kundliInformationList[index].name,
                                    style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    kundliInformationList[index].dob +
                                        ", " +
                                        kundliInformationList[index].dobTime,
                                    style:
                                        GoogleFonts.poppins(color: Colors.grey),
                                  ),
                                  Text(
                                    kundliInformationList[index].pob,
                                    maxLines: 2,
                                    style:
                                        GoogleFonts.poppins(color: Colors.grey),
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  @override
  bool get wantKeepAlive => true;
}

class KundliEditFieldWidget extends StatelessWidget {
  final String title;
  final String hintText;
  final TextEditingController? controller;
  final Widget? prefixIcon;
  final bool isEnabled;

  const KundliEditFieldWidget(
      {super.key,
      required this.title,
      required this.hintText,
      this.controller,
      this.prefixIcon,
      this.isEnabled = true});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 16),
          ),
          SizedBox(
            height: 36,
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                prefixIcon: prefixIcon != null
                    ? Padding(
                        padding: const EdgeInsets.only(right: 8),
                        child: prefixIcon,
                      )
                    : null,
                enabled: isEnabled,
                prefixIconConstraints: BoxConstraints.tight(const Size(28, 28)),
                contentPadding: const EdgeInsets.only(left: 12),
                border: const UnderlineInputBorder(),
                hintText: hintText,
                hintStyle: GoogleFonts.poppins(fontSize: 16),
                labelStyle: GoogleFonts.poppins(fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
