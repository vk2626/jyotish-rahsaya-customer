import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import '../../../Core/Provider/kundli_provider_new.dart';
import '../../../Core/helper_functions.dart';
import '../../../dialog/showLoaderDialog.dart';
import '../../../Core/Provider/kundli_provider.dart';
import '../../../utils/custom_circular_progress_indicator.dart';
import '../../../Core/formatter.dart';
import '../../../kundali_testing_tabs/kundli_testing.dart';
import 'kundli_form_screen.dart';
import 'view_kundli_screen.dart';
import '../../../utils/AppColor.dart';
import '../../../utils/custom_appbar.dart';
import 'package:provider/provider.dart';

import '../../../Core/Model/kundli_list_model.dart';

class FreeKundliScreen extends StatefulWidget {
  const FreeKundliScreen({super.key});

  @override
  State<FreeKundliScreen> createState() => _FreeKundliScreenState();
}

class _FreeKundliScreenState extends State<FreeKundliScreen> {
  List<KundliInformationList> kundliInformationList = [];
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    getKundliListData();
  }

  getKundliListData() async {
    await Provider.of<KundliProvider>(context, listen: false)
        .getAllKundliesList()
        .then(
      (value) {
        if (value != null) {
          setState(() {
            kundliInformationList = value;
            isLoading = false;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "Kundli".tr()),
      backgroundColor: Colors.white,
      body: Container(
        padding: const EdgeInsets.only(top: 12, right: 18, left: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32)),
                  prefixIcon: Icon(Icons.search),
                  hintText: "Search kundli by name".tr(),
                  hintStyle: GoogleFonts.poppins()),
            ),
            SizedBox(height: 12),
            isLoading
                ? Center(child: CustomCircularProgressIndicator())
                : Expanded(
                    child: RefreshIndicator(
                      color: Colors.white,
        backgroundColor: AppColor.appColor,
                      onRefresh: () async {
                        getKundliListData();
                      },
                      child: SingleChildScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        child: Column(
                          children: List.generate(
                            kundliInformationList.length,
                            (index) => InkWell(
                              onLongPress: () {
                                showDialog(
                                    context: context,
                                    builder: (_) => AlertDialog(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(6)),
                                          titlePadding:
                                              const EdgeInsets.symmetric(
                                                  vertical: 12, horizontal: 12),
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 18, vertical: 24),
                                          title: Text(
                                              "Are you sure wanna delete this kundli?"
                                                  .tr(),
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: AppColor.blackColor,
                                                  fontWeight: FontWeight.bold)),
                                          actions: [
                                            ElevatedButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      WidgetStateProperty.all(
                                                          AppColor.whiteColor),
                                                  shape: WidgetStateProperty.all(
                                                      RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)))),
                                              child: Text('Cancel'.tr(),
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          AppColor.blackColor)),
                                            ),
                                            ElevatedButton(
                                              onPressed: () async {
                                                await Provider.of<
                                                            KundliProvider>(
                                                        context,
                                                        listen: false)
                                                    .deleteUserKundli(
                                                        kundliId:
                                                            kundliInformationList[
                                                                    index]
                                                                .id
                                                                .toString())
                                                    .then(
                                                  (value) {
                                                    if (value) {
                                                      setState(() {
                                                        kundliInformationList
                                                            .removeAt(index);
                                                      });
                                                      Navigator.of(context)
                                                          .pop();
                                                    }
                                                  },
                                                );
                                              },
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      WidgetStateProperty.all(
                                                          AppColor.appColor),
                                                  shape: WidgetStateProperty.all(
                                                      RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)))),
                                              child: Text('Yes',
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          AppColor.blackColor)),
                                            )
                                          ],
                                        ));
                              },
                              onTap: () async {
                                bool isEnglish = await THelperFunctions
                                        .getLocalizationCode() ==
                                    'en';
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ListenableProvider(
                                          create: (context) =>
                                              KundliProviderNew(),
                                          child: KundliTesting(
                                              lat: kundliInformationList[index]
                                                  .lat,
                                              long: kundliInformationList[index]
                                                  .lng,
                                              isEnglish: isEnglish,
                                              dateTime: THelperFunctions
                                                  .parseDobAndTime(
                                                      kundliInformationList[
                                                              index]
                                                          .dob,
                                                      kundliInformationList[
                                                              index]
                                                          .dobTime)),
                                        )));
                              },
                              child: IntrinsicHeight(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 12),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 12),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.grey.withOpacity(.6)),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey.shade200,
                                              blurRadius: 1,
                                              offset: Offset(0, 1),
                                              spreadRadius: 2)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 28,
                                          backgroundColor: Colors.green,
                                          child: Text(
                                            kundliInformationList[index]
                                                .name[0],
                                            style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 24,
                                                color: Colors.white),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 12,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              kundliInformationList[index].name,
                                              style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                            ),
                                            Text(
                                              kundliInformationList[index].dob +
                                                  ", " +
                                                  kundliInformationList[index]
                                                      .dobTime,
                                              style: GoogleFonts.poppins(
                                                  color: Colors.grey),
                                            ),
                                            Text(
                                              kundliInformationList[index].pob,
                                              maxLines: 2,
                                              style: GoogleFonts.poppins(
                                                  color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                Navigator.of(context)
                                                    .push(MaterialPageRoute(
                                                        builder: (context) =>
                                                            ListenableProvider(
                                                                create: (context) =>
                                                                    KundliProviderNew(),
                                                                child:
                                                                    KundliFormScreen(
                                                                  kundliId: kundliInformationList[
                                                                          index]
                                                                      .id
                                                                      .toString(),
                                                                  userName:
                                                                      kundliInformationList[
                                                                              index]
                                                                          .name,
                                                                  gender: kundliInformationList[
                                                                          index]
                                                                      .gender,
                                                                  placeOfBirth:
                                                                      kundliInformationList[
                                                                              index]
                                                                          .pob,
                                                                  birthDate: DateFormat(
                                                                          "yyyy-MM-dd")
                                                                      .parse(kundliInformationList[
                                                                              index]
                                                                          .dob),
                                                                  birthTime:
                                                                      TimeOfDay(
                                                                    hour: DateFormat(
                                                                            "hh:mm a")
                                                                        .parse(kundliInformationList[index]
                                                                            .dobTime
                                                                            .toLowerCase())
                                                                        .hour,
                                                                    minute: DateFormat(
                                                                            "hh:mm a")
                                                                        .parse(kundliInformationList[index]
                                                                            .dobTime
                                                                            .toLowerCase())
                                                                        .minute,
                                                                  ),
                                                                  lat: double.parse(
                                                                      kundliInformationList[
                                                                              index]
                                                                          .lat),
                                                                  lon: double.parse(
                                                                      kundliInformationList[
                                                                              index]
                                                                          .lng),
                                                                ))))
                                                    .then((value) {
                                                  setState(() {});
                                                });
                                              },
                                              child: CircleAvatar(
                                                radius: 12,
                                                backgroundColor:
                                                    Colors.grey.shade300,
                                                child:
                                                    Icon(Icons.edit, size: 14),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    )),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
          ],
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
                  builder: (context) => ListenableProvider(
                      create: (context) => KundliProvider(),
                      child: KundliFormScreen())))
              .then((value) {
            setState(() {});
          });
        },
        child: Container(
            margin: EdgeInsets.all(16),
            height: kBottomNavigationBarHeight - 8,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: AppColor.appColor,
                borderRadius: BorderRadius.circular(24)),
            child: Text(
              "Create New Kundli".tr(),
              style: GoogleFonts.poppins(
                  fontSize: 15, fontWeight: FontWeight.w500),
            )),
      ),
    );
  }
}
