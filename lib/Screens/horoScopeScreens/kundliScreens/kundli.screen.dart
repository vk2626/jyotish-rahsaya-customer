// import 'package:flutter/material.dart';
// import 'chart.screen.dart';
// import '../../../utils/AppColor.dart';

// import '../../../Core/langConstant/language.constant.dart';

// class KundliScreen extends StatefulWidget {
//   const KundliScreen({Key? key});

//   @override
//   State<KundliScreen> createState() => _KundliScreenState();
// }

// class _KundliScreenState extends State<KundliScreen>
//     with TickerProviderStateMixin {
//   late TabController _tabController;
//   late TabController _chartTabController;

//   @override
//   void initState() {
//     super.initState();
//     _tabController = TabController(length: 5, vsync: this);
//   }

//   List<String> _chipList = [
//     "Sav",
//     "Asc",
//     "Jupiter",
//     "Mars",
//     "Mercury",
//     "Moon",
//     "Saturn",
//     "Sun",
//     "Venus",
//   ];
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("kundli".tr()),
//       ),
//       body: LayoutBuilder(builder: (context, BoxConstraints constraints) {
//         return Column(
//           children: <Widget>[
//             SizedBox(height: 10),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: Container(
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(10),
//                     border: Border.all(color: AppColor.blackColor)),
//                 child: TabBar(
//                   // isScrollable: true,
//                   indicatorSize: TabBarIndicatorSize.tab,
//                   // dividerColor: AppColor.borderColor,
//                   indicator: BoxDecoration(
//                       border: Border.all(color: AppColor.blackColor),
//                       borderRadius: BorderRadius.circular(10),
//                       color: AppColor.appColor),
//                   controller: _tabController,
//                   tabs: [
//                     Tab(text: 'Basic'),
//                     Tab(text: 'Chart'),
//                     Tab(text: 'KP'),
//                     Tab(text: 'Ashtakvarga'),
//                     Tab(text: 'Dasha'),
//                   ],
//                 ),
//               ),
//             ),
//             Expanded(
//               child: TabBarView(
//                 controller: _tabController,
//                 children: <Widget>[
//                   /*********** Widgets for Basic tab **************/
//                   SingleChildScrollView(
//                     child: Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Container(
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text('Basic Details',
//                                 style: TextStyle(fontSize: 23)),
//                             SizedBox(height: 10),
//                             Container(
//                               width: double.infinity,
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border:
//                                       Border.all(color: AppColor.borderColor)),
//                               child: ListView.builder(
//                                   physics: NeverScrollableScrollPhysics(),
//                                   itemCount: 10,
//                                   shrinkWrap: true,
//                                   itemBuilder: (context, index) {
//                                     return Container(
//                                       padding: EdgeInsets.all(8.0),
//                                       color: index.isEven
//                                           ? AppColor.appColor.withOpacity(.5)
//                                           : AppColor.whiteColor,
//                                       width: double.infinity,
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           Container(
//                                             child: Text(
//                                               "Name",
//                                               style: TextStyle(fontSize: 20),
//                                             ),
//                                           ),
//                                           Text("User Name",
//                                               overflow: TextOverflow.ellipsis,
//                                               maxLines: 3,
//                                               style: TextStyle(fontSize: 20)),
//                                         ],
//                                       ),
//                                     );
//                                   }),
//                             ),
//                             SizedBox(height: 20),
//                             Text('Manglik Analysis',
//                                 style: TextStyle(fontSize: 23)),
//                             SizedBox(width: 10),
//                             Container(
//                               height: 100,
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border: Border.all(color: Colors.green)),
//                               child: Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: Row(
//                                   children: <Widget>[
//                                     CircleAvatar(
//                                       radius: 35,
//                                       child: Text(
//                                         "No",
//                                         style: TextStyle(color: Colors.green),
//                                       ),
//                                     ),
//                                     SizedBox(width: 10),
//                                     Expanded(
//                                       child: Container(
//                                         width: double.infinity,
//                                         child: Column(
//                                           crossAxisAlignment:
//                                               CrossAxisAlignment.start,
//                                           children: <Widget>[
//                                             Text(
//                                               "User Name",
//                                               style: TextStyle(
//                                                   fontWeight: FontWeight.bold,
//                                                   fontSize: 19),
//                                             ),
//                                             Text(
//                                                 "If Android Studio doesn't recognize your device: try unplugging the USB cable and plug it back in or restarting Android Studio.",
//                                                 maxLines: 3,
//                                                 overflow:
//                                                     TextOverflow.ellipsis),
//                                           ],
//                                         ),
//                                       ),
//                                     )
//                                   ],
//                                 ),
//                               ),
//                             ),
//                             SizedBox(height: 20),
//                             Text('Panchang Details',
//                                 style: TextStyle(fontSize: 23)),
//                             SizedBox(height: 20),
//                             Container(
//                               width: double.infinity,
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border:
//                                       Border.all(color: AppColor.borderColor)),
//                               child: ListView.builder(
//                                   physics: NeverScrollableScrollPhysics(),
//                                   itemCount: 10,
//                                   shrinkWrap: true,
//                                   itemBuilder: (context, index) {
//                                     return Container(
//                                       padding: EdgeInsets.all(8.0),
//                                       color: index.isEven
//                                           ? AppColor.appColor.withOpacity(.5)
//                                           : AppColor.whiteColor,
//                                       width: double.infinity,
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           Container(
//                                             child: Text(
//                                               "Name",
//                                               style: TextStyle(fontSize: 20),
//                                             ),
//                                           ),
//                                           Text("User Name",
//                                               overflow: TextOverflow.ellipsis,
//                                               maxLines: 3,
//                                               style: TextStyle(fontSize: 20)),
//                                         ],
//                                       ),
//                                     );
//                                   }),
//                             ),
//                             SizedBox(height: 20),
//                             Text('Avakhada Details',
//                                 style: TextStyle(fontSize: 23)),
//                             SizedBox(height: 20),
//                             Container(
//                               width: double.infinity,
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(10),
//                                   border:
//                                       Border.all(color: AppColor.borderColor)),
//                               child: ListView.builder(
//                                   physics: NeverScrollableScrollPhysics(),
//                                   itemCount: 10,
//                                   shrinkWrap: true,
//                                   itemBuilder: (context, index) {
//                                     return Container(
//                                       padding: EdgeInsets.all(8.0),
//                                       color: index.isEven
//                                           ? AppColor.appColor.withOpacity(.5)
//                                           : AppColor.whiteColor,
//                                       width: double.infinity,
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           Container(
//                                             child: Text(
//                                               "Name",
//                                               style: TextStyle(fontSize: 20),
//                                             ),
//                                           ),
//                                           Text("User Name",
//                                               overflow: TextOverflow.ellipsis,
//                                               maxLines: 3,
//                                               style: TextStyle(fontSize: 20)),
//                                         ],
//                                       ),
//                                     );
//                                   }),
//                             ),
//                             SizedBox(height: 10),
//                             Container(
//                               color: Colors.white,
//                               child: Column(
//                                 children: <Widget>[
//                                   SizedBox(height: 10),
//                                   Padding(
//                                     padding:
//                                         EdgeInsets.symmetric(horizontal: 20),
//                                     child: Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: <Widget>[
//                                         RichText(
//                                           maxLines: 2,
//                                           overflow: TextOverflow.ellipsis,
//                                           text: TextSpan(
//                                               text:
//                                                   "Unable to understand your kundli\n",
//                                               style: TextStyle(
//                                                   fontSize: 14,
//                                                   fontWeight: FontWeight.bold,
//                                                   color: AppColor.darkGrey),
//                                               children: <TextSpan>[
//                                                 TextSpan(
//                                                     text:
//                                                         "Connect with astrologers on live",
//                                                     style: TextStyle(
//                                                         fontSize: 11,
//                                                         fontWeight:
//                                                             FontWeight.normal,
//                                                         color:
//                                                             AppColor.darkGrey)),
//                                               ]),
//                                         ),
//                                         InkWell(
//                                           onTap: () {
//                                             // widget.onChangedItem(99);
//                                             /*Navigator.of(context).push(
//                                                 SlideRightRoute(
//                                                     page: LiveAstrologers()));*/
//                                           },
//                                           child: Text(
//                                             "view_all,
//                                             style: TextStyle(
//                                                 fontSize: 14,
//                                                 color: AppColor.darkGrey,
//                                                 fontWeight: FontWeight.w500),
//                                           ),
//                                         )
//                                       ],
//                                     ),
//                                   ),
//                                   SizedBox(height: 10),
//                                   Container(
//                                     width: double.infinity,
//                                     height: 160,
//                                     child: ListView.builder(
//                                       scrollDirection: Axis.horizontal,
//                                       shrinkWrap: true,
//                                       itemCount: 10,
//                                       itemBuilder:
//                                           (BuildContext context, int i) {
//                                         return Container(
//                                           margin: EdgeInsets.only(left: 10),
//                                           height: 160,
//                                           width: 120,
//                                           child: Card(
//                                             elevation: 2,
//                                             clipBehavior:
//                                                 Clip.antiAliasWithSaveLayer,
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius: BorderRadius.all(
//                                                     Radius.circular(10.0))),
//                                             child: Stack(
//                                               children: [
//                                                 Positioned(
//                                                   child: Image.network(
//                                                     fit: BoxFit.cover,
//                                                     'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=1760&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
//                                                   ),
//                                                   right: 0,
//                                                   left: 0,
//                                                   top: 0,
//                                                   bottom: 0,
//                                                 ),
//                                                 Positioned(
//                                                   child: Text(
//                                                     "Astrologer Name",
//                                                     style: TextStyle(
//                                                         color: Colors.white),
//                                                     textAlign: TextAlign.center,
//                                                   ),
//                                                   right: 5,
//                                                   left: 5,
//                                                   bottom: 5,
//                                                 ),
//                                                 Positioned(
//                                                   child: Column(
//                                                     children: <Widget>[
//                                                       Visibility(
//                                                         child: IconButton(
//                                                           visualDensity:
//                                                               VisualDensity(
//                                                                   horizontal:
//                                                                       -4.0,
//                                                                   vertical:
//                                                                       -4.0),
//                                                           padding:
//                                                               EdgeInsets.zero,
//                                                           icon: Icon(
//                                                             Icons.videocam,
//                                                             color: Colors.white,
//                                                           ),
//                                                           onPressed: () {},
//                                                         ),
//                                                         visible: false,
//                                                       ),
//                                                       Visibility(
//                                                         child: IconButton(
//                                                           visualDensity:
//                                                               VisualDensity(
//                                                                   horizontal:
//                                                                       -4.0,
//                                                                   vertical:
//                                                                       -4.0),
//                                                           padding:
//                                                               EdgeInsets.zero,
//                                                           icon: Icon(
//                                                             Icons.call,
//                                                             color: Colors.white,
//                                                           ),
//                                                           onPressed: () {},
//                                                         ),
//                                                         visible: true,
//                                                       )
//                                                     ],
//                                                   ),
//                                                 )
//                                               ],
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),
//                                   ),
//                                   SizedBox(height: 10),
//                                 ],
//                               ),
//                             ),
//                             SizedBox(height: 20),
//                             ElevatedButton(
//                                 style: ButtonStyle(
//                                     shape: MaterialStateProperty.all<
//                                         RoundedRectangleBorder>(
//                                       RoundedRectangleBorder(
//                                         borderRadius:
//                                             BorderRadius.circular(15.0),
//                                       ),
//                                     ),
//                                     minimumSize: MaterialStateProperty.all(
//                                         const Size(150, 50)),
//                                     backgroundColor: MaterialStateProperty.all(
//                                         AppColor.appColor),
//                                     // elevation: MaterialStateProperty.all(3),
//                                     shadowColor: MaterialStateProperty.all(
//                                         Colors.transparent)),
//                                 onPressed: () {
//                                   showDialog(
//                                     context: context,
//                                     builder: (BuildContext context) =>
//                                         _feedbackDialog(context),
//                                   );
//                                 },
//                                 child: Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Text("Share Feedback",
//                                         style: TextStyle(
//                                             fontSize: 18,
//                                             color: AppColor.blackColor)),
//                                     SizedBox(width: 10),
//                                     Container(
//                                       height: 30,
//                                       width: 30,
//                                       decoration: BoxDecoration(
//                                           color: Colors.black,
//                                           borderRadius:
//                                               BorderRadius.circular(50)),
//                                       child: Icon(
//                                         Icons.arrow_forward_ios_rounded,
//                                         color: Colors.white,
//                                       ),
//                                     )
//                                   ],
//                                 )),
//                             SizedBox(height: 30),
//                           ],
//                         ),
//                       ),
//                     ),
//                   ),
//                   /*********** Widgets for Chart tab **************/
//                   Container(
//                     child: Center(
//                       child: Column(
//                         children: <Widget>[KundliChartScreen()],
//                       ),
//                     ),
//                   ),
//                   /*********** Widgets for KP tab **************/
//                   Container(
//                     child: SingleChildScrollView(
//                       child: Padding(
//                         padding: const EdgeInsets.all(8.0),
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text(
//                               'Bhav Chalit Chat',
//                               style: TextStyle(fontSize: 21),
//                             ),
//                             Divider(),
//                             Text(
//                               'Ruling Planets',
//                               style: TextStyle(fontSize: 21),
//                             ),
//                             Divider(),
//                             Text(
//                               'Planets',
//                               style: TextStyle(fontSize: 21),
//                             ),
//                             Divider(),
//                             Text(
//                               'Cusps',
//                               style: TextStyle(fontSize: 21),
//                             ),
//                             SizedBox(height: 20),
//                             Container(
//                                 decoration: BoxDecoration(
//                                     border:
//                                         Border.all(color: AppColor.blackColor),
//                                     borderRadius: BorderRadius.circular(5)),
//                                 child: ListView.separated(
//                                   separatorBuilder: (context, index) {
//                                     return Divider();
//                                   },
//                                   physics: NeverScrollableScrollPhysics(),
//                                   shrinkWrap: true,
//                                   itemCount: 13,
//                                   itemBuilder: (context, index) {
//                                     if (index == 0) {
//                                       return Container(
//                                         height: 40,
//                                         width: double.infinity,
//                                         color:
//                                             AppColor.appColor.withOpacity(.5),
//                                         child: Padding(
//                                           padding: const EdgeInsets.all(8.0),
//                                           child: Row(
//                                             mainAxisAlignment:
//                                                 MainAxisAlignment.spaceBetween,
//                                             crossAxisAlignment:
//                                                 CrossAxisAlignment.center,
//                                             children: <Widget>[
//                                               Text("Title",
//                                                   style:
//                                                       TextStyle(fontSize: 20)),
//                                               Text("Title",
//                                                   style:
//                                                       TextStyle(fontSize: 20)),
//                                               Text("Title",
//                                                   style:
//                                                       TextStyle(fontSize: 20)),
//                                               Text("Title",
//                                                   style:
//                                                       TextStyle(fontSize: 20)),
//                                               Text("Title",
//                                                   style:
//                                                       TextStyle(fontSize: 20)),
//                                             ],
//                                           ),
//                                         ),
//                                       );
//                                     } else {
//                                       return Column(
//                                         children: <Widget>[
//                                           Row(
//                                             children: <Widget>[
//                                               Container(
//                                                 height: 30,
//                                                 width: 58,
//                                                 child: Center(
//                                                   child: Text(
//                                                     "Ascendant",
//                                                     style:
//                                                         TextStyle(fontSize: 11),
//                                                   ),
//                                                 ),
//                                               ),
//                                               Expanded(
//                                                 child: VerticalDivider(
//                                                   thickness: 1.0,
//                                                 ),
//                                               ),
//                                               Container(
//                                                 height: 30,
//                                                 width: 58,
//                                                 child: Center(
//                                                   child: Text(
//                                                     "Sagittarius",
//                                                     style:
//                                                         TextStyle(fontSize: 12),
//                                                   ),
//                                                 ),
//                                               ),
//                                               VerticalDivider(
//                                                 thickness: 7.0,
//                                                 color: Colors.black,
//                                               ),
//                                               Container(
//                                                 height: 30,
//                                                 width: 58,
//                                                 child: Center(
//                                                   child: Text(
//                                                     "$index+11111",
//                                                     style:
//                                                         TextStyle(fontSize: 11),
//                                                   ),
//                                                 ),
//                                               ),
//                                               VerticalDivider(
//                                                 thickness: 7.0,
//                                                 color: Colors.black,
//                                               ),
//                                               Container(
//                                                 height: 30,
//                                                 width: 58,
//                                                 child: Center(
//                                                   child: Text(
//                                                     "$index+11111",
//                                                     style:
//                                                         TextStyle(fontSize: 11),
//                                                   ),
//                                                 ),
//                                               ),
//                                               VerticalDivider(
//                                                 thickness: 7.0,
//                                                 color: Colors.black,
//                                               ),
//                                               Container(
//                                                 height: 30,
//                                                 width: 58,
//                                                 child: Center(
//                                                   child: Text(
//                                                     "$index+11111",
//                                                     style:
//                                                         TextStyle(fontSize: 11),
//                                                   ),
//                                                 ),
//                                               ),
//                                             ],
//                                           ),
//                                         ],
//                                       );
//                                     }
//                                   },
//                                 )),
//                             Divider(),
//                           ],
//                         ),
//                       ),
//                     ),
//                   ),
//                   /*********** Widgets for Ashtakvarga tab **************/
//                   Container(
//                     child: Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: <Widget>[
//                           Text(
//                             'Ashtakvarga Chart',
//                             style: TextStyle(fontSize: 21),
//                           ),
//                           Container(
//                             width: double.infinity,
//                             child: Text(
//                               "Pages that you view in this window won't appear in the browser history and they won't leave other traces, like cookies, on the computer after you close all open Guest windows. Any files that you download will be preserved, however.",
//                               style: TextStyle(),
//                             ),
//                           ),
//                           Container(
//                             margin: EdgeInsets.only(top: 10, bottom: 10),
//                             width: double.infinity,
//                             height: 40,
//                             child: ListView.builder(
//                               scrollDirection: Axis.horizontal,
//                               shrinkWrap: true,
//                               itemCount: _chipList.length,
//                               itemBuilder: (BuildContext context, int i) {
//                                 return Container(
//                                   margin: EdgeInsets.only(left: 10),
//                                   height: 40,
//                                   child: Card(
//                                     color: Colors.white,
//                                     elevation: 2,
//                                     clipBehavior: Clip.antiAliasWithSaveLayer,
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius: BorderRadius.all(
//                                             Radius.circular(25.0)),
//                                         side: BorderSide(
//                                             color: AppColor.appColor)),
//                                     child: GestureDetector(
//                                       child: Row(
//                                         children: <Widget>[
//                                           SizedBox(width: 10),
//                                           Text(
//                                             "${_chipList[i]}",
//                                             style:
//                                                 TextStyle(color: Colors.black),
//                                             textAlign: TextAlign.center,
//                                           ),
//                                           SizedBox(width: 10),
//                                         ],
//                                       ),
//                                       onTap: () {
//                                         // setState(() {
//                                         //   selectedPos = i;
//                                         //   astroID = resAstroCategoryList[i].id!;
//                                         //
//                                         //   print("Sele ID ---> $astroID");
//                                         // });
//                                       },
//                                     ),
//                                   ),
//                                 );
//                               },
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                   /*********** Widgets for Dasha tab **************/
//                   Container(
//                     child: Center(
//                       child: Text('Dasha Tab Content'),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         );
//       }),
//     );
//   }

//   Widget _feedbackDialog(BuildContext context) {
//     List<String> feedbackOptions = ["Great", "Average", "Needs improvement"];
//     TextEditingController _feedBckController = TextEditingController();
//     final GlobalKey formKey = GlobalKey();
//     int? selectedOptionIndex;
//     return StatefulBuilder(builder: (context, StateSetter setState) {
//       return Form(
//         key: formKey,
//         child: AlertDialog(
//             backgroundColor: AppColor.whiteColor,
//             title: const Text(
//                 'How was your overall experience of Daily horoscope?'),
//             content: Container(
//                 height: selectedOptionIndex != null ? 400 : 200,
//                 width: 500,
//                 child: Column(
//                   children: <Widget>[
//                     Expanded(
//                       child: ListView.builder(
//                           itemCount: feedbackOptions.length,
//                           itemBuilder: (context, index) {
//                             return CheckboxListTile(
//                               controlAffinity: ListTileControlAffinity.leading,
//                               title: Text(feedbackOptions[index]),
//                               value: selectedOptionIndex == index,
//                               onChanged: (value) {
//                                 setState(() {
//                                   selectedOptionIndex = value! ? index : null;
//                                 });
//                               },
//                             );
//                           }),
//                     ),
//                     Divider(thickness: 1),
//                     Visibility(
//                       visible: selectedOptionIndex != null,
//                       child: Column(
//                         children: <Widget>[
//                           Align(
//                               alignment: Alignment.centerLeft,
//                               child: Text("Share your feedback",
//                                   style: TextStyle(
//                                       fontSize: 20,
//                                       fontWeight: FontWeight.bold))),
//                           SizedBox(height: 10),
//                           TextFormField(
//                             cursorColor: AppColor.borderColor,
//                             maxLines: 5,
//                             keyboardType: TextInputType.text,
//                             decoration: InputDecoration(
//                                 border: InputBorder.none,
//                                 label: Text("Give your feedback here..."),
//                                 fillColor: AppColor.whiteColor,
//                                 focusedBorder: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(10),
//                                     borderSide: BorderSide(
//                                         width: 1, color: AppColor.borderColor)),
//                                 enabledBorder: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(10),
//                                     borderSide: BorderSide(
//                                         width: 1,
//                                         color: AppColor.borderColor))),
//                             controller: _feedBckController,
//                           ),
//                           SizedBox(height: 10),
//                           SizedBox(
//                             height: 50,
//                             width: 500,
//                             child: ElevatedButton(
//                                 style: ButtonStyle(
//                                     shape: MaterialStatePropertyAll<
//                                             RoundedRectangleBorder>(
//                                         RoundedRectangleBorder(
//                                             borderRadius:
//                                                 BorderRadius.circular(10))),
//                                     backgroundColor:
//                                         MaterialStatePropertyAll<Color>(
//                                             AppColor.appColor)),
//                                 onPressed: () {},
//                                 child: Text(
//                                   "Submit",
//                                   style: TextStyle(color: Colors.black),
//                                 )),
//                           )
//                         ],
//                       ),
//                     ),
//                   ],
//                 ))),
//       );
//     });
//   }
// }
