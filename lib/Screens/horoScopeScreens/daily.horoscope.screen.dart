import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../dialog/showLoaderDialog.dart';

import '../../Core/langConstant/language.constant.dart';
import '../../utils/AppColor.dart';

class HoroscopeScreen extends StatefulWidget {
  final IntCallback onChangedItem;

  HoroscopeScreen({super.key, required this.onChangedItem});

  @override
  State<HoroscopeScreen> createState() => _HoroscopeScreenState();
}

typedef void IntCallback(int id);

class _HoroscopeScreenState extends State<HoroscopeScreen> {
  int selectedHoroIcon = -1;
  bool selectDays = false;
  int selectedDayIndex = 1;
  bool selectedYears = false;
  int selectedYrIndex = 1;
  bool alsoCheckVisibility = false;

  Widget build(BuildContext context) {
    final List<String> horoNmList = [
      "Aries",
      "Taurus",
      "Gemini",
      "Cancer",
      "Leo",
      "Virgo",
      "libra",
      "scorpio",
      "Sagittarius",
      "Capricorn",
      "Aquarius",
      "Pisces",
    ];
    final List<String> horoImgList = [
      "asset/icons/aries.png",
      "asset/icons/taurus.png",
      "asset/icons/gemini.png",
      "asset/icons/cancer.png",
      "asset/icons/leo.png",
      "asset/icons/virgo.png",
      "asset/icons/libra.png",
      "asset/icons/scorpio.png",
      "asset/icons/sagittarius.png",
      "asset/icons/capricorn.png",
      "asset/icons/aquarius.png",
      "asset/icons/pisces.png",
    ];
    final List<String> dailyHoroList = [
      "Love",
      "Career",
      "Money",
      "Health",
      "Travel"
    ];
    final List<String> emojiList = ["❤️", "🧰", "🤑", "🩺", "✈️"];
    final List<Color> colorList = [
      Color(0xFFFF6868),
      Color(0xFFFFBB64),
      Color(0xFF6DDCCF),
      Color(0xFF00A9FF),
      Color(0xFFD0A2F7),
    ];
    final List<Color> lightColorList = [
      Color(0xFFFFCAC8),
      Color(0xFFFFD3B4),
      Color(0xFFB8DFD8),
      Color(0xFFA0E9FF),
      Color(0xFFE5D4FF),
    ];

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            "Daily Horoscope".tr(),
            style: GoogleFonts.poppins(),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              /********************** List of Horoscope ***********************/
              Container(
                height: 120,
                width: double.infinity,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: horoImgList.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              selectedHoroIcon = index;
                            });
                          },
                          child: horoScopeWidget("${horoImgList[index]}",
                              "${horoNmList[index]}", index),
                        ),
                      );
                    }),
              ),
              /********************** Days Buttons Section ********************/
              Container(
                height: 70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectDays = false;
                          selectedDayIndex = 0;
                          print("${selectedDayIndex}");
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: AppColor.darkGrey),
                            color: selectedDayIndex == 0
                                ? AppColor.appColor.withOpacity(.2)
                                : AppColor.appBackgroundLightGrey,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10))),
                        height: 40,
                        width: 100,
                        child: Center(child: Text("Yesterday")),
                      ),
                    ),
                    VerticalDivider(
                      width: 1,
                      indent: 15,
                      endIndent: 15,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectDays = false;
                          selectedDayIndex = 1;
                          print("${selectedDayIndex}");
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColor.darkGrey),
                          color: selectedDayIndex == 1
                              ? AppColor.appColor.withOpacity(.2)
                              : AppColor.appBackgroundLightGrey,
                        ),
                        height: 40,
                        width: 100,
                        child: Center(child: Text("Today")),
                      ),
                    ),
                    VerticalDivider(
                      width: 1,
                      indent: 15,
                      endIndent: 15,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectDays = false;
                          selectedDayIndex = 2;
                          print("${selectedDayIndex}");
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColor.darkGrey),
                          color: selectedDayIndex == 2
                              ? AppColor.appColor.withOpacity(.2)
                              : AppColor.appBackgroundLightGrey,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        ),
                        height: 40,
                        width: 100,
                        child: Center(child: Text("Tomorrow")),
                      ),
                    ),
                    // for (int btnIndex = 0;
                    //     btnIndex < daysList.length;
                    //     btnIndex++)
                    //   InkWell(
                    //       onTap: () {
                    //         setState(() {
                    //           selectedDayIndex = btnIndex;
                    //         });
                    //       },
                    //       child: Container(
                    //         color: btnIndex == selectedDayIndex
                    //             ? AppColor.appColor
                    //             : AppColor.appBackgroundLightGrey,
                    //         height: 40,
                    //         width: 100,
                    //         child:
                    //             Center(child: Text("${daysList[btnIndex]}")),
                    //       )),
                  ],
                ),
              ),
              /************************** Card Section ***********************/
              Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 200,
                      width: 500,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          image: DecorationImage(
                              image: AssetImage("asset/icons/nature.jpg"),
                              fit: BoxFit.cover)),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              SizedBox(height: 30),
                              Expanded(
                                child: Divider(
                                  indent: 150,
                                  color: Colors.white,
                                  thickness: 2,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5.0, right: 5.0),
                                child: Text(
                                  "19-01-24",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                child: Divider(
                                  endIndent: 150,
                                  color: Colors.white,
                                  thickness: 2,
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 150),
                            height: 150,
                            width: 500,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Your Daily horoscope is ready!",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Lucky Colours",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      Text(
                                        "Mood of day",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          CircleAvatar(
                                            radius: 8,
                                            backgroundColor: Colors.red,
                                          ),
                                          CircleAvatar(
                                            radius: 8,
                                            backgroundColor: Colors.greenAccent,
                                          ),
                                        ],
                                      ),
                                      Align(
                                          alignment: Alignment.centerRight,
                                          child: Text("😄")),
                                    ],
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Lucky Number",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      Text(
                                        "Lucky Time",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "7",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      Text(
                                        "12:30 AM",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 65,
                    left: 320,
                    child: CircleAvatar(
                      radius: 40,
                      backgroundImage: AssetImage("asset/icons/aries.png"),
                    ),
                  ),
                  Positioned(
                    top: 65,
                    left: 320,
                    child: CircleAvatar(
                      radius: 40,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage("asset/icons/aries.png")),
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [AppColor.appColor, Colors.white])),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 95,
                    left: 295,
                    child: CircleAvatar(
                      radius: 15,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage("asset/icons/aries.png")),
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [AppColor.appColor, Colors.white])),
                      ),
                    ),
                  ),
                ],
              ),
              /************************* Daily Horoscope Section ***********************/
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  "Daily Horoscope",
                  style: TextStyle(fontSize: 25),
                ),
              ),
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: dailyHoroList.length,
                  shrinkWrap: true,
                  itemBuilder: (context, dIndex) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: lightColorList[dIndex].withOpacity(.4),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(
                                color: colorList[dIndex].withOpacity(1))),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "${emojiList[dIndex]}  ${dailyHoroList[dIndex]}",
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    "100%",
                                    style: TextStyle(fontSize: 20),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "You can easily generate a semicircle using a rectangle and a circle. Reshape the containers with BoxDecoration(shape:) This will work, you can change the dimensions, but make sure the height is half the borderRadius and the width is equal to the borderRadius.",
                                style: TextStyle(fontSize: 16),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
              SizedBox(height: 10),
              /*********************** Live Jyotish Section *********************/
              Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Jyotshi Live",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                        InkWell(
                          onTap: () {
                            widget.onChangedItem(99);
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            "view_all",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: double.infinity,
                    height: 200,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: 10,
                      itemBuilder: (BuildContext context, int i) {
                        return Container(
                          margin: EdgeInsets.only(left: 10),
                          height: 200,
                          width: 200,
                          child: Card(
                            elevation: 2,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Stack(
                              children: [
                                Positioned(
                                  child: Image.network(
                                    fit: BoxFit.cover,
                                    'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=1760&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                                  ),
                                  right: 0,
                                  left: 0,
                                  top: 0,
                                  bottom: 0,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 120),
                                  width: double.infinity,
                                  height: 100,
                                  child: ClipRect(
                                    child: BackdropFilter(
                                      filter: ImageFilter.blur(
                                          sigmaX: 1.0, sigmaY: 1.0),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                begin: Alignment.bottomCenter,
                                                end: Alignment.topCenter,
                                                colors: [
                                              Colors.black54.withOpacity(1),
                                              Colors.white70.withOpacity(00)
                                            ])),
                                        height: 40,
                                        child: Center(
                                          child: Text(
                                            "Jyotshi Name",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
              /************************ Daily Horoscope Section ***********************/
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  "Daily Horoscope Insight",
                  style: TextStyle(fontSize: 25),
                ),
              ),
              Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 2,
                    itemBuilder: (BuildContext context, int i) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          height: 300,
                          width: 250,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Colors.grey.shade300,
                                    blurRadius: 1,
                                    offset: Offset(0, 2))
                              ],
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Container(
                                      height: 200,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: CachedNetworkImageProvider(
                                                  'https://images.unsplash.com/photo-1633332755192-727a05c4013d?q=80&w=1760&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'))),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                  width: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "'Light of Diya'",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          "can uplift your mood",
                                          style: TextStyle(
                                              fontSize: 17,
                                              color: AppColor.borderColor,
                                              fontWeight: FontWeight.normal),
                                        ),
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  /*Quotes The Days*/
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      margin: EdgeInsets.only(left: 10),
                      height: 300,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade300,
                                blurRadius: 1,
                                offset: Offset(0, 2))
                          ],
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Container(
                                  height: 200,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Colors.black,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                        height: 200,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            border: Border.all(
                                                color: Colors.white)),
                                        child: Stack(
                                          children: <Widget>[
                                            Image(
                                                height: 35,
                                                image: AssetImage(
                                                    "asset/images/blue_logo1.png")),
                                            Positioned(
                                              top: 7,
                                              left: 45,
                                              child: Text(
                                                "Jyotshi Rahasya",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Positioned(
                                              top: 7,
                                              left: 259,
                                              child: Container(
                                                color: AppColor.appColor,
                                                child: Text(
                                                  "Jyotshi Rahasya",
                                                  style: TextStyle(
                                                      color: AppColor.darkGrey),
                                                ),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.center,
                                              child: Text(
                                                "For this reason, all APIs that allow a Flutter app developer to cancel a back navigation at the time that a back gesture is received are now deprecated. They have been replaced with equivalent APIs that maintain a boolean state at all times that dictates whether or not back navigation is possible. When it is, the predictive back animation happens as usual. Otherwise, navigation is stopped. In both cases, the app developer is informed that a back was attempted and whether it was successful.",
                                                maxLines: 5,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: AppColor.whiteColor),
                                              ),
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            width: double.infinity,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Quote of the Day",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  ElevatedButton(
                                      onPressed: () {},
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStatePropertyAll<Color>(
                                                  AppColor.appColor)),
                                      child: Row(
                                        children: <Widget>[
                                          Image(
                                              height: 25,
                                              image: AssetImage(
                                                  "asset/images/whatsapp.png")),
                                          Text("Share".tr()),
                                        ],
                                      ))
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  "Also Check".tr(),
                  style: TextStyle(fontSize: 25),
                ),
              ),
              Container(
                height: 70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedYears = false;
                          selectedYrIndex = 0;
                          alsoCheckVisibility = false;
                          print("${selectedYrIndex}");
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: AppColor.darkGrey),
                            color: selectedYrIndex == 0
                                ? AppColor.appColor.withOpacity(.2)
                                : AppColor.appBackgroundLightGrey,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10))),
                        height: 50,
                        width: 120,
                        child: Center(
                            child:
                                Center(child: Text("Weekly Horoscope".tr()))),
                      ),
                    ),
                    VerticalDivider(
                      width: 1,
                      indent: 15,
                      endIndent: 15,
                    ),
                    /*Monthly Button*/
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedYears = false;
                          selectedYrIndex = 1;
                          alsoCheckVisibility = true;
                          print("${selectedYrIndex}");
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColor.darkGrey),
                          color: selectedYrIndex == 1
                              ? AppColor.appColor.withOpacity(.2)
                              : AppColor.appBackgroundLightGrey,
                        ),
                        height: 50,
                        width: 120,
                        child: Center(
                            child:
                                Center(child: Text("Monthly Horoscope".tr()))),
                      ),
                    ),
                    VerticalDivider(
                      width: 1,
                      indent: 15,
                      endIndent: 15,
                    ),
                    /*Yearly Button*/
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedYears = false;
                          selectedYrIndex = 2;
                          alsoCheckVisibility = false;
                          print("${selectedYrIndex}");
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColor.darkGrey),
                          color: selectedYrIndex == 2
                              ? AppColor.appColor.withOpacity(.2)
                              : AppColor.appBackgroundLightGrey,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        ),
                        height: 50,
                        width: 120,
                        child: Center(
                            child:
                                Center(child: Text("Yearly Horoscope".tr()))),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              /************************* Weekly Section *******************************/
              Visibility(
                visible:
                    selectedYrIndex == 0 ? alsoCheckVisibility = true : false,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: AppColor.appColor)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Virgo Weekly Horoscope\n----- 21 Jan - 27 Jan -----",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          Text(
                              "When a class is defined, only the specification for the object is defined; no memory or storage is allocated. To use the data and access functions defined in the class, you need to create objects."),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              /*************************** Monthly Section **************************/
              Visibility(
                visible:
                    selectedYrIndex == 1 ? alsoCheckVisibility = true : false,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: AppColor.appColor)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Virgo Monthly Horoscope \n    ------- Jan 2024 -------",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          Text(
                              "The public data members are also accessed in the same way given however the private data members are not allowed to be accessed directly by the object. Accessing a data member depends solely on the access control of that data member."),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              /********************************* Yearly Section **********************/
              Visibility(
                visible:
                    selectedYrIndex == 2 ? alsoCheckVisibility = true : false,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: AppColor.appColor)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Virgo Yearly Horoscope \n       -------2024-------",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          Text(
                              "Note that all the member functions defined inside the class definition are by default inline, but you can also make any non-class function inline by using the keyword inline with them. Inline functions are actual functions, which are copied everywhere during compilation, like pre-processor macro, so the overhead of function calls is reduced."),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              /********************************* Feedback Section **********************/
              InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => _feedbackDialog(context),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: AppColor.appColor.withOpacity(.2),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: AppColor.transparentBlack)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Share Feedback",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          SizedBox(width: 10),
                          Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(
                                Icons.arrow_forward_ios_rounded,
                                size: 15,
                                color: AppColor.whiteColor,
                              ))
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ));
  }

  Widget _feedbackDialog(BuildContext context) {
    List<String> feedbackOptions = ["Great", "Average", "Needs improvement"];
    TextEditingController _feedBckController = TextEditingController();
    final GlobalKey formKey = GlobalKey();
    int? selectedOptionIndex;
    return StatefulBuilder(builder: (context, StateSetter setState) {
      return Form(
        key: formKey,
        child: AlertDialog(
            backgroundColor: AppColor.whiteColor,
            title: Text(
                'How was your overall experience of Daily horoscope?'.tr()),
            content: Container(
                height: selectedOptionIndex != null ? 400 : 200,
                width: 500,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                          itemCount: feedbackOptions.length,
                          itemBuilder: (context, index) {
                            return CheckboxListTile(
                              controlAffinity: ListTileControlAffinity.leading,
                              title: Text(feedbackOptions[index]),
                              value: selectedOptionIndex == index,
                              onChanged: (value) {
                                setState(() {
                                  selectedOptionIndex = value! ? index : null;
                                });
                              },
                            );
                          }),
                    ),
                    Divider(thickness: 1),
                    Visibility(
                      visible: selectedOptionIndex != null,
                      child: Column(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text("Share your feedback".tr(),
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))),
                          SizedBox(height: 10),
                          TextFormField(
                            cursorColor: AppColor.borderColor,
                            maxLines: 5,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                label: Text("Give your feedback here".tr()),
                                fillColor: AppColor.whiteColor,
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: AppColor.borderColor)),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: AppColor.borderColor))),
                            controller: _feedBckController,
                          ),
                          SizedBox(height: 10),
                          SizedBox(
                            height: 50,
                            width: 500,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    shape: MaterialStatePropertyAll<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10))),
                                    backgroundColor:
                                        MaterialStatePropertyAll<Color>(
                                            AppColor.appColor)),
                                onPressed: () {},
                                child: Text(
                                  "Submit".tr(),
                                  style: TextStyle(color: Colors.black),
                                )),
                          )
                        ],
                      ),
                    ),
                  ],
                ))),
      );
    });
  }

  /********************************* Custom Horoscope Slider Section **********************/
  Widget horoScopeWidget(String img, String name, int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        index == selectedHoroIcon
            ? Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 70,
                      width: 70,
                      decoration: BoxDecoration(
                          color: AppColor.appColor,
                          image:
                              DecorationImage(scale: 2, image: AssetImage(img)),
                          borderRadius: BorderRadius.circular(50)),
                    ),
                  ),
                ],
              )
            : Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          border: Border.all(color: AppColor.appColor),
                          color: AppColor.appBackgroundLightGrey,
                          image:
                              DecorationImage(scale: 2, image: AssetImage(img)),
                          borderRadius: BorderRadius.circular(50)),
                    ),
                  ),
                ],
              ),
        SizedBox(height: 10),
        Text(name,
            style: TextStyle(
              fontSize: 12,
            )),
      ],
    );
  }
}
