import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:device_info_plus/device_info_plus.dart';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_google_places_hoc081098/flutter_google_places_hoc081098.dart';
import 'package:flutter_google_places_hoc081098/google_maps_webservice_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:go_router/go_router.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:jyotish_rahsaya/Core/logger_helper.dart';
import '../Core/Api/url_constants.dart';
import '../Core/notification/notification_handler_android.dart';
import '../Core/Model/UpdateProfileResponse.dart';
import '../Core/Model/UserDetails.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../dialog/showLoaderDialog.dart';
import '../utils/AppColor.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileScreenState();
  }
}

class _ProfileScreenState extends State<ProfileScreen> {
  String? profileImg = "https://picsum.photos/id/237/200/300";
  String MobileNumber = "+91-1234567890";
  File? imageFile = null;
  String filePath = "";
  final picker = ImagePicker();

  final _formKey = GlobalKey<FormState>();
  DioClient? client;
  String lat = "";
  String lon = "";
  int selectedGenderTile = 1;
  int selectedMaritalTile = 1;
  int selectedWorkingTile = 1;
  var dates;
  String dob = "";

  String deviceToken = "";
  String selectedDob = "";
  String user_id = "";
  String userImage = "";
  String timeOfBirth = "";
  int? customerID;
  TextEditingController tobController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController zipController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController placeOfBirthController = TextEditingController();
  TextEditingController currentAddressController = TextEditingController();
  TextEditingController cityStateController = TextEditingController();
  UserDetails? userDetails;

  @override
  void initState() {
    super.initState();
    FirebaseMessaging _firebaseMessaging =
        FirebaseMessaging.instance; // Change here
    _firebaseMessaging.getToken().then((token) {
      setState(() {
        deviceToken = token!;
        getUserData();
        getUserId();
      });
    });
    checkAndRequestPermissions();
  }

  String customerName = "";

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      // Retrieve values and handle null cases
      customerID = prefs.getInt(Constants.userID);
      customerName = prefs.getString(Constants.fullName)!;
    });
  }

  void getUserData() async {
    Future.delayed(Duration.zero, () {
      showLoaderDialog(context);
    });

    client = DioClient();
    Dio dio = await client!.getClient();
    final prefs = await SharedPreferences.getInstance();

    var params = jsonEncode({
      'user_id': customerID,
      // 'device_token': deviceToken
    });
    String api = UrlConstants.profileCustomerDetail;
final url = UrlConstants.baseUrl+api;
    UserDetails? userDetails = await client?.userDetails(dio, url, params);

    Navigator.pop(context);
    if (userDetails!.status == true) {
      setState(() {
        nameController
          ..text = userDetails.data.name.toString().replaceAll("null", "");
        MobileNumber = userDetails.data.mobile.toString();
        userImage = userDetails.data.image.toString();
        user_id = userDetails.data.id.toString();
        dob = userDetails.data.customer.dob.toString();
        lat = userDetails.data.customer.lat;
        lon = userDetails.data.customer.lon;
        selectedDob = userDetails.data.customer.dob.toString();
        tobController..text = userDetails.data.customer.dobTime.toString();
        timeOfBirth = userDetails.data.customer.dobTime.toString();
        placeOfBirthController
          ..text = userDetails.data.customer.dobPlace.toString();
        currentAddressController
          ..text = userDetails.data.customer.currentAddress.toString();
        cityStateController
          ..text = userDetails.data.customer.address.toString();
        zipController..text = userDetails.data.customer.pincode.toString();
        if (userDetails.data.customer.gender.toString() == "Male") {
          selectedGenderTile = 1;
        } else {
          selectedGenderTile = 2;
        }
        prefs.setString(Constants.mobile, MobileNumber);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColor.appColor,
      statusBarBrightness: Brightness.dark,
    ));

    return Scaffold(
      backgroundColor: AppColor.appBackgroundLightGrey,
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
        ),
        backgroundColor: AppColor.appColor,
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: Column(
        children: [
          const SizedBox(height: 20),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  Container(
                                    width: 100,
                                    height: 100,
                                    child: Stack(
                                      children: [
                                        CircleAvatar(
                                          radius: 100,
                                          backgroundImage: imageFile == null &&
                                                  userImage == ""
                                              ? AssetImage(
                                                  'asset/images/man.png')
                                              : imageFile == null
                                                  ? CachedNetworkImageProvider(
                                                      "$userImage")
                                                  : Image.file(
                                                      imageFile!,
                                                      fit: BoxFit.cover,
                                                    ).image,
                                        ),
                                        Align(
                                          alignment: Alignment.bottomRight,
                                          child: GestureDetector(
                                            onTap: () {
                                              showOptions();
                                            },
                                            child: Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              child: Icon(Icons.camera),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Text(
                                MobileNumber.toString(),
                                style: TextStyle(fontSize: 14),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: nameController,
                              maxLines: 1,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText: 'Enter Name',
                                labelText: "Name *",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                              validator: (value) {
                                if (value == null) {
                                  return null;
                                }
                                if (value.isEmpty) {
                                  return "Please enter country";
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 10),
                            Row(
                              children: [
                                RichText(
                                  text: const TextSpan(
                                    text: 'Gender : ',
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ), /*defining default style is optional */
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                    child: ListTileTheme(
                                        horizontalTitleGap: 1,
                                        child: RadioListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: 1,
                                          groupValue: selectedGenderTile,
                                          title: Text(
                                            "Male",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          onChanged: (val) {
                                            print("Radio Tile pressed $val");
                                            setSelectedRadioTile(val!);
                                          },
                                          activeColor: AppColor.appColor,
                                        ))),
                                Expanded(
                                    child: ListTileTheme(
                                        horizontalTitleGap: 1,
                                        child: RadioListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: 2,
                                          groupValue: selectedGenderTile,
                                          title: Text("Female",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                          onChanged: (val) {
                                            print("Radio Tile pressed $val");
                                            setSelectedRadioTile(val!);
                                          },
                                          activeColor: AppColor.appColor,
                                        ))),
                                Spacer(),
                              ],
                            ),
                            const SizedBox(height: 10),
                            TextFormField(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                _selectDate(context, "1");
                              },
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: dobController..text = dob,
                              maxLines: 1,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText: 'Please select date of birth',
                                labelText: "Date of Birth",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                TimeOfDay? time = await getTime(
                                  context: context,
                                  title: "Select Time of Birth",
                                );
                                setState(() {
                                  String min = "";
                                  if (time!.minute.toString().length < 2) {
                                    min = "0${time?.minute}";
                                  } else {
                                    min = "${time?.minute}";
                                  }
                                  timeOfBirth =
                                      "${time.hourOfPeriod}:$min ${time.period == DayPeriod.am ? 'AM' : 'PM'}";
                                });
                                print("SelectedTime --> $time");
                              },
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: tobController..text = timeOfBirth,
                              maxLines: 1,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText: 'Please select time of birth',
                                labelText: "Time of Birth",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                Prediction? p = await PlacesAutocomplete.show(
                                  offset: 0,
                                  radius: 1000,
                                  types: [],
                                  strictbounds: false,
                                  region: "in",
                                  onError: onError,
                                  context: context,
                                  apiKey:
                                      "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                                  mode: Mode
                                      .overlay, // Mode.fullscreen language: "en", onError: (value) { Utils.displayToast(context, value.errorMessage!); }, components: [Component(Component.country, "in")] );
                                );
                                displayPrediction(p!);
                                //cityController..text = p.description!;
                                final _places = GoogleMapsPlaces(
                                  apiKey:
                                      "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                                  apiHeaders: await const GoogleApiHeaders()
                                      .getHeaders(),
                                );

                                final detail = await _places
                                    .getDetailsByPlaceId(p.placeId!);
                                final geometry = detail.result.geometry!;
                                setState(() {
                                  lat = geometry.location.lat.toString();
                                  lon = geometry.location.lng.toString();
                                });
                                TLoggerHelper.debug(
                                    lat + "   ------    " + lat);
                                _getAddressFromLatLng(
                                    double.parse(lat),
                                    double.parse(lon),
                                    placeOfBirthController,
                                    p.description!);
                              },
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: placeOfBirthController,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText: 'Place of Birth',
                                labelText: "Place of Birth",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: currentAddressController,
                              maxLines: 1,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText:
                                    'Enter Flat, House no, Building, Apartment',
                                labelText: "Current Address",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                Prediction? p = await PlacesAutocomplete.show(
                                  offset: 0,
                                  radius: 1000,
                                  types: [],
                                  strictbounds: false,
                                  region: "in",
                                  onError: onError,
                                  context: context,
                                  apiKey:
                                      "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                                  mode: Mode
                                      .overlay, // Mode.fullscreen language: "en", onError: (value) { Utils.displayToast(context, value.errorMessage!); }, components: [Component(Component.country, "in")] );
                                );
                                displayPrediction(p!);
                                //cityController..text = p.description!;
                                final _places = GoogleMapsPlaces(
                                  apiKey:
                                      "AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4",
                                  apiHeaders: await const GoogleApiHeaders()
                                      .getHeaders(),
                                );

                                final detail = await _places
                                    .getDetailsByPlaceId(p.placeId!);
                                final geometry = detail.result.geometry!;
                                setState(() {
                                  lat = geometry.location.lat.toString();
                                  lon = geometry.location.lng.toString();
                                });
                                TLoggerHelper.debug(
                                    lat + "   ------    " + lat);

                                _getAddressFromLatLng(
                                    double.parse(lat),
                                    double.parse(lon),
                                    cityStateController,
                                    p.description!);
                              },
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: cityStateController,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText: 'City, State, Country',
                                labelText: "City, State, Country",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              // The validator receives the text that the user has entered.
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: zipController,
                              keyboardType: TextInputType.number,
                              maxLines: 1,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: AppColor.appColor,
                                      width: 1.5,
                                    )),
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                hintText: 'Enter pincode',
                                labelText: "Pincode *",
                                labelStyle: const TextStyle(
                                    color: Colors.black87, fontSize: 16),
                                hintStyle: const TextStyle(fontSize: 14),
                              ),
                              maxLength: 6,
                            ),
                            const SizedBox(height: 20),
                          ],
                        )),
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              updateProfile();
            },
            child: Container(
              color: AppColor.appColor,
              padding: EdgeInsets.all(10),
              width: double.infinity,
              child: Center(
                child: Text(
                  "Submit".tr(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<TimeOfDay?> getTime({
    required BuildContext context,
    String? title,
    TimeOfDay? initialTime,
    String? cancelText,
    String? confirmText,
  }) async {
    TimeOfDay? time = await showTimePicker(
      initialEntryMode: TimePickerEntryMode.dial,
      context: context,
      initialTime: initialTime ?? TimeOfDay.now(),
      cancelText: cancelText ?? "Cancel",
      confirmText: confirmText ?? "Save",
      helpText: title ?? "Select time",
      builder: (context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child!,
        );
      },
    );

    return time;
  }

  _selectDate(BuildContext context, String type) async {
    print("DatePickerDialog");
    DateTime selectedDate = DateTime.now();
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        // Refer step 1
        firstDate: DateTime(1950),
        lastDate: DateTime.now(),
        helpText: 'Select date range',
        // Can be used as title
        cancelText: 'Not Now',
        confirmText: 'Confirm');
    if (picked != null && picked != selectedDate) {
      setState(() {
        String formattedDate = DateFormat('dd-MMMM-yyyy').format(picked);
        selectedDob = DateFormat('yyyy/MM/dd').format(picked);
        dob = formattedDate;
      });
    }
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedGenderTile = val;
    });
  }

  selectFile() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
        filePath = pickedFile.path;
        print("filePath---> $filePath");
      }
    });
  }

  Future<bool> checkAndRequestPermissions() async {
    if (Platform.isAndroid) {
      final androidInfo = await DeviceInfoPlugin().androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        Map<Permission, PermissionStatus> status = await [
          Permission.camera,
          Permission.storage,
        ].request();

        if (status[Permission.camera] == PermissionStatus.granted &&
            status[Permission.storage] == PermissionStatus.granted) {
          return true;
        } else {
          checkAndRequestPermissions();
          return false;
        }
      } else {
        Map<Permission, PermissionStatus> status = await [
          Permission.camera,
          Permission.photos,
        ].request();

        if (status[Permission.camera] == PermissionStatus.granted &&
            status[Permission.photos] == PermissionStatus.granted) {
          return true;
        } else {
          checkAndRequestPermissions();
          return false;
        }
      }
    }
    return false;
  }

  Future getImageFromCamera() async {
    final pickedFile = await picker.pickImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
        filePath = pickedFile.path;
        print("filePath---> $filePath");
      }
    });
  }

  Future showOptions() async {
    showCupertinoModalPopup(
      context: context,
      builder: (context) => CupertinoActionSheet(
        actions: [
          CupertinoActionSheetAction(
            child: Text('Photo Gallery'),
            onPressed: () {
              // close the options modal
              Navigator.of(context).pop();
              // get image from gallery
              selectFile();
            },
          ),
          CupertinoActionSheetAction(
            child: Text('Camera'),
            onPressed: () {
              // close the options modal
              Navigator.of(context).pop();
              // get image from camera
              getImageFromCamera();
            },
          ),
        ],
      ),
    );
  }

  Future<void> _getAddressFromLatLng(double lat, double lng,
      TextEditingController cityController, String description) async {
    final prefs = await SharedPreferences.getInstance();
    await placemarkFromCoordinates(lat, lng).then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      print(place);
      setState(() {
        prefs.setString(Constants.user_address, description);
        /*countryController..text = place.country!;
        stateController..text = place.administrativeArea!;*/
        cityController
          ..text = place.locality! +
              ", " +
              place.administrativeArea! +
              ", " +
              place.country!;
        //zipController..text = place.postalCode!;
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  Future<void> updateProfile() async {
    final prefs = await SharedPreferences.getInstance();

    String fileName = "";
    String filePath_ = filePath;
    if (filePath != "") {
      fileName = imageFile!.path.split('/').last;
    }
    Future.delayed(Duration.zero, () {
      showLoaderDialog(context);
    });
    client = DioClient();
    Dio dio = await client!.getClient();

    var formdata = FormData.fromMap({
      'image': filePath_ != ""
          ? await MultipartFile.fromFile(imageFile!.path, filename: fileName)
          : "",
      'user_id': prefs.getInt(Constants.userID),
      'name': nameController.text,
      'gender': selectedGenderTile == 1 ? "Male" : "Female",
      'dob': selectedDob,
      "dob_time": timeOfBirth,
      'dob_place': placeOfBirthController.text,
      'pincode': zipController.text,
      'current_address': currentAddressController.text,
      'address': cityStateController.text,
      "lat": lat,
      "lon": lon
      // other parameter may you need to send
    });

    print("formdata---> ${formdata.toString()}");

    dio
        .post(UrlConstants.baseUrl+UrlConstants.updateProfileCustomer, data: formdata)
        .catchError((error) => Fluttertoast.showToast(
            msg: "Something went wrong, please try again later."))
        .then((response) async {
      Navigator.pop(context);
      var jsonResponse = jsonDecode(response.toString());
      if (jsonResponse["status"] == true) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        // SnackBar(content: Text("Profile updated successfully"));
        var mobileNumm = jsonResponse["customer"]["mobile"];
        var userName = jsonResponse["customer"]["name"];
        var userEmail = jsonResponse["customer"]["email"];
        print("================== Shared Preferences==================");
        await prefs.setString(Constants.mobile, mobileNumm.toString());
        await prefs.setString(Constants.fullName, userName);
        await prefs.setString(
            Constants.profileImage, jsonResponse["customer"]["image"]);
        await prefs.setString(
            Constants.gender, jsonResponse["customer"]["gender"]);
        await prefs.setString(Constants.dob, jsonResponse["customer"]["dob"]);
        await prefs.setString(
            Constants.dob_time, jsonResponse["customer"]["dob_time"]);
        await prefs.setString(
            Constants.dob_place, jsonResponse["customer"]["dob_place"]);
        await prefs.setString(
            Constants.pincode, jsonResponse["customer"]["pincode"]);
        await prefs.setString(Constants.current_address,
            jsonResponse["customer"]["current_address"]);
        await prefs.setString(
            Constants.address, jsonResponse["customer"]["address"]);
        await prefs.setString(Constants.email, userEmail ?? "");
        int? newUserID = prefs.getInt(Constants.userID);
        print("My User ProfileID -> $newUserID");
        Fluttertoast.showToast(msg: "Profile updated successfully.");
        context.pop();
      } else {
        Fluttertoast.showToast(
            msg: "Something went wrong, please try again later.");
      }
    });

    /*var params =  jsonEncode({'user_id': user_id,'name': nameController.text,'gender': selectedGenderTile == 1 ? "Male" : "Female",
      'dob': selectedDob,'dob_time': timeOfBirth,'dob_place': placeOfBirthController.text,'image': filePath,
      'pincode': zipController.text,'current_address': currentAddressController.text,'address': cityStateController.text, });
    String api = Constants.updateProfile;

    UpdateProfileResponse? updateProfileResponse = await client?.updateProfile(dio,api,params);
    Navigator.pop(context);
    if(updateProfileResponse!.status == true){
      SnackBar(content: Text("Profile updated successfully"));
    }else{
      SnackBar(content: Text("Please try again"));
    }*/
  }
}

void onError(PlacesAutocompleteResponse response) {
  print(response.errorMessage);
}

void displayPrediction(Prediction prediction) {
  print(prediction.description);
}
