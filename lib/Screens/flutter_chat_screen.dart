import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:clipboard/clipboard.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/Model/recharge.amount.model.dart';
import '../Core/Provider/kundli_provider_new.dart';
import 'NavigationScreen.dart';
import '../SubScreens/HomeScreen.dart';
import '../dialog/showLoaderDialog.dart';
import '../kundali_testing_tabs/kundli_testing.dart';
import '../router_constants.dart';
import '../Core/Provider/kundli_provider.dart';
import '../Core/helper_functions.dart';
import '../utils/custom_circular_progress_indicator.dart';
import 'horoScopeScreens/kundliScreens/view_kundli_screen.dart';
import 'payment.info.dart';
import '../Core/Provider/list.of.api.provider.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:swipe_to/swipe_to.dart';
import 'package:uuid/uuid.dart';
import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Model/wallet.transection.model.dart';
import '../Core/Provider/chatting_provider.dart';
import '../Core/formatter.dart';
import '../Core/nav/SlideRightRoute.dart';
import '../core/logger_helper.dart';
import '../utils/AppColor.dart';
import '../utils/snack_bar.dart';

class FlutterChatScreen extends StatefulWidget {
  final String fromId;
  final String toId;
  final Duration timerDuration;
  final String astrologerName;
  final String astrologerImage;
  final int chat_price;
  final String cummID;
  final String orderID;
  bool isFreeChat;
  final Map<String, String> messageInfo;
  final String infoId;
  final bool isToView;

  final int initialRating;
  final String initialReview;
  final int reviewId;
  final String lat;
  final String lon;
  final String dob;
  final String tob;
  FlutterChatScreen({
    super.key,
    required this.orderID,
    required this.cummID,
    required this.chat_price,
    required this.fromId,
    required this.toId,
    required this.timerDuration,
    required this.astrologerName,
    required this.astrologerImage,
    required this.dob,
    required this.tob,
    required this.messageInfo,
    required this.infoId,
    required this.lat,
    required this.lon,
    this.isFreeChat = false,
    this.isToView = false,
    this.initialRating = 0,
    this.initialReview = "",
    this.reviewId = -1,
  });

  static late Timer timer;
  static late TextEditingController messageTextController;

  @override
  State<FlutterChatScreen> createState() => _FlutterChatScreenState();
}

class _FlutterChatScreenState extends State<FlutterChatScreen> {
  bool isEndLoading = false;
  bool isShowLastRechargeDialog = false;
  bool isShowFirstRechargeDialog = false;
  List<types.Message> _messages = [];
  List<types.User> typingList = [];
  String? referenceId;
  Timer? _calloutTimer;
  bool isToViewLocal = false;
  String imageUrlToSend = "";
  String messageToSend = "";
  late types.User user;
  late types.User otherUser;
  int customerID = 0;
  DioClient? client;
  String walletAmount = "0";
  Duration centerTempDuration = Duration.zero;

  File? file;
  int pageNumber = 1;

  late Duration chatStartDuration;

  refreshMessages() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      if (message.data['type'] == 'astrologer') {
        var localMessage;
        if (message.data['chat-image'].toString().isNotEmpty) {
          await Provider.of<ChattingProvider>(context, listen: false)
              .getMessageFromID(chatId: message.data['chat-id'])
              .then((value) {
            if (value != null) {
              var val = value;
              TLoggerHelper.debug(val.body.toString());
              TLoggerHelper.debug(val.attachment.toString());
              localMessage = types.ImageMessage(
                  author: otherUser,
                  showStatus: true,
                  id: val.id,
                  name: val.attachment!,
                  metadata: {"message_type": "chat"},
                  remoteId: message.data['chat-referenceId'],
                  status: types.Status.delivered,
                  size: 1440,
                  uri: val.attachment!);
              setState(() {
                _messages.insert(0, localMessage);
              });
            }
          });
        } else {
          localMessage = types.TextMessage(
              author: otherUser,
              remoteId: message.data['chat-referenceId'],
              id: message.data['chat-id'].toString(),
              status: types.Status.delivered,
              metadata: {"message_type": "chat"},
              text: message.notification!.body!);
          setState(() {
            _messages.insert(0, localMessage);
          });
        }
        Provider.of<ChattingProvider>(context, listen: false).sendSeenStatus(
            fromId: widget.fromId,
            toId: widget.toId,
            chatInfoId: widget.infoId);
      } else if (message.data['type'].toString().toLowerCase() ==
          'end-resume'.toLowerCase()) {
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
      } else if (message.data['type'].toLowerCase().toString() == "chat-end") {
        if (FlutterChatScreen.timer != null) {
          FlutterChatScreen.timer.cancel();
        }
        if (FlutterChatScreen.timer != null) {
          if (FlutterChatScreen.timer.isActive) {
            FlutterChatScreen.timer.cancel();
          }
        }
        THelperFunctions.clearCallingPrefs();
        // Show the feedback dialog first
        if (!widget.isFreeChat) {
          showLoaderDialog(context).then(
            (value) async {
              await Future.delayed(Duration(seconds: 1));
              await _loadMessages();
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            },
          );
          setState(() {
            isToViewLocal = true;
          });
        } else {
          Future.delayed(Duration(milliseconds: 300), () {
            context.goNamed(RouteConstants.mainhomescreen);
          });
        }
      } else if (message.data['type'].toString().toLowerCase() == 'typing') {
        if (message.data['status'] == true ||
            message.data['status'] == 'true') {
          setState(() {
            typingList = [otherUser];
          });
        } else {
          setState(() {
            typingList = [];
          });
        }

// For debugging purposes, log the value
        TLoggerHelper.debug(
            'Status: ${message.data['status']}, Typing List: ${typingList.toString()}');
      } else if (message.data['type'].toString().toLowerCase() ==
          "seen-status") {
        if (_messages[1].status != types.Status.seen) {
          _messages.forEach((element) => _handleSeen(message: element));
        } else {
          _handleSeen(message: _messages[0]);
        }
      }
      TLoggerHelper.debug(message.data.entries.toString());
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      if (message.data['type'] == 'astrologer') {
        var localMessage;
        if (message.data['chat-image'].toString().isNotEmpty) {
          await Provider.of<ChattingProvider>(context, listen: false)
              .getMessageFromID(chatId: message.data['chat-id'])
              .then((value) {
            if (value != null) {
              var val = value;
              localMessage = types.ImageMessage(
                  author: otherUser,
                  id: val.id,
                  name: val.attachment!,
                  metadata: {"message_type": "chat"},
                  status: types.Status.delivered,
                  remoteId: message.data['chat-referenceId'],
                  size: 1440,
                  uri: val.attachment!);
              setState(() {
                _messages.insert(0, localMessage);
              });
            }
          });
        } else {
          localMessage = types.TextMessage(
              author: otherUser,
              id: message.data['chat-id'].toString(),
              status: types.Status.delivered,
              metadata: {"message_type": "chat"},
              remoteId: message.data['chat-referenceId'],
              text: message.notification!.body!);
          setState(() {
            _messages.insert(0, localMessage);
          });
        }
      } else if (message.data['type'].toString().toLowerCase() ==
          'end-resume'.toLowerCase()) {
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
      } else if (message.data['type'].toLowerCase().toString() == "chat-end") {
        if (FlutterChatScreen.timer != null) {
          FlutterChatScreen.timer.cancel();
        }
        if (FlutterChatScreen.timer != null) {
          if (FlutterChatScreen.timer.isActive) {
            FlutterChatScreen.timer.cancel();
          }
        }
        THelperFunctions.clearCallingPrefs();
        // Show the feedback dialog first
        if (!widget.isFreeChat) {
          showLoaderDialog(context).then(
            (value) async {
              await Future.delayed(Duration(seconds: 1));
              await _loadMessages();
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            },
          );

          setState(() {
            isToViewLocal = true;
          });
        } else {
          Future.delayed(Duration(milliseconds: 300), () {
            context.goNamed(RouteConstants.mainhomescreen);
          });
        }
      } else if (message.data['type'].toString().toLowerCase() == 'typing') {
        if (message.data['status'] == true ||
            message.data['status'] == 'true') {
          setState(() {
            typingList = [otherUser];
          });
        } else {
          setState(() {
            typingList = [];
          });
        }

// For debugging purposes, log the value
        TLoggerHelper.debug(
            'Status: ${message.data['status']}, Typing List: ${typingList.toString()}');
      }
      TLoggerHelper.debug(message.data.entries.toString());
    });
  }

  bool isFirstSent = false;

  void _startTimer(Duration timerDuration) {
    int elapsedSeconds = timerDuration.inSeconds;
    TLoggerHelper.warning(
        "This is the isFreeChat Status:---    " + widget.isFreeChat.toString());
    FlutterChatScreen.timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        elapsedSeconds = elapsedSeconds - 1;
        centerTempDuration = Duration(seconds: elapsedSeconds);

        if (centerTempDuration.inSeconds <= 90) {
          if (!widget.isFreeChat) {
            isShowFirstRechargeDialog = true;
          }
          if (centerTempDuration.inSeconds <= 30) {
            if (!widget.isFreeChat) {
              isShowFirstRechargeDialog = false;
              isShowLastRechargeDialog = true;
            }
          }
          if (centerTempDuration.inSeconds <= 0) {
            timer.cancel(); // Stop the current timer
            _stopAndStartNewCalloutTimer();
            // if (widget.isFreeChat) {
            // } else {
            //   _handleApiCall();
            // }
          }
        }
      });
    });
  }

  bool isLastCalloutTimer = false;

  void _stopAndStartNewCalloutTimer() {
    setState(() {
      isLastCalloutTimer = true;
    });
    _startNewCalloutTimer(); // Start a new 30-second timer
  }

  void _startNewCalloutTimer() {
    // _calloutTimer = Timer(Duration(seconds: widget.isFreeChat ? 1 : 30), () {
    _handleApiCall();
    // });
  }

  void _handleApiCall() {
    Provider.of<ChattingProvider>(context, listen: false)
        .sendChatDuration(
      communicationId: widget.cummID,
    )
        .whenComplete(
      () {
        if (_calloutTimer != null) {
          _calloutTimer!.cancel();
        }
        if (FlutterChatScreen.timer != null) {
          FlutterChatScreen.timer.cancel();
        }
        if (FlutterChatScreen.timer != null) {
          if (FlutterChatScreen.timer.isActive) {
            FlutterChatScreen.timer.cancel();
          }
        }
        THelperFunctions.clearCallingPrefs();
        // Show the feedback dialog first
        if (!widget.isFreeChat) {
          showLoaderDialog(context).then(
            (value) async {
              await Future.delayed(Duration(seconds: 1));
              await _loadMessages();
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            },
          );
          setState(() {
            isToViewLocal = true;
          });
        } else {
          showLoaderDialog(context).then(
            (value) async {
              await Future.delayed(Duration(seconds: 1));
              await _loadMessages();
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            },
          );
          setState(() {
            isToViewLocal = true;
          });
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    chatStartDuration = widget.timerDuration;
    isToViewLocal = widget.isToView;
    FlutterChatScreen.messageTextController = TextEditingController();
    TLoggerHelper.info("Timer Duration:- ${widget.timerDuration.inMinutes}");
    user = types.User(id: widget.fromId);
    otherUser = types.User(
        id: widget.toId,
        imageUrl: widget.astrologerImage,
        firstName: widget.astrologerName);
    if (!isToViewLocal) {
      _startTimer(widget.timerDuration);
      refreshMessages();
    }
    _loadMessages();
    getUserId();
    THelperFunctions.logScreenNameEvent(screenName: "chat_screen");
  }

  void _addMessage(types.Message message) {
    setState(() {
      _messages.insert(0, message);
    });
  }

  void _handleImageSelection({required ImageSource imageSource}) async {
    int index = 0;
    final result = await ImagePicker().pickImage(
      imageQuality: 70,
      maxWidth: 1440,
      source: imageSource,
    );
    setState(() {
      messageToSend = "";
      imageUrlToSend = "";
    });
    if (result != null) {
      file = File(result!.path);
      final bytes = await result.readAsBytes();
      final image = await decodeImageFromList(bytes);
      var message = types.ImageMessage(
          author: user,
          createdAt: DateTime.now().millisecondsSinceEpoch,
          height: image.height.toDouble(),
          id: const Uuid().v4(),
          name: result.name,
          size: bytes.length,
          metadata: {"message_type": "chat"},
          remoteId: referenceId,
          uri: result.path,
          width: image.width.toDouble(),
          showStatus: true,
          status: types.Status.sending);
      _addMessage(message);
      await Provider.of<ChattingProvider>(context, listen: false)
          .sendMessage(
              context: context,
              infoId: widget.infoId,
              textBody: "",
              toId: widget.toId,
              imagePath: result.path,
              referenceChatId: referenceId,
              fromId: widget.fromId)
          .then((response) async {
        index = _messages.indexWhere((element) => element.id == message.id);
        if (response.entries.first.key) {
          await Provider.of<ChattingProvider>(context, listen: false)
              .getMessages(
                  context: context,
                  toId: widget.toId,
                  infoId: widget.infoId,
                  fromId: widget.fromId,
                  pageNumber: 0)
              .then((value) {
            if (value!.status) {
              var val = value.messages.data[value.messages.data.length - 1];
              TLoggerHelper.debug(val.body.toString());
              TLoggerHelper.debug(val.attachment.toString());

              message = types.ImageMessage(
                  author: user,
                  createdAt: DateTime.now().millisecondsSinceEpoch,
                  height: image.height.toDouble(),
                  id: response.entries.first.value,
                  name: result.name,
                  remoteId: referenceId,
                  size: bytes.length,
                  uri: val.attachment!,
                  width: image.width.toDouble(),
                  metadata: {"message_type": "chat"},
                  showStatus: true,
                  status: types.Status.delivered);
            }
          });
        } else {
          message = types.ImageMessage(
              author: user,
              createdAt: DateTime.now().millisecondsSinceEpoch,
              height: image.height.toDouble(),
              id: const Uuid().v4(),
              name: result.name,
              remoteId: referenceId,
              metadata: {"message_type": "chat"},
              size: bytes.length,
              uri: result.path,
              width: image.width.toDouble(),
              showStatus: true,
              status: types.Status.error);
        }
      });
      setState(() {
        _messages[index] = message;
        referenceId = null;
        messageToSend = "";
        imageUrlToSend = "";
      });
    }
  }

  void _handlePreviewDataFetched(
    types.TextMessage message,
    types.PreviewData previewData,
  ) {
    final index = _messages.indexWhere((element) => element.id == message.id);
    final updatedMessage = (_messages[index] as types.TextMessage).copyWith(
      previewData: previewData,
    );

    setState(() {
      _messages[index] = updatedMessage;
    });
  }

  void _handleSeen({required types.Message message}) {
    final index = _messages.indexWhere((element) => element.id == message.id);

    if (index != -1) {
      final updatedMessage = _createSeenMessage(_messages[index]);
      setState(() {
        _messages[index] = updatedMessage;
      });
    }
  }

  types.Message _createSeenMessage(types.Message message) {
    if (message is types.TextMessage) {
      return message.copyWith(status: types.Status.seen);
    } else if (message is types.ImageMessage) {
      return message.copyWith(status: types.Status.seen);
    }

    throw Exception('Unsupported message type');
  }

  void _handleSendPressed(types.PartialText message) async {
    int index = 0;
    if (!isToViewLocal) {
      if (message.text.isNotEmpty) {
        // String message = messageController.text;

        // Regular expression to check for mobile numbers

        final RegExp mobileNumberPattern =
            RegExp(r'\b(?:\+?[0-9][\s-]*){7,}\b');

        if (mobileNumberPattern.hasMatch(message.text)) {
          // Show toast message
          Fluttertoast.showToast(
            msg: "You cannot send messages containing mobile numbers.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0,
          );
          return;
        }
        THelperFunctions.showTyping(astrologerId: widget.toId, status: false);
        if (mounted) {
          setState(() {
            messageToSend = "";
            imageUrlToSend = "";
            FlutterChatScreen.messageTextController.clear();
          });
        }
        var textMessage = types.TextMessage(
            author: isFirstSent ? otherUser : user,
            createdAt: DateTime.now().millisecondsSinceEpoch,
            id: const Uuid().v4(),
            text: message.text,
            remoteId: referenceId,
            metadata: {"message_type": "chat"},
            showStatus: true,
            status: types.Status.sending);

        _addMessage(textMessage);
        await Provider.of<ChattingProvider>(context, listen: false)
            .sendMessage(
                context: context,
                textBody: message.text,
                toId: isFirstSent ? widget.fromId : widget.toId,
                referenceChatId: referenceId,
                infoId: widget.infoId,
                imagePath: "",
                fromId: isFirstSent ? widget.toId : widget.fromId)
            .then((response) async {
          index =
              _messages.indexWhere((element) => element.id == textMessage.id);
          if (response.entries.first.key) {
            textMessage = types.TextMessage(
                author: isFirstSent ? otherUser : user,
                createdAt: DateTime.now().millisecondsSinceEpoch,
                id: response.entries.first.value,
                text: message.text,
                remoteId: referenceId,
                metadata: {"message_type": "chat"},
                showStatus: true,
                status: types.Status.delivered);
          } else {
            textMessage = types.TextMessage(
                author: isFirstSent ? otherUser : user,
                createdAt: DateTime.now().millisecondsSinceEpoch,
                id: const Uuid().v4(),
                text: message.text,
                metadata: {"message_type": "chat"},
                remoteId: referenceId,
                showStatus: true,
                status: types.Status.error);
          }

          setState(() {
            _messages[index] = textMessage;
            referenceId = null;
            messageToSend = "";
            imageUrlToSend = "";
          });
        });
      }
    }
  }

  Future<void> _loadMessages() async {
    var textMessage;
    List<types.Message> localMessageList = [];
    await Provider.of<ChattingProvider>(context, listen: false)
        .getMessages(
            infoId: widget.infoId,
            context: context,
            toId: widget.toId,
            fromId: widget.fromId,
            pageNumber: pageNumber)
        .then((value) {
      if (value != null) {
        if (value.status == true) {
          TLoggerHelper.info(value.messages.data[0].body!);
          for (int index = 0; index < value.messages.data.length; index++) {
            var val = value.messages.data[index];
            if (widget.fromId == val.fromId.toString()) {
              if (val.attachment!.isEmpty) {
                textMessage = types.TextMessage(
                  author: user,
                  createdAt: val.createdAt.millisecondsSinceEpoch,
                  id: val.id,
                  showStatus: true,
                  metadata: {"message_type": val.messageType},
                  remoteId: val.referenceChatId,
                  status: val.seen == 1
                      ? types.Status.seen
                      : types.Status.delivered,
                  text: val.body ?? "",
                );
              } else {
                textMessage = types.ImageMessage(
                  author: user,
                  createdAt: val.createdAt.millisecondsSinceEpoch,
                  id: val.id,
                  showStatus: true,
                  status: val.seen == 1
                      ? types.Status.seen
                      : types.Status.delivered,
                  uri: val.attachment!,
                  metadata: {"message_type": val.messageType},
                  remoteId: val.referenceChatId,
                  name: val.attachment!,
                  size: 1440,
                );
              }
            } else {
              if (val.attachment!.isEmpty) {
                textMessage = types.TextMessage(
                  author: otherUser,
                  createdAt: val.createdAt.millisecondsSinceEpoch,
                  id: val.id,
                  text: val.body ?? "",
                  showStatus: true,
                  remoteId: val.referenceChatId,
                  metadata: {"message_type": val.messageType},
                  status: val.seen == 1
                      ? types.Status.seen
                      : types.Status.delivered,
                );
              } else {
                textMessage = types.ImageMessage(
                  author: otherUser,
                  createdAt: val.createdAt.millisecondsSinceEpoch,
                  id: val.id,
                  showStatus: true,
                  status: val.seen == 1
                      ? types.Status.seen
                      : types.Status.delivered,
                  metadata: {"message_type": val.messageType},
                  uri: val.attachment!,
                  name: val.attachment!,
                  remoteId: val.referenceChatId,
                  size: 1440,
                );
              }
            }

            if (pageNumber == 1) {
              _messages.add(textMessage);
            } else {
              localMessageList.add(textMessage);
            }
          }
          setState(() {
            if (pageNumber == 1) {
              _messages = _messages.reversed.toList();
            } else {
              localMessageList = localMessageList.reversed.toList();
              _messages.addAll(localMessageList);
            }
            pageNumber++;
          });
          _messages.forEach((element) {
            if (element.remoteId != null) {
              TLoggerHelper.debug(element.type.name);
            }
          });
        }
      }
    });
    _messages.forEach((element) {
      if (element.remoteId != null) {
        if (!_messages.any((test) => test.id == element.remoteId)) {
          _loadMessages();
        }
      }
    });
    Provider.of<ChattingProvider>(context, listen: false).sendSeenStatus(
        fromId: widget.fromId, toId: widget.toId, chatInfoId: widget.infoId);
  }

  // if (didPop) {
  //   return;
  // }
  // if (!isToViewLocal) {
  //   // await _continueDialog(context);
  // } else {
  //   Navigator.of(context).pop();
  // }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(isShowLastRechargeDialog
            ? kToolbarHeight + MediaQuery.of(context).size.height * .06
            : kToolbarHeight),
        child: Column(
          children: [
            AppBar(
                elevation: 0,
                backgroundColor: AppColor.appColor,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      TFormatter.capitalizeSentence(widget.astrologerName),
                      style: GoogleFonts.poppins(
                          fontSize: 15, fontWeight: FontWeight.w500),
                    ),
                    if (!isToViewLocal)
                      Text(
                          isLastCalloutTimer
                              ? "WAITING FOR ASTROLOGER"
                              : "CHAT IN PROGRESS",
                          style: GoogleFonts.poppins(
                              fontSize: 12, color: Colors.green)),
                  ],
                ),
                actions: [
                  if (!isToViewLocal)
                    Padding(
                      padding: const EdgeInsets.only(right: 18.0),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                        decoration: BoxDecoration(
                            color: Colors.yellow,
                            borderRadius: BorderRadius.circular(8)),
                        child: Text(
                          TFormatter.formatChatDuration(centerTempDuration),
                          style: GoogleFonts.poppins(
                              fontSize: 12, color: Colors.red),
                        ),
                      ),
                    ),
                  if (!isToViewLocal)
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: InkWell(
                        onTap: () async {
                          if (!isEndLoading) {
                            await Provider.of<ChattingProvider>(context,
                                    listen: false)
                                .sendChatDuration(
                              communicationId: widget.cummID,
                            )
                                .then(
                              (value) async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                if (FlutterChatScreen.timer != null) {
                                  FlutterChatScreen.timer.cancel();
                                }
                                if (FlutterChatScreen.timer != null) {
                                  if (FlutterChatScreen.timer.isActive) {
                                    FlutterChatScreen.timer.cancel();
                                  }
                                }
                                THelperFunctions.clearCallingPrefs();
                                // Show the feedback dialog first
                                if (!widget.isFreeChat) {
                                  showLoaderDialog(context).then(
                                    (value) async {
                                      await Future.delayed(
                                          Duration(seconds: 1));
                                      if (Navigator.of(context).canPop()) {
                                        Navigator.of(context).pop();
                                      }
                                    },
                                  );
                                  setState(() {
                                    isToViewLocal = true;
                                  });
                                } else {
                                  Future.delayed(Duration(milliseconds: 300),
                                      () async {
                                    context
                                        .goNamed(RouteConstants.mainhomescreen);
                                    await prefs.setInt(Constants.isFreeFlag, 1);
                                  });
                                }
                              },
                            );
                          } else {
                            Fluttertoast.showToast(msg: "Chat Ending !!");
                          }
                        },
                        child: CircleAvatar(
                          radius: 24,
                          backgroundColor: isEndLoading
                              ? Colors.grey.shade300
                              : AppColor.whiteColor,
                          child: isEndLoading
                              ? Text(
                                  "Ending",
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      height: 0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                )
                              : Text(
                                  "End",
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      height: 0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                        ),
                      ),
                    )
                ]),
            if (isShowLastRechargeDialog)
              Flexible(
                child: Container(
                  alignment: Alignment.center,
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Balance < 5 minutes, \n Quick Recharge",
                        style: GoogleFonts.poppins(color: Colors.red),
                      ),
                      InkWell(
                        onTap: () async {
                          num initialWalletAmount =
                              await THelperFunctions.getWalletAmount();
                          await _rechargeDialog(context, "wallet").whenComplete(
                            () async {
                              num amount =
                                  await THelperFunctions.getWalletAmount() -
                                      initialWalletAmount;
                              if (amount > widget.chat_price) {
                                if (amount < widget.chat_price) {
                                  showSnackBarWidget(context,
                                      "You won't have enought recharge as of now!!");
                                } else {
                                  int totalminutestoAdd =
                                      amount ~/ (widget.chat_price);
                                  TLoggerHelper.info(
                                      "The Recharge amount is :- $amount and total time to add is $totalminutestoAdd");
                                  await Provider.of<ChattingProvider>(context,
                                          listen: false)
                                      .sendChatDurationResume(
                                          communicationId: widget.cummID,
                                          chatDuration:
                                              totalminutestoAdd.toString(),
                                          isResume: true)
                                      .whenComplete(
                                    () {
                                      setState(() {
                                        centerTempDuration =
                                            centerTempDuration +
                                                Duration(
                                                    minutes: totalminutestoAdd);
                                        isShowFirstRechargeDialog = false;
                                        isShowLastRechargeDialog = false;
                                      });
                                      if (FlutterChatScreen.timer != null) {
                                        FlutterChatScreen.timer
                                            .cancel(); // Stop the current timer
                                      }
                                      _startTimer(centerTempDuration);
                                    },
                                  );
                                }
                              }
                            },
                          );
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.green),
                              borderRadius: BorderRadius.circular(8)),
                          child: Text(
                            'Recharge',
                            style: GoogleFonts.poppins(color: Colors.green),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
          ],
        ),
      ),
      body: Stack(fit: StackFit.expand, children: [
        Image.asset(
          'asset/images/rectangle.png',
          fit: BoxFit.cover, // Cover the entire screen
        ),
        Chat(
          messages: _messages,
          onSendPressed: _handleSendPressed,
          onPreviewDataFetched: _handlePreviewDataFetched,
          slidableMessageBuilder: (p0, msgWidget) {
            return SwipeTo(
              child: msgWidget,
              iconOnLeftSwipe: Icons.arrow_forward,
              iconOnRightSwipe: Icons.arrow_back,
              onRightSwipe: (details) {
                // if (isDelivered) {
                var index =
                    _messages.indexWhere((element) => element.id == p0.id);

                TLoggerHelper.debug(index.toString() + "  " + p0.id);
                if (_messages[index].type == types.MessageType.text &&
                    (_messages[index].status == types.Status.delivered ||
                        _messages[index].status == types.Status.seen)) {
                  types.TextMessage textMessage =
                      _messages[index] as types.TextMessage;
                  TLoggerHelper.info(p0.id);
                  setState(() {
                    // replyingTextMap = {p0.id: textMessage.text};
                    // replyingImageUrlMap = {};
                    referenceId = p0.id;
                    messageToSend = textMessage.text;
                    imageUrlToSend = "";
                  });
                } else if (_messages[index].type == types.MessageType.image &&
                    (_messages[index].status == types.Status.delivered ||
                        _messages[index].status == types.Status.seen)) {
                  types.ImageMessage imageMessage =
                      _messages[index] as types.ImageMessage;
                  TLoggerHelper.info(p0.id);
                  setState(() {
                    // replyingImageUrlMap = {p0.id: imageMessage.uri};
                    // replyingTextMap = {};
                    referenceId = p0.id;
                    messageToSend = "";
                    imageUrlToSend = imageMessage.uri;
                  });
                }
                // }
                // messageTextController.text = ;
              },
              swipeSensitivity: 5,
              offsetDx: 0.1,
            );
          },
          onEndReached: _loadMessages,
          showUserAvatars: true,
          textMessageBuilder: (p0, {required messageWidth, required showName}) {
            final message;
            dynamic repliedMessage;
            if (p0.remoteId != null) {
              message =
                  _messages.firstWhere((element) => p0.remoteId == element.id);

              if (_messages
                      .firstWhere((element) => p0.remoteId == element.id)
                      .type
                      .name ==
                  'text') {
                repliedMessage = message as types.TextMessage;
                return TextRepliedMessageWidget(
                  myColor:
                      p0.author == user ? Colors.white : Colors.amber.shade50,
                  messageText: p0.text,
                  widget: widget,
                  repliedMessage: repliedMessage.text,
                  repliedImageUrl: "",
                );
              } else {
                repliedMessage = message as types.ImageMessage;
                return TextRepliedMessageWidget(
                  myColor:
                      p0.author == user ? Colors.white : Colors.amber.shade50,
                  messageText: p0.text,
                  widget: widget,
                  repliedMessage: "",
                  repliedImageUrl: repliedMessage.uri,
                );
              }
            } else {
              return Container(
                padding: const EdgeInsets.all(10),
                color: p0.author == user ? Colors.white : Colors.amber.shade50,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (p0.author.firstName != null)
                      Text(
                        p0.author.firstName.toString(),
                        style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.blueGrey),
                      ),
                    Text(
                      p0.text,
                      style: GoogleFonts.poppins(
                          fontSize: 14,
                          color: p0.metadata!['message_type'] == "bot"
                              ? Colors.red
                              : null),
                    ),
                  ],
                ),
              );
            }
          },
          typingIndicatorOptions:
              TypingIndicatorOptions(typingUsers: typingList),
          theme: DefaultChatTheme(
              messageBorderRadius: 12,
              typingIndicatorTheme: TypingIndicatorTheme(
                  animatedCirclesColor: Colors.blueAccent,
                  animatedCircleSize: 6,
                  bubbleBorder: BorderRadius.zero,
                  bubbleColor: Colors.transparent,
                  countAvatarColor: Colors.white,
                  countTextColor: Colors.white,
                  multipleUserTextStyle: TextStyle()),
              backgroundColor: Colors.transparent,
              seenIcon: const Icon(
                Icons.done_all,
                color: Colors.blue,
                size: 18,
              ),
              deliveredIcon: const Icon(
                Icons.done_all,
                color: Colors.black,
                size: 18,
              ),
              errorIcon: const Icon(
                Icons.error,
                color: Colors.red,
              ),
              sendingIcon: const Icon(Icons.timelapse_rounded,
                  color: Colors.black, size: 18),
              inputBackgroundColor: Colors.grey.shade100,
              attachmentButtonIcon: const Icon(Icons.attachment),
              inputTextStyle: GoogleFonts.poppins(color: Colors.black),
              inputTextColor: Colors.black,
              inputBorderRadius: BorderRadius.zero),
          inputOptions: InputOptions(
              textEditingController: FlutterChatScreen.messageTextController,
              enabled: !isToViewLocal),
          customBottomWidget: isToViewLocal
              ? Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: List.generate(
                              5,
                              (index) => Icon(
                                Icons.star,
                                color: index < widget.initialRating
                                    ? Colors.amber
                                    : Colors.grey,
                              ),
                            ),
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (BuildContext context) =>
                                      _feedbackDialog(context),
                                );
                              },
                              child: Icon(Icons.edit))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(8),
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8)),
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(6),
                            decoration: BoxDecoration(
                                color: AppColor.appColor.withOpacity(.3),
                                borderRadius: BorderRadius.circular(8)),
                            child: Row(
                              children: [
                                CircleAvatar(
                                  radius: 28,
                                  backgroundImage: CachedNetworkImageProvider(
                                      widget.astrologerImage),
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Expanded(
                                  child: Text(
                                    "Hi".tr() +
                                        " ${widget.astrologerName}, " +
                                        "Let's Continue this Chat".tr(),
                                    style: GoogleFonts.poppins(fontSize: 16),
                                    maxLines: 2,
                                  ),
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              if (isToViewLocal) {
                                context.pushNamed(
                                  RouteConstants.astrologerProfile,
                                  queryParameters: {
                                    'astrologerId': widget.toId,
                                    'type': 'chat',
                                  },
                                );
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.all(12),
                              margin: EdgeInsets.only(top: 12),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: AppColor.appColor,
                                  borderRadius: BorderRadius.circular(8)),
                              alignment: Alignment.center,
                              child: Text(
                                "Yes, Continue this Chat".tr(),
                                style: GoogleFonts.poppins(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                )
              : Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (imageUrlToSend.isNotEmpty || messageToSend.isNotEmpty)
                      IntrinsicHeight(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey.shade100,
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          width: double.infinity,
                          padding:
                              const EdgeInsets.only(top: 8, right: 8, left: 8),
                          child: Container(
                            width: MediaQuery.of(context).size.width * .9,
                            decoration: BoxDecoration(
                                color: Colors.white70,
                                borderRadius: BorderRadius.circular(6)),
                            padding: const EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      color: Colors.amber),
                                  width: 4,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            widget.astrologerName,
                                            style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black87),
                                          ),
                                          GestureDetector(
                                              onTap: () => setState(() {
                                                    referenceId = null;
                                                    messageToSend = "";
                                                    imageUrlToSend = "";
                                                  }),
                                              child: const Icon(Icons.close,
                                                  color: Colors.grey))
                                        ],
                                      ),
                                      const SizedBox(height: 8),
                                      if (messageToSend.isNotEmpty)
                                        Text(
                                          messageToSend,
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.start,
                                          style: GoogleFonts.poppins(
                                              color: Colors.grey),
                                        ),
                                      if (imageUrlToSend.isNotEmpty)
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .3,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .08,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image:
                                                      CachedNetworkImageProvider(
                                                          imageUrlToSend))),
                                        )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    // Input(
                    //   onSendPressed: _handleSendPressed,
                    //   onAttachmentPressed: _handleAttachmentPressed,
                    //   options: InputOptions(
                    //     textEditingController: messageTextController,
                    //   ),
                    // ),
                    Column(
                      children: [
                        if (isShowFirstRechargeDialog)
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            margin: EdgeInsets.symmetric(
                                horizontal: 12, vertical: 8),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8)),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: MediaQuery.of(context).size.width * .7,
                                  child: Text(
                                      "Would you like to continue your price at normal price of ₹ ${widget.chat_price}/min after this offer ends?"),
                                ),
                                Spacer(),
                                InkWell(
                                  onTap: () async {
                                    num initialWalletAmount =
                                        await THelperFunctions
                                            .getWalletAmount();
                                    await _rechargeDialog(context, "wallet")
                                        .whenComplete(
                                      () async {
                                        num amount = await THelperFunctions
                                                .getWalletAmount() -
                                            initialWalletAmount;
                                        if (amount > widget.chat_price) {
                                          if (amount < widget.chat_price) {
                                            showSnackBarWidget(context,
                                                "You won't have enought recharge as of now!!");
                                          } else {
                                            int totalminutestoAdd =
                                                amount ~/ (widget.chat_price);
                                            TLoggerHelper.info(
                                                "The Recharge amount is :- $amount and total time to add is $totalminutestoAdd");
                                            await Provider.of<ChattingProvider>(
                                                    context,
                                                    listen: false)
                                                .sendChatDurationResume(
                                                    communicationId:
                                                        widget.cummID,
                                                    chatDuration:
                                                        totalminutestoAdd
                                                            .toString(),
                                                    isResume: true)
                                                .whenComplete(
                                              () {
                                                setState(() {
                                                  centerTempDuration =
                                                      centerTempDuration +
                                                          Duration(
                                                              minutes:
                                                                  totalminutestoAdd);
                                                  isShowFirstRechargeDialog =
                                                      false;
                                                  isShowLastRechargeDialog =
                                                      false;
                                                });
                                                if (FlutterChatScreen.timer !=
                                                    null) {
                                                  FlutterChatScreen.timer
                                                      .cancel(); // Stop the current timer
                                                }
                                                _startTimer(centerTempDuration);
                                              },
                                            );
                                          }
                                        }
                                      },
                                    );
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 18, vertical: 8),
                                    decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Text(
                                      "Yes",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        // Container(
                        //   width: MediaQuery.of(context).size.width,
                        //   height: MediaQuery.of(context).size.height * .08,
                        //   padding: EdgeInsets.symmetric(horizontal: 12),
                        //   color: Colors.white,
                        //   child: Column(
                        //     children: [
                        //       Row(
                        //         children: [
                        //           GestureDetector(
                        //             onTap: () {
                        //               _handleAttachmentPressed();
                        //             },
                        //             child: Icon(Icons.attachment_rounded,
                        //                 size: 28),
                        //           ),
                        //           SizedBox(
                        //             width: 12,
                        //           ),
                        //           GestureDetector(
                        //             onTap: () {
                        // TLoggerHelper.info(
                        //     widget.dob + "     " + widget.tob);
                        // Navigator.of(context).push(
                        //     MaterialPageRoute(
                        //         builder: (context) =>
                        //             ListenableProvider(
                        //               create: (context) =>
                        //                   KundliProvider(),
                        //               child: ViewKundliScreen(
                        //                   dob: DateFormat(
                        //                           'dd/MM/yyyy')
                        //                       .format(DateTime
                        //                           .parse(widget
                        //                               .dob)),
                        //                   tob: TFormatter
                        //                       .convertTo24HourFormat(
                        //                           widget.tob),
                        //                   lat: widget.lat,
                        //                   pob: "",
                        //                   userName: widget
                        //                       .astrologerName,
                        //                   lon: widget.lon),
                        //             )));
                        //             },
                        //             child: Image.asset(
                        //               "asset/images/kundli_icon.png",
                        //               height: 32,
                        //             ),
                        //           ),
                        //           Expanded(
                        //             child: Padding(
                        //               padding:
                        //                   EdgeInsets.symmetric(horizontal: 8),
                        //               child: TextField(
                        //                 controller: FlutterChatScreen
                        //                     .messageTextController,
                        //                 maxLines: 5,
                        //                 minLines: 1,
                        //                 decoration: InputDecoration(
                        //                   border: OutlineInputBorder(
                        //                     borderSide: BorderSide.none,
                        //                   ),
                        //                 ),
                        //                 onChanged: (value) {
                        //                   if (value.length == 1) {
                        // THelperFunctions.showTyping(
                        //     astrologerId: widget.toId,
                        //     status: true);
                        //                   }
                        //                 },
                        //                 textCapitalization:
                        //                     TextCapitalization.sentences,
                        //               ),
                        //             ),
                        //           ),
                        //           GestureDetector(
                        //               onTap: () {
                        //                 _handleSendPressed(types.PartialText(
                        //                     text: FlutterChatScreen
                        //                         .messageTextController.text));
                        //               },
                        //               child: Icon(Icons.send))
                        //         ],
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        Container(
                          constraints: BoxConstraints(
                            minHeight: MediaQuery.of(context).size.height *
                                0.12, // Default min height
                            maxHeight: MediaQuery.of(context).size.height *
                                0.25, // Set a maximum height for expansion
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize
                                .min, // Use min to take only necessary space
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                width: MediaQuery.of(context).size.width,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(28),
                                        ),
                                        child: TextField(
                                          controller: FlutterChatScreen
                                              .messageTextController,
                                          minLines: 1, // Starts with 1 line
                                          maxLines: 5, // Can grow up to 5 lines
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                              borderSide: BorderSide.none,
                                            ),
                                            hintText:
                                                "Type Message".tr() + " ...",
                                            hintStyle: TextStyle(
                                                color: Colors.black45),
                                          ),
                                          onChanged: (value) {
                                            if (value.length == 1) {
                                              THelperFunctions.showTyping(
                                                astrologerId: widget.toId,
                                                status: true,
                                              );
                                            }
                                          },
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    GestureDetector(
                                      onTap: () {
                                        _handleSendPressed(types.PartialText(
                                          text: FlutterChatScreen
                                              .messageTextController!.text,
                                        ));
                                      },
                                      child: CircleAvatar(
                                        radius: 24,
                                        child: Icon(Icons.send),
                                        backgroundColor: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 12),
                                height: MediaQuery.of(context).size.height *
                                    0.05, // Set a fixed height for this row
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        _handleImageSelection(
                                            imageSource: ImageSource.camera);
                                      },
                                      child: Icon(Icons.camera_alt, size: 24),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        _handleImageSelection(
                                            imageSource: ImageSource.gallery);
                                      },
                                      child: Icon(Icons.photo_album, size: 24),
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        TLoggerHelper.info(
                                            widget.dob + "     " + widget.tob);
                                        bool isEnglish = await THelperFunctions
                                                .getLocalizationCode() ==
                                            'en';
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                ListenableProvider(
                                                    create: (context) =>
                                                        KundliProviderNew(),
                                                    child: KundliTesting(
                                                        isEnglish: isEnglish,
                                                        lat: widget.lat,
                                                        long: widget.lon,
                                                        dateTime:
                                                            THelperFunctions
                                                                .parseDobAndTime(
                                                                    widget.dob,
                                                                    widget
                                                                        .tob))),
                                          ),
                                        );
                                      },
                                      child: Image.asset(
                                        "asset/images/kundli_icon.png",
                                        height: 32,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
          onMessageLongPress: (context, p1) {
            types.TextMessage message =
                types.TextMessage(author: p1.author, id: p1.id, text: "");
            if (p1.type == types.MessageType.text) {
              message = p1 as types.TextMessage;
            }
            FlutterClipboard.copy(message.text).whenComplete(() {
              Fluttertoast.showToast(msg: "Text Copied Successfully !!");
            });
          },
          imageMessageBuilder: (p0, {required messageWidth}) {
            final message;
            dynamic repliedMessage;
            if (p0.remoteId != null) {
              message =
                  _messages.firstWhere((element) => p0.remoteId == element.id);

              if (_messages
                      .firstWhere((element) => p0.remoteId == element.id)
                      .type
                      .name ==
                  'image') {
                repliedMessage = message as types.ImageMessage;
                return ImageRepliedMessageWidget(
                  imageUrl: p0.uri,
                  widget: widget,
                  repliedMessage: "",
                  repliedImageUrl: repliedMessage.uri,
                );
              } else {
                repliedMessage = message as types.TextMessage;
                return ImageRepliedMessageWidget(
                  imageUrl: p0.uri,
                  widget: widget,
                  repliedMessage: repliedMessage.text,
                  repliedImageUrl: "",
                );
              }
            } else {
              return Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (p0.author.firstName != null)
                      Text(
                        p0.author.firstName.toString(),
                        style: GoogleFonts.poppins(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueGrey,
                        ),
                      ),
                    SizedBox(
                      child: Container(
                        width: messageWidth.toDouble(),
                        constraints: BoxConstraints(
                            maxHeight: MediaQuery.of(context).size.height * .5),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: p0.uri.contains(
                                "https://admin.jyotishrahsya.com/storage/attachments")
                            ? CachedNetworkImage(
                                imageUrl: p0.uri,
                                fit: BoxFit.fitWidth,
                              )
                            : Image.file(file!),
                      ),
                    )
                  ],
                ),
              );
            }
          },
          showUserNames: true,
          user: user,
        ),
      ]),
    );
  }

  _feedbackDialog(BuildContext context) {
    TLoggerHelper.debug("Feedback Function getting called !!!");
    TextEditingController _feedBckController = TextEditingController();
    _feedBckController.text = widget.initialReview;
    final _formKey = GlobalKey<FormState>();
    double? rating = widget.initialRating.toDouble();

    return ListenableProvider(
      create: (context) => ListOfApisProvider(),
      child: StatefulBuilder(
        builder: (dialogContext, StateSetter setState) {
          return Dialog(
            backgroundColor: AppColor.whiteColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                        onTap: () {
                          if (Navigator.of(dialogContext).canPop()) {
                            Navigator.of(dialogContext).pop();
                          }
                        },
                        child: Icon(Icons.close),
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      "How was your overall experience with Jyotish Ji?".tr(),
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 12),
                    RatingBar.builder(
                      initialRating: widget.initialRating.toDouble(),
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 32,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        size: 8,
                        color: AppColor.appColor,
                      ),
                      onRatingUpdate: (newRating) {
                        setState(() {
                          rating = newRating;
                        });
                      },
                    ),
                    SizedBox(height: 12),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Share your feedback".tr(),
                        style: GoogleFonts.poppins(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      cursorColor: AppColor.borderColor,
                      maxLines: 5,
                      keyboardType: TextInputType.text,
                      style: GoogleFonts.poppins(color: Colors.black87),
                      decoration: InputDecoration(
                        hintText: "Add Feedback",
                        hintStyle: GoogleFonts.poppins(color: Colors.grey),
                        border: InputBorder.none,
                        fillColor: AppColor.whiteColor,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              BorderSide(width: 1, color: AppColor.borderColor),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              BorderSide(width: 1, color: AppColor.borderColor),
                        ),
                      ),
                      controller: _feedBckController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your feedback';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    SizedBox(
                      height: 50,
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          backgroundColor: AppColor.appColor,
                        ),
                        onPressed: () {
                          TLoggerHelper.debug("Holding Feedback functions");
                          if (_formKey.currentState!.validate()) {
                            TLoggerHelper.debug("Running Feedback functions");
                            Provider.of<ListOfApisProvider>(dialogContext,
                                    listen: false)
                                .sendReviewProvider(
                              astrologer_id: widget.toId,
                              customer_id: widget.fromId,
                              description: _feedBckController.text,
                              rate: rating!.toInt().toString(),
                              isLike: '',
                              reviewId: widget.reviewId,
                              type: 'chat',
                              infoId: widget.infoId,
                              orderID: widget.orderID,
                            )
                                .then((val) {
                              if (val!.status!) {
                                Navigator.of(context)
                                    .push(
                                  MaterialPageRoute(
                                    builder: (context) => MultiProvider(
                                      providers: [
                                        ListenableProvider(
                                            create: (context) =>
                                                ChattingProvider()),
                                        ListenableProvider(
                                            create: (context) =>
                                                ListOfApisProvider()),
                                      ],
                                      child: FlutterChatScreen(
                                        orderID: widget.orderID,
                                        isToView: true,
                                        cummID: widget.cummID,
                                        chat_price: widget.chat_price,
                                        fromId: widget.fromId,
                                        toId: widget.toId,
                                        timerDuration: widget.timerDuration,
                                        astrologerName: widget.astrologerName,
                                        astrologerImage: widget.astrologerImage,
                                        isFreeChat: widget.isFreeChat,
                                        dob: widget.dob,
                                        tob: widget.tob,
                                        messageInfo: widget.messageInfo,
                                        infoId: widget.infoId,
                                        lat: widget.lat,
                                        lon: widget.lon,
                                      ),
                                    ),
                                  ),
                                )
                                    .then((_) {
                                  if (Navigator.of(dialogContext).canPop()) {
                                    Navigator.of(dialogContext).pop();
                                  }
                                  if (Navigator.of(dialogContext).canPop()) {
                                    Navigator.of(dialogContext).pop();
                                  }
                                  if (Navigator.of(dialogContext).canPop()) {
                                    Navigator.of(dialogContext).pop();
                                  }
                                });
                              }
                            });
                          }
                        },
                        child: Text(
                          "Submit".tr(),
                          style: GoogleFonts.poppins(color: Colors.black),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  // _feedbackDialog(BuildContext context) {
  //   TLoggerHelper.debug("Feedback Function getting called !!!");
  //   TextEditingController _feedBckController = TextEditingController();
  //   _feedBckController.text = widget.initialReview;
  //   final _formKey = GlobalKey<FormState>();
  //   double? rating;
  //   rating = widget.initialRating.toDouble();

  //   return ListenableProvider(
  //     create: (context) => ListOfApisProvider(),
  //     child: StatefulBuilder(builder: (dialogContext, StateSetter setState) {
  //       return Form(
  //         key: _formKey,
  //         child: AlertDialog(
  //           icon: GestureDetector(
  //             onTap: () {
  //               //   if (Navigator.of(dialogContext).canPop()) {
  //               //     Navigator.of(dialogContext).pop();
  //               //   }
  //               //   if (Navigator.of(dialogContext).canPop()) {
  //               //     Navigator.of(dialogContext).pop();
  //               //   }
  //               if (Navigator.of(dialogContext).canPop()) {
  //                 Navigator.of(dialogContext).pop();
  //               }
  //             },
  //             child: Align(
  //                 alignment: Alignment.topRight, child: Icon(Icons.close)),
  //           ),
  //           backgroundColor: AppColor.whiteColor,
  //           title:
  //               Text("How was your overall experience with Jyotish Ji?".tr()),
  //           content: Column(
  //             mainAxisSize: MainAxisSize.min,
  //             children: <Widget>[
  //               RatingBar.builder(
  //                 initialRating: widget.initialRating.toDouble(),
  //                 minRating: 1,
  //                 direction: Axis.horizontal,
  //                 allowHalfRating: true,
  //                 itemCount: 5,
  //                 itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
  //                 itemBuilder: (context, _) => Icon(
  //                   Icons.star,
  //                   color: AppColor.appColor,
  //                 ),
  //                 onRatingUpdate: (newRating) {
  //                   setState(() {
  //                     rating = newRating;
  //                   });
  //                 },
  //               ),
  //               SizedBox(height: 20),
  //               Align(
  //                 alignment: Alignment.centerLeft,
  //                 child: Text(
  //                   "Share your feedback".tr(),
  //                   style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
  //                 ),
  //               ),
  //               SizedBox(height: 10),
  //               TextFormField(
  //                 cursorColor: AppColor.borderColor,
  //                 maxLines: 5,
  //                 keyboardType: TextInputType.text,
  //                 decoration: InputDecoration(
  //                   border: InputBorder.none,
  //                   fillColor: AppColor.whiteColor,
  //                   focusedBorder: OutlineInputBorder(
  //                     borderRadius: BorderRadius.circular(10),
  //                     borderSide:
  //                         BorderSide(width: 1, color: AppColor.borderColor),
  //                   ),
  //                   enabledBorder: OutlineInputBorder(
  //                     borderRadius: BorderRadius.circular(10),
  //                     borderSide:
  //                         BorderSide(width: 1, color: AppColor.borderColor),
  //                   ),
  //                 ),
  //                 controller: _feedBckController,
  //                 validator: (value) {
  //                   if (value == null || value.isEmpty) {
  //                     return 'Please enter your feedback';
  //                   }
  //                   return null;
  //                 },
  //               ),
  //               SizedBox(height: 10),
  //               SizedBox(
  //                 height: 50,
  //                 width: 400,
  //                 child: ElevatedButton(
  //                   style: ButtonStyle(
  //                     shape: WidgetStatePropertyAll<RoundedRectangleBorder>(
  //                       RoundedRectangleBorder(
  //                         borderRadius: BorderRadius.circular(10),
  //                       ),
  //                     ),
  //                     backgroundColor:
  //                         WidgetStatePropertyAll<Color>(AppColor.appColor),
  //                   ),
  //                   onPressed: () {
  //                     TLoggerHelper.debug("Holding Feedback functions");
  //                     if (_formKey.currentState!.validate()) {
  //                       TLoggerHelper.debug("Running Feedback functions");
  //                       Provider.of<ListOfApisProvider>(dialogContext,
  //                               listen: false)
  //                           .sendReviewProvider(
  //                               astrologer_id: widget.toId,
  //                               customer_id: widget.fromId,
  //                               description: _feedBckController.text,
  //                               rate: rating!.toInt().toString(),
  //                               isLike: '',
  //                               reviewId: widget.reviewId,
  //                               type: 'chat',
  //                               infoId: widget.infoId,
  //                               orderID: widget.orderID)
  //                           .then((val) {
  //                         if (val!.status!) {
  //                           Navigator.of(context)
  //                               .push(MaterialPageRoute(
  //                                   builder: (context) => MultiProvider(
  //                                         providers: [
  //                                           ListenableProvider(
  //                                               create: (context) =>
  //                                                   ChattingProvider()),
  //                                           ListenableProvider(
  //                                               create: (context) =>
  //                                                   ListOfApisProvider()),
  //                                         ],
  //                                         child: FlutterChatScreen(
  //                                             orderID: widget.orderID,
  //                                             isToView: true,
  //                                             cummID: widget.cummID,
  //                                             chat_price: widget.chat_price,
  //                                             fromId: widget.fromId,
  //                                             toId: widget.toId,
  //                                             timerDuration:
  //                                                 widget.timerDuration,
  //                                             astrologerName:
  //                                                 widget.astrologerName,
  //                                             astrologerImage:
  //                                                 widget.astrologerImage,
  //                                             isFreeChat: widget.isFreeChat,
  //                                             dob: widget.dob,
  //                                             tob: widget.tob,
  //                                             messageInfo: widget.messageInfo,
  //                                             infoId: widget.infoId,
  //                                             lat: widget.lat,
  //                                             lon: widget.lon),
  //                                       )))
  //                               .then((_) {
  //                             if (Navigator.of(dialogContext).canPop()) {
  //                               Navigator.of(dialogContext).pop();
  //                             }
  //                             if (Navigator.of(dialogContext).canPop()) {
  //                               Navigator.of(dialogContext).pop();
  //                             }
  //                             if (Navigator.of(dialogContext).canPop()) {
  //                               Navigator.of(dialogContext).pop();
  //                             }
  //                           });
  //                         }
  //                       });
  //                     }
  //                   },
  //                   child: Text(
  //                     "Submit".tr(),
  //                     style: TextStyle(color: Colors.black),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //       );
  //     }),
  //   );
  // }

  // Future<bool?> _continueDialog(BuildContext context) async {
  //   return await showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
  //         titlePadding:
  //             const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
  //         contentPadding:
  //             const EdgeInsets.symmetric(horizontal: 18, vertical: 24),
  //         title: Text("Are you want to continue the chat with jyotish",
  //             textAlign: TextAlign.center,
  //             style: TextStyle(
  //                 fontSize: 15,
  //                 color: AppColor.blackColor,
  //                 fontWeight: FontWeight.bold)),
  //         actions: [
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             children: [
  //               ElevatedButton(
  //                 style: ButtonStyle(
  //                     backgroundColor:
  //                         WidgetStateProperty.all(AppColor.whiteColor),
  //                     shape: WidgetStateProperty.all(RoundedRectangleBorder(
  //                         borderRadius: BorderRadius.circular(10)))),
  //                 onPressed: () async {
  //                   await Provider.of<ChattingProvider>(context, listen: false)
  //                       .resumeChat(
  //                           communication_id: widget.cummID, isResumed: false)
  //                       .then((val) async {
  //                     if (val) {
  //                       showSnackBarWidget(context, "Chat Completed!!",
  //                           isTrue: true);
  //                       await showDialog(
  //                           barrierDismissible: false,
  //                           context: context,
  //                           builder: (BuildContext context) =>
  //                               _feedbackDialog(context));
  //                     }
  //                   }).whenComplete(() {
  //                     Navigator.pop(context);
  //                     Provider.of<ChattingProvider>(context, listen: false)
  //                         .endChatByAstrologer(communicationId: widget.cummID);
  //                   });
  //                 },
  //                 child: Text('Cancel',
  //                     style: const TextStyle(
  //                         fontWeight: FontWeight.bold,
  //                         color: AppColor.blackColor)),
  //               ),
  //               ElevatedButton(
  //                 style: ButtonStyle(
  //                     backgroundColor:
  //                         WidgetStateProperty.all(AppColor.appColor),
  //                     shape: WidgetStateProperty.all(RoundedRectangleBorder(
  //                         borderRadius: BorderRadius.circular(10)))),
  //                 onPressed: () async {
  //                   await Provider.of<ChattingProvider>(context, listen: false)
  //                       .resumeChat(
  //                           communication_id: widget.cummID, isResumed: true)
  //                       .then((val) {
  //                     if (val) {
  //                       Navigator.of(context).pop();
  //                       if (widget.isFreeChat) {
  //                         setState(() {
  //                           widget.isFreeChat = false;
  //                         });
  //                         FlutterChatScreen.timer.cancel();
  //                         _startTimer();
  //                       } else {
  //                         setState(() {
  //                           walletAmount = (int.parse(walletAmount) -
  //                                   (widget.chat_price * 5).toInt())
  //                               .toString();

  //                           // TLoggerHelper.debug(
  //                           //     "Continue Count Time: ${(widget.chat_price * 5).toInt().toString()}");
  //                         });
  //                         FlutterChatScreen.timer.cancel();
  //                         _startTimer();
  //                       }
  //                     }
  //                   });
  //                 },
  //                 child: Text(
  //                   'Yes',
  //                   style: const TextStyle(
  //                       fontWeight: FontWeight.bold,
  //                       color: AppColor.blackColor),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;

      print("Continue Chat-> $customerID");
      getWalletAmount();
    }
    setState(() {});
  }

  void getWalletAmount() async {
    client = DioClient();
    Dio dio = await client!.getClient();

    var params = jsonEncode({'user_id': customerID});
    String api = UrlConstants.walletTranscation;

    WalletTransactionModel? walletTransactionModel =
        await client?.walletTransaction(dio, api, params);

    if (walletTransactionModel!.status == true) {
      setState(() {
        walletAmount = walletTransactionModel.walletList!.length > 0
            ? "${walletTransactionModel.walletList?.first.balance != null ? walletTransactionModel.walletList?.first.balance : "0"}"
            : "0";
        print("walletAmount----> $walletAmount");
      });
    }
  }

  /********************* Recharge Dialog ************************/
  Future<void> _rechargeDialog(BuildContext context, String s) async {
    int? amID = 0;
    String? amount = "";
    // context
    //     .read<ListOfApisProvider>()
    //     .getRechargeAmountProvider(context: context);
    await showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return ListenableProvider(
          create: (context) => ListOfApisProvider(),
          child: Consumer<ListOfApisProvider>(builder: (context, provider, _) {
            return FutureBuilder(
                future: provider.getRechargeAmountPProvider(context: context),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CustomCircularProgressIndicator(),
                    );
                  } else if (snapshot.hasData) {
                    RechargeAmountModel rechargeAmountModel = snapshot.data;
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topRight,
                              child: CloseButton()),
                          Text(
                            "Minimum balance of 5 minutes".tr() +
                                " is required to start chat with".tr() +
                                " (${widget.astrologerName})",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 14, color: Colors.redAccent),
                          ),
                          SizedBox(height: 10),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Recharge Now'.tr(),
                              style: TextStyle(fontSize: 17),
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.light_mode_rounded,
                                color: AppColor.appColor,
                              ),
                              Text("Tip: 90% user recharge for 10 mins or more"
                                  .tr())
                            ],
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: rechargeAmountModel.recharge!.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4,
                                        childAspectRatio: 6 / 2,
                                        mainAxisSpacing: 10,
                                        crossAxisSpacing: 10),
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(
                                          SlideRightRoute(
                                              page: ListenableProvider(
                                                  create: (context) =>
                                                      ListOfApisProvider(),
                                                  child:
                                                      PaymentInformationScreen(
                                                    userInfoId: widget.infoId,
                                                    title:
                                                        "Recharge Amount".tr(),
                                                    astroName:
                                                        widget.astrologerName,
                                                    price: rechargeAmountModel
                                                        .recharge![index]
                                                        .price!,
                                                    amountID:
                                                        rechargeAmountModel
                                                            .recharge![index]
                                                            .id!,
                                                    from: "wallet",
                                                    astroId: widget.toId,
                                                  ))));

                                      print(rechargeAmountModel
                                          .recharge![index].price);
                                      amID = rechargeAmountModel
                                          .recharge![index].id;
                                      amount = rechargeAmountModel
                                          .recharge![index].price;
                                      print(rechargeAmountModel
                                          .recharge![index].price);

                                      print("Amount ID --> ${amID}");
                                      print(
                                          "Amount ID --> ${amID.runtimeType}");
                                      print("Amount  --> $amount");
                                      print("click");
                                    },
                                    child: Container(
                                        height: 25,
                                        width: 90,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            border: Border.all(
                                                color: AppColor.appColor)),
                                        child: Stack(children: <Widget>[
                                          Positioned(
                                              top: 7,
                                              right: 50.5,
                                              child: Transform.rotate(
                                                  angle: -math.pi / 3.5,
                                                  child: Container(
                                                      height: 15,
                                                      width: 50,
                                                      decoration: BoxDecoration(
                                                          color: Colors.green,
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          5))),
                                                      child: Center(
                                                          child: Text(
                                                              "${rechargeAmountModel.recharge![index].discountPercentage}% Extra",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      8)))))),
                                          Center(
                                              child: Text(
                                                  "${rechargeAmountModel.recharge![index].price}"))
                                        ])),
                                  );
                                }),
                          ),
                          SizedBox(height: 20),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                backgroundColor: AppColor.appColor,
                                textStyle:
                                    Theme.of(context).textTheme.labelLarge,
                              ),
                              child: Text('Proceed to Pay'.tr()),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return SizedBox();
                  }
                });
          }),
        );
      },
    );
  }
}

class TextRepliedMessageWidget extends StatelessWidget {
  const TextRepliedMessageWidget(
      {super.key,
      required this.myColor,
      required this.messageText,
      required this.widget,
      required this.repliedImageUrl,
      required this.repliedMessage});
  final Color myColor;
  final String messageText;
  final FlutterChatScreen widget;
  final String repliedImageUrl;
  final String repliedMessage;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: myColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
            bottomLeft: Radius.circular(12),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IntrinsicHeight(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(6)),
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.amber),
                    width: 4,
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              widget.astrologerName,
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black87),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        if (repliedImageUrl.isNotEmpty)
                          Container(
                            width: MediaQuery.of(context).size.width * .3,
                            height: MediaQuery.of(context).size.height * .08,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: CachedNetworkImageProvider(
                                        repliedImageUrl))),
                          ),
                        if (repliedMessage.isNotEmpty)
                          Text(
                            repliedMessage,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            style: GoogleFonts.poppins(color: Colors.grey),
                          ),
                        // if (replyingImageUrlMap.isNotEmpty)
                        //   Container(
                        //     width:
                        //         MediaQuery.of(context).size.width * .3,
                        //     height: MediaQuery.of(context).size.height *
                        //         .08,
                        //     decoration: BoxDecoration(
                        //         borderRadius: BorderRadius.circular(8),
                        //         image: DecorationImage(
                        //             fit: BoxFit.cover,
                        //             image: CachedNetworkImageProvider(
                        //                 replyingImageUrlMap
                        //                     .entries.first.value))),
                        //   )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 8),
          Text(
            messageText,
            style: GoogleFonts.poppins(fontSize: 14),
          )
        ],
      ),
    );
  }
}

class ImageRepliedMessageWidget extends StatelessWidget {
  const ImageRepliedMessageWidget(
      {super.key,
      required this.imageUrl,
      required this.widget,
      required this.repliedMessage,
      required this.repliedImageUrl});

  final String imageUrl;
  final FlutterChatScreen widget;
  final String repliedMessage;
  final String repliedImageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
            bottomLeft: Radius.circular(12),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IntrinsicHeight(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(6)),
              padding: const EdgeInsets.all(8),
              child: InkWell(
                onTap: () {},
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: Colors.amber),
                      width: 4,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.astrologerName,
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black87),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          if (repliedImageUrl.isNotEmpty)
                            Container(
                              width: MediaQuery.of(context).size.width * .3,
                              height: MediaQuery.of(context).size.height * .08,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: CachedNetworkImageProvider(
                                          repliedImageUrl))),
                            ),
                          if (repliedMessage.isNotEmpty)
                            Text(
                              repliedMessage,
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.start,
                              style: GoogleFonts.poppins(color: Colors.grey),
                            ),
                          // if (replyingImageUrlMap.isNotEmpty)
                          //   Container(
                          //     width:
                          //         MediaQuery.of(context).size.width * .3,
                          //     height: MediaQuery.of(context).size.height *
                          //         .08,
                          //     decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(8),
                          //         image: DecorationImage(
                          //             fit: BoxFit.cover,
                          //             image: CachedNetworkImageProvider(
                          //                 replyingImageUrlMap
                          //                     .entries.first.value))),
                          //   )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 8),
          Container(
            height: MediaQuery.of(context).size.height * .3,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: CachedNetworkImageProvider(imageUrl))),
          )
        ],
      ),
    );
  }
}
