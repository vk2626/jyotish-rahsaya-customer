import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../../dialog/showLoaderDialog.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../Core/helper_functions.dart';
import '../../Core/formatter.dart';
import '../../Core/logger_helper.dart';
import '../../Core/Api/Constants.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import 'wallet.screen.dart';
import '../../utils/AppColor.dart';
import 'package:open_file_plus/open_file_plus.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Core/Api/ApiServices.dart';
import '../../Core/Model/PaymentLogModel.dart';
import '../../Core/Model/wallet.transection.model.dart';
import '../../CustomNavigator/SlideRightRoute.dart';
import 'package:path_provider/path_provider.dart';

class WalletTransactionScreen extends StatefulWidget {
  const WalletTransactionScreen({super.key});

  @override
  State<WalletTransactionScreen> createState() =>
      _WalletTransactionScreenState();
}

class _WalletTransactionScreenState extends State<WalletTransactionScreen>
    with SingleTickerProviderStateMixin {
  ValueNotifier downloadProgressNotifier = ValueNotifier(0);

  bool walletTransaction = true;
  num totalBalance = 0;
  int customerID = 0;
  List<WalletList> walletData = [];

  @override
  void initState() {
    super.initState();

    context
        .read<ListOfApisProvider>()
        .walletTransactionProvider(userID: customerID.toString());
    THelperFunctions.getWalletAmount().then((value) {
      setState(() {
        totalBalance = value;
      });
    });
    print("walletTransaction = false");
    THelperFunctions.logScreenNameEvent(screenName: "wallet_payments_history");
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Consumer<ListOfApisProvider>(builder: (context, walletState, _) {
            /*if (walletState.isLoading) {
          return Center(
            child: CustomCircularProgressIndicator(),
          );
        }*/
            walletData = walletState.getWalletTransaction.walletList ?? [];

            return Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(color: Colors.red),
                    child: Text(
                      "If the amount is deducted, it will reflect in your account within 24 hours. For issues, please contact support."
                          .tr(),
                      style: GoogleFonts.poppins(color: Colors.white),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                              text: "Available Balance".tr() + "\n",
                              style:
                                  GoogleFonts.poppins(color: AppColor.darkGrey),
                              children: [
                                TextSpan(
                                    text: totalBalance.toString(),
                                    style: GoogleFonts.poppins(fontSize: 30))
                              ]),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: SizedBox(
                            height: 30,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                            side: const BorderSide(
                                                color: AppColor.appColor))),
                                    backgroundColor:
                                        const MaterialStatePropertyAll<Color>(
                                            AppColor.appColor)),
                                onPressed: () {
                                  Navigator.of(context)
                                      .push(SlideRightRoute(
                                          page: ListenableProvider(
                                              create: (context) =>
                                                  ListOfApisProvider(),
                                              child: WalletScreen())))
                                      .whenComplete(
                                        () => THelperFunctions.getWalletAmount()
                                            .then(
                                          (value) {
                                            setState(() {
                                              totalBalance = value;
                                            });
                                          },
                                        ),
                                      );
                                },
                                child: Text("Recharge".tr(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.black))),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: SizedBox(
                          height: 40,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          side: BorderSide(
                                              color: walletTransaction == false
                                                  ? AppColor.borderColor
                                                  : AppColor.appColor))),
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                          walletTransaction == false
                                              ? AppColor.ltGrey
                                              : AppColor.appColor)),
                              onPressed: () {
                                setState(() {
                                  walletTransaction = true;
                                });
                              },
                              child: Text("Wallet Transaction".tr(),
                                  style: GoogleFonts.poppins(color: Colors.black))),
                        ),
                      )),
                      SizedBox(width: 10),
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: SizedBox(
                          height: 40,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          side: BorderSide(
                                              color: walletTransaction == true
                                                  ? AppColor.borderColor
                                                  : AppColor.appColor))),
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                          walletTransaction == true
                                              ? AppColor.ltGrey
                                              : AppColor.appColor)),
                              onPressed: () {
                                setState(() {
                                  walletTransaction = false;
                                  getPaymentLogs();
                                });
                              },
                              child: Text("Payment Logs".tr(),
                                  style: GoogleFonts.poppins(color: Colors.black))),
                        ),
                      )),
                    ],
                  ),
                  /*Wallet Transaction Section*/
                  Visibility(
                    visible: walletTransaction == true ? true : false,
                    child: Expanded(
                      child: RefreshIndicator(
                        color: Colors.white,
                        backgroundColor: AppColor.appColor,
                        onRefresh: () async {
                          context
                              .read<ListOfApisProvider>()
                              .walletTransactionProvider(
                                  userID: customerID.toString());
                          THelperFunctions.getWalletAmount().then((value) {
                            setState(() {
                              totalBalance = value;
                            });
                          });
                        },
                        child: SingleChildScrollView(
                          physics: AlwaysScrollableScrollPhysics(),
                          child: Column(children: [
                            ...List.generate(walletData.length, (index) {
                              return Card(
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              TFormatter.capitalizeSentence(
                                                  walletData[index]
                                                      .remark
                                                      .toString()),
                                              maxLines: 3,
                                              style: GoogleFonts.poppins(
                                                color: AppColor.darkGrey,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                            Text(
                                              TFormatter.formatDateTime(
                                                  walletData[index]
                                                      .createdAt!
                                                      .toLocal()),
                                              style: GoogleFonts.poppins(
                                                color: AppColor.darkGrey,
                                                fontSize: 12,
                                              ),
                                            ),
                                            SizedBox(height: 10),
                                            Wrap(
                                              crossAxisAlignment:
                                                  WrapCrossAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  walletData[index].paymentId !=
                                                          null
                                                      ? "#${walletData[index].paymentId}"
                                                      : "",
                                                  maxLines: 2,
                                                ),
                                                InkWell(
                                                  onTap: () {},
                                                  child: walletData[index]
                                                              .paymentId !=
                                                          null
                                                      ? Icon(Icons.copy,
                                                          size: 12)
                                                      : Text(""),
                                                ),
                                                SizedBox(width: 5),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            "${"credit" == walletData[index].paymentType ? "+" : "-"} ₹${"Cashback" == walletData[index].remark ? walletData[index].cashback : walletData[index].amount}",
                                            style: GoogleFonts.poppins(
                                              color: "credit" ==
                                                      walletData[index]
                                                          .paymentType
                                                  ? Colors.green
                                                  : Colors.red,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            walletData[index].gst_amount ==
                                                    "0.00"
                                                ? ""
                                                : "GST: ₹${walletData[index].gst_amount}",
                                            style: GoogleFonts.poppins(
                                              color: AppColor.darkGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          InkWell(
                                            child: Text(
                                              walletData[index].gst_amount !=
                                                      "0.00"
                                                  ? "Invoice"
                                                  : "",
                                              style: GoogleFonts.poppins(
                                                  color: Colors.blueAccent),
                                            ),
                                            onTap: () async {
                                              downloadProgressNotifier.value =
                                                  1;
                                              await launchUrl(Uri.parse(
                                                  walletData[index]
                                                      .invoice
                                                      .toString()));
                                              // downloadPDF(walletData[index]
                                              //     .invoice
                                              //     .toString());
                                            },
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            SizedBox(
                              height: 100,
                            )
                          ]),
                        ),
                      ),
                    ),
                  ),

                  /*Payment Logs Section*/
                  Visibility(
                    visible: walletTransaction == true ? false : true,
                    child: Expanded(
                      child: RefreshIndicator(
                        color: Colors.white,
                        backgroundColor: AppColor.appColor,
                        onRefresh: () async {
                          getPaymentLogs();
                          THelperFunctions.getWalletAmount().then((value) {
                            setState(() {
                              totalBalance = value;
                            });
                          });
                        },
                        child: SingleChildScrollView(
                          physics: AlwaysScrollableScrollPhysics(),
                          child: Column(children: [
                            ...List.generate(
                              paymentLogList.length,
                              (index) => Card(
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .55,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                                TFormatter.capitalizeSentence(
                                                    paymentLogList[index]
                                                        .remark
                                                        .toString()),
                                                maxLines: 3,
                                                style: GoogleFonts.poppins(
                                                    color: AppColor.darkGrey,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                            Text(
                                                TFormatter.formatDateTime(
                                                    paymentLogList[index]
                                                        .currentDateTime
                                                        .toLocal()),
                                                style: GoogleFonts.poppins(
                                                    color: AppColor.darkGrey,
                                                    fontSize: 12)),
                                            SizedBox(height: 10),
                                            Row(
                                              children: <Widget>[
                                                Text(
                                                    "#${paymentLogList[index].orderId.toString().replaceAll("null", "")}"),
                                                SizedBox(width: 10),
                                                InkWell(
                                                  onTap: () {},
                                                  child: Icon(Icons.copy,
                                                      size: 12),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Text(
                                                "+ ₹${paymentLogList[index].amount.toString()}",
                                                style: GoogleFonts.poppins(
                                                    color: paymentLogList[index]
                                                                .isSuccess ==
                                                            1
                                                        ? Colors.green
                                                        : paymentLogList[index]
                                                                    .isSuccess ==
                                                                2
                                                            ? Colors.red
                                                            : Colors.orange,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            Text(
                                                "GST ${Constants.currency}${paymentLogList[index].gstAmount.toString()}",
                                                style: GoogleFonts.poppins(
                                                    color: AppColor.darkGrey,
                                                    fontSize: 10)),
                                            SizedBox(height: 10),
                                            Wrap(children: [
                                              Text(
                                                ((paymentLogList[index]
                                                                .paymentStatus ==
                                                            "captured") ||
                                                        (paymentLogList[index]
                                                                .paymentStatus ==
                                                            "paid"))
                                                    ? "Payment Completed".tr()
                                                    : (paymentLogList[index]
                                                                .paymentStatus !=
                                                            "created")
                                                        ? "Failed".tr()
                                                        : "Pending".tr(),
                                                maxLines: 2,
                                                textAlign: TextAlign.end,
                                                style: GoogleFonts.poppins(
                                                  color: ((paymentLogList[index]
                                                                  .paymentStatus ==
                                                              "captured") ||
                                                          (paymentLogList[index]
                                                                  .paymentStatus ==
                                                              "paid"))
                                                      ? Colors.green
                                                      : (paymentLogList[index]
                                                                  .paymentStatus !=
                                                              "created")
                                                          ? Colors.red
                                                          : Colors.orange,
                                                ),
                                              ),
                                            ]),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 100)
                          ]),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
          Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: ValueListenableBuilder(
                  valueListenable: downloadProgressNotifier,
                  builder: (context, value, snapshot) {
                    return Visibility(
                      visible: downloadProgressNotifier.value > 0,
                      child: Container(
                        color: AppColor.transparentBlack,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              LinearPercentIndicator(
                                // animation: true,
                                barRadius: const Radius.circular(10),
                                // animationDuration: 400,
                                lineHeight: 15.0,
                                percent: downloadProgressNotifier.value / 100,
                                backgroundColor: Colors.grey.shade300,
                                progressColor: AppColor.appColor,
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                textAlign: TextAlign.center,
                                "Downloading PDF, Please wait".tr() +
                                    "...\n${downloadProgressNotifier.value}%",
                                style: GoogleFonts.poppins(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                              const SizedBox(height: 15),
                            ],
                          ),
                        ),
                      ),
                    );
                  }))
        ],
      ),
    );
  }

  DioClient? client;
  List<PaymentList> paymentLogList = [];

  Future<void> getPaymentLogs() async {
    client = DioClient();
    Dio dio = await client!.getClient();

    var params = jsonEncode({
      'user_id': await THelperFunctions.getUserId(),
    });
    

    PaymentLogModel? paymentLogModel =
        await client?.paymentLogs(dio, UrlConstants.baseUrl+UrlConstants.paymentLog, params);
    if (paymentLogModel!.status == true) {
      setState(() {
        paymentLogList = paymentLogModel.paymentList.reversed.toList();
      });
    }
  }

  Future<void> downloadPDF(String pdf) async {
    var dio = Dio();
    Directory? directory;
    try {
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await getExternalStorageDirectory();
        String newPath = directory?.path.substring(0, 20) ?? "";
        newPath += "Download";
        directory = Directory(newPath);
        if (!directory.existsSync()) {
          await directory.create();
        }
        File saveFile = File(directory.path + "/invoice.pdf");
        try {
          await dio.download(pdf, saveFile.path,
              onReceiveProgress: (received, total) {
            downloadProgressNotifier.value =
                (((received / total) * 100).toInt());
            final url = saveFile.path;
            if (downloadProgressNotifier.value == 100) {
              OpenFile.open(url);
              downloadProgressNotifier.value = 0;
            }
          });
        } on DioException catch (e) {
          print(e.message);
          downloadProgressNotifier.value = 0;
        }
      }
    } catch (err) {
      print("download folder not exist");
      downloadProgressNotifier.value = 0;
    }
  }
}
