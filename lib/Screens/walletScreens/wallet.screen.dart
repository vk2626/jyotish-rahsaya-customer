import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../Core/helper_functions.dart';
import '../../dialog/showLoaderDialog.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Core/Api/ApiServices.dart';
import '../../Core/Api/Constants.dart';
import '../../Core/Model/wallet.transection.model.dart';
import '../../CustomNavigator/SlideRightRoute.dart';
import '../../utils/AppColor.dart';
import '../../utils/custom_circular_progress_indicator.dart';
import '../payment.info.dart';

class WalletScreen extends StatefulWidget {
  const WalletScreen({super.key});

  @override
  State<WalletScreen> createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  int customerID = 0;
  String customerName = "";

  @override
  void initState() {
    super.initState();
    getUserId();
    getWalletAmount();
    THelperFunctions.logScreenNameEvent(screenName: "wallet");
  }

  @override
  Widget build(BuildContext context) {
    context
        .read<ListOfApisProvider>()
        .getRechargeAmountProvider(context: context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Add Money to Wallet".tr(),
          style: GoogleFonts.poppins(fontSize: 16),
        ),
      ),
      body: Consumer<ListOfApisProvider>(builder: (context, state, _) {
        if (state.isLoading) {
          return Center(
            child: CustomCircularProgressIndicator(),
          );
        }

        var rechargeData = state.rechargeAmountModel.recharge ?? [];
        log("Recharge Am --> ${rechargeData.length}");
        return Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                  text: TextSpan(
                    text: "Available Balance".tr() + "\n",
                    style: GoogleFonts.poppins(
                        color: AppColor.darkGrey, fontSize: 13),
                    children: <TextSpan>[
                      TextSpan(
                        text: "₹ $walletAmount",
                        style: GoogleFonts.poppins(
                          fontSize: 20,
                          color: AppColor.darkGrey,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: rechargeData.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 6 / 2,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10),
                  itemBuilder: (context, index) {
                    return InkWell(
                        onTap: () {
                          Navigator.of(context)
                              .push(
                            SlideRightRoute(
                                page: ListenableProvider(
                                    create: (context) => ListOfApisProvider(),
                                    child: PaymentInformationScreen(
                                      userInfoId: "",
                                      title: "Recharge Amount".tr(),
                                      astroName: customerName,
                                      price: rechargeData[index].price!,
                                      amountID: rechargeData[index].id!,
                                      from: 'wallet',
                                      astroId: '0',
                                    ))),
                          )
                              .whenComplete(() {
                            getWalletAmount();
                          });
                        },
                        child: Container(
                          color: Colors.white,
                          child: Stack(
                            children: [
                              Positioned(
                                left:
                                    -20, // Adjust the value to avoid overlapping
                                top:
                                    15, // Adjust the value for proper positioning

                                child: Transform.rotate(
                                  angle: -math.pi / 3.5,
                                  child: Container(
                                    color: index.isOdd
                                        ? Colors.orange
                                        : Colors.green,
                                    width: 100,
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 4),
                                    child: Text(
                                      "${rechargeData[index].discountPercentage}% " +
                                          "Extra".tr(),
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                        color: AppColor.whiteColor,
                                        fontSize: 10,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(3),
                                  border:
                                      Border.all(color: AppColor.borderColor),
                                ),
                                alignment: Alignment.center,
                                child: Text(
                                  "${rechargeData[index].price}",
                                  style: GoogleFonts.poppins(),
                                ),
                              ),
                            ],
                          ),
                        ));
                  }),
            ],
          ),
        );
      }),
    );
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;
      if (prefs.getString(Constants.fullName) != null) {
        customerName = prefs.getString(Constants.fullName)!;
      }
      getWalletAmount();
    }
  }

  DioClient? client;
  String walletAmount = "";

  getWalletAmount() {
    THelperFunctions.getWalletAmount().then((value) {
      setState(() {
        walletAmount = value.toString();
      });
    });
  }
}
