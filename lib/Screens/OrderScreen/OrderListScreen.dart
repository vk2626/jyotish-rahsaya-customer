import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../../Core/Provider/WaitListNotifier.dart';
import '../../Core/Provider/chatting_provider.dart';
import '../../Core/Provider/go_live_provider.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../Core/formatter.dart';
import '../AstrologerProfileNew.dart';
import '../../router_constants.dart';
import 'package:provider/provider.dart';
import '../../Core/helper_functions.dart';
import '../../Core/nav/SlideRightRoute.dart';
import '../../utils/custom_circular_progress_indicator.dart';
import '../flutter_chat_screen.dart';
import '../../Core/logger_helper.dart';
import '../../Core/Model/OrderHistoryModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Core/Api/ApiServices.dart';
import '../../Core/Api/Constants.dart';
import '../../dialog/showLoaderDialog.dart';
import '../../utils/AppColor.dart';
import '../flutter_chat_screen_new.dart';

class OrderListScreen extends StatefulWidget {
  const OrderListScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _OrderListScreenState();
  }
}

class _OrderListScreenState extends State<OrderListScreen> {
  DioClient? client;
  List<OrderHistory> orderHistoryList = [];
  bool isLoading = true; // Add this variable to manage loading state

  @override
  void initState() {
    super.initState();
    getOrderList();
    
    THelperFunctions.logScreenNameEvent(screenName: "orders_list_screen");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: isLoading
          ? Center(child: CustomCircularProgressIndicator())
          : orderHistoryList.isEmpty
              ? Center(
                  child: Text(
                    "No order found".tr(),
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                )
              : SingleChildScrollView(
                  child: Column(
                    children: List.generate(
                      orderHistoryList.length,
                      (index) => InkWell(
                        onTap: () {
                          try {
                            var data = orderHistoryList[index];
                            Navigator.of(context).push(SlideRightRoute(
                              page: MultiProvider(
                                providers: [
                                  ListenableProvider(
                                      create: (context) =>
                                          ListOfApisProvider()),
                                  ListenableProvider(
                                      create: (context) => ChattingProvider()),
                                ],
                                child: FlutterChatScreen(
                                  dob: "",
                                  lat: "",
                                  lon: "",
                                  tob: "",
                                  reviewId: data.review?.id ?? -1,
                                  initialRating:
                                      int.tryParse(data.review?.rate ?? "0") ??
                                          0,
                                  initialReview: data.review?.description ?? "",
                                  orderID: data.orderId,
                                  cummID: data.id.toString(),
                                  chat_price: 0,
                                  fromId: data.userId,
                                  toId: data.astrologerId,
                                  timerDuration: Duration.zero,
                                  astrologerName: TFormatter.capitalizeSentence(data.user.name),
                                  astrologerImage: data.user.avatar,
                                  messageInfo: {},
                                  infoId: data.infoId,
                                  isToView: true,
                                ),
                              ),
                            ));
                          } catch (e) {
                            TLoggerHelper.error(e.toString());
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 6),
                          padding:
                              EdgeInsets.symmetric(horizontal: 4, vertical: 6),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.shade300,
                                    spreadRadius: 2,
                                    blurRadius: 1,
                                    offset: Offset(1, 1))
                              ]),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Card(
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Image.network(
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.cover,
                                  orderHistoryList[index]
                                      .user
                                      .avatar
                                      .toString(),
                                ),
                                shape: CircleBorder(
                                  side: BorderSide(
                                      color: AppColor.appColor, width: 2),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * .5,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        orderHistoryList[index]
                                            .user
                                            .name
                                            .toString(),
                                        style: GoogleFonts.poppins(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16),
                                      ),
                                      SizedBox(height: 4),
                                      if (orderHistoryList[index].type ==
                                          "chat")
                                        Text(
                                          orderHistoryList[index]
                                                  .messages
                                                  .body ??
                                              orderHistoryList[index]
                                                  .messages
                                                  .attachment ??
                                              "",
                                          style: GoogleFonts.poppins(),
                                        ),
                                      if (orderHistoryList[index].type ==
                                          "call")
                                        Row(
                                          children: [
                                            Icon(Icons.mic,
                                                color: Colors.green, size: 16),
                                            SizedBox(width: 4),
                                            Text(
                                              TFormatter.formatDuration(
                                                  orderHistoryList[index]
                                                      .endTime!
                                                      .difference(
                                                          orderHistoryList[
                                                                  index]
                                                              .startTime)),
                                              style: GoogleFonts.poppins(),
                                            ),
                                          ],
                                        )
                                    ],
                                  ),
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: EdgeInsets.only(top: 10, right: 10),
                                child: Text(
                                  DateFormat("MMM dd, yyyy").format(
                                      DateTime.parse(orderHistoryList[index]
                                          .createdAt
                                          .toString())),
                                  style: GoogleFonts.poppins(fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
    );
  }

  Future<void> getOrderList() async {
    final prefs = await SharedPreferences.getInstance();
    var userId = prefs.getInt(Constants.userID).toString();

    client = DioClient();
    Dio dio = await client!.getClient();

    // String api = Constants.order;

    var params = jsonEncode({'user_id': userId});
    TLoggerHelper.debug(params);
    OrderHistoryModel? orderHistoryModel =
        await client?.orderHistory(dio, UrlConstants.baseUrl+UrlConstants.order, params);

    if (orderHistoryModel!.status == true) {
      setState(() {
        orderHistoryList
            .addAll(orderHistoryModel.orderHistory as Iterable<OrderHistory>);
        isLoading = false; // Set isLoading to false when data is loaded
      });
    } else {
      setState(() {
        isLoading = false; // Set isLoading to false even if no data is found
      });
    }
  }
}
