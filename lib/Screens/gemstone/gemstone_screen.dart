import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/custom_appbar.dart';

import '../../utils/custom_slider_data.dart';

class GemstoneScreen extends StatefulWidget {
  const GemstoneScreen({super.key});

  @override
  State<GemstoneScreen> createState() => _GemstoneScreenState();
}

class _GemstoneScreenState extends State<GemstoneScreen> {
  List<String> typeOfProductImageList = [
    "asset/icons/Diamond.png",
    "asset/icons/Ring.png",
    "asset/icons/Pendant.png",
  ];
  List<String> typeOfProductNameList = ["Gemstone", "Ring", "Pendant"];
  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CustomAppBar(title: "Product Details"),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: mediaQuery.height * .4,
                  width: mediaQuery.width * .9,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: CachedNetworkImageProvider(
                              "https://www.capediamondexchange.com/cdn/shop/files/11.8mmx9.8mmOvalRubyStone_1200x.jpg?v=1713261118"))),
                ),
                SizedBox(height: 12),
                Text(
                  "Ruby (Attract Wealth)",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 20),
                ),
                SizedBox(height: 4),
                Text(
                  "Lab Grown & Earth Friendly",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      color: Colors.green),
                ),
                SizedBox(height: 4),
                Row(
                  children: [
                    RatingBarIndicator(
                        rating: 3,
                        itemCount: 5,
                        itemSize: 15.0,
                        itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            )),
                    Text(
                      " | ",
                      style: TextStyle(color: Colors.grey, fontSize: 20),
                    ),
                    Text(
                      "1745 Customer Review",
                      style:
                          GoogleFonts.poppins(fontSize: 12, color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Text(
                  "Stimulates Chakra | Boost Self Confidence | Increased Wellness | Certificate Included | Energized | Free Shipping | 100% Cashback | Use Code CASHBACK100",
                  style: GoogleFonts.poppins(fontSize: 14, color: Colors.grey),
                ),
                SizedBox(height: 18),
                Text(
                  "Select Type",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 4),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                        children: List.generate(
                      5,
                      (index) => Container(
                        margin: EdgeInsets.only(right: 8),
                        padding:
                            EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                        decoration: BoxDecoration(
                            color: index == 1 ? Colors.yellow : Colors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: Colors.grey.shade300),
                            boxShadow: [
                              if (index == 1)
                                BoxShadow(
                                    color: Colors.grey.shade300,
                                    spreadRadius: 1,
                                    blurRadius: 2,
                                    offset: Offset(-1, 1))
                            ]),
                        child: Text(
                          "${index + 4} Ratti",
                          style: GoogleFonts.poppins(
                              fontWeight: index != 1
                                  ? FontWeight.normal
                                  : FontWeight.w600,
                              color: index == 1 ? Colors.black : Colors.grey),
                        ),
                      ),
                    )),
                  ),
                ),
                SizedBox(height: 4),
                Divider(color: Colors.grey.shade300),
                SizedBox(height: 4),
                Row(
                    children: List.generate(
                        3,
                        (index) => Padding(
                              padding: const EdgeInsets.only(right: 18.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    // height: 64,

                                    padding: EdgeInsets.all(18),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border:
                                            Border.all(color: Colors.black)),
                                    child: Image.asset(
                                      typeOfProductImageList[index],
                                      height: 28,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(height: 4),
                                  Text(
                                    typeOfProductNameList[index],
                                    style: GoogleFonts.poppins(fontSize: 14),
                                  )
                                ],
                              ),
                            ))),
                SizedBox(height: 18),
                Text(
                  "Add ons",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 8),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.shade300,
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(-1, 1),
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Material(
                            shape: CircleBorder(),
                            elevation: 4, // Provide elevation here
                            shadowColor: Colors.grey.shade300,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              child: Icon(
                                Icons.battery_charging_full_rounded,
                                color: Colors.blueAccent,
                              ),
                            ),
                          ),
                          SizedBox(
                              width:
                                  12), // Add spacing between the avatar and text
                          Text(
                            "108 Hours Charging",
                            style: GoogleFonts.poppins(
                                fontSize: 15), // Optional text styling
                          ),
                        ],
                      ),
                      Checkbox(
                        value: false,
                        onChanged: (value) {},
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 24),
                Text(
                  "Product Details",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 12),
                Row(
                  children: [
                    SizedBox(
                      width: 100,
                      child: Text(
                        "Category",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                    ),
                    SizedBox(width: 12),
                    Text(
                      "Gemstone",
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    SizedBox(
                      width: 100,
                      child: Text(
                        "Product",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                    ),
                    SizedBox(width: 12),
                    Flexible(
                        child: Text(
                      "5 Ratti Ruby (Manikya) (Lab Grown)",
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.grey),
                      maxLines: 2,
                    )),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    SizedBox(
                      width: 100,
                      child: Text(
                        "Shape",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                    ),
                    SizedBox(width: 12),
                    Text(
                      "Oval",
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Divider(color: Colors.grey.shade300),
                SizedBox(height: 12),
                Text(
                  "Lab Certified. Energized. Certificate Included. Free Shipping. Gemstone is available only in Panchdhatu Ring/Pendant.",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      color: Colors.grey),
                ),
                SizedBox(height: 12),
                // Padding(
                //   padding: const EdgeInsets.symmetric(vertical: 8.0),
                //   child: Row(
                //     children: [
                //       Text(
                //         "Purpose: ",
                //         style: GoogleFonts.poppins(
                //             fontWeight: FontWeight.w500,
                //             fontSize: 14,
                //             color: Colors.black54),
                //       ),
                //       Flexible(
                //         child: Text(
                //           "Gain success, popularity, and health protection.",
                //           style: GoogleFonts.poppins(
                //               fontWeight: FontWeight.w500,
                //               fontSize: 14,
                //               color: Colors.grey),
                //         ),
                //       )
                //     ],
                //   ),
                // ),
                DetailsWidget(
                    heading: "Purpose: ",
                    description:
                        "Gain success, popularity, and health protection."),
                DetailsWidget(
                    heading: "How to wear: ",
                    description:
                        "It can be worn as a ring or pendant on the right hand's ring finger"),
                DetailsWidget(
                    heading: "Symbolizes: ",
                    description: "Success, Health, Popularity"),
                DetailsWidget(heading: "Origin: ", description: "Burma"),
                DetailsWidget(
                    heading: "Return Policy: ",
                    description: "7 days Return & Replacement Policy"),
                DetailsWidget(
                    heading: "Astrotalk Guarantee: ",
                    description: "100% Genuine, Lab Certified, and Authentic"),
                DetailsWidget(
                    heading: "Please Note: ",
                    description:
                        "If you book this gemstone without any Add-on, you will receive this as loose gemstone only."),
                SizedBox(height: 8),
                Text(
                  "*Here is a short guide for Carat to Ratti interconversion*",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "1 carat = 200mg = 0.20g",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                Text(
                  "1 Ratti = 182mg = 0.18g",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                Text(
                  "Hence,",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                Text(
                  "*1 Ratti = 0.90 carats or 1 carat = 1.09 Ratti*",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(height: 24),
                Text(
                  "Most Asked Questions",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 12),
                buildExpansionTile(
                  title: "What is the Origin & Carat Weight of the product?",
                  contentList: ["Origin - Lab Grown\nCarat Weight - 4.55"],
                ),
                buildExpansionTile(
                  title: "What are the benefits?",
                  contentList: [
                    "This Gemstone is Lab Grown & Earth Friendly.",
                    "Ruby represents the Manipura or Navel Chakra in your body.",
                    "Stimulates chakra and removes self-doubt and depression.",
                    "Boosts self-confidence and passion towards life."
                  ],
                ),
                buildExpansionTile(
                    title:
                        "What is the return policy and Estimated delivery duration ?",
                    contentList: [
                      "We have a 7 days Return and Replacement Policy, ONLY is the product delivered is damaged or defective.",
                      "This product requires energization of upto 2 or 3 days by the astrologer, following which it will be shipped to you.",
                      "The estimated time of delivery is about 7 to 10 days"
                    ]),
                buildExpansionTile(
                    title: "When will it be delivered?",
                    contentList: [
                      "This product required 2 to 3 days energization by astrologer before shipping. Once shipped it will reach in 5 to 7 days."
                    ]),
                buildExpansionTile(title: "Disclaimer", contentList: [
                  "For external use only. Keep away from children. Handle with care to avoid breakage. Avoid heat and moisture. Use gentle cleaners to avoid any damage. Contact us for assistance."
                ]),
                SizedBox(height: 24),
                Text(
                  "Related Products",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 12),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      children: List.generate(
                        4,
                        (index) => Container(
                          width: mediaQuery.width * .4,
                          height: mediaQuery.height * .2,
                          margin: EdgeInsets.only(left: 12),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.shade300,
                                    spreadRadius: 1,
                                    blurRadius: 2,
                                    offset: Offset(-1, 1))
                              ]),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            topRight: Radius.circular(8)),
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: CachedNetworkImageProvider(
                                                "https://www.rajendrasgems.com/wp-content/uploads/pukhraj-yellow-sapphire-D0623-rg-p-rgw-602-1-1.jpg"))),
                                  )),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 8),
                                  child: Row(
                                    children: [
                                      Flexible(
                                          child: Text(
                                              "5.4 ratti Yellow Sapphire")),
                                      Icon(Icons.auto_graph_rounded,
                                          size: 18, color: Colors.amber)
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 24),
                Text(
                  "Reviews",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 12),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.shade300,
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(-1, 1))
                      ],
                      borderRadius: BorderRadius.circular(8)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              "4.7",
                              style: GoogleFonts.poppins(
                                  fontSize: 32, fontWeight: FontWeight.w700),
                            ),
                            Chip(
                                color: WidgetStatePropertyAll(Colors.black),
                                padding: EdgeInsets.zero,
                                label: Text("1745 reviews",
                                    style: GoogleFonts.poppins(
                                        color: Colors.white, fontSize: 12)))
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize
                              .min, // Ensure the column height matches its children
                          children: List.generate(
                            5,
                            (index) => Row(
                              mainAxisSize: MainAxisSize
                                  .min, // Avoid extra space in the rows
                              children: [
                                Text(
                                  "5",
                                  style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black54,
                                  ),
                                ),
                                SizedBox(width: 4),
                                Icon(Icons.star, color: Colors.amber),
                                SizedBox(width: 4),
                                SizedBox(
                                  width: mediaQuery.width * .3,
                                  child: SliderTheme(
                                    data: SliderThemeData(
                                        // trackShape: CustomSliderTrackShape(),
                                        activeTrackColor: Colors.amber,
                                        thumbShape:
                                            SliderComponentShape.noThumb,
                                        overlayShape:
                                            SliderComponentShape.noOverlay),
                                    child: Slider(
                                      value: .5,
                                      onChanged: (value) {},
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ]),
                ),
                SizedBox(height: 24),
                Text(
                  "User Reviews",
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600, fontSize: 18),
                ),
                SizedBox(height: 12),
                Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.shade300,
                              spreadRadius: 1,
                              blurRadius: 2,
                              offset: Offset(-1, 1))
                        ],
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            CircleAvatar(),
                            SizedBox(
                              width: 8,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Daizy"),
                                RatingBarIndicator(
                                    rating: 3,
                                    itemCount: 5,
                                    itemSize: 15.0,
                                    itemBuilder: (context, _) => const Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                        )),
                              ],
                            ),
                            Spacer(),
                            Icon(Icons.more_vert)
                          ],
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          "The size of the stone is so bigger as it feels like it is made for male person, normally women hand is smaller. It is very giant in my hands.",
                          style: GoogleFonts.poppins(color: Colors.grey),
                        )
                      ],
                    )),
                SizedBox(
                  height: mediaQuery.height * .1,
                )
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          height: mediaQuery.height * .09,
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.all(12),
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
                color: Colors.yellow,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.shade300,
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(-1, 1))
                ],
                borderRadius: BorderRadius.circular(8)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "BUY NOW",
                  style: GoogleFonts.poppins(fontSize: 18),
                ),
                Text(
                  "₹ 1435",
                  style: GoogleFonts.poppins(fontSize: 18),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget buildExpansionTile(
    {required String title, required List<String> contentList}) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 4.0),
    child: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      color: Colors.grey.shade50,
      elevation: 1,
      child: ExpansionTile(
        title: Text(
          title,
          style: GoogleFonts.poppins(
              fontSize: 15, fontWeight: FontWeight.w500, color: Colors.black54),
        ),
        children: [
          Container(
              width: double.infinity,
              padding: const EdgeInsets.all(12.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(
                    contentList.length,
                    (index) => Text(
                      (contentList.length == 1 ? "" : "• ") +
                          contentList[index],
                      style:
                          GoogleFonts.poppins(fontSize: 14, color: Colors.grey),
                    ),
                  ))),
        ],
        trailing: CircleAvatar(
          backgroundColor: Colors.black,
          radius: 12,
          child: const Icon(Icons.keyboard_arrow_down, color: Colors.white),
        ),
      ),
    ),
  );
}

class DetailsWidget extends StatelessWidget {
  final String heading;
  final String description;

  const DetailsWidget({
    Key? key,
    required this.heading,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            heading,
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: Colors.black54,
            ),
          ),
          const SizedBox(
              width: 4), // Adds some spacing between heading and description
          Expanded(
            // Ensures the description wraps properly
            child: Text(
              description,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
