import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../utils/custom_appbar.dart';

class AddressScreen extends StatefulWidget {
  const AddressScreen({super.key});

  @override
  State<AddressScreen> createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(title: "Address"),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
                padding: EdgeInsets.all(12),
                width: double.infinity,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.circular(8)),
                child: Text(
                  "Add new address",
                  style: GoogleFonts.poppins(
                      fontSize: 18, fontWeight: FontWeight.w600),
                )),
            Column(
                children: List.generate(
              5,
              (index) => Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Hemant Prajapati"),
                            Text("Near D-9, Sector 15"),
                            Text("Noida, Uttar Pradesh"),
                            Text("+91837484334"),
                            Text("+91738738278"),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(Icons.edit),
                            Chip(
                              side: BorderSide(
                                  color: Colors.green,
                                  width: 1.0), // Set border color and width
                              backgroundColor: Colors
                                  .white, // Use backgroundColor instead of WidgetStatePropertyAll
                              label: Text(
                                "Select",
                                style: GoogleFonts.poppins(color: Colors.green),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Divider()
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
