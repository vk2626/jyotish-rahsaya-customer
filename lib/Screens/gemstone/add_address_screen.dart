import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/AppColor.dart';

import '../../utils/custom_appbar.dart';

class AddAddressScreen extends StatefulWidget {
  const AddAddressScreen({super.key});

  @override
  State<AddAddressScreen> createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  // Controllers for text fields
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _flatNoController = TextEditingController();
  final TextEditingController _localityController = TextEditingController();
  final TextEditingController _landmarkController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _stateController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();
  final TextEditingController _pincodeController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(title: "Add Address"),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                AddressTextField(
                  headingName: "Name",
                  hintText: "Enter Name",
                  controller: _nameController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "Name is required";
                    }
                    return null;
                  },
                ),
                AddressTextField(
                  headingName: "Flat no.",
                  hintText: "Enter flat no.",
                  controller: _flatNoController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "Flat number is required";
                    }
                    return null;
                  },
                ),
                AddressTextField(
                  headingName: "Locality",
                  hintText: "Enter locality.",
                  controller: _localityController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "Locality is required";
                    }
                    return null;
                  },
                ),
                AddressTextField(
                  headingName: "Landmark",
                  hintText: "Enter landmark.",
                  controller: _landmarkController,
                  validator: (value) {
                    return null; // Optional field
                  },
                ),
                AddressTextField(
                  headingName: "City",
                  hintText: "Enter city.",
                  controller: _cityController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "City is required";
                    }
                    return null;
                  },
                ),
                AddressTextField(
                  headingName: "State/Province",
                  hintText: "Enter state/Province.",
                  controller: _stateController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "State/Province is required";
                    }
                    return null;
                  },
                ),
                AddressTextField(
                  headingName: "Country",
                  hintText: "Enter country.",
                  controller: _countryController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "Country is required";
                    }
                    return null;
                  },
                ),
                AddressTextField(
                  headingName: "Pincode",
                  hintText: "Enter pincode.",
                  controller: _pincodeController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "Pincode is required";
                    }
                    if (!RegExp(r'^\d{6}$').hasMatch(value)) {
                      return "Enter a valid 6-digit pincode";
                    }
                    return null;
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: GestureDetector(
        onTap: () {
          if (_formKey.currentState!.validate()) {
            // Process the form
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Address saved successfully")),
            );
          }
        },
        child: Container(
          height: mediaQuery.height * .065,
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(color: Colors.yellow),
          alignment: Alignment.center,
          child: Text(
            "CONTINUE",
            style: GoogleFonts.poppins(fontSize: 18),
          ),
        ),
      ),
    );
  }
}

class AddressTextField extends StatelessWidget {
  final String hintText;
  final String headingName;
  final TextEditingController controller;
  final String? Function(String?)? validator;

  const AddressTextField({
    super.key,
    required this.hintText,
    required this.headingName,
    required this.controller,
    required this.validator,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            headingName,
            style: GoogleFonts.poppins(fontWeight: FontWeight.w500),
          ),
          TextFormField(
            controller: controller,
            validator: validator,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(4),
              isDense: true,
              hintText: hintText,
              labelStyle:
                  GoogleFonts.poppins(color: AppColor.appColor, fontSize: 16),
              hintStyle:
                  GoogleFonts.poppins(color: Colors.black45, fontSize: 13),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.black38),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColor.appColor),
              ),
            ),
          )
        ],
      ),
    );
  }
}
