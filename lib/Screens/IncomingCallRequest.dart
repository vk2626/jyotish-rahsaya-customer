import 'dart:async';
import 'dart:developer';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background/flutter_background.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../foreground_service_manager.dart';
import '../router_constants.dart';
import '../Core/helper_functions.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../utils/snack_bar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Core/Api/Constants.dart';

import '../Core/formatter.dart';
import '../Core/logger_helper.dart';
import '../Core/nav/SlideRightRoute.dart';
import 'NavigationScreen.dart';

class IncomingCallRequest extends StatefulWidget {
  final String token;
  final String channelName;
  final String astrologerId;
  final String userName;
  final String imageUrl;
  final String communication_id;
  final Duration totalCallDuration;
  final bool isAttended;

  IncomingCallRequest(
      {Key? key,
      required this.token,
      required this.communication_id,
      required this.channelName,
      required this.userName,
      required this.astrologerId,
      this.isAttended = false,
      required this.imageUrl,
      required this.totalCallDuration})
      : super(key: key);
  static late RtcEngine agoraEngine;
  @override
  State<IncomingCallRequest> createState() => _IncomingCallRequestState();
}

class _IncomingCallRequestState extends State<IncomingCallRequest> {
  final String appId = "2b2b29ac0c8b40e88eae4516659054ed";
  late final String channelName;
  String? token;
  bool isConnected = false;
  SharedPreferences? prefs;
  int minutesCompleted = 0;
  int uid = 0;
  bool isAttended = false;
  bool isMuted = false;
  bool isSpeaker = false;
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  // ForegroundServiceManager _foregroundServiceManager =
  //     ForegroundServiceManager();
  late Duration centerTempDuration;

  Timer? _timer;
  int durationMin = 0;

  @override
  void initState() {
    super.initState();

    token = widget.token;
    channelName = widget.channelName;
    centerTempDuration = Duration.zero;

    TLoggerHelper.info(
        "Token is:- ${widget.token} && Channel Name is:- ${widget.channelName}");

    setup();
    THelperFunctions.logScreenNameEvent(screenName: "incoming_call");
    // _startForegroundService();
  }

  // Future<void> _startForegroundService() async {
  //   ForegroundServiceManager()
  //       .startForegroundService(callback: foregroundTaskCallback);
  // }
  Future<void> enableBackgroundExecution() async {
    try {
      const androidConfig = FlutterBackgroundAndroidConfig(
        notificationTitle: "Jyotish Rahsaya",
        notificationText: "Jyotish Rahsaya calling is under progress",
        notificationImportance: AndroidNotificationImportance.max,

        notificationIcon: AndroidResource(
            name: 'background_icon',
            defType: 'drawable'), // Default is ic_launcher from folder mipmap
      );
      bool success =
          await FlutterBackground.initialize(androidConfig: androidConfig);

      if (success) {
        await FlutterBackground.enableBackgroundExecution();
      } else {
        TLoggerHelper.error("Some Error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  setup() async {
    IncomingCallRequest.agoraEngine = createAgoraRtcEngine();
    await setupVoiceSDKEngine();
    prefs = await SharedPreferences.getInstance();
  }

  void foregroundTaskCallback() {
    // Ensure you have access to the Agora engine instance
    // You might need to manage this globally or through some state management
    final RtcEngine engine = IncomingCallRequest.agoraEngine;

    if (engine == null) {
      print("Agora engine is not initialized.");
      return;
    }

    // Implement any logic you need to keep the call active
    // For example, checking connection status and maintaining audio
    engine.getConnectionState().then((state) {
      if (state == ConnectionStateType.connectionStateConnected) {
        log("Agora call connected.");
        // Additional logic if needed
      } else if (state == ConnectionStateType.connectionStateReconnecting) {
        log("Agora call reconnecting...");
        // Handle reconnection if necessary
      } else if (state == ConnectionStateType.connectionStateDisconnected) {
        log("Agora call disconnected.");
        // Implement reconnection logic or notify the user
      }
    });

    engine.registerEventHandler(RtcEngineEventHandler(
      onUserOffline: (connection, remoteUid, reason) {
        log("Remote user uid:$remoteUid went offline.");
      },
      onUserJoined: (connection, remoteUid, elapsed) {
        log("Remote user uid:$remoteUid joined the channel.");
      },
    ));

    // // Maintain audio mixing or playback
    // engine.startAudioMixing(
    //     filePath: 'https://example.com/your-audio-file.mp3',
    //     loopback: true,
    //     cycle: 1, // Adjust as needed
    //     startPos: 0);
  }

  Future<void> setupVoiceSDKEngine() async {
    TLoggerHelper.info("Setup Voice SDK Started");
    try {
      await Permission.microphone.request();
      await enableBackgroundExecution();
      await IncomingCallRequest.agoraEngine
          .initialize(RtcEngineContext(appId: appId));
      await IncomingCallRequest.agoraEngine
          .setParameters('{"che.audio.opensl":true}');
      await IncomingCallRequest.agoraEngine
          .setDefaultAudioRouteToSpeakerphone(false);

      IncomingCallRequest.agoraEngine.registerEventHandler(
        RtcEngineEventHandler(
          onJoinChannelSuccess: (RtcConnection connection, int elapsed) async {
            showMessage(
                "Local user uid:${connection.localUid} joined the channel");
            setState(() {});

            await IncomingCallRequest.agoraEngine.startAudioMixing(
              filePath: 'https://admin.jyotishrahsya.com/call_ring.mp3',
              loopback: true, // Ensure it's played only on the local device
              cycle: 2, // Loop the sound 10 times
              startPos: 0, // Start from the beginning
            );
          },
          onUserJoined:
              (RtcConnection connection, int remoteUid, int elapsed) async {
            showMessage("Remote user uid:$remoteUid joined the channel");
            setState(() {
              isAttended = true;
              isConnected = true;
            });
            await IncomingCallRequest.agoraEngine.stopAudioMixing();
            _startTimer();
          },
          onUserOffline: (RtcConnection connection, int remoteUid,
              UserOfflineReasonType reason) async {
            await leave();
            showMessage("Remote user uid:$remoteUid left the channel");
          },
          onConnectionStateChanged: (connection, state, reason) async {
            await IncomingCallRequest.agoraEngine
                .muteLocalAudioStream(false)
                .whenComplete(
              () {
                TLoggerHelper.debug(
                    connection.toString() + " ----" + " changed");
              },
            );
          },
        ),
      );
      TLoggerHelper.debug(
          "Check Token When Join: ${token} ${channelName} ${uid}");
      TLoggerHelper.warning(
          "IsConnected :-- $isConnected ${widget.totalCallDuration}");
      if (!widget.isAttended) {
        join();
      } else {
        _startTimer();
      }
    } on AgoraRtcException catch (e) {
      TLoggerHelper.error(e.message.toString());
    }
  }

  join() async {
    Provider.of<ListOfApisProvider>(context, listen: false).acceptCallStart(
        communicationId: widget.communication_id,
        astrologerId: widget.astrologerId);
    ChannelMediaOptions options = const ChannelMediaOptions(
        clientRoleType: ClientRoleType.clientRoleBroadcaster,
        channelProfile: ChannelProfileType.channelProfileCommunication);

    try {
      TLoggerHelper.debug(
          "Check Token When Join: ${token} ${channelName} ${uid}");
      IncomingCallRequest.agoraEngine.joinChannel(
          token: token.toString(),
          channelId: channelName,
          options: options,
          uid: uid);
    } catch (e) {
      showMessage("Failed to join the channel: $e");
      TLoggerHelper.error("Failed to join the channel: $e");
    }
  }

  Future<void> leave() async {
    if (FlutterBackground.isBackgroundExecutionEnabled) {
      await FlutterBackground.disableBackgroundExecution();
    }
    var releaseOperation = IncomingCallRequest.agoraEngine.release();
    var leaveChannelOperation = IncomingCallRequest.agoraEngine.leaveChannel();
    await Future.wait([releaseOperation, leaveChannelOperation]);
    if (isConnected) {
      try {
        await Provider.of<ListOfApisProvider>(context, listen: false)
            .sendCallDurationProvider(
                communicationID: widget.communication_id,
                duration: minutesCompleted);
      } catch (e) {
        TLoggerHelper.debug(
            "isConnected is false, skipping sendDurationCallEnd.");
      }
    }

    // Stop the foreground service when leaving the channel
    if (!isAttended) {
      try {
        rejectCallFunction(communicationId: widget.communication_id);
      } catch (e) {
        TLoggerHelper.error("Error in isNotAttended: $e");
      }
    }

    if (mounted && Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
    }

    THelperFunctions.clearCallingPrefs();
  }

  void _startTimer() {
    TLoggerHelper.debug(
        "IsConnected :-- $isConnected ${widget.totalCallDuration}");
    int elapsedSeconds = widget.totalCallDuration.inSeconds;
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (!isConnected) {
          isConnected = true;
        }
        elapsedSeconds = elapsedSeconds - 1;
        centerTempDuration = Duration(seconds: elapsedSeconds);

        if (centerTempDuration.inSeconds >= 60) {
          minutesCompleted =
              (widget.totalCallDuration.inSeconds - elapsedSeconds) ~/ 60;
        }
        if (centerTempDuration.inSeconds <= 0) {
          leave();
        }
        // TLoggerHelper.debug(
        //     "Minutes Completed:-  ${minutesCompleted} --- Elapased time:- ${_elapsedSeconds}");
      });
    });
  }

  void showMessage(String message) {
    scaffoldMessengerKey.currentState
        ?.showSnackBar(SnackBar(content: Text(message)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldMessengerKey,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'asset/images/rectangle.png',
            fit: BoxFit.cover,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text(
                    TFormatter.capitalizeSentence(widget.userName),
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 18),
                  CircleAvatar(
                    radius: 72,
                    backgroundImage:
                        CachedNetworkImageProvider(widget.imageUrl),
                  ),
                  SizedBox(height: 18),
                  Text(
                    isConnected
                        ? TFormatter.formatDuration(centerTempDuration)
                        : "On Call",
                  ),
                ],
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                          onPressed: () async {
                            setState(() {
                              isMuted = !isMuted;
                            });
                            await IncomingCallRequest.agoraEngine
                                .muteLocalAudioStream(isMuted);
                          },
                          icon: CircleAvatar(
                              backgroundColor: isMuted
                                  ? Colors.blueGrey.withOpacity(.6)
                                  : Colors.white,
                              radius: 26,
                              child: Icon(isMuted
                                  ? Icons.keyboard_voice_outlined
                                  : Icons.keyboard_voice_rounded))),
                      IconButton(
                          onPressed: () async {
                            setState(() {
                              isSpeaker = !isSpeaker;
                            });
                            await IncomingCallRequest.agoraEngine
                                .setEnableSpeakerphone(isSpeaker);
                          },
                          icon: CircleAvatar(
                              backgroundColor: isSpeaker
                                  ? Colors.blueGrey.withOpacity(.6)
                                  : Colors.white,
                              radius: 26,
                              child: Icon(isSpeaker
                                  ? Icons.volume_up_rounded
                                  : Icons.volume_off))),
                    ],
                  ),
                  IconButton(
                    onPressed: () async {
                      THelperFunctions.clearCallingPrefs();
                      if (!isAttended) {
                        await Provider.of<ListOfApisProvider>(context,
                                listen: false)
                            .sendCallDurationProvider(
                                communicationID: widget.communication_id,
                                duration: minutesCompleted)
                            .whenComplete(() async {
                          if (FlutterBackground.isBackgroundExecutionEnabled) {
                            await FlutterBackground
                                .disableBackgroundExecution();
                          }
                        });
                      }
                      // FlutterBackground.disableBackgroundExecution();
                      await leave();
                    },
                    icon: CircleAvatar(
                      radius: 32,
                      backgroundColor: Colors.red,
                      child: Icon(
                        Icons.call_end_rounded,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  static Future<void> rejectCallFunction(
      {required String communicationId}) async {
    var endPointUrl = UrlConstants.rejectCall;
    final Map<String, String> body = {'communication_id': communicationId};
    final dio = Dio();
    await dio
        .post("${UrlConstants.baseUrl}$endPointUrl", data: body)
        .then((val) => TLoggerHelper.info("Completed Successfully!!"));
  }

  //
}
