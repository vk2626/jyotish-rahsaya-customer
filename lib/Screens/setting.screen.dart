import 'dart:convert';
import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Core/helper_functions.dart';
import '../dialog/showLoaderDialog.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../SubScreens/restart.app.widget.dart';
import '../handler_classes/secure_storage.dart';
import '../utils/AppColor.dart';
import '../Core/langConstant/language.constant.dart';
import '../Core/langConstant/language.dart';

import 'package:http/http.dart' as http;

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  String selectedLanguage = "";
  Language? language;
  int selectedIdx = -1;
  String customerId = "";
  List<String> listLang = [
    "ENG\nEnglish",
    "हिंदी\nHindi",
    // "ਪੰਜਾਬੀ\nPunjabi",
    // "मराठी\nMarathi",
    // "తెలుగు\nTelugu",
    // "ಕನ್ನಡ\nKannada",
    // "বাংলা\nBengali"
  ];

  @override
  void initState() {
    super.initState();
    _getSelectedLanguage();
  }

  Future<void> _getSelectedLanguage() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String userId = await THelperFunctions.getUserId(); // Fetch first

  setState(() {
    selectedLanguage = _prefs.getString(LAGUAGE_NAME) ?? listLang[0];
    selectedIdx = _prefs.getInt("Selected_Index") ?? -1;
    customerId = userId;  // Update inside setState
    print(customerId);
  });
}


  Future<void> _applyLanguage() async {
    if (selectedIdx != -1) {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      language = Language.languageList()[selectedIdx];
      Locale _locale = await setLocale(language!.languageCode);

      // Check if the language has changed
      if (selectedIdx != _prefs.getInt("Selected_Index")) {
        await _prefs.setString(LAGUAGE_NAME, language!.name);
        await _prefs.setString(LAGUAGE_CODE, language!.languageCode);
        await _prefs.setInt("Selected_Index", selectedIdx);

        // RestartWidget.restartApp(context);
        // Navigator.of(context).pop();
        Restart.restartApp();
        Fluttertoast.showToast(msg: "Language Changed Successfully !!");
      } else {
        print("Language index is the same. No need to apply.");
      }
    } else {
      print("Error: No language selected!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.appBackgroundLightGrey,
      appBar: AppBar(title: Text("Setting".tr())),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            _buildNotificationSettings(),
            SizedBox(height: 10),
            _buildLanguageSettings(context),
            if (customerId.isNotEmpty && customerId != "null" && customerId!="0")
  Column(
    children: [
      SizedBox(height: 10),
      _buildDeleteAccountSettings(context),
    ],
  ),

            
          ],
        ),
      ),
    );
  }

  Widget _buildNotificationSettings() {
    return Container(
      height: 170,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SizedBox(height: 10),
            Align(
              alignment: Alignment.topLeft,
              child: Text("Notification".tr(),
                  style: GoogleFonts.poppins(fontWeight: FontWeight.bold)),
            ),
            SwitchListTile(
              title: Text("Jyotish Mall".tr(), style: GoogleFonts.poppins()),
              value: true,activeColor: AppColor.appColor,thumbColor: WidgetStateColor.resolveWith((states) {
                
                return AppColor.whiteColor;
              },),
              onChanged: (value) {},
            ),
            SwitchListTile(
              title: Text("Live Event".tr(), style: GoogleFonts.poppins()),
              value: true,activeColor: AppColor.appColor,thumbColor: WidgetStateColor.resolveWith((states) {
                
                return AppColor.whiteColor;
              },),
              onChanged: (value) {},
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDeleteAccountSettings(BuildContext context) {
    return InkWell(
      onTap: () {
        showLogoutDialog(context);
      },
      child: Container(
        padding: EdgeInsets.all(12),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          children: [
            Icon(Icons.delete, color: Colors.red),
            SizedBox(width: 12),
            Text("Delete My Account".tr(),
                style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.red)),
          ],
        ),
      ),
    );
  }

  void showLogoutDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false, // User must tap a button to close the dialog

      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
          title: Text(
            'Are you sure you want to delete your account?'.tr(),
            style: GoogleFonts.poppins(color: Colors.black, fontSize: 14),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
                // Handle the "No" action here if needed
              },
              child: Text(
                'No'.tr(),
                style: GoogleFonts.poppins(color: AppColor.appColor),
              ),
            ),
            TextButton(
              onPressed: () async {
                THelperFunctions.httpPostHandler(
                    client: http.Client(),
                    endPointUrl: "delete",
                    body: {
                      "id": await THelperFunctions.getUserId(),
                      "user_type": "2"
                    }).then(
                  (value) async {
                    final Map<String, dynamic> parseData =
                        await jsonDecode(value);
                    if (parseData['status']) {
                      final prefs = await SharedPreferences.getInstance();
                      await prefs.clear();
                      await SecureStorage().clearAll();

                      Restart.restartApp();
                    }
                  },
                );
              },
              child: Text(
                'Yes'.tr(),
                style: GoogleFonts.poppins(color: AppColor.appColor),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildLanguageSettings(BuildContext context) {
    return Container(
      height: 100,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text("Change App Language".tr(),
                  style: GoogleFonts.poppins(fontWeight: FontWeight.bold)),
            ),
            SizedBox(height: 10),
            GestureDetector(
              onTap: () => _showLanguageDialog(context),
              child: Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: AppColor.borderColor),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        selectedLanguage,
                        style: GoogleFonts.poppins(
                            fontSize: 15, color: AppColor.darkGrey),
                      ),
                      Icon(Icons.arrow_drop_down),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _showLanguageDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => StatefulBuilder(
        builder: (context, StateSetter setState) {
          return AlertDialog(
            title: Text('Choose your app language'.tr()),
            content: Container(
              height: 400,
              width: 500,
              child: Column(
                children: [
                  Expanded(
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0,
                      ),
                      padding: EdgeInsets.all(8.0),
                      itemCount: listLang.length,
                      itemBuilder: (context, subindex) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              selectedIdx = subindex;
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: subindex == selectedIdx
                                  ? AppColor.appColor
                                  : AppColor.appBackgroundLightGrey,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: Text(
                                listLang[subindex],
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 18.0, color: Colors.black87),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 50,
                    width: 500,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        backgroundColor:
                            MaterialStatePropertyAll<Color>(AppColor.appColor),
                      ),
                      onPressed: _applyLanguage,
                      child: Text("APPLY".tr(),
                          style: GoogleFonts.poppins(color: Colors.black)),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
