import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import '../../../utils/custom_circular_progress_indicator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import '../../Core/Model/astrologer_short_details_model.dart';
import '../../Core/Model/pooja_details_model.dart';
import '../../Core/Provider/list.of.api.provider.dart';
import '../../Core/formatter.dart';
import '../../Core/helper_functions.dart';
import '../../Core/nav/SlideRightRoute.dart';
import '../payment.info.dart';
import '../../router_constants.dart';
import '../../utils/AppColor.dart';
import 'package:provider/provider.dart';

class PoojaDetailsScreenNew extends StatefulWidget {
  final String userInfoId;
  final String poojaId;
  final bool isDirectPooja;
  final String astrologerId;

  PoojaDetailsScreenNew(
      {super.key,
      required this.userInfoId,
      required this.poojaId,
      this.isDirectPooja = true,
      required this.astrologerId});

  @override
  State<PoojaDetailsScreenNew> createState() => _PoojaDetailsScreenNewState();
}

class _PoojaDetailsScreenNewState extends State<PoojaDetailsScreenNew> {
  Duration _remainingTime = Duration();
  late PoojaDetailsModel poojaDetailsModel;
  late AstrologerShortDetailsModel astrologerShortDetailsModel;
  bool isPoojaLoading = true;
  String customerId = "";
  bool isAstrologerLoading = true;
  DateTime dateTime = DateTime.now();
  String formattedDate = "";

  String formattedMonth = "";
  String formattedYear = "";
  String formattedTime = "";
  List<String> newPriceList = [];
  List<String> newLaunchedPriceList = [];
  int totalAmount = 0;
  late Timer _timer;
  @override
  void initState() {
    super.initState();
    _loadPoojaDetails();
    THelperFunctions.logScreenNameEvent(screenName: "pooja_details");
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await THelperFunctions.getUserId().then((value) {
        setState(() {
          customerId = value;
        });
      });
    });
  }

  Future<void> _loadPoojaDetails() async {
    final details =
        await Provider.of<ListOfApisProvider>(context, listen: false)
            .getPoojaDetails(poojaId: widget.poojaId);

    if (details != null) {
      setState(() {
        poojaDetailsModel = details;
        isPoojaLoading = false;
        dateTime = poojaDetailsModel.data.time ?? DateTime.now();

        formattedDate = DateFormat('dd').format(dateTime);
        formattedMonth = DateFormat('MMM').format(dateTime);
        formattedYear = DateFormat('yyyy').format(dateTime);
        formattedTime = DateFormat('hh:mm a').format(dateTime);
      });
      if (widget.astrologerId.isNotEmpty) {
        _fetchPanditDetails(astrologerDetails: widget.astrologerId);
      }
      if (poojaDetailsModel.data.time != null) {
        _startTimer(poojaDetailsModel.data.time ?? DateTime.now());
      }
    }
  }

  Future<void> _fetchPanditDetails({required String astrologerDetails}) async {
    final details =
        await Provider.of<ListOfApisProvider>(context, listen: false)
            .getPanditDetails(astrologerId: astrologerDetails);
    if (details != null) {
      setState(() {
        astrologerShortDetailsModel = details;
        isAstrologerLoading = false;
      });
    }
  }

  void _startTimer(DateTime poojaDateTime) {
    _remainingTime = poojaDateTime.difference(DateTime.now());

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _remainingTime = poojaDateTime.difference(DateTime.now());

        if (_remainingTime.isNegative) {
          _timer.cancel();
        }
      });
    });
  }

  String formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final hours = twoDigits(duration.inHours);
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));
    return "$hours:$minutes:$seconds";
  }

  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;

    final formattedDuration = formatDuration(_remainingTime);
    final formattedParts = formattedDuration.split(':');

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Product Details".tr(),
          style: GoogleFonts.poppins(),
        ),
      ),
      body: isPoojaLoading
          ? Center(child: CustomCircularProgressIndicator())
          : SingleChildScrollView(
              child: Column(children: [
                Container(
                  height: mediaQuery.height * .3,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                              poojaDetailsModel.data.image),
                          fit: BoxFit.fill)),
                ),
                Container(
                  width: mediaQuery.width,
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Pooja Start in".tr() + ":",
                            style: GoogleFonts.poppins(fontSize: 16),
                          ),
                          SizedBox(width: 12),
                          if (poojaDetailsModel.data.time != null)
                            _buildCountdownBox(
                                formattedParts[0] + 'h'), // Hours
                          if (poojaDetailsModel.data.time != null)
                            _buildCountdownBox(
                                formattedParts[1] + 'm'), // Minutes
                          if (poojaDetailsModel.data.time != null)
                            _buildCountdownBox(
                                formattedParts[2] + 's'), // Seconds
                        ],
                      ),
                      SizedBox(height: 18),
                      Text(
                        // widget.title,
                        TFormatter.capitalize(poojaDetailsModel.data.title),
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                        TFormatter.capitalize(
                            poojaDetailsModel.data.shortDescription),
                        style: GoogleFonts.poppins(
                            fontSize: 14, color: Colors.grey.shade600),
                      ),
                      // SizedBox(
                      //   height: 8,
                      // ),
                      // Row(
                      //   children: [
                      //     Icon(
                      //       Icons.thumb_up_alt_rounded,
                      //       size: 18,
                      //       color: Colors.green,
                      //     ),
                      //     SizedBox(
                      //       width: 8,
                      //     ),
                      //     Text(
                      //       "Top 1% Pandit",
                      //       style: GoogleFonts.poppins(
                      //           fontWeight: FontWeight.w500,
                      //           fontSize: 14,
                      //           color: Colors.grey.shade700),
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 6,
                      // ),
                      // Row(
                      //   children: [
                      //     Icon(
                      //       Icons.mode_standby,
                      //       size: 18,
                      //       color: Colors.green,
                      //     ),
                      //     SizedBox(
                      //       width: 8,
                      //     ),
                      //     Text(
                      //       "Recording after Pooja",
                      //       style: GoogleFonts.poppins(
                      //           fontWeight: FontWeight.w500,
                      //           fontSize: 14,
                      //           color: Colors.grey.shade700),
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 6,
                      // ),
                      // Row(
                      //   children: [
                      //     Icon(
                      //       Icons.money_rounded,
                      //       size: 18,
                      //       color: Colors.green,
                      //     ),
                      //     SizedBox(
                      //       width: 8,
                      //     ),
                      //     Text(
                      //       "30 days money back guarantee",
                      //       style: GoogleFonts.poppins(
                      //           fontWeight: FontWeight.w500,
                      //           fontSize: 14,
                      //           color: Colors.grey.shade700),
                      //     ),
                      //   ],
                      // ),
                      Divider(),
                      Text(
                        "About Pandit".tr(),
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.grey.shade800),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      if (widget.astrologerId.isNotEmpty)
                        isAstrologerLoading
                            ? SizedBox(
                                height: 50,
                                child: Center(
                                  child: CustomCircularProgressIndicator(),
                                ),
                              )
                            : Column(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      context.pushNamed(
                                        RouteConstants.astrologerProfile,
                                        queryParameters: {
                                          'astrologerId':
                                              astrologerShortDetailsModel
                                                  .astrologers.first.id
                                                  .toString(),
                                          'type': 'chat',
                                          'isAstrologerOnline':"0"
                                        },
                                      );
                                    },
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 26,
                                          backgroundColor: Colors.grey,
                                          child: CircleAvatar(
                                            radius: 25,
                                            backgroundImage:
                                                CachedNetworkImageProvider(
                                                    astrologerShortDetailsModel
                                                        .astrologers
                                                        .first
                                                        .avatar),
                                          ),
                                        ),
                                        SizedBox(width: 12),
                                        Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                TFormatter.capitalize(
                                                    astrologerShortDetailsModel
                                                        .astrologers
                                                        .first
                                                        .name),
                                                style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    color:
                                                        Colors.grey.shade600),
                                              ),
                                              // SizedBox(
                                              //   height: 2,
                                              // ),
                                              // Text(
                                              //   "Vedic Guide",
                                              //   style: GoogleFonts.poppins(
                                              //       fontWeight: FontWeight.w500,
                                              //       fontSize: 14,
                                              //       color: Colors.grey.shade800),
                                              // ),
                                              // SizedBox(
                                              //   height: 2,
                                              // ),
                                              Text(
                                                "${formattedDate} ${formattedMonth} ${formattedYear}, ${formattedTime}",
                                                style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color:
                                                        Colors.grey.shade800),
                                              ),
                                              SizedBox(
                                                height: 4,
                                              ),
                                              Row(
                                                children: [
                                                  Icon(Icons.star,
                                                      size: 18,
                                                      color: Colors.green),
                                                  Text(
                                                      "${astrologerShortDetailsModel.astrologers.first.rating} Stars",
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color: Colors.grey
                                                                  .shade700)),
                                                  SizedBox(
                                                    width: 12,
                                                  ),
                                                  // Icon(Icons.calendar_month_sharp,
                                                  //     size: 18, color: Colors.green),
                                                  // Text("64 Pooja's done",
                                                  //     style: GoogleFonts.poppins(
                                                  //         color: Colors.grey.shade700))
                                                ],
                                              ),
                                            ]),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 12,
                                  ),
                                  Text(
                                    TFormatter.capitalize(
                                        astrologerShortDetailsModel
                                            .astrologers.first.astrologer.bio),
                                    style: GoogleFonts.poppins(
                                        fontSize: 14,
                                        color: Colors.grey.shade600),
                                  ),
                                  Divider(),
                                  Html(
                                    data: poojaDetailsModel.data.description,
                                  ),
                                  Divider(),
                                ],
                              ),
                      if (widget.astrologerId.isEmpty)
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                                context.pushNamed(
                                  RouteConstants.astrologerProfile,
                                  queryParameters: {
                                    'astrologerId':
                                        poojaDetailsModel.data.id.toString(),
                                    'type': 'chat',
                                    'isAstrologerOnline':"0"
                                  },
                                );
                              },
                              child: Row(
                                children: [
                                  CircleAvatar(
                                    radius: 26,
                                    backgroundColor: Colors.grey,
                                    child: CircleAvatar(
                                      radius: 25,
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                              poojaDetailsModel
                                                  .data.astrologerImage),
                                    ),
                                  ),
                                  SizedBox(width: 12),
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          TFormatter.capitalize(
                                              poojaDetailsModel
                                                  .data.astrologerDisplayName),
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              color: Colors.grey.shade600),
                                        ),
                                        // SizedBox(
                                        //   height: 2,
                                        // ),
                                        // Text(
                                        //   "Vedic Guide",
                                        //   style: GoogleFonts.poppins(
                                        //       fontWeight: FontWeight.w500,
                                        //       fontSize: 14,
                                        //       color: Colors.grey.shade800),
                                        // ),
                                        // SizedBox(
                                        //   height: 2,
                                        // ),
                                        Text(
                                          "${formattedDate} ${formattedMonth} ${formattedYear}, ${formattedTime}",
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14,
                                              color: Colors.grey.shade800),
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Row(
                                          children: [
                                            Icon(Icons.star,
                                                size: 18, color: Colors.green),
                                            Text(
                                                "${poojaDetailsModel.data.rating} Stars",
                                                style: GoogleFonts.poppins(
                                                    color:
                                                        Colors.grey.shade700)),
                                            SizedBox(
                                              width: 12,
                                            ),
                                            // Icon(Icons.calendar_month_sharp,
                                            //     size: 18, color: Colors.green),
                                            // Text("64 Pooja's done",
                                            //     style: GoogleFonts.poppins(
                                            //         color: Colors.grey.shade700))
                                          ],
                                        ),
                                      ]),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Text(
                              TFormatter.capitalize(
                                  poojaDetailsModel.data.astrologerBio),
                              style: GoogleFonts.poppins(
                                  fontSize: 14, color: Colors.grey.shade600),
                            ),
                            Divider(),
                            Html(
                              data: poojaDetailsModel.data.description,
                            ),
                            Divider(),
                          ],
                        ),
                    ],
                  ),
                ),
              ]),
            ),
      bottomNavigationBar: (!isPoojaLoading && customerId != "0")
          ? Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 60,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      backgroundColor: poojaDetailsModel.data.isPoojaBooked == 0
                          ? AppColor.appColor
                          : Colors.grey.shade300),
                  onPressed: () {
                    if (poojaDetailsModel.data.isPoojaBooked == 0) {
                      _settingModalBottomSheet(context);
                    } else {
                      Fluttertoast.showToast(msg: "Pooja is already Booked");
                    }
                  },
                  child: isPoojaLoading || poojaDetailsModel == null
                      ? CircularProgressIndicator(
                          color: Colors.white,
                        )
                      : Text(
                          poojaDetailsModel.data.isPoojaBooked == 1
                              ? "Already Booked"
                              : "Book Now".tr(),
                          style: GoogleFonts.poppins(
                              fontSize: 20, color: AppColor.darkGrey),
                        ),
                ),
              ),
            )
          : SizedBox.shrink(),
    );
  }

  Widget _buildCountdownBox(String text) {
    return Container(
      height: 36,
      width: 36,
      margin: EdgeInsets.only(right: 8),
      decoration: BoxDecoration(
        color: AppColor.appColor.withOpacity(.9),
        borderRadius: BorderRadius.circular(8),
      ),
      alignment: Alignment.center,
      child: Text(
        text,
        style: GoogleFonts.poppins(fontSize: 15),
      ),
    );
  }

  Widget customUserList(String imgVal, String text) {
    return Container(
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: CircleAvatar(
              radius: 20.0,
              backgroundImage: AssetImage(
                imgVal,
              ),
            ),
          ),
          SizedBox(width: 10),
          Text(
            text,
            style: GoogleFonts.poppins(
              fontSize: 15.0,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  void _settingModalBottomSheet(context) {
    List<bool> isCheckedList =
        List.generate(poojaDetailsModel.data.price.length, (index) => false);
    final DateTime dateTime = poojaDetailsModel.data.time ?? DateTime.now();
    final String formattedDate = DateFormat('dd').format(dateTime);
    final String formattedMonth = DateFormat('MM').format(dateTime);
    final String formattedYear = DateFormat('yyyy').format(dateTime);
    final String formattedTime = DateFormat('hh:mm a').format(dateTime);

    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        builder: (BuildContext bc) {
          return ListenableProvider(
            create: (context) => ListOfApisProvider(),
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 43,
                      backgroundColor: Colors.black,
                      child: CircleAvatar(
                          radius: 42,
                          backgroundImage: CachedNetworkImageProvider(
                              poojaDetailsModel.data.astrologerImage)),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(poojaDetailsModel.data.astrologerDisplayName,
                        style: GoogleFonts.poppins(fontSize: 16)),
                    SizedBox(
                      height: 4,
                    ),
                    Text(poojaDetailsModel.data.title,
                        style: GoogleFonts.poppins(fontSize: 18)),
                    SizedBox(
                      height: 4,
                    ),
                    RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                "${formattedDate}-${formattedMonth}-${formattedYear}",
                            style: GoogleFonts.poppins(
                                color: Colors.black87,
                                fontSize: 12,
                                fontWeight: FontWeight.w500),
                          ),
                          TextSpan(
                              text: ", ${formattedTime}",
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 12)),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Expanded(
                      child: Container(
                          // child: poojaDetailsModel.data.price.isNotEmpty
                          //     ? ListView.builder(
                          child: ListView.builder(
                              itemCount: poojaDetailsModel.data.price.length,
                              itemBuilder: (context, index) {
                                return Visibility(
                                  visible: true,
                                  child: ListTile(
                                    title: Row(
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: poojaDetailsModel.data
                                                    .price[index].priceTitle,
                                                style: GoogleFonts.poppins(
                                                    color: Colors.black87,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              TextSpan(
                                                text:
                                                    " @ ₹ ${(poojaDetailsModel.data.price[index].priceDiscount.toString() == "null" || poojaDetailsModel.data.price![index].priceDiscount == "0") ? '' : poojaDetailsModel.data.price![index].priceDiscount} ",
                                                style: GoogleFonts.poppins(
                                                    color: AppColor.darkGrey,
                                                    fontSize: 14),
                                              ),
                                              TextSpan(
                                                text: poojaDetailsModel
                                                    .data.price[index].price,
                                                style: GoogleFonts.poppins(
                                                    color: AppColor.darkGrey,
                                                    fontSize: 14,
                                                    decoration: (poojaDetailsModel
                                                                    .data
                                                                    .price[
                                                                        index]
                                                                    .priceDiscount ==
                                                                null &&
                                                            poojaDetailsModel
                                                                    .data
                                                                    .price[
                                                                        index]
                                                                    .priceDiscount ==
                                                                "0")
                                                        ? TextDecoration.none
                                                        : TextDecoration
                                                            .lineThrough,
                                                    decorationColor:
                                                        Colors.red),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    subtitle: Container(
                                      child: Text(
                                        "${poojaDetailsModel.data.price![index].priceDescription}",
                                        style: GoogleFonts.poppins(
                                          color: Colors.grey,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    trailing: Checkbox(
                                      value: isCheckedList[index],
                                      onChanged: (value) {
                                        setState(() {
                                          isCheckedList[index] = value!;
                                          if (value!) {
                                            var amount = (poojaDetailsModel
                                                            .data
                                                            .price[index]
                                                            .priceDiscount ==
                                                        null &&
                                                    poojaDetailsModel
                                                            .data
                                                            .price![index]
                                                            .priceDiscount ==
                                                        "0")
                                                ? poojaDetailsModel
                                                    .data.price![index].price
                                                    .toString()
                                                : poojaDetailsModel.data
                                                    .price![index].priceDiscount
                                                    .toString();
                                            totalAmount += int.parse(amount);
                                            newPriceList.add(amount);
                                          } else {
                                            var amount = (poojaDetailsModel
                                                            .data
                                                            .price[index]
                                                            .priceDiscount ==
                                                        null &&
                                                    poojaDetailsModel
                                                            .data
                                                            .price![index]
                                                            .priceDiscount ==
                                                        "0")
                                                ? poojaDetailsModel
                                                    .data.price![index].price
                                                    .toString()
                                                : poojaDetailsModel.data
                                                    .price![index].priceDiscount
                                                    .toString();
                                            newPriceList.remove(amount);
                                            totalAmount -= int.parse(amount);
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                );
                              })
                          // : ListView.builder(
                          //     itemCount: widget.nprice!.length,
                          //     itemBuilder: (context, index) {
                          //       return Visibility(
                          //         visible: true,
                          //         child: ListTile(
                          //           title: Row(
                          //             children: <Widget>[
                          //               RichText(
                          //                 text: TextSpan(
                          //                   children: <TextSpan>[
                          //                     TextSpan(
                          //                       text:
                          //                           "${widget.nprice![index].priceTitle}",
                          //                       style: GoogleFonts.poppins(
                          //                           color: Colors.black87,
                          //                           fontSize: 14,
                          //                           fontWeight:
                          //                               FontWeight.w600),
                          //                     ),
                          //                     TextSpan(
                          //                       text: " @ ₹ " +
                          //                           ((widget.nprice![index]
                          //                                           .priceDiscount ==
                          //                                       "" &&
                          //                                   widget
                          //                                           .price![
                          //                                               index]
                          //                                           .priceDiscount
                          //                                           .toString() ==
                          //                                       "0")
                          //                               ? ''
                          //                               : widget
                          //                                   .nprice![index]
                          //                                   .priceDiscount!),
                          //                       style: GoogleFonts.poppins(
                          //                           color:
                          //                               AppColor.darkGrey,
                          //                           fontSize: 14),
                          //                     ),
                          //                     TextSpan(
                          //                       text:
                          //                           "${widget.nprice![index].price}",
                          //                       style: GoogleFonts.poppins(
                          //                           color:
                          //                               AppColor.darkGrey,
                          //                           fontSize: 14,
                          //                           decoration: (widget
                          //                                           .nprice![
                          //                                               index]
                          //                                           .priceDiscount ==
                          //                                       null &&
                          //                                   widget
                          //                                           .price![
                          //                                               index]
                          //                                           .priceDiscount ==
                          //                                       "0")
                          //                               ? TextDecoration
                          //                                   .none
                          //                               : TextDecoration
                          //                                   .lineThrough,
                          //                           decorationColor:
                          //                               Colors.red),
                          //                     ),
                          //                   ],
                          //                 ),
                          //               ),
                          //             ],
                          //           ),
                          //           subtitle: Container(
                          //             child: Text(
                          //               "${widget.nprice![index].priceDescription}",
                          //               style: GoogleFonts.poppins(
                          //                 color: Colors.grey,
                          //                 fontSize: 16,
                          //               ),
                          //             ),
                          //           ),
                          //           trailing: Checkbox(
                          //             value: isCheckedList[index],
                          //             onChanged: (value) {
                          //               setState(() {});
                          //             },
                          //           ),
                          //         ),
                          //       );
                          //     })
                          ),
                    ),
                    InkWell(
                      onTap: () async {
                        // await Provider.of<ListOfApisProvider>(context,
                        //         listen: false)
                        //     .createOrderPoojaListProvider(
                        //   context: context,
                        //   userID: customerID.toString(),
                        //   astrologerID: widget.astrologer_id,
                        //   productID: widget.id,
                        //   isWallet: '1',
                        //   productPrice: widget.nprice!.isEmpty
                        //       ? newPriceList
                        //       : newLaunchedPriceList,
                        //   amount: totalAmount.toString(),
                        // )
                        //     .then((val) {
                        //   developer.log(val.entries.toString());
                        //   if (val.entries.first.key) {
                        //     developer.log(widget.astrologer_id.toString());
                        //     developer.log(customerName.toString());
                        //     developer.log(totalAmount.toString());
                        //     developer.log(customerID.toString());
                        //     developer.log(widget.id.toString());
                        //     developer.log(
                        //         "Pooja OrderID Here Created:${val.entries.first.value.toString()}");
                        //   }
                        // });
                        if (totalAmount > 0) {
                          Navigator.of(context).push(
                            SlideRightRoute(
                                page: ListenableProvider(
                                    create: (context) => ListOfApisProvider(),
                                    child: PaymentInformationScreen(
                                      userInfoId: widget.userInfoId,
                                      title: poojaDetailsModel.data.title,
                                      astroId: widget.astrologerId,
                                      astroName:
                                          await THelperFunctions.getUserName(),
                                      price: totalAmount.toString(),
                                      amountID: 0,
                                      // productID: poojaDetailsModel
                                      //         .data.suggestedAstroId.isEmpty
                                      //     ? poojaDetailsModel.data.id.toString()
                                      //     : poojaDetailsModel.data.astroPoojaId
                                      //         .toString(),
                                      productID:
                                          poojaDetailsModel.data.id.toString(),
                                      from: '',
                                      isProductOrPooja: true,
                                      suggested_astro_id: poojaDetailsModel
                                          .data.suggestedAstroId,
                                      remedy_id:
                                          poojaDetailsModel.data.poojaOrderId,

                                      originalProductId: poojaDetailsModel
                                          .data.astroPoojaId
                                          .toString(),
                                    ))),
                          );
                        } else {
                          Fluttertoast.showToast(
                              msg: "You haven't selected any Pooja");
                        }
                      },
                      child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 12),
                          padding: EdgeInsets.symmetric(
                              horizontal: 18, vertical: 12),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.appColor,
                              borderRadius: BorderRadius.circular(8)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                    "Total Amount".tr() + ": ₹ ${totalAmount} ",
                                    style: GoogleFonts.poppins(fontSize: 16)),
                                Text("Confirm".tr(),
                                    style: GoogleFonts.poppins(fontSize: 16)),
                              ],
                            ),
                          )),
                    ),
                  ],
                ),
              );
            }),
          );
        });
  }
}
