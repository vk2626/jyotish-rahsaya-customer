// import 'dart:async';
// import 'dart:developer' as developer;

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_html/flutter_html.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:intl/intl.dart';
// import 'package:jyotish_rahsaya/utils/snack_bar.dart';
// import '../../Core/formatter.dart';
// import '../../Core/Model/create_order_pooja_model.dart';
// import '../../Core/Provider/list.of.api.provider.dart';
// import '../payment.info.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../../Core/Api/Constants.dart';
// import '../../Core/Services/payment.services.dart';
// import '../../Core/nav/SlideRightRoute.dart';
// import '../../utils/AppColor.dart';

// import '../../Core/Model/book.pooja.model.dart';
// import '../../Core/Model/newly.launched.model.dart';

// class PoojaDetailScreen extends StatefulWidget {
//   const PoojaDetailScreen(
//       {super.key,
//       required this.title,
//       required this.shortDes,
//       required this.urName,
//       required this.astrologer_id,
//       required this.date,
//       required this.description,
//       required this.uProfile,
//       required this.price,
//       required this.nprice,
//       required this.pImage,
//       required this.uBio,
//       required this.rating,
//       required this.astrologerDisplayName,
//       required this.id});

//   final String id;
//   final String title;
//   final String rating;
//   final String shortDes;
//   final String astrologerDisplayName;
//   final String urName;
//   final String astrologer_id;
//   final DateTime date;
//   final String pImage;
//   final String description;
//   final String uProfile;
//   final String uBio;
//   final List<Price>? price;
//   final List<LaunchedPrice>? nprice;

//   @override
//   State<PoojaDetailScreen> createState() => _PoojaDetailScreenState();
// }

// class _PoojaDetailScreenState extends State<PoojaDetailScreen> {
//   int customerID = 0;
//   String customerName = "";
//   late Timer _timer;
//   Duration _remainingTime = Duration();
//   late DateTime eventDateTime;

//   List<String> newPriceList = [];
//   List<String> newLaunchedPriceList = [];
//   int totalAmount = 0;
//   CreateOrderPoojaModel createOrderPoojaModel = CreateOrderPoojaModel();

//   @override
//   void initState() {
//     eventDateTime = widget.date;

//     _startTimer();
//     getUserId();
//     super.initState();
//   }

//   @override
//   void dispose() {
//     _timer.cancel();
//     super.dispose();
//   }

//   void _startTimer() {
//     final DateTime poojaDateTime = widget.date;
//     _remainingTime = poojaDateTime.difference(DateTime.now());

//     _timer = Timer.periodic(Duration(seconds: 1), (timer) {
//       setState(() {
//         _remainingTime = poojaDateTime.difference(DateTime.now());

//         if (_remainingTime.isNegative) {
//           _timer.cancel();
//         }
//       });
//     });
//   }

//   late PaymentServices paymentServices;

//   void getUserId() async {
//     final prefs = await SharedPreferences.getInstance();
//     if (prefs.getInt(Constants.userID) != null ||
//         prefs.getString(Constants.fullName) != null) {
//       customerID = prefs.getInt(Constants.userID)!;
//       customerName = prefs.getString(Constants.fullName)!;
//     }
//   }

//   String formatDuration(Duration duration) {
//     String twoDigits(int n) => n.toString().padLeft(2, '0');
//     final hours = twoDigits(duration.inHours);
//     final minutes = twoDigits(duration.inMinutes.remainder(60));
//     final seconds = twoDigits(duration.inSeconds.remainder(60));
//     return "$hours:$minutes:$seconds";
//   }

//   @override
//   Widget build(BuildContext context) {
//     Size mediaQuery = MediaQuery.of(context).size;
//     final DateTime dateTime = widget.date;
//     final String formattedDate = DateFormat('dd').format(dateTime);
//     final String formattedMonth = DateFormat('MMM').format(dateTime);
//     final String formattedYear = DateFormat('yyyy').format(dateTime);
//     final String formattedTime = DateFormat('hh:mm a').format(dateTime);
//     final formattedDuration = formatDuration(_remainingTime);
//     final formattedParts = formattedDuration.split(':');

//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         title: Text(
//           "Product Details",
//           style: GoogleFonts.poppins(),
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           children: <Widget>[
//             Container(
//               height: mediaQuery.height * .3,
//               width: double.infinity,
//               decoration: BoxDecoration(
//                   image: DecorationImage(
//                       image: CachedNetworkImageProvider(widget.pImage),
//                       fit: BoxFit.fill)),
//             ),
//             Container(
//               width: mediaQuery.width,
//               padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Row(
//                     children: [
//                       Text(
//                         "Pooja Start in:",
//                         style: GoogleFonts.poppins(fontSize: 16),
//                       ),
//                       SizedBox(width: 12),
//                       _buildCountdownBox(formattedParts[0] + 'h'), // Hours
//                       _buildCountdownBox(formattedParts[1] + 'm'), // Minutes
//                       _buildCountdownBox(formattedParts[2] + 's'), // Seconds
//                     ],
//                   ),
//                   SizedBox(height: 18),
//                   Text(
//                     widget.title,
//                     style: GoogleFonts.poppins(
//                         fontWeight: FontWeight.w600, fontSize: 18),
//                   ),
//                   SizedBox(
//                     height: 6,
//                   ),
//                   Text(
//                     widget.shortDes,
//                     style: GoogleFonts.poppins(
//                         fontSize: 14, color: Colors.grey.shade600),
//                   ),
//                   // SizedBox(
//                   //   height: 8,
//                   // ),
//                   // Row(
//                   //   children: [
//                   //     Icon(
//                   //       Icons.thumb_up_alt_rounded,
//                   //       size: 18,
//                   //       color: Colors.green,
//                   //     ),
//                   //     SizedBox(
//                   //       width: 8,
//                   //     ),
//                   //     Text(
//                   //       "Top 1% Pandit",
//                   //       style: GoogleFonts.poppins(
//                   //           fontWeight: FontWeight.w500,
//                   //           fontSize: 14,
//                   //           color: Colors.grey.shade700),
//                   //     ),
//                   //   ],
//                   // ),
//                   // SizedBox(
//                   //   height: 6,
//                   // ),
//                   // Row(
//                   //   children: [
//                   //     Icon(
//                   //       Icons.mode_standby,
//                   //       size: 18,
//                   //       color: Colors.green,
//                   //     ),
//                   //     SizedBox(
//                   //       width: 8,
//                   //     ),
//                   //     Text(
//                   //       "Recording after Pooja",
//                   //       style: GoogleFonts.poppins(
//                   //           fontWeight: FontWeight.w500,
//                   //           fontSize: 14,
//                   //           color: Colors.grey.shade700),
//                   //     ),
//                   //   ],
//                   // ),
//                   // SizedBox(
//                   //   height: 6,
//                   // ),
//                   // Row(
//                   //   children: [
//                   //     Icon(
//                   //       Icons.money_rounded,
//                   //       size: 18,
//                   //       color: Colors.green,
//                   //     ),
//                   //     SizedBox(
//                   //       width: 8,
//                   //     ),
//                   //     Text(
//                   //       "30 days money back guarantee",
//                   //       style: GoogleFonts.poppins(
//                   //           fontWeight: FontWeight.w500,
//                   //           fontSize: 14,
//                   //           color: Colors.grey.shade700),
//                   //     ),
//                   //   ],
//                   // ),
//                   Divider(),
//                   Text(
//                     "About Pandit",
//                     style: GoogleFonts.poppins(
//                         fontWeight: FontWeight.w500,
//                         fontSize: 16,
//                         color: Colors.grey.shade800),
//                   ),
//                   SizedBox(
//                     height: 12,
//                   ),
//                   Row(
//                     children: [
//                       CircleAvatar(
//                         radius: 26,
//                         backgroundColor: Colors.grey,
//                         child: CircleAvatar(
//                           radius: 25,
//                           backgroundImage:
//                               CachedNetworkImageProvider(widget.uProfile),
//                         ),
//                       ),
//                       SizedBox(width: 12),
//                       Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Text(
//                               widget.urName,
//                               style: GoogleFonts.poppins(
//                                   fontWeight: FontWeight.w500,
//                                   fontSize: 16,
//                                   color: Colors.grey.shade600),
//                             ),
//                             // SizedBox(
//                             //   height: 2,
//                             // ),
//                             // Text(
//                             //   "Vedic Guide",
//                             //   style: GoogleFonts.poppins(
//                             //       fontWeight: FontWeight.w500,
//                             //       fontSize: 14,
//                             //       color: Colors.grey.shade800),
//                             // ),
//                             // SizedBox(
//                             //   height: 2,
//                             // ),
//                             Text(
//                               "${formattedDate} ${formattedMonth} ${formattedYear}, ${formattedTime}",
//                               style: GoogleFonts.poppins(
//                                   fontWeight: FontWeight.w500,
//                                   fontSize: 14,
//                                   color: Colors.grey.shade800),
//                             ),
//                             SizedBox(
//                               height: 4,
//                             ),
//                             Row(
//                               children: [
//                                 Icon(Icons.star, size: 18, color: Colors.green),
//                                 Text("${widget.rating} Stars",
//                                     style: GoogleFonts.poppins(
//                                         color: Colors.grey.shade700)),
//                                 SizedBox(
//                                   width: 12,
//                                 ),
//                                 // Icon(Icons.calendar_month_sharp,
//                                 //     size: 18, color: Colors.green),
//                                 // Text("64 Pooja's done",
//                                 //     style: GoogleFonts.poppins(
//                                 //         color: Colors.grey.shade700))
//                               ],
//                             ),
//                           ]),
//                     ],
//                   ),
//                   SizedBox(
//                     height: 12,
//                   ),
//                   Text(
//                     TFormatter.capitalize(widget.uBio),
//                     style: GoogleFonts.poppins(
//                         fontSize: 14, color: Colors.grey.shade600),
//                   ),
//                   Divider(),
//                   Html(
//                     data: widget.description,
//                   ),
//                   Divider(),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//       bottomNavigationBar: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Container(
//           height: 60,
//           child: ElevatedButton(
//             style: ElevatedButton.styleFrom(
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.all(Radius.circular(10))),
//                 backgroundColor: AppColor.appColor),
//             onPressed: () {
//               _settingModalBottomSheet(context);
//             },
//             child: Text(
//               "Book Now",
//               style:
//                   GoogleFonts.poppins(fontSize: 20, color: AppColor.darkGrey),
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget _buildCountdownBox(String text) {
//     return Container(
//       height: 36,
//       width: 36,
//       margin: EdgeInsets.only(right: 8),
//       decoration: BoxDecoration(
//         color: AppColor.appColor.withOpacity(.9),
//         borderRadius: BorderRadius.circular(8),
//       ),
//       alignment: Alignment.center,
//       child: Text(
//         text,
//         style: GoogleFonts.poppins(fontSize: 15),
//       ),
//     );
//   }

//   Widget customUserList(String imgVal, String text) {
//     return Container(
//       child: Row(
//         children: <Widget>[
//           Padding(
//             padding: const EdgeInsets.all(2.0),
//             child: CircleAvatar(
//               radius: 20.0,
//               backgroundImage: AssetImage(
//                 imgVal,
//               ),
//             ),
//           ),
//           SizedBox(width: 10),
//           Text(
//             text,
//             style: GoogleFonts.poppins(
//               fontSize: 15.0,
//               color: Colors.black,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   void _settingModalBottomSheet(context) {
//     List<bool> isCheckedList = List.generate(
//         widget.price!.length + widget.nprice!.length, (index) => false);
//     final DateTime dateTime = widget.date;
//     final String formattedDate = DateFormat('dd').format(dateTime);
//     final String formattedMonth = DateFormat('MM').format(dateTime);
//     final String formattedYear = DateFormat('yyyy').format(dateTime);
//     final String formattedTime = DateFormat('hh:mm a').format(dateTime);

//     showModalBottomSheet(
//         context: context,
//         backgroundColor: Colors.white,
//         builder: (BuildContext bc) {
//           return ListenableProvider(
//             create: (context) => ListOfApisProvider(),
//             child: StatefulBuilder(
//                 builder: (BuildContext context, StateSetter setState) {
//               return Padding(
//                 padding: const EdgeInsets.only(top: 12),
//                 child: Column(
//                   children: <Widget>[
//                     CircleAvatar(
//                       radius: 43,
//                       backgroundColor: Colors.black,
//                       child: CircleAvatar(
//                           radius: 42,
//                           backgroundImage:
//                               CachedNetworkImageProvider("${widget.uProfile}")),
//                     ),
//                     SizedBox(
//                       height: 8,
//                     ),
//                     Text("${widget.urName}",
//                         style: GoogleFonts.poppins(fontSize: 16)),
//                     SizedBox(
//                       height: 4,
//                     ),
//                     Text("${widget.title}",
//                         style: GoogleFonts.poppins(fontSize: 18)),
//                     SizedBox(
//                       height: 4,
//                     ),
//                     RichText(
//                       text: TextSpan(
//                         children: <TextSpan>[
//                           TextSpan(
//                             text:
//                                 "${formattedDate}-${formattedMonth}-${formattedYear}",
//                             style: GoogleFonts.poppins(
//                                 color: Colors.black87,
//                                 fontSize: 12,
//                                 fontWeight: FontWeight.w500),
//                           ),
//                           TextSpan(
//                               text: ", ${formattedTime}",
//                               style: GoogleFonts.poppins(
//                                   color: Colors.grey, fontSize: 12)),
//                         ],
//                       ),
//                     ),
//                     SizedBox(height: 10),
//                     Expanded(
//                       child: Container(
//                           child: widget.price!.isNotEmpty
//                               ? ListView.builder(
//                                   itemCount: widget.price!.length,
//                                   itemBuilder: (context, index) {
//                                     return Visibility(
//                                       visible: true,
//                                       child: ListTile(
//                                         title: Row(
//                                           children: <Widget>[
//                                             RichText(
//                                               text: TextSpan(
//                                                 children: <TextSpan>[
//                                                   TextSpan(
//                                                     text: widget.price![index]
//                                                         .priceTitle,
//                                                     style: GoogleFonts.poppins(
//                                                         color: Colors.black87,
//                                                         fontSize: 14,
//                                                         fontWeight:
//                                                             FontWeight.w600),
//                                                   ),
//                                                   TextSpan(
//                                                     text:
//                                                         " @ ₹ ${(widget.price![index].priceDiscount.toString() == "null" || widget.price![index].priceDiscount == "0") ? '' : widget.price![index].priceDiscount} ",
//                                                     style: GoogleFonts.poppins(
//                                                         color:
//                                                             AppColor.darkGrey,
//                                                         fontSize: 14),
//                                                   ),
//                                                   TextSpan(
//                                                     text: widget
//                                                         .price![index].price,
//                                                     style: GoogleFonts.poppins(
//                                                         color:
//                                                             AppColor.darkGrey,
//                                                         fontSize: 14,
//                                                         decoration: (widget
//                                                                         .price![
//                                                                             index]
//                                                                         .priceDiscount ==
//                                                                     null &&
//                                                                 widget
//                                                                         .price![
//                                                                             index]
//                                                                         .priceDiscount ==
//                                                                     "0")
//                                                             ? TextDecoration
//                                                                 .none
//                                                             : TextDecoration
//                                                                 .lineThrough,
//                                                         decorationColor:
//                                                             Colors.red),
//                                                   ),
//                                                 ],
//                                               ),
//                                             ),
//                                           ],
//                                         ),
//                                         subtitle: Container(
//                                           child: Text(
//                                             "${widget.price![index].priceDescription}",
//                                             style: GoogleFonts.poppins(
//                                               color: Colors.grey,
//                                               fontSize: 16,
//                                             ),
//                                           ),
//                                         ),
//                                         trailing: Checkbox(
//                                           value: isCheckedList[index],
//                                           onChanged: (value) {
//                                             setState(() {
//                                               isCheckedList[index] = value!;
//                                               if (value!) {
//                                                 var amount = (widget
//                                                                 .price![index]
//                                                                 .priceDiscount ==
//                                                             null &&
//                                                         widget.price![index]
//                                                                 .priceDiscount ==
//                                                             "0")
//                                                     ? widget.price![index].price
//                                                         .toString()
//                                                     : widget.price![index]
//                                                         .priceDiscount
//                                                         .toString();
//                                                 totalAmount +=
//                                                     int.parse(amount);
//                                                 newPriceList.add(amount);
//                                               } else {
//                                                 var amount = (widget
//                                                                 .price![index]
//                                                                 .priceDiscount ==
//                                                             null &&
//                                                         widget.price![index]
//                                                                 .priceDiscount ==
//                                                             "0")
//                                                     ? widget.price![index].price
//                                                         .toString()
//                                                     : widget.price![index]
//                                                         .priceDiscount
//                                                         .toString();
//                                                 newPriceList.remove(amount);
//                                                 totalAmount -=
//                                                     int.parse(amount);
//                                               }
//                                             });
//                                           },
//                                         ),
//                                       ),
//                                     );
//                                   })
//                               : ListView.builder(
//                                   itemCount: widget.nprice!.length,
//                                   itemBuilder: (context, index) {
//                                     return Visibility(
//                                       visible: true,
//                                       child: ListTile(
//                                         title: Row(
//                                           children: <Widget>[
//                                             RichText(
//                                               text: TextSpan(
//                                                 children: <TextSpan>[
//                                                   TextSpan(
//                                                     text:
//                                                         "${widget.nprice![index].priceTitle}",
//                                                     style: GoogleFonts.poppins(
//                                                         color: Colors.black87,
//                                                         fontSize: 14,
//                                                         fontWeight:
//                                                             FontWeight.w600),
//                                                   ),
//                                                   TextSpan(
//                                                     text: " @ ₹ " +
//                                                         ((widget.nprice![index]
//                                                                         .priceDiscount ==
//                                                                     "" &&
//                                                                 widget
//                                                                         .price![
//                                                                             index]
//                                                                         .priceDiscount
//                                                                         .toString() ==
//                                                                     "0")
//                                                             ? ''
//                                                             : widget
//                                                                 .nprice![index]
//                                                                 .priceDiscount!),
//                                                     style: GoogleFonts.poppins(
//                                                         color:
//                                                             AppColor.darkGrey,
//                                                         fontSize: 14),
//                                                   ),
//                                                   TextSpan(
//                                                     text:
//                                                         "${widget.nprice![index].price}",
//                                                     style: GoogleFonts.poppins(
//                                                         color:
//                                                             AppColor.darkGrey,
//                                                         fontSize: 14,
//                                                         decoration: (widget
//                                                                         .nprice![
//                                                                             index]
//                                                                         .priceDiscount ==
//                                                                     null &&
//                                                                 widget
//                                                                         .price![
//                                                                             index]
//                                                                         .priceDiscount ==
//                                                                     "0")
//                                                             ? TextDecoration
//                                                                 .none
//                                                             : TextDecoration
//                                                                 .lineThrough,
//                                                         decorationColor:
//                                                             Colors.red),
//                                                   ),
//                                                 ],
//                                               ),
//                                             ),
//                                           ],
//                                         ),
//                                         subtitle: Container(
//                                           child: Text(
//                                             "${widget.nprice![index].priceDescription}",
//                                             style: GoogleFonts.poppins(
//                                               color: Colors.grey,
//                                               fontSize: 16,
//                                             ),
//                                           ),
//                                         ),
//                                         trailing: Checkbox(
//                                           value: isCheckedList[index],
//                                           onChanged: (value) {
//                                             setState(() {});
//                                           },
//                                         ),
//                                       ),
//                                     );
//                                   })),
//                     ),
//                     InkWell(
//                       onTap: () async {
//                         // await Provider.of<ListOfApisProvider>(context,
//                         //         listen: false)
//                         //     .createOrderPoojaListProvider(
//                         //   context: context,
//                         //   userID: customerID.toString(),
//                         //   astrologerID: widget.astrologer_id,
//                         //   productID: widget.id,
//                         //   isWallet: '1',
//                         //   productPrice: widget.nprice!.isEmpty
//                         //       ? newPriceList
//                         //       : newLaunchedPriceList,
//                         //   amount: totalAmount.toString(),
//                         // )
//                         //     .then((val) {
//                         //   developer.log(val.entries.toString());
//                         //   if (val.entries.first.key) {
//                         //     developer.log(widget.astrologer_id.toString());
//                         //     developer.log(customerName.toString());
//                         //     developer.log(totalAmount.toString());
//                         //     developer.log(customerID.toString());
//                         //     developer.log(widget.id.toString());
//                         //     developer.log(
//                         //         "Pooja OrderID Here Created:${val.entries.first.value.toString()}");
//                         //   }
//                         // });
//                         if (totalAmount > 0) {
//                           Navigator.of(context).push(
//                             SlideRightRoute(
//                                 page: ListenableProvider(
//                                     create: (context) => ListOfApisProvider(),
//                                     child: PaymentInformationScreen(
//                                       title: widget.,
//                                       astroId: widget.astrologer_id.toString(),
//                                       astroName: customerName.toString(),
//                                       price: totalAmount.toString(),
//                                       amountID: 0,
//                                       userID: customerID.toString(),
//                                       productID: widget.id,
//                                       from: '',
//                                       isProductOrPooja: true,
//                                     ))),
//                           );
//                         } else {
//                           Fluttertoast.showToast(
//                               msg: "You haven't selected any Pooja");
//                         }
//                       },
//                       child: Container(
//                           margin: EdgeInsets.symmetric(horizontal: 12),
//                           padding: EdgeInsets.symmetric(
//                               horizontal: 18, vertical: 12),
//                           width: double.infinity,
//                           decoration: BoxDecoration(
//                               color: AppColor.appColor,
//                               borderRadius: BorderRadius.circular(8)),
//                           child: Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: <Widget>[
//                                 Text("Total Amount: ₹ ${totalAmount} ",
//                                     style: GoogleFonts.poppins(fontSize: 16)),
//                                 Text("Confirm",
//                                     style: GoogleFonts.poppins(fontSize: 16)),
//                               ],
//                             ),
//                           )),
//                     ),
//                   ],
//                 ),
//               );
//             }),
//           );
//         });
//   }
// }
