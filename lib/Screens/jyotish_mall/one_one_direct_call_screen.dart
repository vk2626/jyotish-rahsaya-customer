import 'dart:async'; // Import async for Timer
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background/flutter_background.dart';
import '../../Core/helper_functions.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../Core/logger_helper.dart';

class OneOneDirectCallScreen extends StatefulWidget {
  final String token;
  final String channelName;
  final String astrologerName;
  final String astrologerImage;
  const OneOneDirectCallScreen(
      {super.key,
      required this.token,
      required this.channelName,
      required this.astrologerImage,
      required this.astrologerName});

  @override
  State<OneOneDirectCallScreen> createState() => _OneOneDirectCallScreenState();
}

class _OneOneDirectCallScreenState extends State<OneOneDirectCallScreen> {
  late RtcEngine agoraEngine;
  int? remoteUIds;
  bool isMuted = false;
  bool isSpeaker = false;
  bool isConnected = false;
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  Timer? callTimer;
  Duration callDuration = Duration.zero;

  @override
  void initState() {
    super.initState();
    setupVideoSDKEngine();
    agoraEngine = createAgoraRtcEngine();
  }

  Future<void> enableBackgroundExecution() async {
    try {
      const androidConfig = FlutterBackgroundAndroidConfig(
        notificationTitle: "Jyotish Rahsaya",
        notificationText: "Jyotish Rahsaya calling is under progress",
        notificationImportance: AndroidNotificationImportance.max,

        notificationIcon: AndroidResource(
            name: 'background_icon',
            defType: 'drawable'), // Default is ic_launcher from folder mipmap
      );
      bool success =
          await FlutterBackground.initialize(androidConfig: androidConfig);

      if (success) {
        await FlutterBackground.enableBackgroundExecution();
      } else {
        TLoggerHelper.error("Some Error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  @override
  void dispose() {
    stopCallTimer(); // Stop the timer if it's running
    if (FlutterBackground.isBackgroundExecutionEnabled) {
      FlutterBackground.disableBackgroundExecution();
    }

    agoraEngine.leaveChannel();
    agoraEngine.release();
    super.dispose();
  }

  Future<void> setupVideoSDKEngine() async {
    try {
      var microphoneStatus = await Permission.microphone.request();
      var cameraStatus = await Permission.camera.request();
      await enableBackgroundExecution();
      if (microphoneStatus.isDenied || cameraStatus.isDenied) {
        showPermissionSnackbar(context);
      }

      await agoraEngine.initialize(
          const RtcEngineContext(appId: "2b2b29ac0c8b40e88eae4516659054ed"));

      await agoraEngine.setDefaultAudioRouteToSpeakerphone(false);

      agoraEngine.registerEventHandler(
        RtcEngineEventHandler(
          onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
            showMessage(
                "Local user uid:${connection.localUid} joined the channel");
            TLoggerHelper.info(
                "Local user uid:${connection.localUid} joined the channel");
            setState(() {});
          },
          onUserJoined:
              (RtcConnection connection, int remoteUidd, int elapsed) {
            showMessage("Remote user uid:$remoteUidd joined the channel");
            TLoggerHelper.info(
                "Joined user uid:$remoteUidd joined the channel");
            setState(() {
              remoteUIds = remoteUidd;
              startCallTimer(); // Start the timer when user joins
              TLoggerHelper.info(
                  "Yeh hai remote uid jo ke user joined hone ke baad milta hai $remoteUidd");
            });
          },
          onUserOffline: (RtcConnection connection, int remoteUid,
              UserOfflineReasonType reason) {
            showMessage("Remote user uid:$remoteUid left the channel");
            setState(() {
              remoteUIds = null;
            });
            stopCallTimer(); // Stop the timer when user leaves
            leave();
          },
        ),
      );
      join();
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  void startCallTimer() {
    callTimer?.cancel();
    callTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        callDuration = callDuration + Duration(seconds: 1);
      });
    });
  }

  void stopCallTimer() {
    callTimer?.cancel();
  }

  leave() async {
    // stopCallTimer(); // Ensure timer stops when leaving
    FlutterBackground.disableBackgroundExecution();
    agoraEngine.leaveChannel();
    if (Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
    }
  }

  join() async {
    ChannelMediaOptions options = const ChannelMediaOptions(
        clientRoleType: ClientRoleType.clientRoleBroadcaster,
        channelProfile: ChannelProfileType.channelProfileCommunication);
    try {
      TLoggerHelper.debug(
          widget.channelName + "      --------      " + widget.token);
      agoraEngine.joinChannel(
          token: widget.token,
          channelId: widget.channelName,
          uid: int.parse(await THelperFunctions.getUserId()),
          options: options);
    } catch (e) {
      showMessage("Failed to join the channel: $e");
      TLoggerHelper.error(e.toString());
    }
  }

  showMessage(String message) {
    scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  void showPermissionSnackbar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: const Text('Microphone and Camera permission are required.'),
        action: SnackBarAction(
          label: 'Settings',
          onPressed: () {
            openAppSettings();
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String timerText =
        '${callDuration.inMinutes.toString().padLeft(2, '0')}:${(callDuration.inSeconds % 60).toString().padLeft(2, '0')}';

    return SafeArea(
      child: Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              'asset/images/rectangle.png',
              fit: BoxFit.cover,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text(
                      widget.astrologerName,
                      style: const TextStyle(
                          fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 18),
                    CircleAvatar(
                      radius: 72,
                      backgroundImage:
                          CachedNetworkImageProvider(widget.astrologerImage),
                    ),
                    const SizedBox(height: 18),
                    Text(
                      timerText, // Display the timer
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        IconButton(
                            onPressed: () async {
                              setState(() {
                                isMuted = !isMuted;
                              });
                              await agoraEngine.muteLocalAudioStream(isMuted);
                            },
                            icon: CircleAvatar(
                                backgroundColor: isMuted
                                    ? Colors.blueGrey.withOpacity(.6)
                                    : Colors.white,
                                radius: 26,
                                child: Icon(isMuted
                                    ? Icons.keyboard_voice_outlined
                                    : Icons.keyboard_voice_rounded))),
                        IconButton(
                            onPressed: () async {
                              setState(() {
                                isSpeaker = !isSpeaker;
                              });
                              await agoraEngine
                                  .setEnableSpeakerphone(isSpeaker);
                            },
                            icon: CircleAvatar(
                                backgroundColor: isSpeaker
                                    ? Colors.blueGrey.withOpacity(.6)
                                    : Colors.white,
                                radius: 26,
                                child: Icon(isSpeaker
                                    ? Icons.volume_up_rounded
                                    : Icons.volume_off))),
                      ],
                    ),
                    IconButton(
                        onPressed: () async {
                          await leave();
                        },
                        icon: const CircleAvatar(
                          radius: 32,
                          backgroundColor: Colors.red,
                          child: Icon(
                            Icons.call_end_rounded,
                            color: Colors.white,
                          ),
                        )),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
