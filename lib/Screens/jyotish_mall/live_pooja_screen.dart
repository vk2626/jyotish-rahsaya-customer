import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background/flutter_background.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../Core/helper_functions.dart';
import '../../Core/logger_helper.dart';
import 'package:permission_handler/permission_handler.dart';

class LivePoojaScreen extends StatefulWidget {
  final String token;
  final String channelName;
  final String astrologerName;
  final String astrologerImage;
  final String poojaId;
  final String astrologerId;

  const LivePoojaScreen(
      {super.key,
      required this.token,
      required this.channelName,
      required this.astrologerName,
      required this.astrologerImage,
      required this.poojaId,
      required this.astrologerId});

  @override
  State<LivePoojaScreen> createState() => _LivePoojaScreenState();
}

class _LivePoojaScreenState extends State<LivePoojaScreen> {
  late RtcEngine agoraEngine;
  int? remoteUIds;
  bool isMuted = false;
  bool isSpeaker = false;
  bool isConnected = false;
  bool isButtonClicked = false;
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    setupVideoSDKEngine();
    agoraEngine = createAgoraRtcEngine();
  }

  Future<void> enableBackgroundExecution() async {
    try {
      const androidConfig = FlutterBackgroundAndroidConfig(
        notificationTitle: "Jyotish Rahsaya",
        notificationText: "Jyotish Rahsaya calling is under progress",
        notificationImportance: AndroidNotificationImportance.max,

        notificationIcon: AndroidResource(
            name: 'background_icon',
            defType: 'drawable'), // Default is ic_launcher from folder mipmap
      );
      bool success =
          await FlutterBackground.initialize(androidConfig: androidConfig);

      if (success) {
        await FlutterBackground.enableBackgroundExecution();
      } else {
        TLoggerHelper.error("Some Error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  @override
  void dispose() {
    FlutterBackground.disableBackgroundExecution();
    agoraEngine.leaveChannel();
    // agoraEngine.release();
    super.dispose();
  }

  Future<void> setupVideoSDKEngine() async {
    try {
      var microphoneStatus = await Permission.microphone.request();
      var cameraStatus = await Permission.camera.request();
      await enableBackgroundExecution();
      if (microphoneStatus.isDenied || cameraStatus.isDenied) {
        showPermissionSnackbar(context);
      }

      await agoraEngine.initialize(
          const RtcEngineContext(appId: "2b2b29ac0c8b40e88eae4516659054ed"));

      await agoraEngine.enableVideo();
      await agoraEngine.startPreview();
      agoraEngine.registerEventHandler(
        RtcEngineEventHandler(
          onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
            showMessage(
                "Local user uid:${connection.localUid} joined the channel");
            TLoggerHelper.info(
                "Local user uid:${connection.localUid} joined the channel");
          },
          onUserJoined:
              (RtcConnection connection, int remoteUidd, int elapsed) {
            showMessage("Remote user uid:$remoteUidd joined the channel");
            TLoggerHelper.info(
                "Joined user uid:$remoteUidd joined the channel");
            if (mounted) {
              setState(() {
                remoteUIds = int.parse(widget.astrologerId);
                TLoggerHelper.info(
                    "Yeh hai remote uid jo ke user joined hone ke baad milta hai $remoteUidd");
              });
            }
          },
          onUserOffline: (RtcConnection connection, int remoteUid,
              UserOfflineReasonType reason) {
            showMessage("Remote user uid:$remoteUid left the channel");
            if (mounted) {
              setState(() {
                // remoteUIds = null;
              });
            }
            Navigator.of(context).pop();
          },
        ),
      );
      join();
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<void> join() async {
    try {
      ChannelMediaOptions options = const ChannelMediaOptions(
          clientRoleType: ClientRoleType.clientRoleAudience,
          channelProfile: ChannelProfileType.channelProfileLiveBroadcasting);
      int uid = int.parse(await THelperFunctions.getUserId());
      await agoraEngine.joinChannel(
          token: widget.token,
          channelId: widget.channelName,
          uid: uid,
          options: const ChannelMediaOptions(
              clientRoleType: ClientRoleType.clientRoleAudience,
              channelProfile:
                  ChannelProfileType.channelProfileLiveBroadcasting));
      TLoggerHelper.info("Token:-  " +
          widget.token +
          ", Channel Name:-  " +
          widget.channelName +
          "Astrologer Id is ${widget.astrologerId} UID:- $uid");
    } catch (e) {
      showMessage("Failed to join the channel: $e");
      TLoggerHelper.error(e.toString());
    }
  }

  void showPermissionSnackbar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: const Text('Microphone and Camera permission are required.'),
        action: SnackBarAction(
          label: 'Settings',
          onPressed: () {
            openAppSettings();
          },
        ),
      ),
    );
  }

  void showMessage(String message) {
    scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
      content: Text(message, style: GoogleFonts.poppins()),
    ));
  }

  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        fit: StackFit.expand,
        children: [
          if (remoteUIds != null) _remotePreview(mediaQuery),
        ],
      ),
    );
  }

  Widget _remotePreview(Size mediaQuery) {
    return AgoraVideoView(
      controller: VideoViewController.remote(
        rtcEngine: agoraEngine,
        canvas: VideoCanvas(uid: int.parse(widget.astrologerId)),
        connection: RtcConnection(channelId: widget.channelName),
      ),
    );
  }
}
