import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Core/helper_functions.dart';
import 'chatSupport/chat.support.screen.dart';
import 'mallScreens/astromall_allitem_screen.dart';
import 'mallScreens/mall_item_details_screen.dart';
import 'order.history.dart';
import '../router_constants.dart';
import '../Core/Model/AddToRecentListModel.dart';
import '../Core/Model/RecentSearchModel.dart' as recentSearchAlias;
import '../Core/Provider/go_live_provider.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Core/formatter.dart';
import '../utils/custom_circular_progress_indicator.dart';
import 'payment.info.dart';
import '../dialog/DeleteAllRecentSearchDialog.dart';
import '../dialog/showLoaderDialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/ApiServices.dart';
import '../Core/Api/Constants.dart';
import '../Core/Model/wallet.transection.model.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../utils/AppColor.dart';

import 'package:http/http.dart' as http;
import 'LoginScreen.dart';
import 'NavigationScreen.dart';
import 'ProfileScreen.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final TextEditingController searchController = TextEditingController();
  late FocusNode _searchFocusNode;
  bool showButtons = false;
  int _currentIndex = 0;
  String? astroID = "";
  String? astroName = "";
  String customerName = "";
  int customerID = 0;
  bool _isFlag = true;
  List<recentSearchAlias.AstrologerElement> recentAstrologers = [];
  List<recentSearchAlias.Product> recentPoojaProducts = [];
  bool isRecentSearchAvailable = false;

  @override
  void initState() {
    super.initState();
    _searchFocusNode = FocusNode();
    Future.delayed(Duration.zero, () {
      FocusScope.of(context).requestFocus(_searchFocusNode);
    });
    getUserId();
    THelperFunctions.logScreenNameEvent(screenName: "search_screen");
  }

  List<Map<String, dynamic>> services = [
    {
      "image": "asset/images/call.png",
      "label": "Call".tr(),
      "color": Colors.lightBlueAccent.withOpacity(.2),
      "page": MultiProvider(providers: [
        ListenableProvider(create: (context) => GoLiveProvider()),
        ListenableProvider(create: (context) => ListOfApisProvider())
      ], child: NavigationScreen(pageIndex: 3)),
    },
    {
      "image": "asset/images/chat.png",
      "label": "Chat".tr(),
      "color": Colors.purpleAccent.withOpacity(.2),
      "page": MultiProvider(providers: [
        ListenableProvider(create: (context) => GoLiveProvider()),
        ListenableProvider(create: (context) => ListOfApisProvider())
      ], child: NavigationScreen(pageIndex: 1)),
    },
    {
      "image": "asset/images/live.png",
      "label": "Live".tr(),
      "color": AppColor.btnActiveColorLight,
      "page": null,
    },
    {
      "image": "asset/images/bag.png",
      "label": "Jyotish Mall".tr(),
      "color": Colors.greenAccent.withOpacity(.2),
      "page": MultiProvider(providers: [
        ListenableProvider(create: (context) => ListOfApisProvider())
      ], child: AstroMallAllItemScreen(onChangedItem: (value) {})),
    },
  ];

  @override
  void dispose() {
    _searchFocusNode.dispose();
    super.dispose();
  }

  List<String> iconList = [
    "asset/images/call.png",
    "asset/images/chat.png",
    "asset/images/live.png",
    "asset/images/bag.png",
  ];
  List<String> iconNameList = [
    "Call".tr(),
    "Chat".tr(),
    "Live".tr(),
    "Jyotish Mall".tr(),
  ];

  void _performSearch(String name, ListOfApisProvider provider) async {
    try {
      await provider.searchProvider(name: name);
    } catch (e) {
      print("PerformSearchError --> $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget serviceContainer(
        BuildContext context, Map<String, dynamic> service) {
      return Container(
        height: 30,
        decoration: BoxDecoration(
          color: service['color'],
          borderRadius: BorderRadius.circular(15),
        ),
        child: GestureDetector(
          onTap: () {
            if (service['page'] != null) {
              Navigator.of(context)
                  .push(SlideRightRoute(page: service['page']));
            }
          },
          child: Row(
            children: <Widget>[
              SizedBox(width: 10),
              Image(image: AssetImage(service['image']), height: 15),
              Text("  ${service['label']}"),
              SizedBox(width: 10),
            ],
          ),
        ),
      );
    }

    // context.read<ListOfApisProvider>().searchProvider(name: "name");
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
              print("Back");
            },
          ),
          title: TextFormField(
            onChanged: (value) {
              setState(() {
                showButtons = value.isNotEmpty;
              });
              _performSearch(
                value,
                Provider.of<ListOfApisProvider>(context, listen: false),
              );
            },
            focusNode: _searchFocusNode,
            controller: searchController,
            decoration: InputDecoration(
              hintText: "Search Astrologer, Jyotish Mall Product".tr(),
              hintStyle: GoogleFonts.poppins(color: Colors.grey, fontSize: 14),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
            ),
            style: GoogleFonts.poppins(color: Colors.black, fontSize: 16),
          ),
          actions: [
            if (showButtons)
              IconButton(
                icon: Icon(Icons.close, color: Colors.black),
                onPressed: () {
                  searchController.clear();
                  setState(() {
                    showButtons = false;
                  });
                },
              ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Row(
                //     children: <Widget>[
                //       IconButton(
                //           onPressed: () {
                //             Navigator.pop(context);
                //             print("Back");
                //           },
                //           icon: Icon(Icons.arrow_back)),
                //       Expanded(
                //         child: Container(
                //           width: double.infinity,
                //           child: TextFormField(
                //             onChanged: (value) {
                //               setState(() {
                //                 showButtons = value.isNotEmpty;
                //               });
                //               _performSearch(
                //                   value,
                //                   Provider.of<ListOfApisProvider>(context,
                //                       listen: false));
                //             },
                //             focusNode: _searchFocusNode,
                //             autovalidateMode: AutovalidateMode.onUserInteraction,
                //             controller: searchController,
                //             keyboardType: TextInputType.emailAddress,
                //             decoration: InputDecoration(
                //               border: InputBorder.none,
                //               focusedBorder: InputBorder.none,
                //               enabledBorder: InputBorder.none,
                //               errorBorder: InputBorder.none,
                //               disabledBorder: InputBorder.none,
                //               hintText: "Search Astrologer, Jyotish Mall Product",
                //               labelStyle: const GoogleFonts.poppins(
                //                   color: AppColor.appColor,
                //                   fontWeight: FontWeight.bold,
                //                   fontSize: 16),
                //               hintStyle: const GoogleFonts.poppins(fontSize: 14),
                //             ),
                //           ),
                //         ),
                //       ),
                //       IconButton(
                //           onPressed: () {
                //             searchController.clear();
                //             setState(() {
                //               showButtons = false;
                //             });
                //           },
                //           icon: Icon(Icons.close)),
                //     ],
                //   ),
                // ),
                // Divider(),
                if (showButtons)
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          side: BorderSide(
                                            color: _isFlag
                                                ? AppColor.btnActiveColorLight
                                                : AppColor.borderColor,
                                          ))),
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                    _isFlag
                                        ? AppColor.btnActiveColorLight
                                        : AppColor.appBackgroundLightGrey,
                                  )),
                              onPressed: () {
                                setState(() {
                                  _currentIndex = 0;
                                  _isFlag = true;
                                });
                                print("Astrologer button pressed");
                              },
                              child: Text(
                                "Astrologer".tr(),
                                style: GoogleFonts.poppins(
                                    color: AppColor.darkGrey),
                              ),
                            ),
                            ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          side: BorderSide(
                                            color: !_isFlag
                                                ? AppColor.btnActiveColorLight
                                                : AppColor.borderColor,
                                          ))),
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                    !_isFlag
                                        ? AppColor.btnActiveColorLight
                                        : AppColor.appBackgroundLightGrey,
                                  )),
                              onPressed: () {
                                setState(() {
                                  _currentIndex = 1;
                                  _isFlag = false;
                                });
                                print("Jyotishmall button pressed");
                              },
                              child: Text("Jyotishmall Products".tr(),
                                  style: GoogleFonts.poppins(
                                      color: AppColor.darkGrey)),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: _currentIndex == 0,
                        child: Consumer<ListOfApisProvider>(
                            builder: (context, search1, _) {
                          if (search1.isLoading) {
                            return Center(
                              child: CustomCircularProgressIndicator(),
                            );
                          }
                          final search1Data = search1.searchModel;
                          if (search1Data.filter?.astrologers?.isEmpty ??
                              true) {
                            // Display a message when no products are found
                            return Center(
                              child: Container(
                                width: double.infinity,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 25,
                                    ),
                                    Image.asset("asset/icons/not found.gif"),
                                    Text(
                                      "Oops! No astrologer found".tr() +
                                          "\n " +
                                          "try searching something else.".tr(),
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 20),
                                    Text("Popular searches".tr()),
                                    SizedBox(height: 10),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              color: Colors.lightBlueAccent
                                                  .withOpacity(.2),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/call.png"),
                                                  height: 20),
                                              Text(
                                                "Call".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              color: Colors.purpleAccent
                                                  .withOpacity(.2),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/chat.png"),
                                                  height: 20),
                                              Text(
                                                "Chat".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              color:
                                                  AppColor.btnActiveColorLight,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/live.png"),
                                                  height: 20),
                                              Text(
                                                "Live".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          decoration: BoxDecoration(
                                              color: Colors.greenAccent
                                                  .withOpacity(.2),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/bag.png"),
                                                  height: 20),
                                              Text(
                                                "Jyotish Mall".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          }
                          return ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount:
                                  search1Data.filter?.astrologers?.length ?? 0,
                              itemBuilder: (context, indexAstro) {
                                final astrologerSearch = search1Data
                                    .filter?.astrologers?[indexAstro];
                                return Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, top: 2),
                                  child: InkWell(
                                    onTap: () {
                                      astroName = astrologerSearch.name;
                                      astroID = astrologerSearch.id.toString();
                                      _searchFocusNode.unfocus();

                                      context.pushNamed(
                                        RouteConstants.astrologerProfile,
                                        queryParameters: {
                                          'astrologerId':
                                              astrologerSearch.id.toString(),
                                          'type': 'chat',
                                          'isAstrologerOnline': astrologerSearch
                                              .isOnlineAstrologer
                                              .toString()
                                        },
                                      ).then(
                                        (value) {
                                          getRecentSearch();
                                        },
                                      );

                                      recentSearch(astroID.toString(), "");
                                      // Navigator.of(context).push(
                                      //     SlideRightRoute(
                                      //
                                    },
                                    child: Card(
                                      color: Colors.white,
                                      elevation: 2,
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                      ),
                                      child: Stack(
                                        children: [
                                          Row(
                                            children: [
                                              SizedBox(width: 10),
                                              Column(
                                                children: <Widget>[
                                                  SizedBox(height: 10),
                                                  Stack(
                                                    children: [
                                                      Card(
                                                        clipBehavior: Clip
                                                            .antiAliasWithSaveLayer,
                                                        child: CircleAvatar(
                                                          radius: 40,
                                                          backgroundImage:
                                                              CachedNetworkImageProvider(
                                                                  "${astrologerSearch?.avatar}"),
                                                        ),
                                                        shape: CircleBorder(
                                                            side: BorderSide(
                                                                color: AppColor
                                                                    .appColor,
                                                                width: 2)),
                                                      ),
                                                      if (astrologerSearch!
                                                              .label !=
                                                          0)
                                                        Positioned(
                                                          bottom: -4,
                                                          left: 0,
                                                          right: 0,
                                                          child: Center(
                                                            child: Card(
                                                              color:
                                                                  Colors.amber,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius
                                                                              .all(
                                                                        Radius.circular(
                                                                            10.0),
                                                                      ),
                                                                      side: BorderSide(
                                                                          color:
                                                                              Colors.yellow)),
                                                              child: Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            12,
                                                                        right:
                                                                            12,
                                                                        bottom:
                                                                            3,
                                                                        top: 2),
                                                                child: Text(
                                                                    TFormatter.capitalizeSentence(astrologerSearch!
                                                                        .astrologer
                                                                        .labelText),
                                                                    maxLines: 1,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .fade,
                                                                    style: GoogleFonts.poppins(
                                                                        fontSize:
                                                                            8,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        color: Colors
                                                                            .white)),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                    ],
                                                  ),
                                                  RatingBarIndicator(
                                                      rating: astrologerSearch!
                                                          .astrologer.rating,
                                                      itemCount: 5,
                                                      itemSize: 15.0,
                                                      itemBuilder: (context,
                                                              _) =>
                                                          const Icon(
                                                            Icons.star,
                                                            color: Colors.amber,
                                                          )),
                                                  Text(
                                                    "${astrologerSearch?.astrologer?.orderCount} " +
                                                        "orders".tr(),
                                                    style: GoogleFonts.poppins(
                                                        color: Colors.grey[600],
                                                        fontSize: 12),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  SizedBox(height: 10),
                                                ],
                                              ),
                                              SizedBox(width: 10),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(height: 10),
                                                    Text(
                                                      "${astrologerSearch?.name}",
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                    ),
                                                    Text(
                                                        "${astrologerSearch?.astrologer?.allSkillName}",
                                                        style:
                                                            GoogleFonts.poppins(
                                                          color:
                                                              Colors.grey[600],
                                                        ),
                                                        maxLines: 3),
                                                    Text(
                                                      "Exp".tr() +
                                                          " : ${astrologerSearch?.astrologer?.experienceYear} " +
                                                          "yr".tr(),
                                                      style:
                                                          GoogleFonts.poppins(
                                                        color: Colors.grey[600],
                                                      ),
                                                    ),
                                                    Text.rich(
                                                      TextSpan(
                                                        children: [
                                                          TextSpan(
                                                            text: "₹",
                                                            style: GoogleFonts
                                                                .poppins(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: Colors
                                                                  .grey[600],
                                                              // Add line through original price
                                                            ),
                                                          ),
                                                          TextSpan(
                                                            text:
                                                                "${astrologerSearch?.astrologer.chatPrice == "0" ? "0" : astrologerSearch?.astrologer.chatPrice}",
                                                            style: GoogleFonts
                                                                .poppins(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: Colors
                                                                  .grey[600],
                                                              decoration: astrologerSearch
                                                                          ?.astrologer
                                                                          .chatDiscountPrice ==
                                                                      0
                                                                  ? TextDecoration
                                                                      .none
                                                                  : astrologerSearch
                                                                              ?.astrologer
                                                                              .chatDiscountPrice !=
                                                                          "0"
                                                                      ? TextDecoration
                                                                          .lineThrough
                                                                      : TextDecoration
                                                                          .none, // Add line through original price
                                                            ),
                                                          ),
                                                          if (astrologerSearch
                                                                  ?.astrologer
                                                                  .chatDiscountPrice !=
                                                              "0")
                                                            TextSpan(
                                                              text: astrologerSearch
                                                                          .astrologer
                                                                          .chatDiscountPrice ==
                                                                      0
                                                                  ? ""
                                                                  : " ${astrologerSearch.astrologer.chatDiscountPrice}",
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                color:
                                                                    Colors.red,
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                        ],
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(height: 10),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(width: 10),
                                              SizedBox(
                                                height: 100,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 35,
                                                      width: 80,
                                                      child: TextButton(
                                                        onPressed: () {
                                                          astroName =
                                                              astrologerSearch
                                                                  .name;
                                                          astroID =
                                                              astrologerSearch
                                                                  .id
                                                                  .toString();

                                                          context.pushNamed(
                                                            RouteConstants
                                                                .astrologerProfile,
                                                            queryParameters: {
                                                              'astrologerId':
                                                                  astrologerSearch
                                                                      .id
                                                                      .toString(),
                                                              'type': 'chat',
                                                            },
                                                          ).then(
                                                            (value) {
                                                              getRecentSearch();
                                                            },
                                                          );
                                                          recentSearch(
                                                              astroID
                                                                  .toString(),
                                                              "");
                                                          // Navigator.of(context).push(
                                                          //     SlideRightRoute(
                                                          //         page: ChattingScreen()));
                                                        },
                                                        style: TextButton.styleFrom(
                                                            side: BorderSide(
                                                                color: Colors
                                                                    .greenAccent),
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5))),
                                                        child: Text(
                                                          "Chat".tr(),
                                                          style: GoogleFonts
                                                              .poppins(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .green),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(width: 10),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              });
                        }),
                      ),
                      Visibility(
                        visible: _currentIndex == 1,
                        child: Consumer<ListOfApisProvider>(
                            builder: (context, search2, _) {
                          if (search2.isLoading) {
                            return Center(
                              child: CustomCircularProgressIndicator(),
                            );
                          }
                          var search2Data = search2.searchModel;

                          if (search2Data.filter?.products?.isEmpty ?? true) {
                            // Display a message when no products are found
                            return Center(
                              child: Container(
                                width: double.infinity,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 25,
                                    ),
                                    Image.asset("asset/icons/not found.gif"),
                                    Text(
                                      "Oops! No products found".tr() +
                                          "\n" +
                                          "try searching something else".tr(),
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 20),
                                    Text(
                                      "Popular searches".tr(),
                                      style: GoogleFonts.poppins(),
                                    ),
                                    SizedBox(height: 10),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              color: Colors.lightBlueAccent
                                                  .withOpacity(.2),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/call.png"),
                                                  height: 20),
                                              Text(
                                                "Call".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              color: Colors.purpleAccent
                                                  .withOpacity(.2),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/chat.png"),
                                                  height: 20),
                                              Text(
                                                "Chat".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              color:
                                                  AppColor.btnActiveColorLight,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/live.png"),
                                                  height: 20),
                                              Text(
                                                "Live".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Container(
                                          height: 60,
                                          decoration: BoxDecoration(
                                              color: Colors.greenAccent
                                                  .withOpacity(.2),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                  image: AssetImage(
                                                      "asset/images/bag.png"),
                                                  height: 20),
                                              Text(
                                                "Jyotish Mall".tr(),
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          }

                          return ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount:
                                  search2Data.filter?.products?.length ?? 0,
                              itemBuilder: (context, indexPro) {
                                final product =
                                    search2Data.filter?.products?[indexPro];
                                return Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, top: 2),
                                  child: Card(
                                    color: Colors.white,
                                    elevation: 2,
                                    clipBehavior: Clip.antiAliasWithSaveLayer,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                    ),
                                    child: Row(
                                      children: [
                                        SizedBox(width: 10),
                                        Column(
                                          children: <Widget>[
                                            SizedBox(height: 10),
                                            Card(
                                              clipBehavior:
                                                  Clip.antiAliasWithSaveLayer,
                                              child: CircleAvatar(
                                                radius: 40,
                                                backgroundImage:
                                                    CachedNetworkImageProvider(
                                                        "${product?.image}"),
                                              ),
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: AppColor.appColor,
                                                      width: 2)),
                                            ),
                                          ],
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              SizedBox(height: 10),
                                              Text(
                                                "${product?.title}",
                                                style: GoogleFonts.poppins(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                  "${product?.shortDescription}",
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                    fontSize: 12,
                                                  ),
                                                  maxLines: 3),
                                              Text("₹ ${product?.price}",
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                  )),
                                              SizedBox(height: 10),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        SizedBox(
                                          height: 100,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              SizedBox(
                                                height: 35,
                                                width: 80,
                                                child: TextButton(
                                                  onPressed: () {
                                                    astroName = product?.title;
                                                    astroID =
                                                        product?.id.toString();
                                                    //_rechargeDialog(context,"wallet");
                                                    Navigator.of(context)
                                                        .push(SlideRightRoute(
                                                            page:
                                                                ListenableProvider(
                                                          create: (context) =>
                                                              ListOfApisProvider(),
                                                          child:
                                                              MallItemDetailsScreen(
                                                            remedyId: product!
                                                                .id
                                                                .toString(),
                                                            productName:
                                                                product!.title
                                                                    .toString(),
                                                          ),
                                                        )))
                                                        .then((value) =>
                                                            getRecentSearch());

                                                    recentSearch("",
                                                        product.id.toString());
                                                    // Navigator.of(context).push(
                                                    //     SlideRightRoute(
                                                    //         page: ChattingScreen()));
                                                  },
                                                  style: TextButton.styleFrom(
                                                      side: BorderSide(
                                                          color: Colors
                                                              .greenAccent),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5))),
                                                  child: Text(
                                                    "Buy".tr(),
                                                    style: GoogleFonts.poppins(
                                                        fontSize: 16,
                                                        color: Colors.green),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: 10),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                      ],
                                    ),
                                  ),
                                );
                              });
                        }),
                      ),
                    ],
                  ),
                if (!showButtons)
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Top Services".tr(),
                          style: GoogleFonts.poppins(fontSize: 14),
                        ),
                        SizedBox(height: 10),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 30,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              SizedBox(width: 10),
                              ...services.map((service) => Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: serviceContainer(context, service),
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Quick Link".tr(),
                          style: GoogleFonts.poppins(fontSize: 14),
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: GestureDetector(
                                  onTap: () {
                                    // Navigator.of(context).push(
                                    //     SlideRightRoute(
                                    //         page: NavigationScreen(page: 5)));
                                    Navigator.of(context).push(SlideRightRoute(
                                        page: ListenableProvider(
                                      create: (context) => ListOfApisProvider(),
                                      child:
                                          OrderHistoryScreen(selected_index: 0),
                                    )));
                                  },
                                  child: Container(
                                    height: 100,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.black, width: 2),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Image(
                                            image: AssetImage(
                                                "asset/images/wallet.png"),
                                            height: 20),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          "Wallet".tr(),
                                          style:
                                              GoogleFonts.poppins(fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  )),
                              flex: 1,
                            ),
                            SizedBox(width: 5),
                            Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(SlideRightRoute(
                                        page: ListenableProvider(
                                      create: (context) => ListOfApisProvider(),
                                      child: ChatSupportScreen(),
                                    )));
                                  },
                                  child: Container(
                                    height: 100,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.black, width: 2),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Image(
                                            image: AssetImage(
                                                "asset/images/customer_support.png"),
                                            height: 20),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          "Customer".tr() +
                                              "\n" +
                                              "Support".tr(),
                                          textAlign: TextAlign.center,
                                          style:
                                              GoogleFonts.poppins(fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  ),
                                )),
                            SizedBox(width: 5),
                            Expanded(
                                flex: 1,
                                child: GestureDetector(
                                    onTap: () {
                                      Navigator.of(context)
                                          .push(SlideRightRoute(
                                              page: ListenableProvider(
                                        create: (context) =>
                                            ListOfApisProvider(),
                                        child: OrderHistoryScreen(
                                            selected_index: 1),
                                      )));
                                    },
                                    child: Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.black, width: 2),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Image(
                                              image: AssetImage(
                                                  "asset/images/order_history.png"),
                                              height: 20),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text(
                                            "Order".tr() +
                                                "\n" +
                                                "History".tr(),
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(
                                                fontSize: 14),
                                          ),
                                        ],
                                      ),
                                    ))),
                            SizedBox(width: 5),
                            Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    if (customerID == 0) {
                                      Navigator.of(context).push(
                                          SlideRightRoute(page: LoginScreen()));
                                    } else {
                                      Navigator.of(context).push(
                                          SlideRightRoute(
                                              page: ProfileScreen()));
                                    }
                                  },
                                  child: Container(
                                    height: 100,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.black, width: 2),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Image(
                                            image: AssetImage(
                                                "asset/images/person.png"),
                                            height: 20),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          "Profile".tr(),
                                          style:
                                              GoogleFonts.poppins(fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  ),
                                )),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Visibility(
                          visible: recentPoojaProducts.length > 0 ||
                              recentAstrologers.length > 0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Recent Searches".tr(),
                                style: GoogleFonts.poppins(fontSize: 14),
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.delete_forever_rounded,
                                  color: Colors.red[800],
                                  size: 20,
                                ),
                                onPressed: () async {
                                  var result = await showDialog(
                                      context: context,
                                      builder: (context) => DeleteAll(context));
                                  print("Result---> ${result.toString()}");
                                  if (result == "yes") removeRecent("", "");
                                },
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: recentAstrologers.length > 0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Astrologers'.tr(),
                                style: GoogleFonts.poppins(fontSize: 14),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10, bottom: 10),
                                width: double.infinity,
                                height: 140,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemCount: recentAstrologers.length,
                                  itemBuilder: (BuildContext context, int i) {
                                    return Container(
                                      height: 140,
                                      width: 120,
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            child: Card(
                                              color: Colors.white,
                                              elevation: 2,
                                              clipBehavior:
                                                  Clip.antiAliasWithSaveLayer,
                                              child: GestureDetector(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Align(
                                                      alignment:
                                                          Alignment.center,
                                                      child: CircleAvatar(
                                                        radius: 35,
                                                        backgroundImage:
                                                            Image.network(
                                                                    fit: BoxFit
                                                                        .cover,
                                                                    "${recentAstrologers[i].avatar}")
                                                                .image,
                                                      ),
                                                    ),
                                                    SizedBox(height: 10),
                                                    Text(
                                                      "${recentAstrologers[i].name}",
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14),
                                                      textAlign:
                                                          TextAlign.center,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 2,
                                                    ),
                                                  ],
                                                ),
                                                onTap: () {
                                                  context.pushNamed(
                                                    RouteConstants
                                                        .astrologerProfile,
                                                    queryParameters: {
                                                      'astrologerId':
                                                          recentAstrologers[i]
                                                              .id
                                                              .toString(),
                                                      'type': 'chat',
                                                    },
                                                  );
                                                },
                                              ),
                                            ),
                                            right: 0,
                                            left: 0,
                                            top: 0,
                                            bottom: 0,
                                          ),
                                          Positioned(
                                            right: -5,
                                            top: -5,
                                            child: IconButton(
                                                onPressed: () {
                                                  removeRecent(
                                                      recentAstrologers[i]
                                                          .id
                                                          .toString(),
                                                      "");
                                                },
                                                icon: Icon(Icons.close)),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        Visibility(
                          visible: recentPoojaProducts.length > 0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Jyotish Mall Products'.tr()),
                              Container(
                                margin: EdgeInsets.only(top: 10, bottom: 10),
                                width: double.infinity,
                                height: 140,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemCount: recentPoojaProducts.length,
                                  itemBuilder: (BuildContext context, int i) {
                                    return Container(
                                      height: 140,
                                      width: 120,
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            child: Card(
                                              color: Colors.white,
                                              elevation: 2,
                                              clipBehavior:
                                                  Clip.antiAliasWithSaveLayer,
                                              child: GestureDetector(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Align(
                                                      alignment:
                                                          Alignment.center,
                                                      child: CircleAvatar(
                                                        radius: 35,
                                                        backgroundImage:
                                                            CachedNetworkImageProvider(
                                                                "${recentPoojaProducts[i].image}"),
                                                      ),
                                                    ),
                                                    SizedBox(height: 10),
                                                    Text(
                                                      "${recentPoojaProducts[i].title}",
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color:
                                                                  Colors.black),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ],
                                                ),
                                                onTap: () {
                                                  Navigator.of(context).push(
                                                      SlideRightRoute(
                                                          page:
                                                              ListenableProvider(
                                                    create: (context) =>
                                                        ListOfApisProvider(),
                                                    child:
                                                        MallItemDetailsScreen(
                                                      remedyId:
                                                          recentPoojaProducts[i]
                                                              .id
                                                              .toString(),
                                                      productName:
                                                          recentPoojaProducts[i]
                                                              .title
                                                              .toString(),
                                                    ),
                                                  )));
                                                },
                                              ),
                                            ),
                                            right: 0,
                                            left: 0,
                                            top: 0,
                                            bottom: 0,
                                          ),
                                          Positioned(
                                            right: -5,
                                            top: -5,
                                            child: IconButton(
                                                onPressed: () {
                                                  removeRecent(
                                                      "",
                                                      recentPoojaProducts[i]
                                                          .id
                                                          .toString());
                                                },
                                                icon: Icon(Icons.close)),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(""),
                ),
              ]),
        ));
  }

  /********************* Recharge Dialog ************************/
  Future<void> _rechargeDialog(
      BuildContext context, String s, String name, int amountPerMinute) async {
    int? amID = 0;
    String? amount = "";
    // context.read<PaymentServices>().checkMeOUT(
    //     context: context,
    //     rechargeAmount: amount.toString(),
    //     astroName: astroName);
    context
        .read<ListOfApisProvider>()
        .getRechargeAmountProvider(context: context);
    await showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Consumer<ListOfApisProvider>(builder: (context, state, _) {
          if (state.isLoading) {
            return Center(
              child: CustomCircularProgressIndicator(),
            );
          }
          var rechargeData = state.rechargeAmountModel.recharge ?? [];
          log("Recharge Am --> ${rechargeData.length}");
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(alignment: Alignment.topRight, child: CloseButton()),
                Text(
                  "Minimum balance of 5 minutes".tr() +
                      " is required to start chat with".tr() +
                      " (UserName)",
                  textAlign: TextAlign.start,
                  style: GoogleFonts.poppins(
                      fontSize: 14, color: Colors.redAccent),
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Recharge Now'.tr(),
                    style: GoogleFonts.poppins(fontSize: 17),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.light_mode_rounded,
                      color: AppColor.appColor,
                    ),
                    Text("Tip: 90% user recharge for 10 mins or more".tr())
                  ],
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: rechargeData.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          childAspectRatio: 6 / 2,
                          mainAxisSpacing: 10,
                          crossAxisSpacing: 10),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(SlideRightRoute(
                                page: PaymentInformationScreen(
                              userInfoId: "",
                              astroName:
                                  TFormatter.capitalizeSentence(customerName),
                              title: "Recharge Amount".tr(),
                              price: rechargeData[index].price!,
                              amountID: rechargeData[index].id!,
                              from: s,
                              astroId: astroID.toString(),
                            )));

                            print("customerID -----> $customerID");
                            print("Recharge Of---->${astroName}");
                            print(rechargeData[index].price);
                            amID = rechargeData[index].id;
                            amount = rechargeData[index].price;
                            print(rechargeData[index].price);

                            print("Amount ID --> ${amID}");
                            print("Amount ID --> ${amID.runtimeType}");
                            print("Amount  --> $amount");
                            print("click");
                          },
                          child: Container(
                              height: 25,
                              width: 90,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: AppColor.appColor)),
                              child: Stack(children: <Widget>[
                                Positioned(
                                    top: 7,
                                    right: 50.5,
                                    child: Transform.rotate(
                                        angle: -math.pi / 3.5,
                                        child: Container(
                                            height: 15,
                                            width: 50,
                                            decoration: BoxDecoration(
                                                color: Colors.green,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Center(
                                                child: Text(
                                                    "${rechargeData[index].discountPercentage}% Extra",
                                                    style: GoogleFonts.poppins(
                                                        fontSize: 8)))))),
                                Center(
                                    child: Text("${rechargeData[index].price}"))
                              ])),
                        );
                      }),
                ),
                SizedBox(height: 20),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      backgroundColor: AppColor.appColor,
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: Text('Proceed to Pay'.tr()),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          );
        });
      },
    );
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.getInt(Constants.userID) != null) {
      customerID = prefs.getInt(Constants.userID)!;
      if (prefs.getString(Constants.fullName) != null) {
        customerName = prefs.getString(Constants.fullName)!;
      }
      getRecentSearch();
    }
  }

  DioClient? client;
  int walletAmount = 0;

  void getWalletAmount() async {
    client = DioClient();
    Dio dio = await client!.getClient();

    var params = jsonEncode({'user_id': customerID});
    String api = UrlConstants.walletTranscation;
    final url = UrlConstants.baseUrl + api;
    WalletTransactionModel? walletTransactionModel =
        await client?.walletTransaction(dio, url, params);

    if (walletTransactionModel!.status == true) {
      setState(() {
        walletAmount = (walletTransactionModel.walletList!.length > 0
            ? walletTransactionModel.walletList?.first.balance != null
                ? walletTransactionModel.walletList?.first.balance
                : 0
            : 0)!;
        print("walletAmount----> $walletAmount");
      });
    }
  }

  void recentSearch(String id, String productId) async {
    if (customerID > 0) {
      client = DioClient();
      Dio dio = await client!.getClient();

      var params = jsonEncode({
        'customer_id': customerID,
        'astrologer_id': id,
        'product_id': productId,
        "product_pooja_id": ""
      });
      String api = UrlConstants.recentSearch;
      final url = UrlConstants.baseUrl + api;
      AddToRecentListModel? addToRecentListModel =
          await client?.addToRecentList(dio, url, params);
      print(addToRecentListModel?.status);
    }
  }

  void getRecentSearch() async {
    client = DioClient();
    Dio dio = await client!.getClient();

    var params = jsonEncode({'user_id': customerID});
    String api = UrlConstants.astrologerRecentList;
    final url = UrlConstants.baseUrl + api;
    recentSearchAlias.RecentSearchModel? recentSearchModel =
        await client?.recentSearch(dio, url, params);
    recentAstrologers = recentSearchModel!.astrologers;
    recentPoojaProducts = recentSearchModel.products;
    setState(() {});
  }

  void removeRecent(String astroId, String productId) async {
    Future.delayed(Duration.zero, () {
      showLoaderDialog(context);
    });

    client = DioClient();
    Dio dio = await client!.getClient();

    Map<String, String> body = {
      'customer_id': await THelperFunctions.getUserId(),
      'astrologer_id': astroId,
      'product_id': productId,
    };
    String api = "recent-delete";

    // recentSearchAlias.RecentSearchModel? recentSearchModel =
    final Map<String, dynamic> parseData = await jsonDecode(
        await THelperFunctions.httpPostHandler(
            client: http.Client(), endPointUrl: api, body: body));
    if (parseData['status']) {
      getRecentSearch();
      Navigator.pop(context);
    }
  }
}
