import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../dialog/showLoaderDialog.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../utils/AppColor.dart';

import 'liveScreen/live.upcoming.screen.dart';

class LiveAstrologers extends StatefulWidget {
  const LiveAstrologers({super.key});

  @override
  State<LiveAstrologers> createState() => _LiveAstrologersState();
}

class _LiveAstrologersState extends State<LiveAstrologers> {
  List<String> _listName = [
    "Tarot",
    "New",
    "Trending",
    "Vedic",
    "Face Reading",
    "Palmistry",
    "Numerology",
    "All",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: LayoutBuilder(builder: (context, BoxConstraints constraints) {
        return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 10),
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: _listName.length,
                  itemBuilder: (context, index) {
                    return Container(
                      // color: Colors.greenAccent,
                      height: 250,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Text(
                              "${_listName[index]}",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            height: 200,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: _listName.length,
                                itemBuilder: (context, innerIndex) {
                                  return Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, right: 8.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: CachedNetworkImageProvider(
                                                      "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80"),
                                                  fit: BoxFit.cover),
                                              color: Colors.white70,
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          height: 170,
                                          width: 120,
                                        ),
                                      ),
                                    ],
                                  );
                                }),
                          ),
                        ],
                      ),
                    );
                  }),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Upcoming Astrologers",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                              SlideRightRoute(page: LiveUpcomingScreen()));
                        },
                        child: Text(
                          "View All".tr(),
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 200,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 7,
                        itemBuilder: (context, innerIndex) {
                          return Stack(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 170,
                                  width: 120,
                                  decoration: BoxDecoration(
                                      color: Colors.white70,
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: ImageFiltered(
                                      imageFilter: ImageFilter.blur(
                                          sigmaX: 2, sigmaY: 2),
                                      child: Container(
                                        height: 175,
                                        width: 120,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image: CachedNetworkImageProvider(
                                                    "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80")),
                                            color: Colors.white70,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 160,
                                left: 100,
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Icon(Icons.share,
                                      color: AppColor.whiteColor),
                                ),
                              ),
                              Positioned(
                                bottom: 120,
                                left: 35,
                                child: Container(
                                  height: 60,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border:
                                          Border.all(color: AppColor.appColor)),
                                ),
                              ),
                              Positioned(
                                bottom: 100,
                                left: 35,
                                child: Text(
                                  "User Name",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: AppColor.darkGrey),
                                ),
                              ),
                              Positioned(
                                bottom: 65,
                                left: 27,
                                child: Text(
                                  "Date Mon,Day,\nTime :PM",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: AppColor.darkGrey),
                                ),
                              ),
                              Positioned(
                                bottom: 40,
                                left: 27,
                                child: SizedBox(
                                  height: 20,
                                  width: 90,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          backgroundColor: Colors.transparent,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          side: BorderSide(
                                              color: AppColor.appColor)),
                                      onPressed: () {},
                                      child: Text(
                                        "Join Waitlist",
                                        textAlign: TextAlign.center,
                                      )),
                                ),
                              )
                            ],
                          );
                        }),
                  ),
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
