import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import '../main.dart';
import '../router_constants.dart';
import '../Core/Provider/authentication.provider.dart';
import 'package:provider/provider.dart';
import '../Core/Api/authentication.apis.dart';
import '../utils/AppColor.dart';
import 'package:pinput/pinput.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Core/Api/Constants.dart';
import '../CustomNavigator/SlideRightRoute.dart';
import '../dialog/showLoaderDialog.dart';
import 'NavigationScreen.dart';

class OTPScreen extends StatefulWidget {
  String mobile, verificationId;

  OTPScreen(this.mobile, this.verificationId);

  @override
  State<StatefulWidget> createState() {
    return _OTPScreenState();
  }
}

class _OTPScreenState extends State<OTPScreen> {
  final otpFormKey = GlobalKey<FormState>();
  int secondsRemaining = 30;
  bool enableResend = false;
  late Timer timer;
  final pinController = TextEditingController();
  String otpCode = "";
  bool getOTP = true;
  String otp = "", authStatus = "", deviceToken = "";
  int userId = 0;

  late AuthenticationProvider authenticationProvider = AuthenticationProvider();

  // Future<void> verifyPhoneNumber(BuildContext context) async {
  //   await FirebaseAuth.instance.verifyPhoneNumber(
  //     phoneNumber: "+91" + widget.mobile,
  //     timeout: const Duration(seconds: 15),
  //     verificationCompleted: (AuthCredential authCredential) {
  //       setState(() {
  //         authStatus = "Your account is successfully verified";
  //       });
  //     },
  //     verificationFailed: (FirebaseAuthException authException) {
  //       setState(() {
  //         authStatus = "Authentication failed";
  //       });
  //     },
  //     codeSent: (String verId, [int? forceCodeResent]) {
  //       widget.verificationId = verId;
  //       setState(() {
  //         authStatus = "OTP has been successfully send";
  //       });
  //       if (!mounted) return;
  //       Navigator.of(context).pop();
  //       timer = Timer.periodic(Duration(seconds: 1), (_) {
  //         if (secondsRemaining != 0) {
  //           setState(() {
  //             secondsRemaining--;
  //           });
  //         } else {
  //           setState(() {
  //             enableResend = true;
  //           });
  //         }
  //       });
  //       //showLoaderDialog(context);
  //       setState(() {
  //         getOTP = true;
  //       });
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(content: Text(authStatus)),
  //       );
  //       //otpDialogBox(context).then((value) {});
  //     },
  //     codeAutoRetrievalTimeout: (String verId) {
  //       widget.verificationId = verId;
  //       setState(() {
  //         authStatus = "TIMEOUT";
  //       });
  //     },
  //   );
  // }

  // Future<void> signIn(String otp) async {
  //   await FirebaseAuth.instance
  //       .signInWithCredential(PhoneAuthProvider.credential(
  //     verificationId: widget.verificationId,
  //     smsCode: otp,
  //   ))
  //       .then((value) async {
  //     if (!mounted) return;
  //     Navigator.of(context).pop();
  //     setState(() {
  //       getOTP = false;
  //       enableResend = false;
  //       timer.cancel();
  //       secondsRemaining = 30;
  //       //prefs.setString(Constants.tempUserID, (verifyOtp.data!.id).toString());
  //     });
  //     FirebaseAuth.instance.signOut();
  //     final prefs = await SharedPreferences.getInstance();
  //     await prefs.setInt(Constants.userID, userId);
  //
  //     Navigator.of(context).pushAndRemoveUntil(
  //         SlideRightRoute(page: NavigationScreen(page: 0)),
  //         ModalRoute.withName("/"));
  //   }).catchError((error) {
  //     if (!mounted) return;
  //     Navigator.of(context).pop();
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       SnackBar(content: Text("Incorrect OTP. Please try with valid OTP.")),
  //     );
  //   });
  // }

  @override
  initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (_) {
      if (secondsRemaining != 0) {
        setState(() {
          secondsRemaining--;
        });
      } else {
        setState(() {
          enableResend = true;
        });
      }
    });
    getUserId();
  }

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 50,
      height: 50,
      textStyle:  GoogleFonts.poppins(
          fontSize: 18,
          color: Color.fromRGBO(30, 60, 87, 1),
          fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: AppColor.appColor),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration?.copyWith(
        color: Colors.white,
        border: Border.all(color: AppColor.appColor),
      ),
    );

    return Scaffold(backgroundColor: Colors.white,
          appBar: AppBar(
    title: Text(""),
          ),
          body: Column(
            
    children: [
      SizedBox(height: 18,),
      Align(
        child: Text(
          "Verify Phone".tr(),
          style: GoogleFonts.poppins(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        alignment: Alignment.center,
      ),
      const SizedBox(
        height: 10,
      ),
      Align(
        child: Text(
          "OTP sent to".tr() + " +91-${widget.mobile} ",
          style: GoogleFonts.poppins(fontSize: 15, fontWeight: FontWeight.w500),
        ),
        alignment: Alignment.center,
      ),
      const SizedBox(
        height: 30,
      ),
      Form(
          key: otpFormKey,
          child: Column(
            children: [
              Pinput(
                controller: pinController,
                length: 4,
                defaultPinTheme: defaultPinTheme,
                focusedPinTheme: focusedPinTheme,
                submittedPinTheme: submittedPinTheme,
                validator: (s) {
                  if (s == "" || s!.length < 4) {
                    return 'Pin is incorrect'.tr();
                  } else {
                    return null;
                  }
                },
                pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                showCursor: true,
                onCompleted: (pin) => setState(() {
                  otpCode = pin;
                }),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        getOTP = false;
                        enableResend = false;
                        timer.cancel();
                        secondsRemaining = 30;
                        Navigator.pop(context);
                      });
                    },
                    child: Text(
                      "Back".tr(),
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.grey[200]!),
                    ),
                  )),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: ElevatedButton(
                    onPressed: () async {
                      if (otpFormKey.currentState!.validate()) {
                        await Provider.of<AuthenticationProvider>(context,
                                listen: false)
                            .verifyOTPProvider(
                                context: context,
                                mobileNumber: widget.mobile,
                                otp: pinController.text.trim());
                      }
                      // if (otpFormKey.currentState!.validate()) {
                      //   showLoaderDialog(context);
                      //   signIn(otpCode);
                      //   verifyOTP();
                      //   print("UserID Saved--->${userId.toString()}");
                      // } else {
                      //   PinputAutovalidateMode.onSubmit;
                      // }
                    },
                    child: Text(
                      "Verify OTP".tr(),
                      style: GoogleFonts.poppins(color: Colors.white),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          AppColor.appColor),
                    ),
                  )),
                  const SizedBox(width: 20),
                ],
              ),
              const SizedBox(height: 30),
              AnimatedSwitcher(
                duration: Duration(seconds: 3000),
                child: enableResend
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Column(
                              crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  'Resend OTP Available'.tr(),
                                  style: GoogleFonts.poppins(
                                      color: Colors.black, fontSize: 16),
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      showLoaderDialog(context);
                                      authenticationProvider.login(
                                          context: context,
                                          mobileNum: widget.mobile.trim(),
                                          token: deviceToken);
                                      if (Navigator.of(context).canPop()) {
                                        Navigator.of(context).pop();
                                      }
                                    },
                                    child: Text(
                                      "Resend OTP on SMS".tr(),
                                      style: GoogleFonts.poppins(color: Colors.white),
                                    ),
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              AppColor.appColor),
                                    )),
                              ],
                            ),
                          )
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            textAlign: TextAlign.center,
                            "Resend OTP in".tr() +
                                " $secondsRemaining " +
                                "seconds".tr(),
                            style: GoogleFonts.poppins(
                                color: Colors.black, fontSize: 16),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: ElevatedButton(
                                onPressed: null,
                                child: Text("Resend OTP on SMS".tr(),
                                    style:
                                        GoogleFonts.poppins(color: Colors.grey[700])),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.grey[200]!),
                                )),
                          ),
                        ],
                      ),
              )
            ],
          ))
    ],
          ),
        );
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.getInt(Constants.tempUserID) != null) {
      userId = prefs.getInt(Constants.tempUserID)!;
    }
  }
}
