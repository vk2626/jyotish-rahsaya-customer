import 'package:flutter/material.dart';
import '../utils/AppColor.dart';

class BuyMemberShip extends StatefulWidget {
  const BuyMemberShip({super.key});

  @override
  State<BuyMemberShip> createState() => _BuyMemberShipState();
}

class _BuyMemberShipState extends State<BuyMemberShip> {
  // List<MembershipPointWidget> _list = getPoints();
  // bool flag = true;
  // int itemsToShow = 5;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buy Membership"),
      ),
      body: LayoutBuilder(builder: (context, BoxConstraints contraints) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Card(
                child: Container(
                  height: 350,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(color: AppColor.appColor)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 40,
                              child: Icon(Icons.calendar_today),
                            ),
                            SizedBox(width: 10),
                            Text(
                              "Daily Pass Membership",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 25),
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Expanded(
                          child: ListView.builder(
                              itemCount: 10,
                              itemBuilder: (context, idx) {
                                return Row(
                                  children: [
                                    Icon(Icons.check_circle_outline,
                                        color: AppColor.transparentBlack),
                                    SizedBox(width: 10),
                                    Text(
                                      "${idx} Enjoy a free chat session everyday",
                                      style: TextStyle(fontSize: 19),
                                    ),
                                  ],
                                );
                              }),
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10),
                          child: Divider(
                              thickness: 2, color: AppColor.transparentBlack),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                  text: "₹499\n",
                                  style: TextStyle(
                                    fontSize: 19,
                                    color: AppColor.darkGrey,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                                  children: [
                                    TextSpan(
                                        text: "₹99",
                                        style: TextStyle(
                                          fontSize: 19,
                                          color: Colors.red,
                                          decoration: TextDecoration.none,
                                        )),
                                    TextSpan(
                                        text: "/10 days",
                                        style: TextStyle(
                                          fontSize: 15,
                                          decoration: TextDecoration.none,
                                        )),
                                  ]),
                            ),
                            SizedBox(
                              height: 50,
                              width: 120,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(18.0),
                                              side: const BorderSide(
                                                  color: AppColor.appColor))),
                                      backgroundColor:
                                          const MaterialStatePropertyAll<Color>(
                                              AppColor.appColor)),
                                  onPressed: () {},
                                  child: const Text("Buy Now",
                                      style: TextStyle(fontSize: 20))),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }

// Widget _renderPoints() {
//   return ExpansionPanelList(
//     expansionCallback: (int index, bool isExpanded) {
//       setState(() {
//         _list[index].isExpanded = !isExpanded;
//       });
//     },
//     children: _list.map<ExpansionPanel>((MembershipPointWidget pointWidget) {
//       return ExpansionPanel(
//           headerBuilder: (BuildContext context, bool isExpanded) {
//             return ListTile(
//               title: Text(pointWidget.title),
//             );
//           },
//           body: ListTile(
//             subtitle: Text(pointWidget.icon.toString()),
//           ),
//           isExpanded: pointWidget.isExpanded);
//     }).toList(),
//   );
// }
}

// List<MembershipPointWidget> getPoints() {
//   return [
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//     MembershipPointWidget(
//         Icons.check_circle_outline, "Enjoy a free chat session everyday"),
//   ];
// }

class MembershipPointWidget {
  IconData icon;

  String title;
  bool isExpanded;

  MembershipPointWidget(this.icon, this.title, [this.isExpanded = false]);
}
