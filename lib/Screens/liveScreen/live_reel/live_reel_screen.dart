import 'dart:async';
import 'dart:math';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../utils/custom_circular_progress_indicator.dart';
import '../../../Core/Provider/WaitListNotifier.dart';
import '../../../Core/Model/astrologer_live_details_model.dart';
import '../../../Core/Model/gift.api.model.dart';
import '../../../Core/Model/waitlist_user_live_model.dart';
import '../../ChatIntakeForm.dart';
import '../../../utils/AppColor.dart';
import '../../../../Core/formatter.dart';
import '../../../../Core/helper_functions.dart';
import '../../../../utils/snack_bar.dart';
import '../../../../Core/Model/astrologer_live_reels_model.dart';
import '../../../../Core/Provider/go_live_provider.dart';
import '../../../../Core/Provider/list.of.api.provider.dart';
import '../../../../Core/logger_helper.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../../../Core/Model/golive_messages_model.dart';

class LiveReelScreen extends StatefulWidget {
  const LiveReelScreen({super.key});

  @override
  State<LiveReelScreen> createState() => _LiveReelScreenState();
}

class _LiveReelScreenState extends State<LiveReelScreen>
    with WidgetsBindingObserver {
  late RtcEngine _agoraEngine;
  late Timer _headTimer;
  Duration remainingTime = Duration.zero;
  Timer? timer;
  List<AstrologerOnline> _astrologersList = [];
  AstrologerOnline? astrologerOnline;
  bool _isLoading = true;
  int? _remoteUid;
  List<Comment> commentList = [];
  String? onGoingCommunicationId;
  late Timer _timer;
  bool isFollowed = false;
  TextEditingController comment = TextEditingController();
  GiftApiModel giftApiModel = GiftApiModel();
  List<WaitListUserLive> waitListUsersLive = [];
  bool isCallAttended = false;
  bool isVideoCallEnabled = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _agoraEngine = createAgoraRtcEngine();
    _initializeAgora().whenComplete(() => _fetchAstrologersList());
    refreshMessages();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
  }

  @override
  void dispose() async {
    WidgetsBinding.instance.removeObserver(this);
    _agoraEngine.release();
    if (_timer.isActive) {
      _timer.cancel();
    }
    super.dispose();
  }

  AstrologerLiveDetailsModel? astrologerLiveDetailsModel;
  fetchAstrologerPricing({required String astrologerId}) async {
    await Provider.of<ListOfApisProvider>(context, listen: false)
        .astrologerDetailsLive(id: astrologerId)
        .then((value) {
      if (value != null)
        setState(() {
          astrologerLiveDetailsModel = value;
        });
    });
  }

  refreshMessages() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      if (mounted) {
        if (message.data['type'].toString() == 'audio-call-live') {
          // await _agoraEngine.muteLocalAudioStream(false);
          _agoraEngine.setClientRole(
              role: ClientRoleType.clientRoleBroadcaster);
          _agoraEngine.muteLocalAudioStream(false);
          showSnackBarWidget(context, "You're now connected with astrologer !!",
              isTrue: true);
          setState(() {
            isCallAttended = true;
            onGoingCommunicationId = message.data['communicationId'].toString();
          });
          _initializeTimer();
        }
        if (message.data['type'].toString() == 'anonymous-call-live') {
          _agoraEngine.muteAllRemoteAudioStreams(true);
          _agoraEngine.setClientRole(
              role: ClientRoleType.clientRoleBroadcaster);
          _agoraEngine.muteRemoteAudioStream(
              mute: false, uid: int.parse(await THelperFunctions.getUserId()));
          showSnackBarWidget(context, "You're now connected with astrologer !!",
              isTrue: true);
          setState(() {
            isCallAttended = true;
            onGoingCommunicationId = message.data['communicationId'].toString();
          });
          _initializeTimer();
        }
        if (message.data['type'].toString() == 'video-call-live') {
          _agoraEngine.setClientRole(
              role: ClientRoleType.clientRoleBroadcaster);
          _agoraEngine.muteLocalAudioStream(false);
          _agoraEngine.enableLocalVideo(true);
          _agoraEngine.muteLocalVideoStream(false);
          showSnackBarWidget(context, "You're now connected with astrologer !!",
              isTrue: true);
          setState(() {
            isCallAttended = true;
            isVideoCallEnabled = true;
            onGoingCommunicationId = message.data['communicationId'].toString();
          });
          _initializeTimer();
        }
      }
      TLoggerHelper.debug(message.data.entries.toString());
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      if (mounted) {
        if (message.data['type'].toString() == 'audio-call-live') {
          // await _agoraEngine.muteLocalAudioStream(false);

          _agoraEngine.setClientRole(
              role: ClientRoleType.clientRoleBroadcaster);
          _agoraEngine.muteRemoteAudioStream(
              mute: false, uid: int.parse(await THelperFunctions.getUserId()));
          showSnackBarWidget(context, "You're now connected with astrologer !!",
              isTrue: true);
          setState(() {
            isCallAttended = true;
            onGoingCommunicationId = message.data['communicationId'].toString();
          });
          _headTimer = Timer.periodic(const Duration(seconds: 1), (timer) {});
        }
        if (message.data['type'].toString() == 'anonymous-call-live') {}
      }
      TLoggerHelper.debug(message.data.entries.toString());
    });
  }

  Future<void> _initializeAgora() async {
    try {
      var microphoneStatus = await Permission.microphone.request();
      var cameraStatus = await Permission.camera.request();

      if (microphoneStatus.isDenied || cameraStatus.isDenied) {
        _showPermissionSnackbar();
      }

      await _agoraEngine.initialize(
          const RtcEngineContext(appId: "2b2b29ac0c8b40e88eae4516659054ed"));
      await _agoraEngine.enableVideo();
      await _agoraEngine.enableAudio();
      TLoggerHelper.warning(await THelperFunctions.getUserId());
      _agoraEngine.registerEventHandler(
        RtcEngineEventHandler(
          onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
            if (mounted) {
              setState(() {
                _isLoading = false;
              });
            }
          },
          onUserJoined:
              (RtcConnection connection, int remoteUidd, int elapsed) {
            TLoggerHelper.info(
                "Remote Connection connected!! with Id:- $remoteUidd");
            if (mounted) {
              setState(() {
                _remoteUid = remoteUidd;
              });
            }
          },
          onUserOffline: (connection, remoteUid, reason) {
            if (mounted) {
              setState(() {
                _remoteUid = null;
              });
            }
          },
        ),
      );
    } on AgoraRtcException catch (e) {
      TLoggerHelper.error(e.message.toString());
    }
  }

  Future<void> _fetchAstrologersList() async {
    try {
      final provider = Provider.of<GoLiveProvider>(context, listen: false);

      AstrologerLiveReelsModel? model = await provider.getReelsLiveList();

      if (model!.status) {
        setState(() {
          _astrologersList = model.astrologerOnline;
          _isLoading = false;
          astrologerOnline = _astrologersList.first;
        });
        if (_astrologersList.isEmpty) {
          Navigator.of(context).pop();
          showSnackBarWidget(context, "Currently no astrologers are live.");
        }
        TLoggerHelper.debug(_astrologersList.first.id.toString());
        THelperFunctions.sendLiveOnlineStatus(
            status: true,
            astrologerId: _astrologersList.first.id.toString(),
            liveId: _astrologersList.first.liveId.toString());
        await _joinChannel(_astrologersList.first).whenComplete(() {
          _fetchWaitListUserLive(
              astrologerId: _astrologersList.first.id.toString());
        });
      }
      await getGiftData();
    } catch (e) {
      TLoggerHelper.error("Failed to fetch astrologers list: $e");
    }
  }

  Future _fetchWaitListUserLive({required String astrologerId}) async {
    try {
      Provider.of<GoLiveProvider>(context, listen: false)
          .fetchWaitListLiveUsers(astrologerId: astrologerId)
          .then((val) {
        setState(() {
          waitListUsersLive = val;
        });
      });
      _initializeTimer();
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  void _initializeTimer() {
    // Check if there are pending users
    if (waitListUsersLive.any((element) => element.customer.status == 1)) {
      final user = waitListUsersLive
          .firstWhere((element) => element.customer.status == 1);

      final totalDuration =
          Duration(minutes: int.parse(user.customer.duration));
      final elapsedTime = DateTime.now().difference(user.customer.startTime);
      remainingTime = totalDuration - elapsedTime;

      if (remainingTime.isNegative) {
        remainingTime = Duration.zero;
      }

      // Start the countdown timer
      timer?.cancel(); // Cancel any existing timer
      timer = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (remainingTime > Duration.zero) {
          setState(() {
            remainingTime -= const Duration(seconds: 1);
            TLoggerHelper.debug(remainingTime.toString());
          });
        } else {
          timer.cancel();
        }
      });
    }
  }

  Future<void> _joinChannel(AstrologerOnline astrologer) async {
    setState(() {
      // if (_timer.isActive || _timer != null) {
      //   _timer.cancel();
      // }
      _remoteUid = null;
      commentList = [];
    });

    TLoggerHelper.info(
        "Token:--> ${astrologer.liveToken} ChannelName:--> ${astrologer.channelName}");
    _agoraEngine.muteLocalAudioStream(true);
    _agoraEngine.startPreview();
    ChannelMediaOptions options = const ChannelMediaOptions(
        clientRoleType: ClientRoleType.clientRoleAudience,
        channelProfile: ChannelProfileType.channelProfileLiveBroadcasting);

    await _agoraEngine
        .joinChannel(
      token: astrologer.liveToken,
      channelId: astrologer.channelName,
      options: options,
      uid: int.parse(await THelperFunctions.getUserId()),
    )
        .whenComplete(
      () {
        _timer = Timer.periodic(const Duration(seconds: 3), (timer) async {
          await getListData(astrologer);
        });
      },
    );
  }

  void _showPermissionSnackbar() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: const Text('Microphone and Camera permission are required.'),
        action: SnackBarAction(
          label: 'Settings',
          onPressed: () {
            openAppSettings();
          },
        ),
      ),
    );
  }

  getGiftData() async {
    Provider.of<GoLiveProvider>(context, listen: false)
        .getGiftListData(context: context)
        .then((val) {
      if (val != null) {
        setState(() {
          giftApiModel = val;
        });
      }
    });
  }

  getListData(AstrologerOnline astrologer) async {
    await Provider.of<GoLiveProvider>(context, listen: false)
        .getAllMessageList(
            context: context, liveId: astrologer.liveId.toString())
        .then((value) {
      if (value != null) {
        GoLiveMessagesModel goLiveMessagesModel = value;
        if (value.status) {
          setState(() {
            if (commentList.isEmpty) {
              commentList = goLiveMessagesModel.comments;
            } else {
              if (commentList[commentList.length - 1].commentId !=
                  value.comments[value.comments.length - 1].commentId) {
                commentList.add(value.comments[value.comments.length - 1]);
                // scroll();
              }
            }
          });
        }
      }
    });
  }

  leave() async {
    await _agoraEngine.leaveChannel();
    await _agoraEngine.release();
    Navigator.of(context).pop();
  }

  cancelTimer() {
    setState(() {
      if (_timer.isActive) {
        _timer.cancel();
      }
      if (timer != null) {
        if (timer!.isActive) {
          timer!.cancel();
        }
      }
      remainingTime = Duration.zero;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          // extendBody: true,
          backgroundColor: Colors.black,
          resizeToAvoidBottomInset: false,
          body: _isLoading
              ? Stack(
                  fit: StackFit.expand,
                  children: [
                    Image.asset(
                      "asset/images/rectangle.png",
                      fit: BoxFit.cover,
                    ),
                    Positioned(top: 18, right: 18, child: CloseButton())
                  ],
                )
              : PageView.builder(
                  itemCount: _astrologersList.length,
                  onPageChanged: (value) async {
                    if (mounted) {
                      setState(() {
                        astrologerOnline = _astrologersList[value];
                        _remoteUid = null;
                        cancelTimer();
                      });
                    }
                    await _agoraEngine.leaveChannel();
                    await _agoraEngine.release();

                    // Reinitialize the Agora engine and event handler
                    _agoraEngine = createAgoraRtcEngine();
                    await _initializeAgora();

                    // Join the new channel
                    await _joinChannel(_astrologersList[value]).whenComplete(
                      () async {
                        THelperFunctions.sendLiveOnlineStatus(
                            status: true,
                            astrologerId: _astrologersList[value].id.toString(),
                            liveId: _astrologersList[value].liveId.toString());
                        _fetchWaitListUserLive(
                            astrologerId:
                                _astrologersList[value].id.toString());
                      },
                    );
                  },
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return _remoteUid != null
                        ? Stack(
                            fit: StackFit.expand,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height,
                                color: Colors.transparent,
                                child: Column(
                                  children: [
                                    // First AgoraVideoView (Remote Video)
                                    Expanded(
                                        flex: 1, // Allocate equal space
                                        child: Stack(
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 4),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(32),
                                                child: AgoraVideoView(
                                                  controller:
                                                      VideoViewController
                                                          .remote(
                                                    useAndroidSurfaceView: true,
                                                    rtcEngine: _agoraEngine,
                                                    canvas: VideoCanvas(
                                                      uid: astrologerOnline!.id,
                                                      renderMode: RenderModeType
                                                          .renderModeHidden,
                                                      setupMode:
                                                          VideoViewSetupMode
                                                              .videoViewSetupAdd,
                                                    ),
                                                    connection: RtcConnection(
                                                      channelId:
                                                          _astrologersList[
                                                                  index]
                                                              .channelName,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.bottomRight,
                                              child: Container(
                                                margin: EdgeInsets.all(18),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 8),
                                                decoration: BoxDecoration(
                                                    color: AppColor.appColor
                                                        .withOpacity(.6),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            18)),
                                                child: Text("Astrologer View",
                                                    style:
                                                        GoogleFonts.poppins()),
                                              ),
                                            ),
                                          ],
                                        )),
                                    if (isVideoCallEnabled)
                                      Expanded(
                                          flex: 1, // Allocate equal space
                                          child: Stack(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 8,
                                                        vertical: 4),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(32),
                                                  child: AgoraVideoView(
                                                    controller:
                                                        VideoViewController(
                                                      rtcEngine: _agoraEngine,
                                                      canvas:
                                                          VideoCanvas(uid: 0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Align(
                                                alignment: Alignment.topLeft,
                                                child: Container(
                                                  margin: EdgeInsets.all(18),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 16,
                                                      vertical: 8),
                                                  decoration: BoxDecoration(
                                                      color: AppColor.appColor
                                                          .withOpacity(.6),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              18)),
                                                  child: Text(
                                                    "Local View",
                                                    style:
                                                        GoogleFonts.poppins(),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )),
                                    if (_remoteUid != astrologerOnline!.id)
                                      Expanded(
                                          flex: 1, // Allocate equal space
                                          child: Stack(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 8,
                                                        vertical: 4),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(32),
                                                  child: AgoraVideoView(
                                                    controller:
                                                        VideoViewController
                                                            .remote(
                                                      useAndroidSurfaceView:
                                                          true,
                                                      rtcEngine: _agoraEngine,
                                                      canvas: VideoCanvas(
                                                        uid: _remoteUid,
                                                        renderMode: RenderModeType
                                                            .renderModeHidden,
                                                        setupMode:
                                                            VideoViewSetupMode
                                                                .videoViewSetupAdd,
                                                      ),
                                                      connection: RtcConnection(
                                                        channelId:
                                                            _astrologersList[
                                                                    index]
                                                                .channelName,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Align(
                                                alignment: Alignment.topLeft,
                                                child: Container(
                                                  margin: EdgeInsets.all(18),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 16,
                                                      vertical: 8),
                                                  decoration: BoxDecoration(
                                                      color: AppColor.appColor
                                                          .withOpacity(.6),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              18)),
                                                  child: Text(
                                                    "Connected User's View",
                                                    style:
                                                        GoogleFonts.poppins(),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )),
                                  ],
                                ),
                              ),
                              Align(
                                  alignment: Alignment.topCenter,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 6),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        IntrinsicHeight(
                                          child: Container(
                                            padding: EdgeInsets.all(6),
                                            decoration: BoxDecoration(
                                                color:
                                                    Colors.grey.withOpacity(.3),
                                                borderRadius:
                                                    BorderRadius.circular(24)),
                                            child: Row(
                                              children: [
                                                // if (waitListUsersLive.any(
                                                //         (element) =>
                                                //             element.customer
                                                //                 .status !=
                                                //             1) ||
                                                //     waitListUsersLive.isEmpty)
                                                if (remainingTime ==
                                                    Duration.zero)
                                                  CircleAvatar(
                                                      radius: 16,
                                                      backgroundImage:
                                                          CachedNetworkImageProvider(
                                                              astrologerOnline!
                                                                  .avatar)),
                                                if (waitListUsersLive.any(
                                                        (element) =>
                                                            element.customer
                                                                .status ==
                                                            1) &&
                                                    (remainingTime !=
                                                        Duration.zero))
                                                  SizedBox(
                                                    height: 30,
                                                    width: 30,
                                                    child: Stack(
                                                      children: [
                                                        if (astrologerOnline !=
                                                            null)
                                                          Align(
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: CircleAvatar(
                                                              radius: 10,
                                                              backgroundColor:
                                                                  Colors.white,
                                                              child: CircleAvatar(
                                                                  radius: 8,
                                                                  backgroundImage:
                                                                      CachedNetworkImageProvider(
                                                                          astrologerOnline!
                                                                              .avatar)),
                                                            ),
                                                          ),
                                                        Align(
                                                          alignment: Alignment
                                                              .bottomRight,
                                                          child: CircleAvatar(
                                                            radius: 10,
                                                            backgroundColor:
                                                                Colors.white,
                                                            child: CircleAvatar(
                                                                radius: 8,
                                                                backgroundImage: CachedNetworkImageProvider(waitListUsersLive
                                                                    .firstWhere((element) =>
                                                                        element
                                                                            .customer
                                                                            .status ==
                                                                        1)
                                                                    .avatar)),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                SizedBox(
                                                  width: 8,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      astrologerOnline != null
                                                          ? TFormatter
                                                              .capitalizeSentence(
                                                                  astrologerOnline!
                                                                      .name)
                                                          : "",
                                                      style:
                                                          GoogleFonts.poppins(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                    ),
                                                    if (waitListUsersLive.any(
                                                            (element) =>
                                                                element.customer
                                                                    .status ==
                                                                1) &&
                                                        (remainingTime !=
                                                            Duration.zero))
                                                      Text(
                                                        "& ${TFormatter.capitalizeSentence(waitListUsersLive.firstWhere((element) => element.customer.status == 1).name)}",
                                                        style:
                                                            GoogleFonts.poppins(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 12),
                                                      ),
                                                  ],
                                                ),
                                                // if (waitListUsersLive.any(
                                                //     (element) =>
                                                //         element
                                                //             .customer.status ==
                                                //         1))
                                                if (waitListUsersLive.any(
                                                        (element) =>
                                                            element.customer
                                                                .status ==
                                                            1) &&
                                                    (remainingTime !=
                                                        Duration.zero))
                                                  Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                        vertical: 6.0),
                                                    child: VerticalDivider(
                                                      thickness: 2,
                                                    ),
                                                  ),
                                                // if (waitListUsersLive.any(
                                                //     (element) =>
                                                //         element
                                                //             .customer.status ==
                                                //         1))
                                                //   Text(
                                                //     ((Duration(
                                                //                 minutes: int.parse(waitListUsersLive
                                                //                     .firstWhere((element) =>
                                                //                         element
                                                //                             .customer
                                                //                             .status ==
                                                //                         1)
                                                //                     .customer
                                                //                     .duration)) -
                                                //             DateTime.now().difference(
                                                //                 waitListUsersLive
                                                //                     .firstWhere((element) =>
                                                //                         element
                                                //                             .customer
                                                //                             .status ==
                                                //                         1)
                                                //                     .customer
                                                //                     .startTime))
                                                //         .toString()),
                                                //     style: GoogleFonts.poppins(
                                                //         color: Colors.white,
                                                //         fontSize: 12,
                                                //         fontWeight:
                                                //             FontWeight.w600),
                                                //   ),
                                                if (waitListUsersLive.any(
                                                        (element) =>
                                                            element.customer
                                                                .status ==
                                                            1) &&
                                                    (remainingTime !=
                                                        Duration.zero))
                                                  Text(
                                                    // '${remainingTime.inMinutes.remainder(60).toString().padLeft(2, '0')}:${remainingTime.inSeconds.remainder(60).toString().padLeft(2, '0')}',
                                                    TFormatter
                                                        .formatTimeFromDuration(
                                                            remainingTime),
                                                    style: GoogleFonts.poppins(
                                                      color: Colors.white,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            if (!_astrologersList[index]
                                                .followingUser)
                                              InkWell(
                                                onTap: () async {
                                                  String customerId =
                                                      await THelperFunctions
                                                          .getUserId();

                                                  Provider.of<ListOfApisProvider>(
                                                          context,
                                                          listen: false)
                                                      .followerProvider(
                                                          customerID:
                                                              customerId,
                                                          astrologerID:
                                                              _astrologersList[
                                                                      index]
                                                                  .id
                                                                  .toString(),
                                                          status: '1');
                                                },
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 16,
                                                      vertical: 8),
                                                  decoration: BoxDecoration(
                                                      color: Colors.grey
                                                          .withOpacity(.3),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              24)),
                                                  child: Text(
                                                    "Follow",
                                                    style: GoogleFonts.poppins(
                                                        color: Colors.white,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                              ),
                                            SizedBox(width: 8),
                                            Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(.3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          24)),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.remove_red_eye,
                                                    size: 16,
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(
                                                    width: 6,
                                                  ),
                                                  Text(
                                                    TFormatter.formatLiveCount(
                                                        _astrologersList[index]
                                                            .liveCount),
                                                    style: GoogleFonts.poppins(
                                                        color: Colors.white,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                  Icon(
                                                    Icons
                                                        .arrow_forward_ios_rounded,
                                                    size: 8,
                                                    color: Colors.white,
                                                  )
                                                ],
                                              ),
                                            ),
                                            SizedBox(width: 8),
                                            InkWell(
                                              onTap: leave,
                                              child: CircleAvatar(
                                                backgroundColor:
                                                    Colors.grey.withOpacity(.3),
                                                child: Icon(
                                                  Icons.close,
                                                  color: Colors.white60,
                                                  size: 24,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )),
                              Positioned(
                                left: 18,
                                bottom: 70,
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width *
                                      0.55, // Adjust as needed
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      if (isCallAttended)
                                        InkWell(
                                          onTap: () {
                                            if (onGoingCommunicationId !=
                                                null) {
                                              Provider.of<GoLiveProvider>(
                                                      context,
                                                      listen: false)
                                                  .sendCallDurationLive(
                                                      liveId: astrologerOnline!
                                                          .liveId
                                                          .toString(),
                                                      context: context,
                                                      communcationId:
                                                          onGoingCommunicationId
                                                              .toString(),
                                                      duration: 2)
                                                  .then(
                                                (value) {
                                                  if (value) {
                                                    setState(() {
                                                      isCallAttended = false;
                                                      isVideoCallEnabled =
                                                          false;
                                                      _agoraEngine
                                                          .muteLocalAudioStream(
                                                              true);
                                                      _agoraEngine.setClientRole(
                                                          role: ClientRoleType
                                                              .clientRoleAudience);
                                                      if (timer != null) {
                                                        if (timer!.isActive) {
                                                          timer!.cancel();
                                                        }
                                                      }
                                                      cancelTimer();
                                                    });
                                                  }
                                                },
                                              );
                                            }
                                          },
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 28, vertical: 8),
                                            decoration: BoxDecoration(
                                                color: Colors.redAccent
                                                    .withOpacity(.3),
                                                borderRadius:
                                                    BorderRadius.circular(24)),
                                            child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Icon(
                                                  Icons.close_rounded,
                                                  color: Colors.white,
                                                  size: 16,
                                                ),
                                                SizedBox(
                                                  width: 6,
                                                ),
                                                Text(
                                                  "Leave",
                                                  style: GoogleFonts.poppins(
                                                      color: Colors.white,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      SizedBox(height: 12),
                                      InkWell(
                                        onTap: () {
                                          showGiftList(
                                              astrologerOnline:
                                                  _astrologersList[index]);
                                        },
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 28, vertical: 8),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(.3),
                                              borderRadius:
                                                  BorderRadius.circular(24)),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Icon(
                                                Icons.card_giftcard_rounded,
                                                color: Colors.white,
                                                size: 16,
                                              ),
                                              SizedBox(
                                                width: 6,
                                              ),
                                              Text(
                                                "Send Gift",
                                                style: GoogleFonts.poppins(
                                                    color: Colors.white,
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 12),
                                      ConstrainedBox(
                                        constraints: BoxConstraints(
                                          maxHeight: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.3,
                                          minHeight: 0,
                                          minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.55,
                                        ),
                                        child: ListView.separated(
                                          shrinkWrap: true,
                                          itemCount: commentList.length,
                                          reverse: true,
                                          separatorBuilder: (context, index) =>
                                              SizedBox(height: 8),
                                          itemBuilder: (context, index) {
                                            final reversedIndex =
                                                commentList.length - 1 - index;
                                            return Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 18,
                                                  backgroundImage:
                                                      CachedNetworkImageProvider(
                                                    commentList[reversedIndex]
                                                            .userImage
                                                            .isEmpty
                                                        ? "https://static.vecteezy.com/system/resources/thumbnails/002/002/403/small/man-with-beard-avatar-character-isolated-icon-free-vector.jpg"
                                                        : commentList[
                                                                reversedIndex]
                                                            .userImage,
                                                  ),
                                                ),
                                                SizedBox(width: 12),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      TFormatter
                                                          .capitalizeSentence(
                                                        commentList[
                                                                reversedIndex]
                                                            .userName,
                                                      ),
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.35,
                                                      child: Text(
                                                        TFormatter.capitalize(
                                                          commentList[
                                                                  reversedIndex]
                                                              .comment,
                                                        ),
                                                        maxLines: 4,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                          fontSize: 16,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 12,
                                right: 12,
                                child: Column(
                                  children: [
                                    FloatingActionButton(
                                      mini: false,
                                      elevation: 0,
                                      shape: CircleBorder(),
                                      backgroundColor: Colors.black12,
                                      child: Image.asset(
                                        'asset/images/send.png',
                                        height: 22,
                                        color: Colors.white,
                                      ),
                                      onPressed: () {},
                                    ),
                                    SizedBox(height: 12),
                                    FloatingActionButton(
                                      mini: false,
                                      elevation: 0,
                                      shape: CircleBorder(),
                                      backgroundColor: Colors.black12,
                                      child: Icon(
                                        Icons.card_giftcard_rounded,
                                        color: Colors.white,
                                        size: 22,
                                      ),
                                      onPressed: () {
                                        showGiftList(
                                            astrologerOnline:
                                                _astrologersList[index]);
                                      },
                                    ),
                                    SizedBox(height: 12),
                                    FloatingActionButton(
                                      mini: false,
                                      elevation: 0,
                                      shape: CircleBorder(),
                                      backgroundColor: Colors.black12,
                                      child: Image.asset(
                                        'asset/images/hourglass.png',
                                        height: 22,
                                        color: Colors.white,
                                      ),
                                      onPressed: () {
                                        showWaitList(
                                            astrologerOnline:
                                                _astrologersList[index]);
                                      },
                                    ),
                                    SizedBox(height: 12),
                                    FloatingActionButton(
                                      mini: false,
                                      elevation: 0,
                                      shape: CircleBorder(),
                                      backgroundColor: Colors.black12,
                                      child: Icon(
                                        Icons.phone,
                                        color: Colors.white,
                                        size: 22,
                                      ),
                                      onPressed: () {
                                        TLoggerHelper.debug(
                                            _astrologersList[index]
                                                .id
                                                .toString());
                                        showPricingBottomSheet(
                                            id: _astrologersList[index]
                                                .id
                                                .toString());
                                      },
                                    ),
                                    SizedBox(height: 12),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8),
                                      decoration: BoxDecoration(
                                          color: Colors.black12,
                                          borderRadius:
                                              BorderRadius.circular(24)),
                                      child: Column(
                                        children: [
                                          Text(
                                            "₹ ${_astrologersList[index].astrologerList.callDiscountPrice}/m",
                                            style: GoogleFonts.poppins(
                                                color: Colors.white,
                                                fontSize: 15),
                                          ),
                                          Text(
                                            "₹ ${_astrologersList[index].astrologerList.callPrice}/m",
                                            style: GoogleFonts.poppins(
                                                color: Colors.white,
                                                fontSize: 13,
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                decorationColor: Colors.red,
                                                decorationThickness: 2),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Positioned(
                                  bottom: 12,
                                  left: 12,
                                  child: InkWell(
                                    onTap: () {
                                      _showCommentDialog(
                                          context,
                                          _astrologersList[index]
                                              .liveId
                                              .toString());
                                    },
                                    child: SizedBox(
                                      height: 42,
                                      width: MediaQuery.of(context).size.width *
                                          .5,
                                      child: TextField(
                                        decoration: InputDecoration(
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 16, vertical: 2),
                                          fillColor:
                                              Colors.grey.withOpacity(.3),
                                          filled: true,
                                          hintText: "Say hi",
                                          enabled: false,
                                          hintStyle: GoogleFonts.poppins(
                                              color: Colors.white54),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(16)),
                                        ),
                                      ),
                                    ),
                                  ))
                            ],
                          )
                        : Image.asset("asset/images/rectangle.png",
                            fit: BoxFit.cover);
                  })),
    );
  }

  void showGiftList({required AstrologerOnline astrologerOnline}) {
    int? selectedIdx;
    bool? isSelected;
    String? giftID;
    String? giftAmount;
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Stack(
                      children: [
                        SizedBox(
                          height: 50,
                          child: Center(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Gifts",
                                  style: GoogleFonts.poppins(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(width: 8),
                                Icon(Icons.info_outline)
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 50,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "₹ 25",
                              style: GoogleFonts.poppins(fontSize: 18),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .35,
                      child: GridView.builder(
                        itemCount: giftApiModel.data!.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 4,
                            childAspectRatio: 8 / 10,
                            mainAxisSpacing: 2,
                            crossAxisSpacing: 3),
                        itemBuilder: (context, giftIdx) {
                          var giftData = giftApiModel.data!;
                          return InkWell(
                            onTap: () {
                              setState(() {
                                selectedIdx = giftIdx;
                                isSelected = true;
                              });
                              giftID = giftData[giftIdx].id.toString();
                              giftAmount = giftData[giftIdx].price.toString();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: giftIdx == selectedIdx
                                    ? AppColor.borderColor.withOpacity(.3)
                                    : Colors.transparent,
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  SizedBox(
                                    height: 70,
                                    width: 50,
                                    child: CachedNetworkImage(
                                        imageUrl: giftData[giftIdx].image!),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        "${giftData[giftIdx].title}",
                                        textAlign: TextAlign.center,
                                      ),
                                      Text("₹ ${giftData[giftIdx].price}")
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    Divider(),
                    InkWell(
                      onTap: () {
                        Provider.of<ListOfApisProvider>(context, listen: false)
                            .sendGiftWallet(
                                context: context,
                                astrologerId: astrologerOnline.id.toString(),
                                giftId: giftID!);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 18),
                        width: double.infinity,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: AppColor.appColor,
                            borderRadius: BorderRadius.circular(8)),
                        child: Text("Send Gift",
                            style: GoogleFonts.poppins(
                                fontWeight: FontWeight.w600)),
                      ),
                    )
                  ],
                ),
              );
            },
          );
        });
  }

  void showPricingBottomSheet({required String id}) async {
    // Fetch data and handle null cases
    await fetchAstrologerPricing(astrologerId: id.toString());

    if (astrologerLiveDetailsModel == null) {
      // Handle the case where the model is still null
      showModalBottomSheet(
        backgroundColor: Colors.white,
        context: context,
        builder: (_) {
          return SizedBox(
            height: MediaQuery.of(context).size.height * .3,
            width: MediaQuery.of(context).size.width,
            child: Center(child: CustomCircularProgressIndicator()),
          );
        },
      );
      return; // Exit the function if the model is null
    }

    // Define your lists
    List<Widget> iconsList = [
      Icon(Icons.phone),
      Image.asset("asset/images/incognito.png",
          height: 32, color: Colors.black),
      Icon(Icons.private_connectivity_rounded),
      Icon(Icons.videocam_rounded),
    ];
    List<String> pricingTitleList = [
      "Audio Call",
      "Video Call",
      "Anonymous Call",
      "Private Call",
    ];
    List<String> pricingDescList = [
      "Consulant on video, you on audio.",
      "Both consultant and you on video call.",
      "Consultant on video, you on audio. No one can hear you, Consultant is audible.",
      "Consultant on video, you on audio. No one else can listen to your conversation.",
    ];
    List<String> typesList = [
      "audio-call-live",
      "video-call-live",
      "anonymous-call-live",
      "private-call-live",
    ];
    List<String> postCallTypeList = [
      "audio_call",
      "video_call",
      "anonymous_call",
      "private_call",
    ];
    List<String> priceList = [
      astrologerLiveDetailsModel!.astrlogerPriceList.audioCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.videoCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.anonymousCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.privateCall,
    ];
    List<String> priceDiscountList = [
      astrologerLiveDetailsModel!.astrlogerPriceList.discountAudioCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.discountVideoCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.discountAnonymousCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.discountPrivateCall,
    ];
    List<int> statusList = [
      astrologerLiveDetailsModel!.astrlogerPriceList.statusAudioCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.statusVideoCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.statusAnonymousCall,
      astrologerLiveDetailsModel!.astrlogerPriceList.statusPrivateCall,
    ];

    showModalBottomSheet(
      backgroundColor: Colors.white,
      context: context,
      builder: (_) {
        return Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CircleAvatar(
                radius: 42,
                backgroundImage: CachedNetworkImageProvider(
                    astrologerLiveDetailsModel!.astrlogerPriceList.avatar),
              ),
              SizedBox(height: 8),
              Text(
                astrologerLiveDetailsModel!.astrlogerPriceList.name,
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Divider(),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: List.generate(
                      iconsList.length,
                      (index) {
                        return statusList[index] == 1
                            ? Column(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 24,
                                          backgroundColor: AppColor.appColor,
                                          child: iconsList[index],
                                        ),
                                        SizedBox(width: 12),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "${pricingTitleList[index]} ",
                                                  style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                                Text(
                                                  "@ ₹ ${priceList[index]} ",
                                                  style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                                if (priceDiscountList[index] !=
                                                        "0" ||
                                                    int.parse(priceDiscountList[
                                                            index]) >=
                                                        1)
                                                  Text(
                                                    "₹ ${priceDiscountList[index]}/" +
                                                        "min".tr(),
                                                    style: GoogleFonts.poppins(
                                                      fontSize: 12,
                                                      decoration: TextDecoration
                                                          .lineThrough,
                                                      decorationColor:
                                                          Colors.redAccent,
                                                    ),
                                                  ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.6,
                                              child: Text(
                                                pricingDescList[index],
                                                style: GoogleFonts.poppins(),
                                                maxLines: 3,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        InkWell(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    MultiProvider(
                                                  providers: [
                                                    ListenableProvider(
                                                        create: (context) =>
                                                            ListOfApisProvider()),
                                                    // ListenableProvider(
                                                    //     create: (context) =>
                                                    //         WaitListNotifier()),
                                                  ],
                                                  child: ChatIntakeForm(
                                                    astrologerOnline!.name,
                                                    astrologerOnline!.id,
                                                    astrologerOnline!.id,
                                                    typesList[index],
                                                    0,
                                                    isLive: true,
                                                  ),
                                                ),
                                              ),
                                            );
                                          },
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 12, vertical: 8),
                                            decoration: BoxDecoration(
                                              color: AppColor.appColor,
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                            ),
                                            child: Text(
                                              "Join",
                                              style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                      color: (index == 3)
                                          ? Colors.transparent
                                          : null),
                                ],
                              )
                            : SizedBox();
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void showWaitList({required AstrologerOnline astrologerOnline}) async {
    // Fetch the wait list users
    await _fetchWaitListUserLive(astrologerId: astrologerOnline.id.toString());

    // Ensure the data is available before showing the bottom sheet
    if (waitListUsersLive == null || waitListUsersLive.isEmpty) {
      // Handle the case where the wait list is empty or null
      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: const EdgeInsets.all(18.0),
            child: Center(
              child: Text(
                "No waitlist users available.",
                style: GoogleFonts.poppins(
                    fontSize: 16, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ),
          );
        },
      );
      return; // Exit the function if no users are available
    }

    // Show the wait list in a modal bottom sheet
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Stack(
                children: [
                  SizedBox(
                    height: 50,
                    child: Center(
                      child: Text(
                        "Waitlist",
                        style: GoogleFonts.poppins(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: CloseButton(),
                  ),
                ],
              ),
              Text(
                "Customers who missed the call and were marked offline will get priority as per the list if they come online.",
                style: GoogleFonts.poppins(fontSize: 14, color: Colors.black45),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 8),
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * .25,
                ),
                child: ListView.builder(
                  itemCount: waitListUsersLive.length,
                  itemBuilder: (context, index) {
                    final user = waitListUsersLive[index];
                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  radius: 18,
                                  backgroundImage:
                                      CachedNetworkImageProvider(user.avatar),
                                ),
                                SizedBox(width: 12),
                                Text(
                                  TFormatter.capitalizeSentence(user.name),
                                  style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * .3,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CircleAvatar(
                                    radius: 8,
                                    backgroundColor: AppColor.appColor,
                                    child: Icon(
                                      Icons.call,
                                      size: 12,
                                    ),
                                  ),
                                  SizedBox(width: 12),
                                  Text(
                                    TFormatter.formatTimeFromMinutes(
                                        int.parse(user.customer.duration)),
                                    style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 12),
                      ],
                    );
                  },
                ),
              ),
              Divider(),
              InkWell(
                onTap: () {
                  showPricingBottomSheet(id: astrologerOnline.id.toString());
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 18),
                  width: double.infinity,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: AppColor.appColor,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    "Join Waitlist",
                    style: GoogleFonts.poppins(fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _showCommentDialog(BuildContext context, String liveId) {
    showDialog(
      context: context,
      builder: (ctx) {
        return Dialog(
          insetPadding: EdgeInsets.zero,
          backgroundColor: Colors.transparent,
          child: Container(
            padding: const EdgeInsets.all(12.0),
            alignment: Alignment.bottomLeft,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 42,
                  width: MediaQuery.of(context).size.width * .5,
                  child: TextField(
                    autofocus: true,
                    controller: comment,
                    expands: false,
                    style: TextStyle(fontSize: 16, color: Colors.white),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 2),
                      fillColor: Colors.grey.withOpacity(.3),
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16)),
                    ),
                  ),
                ),
                // const SizedBox(
                //   width: 28,
                // ),
                CircleAvatar(
                  backgroundColor: Colors.blue,
                  // Change color according to your theme
                  child: IconButton(
                    icon: const Icon(
                      Icons.send,
                      color: Colors.white,
                    ),
                    onPressed: () async {
                      if (comment.text.isNotEmpty) {
                        await Provider.of<GoLiveProvider>(context,
                                listen: false)
                            .sendLiveComment(
                                context: context,
                                liveId: liveId,
                                comment: comment.text)
                            .then((val) {
                          if (val) {
                            setState(() {
                              comment.clear();
                            });
                          }
                          Navigator.of(context).pop();
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
