import 'dart:async';

import 'package:flutter/material.dart';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import '../../../Core/Api/list.of.apis.dart';
import '../../../Core/helper_functions.dart';
import '../../../Core/logger_helper.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../../utils/custom_circular_progress_indicator.dart';

class AgoraLiveScreen extends StatefulWidget {
  const AgoraLiveScreen(
      {super.key,
      required this.token,
      required this.channelName,
      required this.sessionId,
      required this.liveId,
      required this.userId});

  static late RtcEngine agoraEngine;
  static late Timer timer;
  final String userId;
  final String channelName;
  final String liveId;
  final String sessionId;
  final String token;

  @override
  _AgoraLiveScreenState createState() => _AgoraLiveScreenState();
}

class _AgoraLiveScreenState extends State<AgoraLiveScreen>
    with WidgetsBindingObserver {
  final String appId = "2b2b29ac0c8b40e88eae4516659054ed";
  int? remoteUIds;
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  bool _isJoined = false;

  @override
  void dispose() {
    AgoraLiveScreen.agoraEngine.leaveChannel();
    AgoraLiveScreen.agoraEngine.release();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    setupVideoSDKEngine();
    AgoraLiveScreen.agoraEngine = createAgoraRtcEngine();
  }

  Future<void> setupVideoSDKEngine() async {
    try {
      var microphoneStatus = await Permission.microphone.request();
      var cameraStatus = await Permission.camera.request();

      if (microphoneStatus.isDenied || cameraStatus.isDenied) {
        showPermissionSnackbar(context);
      }

      await AgoraLiveScreen.agoraEngine.initialize(
          const RtcEngineContext(appId: "2b2b29ac0c8b40e88eae4516659054ed"));

      await AgoraLiveScreen.agoraEngine.enableVideo();

      AgoraLiveScreen.agoraEngine.registerEventHandler(
        RtcEngineEventHandler(
          onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
            showMessage(
                "Local user uid:${connection.localUid} joined the channel");
            TLoggerHelper.info(
                "Local user uid:${connection.localUid} joined the channel");
            setState(() {
              _isJoined = true;
            });
          },
          onUserJoined:
              (RtcConnection connection, int remoteUidd, int elapsed) {
            showMessage("Remote user uid:$remoteUidd joined the channel");
            TLoggerHelper.info(
                "Joined user uid:$remoteUidd joined the channel");
            setState(() {
              remoteUIds = remoteUidd;
              TLoggerHelper.info(
                  "Yeh hai remote uid jo ke user joined hone ke baad milta hai $remoteUidd");
            });
          },
          onUserOffline: (RtcConnection connection, int remoteUid,
              UserOfflineReasonType reason) {
            showMessage("Remote user uid:$remoteUid left the channel");
            setState(() {
              remoteUIds = null;
            });
          },
        ),
      );

      joinChannel();
    } on AgoraRtcException catch (e) {
      TLoggerHelper.error(e.message.toString());
    }
  }

  showMessage(String message) {
    scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  Future<void> leave(String liveId) async {
    setState(() {
      _isJoined = false;
      remoteUIds = null;
    });

    await AgoraLiveScreen.agoraEngine.release();
    await AgoraLiveScreen.agoraEngine.leaveChannel();

    if (AgoraLiveScreen.timer.isActive) {
      AgoraLiveScreen.timer.cancel();
    }
  }

  Future<void> joinChannel() async {
    await AgoraLiveScreen.agoraEngine.startPreview();
    // Set channel options including the client role and channel profile
    ChannelMediaOptions options = const ChannelMediaOptions(
      clientRoleType: ClientRoleType.clientRoleAudience,
      channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
    );
    await AgoraLiveScreen.agoraEngine.joinChannel(
        token: widget.token,
        channelId: widget.channelName,
        options: options,
        uid: int.parse(await THelperFunctions.getUserId()));
  }

  void showPermissionSnackbar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: const Text('Microphone and Camera permission are required.'),
        action: SnackBarAction(
          label: 'Settings',
          onPressed: () {
            openAppSettings();
          },
        ),
      ),
    );
  }

  Widget _localPreview() {
    return AgoraVideoView(
        controller: VideoViewController.remote(
            rtcEngine: AgoraLiveScreen.agoraEngine,
            canvas: VideoCanvas(uid: int.parse(widget.userId)),
            connection: RtcConnection(channelId: widget.channelName)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldMessengerKey,
      extendBody: true,
      body: Center(
        child: remoteUIds != null
            ? AgoraVideoView(
                controller: VideoViewController.remote(
                  useAndroidSurfaceView: true,
                  rtcEngine: AgoraLiveScreen.agoraEngine,
                  canvas: VideoCanvas(
                    uid: remoteUIds,
                    renderMode: RenderModeType.renderModeHidden,
                    setupMode: VideoViewSetupMode.videoViewSetupAdd,
                  ),
                  connection: RtcConnection(
                    channelId: widget.channelName,
                  ),
                ),
              )
            : Center(
                child: CustomCircularProgressIndicator(),
              ),
      ),
    );
  }
}
