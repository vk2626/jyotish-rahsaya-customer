import 'package:flutter/material.dart';

import '../../utils/AppColor.dart';

class LiveUpcomingScreen extends StatefulWidget {
  const LiveUpcomingScreen({super.key});

  @override
  State<LiveUpcomingScreen> createState() => _LiveUpcomingScreenState();
}

class _LiveUpcomingScreenState extends State<LiveUpcomingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Upcoming Events"),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.search))],
      ),
      body: LayoutBuilder(builder: (context, BoxConstraints constraints) {
        return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 10,
                itemBuilder: (BuildContext context, int i) {
                  return Stack(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     SlideRightRoute(
                          //         page: AstrologerProfile(
                          //           uName: "${resAstroList[i].name.toString()}",
                          //           urExp:
                          //           "${resAstroList[i].experienceYear.toString()}",
                          //           uProfile: resAstroList[i].avatar.toString(),
                          //           uBio: "${resAstroList[i].bio.toString()}",
                          //           uPrice:
                          //           "${resAstroList[i].price == null ? '0' : resAstroList[i].price.toString()}",
                          //           uDiscount:
                          //           "${resAstroList[i].discountPrice == null ? '' : resAstroList[i].discountPrice.toString()}",
                          //           gallery: resAstroList[i].gallery,
                          //         )));
                          print("pos---> $i");
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10, top: 2),
                          child: Card(
                            color: Colors.white,
                            elevation: 2,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Card(
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      child: CircleAvatar(
                                        radius: 40,
                                        backgroundImage:
                                            AssetImage("asset/images/man.png"),
                                      ),
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color: AppColor.appColor,
                                              width: 2)),
                                    ),
                                    Text(
                                      "User Name",
                                      style: TextStyle(
                                          color: Colors.grey[600],
                                          fontSize: 17),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Event Name",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        "Date Mon, Day",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                        ),
                                        textAlign: TextAlign.center,
                                        maxLines: 3,
                                      ),
                                      Text(
                                        "Time",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(width: 10),
                                SizedBox(
                                  height: 100,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Align(
                                          alignment: Alignment.topRight,
                                          child: Icon(Icons.share)),
                                      SizedBox(
                                        height: 35,
                                        width: 130,
                                        child: ElevatedButton(
                                          onPressed: () {
                                            _settingModalBottomSheet(context);
                                          },
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  AppColor.appColor,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          5))),
                                          child: Text(
                                            "Join Wait list",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(width: 10),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
              SizedBox(height: 25)
            ],
          ),
        );
      }),
    );
  }

  void _settingModalBottomSheet(context) {
    // double totalAmount = 0.0;
    // var amount = '';
    // List<bool> isCheckedList =
    // List.generate(widget.price!.length, (index) => false);
    // final DateTime dateTime =
    // DateFormat("yyyy-MM-dd hh:mm:ss a").parse(widget.date.toString());
    // final String formattedDate = DateFormat('dd').format(dateTime);
    // final String formattedMonth = DateFormat('MMM').format(dateTime);
    // final String formattedYear = DateFormat('yyyy').format(dateTime);
    // final String formattedTime = DateFormat('hh:mm a').format(dateTime);
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 460,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            topLeft: Radius.circular(20))),
                  ),
                ),
                Column(
                  children: <Widget>[
                    CircleAvatar(
                        radius: 45,
                        backgroundImage: AssetImage("asset/images/man.png")),
                    Text("User Name", style: TextStyle(fontSize: 25)),
                    SizedBox(height: 30),
                    ListView.separated(
                      separatorBuilder: (BuildContext context, int index) {
                        return Center(child: Divider());
                      },
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Row(
                            children: <Widget>[
                              RichText(
                                text: TextSpan(
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: "Video Call",
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: " @ ₹ 21/min ",
                                      style: TextStyle(
                                          color: AppColor.darkGrey,
                                          fontSize: 14),
                                    ),
                                    TextSpan(
                                      text: "₹ 28/" + "min",
                                      style: TextStyle(
                                          color: AppColor.darkGrey,
                                          fontSize: 14,
                                          // decoration: widget
                                          //     .price![index]
                                          //     .priceDiscount ==
                                          //     null
                                          //     ? TextDecoration.none
                                          //     : TextDecoration
                                          //     .lineThrough,
                                          decorationColor: Colors.red),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          subtitle: Container(
                            child: Text(
                              "Both consulting and you on video call",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          trailing: SizedBox(
                            height: 35,
                            width: 100,
                            child: ElevatedButton(
                              onPressed: () {
                                // Navigator.of(context).push(
                                //     SlideRightRoute(
                                //         page: ChattingScreen()));
                                // _rechargeDialog(context);
                                _settingModalBottomSheet(context);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: AppColor.appColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5))),
                              child: Text(
                                "Join",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 15, color: Colors.black87),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            );
          });
        });
  }
}
