import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import '../dialog/showLoaderDialog.dart';
import '../router_constants.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math' as math;
import '../Core/Api/Constants.dart';
import '../Core/Provider/list.of.api.provider.dart';
import '../Core/logger_helper.dart';
import '../utils/AppColor.dart';
import '../utils/custom_circular_progress_indicator.dart';

class MyFollowingScreen extends StatefulWidget {
  const MyFollowingScreen({super.key});

  @override
  State<MyFollowingScreen> createState() => _MyFollowingScreenState();
}

class _MyFollowingScreenState extends State<MyFollowingScreen> {
  String? astroID = "";
  String? astroName = "";
  int? customerID;
  String customerName = "";

  @override
  void initState() {
    super.initState();
    getUserId();
  }

  void getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      // Retrieve values and handle null cases
      customerID = prefs.getInt(Constants.userID);
      customerName = prefs.getString(Constants.fullName)!;
    });
  }

  @override
  Widget build(BuildContext context) {
    // context
    //     .read<ListOfApisProvider>()
    //     .followingProvider(customerID: customerID.toString());
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Following".tr(),
          style: GoogleFonts.poppins(),
        ),
      ),
      body: SingleChildScrollView(
        child:
            Consumer<ListOfApisProvider>(builder: (context, followingState, _) {
          return FutureBuilder(
              future: followingState.followingProvider(
                  customerID: customerID.toString()),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CustomCircularProgressIndicator());
                } else if (snapshot.hasData) {
                  if (snapshot.data!.followerDetails!.isEmpty) {
                    return Center(
                      child: Text("You're not following anyone."),
                    );
                  } else {
                    var followingData = snapshot.data!.followerDetails;
                    return ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: followingData!.length,
                        itemBuilder: (context, index) {
                          final astrologerFollowing = followingData[index];
                          return InkWell(
                            onTap: () {
                              context.pushNamed(
                                  RouteConstants.astrologerProfile,
                                  queryParameters: {
                                    'astrologerId':
                                        astrologerFollowing.id.toString(),
                                    'type': 'chat',
                                  });
                            },
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 10, right: 10, top: 2),
                              child: Card(
                                color: Colors.white,
                                elevation: 2,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                                child: Stack(
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(width: 20),
                                        Column(
                                          children: <Widget>[
                                            SizedBox(height: 10),
                                            Card(
                                              clipBehavior:
                                                  Clip.antiAliasWithSaveLayer,
                                              child: CircleAvatar(
                                                radius: 40,
                                                backgroundImage:
                                                    CachedNetworkImageProvider(
                                                        "${astrologerFollowing.avatar}"),
                                              ),
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: AppColor.appColor,
                                                      width: 2)),
                                            ),
                                            RatingBarIndicator(
                                                rating: astrologerFollowing
                                                    .astrologer.rating
                                                    .toDouble(),
                                                itemCount: 5,
                                                itemSize: 15.0,
                                                itemBuilder: (context, _) =>
                                                    const Icon(
                                                      Icons.star,
                                                      color: Colors.amber,
                                                    )),
                                            Text(
                                              "${astrologerFollowing.astrologer.orderCount} " +
                                                  "orders".tr(),
                                              style: GoogleFonts.poppins(
                                                  color: Colors.grey[600],
                                                  fontSize: 12),
                                              textAlign: TextAlign.center,
                                            ),
                                            SizedBox(height: 10),
                                          ],
                                        ),
                                        SizedBox(width: 10),
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .3,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              SizedBox(height: 10),
                                              Text(
                                                "${astrologerFollowing.name}",
                                                style: GoogleFonts.poppins(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                  astrologerFollowing
                                                      .astrologer.allSkillName,
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                  ),
                                                  maxLines: 3),
                                              Text(
                                                  "Exp".tr() +
                                                      " : ${astrologerFollowing.astrologer!.experienceYear}",
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.grey[600],
                                                  ),
                                                  textAlign: TextAlign.center),
                                              SizedBox(height: 10),
                                            ],
                                          ),
                                        ),
                                        Spacer(),
                                        SizedBox(
                                          height: 100,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              TextButton(
                                                onPressed: () {
                                                  context
                                                      .read<
                                                          ListOfApisProvider>()
                                                      .followerProvider(
                                                          customerID: customerID
                                                              .toString(),
                                                          astrologerID:
                                                              followingData[
                                                                      index]
                                                                  .id
                                                                  .toString(),
                                                          status: '0');
                                                  followingData.removeAt(index);
                                                  print(
                                                      "Name Click---> ${astroName}");
                                                  print(
                                                      "AstroID Click ${astroID.toString()}");
                                                },
                                                style: TextButton.styleFrom(
                                                    backgroundColor: AppColor
                                                        .btnActiveColorLight,
                                                    side: BorderSide(
                                                        color:
                                                            AppColor.appColor),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5))),
                                                child: Text(
                                                  "Unfollow".tr(),
                                                  style: GoogleFonts.poppins(
                                                      fontSize: 16,
                                                      color:
                                                          AppColor.blackColor),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                      ],
                                    ),
                                    if (astrologerFollowing.label != 0)
                                      Positioned(
                                        left: -25,
                                        top: 15,
                                        child: Transform.rotate(
                                          angle: -math.pi / 3.5,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                colors: [
                                                  Color(
                                                      0xFF3D3D3D), // Dark gray (closer to black)
                                                  Color(
                                                      0xFF1C1C1C), // Almost black
                                                  Color(
                                                      0xFF000000), // Pure black
                                                  Color(
                                                      0xFF434343), // Slightly lighter for a metallic effect
                                                ],
                                                begin: Alignment.topCenter,
                                                end: Alignment.bottomCenter,
                                              ),
                                            ),
                                            width: 100,
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.symmetric(
                                              vertical: 2,
                                            ),
                                            child: Text(
                                              astrologerFollowing
                                                  .astrologer.labelText,
                                              textAlign: TextAlign.center,
                                              style: GoogleFonts.poppins(
                                                color: AppColor.whiteColor,
                                                fontSize: 10,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        });
                  }
                } else {
                  TLoggerHelper.error("Error building widget");
                  return SizedBox();
                }
              });
        }),
      ),
    );
  }
}
