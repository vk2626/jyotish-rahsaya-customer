import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../utils/AppColor.dart';

Future showLoaderDialog(BuildContext context) async{
  AlertDialog alert = AlertDialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
    content: Row(
      children: [
        const CircularProgressIndicator(color: AppColor.appColor),
        Container(
            margin: const EdgeInsets.only(left: 35),
            child: Text(
              "Processing".tr()+"...",
              style: GoogleFonts.poppins(),
            )),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
