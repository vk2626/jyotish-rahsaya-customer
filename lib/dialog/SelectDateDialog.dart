

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateDialog extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _DateDialogState();
  }
}

class _DateDialogState extends State<DateDialog> {
  var startDate = "From Date";
  var endDate = "To Date";
  //var dateData = jsonEncode({ 'Date': { 'startDate': startDate, 'endDate' : endDate } });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: ()=> _selectDate(context,"1"),
                    child:  Card(
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(color: Color.fromARGB(255, 27, 58, 104),),
                        borderRadius: BorderRadius.circular(10.0), //<-- SEE HERE
                      ),
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 12,horizontal: 5),
                        child: Row(
                          children: [
                            Expanded(flex: 1,
                                child: Align(
                                  alignment: Alignment.center,
                                  child:  Text(
                                    startDate,
                                    style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                                  ) ,
                                )),
                            const Icon(Icons.calendar_month, size: 18,)
                          ],
                        ),
                      ),
                    ),
                  )),
              const SizedBox(width: 10,),
              Image.asset("assets/images/opposite_arrow.png", scale: 35,),
              const SizedBox(width: 10,),
              Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: ()=> _selectDate(context,"2"),
                    child:Card(
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(color: Color.fromARGB(255, 27, 58, 104),),
                        borderRadius: BorderRadius.circular(10.0), //<-- SEE HERE
                      ),
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 12,horizontal: 5),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: Align(
                                  alignment: Alignment.center,
                                  child:  Text(
                                    endDate,
                                    style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                                  ) ,
                                )),
                            const Icon(Icons.calendar_month, size: 18,)
                          ],
                        ),
                      ),
                    ),
                  ))
            ],
          ),
          const SizedBox(height: 20,),
          const Text("Select the dates to get your attendance summary for more than seven days." ,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14)),
          const SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                  onPressed: () => Navigator.pop(context),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red
                  ),
                  child: const Text('Cancel')),
              const SizedBox(width: 20,),
              ElevatedButton(
                  onPressed: () => Navigator.pop(context, jsonEncode({'startDate': startDate, 'endDate' : endDate})),
                  child: const Text('Search')),
            ],
          ),
        ],),
    );
  }
  _selectDate(BuildContext context, String type) async {
    print("DatePickerDialog");
    DateTime selectedDate = DateTime.now();
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(), // Refer step 1
        firstDate: DateTime(1950),
        lastDate: DateTime.now(),
        helpText: 'Select date range', // Can be used as title
        cancelText: 'Not Now',
        confirmText: 'Confirm'
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        String formattedDate = DateFormat('dd-MM-yyyy').format(picked);
        if(type == "1") {
          startDate = formattedDate;
        }
        else if(type == "2") {
          endDate = formattedDate;
        }
      });
    }
  }
}


/*
selectDateDialog(BuildContext context){

  AlertDialog alert=AlertDialog(
    content: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Card(
              shape: RoundedRectangleBorder(
                side: const BorderSide(color: Color.fromARGB(255, 27, 58, 104),),
                borderRadius: BorderRadius.circular(10.0), //<-- SEE HERE
              ),
              child: Container(
                padding: const EdgeInsets.all(16),
                child: Row(
                  children: [
                    Text(
                      startDate,
                      style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    const Icon(Icons.calendar_month, size: 20,)
                  ],
                ),
              ),
            ),
            const Spacer(),
            Image.asset("assets/images/opposite_arrow.png", scale: 20,),
            const Spacer(),
            Card(
              shape: RoundedRectangleBorder(
                side: const BorderSide(color: Color.fromARGB(255, 27, 58, 104),),
                borderRadius: BorderRadius.circular(10.0), //<-- SEE HERE
              ),
              child: Container(
                padding: const EdgeInsets.all(16),
                child: Row(
                  children: [
                    GestureDetector(
                      child:Text(
                        endDate,
                        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                      ) ,
                      onTap: () async {
                        DateTime? pickedDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1950),
                            //DateTime.now() - not to allow to choose before today.
                            lastDate: DateTime(DateTime.now() as int));

                        if (pickedDate != null) {
                          print(pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                          String formattedDate =
                          DateFormat('yyyy-MM-dd').format(pickedDate);
                          print(formattedDate); //formatted date output using intl package =>  2021-03-16
                          setState(() {

                          });
                        } else {}
                      },
                    ),
                    const Icon(Icons.calendar_month, size: 20,)
                  ],
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('cancel')),
            ElevatedButton(
                onPressed: () => Navigator.pop(context, jsonEncode({'startDate': startDate, 'endDate' : endDate})),
                child: const Text('confirm')),
          ],
        ),
      ],),
  );
  showDialog(barrierDismissible: false,
    context:context,
    builder:(BuildContext context){
      return alert;
    },
  );
}*/
