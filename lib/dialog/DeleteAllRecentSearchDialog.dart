import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utils/AppColor.dart';

DeleteAll(BuildContext context){
  return AlertDialog(
    content: SizedBox(
      height: 60,
      width: MediaQuery.of(context).size.width-10,
      child: Column(
        children: [
          Text("Do you wish to delete all your previous searches?"),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.pop(context, "yes");
                },
                child: Text("YES", style: TextStyle(color: Colors.red[800],fontWeight: FontWeight.bold),),
              ),
              SizedBox(width: 15,),
              GestureDetector(
                onTap: (){
                  Navigator.pop(context, "no");
                },
                child: Text("NO", style: TextStyle(color: AppColor.blackColor,fontWeight: FontWeight.bold),),
              )
            ],
          )
        ],
      ),
    ),
  );
}