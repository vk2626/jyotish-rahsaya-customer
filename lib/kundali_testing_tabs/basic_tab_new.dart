import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/kundli_provider_new.dart';
import '../Core/formatter.dart';
import '../../../utils/custom_circular_progress_indicator.dart';
import '../utils/AppColor.dart';

class KundliBasicTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;
  const KundliBasicTabNew(
      {super.key,
      required this.lang,
      required this.lat,
      required this.long,
      required this.dateTime});

  @override
  State<KundliBasicTabNew> createState() => _KundliBasicTabNewState();
}

class _KundliBasicTabNewState extends State<KundliBasicTabNew>
    with AutomaticKeepAliveClientMixin {
  Map<String, dynamic> basicData = {};
  Map<String, dynamic> basicPanchang = {};
  bool isLoadingKundli = true;
  bool isLoadingPanchang = true;
  @override
  void initState() {
    super.initState();
    getBasicData();
    getPanchangData();
  }

  getBasicData() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getBasicDetails(lang: widget.lang,
            dateTime: widget.dateTime, lat: widget.lat, long: widget.long)
        .then(
      (value) {
        if (value.isNotEmpty) {
          setState(() {
            basicData = value;
            isLoadingKundli = false;
          });
        } else {}
      },
    );
  }

  getPanchangData() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getPanchangDetails(lang: widget.lang,
            dateTime: widget.dateTime, lat: widget.lat, long: widget.long)
        .then(
      (value) {
        if (value.isNotEmpty) {
          setState(() {
            basicPanchang = value;
            isLoadingPanchang = false;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.lang == "en" ? "Kundli Basic Details" : "कुंडली मूल विवरण",
              style: GoogleFonts.poppins(
                  fontSize: 18, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 12),
            Container(
              decoration: BoxDecoration(
                  border:
                      Border.all(color: Colors.grey.withOpacity(.2), width: 2),
                  borderRadius: BorderRadius.circular(12)),
              child: isLoadingKundli
                  ? SizedBox(
                      height: MediaQuery.of(context).size.height * .2,
                      child: Center(
                        child: CustomCircularProgressIndicator(),
                      ),
                    )
                  : Column(
                      children: List.generate(
                          basicData.length,
                          (index) => Container(
                                padding: const EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                    color: index % 2 == 0
                                        ? AppColor.appColor
                                        : Colors.transparent),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      TFormatter.convertToReadableFormat(
                                          basicData.entries
                                              .elementAt(index)
                                              .key
                                              .toString()),
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    )),
                                    Expanded(
                                        child: Text(
                                      basicData.entries
                                          .elementAt(index)
                                          .value
                                          .toString(),
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                              )),
                    ),
            ),
            SizedBox(height: 18),
            Text(
              widget.lang == "en" ? "Panchang Details" : "पंचांग विवरण",
              style: GoogleFonts.poppins(
                  fontSize: 18, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 12),
            Container(
              decoration: BoxDecoration(
                  border:
                      Border.all(color: Colors.grey.withOpacity(.2), width: 2),
                  borderRadius: BorderRadius.circular(12)),
              child: isLoadingPanchang
                  ? SizedBox(
                      height: MediaQuery.of(context).size.height * .2,
                      child: Center(
                        child: CustomCircularProgressIndicator(),
                      ),
                    )
                  : Column(
                      children: List.generate(
                          basicPanchang.length,
                          (index) => Container(
                                padding: const EdgeInsets.all(8.0),
                                decoration: BoxDecoration(
                                    color: index % 2 == 0
                                        ? AppColor.appColor
                                        : Colors.transparent),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      TFormatter.convertToReadableFormat(
                                          basicPanchang.entries
                                              .elementAt(index)
                                              .key
                                              .toString()),
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    )),
                                    Expanded(
                                        child: Text(
                                      TFormatter.capitalize(basicPanchang
                                          .entries
                                          .elementAt(index)
                                          .value
                                          .toString()),
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                              )),
                    ),
            ),
            SizedBox(height: 48),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
