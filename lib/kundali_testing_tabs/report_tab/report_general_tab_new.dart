import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../../../utils/custom_circular_progress_indicator.dart';
import '../../Core/Provider/kundli_provider_new.dart';

class ReportGeneralTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;
  const ReportGeneralTabNew(
      {super.key,
      required this.lat,
      required this.long,
      required this.lang,
      required this.dateTime});

  @override
  State<ReportGeneralTabNew> createState() => _ReportGeneralTabNewState();
}

class _ReportGeneralTabNewState extends State<ReportGeneralTabNew> {
  List<String> selectionList = ["Nakshatra", "Ascendant"];
  Map<String, dynamic> nakshatraData = {};
  Map<String, dynamic> ascendantData = {};
  bool isNakshatraLoading = true;
  bool isAscendantLoading = true;
  int selectionIndex = 0;

  @override
  void initState() {
    super.initState();
    getGeneralDetails(type: "nakshatra");
  }

  getGeneralDetails({required String type}) async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getGeneralReportDetails(lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long,
            type: type)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          if (type == "nakshatra") {
            nakshatraData = value;
            isNakshatraLoading = false;
          } else {
            ascendantData = value;
            isAscendantLoading = false;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                  children: List.generate(
                selectionList.length,
                (index) => Container(
                  margin: const EdgeInsets.only(right: 8.0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        selectionIndex = index;
                        if (nakshatraData.isEmpty && selectionIndex == 0) {
                          isNakshatraLoading = true;
                        } else if (selectionIndex == 1 &&
                            ascendantData.isEmpty) {
                          isAscendantLoading = true;
                        }
                      });
                      if (nakshatraData.isEmpty && selectionIndex == 0) {
                        getGeneralDetails(type: "nakshatra");
                      } else if (selectionIndex == 1 && ascendantData.isEmpty) {
                        getGeneralDetails(type: "ascendant");
                      }
                    },
                    child: Chip(
                      label: Text(selectionList[index],
                          style:
                              GoogleFonts.poppins(fontWeight: FontWeight.w500)),
                      color: WidgetStatePropertyAll(index == selectionIndex
                          ? AppColor.appColor
                          : Colors.transparent),
                    ),
                  ),
                ),
              )),
            ),
            const SizedBox(height: 12),
            isAscendantLoading || isNakshatraLoading
                ? SizedBox(
                    height: MediaQuery.of(context).size.height * .5,
                    child: Center(
                      child: CustomCircularProgressIndicator(),
                    ),
                  )
                : selectionIndex == 0
                    ? Column(
                        children: List.generate(
                            selectionIndex,
                            (index) => Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(12),
                                  margin: EdgeInsets.only(bottom: 12),
                                  decoration: BoxDecoration(
                                      border: Border.all(),
                                      borderRadius: BorderRadius.circular(12)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Analysis",
                                        style: GoogleFonts.poppins(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(height: 8),
                                      Text(
                                        "You have an abundance of physical vitality. Sometimes there is so much energy that you become reckless, impulsive, and throw caution to the wind. You may experience cuts and burns on your head and face which may leave some sort of scar. You are assertive, independent, impatient and want to do your own thing. You have strong organizing ability. You are usually self-confident. Guard against accidents due to rushing around in your impulsiveness. You may be subject to higher than normal fevers.",
                                        style: GoogleFonts.poppins(
                                            color: Colors.black54),
                                      )
                                    ],
                                  ),
                                )))
                    : Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(12),
                        margin: EdgeInsets.only(bottom: 12),
                        decoration: BoxDecoration(
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(12)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Ascendant ",
                                  style: GoogleFonts.poppins(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  "(" +
                                      ascendantData['asc_report']['ascendant']
                                          .toString() +
                                      ")",
                                  style: GoogleFonts.poppins(
                                      fontSize: 14, color: Colors.grey),
                                )
                              ],
                            ),
                            SizedBox(height: 8),
                            Text(
                              ascendantData['asc_report']['report'].toString(),
                              style: GoogleFonts.poppins(color: Colors.black54),
                            )
                          ],
                        ),
                      )
          ],
        ),
      ),
    );
  }
}
