import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../../../utils/custom_circular_progress_indicator.dart';
import '../../Core/Provider/kundli_provider_new.dart';
import '../../Core/formatter.dart';

class ReportRemediesTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;
  const ReportRemediesTabNew(
      {super.key,
      required this.lat,
      required this.long,
      required this.lang,
      required this.dateTime});

  @override
  State<ReportRemediesTabNew> createState() => _ReportRemediesTabNewState();
}

class _ReportRemediesTabNewState extends State<ReportRemediesTabNew> {
  List<String> selectionList = ["Rudraksha", "Gemstones"];
  Map<String, dynamic> rudrakshaData = {};
  Map<String, dynamic> gemstoneData = {};
  bool isRudrakshaLoading = true;
  bool isGemstoneLoading = true;
  int selectionIndex = 0;

  @override
  void initState() {
    super.initState();
    getRemediesData(type: "rudraksha");
  }

  getRemediesData({required String type}) async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getRemediesReportDetails(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long,
            type: type)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          if (type == "rudraksha") {
            rudrakshaData = value;
            isRudrakshaLoading = false;
          } else {
            gemstoneData = value;
            isGemstoneLoading = false;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                  children: List.generate(
                selectionList.length,
                (index) => Container(
                  margin: const EdgeInsets.only(right: 8.0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        selectionIndex = index;
                        if (rudrakshaData.isEmpty && selectionIndex == 0) {
                          isRudrakshaLoading = true;
                        } else if (selectionIndex == 1 &&
                            gemstoneData.isEmpty) {
                          isGemstoneLoading = true;
                        }
                      });
                      if (rudrakshaData.isEmpty && selectionIndex == 0) {
                        getRemediesData(type: "rudraksha");
                      } else if (selectionIndex == 1 && gemstoneData.isEmpty) {
                        getRemediesData(type: "gemstone");
                      }
                    },
                    child: Chip(
                      clipBehavior: Clip.none,
                      label: Text(selectionList[index],
                          style:
                              GoogleFonts.poppins(fontWeight: FontWeight.w500)),
                      color: WidgetStatePropertyAll(index == selectionIndex
                          ? AppColor.appColor
                          : Colors.transparent),
                    ),
                  ),
                ),
              )),
            ),
            SizedBox(height: 12),
            if (selectionIndex == 0)
              isRudrakshaLoading
                  ? SizedBox(
                      height: MediaQuery.of(context).size.height * .5,
                      child: Center(
                        child: CustomCircularProgressIndicator(),
                      ),
                    )
                  : Column(children: [
                      CustomContainer(
                          title: "Name", data: rudrakshaData['name']),
                      CustomContainer(
                          title: "Recommend", data: rudrakshaData['recommend']),
                      CustomContainer(
                          title: "Detail", data: rudrakshaData['detail']),
                    ]),
            if (selectionIndex == 1)
              isGemstoneLoading
                  ? SizedBox(
                      height: MediaQuery.of(context).size.height * .5,
                      child: Center(
                        child: CustomCircularProgressIndicator(),
                      ),
                    )
                  : Column(
                      children: gemstoneData.entries.map((entry) {
                        return Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(12),
                          margin: EdgeInsets.only(bottom: 12),
                          decoration: BoxDecoration(
                              border: Border.all(),
                              borderRadius: BorderRadius.circular(12)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                entry.key,
                                style: GoogleFonts.poppins(
                                    fontSize: 18, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(height: 8),
                              Table(
                                defaultVerticalAlignment:
                                    TableCellVerticalAlignment.middle,
                                columnWidths: const {
                                  0: FlexColumnWidth(2),
                                  1: FlexColumnWidth(3),
                                },
                                children: [
                                  _buildTableRow(
                                      "Gemstone", entry.value['name']),
                                  _buildTableRow(
                                      "Semi-Gemstone", entry.value['semi_gem']),
                                  _buildTableRow("Wear Finger",
                                      entry.value['wear_finger']),
                                  _buildTableRow("Weight Caret",
                                      entry.value['weight_caret']),
                                  _buildTableRow(
                                      "Wear Metal", entry.value['wear_metal']),
                                  _buildTableRow(
                                      "Wear Day", entry.value['wear_day']),
                                  _buildTableRow(
                                      "Deity", entry.value['gem_deity']),
                                ],
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                    )
          ],
        ),
      ),
    );
  }

  TableRow _buildTableRow(String key, String value) {
    return TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            key,
            style:
                GoogleFonts.poppins(fontSize: 14, fontWeight: FontWeight.w500),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            value,
            style: GoogleFonts.poppins(fontSize: 14, color: Colors.black54),
          ),
        ),
      ],
    );
  }
}

class CustomContainer extends StatelessWidget {
  final String title;
  final String data;
  const CustomContainer({super.key, required this.title, required this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(12),
      margin: EdgeInsets.only(bottom: 12),
      decoration: BoxDecoration(
          border: Border.all(), borderRadius: BorderRadius.circular(12)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          SizedBox(height: 8),
          Text(
            TFormatter.capitalize(data),
            style: GoogleFonts.poppins(color: Colors.black54),
          )
        ],
      ),
    );
  }
}
