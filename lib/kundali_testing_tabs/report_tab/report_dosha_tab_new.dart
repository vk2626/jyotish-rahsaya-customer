import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../../../utils/custom_circular_progress_indicator.dart';
import '../../Core/Provider/kundli_provider_new.dart';

class ReportDoshaTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;

  const ReportDoshaTabNew(
      {super.key,
      required this.lang,
      required this.lat,
      required this.long,
      required this.dateTime});

  @override
  State<ReportDoshaTabNew> createState() => _ReportDoshaTabNewState();
}

class _ReportDoshaTabNewState extends State<ReportDoshaTabNew> {
  Map<String, dynamic> manglikData = {};
  Map<String, dynamic> kalSarpData = {};
  Map<String, dynamic> sadeSatiData = {};
  bool isManglikLoading = true;
  bool isKalsarpLoading = true;
  bool isSadesatiLoading = true;
  int selectionIndex = 0;

  @override
  void initState() {
    super.initState();
    getManglikDetails();
  }

  getManglikDetails() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getManglikDoshaReportDetails(
          lang: widget.lang,
            dateTime: widget.dateTime, lat: widget.lat, long: widget.long)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          manglikData = value;
          isManglikLoading = false;
        });
      }
    });
  }

  getKalsarpDetails() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getKalsarpDoshaReportDetails(
          lang: widget.lang,
            dateTime: widget.dateTime, lat: widget.lat, long: widget.long)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          kalSarpData = value;
          isKalsarpLoading = false;
        });
      }
    });
  }

  getSadesatiDetails() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getSadesatiDoshaReportDetails(
          lang: widget.lang,
            dateTime: widget.dateTime, lat: widget.lat, long: widget.long)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          sadeSatiData = value;
          isSadesatiLoading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<String> sadeSatiTableColumnsList = widget.lang == "en"
        ? ["Start", "End", "Sign Name", "Type"]
        : ["प्रारंभ", "अंत", "चिह्न नाम", "प्रकार"];
    List<String> selectionList = widget.lang == 'en'
        ? ["Manglik", "Kalsarpa", "Sadesati"]
        : ["मांगलिक", "कालसर्प", "साढ़ेसाती"];
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                  children: List.generate(
                selectionList.length,
                (index) => Container(
                  margin: const EdgeInsets.only(right: 8.0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        selectionIndex = index;
                        if (selectionIndex == 1 && kalSarpData.isEmpty) {
                          getKalsarpDetails();
                        } else if (selectionIndex == 2 &&
                            sadeSatiData.isEmpty) {
                          getSadesatiDetails();
                        }
                      });
                    },
                    child: Chip(
                      label: Text(selectionList[index],
                          style:
                              GoogleFonts.poppins(fontWeight: FontWeight.w500)),
                      backgroundColor: index == selectionIndex
                          ? AppColor.appColor
                          : Colors.transparent,
                    ),
                  ),
                ),
              )),
            ),
            const SizedBox(height: 12),
            Text(
              "Analysis",
              style: GoogleFonts.poppins(
                  fontSize: 18, fontWeight: FontWeight.w500),
            ),
            const SizedBox(height: 12),
            // Conditional rendering for each selection's content
            if (selectionIndex == 0 && isManglikLoading ||
                selectionIndex == 1 && isKalsarpLoading ||
                selectionIndex == 2 && isSadesatiLoading)
              const Center(child: CustomCircularProgressIndicator()),

            if (selectionIndex == 0 && !isManglikLoading ||
                selectionIndex == 1 && !isKalsarpLoading ||
                selectionIndex == 2 && !isSadesatiLoading)
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.green, width: 1.5),
                    borderRadius: BorderRadius.circular(8)),
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 36,
                      backgroundColor: Colors.green,
                      child: Text(
                        selectionIndex == 0
                            ? (manglikData['is_present'] as bool)
                                ? "Yes"
                                : "No"
                            : selectionIndex == 1
                                ? (kalSarpData['present'] as bool)
                                    ? "Yes"
                                    : "No"
                                : (sadeSatiData['sadhesati_status'] as bool)
                                    ? "Yes"
                                    : "No",
                        style: GoogleFonts.poppins(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(width: 12),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          selectionIndex == 0
                              ? "Manglik"
                              : selectionIndex == 1
                                  ? "Kalsarpa"
                                  : "Sadesati",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        ),
                        SizedBox(height: 4),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .6,
                          child: Text(
                            selectionIndex == 0
                                ? manglikData['manglik_report'] ?? ""
                                : selectionIndex == 1
                                    ? kalSarpData['one_line'] ?? ""
                                    : sadeSatiData['is_undergoing_sadhesati'] ??
                                        "",
                            maxLines: null,
                            style: GoogleFonts.poppins(
                                fontWeight: FontWeight.w500, fontSize: 15),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            const SizedBox(height: 12),
            if (selectionIndex == 0 || selectionIndex == 1)
              Text(
                widget.lang == "en"
                    ? "This is a computer generated result. Please consult an Astrologer to confirm & understand this in detail."
                    : "यह एक कंप्यूटर जनित परिणाम है। इसकी पुष्टि करने और विस्तार से समझने के लिए कृपया किसी ज्योतिषी से परामर्श लें।",
                maxLines: 3,
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 13,
                    color: Colors.grey),
              ),
          ],
        ),
      ),
    );
  }
}
