import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/AppColor.dart';

import 'report_dosha_tab_new.dart';
import 'report_general_tab_new.dart';
import 'report_remedies_tab_new.dart';

class ReportTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;
  const ReportTabNew(
      {super.key,
      required this.lang,
      required this.lat,
      required this.long,
      required this.dateTime});

  @override
  State<ReportTabNew> createState() => _ReportTabNewState();
}

class _ReportTabNewState extends State<ReportTabNew>
    with AutomaticKeepAliveClientMixin {
  List<String> _tabs = ["General", "Remedies", "Dosha"];
  List<Widget> _tabsWidgetList = [];
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _tabsWidgetList = [
      ReportGeneralTabNew(
          lat: widget.lat,
          long: widget.long,
          lang: widget.lang,
          dateTime: widget.dateTime),
      ReportRemediesTabNew(
          lat: widget.lat,
          long: widget.long,
          lang: widget.lang,
          dateTime: widget.dateTime),
      ReportDoshaTabNew(
          lat: widget.lat,
          long: widget.long,
          lang: widget.lang,
          dateTime: widget.dateTime)
    ];
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return DefaultTabController(
      length: _tabs.length,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TabBar(
              tabs: _tabs.map((tab) => Tab(text: tab)).toList(),
              labelStyle: GoogleFonts.lato(), // Example of setting font
              indicatorColor: AppColor.appColor,
              indicatorSize: TabBarIndicatorSize.tab,
            ),
            Expanded(
              child: TabBarView(children: _tabsWidgetList),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
