import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/kundli_provider_new.dart';
import '../Core/formatter.dart';
import '../Core/logger_helper.dart';

class DashaTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;

  const DashaTabNew({
    super.key,
    required this.lang,
    required this.lat,
    required this.long,
    required this.dateTime,
  });

  @override
  State<DashaTabNew> createState() => _DashaTabNewState();
}

class _DashaTabNewState extends State<DashaTabNew>
    with AutomaticKeepAliveClientMixin {
  Map<String, dynamic> vimshottariDashaMap = {};
  List<dynamic> currentDashaList = [];
  String currentLevel = "major"; // Tracks the current dasha level
  int vimshottariDashaIndex = 0;

  @override
  void initState() {
    super.initState();
    getVimshottariDashaData();
  }

  // Fetch Dasha data and initialize with the major dasha level
  getVimshottariDashaData() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getDashaDetails(
            dateTime: widget.dateTime,
            lat: widget.lat,
            lang: widget.lang,
            long: widget.long)
        .then((value) {
      setState(() {
        vimshottariDashaMap = value;
        currentDashaList = vimshottariDashaMap['major']['dasha_period'] ?? [];
      });
      TLoggerHelper.debug(vimshottariDashaMap.entries.toString());
    });
  }

  // Function to drill down to next level of dasha
  void goToNextDashaLevel(String nextLevel) {
    setState(() {
      currentLevel = nextLevel;
      if (nextLevel == "major") {
        vimshottariDashaIndex = 0;
        currentDashaList = vimshottariDashaMap['major']['dasha_period'];
      } else if (nextLevel == "minor") {
        vimshottariDashaIndex = 1;
        currentDashaList = vimshottariDashaMap['minor']['dasha_period'];
      } else if (nextLevel == "sub_minor") {
        vimshottariDashaIndex = 2;
        currentDashaList = vimshottariDashaMap['sub_minor']['dasha_period'];
      } else if (nextLevel == "sub_sub_minor") {
        vimshottariDashaIndex = 3;
        currentDashaList = vimshottariDashaMap['sub_sub_minor']['dasha_period'];
      } else if (nextLevel == "sub_sub_sub_minor") {
        vimshottariDashaIndex = 4;
        currentDashaList =
            vimshottariDashaMap['sub_sub_sub_minor']['dasha_period'];
      } else {
        currentDashaList = [];
      }
    });
  }

  // Function to handle the display level name
  String getLevelName() {
    switch (currentLevel) {
      case "major":
        return widget.lang == "en" ? "MahaDasha" : "विंशोत्तरी";
      case "minor":
        return widget.lang == "en" ? "AntarDasha" : "अंतरदशा";
      case "sub_minor":
        return widget.lang == "en" ? "PratyantarDasha" : "प्रत्यंतरदशा";
      case "sub_sub_minor":
        return widget.lang == "en" ? "SookshmaDasha" : "सूक्ष्मदशा";
      case "sub_sub_sub_minor":
        return widget.lang == "en" ? "PranaDasha" : "प्राणदशा";
      default:
        return widget.lang == "en" ? "MahaDasha" : "विंशोत्तरी";
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    List<String> dashaVimshottariListString = widget.lang == "en"
        ? [
            "MahaDasha",
            "AntarDasha",
            "PratyantarDasha",
            "SookshmaDasha",
            "Prana"
          ]
        : [
            "महादशा", // MahaDasha
            "अंतरदशा", // AntarDasha
            "प्रत्यंतरदशा", // PratyantarDasha
            "सूक्ष्मदशा", // SookshmaDasha
            "प्राण" // Prana
          ];

    List<String> columnsList = widget.lang == "en"
        ? ["Planets", "Start Date", "End Date"]
        : ["ग्रह", "प्रारंभ तिथि", "अंत तिथि"];

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Chip showing the current dasha level
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Chip(
              label: Text(
                widget.lang == "en" ? "Vimshottari" : "विंशोत्तरी",
                style: GoogleFonts.poppins(fontWeight: FontWeight.w500),
              ),
              backgroundColor: AppColor.appColor,
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                vimshottariDashaIndex + 1,
                (index) => GestureDetector(
                  onTap: () {
                    switch (index) {
                      case 0:
                        goToNextDashaLevel("major");
                        break;
                      case 1:
                        goToNextDashaLevel("minor");
                        break;
                      case 2:
                        goToNextDashaLevel("sub_minor");
                        break;
                      case 3:
                        goToNextDashaLevel("sub_sub_minor");
                        break;
                      case 4:
                        goToNextDashaLevel("sub_sub_sub_minor");
                        break;
                      default:
                        goToNextDashaLevel("major");
                        break;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      children: [
                        Text(
                          dashaVimshottariListString[index],
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            // color: index == currentDashaPath.length - 1
                            //     ? Colors.black
                            //     : Colors.blue,
                          ),
                        ),
                        // if (index != currentDashaPath.length - 1)
                        const Icon(Icons.chevron_right, color: Colors.grey),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 8),
          // Dasha Data Table
          Expanded(
            child: DataTable(
              border: TableBorder.all(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey.withOpacity(.2),
                width: 2,
              ),
              headingRowColor: MaterialStateProperty.all(AppColor.appColor),
              dataTextStyle: GoogleFonts.poppins(fontSize: 13),
              columns: List.generate(
                columnsList.length,
                (index) => DataColumn(
                  label: Container(
                    alignment: Alignment.center,
                    child: Text(
                      TFormatter.capitalizeSentence(columnsList[index]),
                    ),
                  ),
                ),
              ),
              rows: currentDashaList.map<DataRow>((period) {
                // Check if the current period has nested dashas
                bool hasNestedDasha = vimshottariDashaIndex < 4;

                return DataRow(
                  cells: [
                    DataCell(
                      Container(
                        alignment: Alignment.center,
                        child: Text(period['planet'] ?? ""),
                      ),
                    ),
                    DataCell(
                      Container(
                        alignment: Alignment.center,
                        child: Text(
                          period['start'] ?? "",
                        ),
                      ),
                    ),
                    DataCell(
                      GestureDetector(
                        onTap: () {
                          if (currentLevel == "major") {
                            goToNextDashaLevel("minor");
                          } else if (currentLevel == "minor") {
                            goToNextDashaLevel("sub_minor");
                          } else if (currentLevel == "sub_minor") {
                            goToNextDashaLevel("sub_sub_minor");
                          } else if (currentLevel == "sub_sub_minor") {
                            goToNextDashaLevel("sub_sub_sub_minor");
                          }
                        },
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(
                            // period['end'] ?? "" + (hasNestedDasha ? " >" : ""),,
                            period['end'],
                            style: TextStyle(
                              color:
                                  hasNestedDasha ? Colors.blue : Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
