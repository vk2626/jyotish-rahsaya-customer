import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/kundli_provider_new.dart';
import 'astakvarga_tab_new.dart';
import 'basic_tab_new.dart';
import 'chart_tab_new.dart';
import 'dasha_tab_new.dart';
import 'kp_tab_new.dart';
import 'report_tab/report_tab_new.dart';

class KundliTesting extends StatefulWidget {
  bool isEnglish;
  final String lat;
  final String long;
  final DateTime dateTime;
  KundliTesting(
      {super.key,
      this.isEnglish = true,
      required this.lat,
      required this.long,
      required this.dateTime});

  @override
  State<KundliTesting> createState() => _KundliTestingState();
}

class _KundliTestingState extends State<KundliTesting> {
  late bool isEnglish;
  late String lat;
  late String long;
  late DateTime dateTime;
  @override
  void initState() {
    super.initState();
    isEnglish = widget.isEnglish;
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    lat = widget.lat;
    long = widget.long;
    dateTime = widget.dateTime;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Kundli",style: GoogleFonts.poppins(),),
          actions: [
            Row(
              children: [
                Text(
                  widget.isEnglish ? "English" : "हिंदी",
                  style: GoogleFonts.poppins(),
                ),
                const SizedBox(width: 12),
                Switch(
                  value: !widget.isEnglish,
                  onChanged: (value) {
                    setState(() {
                      isEnglish = !value; // Toggle language
                    });
    
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ListenableProvider(
                            create: (context) => KundliProviderNew(),
                            child: KundliTesting(
                                isEnglish: isEnglish,
                                lat: widget.lat,
                                dateTime: widget.dateTime,
                                long: widget.long))));
                  },
                ),
              ],
            ),
            const SizedBox(width: 16),
          ],
        ),
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.only(bottom: 12),
          child: DefaultTabController(
              length: 6,
              initialIndex: 0,
              child: Column(
                children: [
                  TabBar(
                      labelStyle: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold),
                      labelColor: Colors.black,
                      unselectedLabelColor: Colors.grey,
                      indicatorColor: Colors.black,
                      isScrollable: true,
                      // controller: _tabController!..index = widget.selected_index,
                      tabs: [
                        Tab(text: widget.isEnglish ? "Basic" : "बुनियादी"),
                        Tab(text: widget.isEnglish ? "Charts" : "चार्ट"),
                        Tab(text: widget.isEnglish ? "KP" : "केपी"),
                        Tab(
                            text: widget.isEnglish
                                ? "Ashtakvarga"
                                : "अष्टकवर्ग"),
                        Tab(text: widget.isEnglish ? "Dasha" : "दशा"),
                        Tab(text: widget.isEnglish ? "Report" : "रिपोर्ट"),
                      ]),
                  const SizedBox(height: 16),
                  Expanded(
                    child: TabBarView(
                      children: [
                        KundliBasicTabNew(
                            lang: widget.isEnglish ? 'en' : 'hi',
                            dateTime: dateTime,
                            lat: lat,
                            long: long),
                        ChartTabNew(
                            lang: widget.isEnglish ? 'en' : 'hi',
                            dateTime: dateTime,
                            lat: lat,
                            long: long),
                        KpTabNew(
                            lang: widget.isEnglish ? 'en' : 'hi',
                            dateTime: dateTime,
                            lat: lat,
                            long: long),
                        AstakvargaTabNew(
                            lang: widget.isEnglish ? 'en' : 'hi',
                            dateTime: dateTime,
                            lat: lat,
                            long: long),
                        DashaTabNew(
                            lang: widget.isEnglish ? 'en' : 'hi',
                            dateTime: dateTime,
                            lat: lat,
                            long: long),
                        ReportTabNew(
                            lang: widget.isEnglish ? 'en' : 'hi',
                            dateTime: dateTime,
                            lat: lat,
                            long: long)
                      ],
                    ),
                  )
                ],
              )),
        ));
  }
}
