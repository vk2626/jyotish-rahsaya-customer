import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/kundli_provider_new.dart';
import '../Core/formatter.dart';
import '../Core/logger_helper.dart';
import '../../../utils/custom_circular_progress_indicator.dart';

class ChartTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;
  const ChartTabNew(
      {super.key,
      required this.lat,
      required this.long,
      required this.lang,
      required this.dateTime});

  @override
  State<ChartTabNew> createState() => _ChartTabNewState();
}

class _ChartTabNewState extends State<ChartTabNew> {
  final ScrollController _scrollController = ScrollController();
  String lagnaChartNorthIndian = "";
  String lagnaChartSouthIndian = "";
  String navasmaChartNorthIndian = "";
  String navasmaChartSouthIndian = "";
  bool isPlanetsLoading = true;
  int selectionIndex = 0;
  int selectionIndexSecond = 0;
  List<Map<String, String>> divisionalDataList = [
    // {
    //   "type": "chalit",
    //   "englishName": "Chalit Chart",
    //   "hindiName": "चलित चार्ट",
    //   "svgImageNorth": "",
    //   "svgImageSouth": ""
    // },
    {
      "type": "SUN",
      "englishName": "Sun Chart",
      "hindiName": "सूर्य चार्ट",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "MOON",
      "englishName": "Moon Chart",
      "hindiName": "चंद्र चार्ट",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D1",
      "englishName": "Birth Chart (D-1)",
      "hindiName": "जन्म चार्ट (डी-1)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D2",
      "englishName": "Hora Chart (D-2)",
      "hindiName": "घड़ी चार्ट (डी-2)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D3",
      "englishName": "Dreshkan Chart (D-3)",
      "hindiName": "द्रेष्काण चार्ट (डी-3)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D4",
      "englishName": "Chaturthamsha Chart (D-4)",
      "hindiName": "चतुर्थांश चार्ट (डी-4)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D5",
      "englishName": "Panchmansha Chart (D-5)",
      "hindiName": "पंचमांश चार्ट (डी-5)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D7",
      "englishName": "Saptamansha Chart (D-7)",
      "hindiName": "सप्तमांश चार्ट (डी-7)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D8",
      "englishName": "Ashtamansha Chart (D-8)",
      "hindiName": "अष्टमांश चार्ट (डी-8)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D9",
      "englishName": "Navamansha Chart (D-9)",
      "hindiName": "नवमांश चार्ट (डी-9)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D10",
      "englishName": "Dasamsa (D-10)",
      "hindiName": "दशांश (डी-10)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D12",
      "englishName": "Dwadashamsha Chart (D-12)",
      "hindiName": "द्वादशांश चार्ट (डी-12)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D16",
      "englishName": "Shodashamsha Chart (D-16)",
      "hindiName": "शोडशांश चार्ट (डी-16)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D20",
      "englishName": "Vishamansha Chart (D-20)",
      "hindiName": "विषमांश चार्ट (डी-20)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D24",
      "englishName": "Chaturvimshamsha Chart (D-24)",
      "hindiName": "चतुर्विम्शांश चार्ट (डी-24)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D27",
      "englishName": "Bhamsha Chart (D-27)",
      "hindiName": "भाम्शा चार्ट (डी-27)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D30",
      "englishName": "Trishamansha Chart (D-30)",
      "hindiName": "त्रिशामांश चार्ट (डी-30)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D40",
      "englishName": "Khavedamsha Chart (D-40)",
      "hindiName": "खवेदम्शा चार्ट (डी-40)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D45",
      "englishName": "Akshvedansha Chart (D-45)",
      "hindiName": "अक्षवेदांश चार्ट (डी-45)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    },
    {
      "type": "D60",
      "englishName": "Shashtymsha Chart (D-60)",
      "hindiName": "षष्ठ्यम्शा चार्ट (डी-60)",
      "svgImageNorth": "",
      "svgImageSouth": ""
    }
  ];

  List<Map<String, dynamic>> planetsDetails = [
    {
      "Planet": "Sun",
      "Sign": "Leo",
      "Degree": "15.75",
      "House": "1",
      "Aspect": "Mars",
    },
    {
      "Planet": "Moon",
      "Sign": "Cancer",
      "Degree": "28.45",
      "House": "2",
      "Aspect": "Venus",
    },
    {
      "Planet": "Mars",
      "Sign": "Aries",
      "Degree": "12.12",
      "House": "10",
      "Aspect": "Jupiter",
    },
    {
      "Planet": "Venus",
      "Sign": "Taurus",
      "Degree": "5.23",
      "House": "4",
      "Aspect": "Saturn",
    },
    {
      "Planet": "Jupiter",
      "Sign": "Sagittarius",
      "Degree": "19.98",
      "House": "9",
      "Aspect": "Mercury",
    },
  ];

  @override
  void initState() {
    super.initState();
    getPlanetsDetails();
    getChartLagna(isNorth: true);
    getChartNavasma(isNorth: true);
    getDivisionalCharts(isNorth: true, type: "SUN");
  }

  getPlanetsDetails() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getChartPlanetsDoshaReportDetails(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long)
        .then((value) {
      if (value.isNotEmpty) {
        setState(() {
          isPlanetsLoading = false;
          planetsDetails = value;
        });
      }
    });
  }

  getChartLagna({required bool isNorth}) async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getChartLagna(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long,
            isNorth: isNorth)
        .then((value) {
      TLoggerHelper.debug(isNorth.toString());
      if (value.isNotEmpty) {
        setState(() {
          if (isNorth) {
            lagnaChartNorthIndian = value;
          } else {
            lagnaChartSouthIndian = value;
          }
        });
      }
    });
  }

  getChartNavasma({required bool isNorth}) async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getChartNavasma(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long,
            isNorth: isNorth)
        .then((value) {
      TLoggerHelper.debug(isNorth.toString());
      if (value.isNotEmpty) {
        setState(() {
          if (isNorth) {
            navasmaChartNorthIndian = value;
          } else {
            navasmaChartSouthIndian = value;
          }
        });
      }
    });
  }

  getDivisionalCharts({required String type, required bool isNorth}) async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getChartsDivisional(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long,
            type: type,
            isNorth: isNorth)
        .then((value) {
      TLoggerHelper.debug(isNorth.toString());
      if (value.isNotEmpty) {
        setState(() {
          if (isNorth) {
            divisionalDataList[selectionIndex]['svgImageNorth'] = value;
          } else {
            divisionalDataList[selectionIndex]['svgImageSouth'] = value;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DefaultTabController(
            length: 3, // Lagna and Navasma tabs
            child: Column(
              children: [
                TabBar(
                  tabs: [
                    Tab(
                        child: Text(widget.lang == "en"
                            ? "Lagna" // English Label
                            : "लग्न")), // Hindi Label
                    Tab(
                        child: Text(widget.lang == "en"
                            ? "Navasma" // English Label
                            : "नवमांश")), // Hindi Label
                    Tab(
                        child: Text(widget.lang == "en"
                            ? "Divisional" // English Label
                            : "नवमांश")), // Hindi Label
                  ],
                  labelStyle: GoogleFonts.poppins(fontSize: 16),
                  labelColor: Colors.black,
                  unselectedLabelColor: Colors.grey,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorColor: AppColor.appColor,
                ),
                const SizedBox(height: 12),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .62,
                  child: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      // Lagna Tab Content
                      DefaultTabController(
                        length: 2,
                        child: Column(
                          children: [
                            Container(
                              height: kToolbarHeight - 8.0,
                              child: TabBar(
                                labelStyle: GoogleFonts.poppins(fontSize: 16),
                                labelColor: Colors.black,
                                unselectedLabelColor: Colors.grey,
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicator: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: AppColor.appColor
                                ),
                                onTap: (value) {
                                  if (value == 0) {
                                    if (lagnaChartNorthIndian.isEmpty) {
                                      getChartLagna(isNorth: true);
                                    }
                                  } else if (value == 1) {
                                    if (lagnaChartSouthIndian.isEmpty) {
                                      getChartLagna(isNorth: false);
                                    }
                                  }
                                },
                                dividerColor: Colors.transparent,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18, vertical: 6),
                                tabs: [
                                  Tab(
                                      child: Text(widget.lang == "en"
                                          ? "North Indian" // English Label
                                          : "उत्तर भारतीय")), // Hindi Label
                                  Tab(
                                      child: Text(widget.lang == "en"
                                          ? "South Indian" // English Label
                                          : "दक्षिण भारतीय")), // Hindi Label
                                ],
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .5,
                              child: TabBarView(
                                physics: const NeverScrollableScrollPhysics(),
                                children: [
                                  Center(
                                    child: lagnaChartNorthIndian.isEmpty
                                        ? const CustomCircularProgressIndicator()
                                        : SvgPicture.string(
                                            lagnaChartNorthIndian),
                                  ),
                                  Center(
                                    child: lagnaChartSouthIndian.isEmpty
                                        ? const CustomCircularProgressIndicator()
                                        : SvgPicture.string(
                                            lagnaChartSouthIndian),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      // Navasma Tab Content
                      DefaultTabController(
                        length: 2,
                        child: Column(
                          children: [
                            // Navasma-specific content can be added here
                            Container(
                              height: kToolbarHeight - 8.0,
                              child: TabBar(
                                labelStyle: GoogleFonts.poppins(fontSize: 16),
                                labelColor: Colors.black,
                                unselectedLabelColor: Colors.grey,
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicator: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: AppColor.appColor
                                ),
                                onTap: (value) {
                                  if (value == 0) {
                                    if (navasmaChartNorthIndian.isEmpty) {
                                      getChartNavasma(isNorth: true);
                                    }
                                  } else if (value == 1) {
                                    if (navasmaChartSouthIndian.isEmpty) {
                                      getChartNavasma(isNorth: false);
                                    }
                                  }
                                },
                                dividerColor: Colors.transparent,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18, vertical: 6),
                                tabs: [
                                  Tab(
                                      child: Text(widget.lang == "en"
                                          ? "North Indian" // English Label
                                          : "उत्तर भारतीय")), // Hindi Label
                                  Tab(
                                      child: Text(widget.lang == "en"
                                          ? "South Indian" // English Label
                                          : "दक्षिण भारतीय")), // Hindi Label
                                ],
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .5,
                              child: TabBarView(
                                physics: const NeverScrollableScrollPhysics(),
                                children: [
                                  Center(
                                    child: navasmaChartNorthIndian.isEmpty
                                        ? const CustomCircularProgressIndicator()
                                        : SvgPicture.string(
                                            navasmaChartNorthIndian),
                                  ),
                                  Center(
                                    child: navasmaChartSouthIndian.isEmpty
                                        ? const CustomCircularProgressIndicator()
                                        : SvgPicture.string(
                                            navasmaChartSouthIndian),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      DefaultTabController(
                        length: 2,
                        child: Column(
                          children: [
                            // Navasma-specific content can be added here
                            Container(
                              height: kToolbarHeight - 8.0,
                              child: TabBar(
                                labelStyle: GoogleFonts.poppins(fontSize: 16),
                                labelColor: Colors.black,
                                unselectedLabelColor: Colors.grey,
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicator: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: AppColor.appColor
                                ),
                                onTap: (value) {
                                  setState(() {
                                    selectionIndexSecond = value;
                                  });
                                  if (value == 0) {
                                    if (divisionalDataList[selectionIndex]
                                            ['svgImageNorth']
                                        .toString()
                                        .isEmpty) {
                                      getDivisionalCharts(
                                          type:
                                              divisionalDataList[selectionIndex]
                                                      ['type']
                                                  .toString(),
                                          isNorth: true);
                                    }
                                  } else if (value == 1) {
                                    if (divisionalDataList[selectionIndex]
                                            ['svgImageSouth']
                                        .toString()
                                        .isEmpty) {
                                      getDivisionalCharts(
                                          type:
                                              divisionalDataList[selectionIndex]
                                                      ['type']
                                                  .toString(),
                                          isNorth: false);
                                    }
                                  }
                                },
                                dividerColor: Colors.transparent,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18, vertical: 6),
                                tabs: [
                                  Tab(
                                      child: Text(widget.lang == "en"
                                          ? "North Indian" // English Label
                                          : "उत्तर भारतीय")), // Hindi Label
                                  Tab(
                                      child: Text(widget.lang == "en"
                                          ? "South Indian" // English Label
                                          : "दक्षिण भारतीय")), // Hindi Label
                                ],
                              ),
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                  children: List.generate(
                                divisionalDataList.length,
                                (index) => Container(
                                  margin: const EdgeInsets.only(right: 8.0),
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        selectionIndex = index;
                                        if (selectionIndexSecond == 0) {
                                          getDivisionalCharts(
                                              type: divisionalDataList[index]
                                                      ['type']
                                                  .toString(),
                                              isNorth: true);
                                        } else if (selectionIndexSecond == 1) {
                                          getDivisionalCharts(
                                              type: divisionalDataList[index]
                                                      ['type']
                                                  .toString(),
                                              isNorth: false);
                                        }
                                      });
                                    },
                                    child: Chip(
                                      clipBehavior: Clip.none,
                                      label: Text(
                                          widget.lang == 'en'
                                              ? divisionalDataList[index]
                                                      ['englishName']
                                                  .toString()
                                              : divisionalDataList[index]
                                                      ['hindiName']
                                                  .toString(),
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.w500)),
                                      color: WidgetStatePropertyAll(
                                          index == selectionIndex
                                              ? AppColor.appColor
                                              : Colors.transparent),
                                    ),
                                  ),
                                ),
                              )),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .5,
                              child: TabBarView(
                                physics: const NeverScrollableScrollPhysics(),
                                children: [
                                  Center(
                                    child: divisionalDataList[selectionIndex]
                                                ['svgImageNorth']
                                            .toString()
                                            .isEmpty
                                        ? const CustomCircularProgressIndicator()
                                        : SvgPicture.string(
                                            divisionalDataList[selectionIndex]
                                                    ['svgImageNorth']
                                                .toString()),
                                  ),
                                  Center(
                                    child: divisionalDataList[selectionIndex]
                                                ['svgImageSouth']
                                            .toString()
                                            .isEmpty
                                        ? const CustomCircularProgressIndicator()
                                        : SvgPicture.string(
                                            divisionalDataList[selectionIndex]
                                                    ['svgImageSouth']
                                                .toString()),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Text(
            "Planets",
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 8),
          isPlanetsLoading
              ? SizedBox(
                  height: MediaQuery.of(context).size.height * .5,
                  child: const Center(
                    child: CustomCircularProgressIndicator(),
                  ),
                )
              : Scrollbar(
                  thickness: 8,
                  thumbVisibility: true,
                  controller: _scrollController,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(.2),
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      controller: _scrollController,
                      child: DataTable(
                        columnSpacing: 18,
                        headingRowColor: MaterialStateProperty.all(
                          AppColor.appColor,
                        ),
                        dataTextStyle: GoogleFonts.poppins(
                          fontSize: 14,
                        ),
                        columns: planetsDetails.first.keys
                            .where((key) =>
                                key != 'id' &&
                                key != 'isRetro' &&
                                key != 'nakshatra_pad' &&
                                key != 'is_planet_set')
                            .map((key) {
                          return DataColumn(
                            label: Flexible(
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  TFormatter.convertToReadableFormat(key),
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                        rows: planetsDetails.map((row) {
                          return DataRow(
                            cells: row.keys
                                .where((key) =>
                                    key != 'id' &&
                                    key != 'isRetro' &&
                                    key != 'nakshatra_pad' &&
                                    key != 'is_planet_set')
                                .map((key) {
                              var value = row[key];
                              if (key == 'fullDegree' ||
                                  key == 'normDegree' ||
                                  key == 'speed') {
                                value = (value as num).toStringAsFixed(3);
                              }
                              return DataCell(
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    value.toString(),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              );
                            }).toList(),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    ));
  }
}
