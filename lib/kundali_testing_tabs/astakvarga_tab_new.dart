import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/kundli_provider_new.dart';
import '../Core/formatter.dart';
import '../../../utils/custom_circular_progress_indicator.dart';
import '../Core/logger_helper.dart';

class AstakvargaTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;

  const AstakvargaTabNew(
      {super.key,
      required this.lang,
      required this.lat,
      required this.long,
      required this.dateTime});

  @override
  State<AstakvargaTabNew> createState() => _AstakvargaTabNewState();
}

class _AstakvargaTabNewState extends State<AstakvargaTabNew>
    with AutomaticKeepAliveClientMixin {
  int selectionIndex = 0;
  bool isLoading = true;
  Map<String, dynamic> jsonData = {};
  final List<String> astakvargaPlanetNamesList = [
    "sun",
    "moon",
    "mars",
    "mercury",
    "jupiter",
    "venus",
    "saturn",
    "ascendant"
  ];
  List<int> points = List.generate(12, (index) => 0);
  @override
  void initState() {
    super.initState();
    getAshtakvargaChart(planetName: 'sun');
  }

  getAshtakvargaChart({required String planetName}) async {
    TLoggerHelper.error(planetName + " is the Planet Name");
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getAshtakvargaChart(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            planetName: planetName,
            long: widget.long)
        .then(
      (value) {
        if (value != null) {
          setState(() {
            jsonData = value;
            points.clear(); // Clear the previous points

            // Iterate through each entry in jsonData['ashtak_points']
            jsonData['ashtak_points'].forEach((key, element) {
              // Check if 'total' exists in the current element
              if (element['total'] != null) {
                points.add(int.parse(element['total']
                    .toString())); // Add total points to the list
              }
            });
            isLoading = false;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    String astakvargaDesc = widget.lang == "en"
        ? "Ashtakvarga is used to assess the strength and patterns that are present in a birth chart. The Astakvarga or Ashtakavarga is a numerical quantification or score of each planet placed in the chart with reference to the other 7 planets and the Lagna. In Sarva Ashtaka Varga the total scores of all the BAVs are overlaid and then totalled. This makes the SAV of the chart. The total of all the scores should be 337."
        : "अष्टकवर्ग का उपयोग जन्म कुंडली में मौजूद शक्ति और पैटर्न का आकलन करने के लिए किया जाता है। अष्टकवर्ग या अष्टकवर्ग अन्य 7 ग्रहों और लग्न के संदर्भ में चार्ट में रखे गए प्रत्येक ग्रह का एक संख्यात्मक परिमाण या स्कोर है। सर्व अष्टक वर्ग में सभी बीएवी के कुल अंकों को जोड़ा जाता है और फिर उनका योग किया जाता है। इससे चार्ट का कुल स्कोर 337 होना चाहिए।";

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.lang == "en" ? "Ashtakvarga Chart" : "अष्टकवर्ग चार्ट",
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 12),
          Text(
            astakvargaDesc,
            style: GoogleFonts.poppins(
              fontSize: 15,
            ),
          ),
          const SizedBox(height: 12),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
                children: List.generate(
              astakvargaPlanetNamesList.length,
              (index) => Container(
                margin: const EdgeInsets.only(right: 8.0),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      selectionIndex = index;
                      isLoading = true;
                    });
                    getAshtakvargaChart(
                        planetName: astakvargaPlanetNamesList[selectionIndex]);
                  },
                  child: Chip(
                    label: Text(
                        TFormatter.capitalize(astakvargaPlanetNamesList[index]),
                        style:
                            GoogleFonts.poppins(fontWeight: FontWeight.w500)),
                    color: WidgetStatePropertyAll(index == selectionIndex
                        ? AppColor.appColor
                        : Colors.transparent),
                  ),
                ),
              ),
            )),
          ),
          const SizedBox(height: 20),
          isLoading
              ? SizedBox(
                  height: MediaQuery.of(context).size.height * .4,
                  child: Center(
                    child: CustomCircularProgressIndicator(),
                  ))
              : Expanded(
                  child: Center(
                    child: CustomPaint(
                      size: const Size(300, 300),
                      painter: KundliPainter(coordinatesList: points),
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class KundliPainter extends CustomPainter {
  final List<int> coordinatesList;
  const KundliPainter({required this.coordinatesList});
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 1.5
      ..style = PaintingStyle.stroke;

    double width = size.width;
    double height = size.height;

    // Draw the outer square
    canvas.drawRect(Rect.fromLTWH(0, 0, width, height), paint);

    // Draw the diagonal lines
    canvas.drawLine(const Offset(0, 0), Offset(width, height), paint);
    canvas.drawLine(Offset(width, 0), Offset(0, height), paint);

    // Draw the intermediate lines
    canvas.drawLine(Offset(width / 2, 0), Offset(width, height * 2 / 4), paint);
    canvas.drawLine(Offset(width / 2, 0), Offset(0, height * 2 / 4), paint);
    //...............
    canvas.drawLine(
        Offset(width / 2, height), Offset(width, height * 2 / 4), paint);
    canvas.drawLine(
        Offset(width / 2, height), Offset(0, height * 2 / 4), paint);

    TextPainter textPainter = TextPainter(
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );

    List<Offset> positions = [
      Offset(width / 2, height / 4), //1
      Offset(width / 4, height / 10), //2
      Offset(width / 12, height / 4), //3
      Offset(width / 4, height / 2), //4
      Offset(width / 12, height * (3 / 4)), //5
      Offset(width / 4, height * (3.7 / 4)), //6
      Offset(width / 2, height * (3 / 4)), //7
      Offset(width * (3 / 4), height * (3.7 / 4)), //8
      Offset(width * (3.7 / 4), height * (3 / 4)), //9
      Offset(width * (3 / 4), height / 2), //10
      Offset(width * (3.7 / 4), height / 4), //11
      Offset(width * (3 / 4), height / 10), //12
      //...........Houses.............
      Offset(width / 2, height / 2.5), //13
      Offset(width / 4, height / 5.5), //14
      Offset(width / 6, height / 4), //15
      Offset(width / 2.5, height / 2), //16
      Offset(width / 6, height * (3 / 4)), //17
      Offset(width / 4, height * (3.3 / 4)), //18
      Offset(width / 2, height * (2.5 / 4)), //19
      Offset(width * (3 / 4), height * (3.3 / 4)), //20
      Offset(width * (3.3 / 4), height * (3 / 4)), //21
      Offset(width * (2.5 / 4), height / 2), //22
      Offset(width * (3.3 / 4), height / 4), //23
      Offset(width * (3 / 4), height / 5.5), //24
    ];

    for (int i = 0; i < positions.length; i++) {
      textPainter.text = TextSpan(
        text: i > 11 ? (i - 11).toString() : coordinatesList[i].toString(),
        style: i > 11
            ? GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: AppColor.appColor)
            : GoogleFonts.poppins(
                fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),
      );
      textPainter.layout();
      textPainter.paint(
        canvas,
        positions[i] - Offset(textPainter.width / 2, textPainter.height / 2),
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
