import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../utils/AppColor.dart';
import 'package:provider/provider.dart';

import '../Core/Provider/kundli_provider_new.dart';
import '../Core/formatter.dart';

class KpTabNew extends StatefulWidget {
  final String lat;
  final String long;
  final String lang;
  final DateTime dateTime;

  const KpTabNew(
      {super.key,
      required this.lang,
      required this.lat,
      required this.long,
      required this.dateTime});

  @override
  State<KpTabNew> createState() => _KpTabNewState();
}

class _KpTabNewState extends State<KpTabNew>
    with AutomaticKeepAliveClientMixin {
  final ScrollController _scrollControllerPlanets = ScrollController();
  final ScrollController _scrollControllerCusps = ScrollController();
  List<Map<String, dynamic>> planetsKP = [
    {
      'Planet': 'Mercury',
      'Sign': 'Gemini',
      'House': '3rd',
      'Element': 'Air',
    },
    {
      'Planet': 'Venus',
      'Sign': 'Taurus',
      'House': '2nd',
      'Element': 'Earth',
    },
    {
      'Planet': 'Earth',
      'Sign': 'Virgo',
      'House': '6th',
      'Element': 'Earth',
    },
    {
      'Planet': 'Mars',
      'Sign': 'Aries',
      'House': '1st',
      'Element': 'Fire',
    },
  ];

  List<Map<String, dynamic>> cuspsKP = [
    {
      'Cusp': 'Aries',
      'Degrees': '0° - 30°',
      'Element': 'Fire',
    },
    {
      'Cusp': 'Taurus',
      'Degrees': '30° - 60°',
      'Element': 'Earth',
    },
    {
      'Cusp': 'Gemini',
      'Degrees': '60° - 90°',
      'Element': 'Air',
    },
    {
      'Cusp': 'Cancer',
      'Degrees': '90° - 120°',
      'Element': 'Water',
    },
  ];

  @override
  void initState() {
    super.initState();
    getPlanetsDetails();
    getCuspsDetails();
  }

  getPlanetsDetails() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getKPPlanetsDetails(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long)
        .then((value) {
      if (value != null) {
        setState(() {
          planetsKP = value;
        });
      }
    });
  }

  getCuspsDetails() async {
    await Provider.of<KundliProviderNew>(context, listen: false)
        .getKPPlanetsDetails(
            lang: widget.lang,
            dateTime: widget.dateTime,
            lat: widget.lat,
            long: widget.long)
        .then((value) {
      if (value != null) {
        setState(() {
          cuspsKP = value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18),
        width: MediaQuery.of(context).size.width,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            widget.lang == "en" ? "Planets" : "ग्रह",
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 12),
          Scrollbar(
            thickness: 8,
            thumbVisibility: true,
            controller:
                _scrollControllerPlanets, // Provide the ScrollController here
            child: Container(
              decoration: BoxDecoration(
                  border:
                      Border.all(color: Colors.grey.withOpacity(.2), width: 2),
                  borderRadius: BorderRadius.circular(12)),
              child: SingleChildScrollView(
                controller: _scrollControllerPlanets,
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  columnSpacing: 18,
                  // horizontalMargin: 8,
                  headingTextStyle: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                  dataTextStyle: GoogleFonts.poppins(
                    fontSize: 14,
                  ),
                  headingRowColor: WidgetStatePropertyAll(AppColor.appColor),
                  columns: planetsKP.first.keys.map((key) {
                    return DataColumn(
                      label: Flexible(
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(TFormatter.convertToReadableFormat(key)),
                        ),
                      ),
                    );
                  }).toList(),
                  rows: planetsKP.map((row) {
                    return DataRow(
                      cells: row.keys.map((key) {
                        return DataCell(
                          Container(
                            alignment: Alignment.center,
                            child: Text(row[key]!.toString(),
                                textAlign: TextAlign.center),
                          ),
                        );
                      }).toList(),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          const SizedBox(height: 18),
          Text(
            widget.lang == "en" ? "Cusps" : "कस्प्स",
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 12),
          Scrollbar(
            thickness: 8,
            thumbVisibility: true,
            controller:
                _scrollControllerCusps, // Provide the ScrollController here
            child: Container(
              decoration: BoxDecoration(
                  border:
                      Border.all(color: Colors.grey.withOpacity(.2), width: 2),
                  borderRadius: BorderRadius.circular(12)),
              child: SingleChildScrollView(
                controller: _scrollControllerCusps,
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  columnSpacing: 18,
                  // horizontalMargin: 8,
                  headingTextStyle: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                  dataTextStyle: GoogleFonts.poppins(
                    fontSize: 14,
                  ),
                  headingRowColor: WidgetStatePropertyAll(AppColor.appColor),
                  columns: cuspsKP.first.keys.map((key) {
                    return DataColumn(
                      label: Flexible(
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(TFormatter.convertToReadableFormat(key)),
                        ),
                      ),
                    );
                  }).toList(),
                  rows: cuspsKP.map((row) {
                    return DataRow(
                      cells: row.keys.map((key) {
                        return DataCell(
                          Container(
                            alignment: Alignment.center,
                            child: Text(row[key]!.toString(),
                                textAlign: TextAlign.center),
                          ),
                        );
                      }).toList(),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          const SizedBox(height: 48),
        ]),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
