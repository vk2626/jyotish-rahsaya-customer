import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import '../Core/logger_helper.dart';

class SecureStorage {
  final storage = const FlutterSecureStorage();

  final String _keyProKeralaAPIKey = "proKeralaAPIKey";
  final String _accessTokenKey = "webSocketAccessToken";

  Future setProKeralaAPI(String apiKey) async {
    TLoggerHelper.info("Saving Newer API Key:- $apiKey");
    await storage.write(key: _keyProKeralaAPIKey, value: apiKey);
  }

  Future<String?> getProKeralaAPI() async {
    return await storage.read(key: _keyProKeralaAPIKey);
  }

  Future setAccessToken(String accessToken) async {
    // TLoggerHelper.info(
    //     "Saving Access Token for key $_accessTokenKey:-- $accessToken");
    await storage.write(key: _accessTokenKey, value: accessToken).whenComplete(
        () => TLoggerHelper.info(
            "Access Token Saved Successfully for key $_accessTokenKey:- $accessToken"));
  }

  Future<String> getAccessToken({required String endPointUrl}) async {
    String token = "";
    try {
      // TLoggerHelper.debug(_accessTokenKey + " is the key of Access Token.");
      await storage.read(key: _accessTokenKey).then((value) {
        TLoggerHelper.info(
            value.toString() + " with endpoint Url.. " + endPointUrl);
        if (value != null) {
          token = value;
          // TLoggerHelper.info(token);
        }
      });
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return token;
  }

  // Clear all keys
  Future clearAll() async {
    try {
      TLoggerHelper.info("Deleting all keys from secure storage.");
      await storage.deleteAll();
      TLoggerHelper.info("All keys deleted successfully.");
    } catch (e) {
      TLoggerHelper.error("Error deleting all keys: ${e.toString()}");
    }
  }
}
