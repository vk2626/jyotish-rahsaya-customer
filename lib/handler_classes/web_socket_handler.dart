// import 'dart:async';
// import 'dart:convert';
// import 'package:dio/dio.dart';
// import 'package:jyotish_rahsaya/Core/helper_functions.dart';
// import 'package:jyotish_rahsaya/Core/logger_helper.dart';
// import 'package:jyotish_rahsaya/handler_classes/secure_storage.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';
// import 'package:http/http.dart' as http;

// class WebSocketHandler {
//   final String webSocketURL =
//       "wss://jyotish.techsaga.live:8080/app/abqvak4gc8wme3xjjq0a?protocol=7&client=js&version=8.4.0-rc2&flash=false";
//   late WebSocketChannel _channel;
//   late StreamController<String> _uiResponderController; // Controller for stream
//   SecureStorage _secureStorage = SecureStorage();
//   String socketId = "";
//   Timer? _pingTimer; // Timer for ping messages
//   final Duration pingInterval = Duration(seconds: 30); // Ping interval
//   // Getter for the channel
//   WebSocketChannel get channel => _channel;
//   Stream<String> get uiResponder => _uiResponderController.stream;
//   // Initial function to connect and authenticate
//   Future<void> initializeWebSocket() async {
//     try {
//       _uiResponderController = StreamController<
//           String>.broadcast(); // Initialize the StreamController
//       await getSocketId();
//     } catch (e) {
//       TLoggerHelper.error("Initialization failed: $e");
//     }
//   }

//   // Handles incoming WebSocket messages
//   Future<String?> _handleMessage(String message) async {
//     TLoggerHelper.debug(message);
//     final decodedMessage = jsonDecode(message);
//     if (decodedMessage['event'] == 'pusher:connection_established') {
//       final data = jsonDecode(decodedMessage['data']);
//       return data['socket_id'];
//     }
//     return null;
//   }

//   // Establishes WebSocket connection and listens for messages
//   Future<void> getSocketId() async {
//     _channel = WebSocketChannel.connect(Uri.parse(webSocketURL));

//     _channel.stream.listen((message) async {
//       socketId = await _handleMessage(message) ?? "";
//       if (socketId.isEmpty) {
//         TLoggerHelper.error("Socket ID is empty");
//       } else {
//         TLoggerHelper.info("New Socket ID: $socketId");
//         await _authenticateAndSubscribe(socketId).whenComplete(() {
//           _startPing(); // Start sending ping messages
//         });
//       }
//       _uiResponderController.add(message);
//     }, onError: (error) {
//       TLoggerHelper.error("WebSocket Error: $error");
//     });
//   }

//   // Fetches authentication token using socketId
//   Future<String?> _fetchAuthToken(String socketId) async {
//     try {
//       final endPointUrl =
//           "https://jyotish.techsaga.live/api/broadcasting/web-auth";
//       final userId = await THelperFunctions.getUserId();
//       final Map<String, String> body = {
//         "channel_name": "private-chat.$userId",
//         "socket_id": socketId
//       };
//       final Map<String, dynamic> headers = {
//         "Accept": "application/json",
//         "Authorization":
//             "Bearer " + (await _secureStorage.getAccessToken(endPointUrl: "WebSocket") ?? "")
//       };
//       TLoggerHelper.debug("Request Body: $body and Headers Body is $headers");

//       final dio = Dio();
//       final response = await dio.post(endPointUrl,
//           data: body, options: Options(headers: headers));
//       final data = response.data;

//       TLoggerHelper.debug("Auth Response: $data");
//       return data['auth'];
//     } catch (e) {
//       TLoggerHelper.error("Auth Token Fetch Error: $e");
//       return null;
//     }
//   }

//   // Subscribes to a private channel after getting the auth token
//   Future<void> _authenticateAndSubscribe(String socketId) async {
//     final authToken = await _fetchAuthToken(socketId);
//     if (authToken != null) {
//       final userId = await THelperFunctions.getUserId();
//       final subscribeMessage = jsonEncode({
//         "event": "pusher:subscribe",
//         "data": {"auth": authToken, "channel": "private-chat.$userId"}
//       });

//       _channel.sink.add(subscribeMessage);
//       TLoggerHelper.info("Subscribed to private-chat.$userId");
//     } else {
//       TLoggerHelper.error("Failed to authenticate and subscribe.");
//     }
//   }

//   // Starts sending ping messages periodically
//   void _startPing() {
//     _pingTimer = Timer.periodic(pingInterval, (timer) {
//       if (_channel != null && _channel.sink != null) {
//         TLoggerHelper.debug("Sending ping...");
//         _channel.sink.add(jsonEncode(
//             {"event": "ping"})); // Replace with your ping event/message
//       }
//     });
//   }

//   // Closes WebSocket connection and cancels the ping timer
//   void closeConnection() {
//     _pingTimer?.cancel(); // Cancel the ping timer
//     _channel.sink.close();
//     TLoggerHelper.info("WebSocket connection closed.");
//   }
// }
