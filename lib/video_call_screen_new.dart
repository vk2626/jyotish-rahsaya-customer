// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';

// import 'Core/logger_helper.dart';

// class VideoCallScreenNew extends StatefulWidget {
//   final String sessionId;
//   final String token;
//   final String channelName;
//   final String liveId;

//   const VideoCallScreenNew({
//     Key? key,
//     required this.token,
//     required this.channelName,
//     required this.sessionId,
//     required this.liveId,
//   }) : super(key: key);

//   @override
//   State<VideoCallScreenNew> createState() => _VideoCallScreenNewState();
// }

// class _VideoCallScreenNewState extends State<VideoCallScreenNew> {
//   final String appId = "2b2b29ac0c8b40e88eae4516659054ed";
//   late final String channelName;
//   late final String token;

//   SharedPreferences? prefs;
//   bool isJoined = false;

//   final httpClient = http.Client();

//   int minutesCompleted = 0;
//   bool isAttended = false;
//   int _elapsedSeconds = 0;
//   late Timer _timer;
//   late final AgoraClient client;

//   @override
//   void initState() {
//     super.initState();
//     token =
//         "007eJxTYJjTqpPxRuGS+ubjSkuYY45HGyXUPzM8a/WohnVSrE2/30IFBqMkILRMTDZItkgyMUi1sEhNTDUxNTQzM7U0MDVJTfmYVZbWEMjIcOxWDisjAwSC+EIMZZkpqfm6yYk5ObrJGYl5eak5DAwABpIknQ==";
//     channelName = "video-call-channel";
//     client = AgoraClient(
//         agoraChannelData: AgoraChannelData(
//             channelProfileType:
//                 ChannelProfileType.channelProfileLiveBroadcasting,
//             clientRoleType: ClientRoleType.clientRoleBroadcaster,
//             enableDualStreamMode: true),
//         agoraConnectionData: AgoraConnectionData(
//             appId: "2b2b29ac0c8b40e88eae4516659054ed",
//             channelName: "video-call-channel",
//             tempToken:
//                 "007eJxTYJjTqpPxRuGS+ubjSkuYY45HGyXUPzM8a/WohnVSrE2/30IFBqMkILRMTDZItkgyMUi1sEhNTDUxNTQzM7U0MDVJTfmYVZbWEMjIcOxWDisjAwSC+EIMZZkpqfm6yYk5ObrJGYl5eak5DAwABpIknQ=="),
//         agoraEventHandlers: AgoraRtcEventHandlers(
//             onConnectionStateChanged: (connection, state, reason) {
//           reconnect();
//         }));
//     client.initialize();
//   }

//   void reconnect() {
//     TLoggerHelper.info(
//         "Reconnecting !!!! And Performing Client Initialization!!");
//     setState(() {
//       client.initialize();
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         body: Stack(
//           children: [
//             AgoraVideoViewer(
//               client: client,
//               floatingLayoutContainerHeight: 200,
//               floatingLayoutContainerWidth: 200,
//               enableHostControls: true,
//             ),
//             AgoraVideoButtons(
//               client: client,
//               addScreenSharing: false,
//             ),
//           ],
//         ),
//         floatingActionButton: FloatingActionButton(
//           onPressed: () {
//             reconnect();
//           },
//           child: Icon(Icons.add),
//         ),
//       ),
//     );
//   }
// }
