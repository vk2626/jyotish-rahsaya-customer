class RouteConstants {
  static const String splashScreen = "splash";
  static const String authScreen = "auth";
  static const String otpScreen = "otp";
  static const String signupScreen = "signup";
  static const String mainhomescreen = "home";
  static const String astrologerProfile = "astrologerProfile";
  static const String poojaScreen = "poojaScreen";
  static const String chatScreen = "chatScreen";
  static const String callScreen = "callScreen";
}
