import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import 'package:transparent_image/transparent_image.dart';
import 'Key/api_keys.dart';
import 'langConstant/language.constant.dart';
import '../handler_classes/secure_storage.dart';
import 'package:retry/retry.dart';
import 'package:restart_app/restart_app.dart';
import 'Api/Constants.dart';
import 'Model/wait_list_model.dart';
import 'logger_helper.dart';

import 'package:http/http.dart' as http;

import 'package:shared_preferences/shared_preferences.dart';

class THelperFunctions {
  static const platform =
      MethodChannel('com.jyotishrahsayacustomer.jyotishRahsaya/restartApp');
  static SecureStorage secureStorage = SecureStorage();
  static final FirebaseAnalytics _analytics = FirebaseAnalytics.instance;
  static Future<void> logEvent({required String eventName}) async {
    try {
      await _analytics.logEvent(name: eventName);
      print("Event logged: $eventName");
    } catch (e) {
      print("Error logging event: $e");
    }
  }

  static void showFullImage(BuildContext context, String imageUrl) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.7,
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Colors.black,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: imageUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
        );
      },
    );
  }

  static Future<void> logScreenNameEvent({required String screenName}) async {
    try {
      await _analytics.logEvent(
        name: 'screen_view',
        parameters: {
          'screen_name': screenName.toLowerCase(),
          'screen_class': 'GoRouter',
        },
      );
      print('Screen view logged: $screenName');
    } catch (e) {
      print("Error logging event: $e");
    }
  }

// Log user properties
  static Future<void> setUserProperty({
    required String name,
    required String value,
  }) async {
    await _analytics.setUserProperty(
      name: name,
      value: value,
    );
    print("User property set: $name = $value");
  }

  // Set user ID
  static Future<void> setUserId() async {
    await _analytics.setUserId(id: await THelperFunctions.getUserId());
  }

  static Future<String> httpPostHandler({
    required Client client,
    required String endPointUrl,
    bool isWithToken = true,
    required Map<String, dynamic> body,
  }) async {
    try {
      final Uri uri = Uri.parse(isWithToken
          ? UrlConstants.baseUrl + endPointUrl
          : UrlConstants.baseUrl_login + endPointUrl);

      final headers = {
        'Accept': 'application/json',
        if (isWithToken)
          'Authorization':
              "Bearer ${await SecureStorage().getAccessToken(endPointUrl: endPointUrl)}"
      };

      final r = RetryOptions(maxAttempts: 3);

      // try {
      final http.Response response = await r.retry(
        () async {
          final result = await client.post(uri, body: body, headers: headers);
          if (result.statusCode != 200) {
            throw Exception('Failed to post data: ${result.statusCode}');
          }
          return result;
        },
        retryIf: (e) => e is SocketException || e is ClientException,
      );

      // TLoggerHelper.warning(response.body.toString());
      return response.body;
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return "{}";
    }
  }

  static Future<void> restartApp() async {
    // if(Platform.isAndroid){
    Restart.restartApp();
    // }else if(Platform.isIOS){
// try {
//       await platform.invokeMethod('restartApp');
//     } on PlatformException catch (e) {
//       print("Failed to restart app: ${e.message}");
//     }
//   }
  }

  static void popout(BuildContext context) {
    if (Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
    }
  }

  static String generateWalletTransactionId() {
    const String prefix = "wallet_pay_"; // Prefix for wallet payments
    const String chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    const int idLength = 10; // Length of the random part (adjustable)

    final Random random = Random();
    String randomPart = List.generate(
      idLength,
      (index) => chars[random.nextInt(chars.length)],
    ).join();

    return "$prefix$randomPart";
  }

  static Future<void> clearCallingPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(Constants.isCalling)) {
      prefs.remove(Constants.isCalling);
    }
  }

  static DateTime parseDobAndTime(String dob, String time) {
    try {
      // Trim and normalize input strings
      String trimmedDob = dob.trim();
      // String trimmedTime = time.trim();
      String trimmedTime = time.trim();

      // Convert `time` to uppercase to ensure compatibility with `DateFormat`
      print(trimmedDob + "    " + trimmedTime);
      print(dob + "    " + time);
      String normalizedTime = trimmedTime.toLowerCase();
      print(dob + "    " + normalizedTime);

      // Normalize the `time` to always include leading zeros (hh:mm a)
      try {
        DateFormat inputTimeFormat = DateFormat("h:mm a");
        DateFormat normalizedTimeFormat = DateFormat("hh:mm a");
        normalizedTime =
            normalizedTimeFormat.format(inputTimeFormat.parse(normalizedTime));
      } catch (e) {
        normalizedTime = trimmedTime.toUpperCase();
        DateFormat inputTimeFormat = DateFormat("h:mm a");
        DateFormat normalizedTimeFormat = DateFormat("hh:mm a");
        normalizedTime =
            normalizedTimeFormat.format(inputTimeFormat.parse(normalizedTime));
        TLoggerHelper.error(e.toString());
      }

      // Combine `dob` and normalized `time` into a single string
      String combined = "$trimmedDob $normalizedTime";

      // Debug log the combined string
      print("Parsing DateTime from: '$combined'");

      TLoggerHelper.info(time + "     " + trimmedTime + "    " + combined);
      // Define the date-time format matching the input
      DateFormat format = DateFormat("yyyy-MM-dd hh:mm a");
      // Parse the combined string into a DateTime object
      return format
          .parseStrict(combined); // Use `parseStrict` for stricter checks
    } catch (e) {
      // Log the error and provide detailed feedback
      print("Error parsing DateTime: $e");
      throw Exception("Invalid date or time format: dob='$dob', time='$time'");
    }
  }

  static String formatDateTimeProKerala(String dob, String tob) {
    // Split the DOB and parse components
    List<String> dobParts = dob.split("/");
    String day = dobParts[0];
    String month = dobParts[1];
    String year = dobParts[2];

    // Split the TOB and parse components
    List<String> tobParts = tob.split(":");
    String hour = tobParts[0];
    String minute = tobParts[1];

    // Format to the desired output
    return "$year-$month-$day" "T$hour:$minute:00";
  }

  static Future<String> getLocalizationCode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(LAGUAGE_CODE) ?? "en";
  }

  static String formatDate(DateTime date) {
    return "${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
  }

  static Future<void> showTyping(
      {required String astrologerId, required bool status}) async {
    var endPointUrl = UrlConstants.showTyping;
    final Map<String, String> body = {
      "customer_id": await THelperFunctions.getUserId(),
      "astrologer_id": astrologerId,
      "status": status.toString(),
      "user_type": "customer"
    };

    await THelperFunctions.httpPostHandler(
        client: http.Client(), endPointUrl: endPointUrl, body: body);
  }

  // static Future<void> showTypingNew(
  //     {required String astrologerId,
  //     required bool status,
  //     required String channelName}) async {
  //   var endPointUrl = "typing-message";
  //   final Map<String, String> body = {
  //     "customer_id": await THelperFunctions.getUserId(),
  //     "astrologer_id": astrologerId,
  //     "status": status.toString(),
  //     "user_type": "customer",
  //     "eventName": "typing",
  //     "channelName": channelName
  //   };

  //   await THelperFunctions.httpPostHandler(
  //       client: http.Client(), endPointUrl: endPointUrl, body: body);
  // }

  static Future<String> httpGetHandler({
    required http.Client client,
    required String endPointUrl,
    Map<String, dynamic>? queryParams,
  }) async {
    Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    if (queryParams != null) {
      uri = uri.replace(queryParameters: queryParams);
    }

    final headers = {
      "Accept": "application/json",
      'Authorization':
          "Bearer ${await SecureStorage().getAccessToken(endPointUrl: endPointUrl)}"
    };

    final r = RetryOptions(maxAttempts: 3);

    try {
      final http.Response response = await r.retry(
        () async {
          final result = await client.get(uri, headers: headers);
          if (result.statusCode != 200) {
            throw Exception('Failed to get data: ${result.statusCode}');
          }
          return result;
        },
        retryIf: (e) => e is SocketException || e is ClientException,
      );

      TLoggerHelper.info(response.body + "  " + endPointUrl);
      return response.body;
    } catch (e) {
      // Handle the error appropriately
      throw Exception('Request failed after retries: $e');
    }
  }

  static Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int? userId = prefs.getInt(Constants.userID); // Retrieve the user ID

    print("userid-->${userId.toString() == "null" ? "0" : userId.toString()}");
    // return userId?.toString() ?? "";
    return userId == null ? "0" : userId.toString();
  }

  static Future<String> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(Constants.fullName).toString() ?? "";
  }

  static Future<String> getUserAvatar() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(Constants.profileImage).toString() ?? "";
  }

  static Future<void> sendLiveOnlineStatus(
      {required bool status,
      required String astrologerId,
      required String liveId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int customerId = prefs.getInt(Constants.userID) ?? 0;

    var endPointUrl = UrlConstants.sendCustomerOnlineOffline;
    String? deviceToken = await FirebaseMessaging.instance.getToken();

    final Map<String, String> body = {
      "status": status ? 1.toString() : 0.toString(),
      "astrologer_id": astrologerId,
      "live_id": liveId,
      "device_token": deviceToken ?? ""
    };
    if (customerId != 0) {
      body['customer_id'] = customerId.toString();
    }
    TLoggerHelper.info(body.entries.toString());
    final dio = Dio();
    String? accessToken =
        await secureStorage.getAccessToken(endPointUrl: endPointUrl);

    Map<String, String> headers = <String, String>{
      'Accept': 'application/json',
      'Authorization':
          'Bearer ${accessToken ?? ""}', // Add Authorization header
    };

    dio.options.headers = headers;
    await dio.post("${UrlConstants.baseUrl}$endPointUrl", data: body).then(
        (val) => TLoggerHelper.info(
            "${status ? "Connected" : "DisConnected"} Successfully!!"));
  }

  // static Future<WaitListModel?> checkWaitList() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   int customerId = prefs.getInt(Constants.userID) ?? 0;
  //   if (customerId == 0) {
  //     return null;
  //   }
  //   var endPointUrl = "wait-chat-list-new";
  //   final Map<String, String> body = {'user_id': customerId.toString()};

  //   // TLoggerHelper.info("Request Body: ${body.entries.toString()}");

  //   try {
  //     final dio = Dio();
  //     String? accessToken =
  //         await secureStorage.getAccessToken(endPointUrl: endPointUrl);

  //     Map<String, String> headers = <String, String>{
  //       'Accept': 'application/json',
  //       'Authorization':
  //           'Bearer ${accessToken ?? ""}', // Add Authorization header
  //     };

  //     dio.options.headers = headers;
  //     // TLoggerHelper.info((await secureStorage.getAccessToken(endPointUrl: endPointUrl) ?? ""));
  //     final response =
  //         await dio.post("${Constants.baseUrl}$endPointUrl", data: body);
  //     // TLoggerHelper.debug(response.toString() + " $endPointUrl");
  //     if (response.statusCode == 200) {
  //       // Since response.data is already a Map, no need for jsonDecode
  //       Map<String, dynamic> parseData = response.data;
  //       return WaitListModel.fromJson(parseData);
  //     } else {
  //       TLoggerHelper.error(
  //           "Error: ${response.statusCode} - ${response.statusMessage}");
  //       return null;
  //     }
  //   } catch (e) {
  //     // TLoggerHelper.error("Exception: $e");
  //     return null;
  //   }
  // }
  static Future<WaitListModel?> checkWaitList() async {
    var endPointUrl = UrlConstants.waitChatList;
    String userId = await THelperFunctions.getUserId();
    print(userId);
    if (userId == "0") return null;
    final Map<String, String> body = {"user_id": userId};

    final data = await THelperFunctions.httpPostHandler(
        client: http.Client(), endPointUrl: endPointUrl, body: body);
    final Map<String, dynamic> parseData = await jsonDecode(data);
    if (parseData['status']) {
      return WaitListModel.fromJson(parseData);
    } else {
      if (parseData['message'] == "Unauthenticated") {
        TLoggerHelper.warning(parseData.entries.toString());
        Fluttertoast.showToast(msg: "Logged in on another device.");
        final prefs = await SharedPreferences.getInstance();
        await prefs.clear();
        await SecureStorage().clearAll();
        await Restart.restartApp(); // Restart the app
      }
      return null;
    }
  }

  static Future<num> getWalletAmount() async {
    try {
      // Initialize Dio
      final dio = Dio();
      String? accessToken =
          await secureStorage.getAccessToken(endPointUrl: UrlConstants.wallet);

      Map<String, String> headers = <String, String>{
        'Accept': 'application/json',
        'Authorization':
            'Bearer ${accessToken ?? ""}', // Add Authorization header
      };

      dio.options.headers = headers;
      // Get SharedPreferences instance
      SharedPreferences prefs = await SharedPreferences.getInstance();

      // Retrieve user ID from SharedPreferences
      int customerId = prefs.getInt(Constants.userID) ?? 0;

      // Endpoint URL
      var endPointUrl = UrlConstants.walletAmountTrasaction;

      // Prepare request body
      final Map<String, String> body = {"user_id": customerId.toString()};

      // Send POST request
      var response =
          await dio.post("${UrlConstants.baseUrl}$endPointUrl", data: body);

      // Decode response data
      final Map<String, dynamic> parseData =
          response.data; // Use response.data directly

      // Return wallet balance
      return parseData['walletBalance'] ??
          0.0; // Provide a default value if walletBalance is missing
    } catch (e) {
      // Handle error
      print("Error occurred: $e");
      return 0; // Return 0 or handle error as needed
    }
  }

  static Future<String?> httpGetHandlerNormal(
      {required http.Client client,
      required String endPointUrl,
      Map<String, dynamic>? queryParams,
      bool isUtf8 = false}) async {
    try {
      Uri uri = Uri.parse(endPointUrl);

      final http.Response response = await client.get(uri, headers: {
        "Accept": "application/json",
        "Authorization":
            (await secureStorage.getAccessToken(endPointUrl: endPointUrl) ?? "")
      });
      TLoggerHelper.info(response.body.toString());
      return isUtf8 ? utf8.decode(response.bodyBytes) : response.body;
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  List<String> getLastKeys(List<Map<String, dynamic>> mapList) {
    return mapList.map<String>((map) {
      final keys = map.keys.toList();
      return keys.isNotEmpty
          ? keys.last
          : 'No keys'; // Handle empty map scenario
    }).toList();
  }

  static Future<String?> httpGetHandlerProKeralaNormal(
      {required http.Client client,
      required String endPointUrl,
      Map<String, dynamic>? queryParams,
      bool isUtf8 = false}) async {
    try {
      Uri uri = Uri.parse(endPointUrl);

      // If you have query parameters, add them to the Uri
      if (queryParams != null) {
        uri = uri.replace(queryParameters: queryParams);
      }

      final http.Response response = await client.get(
        uri,
        headers: {
          "Accept": "application/json",
          "Authorization": (await secureStorage.getProKeralaAPI() ?? "")
        },
      );
// Check if the status code is 200
      TLoggerHelper.debug(response.body);
      if (response.statusCode == 200) {
        return isUtf8 ? utf8.decode(response.bodyBytes) : response.body;
      } else if (response.statusCode == 401 || response.statusCode == 403) {
        // If not 200, hit the token API
        String? accessToken = await _fetchAccessToken(client);
        if (accessToken != null) {
          // Retry the original API call with the new access token
          final retryResponse = await client.get(
            uri,
            headers: {
              "Accept": "application/json",
              "Authorization": "Bearer $accessToken",
            },
          );
          return isUtf8
              ? utf8.decode(retryResponse.bodyBytes)
              : retryResponse.body;
        }
      } else {
        TLoggerHelper.error("Some error status code ${response.statusCode}");
      }
      return response.body;
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  static Future<String?> _fetchAccessToken(http.Client client) async {
    try {
      final response = await client.post(
        Uri.parse('https://api.prokerala.com/token'),
        body: {
          'client_id': TAPIKeys.clientID,
          'client_secret': TAPIKeys.clientSecret,
          'grant_type': 'client_credentials',
        },
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> jsonResponse = json.decode(response.body);
        await secureStorage
            .setProKeralaAPI("Bearer " + jsonResponse['access_token']);
        return jsonResponse['access_token'];
      } else {
        TLoggerHelper.error("Failed to fetch access token: ${response.body}");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  // static String getZodiacImage({required String zodiac}) {
  //   return Constants.zodiacSignsList.entries
  //       .firstWhere((element) => element.key == zodiac.toLowerCase())
  //       .value;
  // }

  // static Future<void> checkAuthUser(
  //     {required String authorization, required Client client}) async {
  //   String userId = await THelperFunctions.getUserId();
  //   print(userId);
  //   if (userId == "0") return null;
  //   final Uri uri = Uri.parse(Constants.baseUrl + "token-check");
  //   final headers = {
  //     'Accept': 'application/json',
  //     'Content-Type': 'application/json',
  //     'Authorization':
  //         "Bearer ${await SecureStorage().getAccessToken(endPointUrl: "token-check")}"
  //   };

  //   final body = jsonEncode({}); // Empty body

  //   try {
  //     final http.Response response =
  //         await http.post(uri, headers: headers, body: body);

  //     if (response.statusCode == 200) {
  //       final Map<String, dynamic> parseData = jsonDecode(response.body);

  //       if (!parseData['status']) {
  //         if (parseData['message'] == "Unauthenticated") {
  //           // You can clear prefs or perform other tasks if needed

  //           // TLoggerHelper.warning(uri.path.toString());
  //           // TLoggerHelper.warning(parseData.entries.toString());
  //           // Fluttertoast.showToast(msg: "Logged in on another device.");
  //           // final prefs = await SharedPreferences.getInstance();
  //           // await prefs.clear();
  //           // await SecureStorage().clearAll();
  //           // Restart.restartApp(); // Restart the app
  //         }
  //       }
  //     } else {
  //       // Handle error response (non-200)
  //       Fluttertoast.showToast(msg: "Error: ${response.statusCode}");
  //     }
  //   } catch (e) {
  //     // Handle exception
  //     Fluttertoast.showToast(msg: "Something went wrong: $e");
  //   }
  // }
}
