import 'dart:convert';
import 'dart:math';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';

class NotificationHandler {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static const String channelId = "normal_channel";
  static const String channelName = "Normal Channel";
  static const String channelDescription = "Receiving All Normal Notifications";

// Define Android notification details
  final AndroidNotificationDetails _androidNotificationDetails =
      AndroidNotificationDetails(
    channelId,
    channelName,
    channelDescription: channelDescription,
    importance: Importance.max,
    priority: Priority.high,
    playSound: true,
    icon: '@mipmap/ic_launcher',
    enableVibration: true,
  );

  // Create a notification channel for Android
  final AndroidNotificationChannel androidNotificationChannel =
      const AndroidNotificationChannel(
    channelId,
    channelName,
    description: channelDescription,
    importance: Importance.max,
    playSound: true,
    enableVibration: true,
  );
  // Initialize notifications
  Future<void> initializeNotification() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    const InitializationSettings initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);

    try {
      // Create the notification channel
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(androidNotificationChannel);

      // Initialize the notification plugin
      await flutterLocalNotificationsPlugin.initialize(initializationSettings);

      print("Notification Initialized Successfully!");
    } catch (e) {
      print("Error initializing notifications: $e");
    }
  }

  // Show a normal notification
  Future<void> showNormalNotification(
      {required String title, required String desc}) async {
    int randomId = Random().nextInt(1000);
    await flutterLocalNotificationsPlugin.show(
      randomId,
      title,
      desc,
      NotificationDetails(android: _androidNotificationDetails),
    );
  }


}
