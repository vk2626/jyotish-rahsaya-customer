import 'dart:convert';

import 'package:flutter_callkit_incoming/entities/entities.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import '../formatter.dart';
import 'package:uuid/uuid.dart';

Future<void> showIncomingCallNotification(
    {required Map<String, dynamic> notificationInfo,
    int duration = 30000}) async {
  await FlutterCallkitIncoming.endAllCalls();

  String messageJson = jsonEncode(notificationInfo.toString());
  print("##############\n\nFull message as CallKit JSON: $messageJson");

  final params = CallKitParams(
      id: Uuid().v4(),
      nameCaller: TFormatter.capitalizeSentence(notificationInfo['callerName']),
      appName: 'Jyotish Rahsaya',
      avatar: notificationInfo['callerAvatar'],
      type: 0,
      duration: duration,
      missedCallNotification: const NotificationParams(
        showNotification: false,
        isShowCallback: false,
      ),
      textAccept: 'Accept',
      textDecline: 'Decline',
      extra: notificationInfo,
      android: AndroidParams(
        isCustomNotification: true,
        isShowLogo: false,
        ringtonePath: 'system_ringtone_default',
        backgroundColor: '#0955fa',
        actionColor: '#4CAF50',
      ),
      ios: IOSParams(
        iconName: 'AppIcon',
        handleType: 'generic',
        supportsVideo: true,
        maximumCallGroups: 2,
        maximumCallsPerCallGroup: 1,
        audioSessionMode: 'default',
        audioSessionActive: true,
        audioSessionPreferredSampleRate: 44100.0,
        audioSessionPreferredIOBufferDuration: 0.005,
        supportsDTMF: true,
        supportsHolding: true,
        supportsGrouping: false,
        supportsUngrouping: false,
        ringtonePath: 'system_ringtone_default',
      ));

  await FlutterCallkitIncoming.showCallkitIncoming(params);
}
