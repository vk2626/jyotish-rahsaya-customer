// import 'dart:async';
// import 'dart:isolate';
// import 'dart:math';
// import 'dart:ui';

// import 'package:awesome_notifications/awesome_notifications.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// import 'package:jyotish_rahsaya/Screens/IncomingCallRequest.dart';
// import 'package:jyotish_rahsaya/main.dart';
// import 'package:permission_handler/permission_handler.dart';

// import '../../Screens/IncomingChatRequest.dart';
// import '../logger_helper.dart';

// class Notify {
//   final BuildContext context;

//   const Notify({required this.context});
//   static bool _initialized = false;
//   Future<void> initialize() async {
//     // Initialize AwesomeNotifications
//     AwesomeNotifications().initialize(
//       "resource://mipmap/launcher_icon",
//       [
//         NotificationChannel(
//           channelKey: 'call_channel',
//           channelName: 'Call Channel',
//           channelDescription: 'Notification channel for Calling',
//           defaultColor: Colors.amber,
//           ledColor: Colors.white,
//           defaultRingtoneType: DefaultRingtoneType.Ringtone,
//           criticalAlerts: true,
//           enableLights: true,
//           importance: NotificationImportance.Max,
//           enableVibration: true,
//           locked: true,
//           channelShowBadge: true,
//           playSound: true,
//         ),
//         NotificationChannel(
//           channelKey: 'chat_channel',
//           channelName: 'Chat Channel',
//           channelDescription: 'Notification channel for Chatting',
//           defaultRingtoneType: DefaultRingtoneType.Notification,
//           importance: NotificationImportance.Low,
//         ),
//       ],
//       debug: true,
//     );

//     // Request notification permissions
//     await AwesomeNotifications().isNotificationAllowed().then(
//       (isAllowed) async {
//         if (!isAllowed) {
//           await AwesomeNotifications().requestPermissionToSendNotifications();
//         }
//       },
//     );

//     // Request notification permission
//     var status = await Permission.notification.request();
//     if (status.isGranted) {
//       print("Permission granted for floating notifications!");
//     } else {
//       print("Permission denied for floating notifications!");
//     }
    // await FirebaseMessaging.instance
    //     .getToken()
    //     .then((value) => TLoggerHelper.info("FCM Token:- ${value!}"));
//     // Set notification listeners
//     await AwesomeNotifications()
//         .setListeners(
//             onActionReceivedMethod: onActionReceivedImplementationMethod,
//             onDismissActionReceivedMethod: onDismissActionReceivedMethod,
//             onNotificationCreatedMethod: onNotificationCreatedMethod,
//             onNotificationDisplayedMethod: onNotificationDisplayedMethod)
//         .then((value) => TLoggerHelper.debug(value.toString()));

//     await FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
//       TLoggerHelper.debug(message.data.entries.toString());
//       await _handleFirebaseMessage(message);
//     });

//     await FirebaseMessaging.onMessageOpenedApp
//         .listen((RemoteMessage message) async {
//       TLoggerHelper.debug(message.data.entries.toString());
//       await _handleFirebaseMessage(message);
//     });

//     // Register the receive port with a unique name

//     // await FirebaseMessaging.instance
//     //     .getInitialMessage()
//     //     .then((RemoteMessage? message) async {
//     //   if (message != null) {
//     //     TLoggerHelper.debug(message.data.entries.toString());
//     //     await _handleFirebaseMessage(message);
//     //   }
//     // });
//   }

//   static ReceivePort? receivePort;
//   static Future<void> initializeIsolateReceivePort() async {
//     // This initialization only happens on main isolate
//     IsolateNameServer.registerPortWithName(
//         receivePort!.sendPort, 'call_channel');
//     // Listen for messages on the receive port
//     receivePort!.listen((var serializedData) async {
//       TLoggerHelper.debug('Action running on main isolate');
//       final receivedAction = ReceivedAction().fromMap(serializedData);
//       onActionReceivedImplementationMethod(receivedAction);
//     });

//     // Set the initialization flag
//     _initialized = true;
//   }

//   static Future<void> onNotificationCreatedMethod(
//       ReceivedNotification receivedNotification) async {
//     debugPrint('onNotificationCreatedMethod');
//   }

//   /// Use this method to detect every time that a new notification is displayed
//   static Future<void> onNotificationDisplayedMethod(
//       ReceivedNotification receivedNotification) async {
//     debugPrint('onNotificationDisplayedMethod');
//   }

//   /// Use this method to detect if the user dismissed a notification
//   static Future<void> onDismissActionReceivedMethod(
//       ReceivedAction receivedAction) async {
//     debugPrint('onDismissActionReceivedMethod');
//   }

//   static Future<void> firebaseMessagingBackgroundHandler(
//       RemoteMessage message) async {
//     TLoggerHelper.debug(message.data.entries.toString());
//     if (message.data['type'].toString() == 'Call') {
//       _createCallNotification(
//           token: message.data['token'],
//           channelName: message.data['channel'],
//           imageUrl: message.data['image'],
//           timeDuration: message.data['duration'],
//           userName: message.data['name']);
//     } else if (message.data['type'].toString() == 'Accept') {
//       Notify._createTextNotification(
//           title: message.notification!.title!,
//           body: message.notification!.body!);
//     }
//   }

//   Future<void> _handleFirebaseMessage(RemoteMessage message) async {
//     TLoggerHelper.debug(message.data.entries.toString());
//     debugPrint(message.data.entries.toString());
//     if (message.data['type'].toString() == "Call") {
//       _createCallNotification(
//           token: message.data['token'],
//           channelName: message.data['channel'],
//           imageUrl: message.data['image'],
//           timeDuration: message.data['duration'],
//           userName: message.data['name']);
//     } else if (message.data['type'].toString() == 'Accept') {
//       if (message.notification != null) {
//         _createTextNotification(
//           title: message.notification!.title!,
//           body: message.notification!.body!,
//         );
//         // Navigator.pop(context);
//         Navigator.of(context).push(
//           MaterialPageRoute(
//             builder: (_) => IncomingChatRequest(
//               message.data["communicationId"],
//               message.data["image"],
//               message.data["name"],
//               message.data["astrologer_id"],
//             ),
//           ),
//         );
//       } else {
//         TLoggerHelper.debug("It is the last else....");
//         _createTextNotification(
//           title: message.notification!.title!,
//           body: message.notification!.body!,
//         );
//         Navigator.of(context).push(
//           MaterialPageRoute(
//             builder: (_) => IncomingChatRequest(
//               message.data["communicationId"],
//               message.data["image"],
//               message.data["name"],
//               message.data["astrologer_id"],
//             ),
//           ),
//         );
//       }
//     }
//   }

//   static Future<void> onActionReceivedImplementationMethod(
//       ReceivedAction receivedAction) async {
//     TLoggerHelper.debug('New action received: ${receivedAction.toMap()}');

//     // If the controller was not initialized or the function is not running in the main isolate,
//     // try to retrieve the ReceivePort at main isolate and them send the execution to it
//     if (!_initialized) {
//       SendPort? uiSendPort = IsolateNameServer.lookupPortByName('call_channel');
//       if (uiSendPort != null) {
//         TLoggerHelper.debug(
//             'Background action running on parallel isolate without valid context. Redirecting execution');
//         uiSendPort.send(receivedAction.toMap());
//         return;
//       }
//     }
//     if (receivedAction.buttonKeyPressed == "attend" ||
//         receivedAction.buttonKeyInput == "attend") {
//       try {
//         TLoggerHelper.debug(receivedAction.payload!.entries.toString());
//       } catch (e) {
//         TLoggerHelper.error(e.toString());
//       }
//       await AwesomeNotifications()
//           .getInitialNotificationAction(removeFromActionEvents: false);
//       MyApp.navigatorKey.currentState!.push(MaterialPageRoute(
//           builder: (context) => IncomingCallRequest(
//                 token: receivedAction.payload!['token'].toString(),
//                 channelName: receivedAction.payload!['channelName'].toString(),
//                 imageUrl: receivedAction.payload!['image'].toString(),
//                 timerDuration: Duration(
//                     minutes: int.parse(
//                         receivedAction.payload!['timerDuration'].toString())),
//                 userName: receivedAction.payload!['userName'].toString(),
//               )));
//       AwesomeNotifications().dismissAllNotifications();
//     } else if (receivedAction.buttonKeyPressed == "reject" ||
//         receivedAction.buttonKeyInput == "reject") {
//       AwesomeNotifications().dismissAllNotifications();
//     }
//   }

//   static void _createCallNotification(
//       {required String token,
//       required String channelName,
//       required String imageUrl,
//       required String timeDuration,
//       required String userName}) {
//     AwesomeNotifications().createNotification(
//       content: NotificationContent(
//         id: Random().nextInt(100),
//         channelKey: "call_channel",
//         fullScreenIntent: true,
//         category: NotificationCategory.Call,
//         title: "Instant Notification",
//         body: "Notification that delivers instantly on trigger..",
//         actionType: ActionType.SilentBackgroundAction,
//         autoDismissible: false,
//         displayOnBackground: true,
//         displayOnForeground: true,
//         criticalAlert: true,
//         locked: true,
//         wakeUpScreen: true,
//         payload: {
//           'token': token,
//           'channelName': channelName,
//           'imageUrl': imageUrl,
//           'timerDuration': timeDuration,
//           'userName': userName
//         },
//       ),
//       actionButtons: <NotificationActionButton>[
//         NotificationActionButton(
//           key: 'reject',
//           label: 'Reject',
//           color: Colors.red,
//           actionType: ActionType.KeepOnTop,
//           autoDismissible: false,
//         ),
//         NotificationActionButton(
//           key: 'attend',
//           label: 'Attend',
//           color: Colors.green,
//           actionType: ActionType.KeepOnTop,
//           autoDismissible: false,
//         ),
//       ],
//     );
//   }

//   static void _createTextNotification({
//     required String title,
//     required String body,
//   }) {
//     AwesomeNotifications().createNotification(
//       content: NotificationContent(
//         id: Random().nextInt(100),
//         channelKey: "chat_channel",
//         title: title,
//         body: body,
//         autoDismissible: true,
//       ),
//     );
//   }
// }
