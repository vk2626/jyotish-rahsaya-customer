// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:jyotish_rahsaya/Screens/IncomingCallRequest.dart';
// import 'package:jyotish_rahsaya/main.dart';

// class MethodChannelHandler {
//   static const MethodChannel _channel = MethodChannel('YOUR_CHANNEL_NAME');

//   static void initialize() {
//     _channel.setMethodCallHandler((MethodCall call) async {
//       switch (call.method) {
//         case 'CALL_ACCEPTED_INTENT':
//           handleCallAccepted(call.arguments);
//           break;
//         default:
//           throw MissingPluginException('notImplemented');
//       }
//     });
//   }

//   static void handleCallAccepted(dynamic arguments) {
//     final String userName = arguments['name'];
//     final String imageUrl = arguments['image'];
//     final int duration = int.parse(arguments['duration']);
//     final String channelName = arguments['channel'];
//     final String token = arguments['token'];

//     if (MyApp.navigatorKey.currentContext != null) {
//       Navigator.push(
//         MyApp.navigatorKey.currentContext!,
//         MaterialPageRoute(
//           builder: (context) => IncomingCallRequest(
//             userName: userName,
//             imageUrl: imageUrl,
//             timerDuration: Duration(minutes: duration),
//             channelName: channelName,
//             token: token,
//           ),
//         ),
//       );
//     }
//   }

//   // static void handleChatAccepted(dynamic arguments) {
//   //   final String communicationId = arguments['communicationId'];
//   //   final String imageUrl = arguments['image'];
//   //   final String userName = arguments['name'];
//   //   final String astrologerId = arguments['astrologer_id'];
//   //
//   //   if (MyApp.navigatorKey.currentContext != null) {
//   //     Navigator.push(
//   //       MyApp.navigatorKey.currentContext!,
//   //       MaterialPageRoute(
//   //         builder: (context) => IncomingChatRequest(
//   //           communicationId,
//   //           imageUrl,
//   //           userName,
//   //           astrologerId,
//   //         ),
//   //       ),
//   //     );
//   //   }
//   // }
// }
