import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_background/flutter_background.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:go_router/go_router.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../../Screens/IncomingCallRequest.dart';
import '../../Screens/liveScreen/live_reel/agora_live_screen.dart';
import '../../firebase_options.dart';
import '../../router_constants.dart';
import '../helper_functions.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Api/Constants.dart';
import 'package:permission_handler/permission_handler.dart';
import '../logger_helper.dart';
import 'calling_service.dart';

class NotificationHandlerAndroid {
  static Future<void> handleFirebaseMessage(
      BuildContext context, RemoteMessage message,
      {VoidCallback? callbackFunction}) async {
    // Convert the entire message to a JSON string and print it
    String messageJson = jsonEncode(message.toMap());
    print("Full message as JSON: $messageJson");

    // Existing logging and handling
    TLoggerHelper.info("Message data entries: ${message.data.entries}");
    if (message.data['type'].toString().toLowerCase() == "call") {
      final Map<String, String> notificationInfo = {
        "callerId": message.data['token'],
        "callerName": message.data['name'],
        "astrologerId": message.data['astrologerId'],
        "callerAvatar": message.data['image'],
        "channel": message.data['channel'],
        "duration": message.data['duration'],
        "token": message.data['token'],
        "communicationId": message.data['communicationId'],
        "type": message.data['type'].toString().toLowerCase()
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toString().toLowerCase() == 'accept') {
      final Map<String, dynamic> messageInfo = jsonDecode(message.data['info']);
      final Map<String, dynamic> notificationInfo = {
        'type': 'chat',
        "callerId": message.data["orderId"],
        "callerName": message.data["astrologerName"],
        "callerAvatar": message.data["image"],
        "communicationId": message.data["communicationId"],
        "image": message.data["image"],
        "name": message.data["astrologerName"],
        "astrologerId": message.data["astrologer_id"],
        "user_id": message.data["user_id"],
        "chat_price": message.data["chat_price"],
        "orderId": message.data["orderId"],
        "infoId": message.data["info_id"],
        "is_freechat": message.data["is_freechat"],
        "durationTime": message.data["duration_time"],
        "message_info": messageInfo
      };

      // TLoggerHelper.warning(message.data["astrologerName"].toString());
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toLowerCase().toString() ==
        "Video".toLowerCase()) {
      TLoggerHelper.info(message.data.entries.toString());
      // } else if (message.data['type'].toLowerCase().toString() == "chat-end") {
      //   TLoggerHelper.info("Listening to Chat end notification !!");

      //   context.goNamed(RouteConstants.mainhomescreen);
    } else if (message.data['type'].toLowerCase().toString() == "call-end") {
      TLoggerHelper.info("Listening to Call end notification !!");
      if (FlutterBackground.isBackgroundExecutionEnabled) {
        await FlutterBackground.disableBackgroundExecution();
      }
      context.goNamed(RouteConstants.mainhomescreen);
      IncomingCallRequest.agoraEngine.leaveChannel();
      IncomingCallRequest.agoraEngine.release();
    } else if (message.data['type'].toString().toLowerCase() == "pooja-video") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-video",
        "callerId": message.data["order_pooja_id"],
        "callerName": message.data["name"],
        "callerAvatar": message.data["image"],
        "order_pooja_id": message.data['order_pooja_id'].toString(),
        "astrologerId": message.data['astrologerId'].toString(),
        "customerId": message.data['customerId'].toString(),
        "image": message.data['image'].toString(),
        "pooja_id": message.data['pooja_id'].toString(),
        "customerName": message.data['name'].toString(),
        "name": message.data['name'].toString(),
        "token": message.data['token'].toString(),
        "channel": message.data['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toString().toLowerCase() == "pooja-call") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-call",
        "callerId": message.data["order_pooja_id"],
        "callerName": message.data["name"],
        "callerAvatar": message.data["image"],
        "order_pooja_id": message.data['order_pooja_id'].toString(),
        "astrologerId": message.data['astrologerId'].toString(),
        "customerId": message.data['customerId'].toString(),
        "image": message.data['image'].toString(),
        "pooja_id": message.data['pooja_id'].toString(),
        "customerName": message.data['name'].toString(),
        "name": message.data['name'].toString(),
        "token": message.data['token'].toString(),
        "channel": message.data['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toString().toLowerCase() ==
        "pooja-video-call") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-video-call",
        "callerId": message.data['poojaId'],
        "callerName": message.data['name'],
        "poojaId": message.data['poojaId'],
        "astrologerId": message.data['astrologerId'],
        "customerId": message.data['customerId'],
        "astrologerName": message.data['name'],
        "astrologerImage": message.data['image'],
        "token": message.data['token'],
        "channelName": message.data['channel']
      };
      await showIncomingCallNotification(
          notificationInfo: notificationInfo, duration: 90000);
    } else if (message.data['type'] == "dismiss") {
      FlutterCallkitIncoming.endAllCalls();
    }
  }

  static void initializeNotifications(BuildContext context) async {
    await Permission.notification.request();
    await Permission.microphone.request();
    await Permission.camera.request();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      await handleFirebaseMessage(context, message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      await handleFirebaseMessage(context, message);
    });
  }

  static Future<void> rejectCallFunction(
      {required String communicationId}) async {
    var endPointUrl = UrlConstants.rejectCall;
    final Map<String, String> body = {'communication_id': communicationId};
    final dio = Dio();
    await dio
        .post("${UrlConstants.baseUrl}$endPointUrl", data: body)
        .then((val) => TLoggerHelper.info("Completed Successfully!!"));
  }

  static Future callDismiss({required String communicationId}) async {
    try {
      var endPointUrl = UrlConstants.callDismiss;
      Map<String, String> body = {
        "communication_id": communicationId,
        "user_type": "astrologer"
      };
      final dio = Dio();
      await dio
          .post("${UrlConstants.baseUrl}$endPointUrl", data: body)
          .then((val) => TLoggerHelper.info("Completed Successfully!!"));
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  static flutterCallKitListener(RemoteMessage message) async {
    FlutterCallkitIncoming.onEvent.listen((CallEvent? event) {
      switch (event!.event) {
        case Event.actionCallIncoming:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallStart:
          break;
        case Event.actionCallAccept:
          TLoggerHelper.info(event.event.name);

          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallDecline:
          TLoggerHelper.info(event.event.name);

          rejectCallFunction(
              communicationId: message.data['communicationId'].toString());
          callDismiss(
              communicationId: message.data['communicationId'].toString());
          break;
        case Event.actionDidUpdateDevicePushTokenVoip:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallEnded:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallTimeout:
          TLoggerHelper.info(event.event.name);
          rejectCallFunction(
              communicationId: message.data['communicationId'].toString());
          break;
        case Event.actionCallCallback:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleHold:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleMute:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleDmtf:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleGroup:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleAudioSession:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallCustom:
          TLoggerHelper.info(event.event.name);
          break;
      }
    });
  }

  @pragma("vm:entry-point")
  static Future<void> firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
        TLoggerHelper.info("Enabling Background Messaging for Android");
    String messageJson = jsonEncode(message.toMap());
    print("Full message as JSON: $messageJson");
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // TLoggerHelper.info("Chat Duration is :- ${message.data['duration']}");
    if (message.data['type'].toString().toLowerCase() == 'Call'.toLowerCase() ||
        message.data['type'].toString().toLowerCase() ==
            "Video".toLowerCase()) {
      final Map<String, String> notificationInfo = {
        "callerId": message.data['token'],
        "callerName": message.data['name'],
        "astrologerId": message.data['astrologerId'],
        "callerAvatar": message.data['image'],
        "channel": message.data['channel'],
        "duration": message.data['duration'],
        "token": message.data['token'],
        "communicationId": message.data['communicationId'],
        "type": message.data['type'].toString().toLowerCase()
      };
      await prefs.setString(Constants.isCalling, jsonEncode(notificationInfo));
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toString().toLowerCase() == 'accept') {
      final Map<String, dynamic> messageInfo = jsonDecode(message.data['info']);
      final Map<String, dynamic> notificationInfo = {
        'type': 'chat',
        "callerId": message.data["orderId"],
        "callerName": message.data["astrologerName"],
        "callerAvatar": message.data["image"],
        "communicationId": message.data["communicationId"],
        "image": message.data["image"],
        "name": message.data["astrologerName"],
        "astrologerId": message.data["astrologer_id"],
        "user_id": message.data["user_id"],
        "chat_price": message.data["chat_price"],
        "orderId": message.data["orderId"],
        "infoId": message.data["info_id"],
        "is_freechat": message.data["is_freechat"],
        "durationTime": message.data["duration_time"],
        "message_info": messageInfo
      };
      TLoggerHelper.warning(message.data["is_freechat"].toString());
      await prefs.setString(Constants.isCalling, jsonEncode(notificationInfo));

      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toString().toLowerCase() == "pooja-video") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-video",
        "callerId": message.data["order_pooja_id"],
        "callerName": message.data["name"],
        "callerAvatar": message.data["image"],
        "order_pooja_id": message.data['order_pooja_id'].toString(),
        "astrologerId": message.data['astrologerId'].toString(),
        "customerId": message.data['customerId'].toString(),
        "image": message.data['image'].toString(),
        "pooja_id": message.data['pooja_id'].toString(),
        "customerName": message.data['name'].toString(),
        "name": message.data['name'].toString(),
        "token": message.data['token'].toString(),
        "channel": message.data['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'].toString().toLowerCase() == "pooja-call") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-call",
        "callerId": message.data["order_pooja_id"],
        "callerName": message.data["name"],
        "callerAvatar": message.data["image"],
        "order_pooja_id": message.data['order_pooja_id'].toString(),
        "astrologerId": message.data['astrologerId'].toString(),
        "customerId": message.data['customerId'].toString(),
        "image": message.data['image'].toString(),
        "pooja_id": message.data['pooja_id'].toString(),
        "customerName": message.data['name'].toString(),
        "name": message.data['name'].toString(),
        "token": message.data['token'].toString(),
        "channel": message.data['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (message.data['type'] == "dismiss") {
      FlutterCallkitIncoming.endAllCalls();
    }

    flutterCallKitListener(message);
  }
}
