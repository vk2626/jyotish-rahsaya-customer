import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_background/flutter_background.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:go_router/go_router.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../../Screens/IncomingCallRequest.dart';
import '../../Screens/liveScreen/live_reel/agora_live_screen.dart';
import '../../firebase_options.dart';
import '../../router.dart';
import '../../router_constants.dart';
import '../helper_functions.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Api/Constants.dart';
import 'package:permission_handler/permission_handler.dart';
import '../logger_helper.dart';
import 'calling_service.dart';

class NotificationHandlerIOS {
  static Future<void> handleFirebaseMessage(
      BuildContext context, RemoteMessage message,
      {VoidCallback? callbackFunction}) async {
    // Convert the entire message to a JSON string and print it
    String messageJson = jsonEncode(message.toMap());
    print("Full message as JSON: $messageJson");

    Map<String, dynamic> parseMessage = await jsonDecode(messageJson);
    parseMessage = parseMessage['data'];
    // Existing logging and handling
    // TLoggerHelper.info("Message data entries: ${parseMessage.entries}");
    if (parseMessage['type'].toString().toLowerCase() == "call") {
      final Map<String, String> notificationInfo = {
        "callerId": parseMessage['token'],
        "callerName": parseMessage['name'],
        "astrologerId": parseMessage['astrologerId'],
        "callerAvatar": parseMessage['image'],
        "channel": parseMessage['channel'],
        "duration": parseMessage['duration'],
        "token": parseMessage['token'],
        "communicationId": parseMessage['communicationId'],
        "type": parseMessage['type'].toString().toLowerCase()
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toString().toLowerCase() == 'accept') {
      final Map<String, dynamic> messageInfo = jsonDecode(parseMessage['info']);
      final Map<String, dynamic> notificationInfo = {
        'type': 'chat',
        "callerId": parseMessage["orderId"],
        "callerName": parseMessage["astrologerName"],
        "callerAvatar": parseMessage["image"],
        "communicationId": parseMessage["communicationId"],
        "image": parseMessage["image"],
        "name": parseMessage["astrologerName"],
        "astrologerId": parseMessage["astrologer_id"],
        "user_id": parseMessage["user_id"],
        "chat_price": parseMessage["chat_price"],
        "orderId": parseMessage["orderId"],
        "infoId": parseMessage["info_id"],
        "is_freechat": parseMessage["is_freechat"],
        "durationTime": parseMessage["duration_time"],
        "message_info": messageInfo
      };


      // TLoggerHelper.warning(parseMessage["astrologerName"].toString());
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toLowerCase().toString() ==
        "Video".toLowerCase()) {
      // TLoggerHelper.info(parseMessage.entries.toString());
      // } else if (parseMessage['type'].toLowerCase().toString() == "chat-end") {
      //   TLoggerHelper.info("Listening to Chat end notification !!");

      //   context.goNamed(RouteConstants.mainhomescreen);
    } else if (parseMessage['type'].toLowerCase().toString() == "call-end") {
      TLoggerHelper.info("Listening to Call end notification !!");
      // if (FlutterBackground.isBackgroundExecutionEnabled) {
      //   FlutterBackground.disableBackgroundExecution();
      // }
      context.goNamed(RouteConstants.mainhomescreen);
      IncomingCallRequest.agoraEngine.leaveChannel();
      IncomingCallRequest.agoraEngine.release();
    } else if (parseMessage['type'].toString().toLowerCase() == "pooja-video") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-video",
        "callerId": parseMessage["order_pooja_id"],
        "callerName": parseMessage["name"],
        "callerAvatar": parseMessage["image"],
        "order_pooja_id": parseMessage['order_pooja_id'].toString(),
        "astrologerId": parseMessage['astrologerId'].toString(),
        "customerId": parseMessage['customerId'].toString(),
        "image": parseMessage['image'].toString(),
        "pooja_id": parseMessage['pooja_id'].toString(),
        "customerName": parseMessage['name'].toString(),
        "name": parseMessage['name'].toString(),
        "token": parseMessage['token'].toString(),
        "channel": parseMessage['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toString().toLowerCase() == "pooja-call") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-call",
        "callerId": parseMessage["order_pooja_id"],
        "callerName": parseMessage["name"],
        "callerAvatar": parseMessage["image"],
        "order_pooja_id": parseMessage['order_pooja_id'].toString(),
        "astrologerId": parseMessage['astrologerId'].toString(),
        "customerId": parseMessage['customerId'].toString(),
        "image": parseMessage['image'].toString(),
        "pooja_id": parseMessage['pooja_id'].toString(),
        "customerName": parseMessage['name'].toString(),
        "name": parseMessage['name'].toString(),
        "token": parseMessage['token'].toString(),
        "channel": parseMessage['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toString().toLowerCase() ==
        "pooja-video-call") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-video-call",
        "callerId": parseMessage['poojaId'],
        "callerName": parseMessage['name'],
        "poojaId": parseMessage['poojaId'],
        "astrologerId": parseMessage['astrologerId'],
        "customerId": parseMessage['customerId'],
        "astrologerName": parseMessage['name'],
        "astrologerImage": parseMessage['image'],
        "token": parseMessage['token'],
        "channelName": parseMessage['channel']
      };
      await showIncomingCallNotification(
          notificationInfo: notificationInfo, duration: 90000);
    } else if (parseMessage['type'] == "dismiss") {
      FlutterCallkitIncoming.endAllCalls();
    }
  }

  static void initializeNotifications(BuildContext context) async {
    await Permission.notification.request();
    await Permission.microphone.request();
    await Permission.camera.request();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      await handleFirebaseMessage(context, message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      await handleFirebaseMessage(context, message);
    });
  }

  static Future<void> rejectCallFunction(
      {required String communicationId}) async {
    var endPointUrl = UrlConstants.rejectCall;
    final Map<String, String> body = {'communication_id': communicationId};
    final dio = Dio();
    await dio
        .post("${UrlConstants.baseUrl}$endPointUrl", data: body)
        .then((val) => TLoggerHelper.info("Completed Successfully!!"));
  }

  static Future<void> runRejectCallIsolate(String communicationId) async {
    await FlutterIsolate.spawn<void>(rejectCallIsolateEntry, communicationId);
  }

  static Future callDismiss({required String communicationId}) async {
    var endPointUrl = UrlConstants.callDismiss;
    Map<String, String> body = {
      "communication_id": communicationId,
      "user_type": "astrologer"
    };
    final dio = Dio();
    await dio
        .post("${UrlConstants.baseUrl}$endPointUrl", data: body)
        .then((val) => TLoggerHelper.info("Completed Successfully!!"));
  }

  static void rejectCallIsolateEntry(dynamic message) async {
    final communicationId = message as String;
    try {
      await rejectCallFunction(communicationId: communicationId);
    } catch (e) {
      TLoggerHelper.error('Error occurred in isolate: $e');
    }
  }

  static flutterCallKitListener(RemoteMessage message) async {
// Convert the entire message to a JSON string and print it
    String messageJson = jsonEncode(message.toMap());
    Map<String, dynamic> parseMessage = await jsonDecode(messageJson);
    parseMessage = parseMessage['data'];
    FlutterCallkitIncoming.onEvent.listen((CallEvent? event) {
      switch (event!.event) {
        case Event.actionCallIncoming:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallStart:
          break;
        case Event.actionCallAccept:
          TLoggerHelper.info(event.event.name);
          FlutterCallkitIncoming.endAllCalls();
          break;
        case Event.actionCallDecline:
          TLoggerHelper.info(event.event.name);

          rejectCallFunction(
              communicationId: parseMessage['communicationId'].toString());
          callDismiss(
              communicationId: parseMessage['communicationId'].toString());
          break;
        case Event.actionDidUpdateDevicePushTokenVoip:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallEnded:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallTimeout:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallCallback:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleHold:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleMute:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleDmtf:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleGroup:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleAudioSession:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallCustom:
          TLoggerHelper.info(event.event.name);
          break;
      }
    });
  }

  @pragma("vm:entry-point")
  static Future<void> firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
        TLoggerHelper.info("Enabling Background Messaging for IOS");
    String messageJson = await jsonEncode(message.toMap());
    print("Full message as JSON: $messageJson");

    Map<String, dynamic> parseMessage = await jsonDecode(messageJson);
    parseMessage = parseMessage['data'];
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // TLoggerHelper.info("Chat Duration is :- ${parseMessage['duration']}");
    if (parseMessage['type'].toString().toLowerCase() == 'call'.toLowerCase() ||
        parseMessage['type'].toString().toLowerCase() ==
            "Video".toLowerCase()) {
      final Map<String, String> notificationInfo = {
        "callerId": parseMessage['token'],
        "callerName": parseMessage['name'],
        "astrologerId": parseMessage['astrologerId'],
        "callerAvatar": parseMessage['image'],
        "channel": parseMessage['channel'],
        "duration": parseMessage['duration'],
        "token": parseMessage['token'],
        "communicationId": parseMessage['communicationId'],
        "type": parseMessage['type'].toString().toLowerCase()
      };
      await prefs.setString(Constants.isCalling, jsonEncode(notificationInfo));
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toString().toLowerCase() == 'accept') {
      final Map<String, dynamic> messageInfo = jsonDecode(parseMessage['info']);
      final Map<String, dynamic> notificationInfo = {
        'type': 'chat',
        "callerId": parseMessage["orderId"],
        "callerName": parseMessage["astrologerName"],
        "callerAvatar": parseMessage["image"],
        "communicationId": parseMessage["communicationId"],
        "image": parseMessage["image"],
        "name": parseMessage["astrologerName"],
        "astrologerId": parseMessage["astrologer_id"],
        "user_id": parseMessage["user_id"],
        "chat_price": parseMessage["chat_price"],
        "orderId": parseMessage["orderId"],
        "infoId": parseMessage["info_id"],
        "is_freechat": parseMessage["is_freechat"],
        "durationTime": parseMessage["duration_time"],
        "message_info": messageInfo
      };
      // TLoggerHelper.warning(parseMessage["is_freechat"].toString());
      await prefs.setString(Constants.isCalling, jsonEncode(notificationInfo));
      // final Map<String, dynamic> notificationInfo = {
      //   "type": "chat",
      //   "callerId": "123456",
      //   "callerName": "Astrologer John Doe",
      //   "callerAvatar":
      //       "https://cdnb.artstation.com/p/assets/images/images/034/457/413/large/shin-min-jeong-.jpg?1612345200",
      //   "communicationId": "78910",
      //   "image":
      //       "https://cdnb.artstation.com/p/assets/images/images/034/457/413/large/shin-min-jeong-.jpg?1612345200",
      //   "name": "Astrologer John Doe",
      //   "astrologer_id": "astrologer_001",
      //   "user_id": "user_123",
      //   "chat_price": "50",
      //   "orderId": "order_456",
      //   "infoId": "info_789",
      //   "is_freechat": "false",
      //   "durationTime": "30",
      //   "message_info": {}
      // };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toString().toLowerCase() == "pooja-video") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-video",
        "callerId": parseMessage["order_pooja_id"],
        "callerName": parseMessage["name"],
        "callerAvatar": parseMessage["image"],
        "order_pooja_id": parseMessage['order_pooja_id'].toString(),
        "astrologerId": parseMessage['astrologerId'].toString(),
        "customerId": parseMessage['customerId'].toString(),
        "image": parseMessage['image'].toString(),
        "pooja_id": parseMessage['pooja_id'].toString(),
        "customerName": parseMessage['name'].toString(),
        "name": parseMessage['name'].toString(),
        "token": parseMessage['token'].toString(),
        "channel": parseMessage['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'].toString().toLowerCase() == "pooja-call") {
      final Map<String, dynamic> notificationInfo = {
        "type": "pooja-call",
        "callerId": parseMessage["order_pooja_id"],
        "callerName": parseMessage["name"],
        "callerAvatar": parseMessage["image"],
        "order_pooja_id": parseMessage['order_pooja_id'].toString(),
        "astrologerId": parseMessage['astrologerId'].toString(),
        "customerId": parseMessage['customerId'].toString(),
        "image": parseMessage['image'].toString(),
        "pooja_id": parseMessage['pooja_id'].toString(),
        "customerName": parseMessage['name'].toString(),
        "name": parseMessage['name'].toString(),
        "token": parseMessage['token'].toString(),
        "channel": parseMessage['channel'].toString(),
      };
      await showIncomingCallNotification(notificationInfo: notificationInfo);
    } else if (parseMessage['type'] == "dismiss") {
      FlutterCallkitIncoming.endAllCalls();
    }

    flutterCallKitListener(message);
  }
}
