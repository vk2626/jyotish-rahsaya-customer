// import 'dart:math';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// class NotificationHandler {
//   final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//       FlutterLocalNotificationsPlugin();

//   static const String channelId = "normal_channel_id";
//   static const String channelName = "Normal Channel";
//   static const String channelDescription = "Receiving All Normal Notifications";

//   // Define Android notification details
//   final AndroidNotificationDetails _androidNotificationDetails =
//       const AndroidNotificationDetails(
//     channelId,
//     channelName,
//     channelDescription: channelDescription,
//     importance: Importance.low,
//     playSound: true,
//     enableVibration: true,
//   );

//   // Define iOS/macOS notification details
//   final DarwinNotificationDetails _darwinNotificationDetails =
//       const DarwinNotificationDetails(
//     presentAlert: true,
//     presentBadge: true,
//     presentSound: true,
//   );

//   // Create a notification channel for Android
//   final AndroidNotificationChannel androidNotificationChannel =
//       const AndroidNotificationChannel(
//     channelId,
//     channelName,
//     description: channelDescription,
//     importance: Importance.low,
//     playSound: true,
//     enableVibration: true,
//   );

//   // Initialize notifications
//   Future<void> initializeNotification() async {
//     const AndroidInitializationSettings initializationSettingsAndroid =
//         AndroidInitializationSettings('@mipmap/ic_launcher');

//     const DarwinInitializationSettings initializationSettingsDarwin =
//         DarwinInitializationSettings(
      
//     );

//     const InitializationSettings initializationSettings =
//         InitializationSettings(
//       android: initializationSettingsAndroid,
//       iOS: initializationSettingsDarwin,
//       macOS: initializationSettingsDarwin,
//     );

//     try {
//       // Create the notification channel for Android
//       await flutterLocalNotificationsPlugin
//           .resolvePlatformSpecificImplementation<
//               AndroidFlutterLocalNotificationsPlugin>()
//           ?.createNotificationChannel(androidNotificationChannel);

//       // Request permissions for iOS
//       await flutterLocalNotificationsPlugin
//           .resolvePlatformSpecificImplementation<
//               IOSFlutterLocalNotificationsPlugin>()
//           ?.requestPermissions(
//             alert: true,
//             badge: true,
//             sound: true,
//           );

//       // Initialize the notification plugin
//       await flutterLocalNotificationsPlugin.initialize(
//         initializationSettings,
//         onDidReceiveNotificationResponse: onNotificationResponse,
//       );

//       print("Notification Initialized Successfully!");
//     } catch (e) {
//       print("Error initializing notifications: $e");
//     }
//   }

//   // Handle notifications received while the app is in the foreground
//   static void onDidReceiveLocalNotification(
//       int id, String? title, String? body, String? payload) {
//     print("Received local notification: $title - $body");
//   }

//   // Handle notification tapped actions
//   static void onNotificationResponse(NotificationResponse response) {
//     if (response.actionId == "accept_action") {
//       print("User accepted the call.");
//     } else if (response.actionId == "decline_action") {
//       print("User declined the call.");
//     } else {
//       print("Notification tapped: ${response.payload}");
//     }
//   }

//   // Show a notification with actions (e.g., Accept and Decline)
//   Future<void> showNotificationWithActions() async {
//     int randomId = Random().nextInt(1000);

//     const AndroidNotificationDetails androidDetailsWithActions =
//         AndroidNotificationDetails(
//       channelId,
//       channelName,
//       channelDescription: channelDescription,
//       importance: Importance.high,
//       playSound: true,
//       enableVibration: true,
//       actions: [
//         AndroidNotificationAction(
//           "accept_action",
//           "Accept",
          
//         ),
//         AndroidNotificationAction(
//           "decline_action",
//           "Decline",
          
//         ),
//       ],
//     );

//     const DarwinNotificationDetails darwinDetailsWithActions =
//         DarwinNotificationDetails(
//       presentAlert: true,
//       presentBadge: true,
//       presentSound: true,
//       categoryIdentifier: "CALL_CATEGORY",
//     );

//     await flutterLocalNotificationsPlugin.show(
//       randomId,
//       "Incoming Call",
//       "You have an incoming call. Accept or Decline?",
//       NotificationDetails(
//         android: androidDetailsWithActions,
//         iOS: darwinDetailsWithActions,
//         macOS: darwinDetailsWithActions,
//       ),
//     );
//   }

//   // Show a normal notification
//   Future<void> showNormalNotification() async {
//     int randomId = Random().nextInt(1000);
//     await flutterLocalNotificationsPlugin.show(
//       randomId,
//       "Notification $randomId",
//       "This is the Notification Description for Notification Id: $randomId",
//       NotificationDetails(
//         android: _androidNotificationDetails,
//         iOS: _darwinNotificationDetails,
//         macOS: _darwinNotificationDetails,
//       ),
//     );
//   }
// }
