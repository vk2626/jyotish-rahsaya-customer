import 'dart:convert';

import 'package:dio/dio.dart';
import '../../handler_classes/secure_storage.dart';
import '../Model/ChatingModel.dart';
import '../Model/remedy_purchased_model.dart';
import '../Model/unblock.model.dart';
import '../logger_helper.dart';

import '../Model/AddToRecentListModel.dart';
import '../Model/AstroMallOrdersModel.dart';
import '../Model/BlockUnblockModel.dart';
import '../Model/ChatIntakeFromResponse.dart';
import '../Model/ChatModel.dart';
import '../Model/LanguageModel.dart';
import '../Model/LeaveChatModel.dart';
import '../Model/OrderHistoryModel.dart';
import '../Model/PaymentLogModel.dart';
import '../Model/RecentSearchModel.dart';
import '../Model/ReviewsModel.dart';
import '../Model/SimilarConsultantsResp.dart';
import '../Model/SkillsModel.dart';
import '../Model/UpdateProfileResponse.dart';
import '../Model/UserDetails.dart';
import '../Model/UserList.dart';
import '../Model/WaitListModel.dart';
import '../Model/astrologersDetails.dart';
import '../Model/createOrderModel.dart';
import '../Model/create_order_pooja_model.dart';
import '../Model/customer_remedy_model.dart';
import '../Model/login.model.dart';
import '../Model/wallet.transection.model.dart';
import '../Model/walletPaymentOrderCreate.dart';

class DioClient {
  static SecureStorage secureStorage = SecureStorage();
  Future<Dio> getClient() async {
    Dio dio = Dio();
    // Get the access token from secure storage
    String? accessToken = await secureStorage.getAccessToken(endPointUrl: "");

    Map<String, String> headers = <String, String>{
      'Accept': 'application/json',
      'Authorization':
          'Bearer ${accessToken ?? ""}', // Add Authorization header
    };

    dio.options.headers = headers;
    dio.options.connectTimeout = const Duration(minutes: 5);
    dio.options.receiveTimeout = const Duration(minutes: 5);

    dio.interceptors.add(LogInterceptor(
        request: true,
        requestBody: true,
        requestHeader: true,
        responseBody: true,
        responseHeader: true));
    return dio;
  }

  Future<OrderHistoryModel> orderHistory(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    OrderHistoryModel lr = OrderHistoryModel.fromJson(response.data);
    return lr;
  }

  Future<LoginModel> loginModel(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    LoginModel lr = LoginModel.fromJson(response.data);
    return lr;
  }

  Future<WalletTransactionModel> walletTransaction(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    WalletTransactionModel lr = WalletTransactionModel.fromJson(response.data);
    return lr;
  }

  Future<AddToRecentListModel> addToRecentList(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    AddToRecentListModel lr = AddToRecentListModel.fromJson(response.data);
    return lr;
  }

  Future<RecentSearchModel> recentSearch(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    RecentSearchModel lr = RecentSearchModel.fromJson(response.data);
    return lr;
  }

  Future<LeaveChatModel> leaveChat(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    LeaveChatModel lr = LeaveChatModel.fromJson(response.data);
    return lr;
  }

  Future<BlockUnblockModel> blockUnblock(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    BlockUnblockModel lr = BlockUnblockModel.fromJson(response.data);
    return lr;
  }

  Future<UnblockModel> Unblock(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    UnblockModel lr = UnblockModel.fromJson(response.data);
    return lr;
  }

  Future<CreateOrderModel> createOrder(
      dio, api, Map<String, dynamic> map) async {
    Response response;
    TLoggerHelper.info(map.entries.toString());
    response = await dio.post(api, data: map);
    TLoggerHelper.debug(map.entries.toString());
    CreateOrderModel lr = CreateOrderModel.fromJson(response.data);
    return lr;
  }

  // Future<CreateOrderPoojaModel> createOrderPooja(dio, api, map) async {
  //   Response response;
  //   response = await dio.post(api, data: map);
  //   CreateOrderPoojaModel lr = CreateOrderPoojaModel.fromJson(response.data);
  //   return lr;
  // }

  Future<WalletPaymentOrderCreate> createWalletOrder(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    WalletPaymentOrderCreate lr =
        WalletPaymentOrderCreate.fromJson(response.data);
    return lr;
  }

  Future<UserDetails> userDetails(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    UserDetails lr = UserDetails.fromJson(response.data);
    return lr;
  }

  Future<PaymentLogModel> paymentLogs(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    PaymentLogModel lr = PaymentLogModel.fromJson(response.data);
    return lr;
  }

  Future<UpdateProfileResponse> updateProfile(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    UpdateProfileResponse lr = UpdateProfileResponse.fromJson(response.data);
    return lr;
  }

  Future<ChatIntakeFromResponse> chatIntakeForm(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    ChatIntakeFromResponse lr = ChatIntakeFromResponse.fromJson(response.data);
    return lr;
  }

  // Future<WaitListModel> waitList(dio, api, map) async {
  //   Response response;
  //   response = await dio.post(api, data: map);
  //   WaitListModel lr = WaitListModel.fromJson(response.data);
  //   return lr;
  // }

  Future<AstrologersDetails> astrologerDetails(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    AstrologersDetails lr = AstrologersDetails.fromJson(response.data);
    return lr;
  }

  Future<AstroMallOrdersModel> astroMallOrders(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    AstroMallOrdersModel lr = AstroMallOrdersModel.fromJson(response.data);
    return lr;
  }

  Future<CustomerRemedyModel> customerRemedy(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    CustomerRemedyModel lr = CustomerRemedyModel.fromJson(response.data);
    return lr;
  }

  Future<CustomerRemedyPurchasedListModel> customerPurchasedRemedy(
      dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    CustomerRemedyPurchasedListModel lr =
        CustomerRemedyPurchasedListModel.fromJson(response.data);
    return lr;
  }

  Future<UserList> chatIntakeFormUsers(dio, api, map) async {
    Response response;
    response = await dio.post(api, data: map);
    UserList lr = UserList.fromJson(response.data);
    return lr;
  }

  Future<ReviewsModel> reviewsResponse(dio, api) async {
    Response response;
    response = await dio.get(api);
    ReviewsModel lr = ReviewsModel.fromJson(response.data);
    return lr;
  }

  Future<ChatModel> chatHistory(dio, api) async {
    Response response;
    response = await dio.get(api);
    ChatModel lr = ChatModel.fromJson(response.data);
    return lr;
  }

  Future<ChatingModel> chatingHistory(dio, api) async {
    Response response;
    response = await dio.get(api);
    ChatingModel lr = ChatingModel.fromJson(response.data);
    return lr;
  }

  Future<SimilarConsultantsResp> similarConsultantsResp(dio, api) async {
    Response response;
    response = await dio.get(api);
    SimilarConsultantsResp lr = SimilarConsultantsResp.fromJson(response.data);
    return lr;
  }

  Future<SkillsModel> skills(dio, api) async {
    Response response;
    response = await dio.get(api);
    SkillsModel lr = SkillsModel.fromJson(response.data);
    return lr;
  }

  Future<LanguageModel> languages(dio, api) async {
    Response response;
    response = await dio.get(api);
    LanguageModel lr = LanguageModel.fromJson(response.data);
    return lr;
  }
}
