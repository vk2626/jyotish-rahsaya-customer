import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';

import '../helper_functions.dart';
import 'Constants.dart';

class AuthenticationAPI {
  final client = http.Client();
  final headers = {'Accept': 'application/json'};

/* User Login */
  Future loginAccount(
      {required String phoneNum, required String deviceToken}) async {
    var endPointUrl = UrlConstants.loginUrl ;

    Uri uri = Uri.parse(UrlConstants.baseUrl_login + endPointUrl);

    Map body = {};
    body['mobile'] = phoneNum;
    body['device_token'] = deviceToken;
    print("login parameters --------->>>> ${phoneNum}  ${deviceToken}");
    final http.Response response =
        await client.post(uri, body: body, headers: headers);
    final dynamic resBody = response.body;
    debugPrint("User Login Response ===============> $resBody");
    return resBody;
  }

  /* Verifies OTP*/

  Future verifyOTP({required String mobileNumber, required String otp}) async {
    var endPointUrl = UrlConstants.verifyOTP;
    Map<String, String> body = {"mobile": mobileNumber, "otp": otp};
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body,isWithToken: false);
  }
}
