import 'dart:convert';
import 'dart:developer' as developer;
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Key/api_keys.dart';
import '../../handler_classes/secure_storage.dart';
import '../Model/mall.product.slider.dart';
import '../logger_helper.dart';
import '../Model/accept_cutomer_call_model.dart';
import '../Model/create_order_pooja_model.dart';
import '../helper_functions.dart';
import 'Constants.dart';
import '../Model/SearchModel.dart';
import '../Model/blog.model.dart';
import '../Model/book.pooja.model.dart';
import '../Model/celebrity.customer.model.dart';
import '../Model/follower.model.dart';
import '../Model/gift.api.model.dart';
import '../Model/jyotish.mall.model.dart';
import '../Model/newly.launched.model.dart';
import '../Model/recharge.amount.model.dart';
import '../Model/recharge.total.amount.model.dart';
import '../Model/total.success.payment.model.dart';

import '../Model/astrologer.category.list.model.dart';
import '../Model/astrologer.list.model.dart';
import '../Model/astrologer.slider.category.model.dart';
import '../Model/banner.model.dart';
import '../Model/gift.wallet.payment.model.dart';

import '../Model/my.following.model.dart';
import '../Model/spell.slider.model.dart';
import '../Model/wallet.transection.model.dart';

class ListOfApi {
  final client = http.Client();
  static SecureStorage secureStorage = SecureStorage();
  // var headers = {'Accept': 'application/json'};  // old header with auth

  Future<Map<String, String>> getHeaders() async {
    var headers = {
      'Accept': 'application/json',
      'Authorization':
          "Bearer ${await SecureStorage().getAccessToken(endPointUrl: "")}"
    };

    return headers;
  }

  Future<AstrologerListModel?> getAstrologerWithOrderList(
      {required String type}) async {
    try {
      String userId = await THelperFunctions.getUserId();
      String endPointUrl =
          "${UrlConstants.recentOrderChatList}?type=$type&user_id=$userId";
      final Map<String, dynamic> parseData = await jsonDecode(
          await THelperFunctions.httpGetHandler(
              client: client, endPointUrl: endPointUrl));
      return AstrologerListModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getSimilarConsultants({required String primarySkillIds}) async {
    try {
      final String endPointUrl =
          "${UrlConstants.similarAstrologerlist}?primary_skill_id=$primarySkillIds";
      TLoggerHelper.debug(endPointUrl);
      return await THelperFunctions.httpGetHandler(
          client: client, endPointUrl: endPointUrl);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getBlogNew() async {
    try {
      final String endPointUrl = UrlConstants.blog;
      TLoggerHelper.debug(endPointUrl);
      return await THelperFunctions.httpGetHandler(
          client: client, endPointUrl: endPointUrl);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getMallDataNew() async {
    try {
      final String endPointUrl = UrlConstants.jyotishMallCategory;
      TLoggerHelper.debug(endPointUrl);
      return await THelperFunctions.httpGetHandler(
          client: client, endPointUrl: endPointUrl);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getAstrologerCategoryList() async {
    try {
      final String endPointUrl = UrlConstants.astrologerCategory;
      TLoggerHelper.debug(endPointUrl);
      return await THelperFunctions.httpGetHandler(
          client: client, endPointUrl: endPointUrl);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getSliderAstrologerCategoryList({required String id}) async {
    try {
      final String endPointUrl = "${UrlConstants.slider}?astrologer_category=$id";
      TLoggerHelper.debug(endPointUrl);
      return await THelperFunctions.httpGetHandler(
          client: client, endPointUrl: endPointUrl);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getCelebrityCustomer() async {
    try {
      final String endPointUrl = UrlConstants.celebrityCustomer;
      TLoggerHelper.debug(endPointUrl);
      return await THelperFunctions.httpGetHandler(
          client: client, endPointUrl: endPointUrl);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getAstrologerDetails(
      {required String astrologerId, required String type}) async {
    try {
      final String endPointUrl = UrlConstants.astrologerProfile;
      final Map<String, String> body = {
        'astrologer_id': astrologerId,
        'user_id': await THelperFunctions.getUserId(),
        "type": type
      };
      TLoggerHelper.warning(body.entries.toString());
      return await THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getProductDetailsItem({required String productId}) async {
    final String endPointUrl = UrlConstants.getProductDetails;
    final Map<String, String> body = {"id": productId};

    return await THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future submitBlockReport(
      {required String astrologerId,
      required String reason,
      required bool blockStatus}) async {
    try {
      final String endPointUrl = UrlConstants.blockAstrologer;
      final Map<String, String> body = {
        "user_id": await THelperFunctions.getUserId(),
        "astrologer_id": astrologerId,
        "status": blockStatus ? "1" : "0",
        "report": reason
      };
      TLoggerHelper.debug(body.entries.toString());
      return await THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getPoojaDetails({required String poojaId}) async {
    try {
      final String endPointUrl = UrlConstants.getPoojaDetails;
      final Map<String, String> body = {
        "customer_id": await THelperFunctions.getUserId(),
        "pooja_id": poojaId,
      };
      TLoggerHelper.debug(body.entries.toString());
      return await THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future getPanditDetails({required String astrologerId}) async {
    try {
      final String endPointUrl = UrlConstants.panditDetails;
      final Map<String, String> body = {
        "astrologer_id": astrologerId,
      };
      TLoggerHelper.debug(body.entries.toString());
      return await THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  /*********************** List of Astrologer *************************/

  Future<AstrologerListModel?> getAstrologerList(
      int userId, String type, int pageNumber, Set<String> pagedUserIDs,
      {required List<String> selectedSortBy,
      required List<String> selectedCountry,
      required List<String> selectedGender,
      required List<String> selectedLng,
      required List<String> selectedSkills}) async {
    try {
    String endPointUrl = UrlConstants.astrologerChatCallList;
    final Map<String, dynamic> body = {
      "type": type,
      "user_id": userId.toString(),
      "page": pageNumber.toString(),
      "fetched_ids[]": jsonEncode(pagedUserIDs.toList()),
    };
    var price = "";
    var experience_year = "";
    var order = "";
    if (selectedSortBy.isNotEmpty) {
      price = selectedSortBy.first == "Price : Low to High"
          ? "low"
          : selectedSortBy.first == "Price : High to Low"
              ? "high"
              : "";
      experience_year = selectedSortBy.first == "Experience : Low to High"
          ? "low"
          : selectedSortBy.first == "Experience : High to Low"
              ? "high"
              : "";
      order = selectedSortBy.first == "Orders : Low to High"
          ? "low"
          : selectedSortBy.first == "Orders : High to Low"
              ? "high"
              : "";
    }
    if (price.isNotEmpty) {
      body['price'] = price;
    }
    if (experience_year.isNotEmpty) {
      body['experience_year'] = experience_year;
    }
    if (order.isNotEmpty) {
      body['order'] = order;
    }
    if (selectedGender.isNotEmpty) {
      selectedGender.forEach((element) => body['gender[]'] = element);
    }
    if (selectedCountry.isNotEmpty) {
      selectedCountry.forEach((element) => body['country[]'] = element);
    }
    if (selectedLng.isNotEmpty) {
      selectedLng.forEach((element) => body['language[]'] = element);
    }
    if (selectedSkills.isNotEmpty) {
      selectedSkills.forEach((element) => body['skill[]'] = element);
    }

    TLoggerHelper.debug(body.entries.toString());
    final Map<String, dynamic> parseData = jsonDecode(
        await THelperFunctions.httpPostHandler(
            client: client, endPointUrl: endPointUrl, body: body));
    TLoggerHelper.debug(parseData.toString());
    if (parseData['status']) {
      return AstrologerListModel.fromJson(parseData);
    } else {
      debugPrint("Error Occured!!");
    }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }
 
  Future getRechargeData() async {
    String endPointUrl = UrlConstants.recharge;
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);

    try {
      final headers = await getHeaders();
      final http.Response response = await client.get(uri, headers: headers);
      var responseBody = response.body;
      developer.log("Recharge Body Data =======> ${responseBody}");
      var jsonData = jsonDecode(responseBody);
      if (jsonData['status'] == true) {
        // var dataList = jsonData['recharge'];

        developer.log("Recharge Response ===> ${jsonData}");
        return RechargeAmountModel.fromJson(jsonData);
      } else {
        print("Something went wrong");
      }
    } catch (e) {
      developer.log("Error---> $e");
    }
  }

  /********************** Payment Recharge TOTAL Create Response Amount Data ***********************/

  Future totalPayment({required String amountID}) async {
    String endPointUrl = "${UrlConstants.payment}?amount=$amountID";
    TLoggerHelper.info(endPointUrl);
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      final headers = await getHeaders();
      final http.Response response = await client.get(uri, headers: headers);
      var totalRBody = response.body;
      var jsonData = jsonDecode(totalRBody);
      developer.log(" Payment Body =======> ${totalRBody}");
      return RechargeTotalAmountModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      print("Recharge Res Error --> $e");
    }
  }

  /********************** Gift Wallet Payment ***********************/

  Future giftWalletPayment(
      {required String userID,
      required String astrologerID,
      required giftID}) async {
    String endPointUrl = "gift-wallet-payment";
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map body = {};
      body['user_id'] = userID;
      body['astrologer_id'] = astrologerID;
      body['gift_id'] = giftID;
      final headers = await getHeaders();
      final http.Response response =
          await client.post(uri, headers: headers, body: body);
      var totalRBody = response.body;
      var jsonData = jsonDecode(totalRBody);
      developer.log("Gift Wallet =======> ${totalRBody}");
      return GiftWalletPaymentModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      print("Gift Wallet Res Error --> $e");
    }
  }

  /********************** Create Order Pooja Payment ***********************/

  Future createOrderPooja(
      {required String astrologerID,
      required String userInfoId,
      required String amount,
      required String productID,
      required String isWallet,
      required String realPricePoojaWithoutGST,
      required List<String> productPrice,
      required String suggestedAstrologerId}) async {
    String endPointUrl = UrlConstants.createOrderPooja;
    final Uri uri = Uri.parse(
        UrlConstants.baseUrl+endPointUrl
        );
    // try {
    Map<String, dynamic> body = {
      'user_id': await THelperFunctions.getUserId(),
      'astrologer_id': astrologerID,
      'product_id': productID,
      'is_wallet': isWallet,
      'amount': amount,
      "pooja_price": realPricePoojaWithoutGST
    };
    if (suggestedAstrologerId.isNotEmpty) {
      body['suggested_astro_id'] = suggestedAstrologerId;
    }

    productPrice.forEach((data) {
      body['product_price[]'] = data;
    });
    TLoggerHelper.debug(body.toString());
    TLoggerHelper.debug(jsonEncode(productPrice).toString());
    final headers = await getHeaders();
    final http.Response response =
        await client.post(uri, headers: headers, body: body);
    var jsonData = jsonDecode(response.body);
    final Map<String, dynamic> parseData = jsonData;
    TLoggerHelper.info("Create Order Pooja =======> ${response.body}");

    if (!parseData['status']) {
      Fluttertoast.showToast(msg: jsonData['message'].toString());
    }
    return CreateOrderPoojaModel.fromJson(jsonData);
    // } catch (e) {
    //   TLoggerHelper.error("Create Order Pooja Res Error --> $e");
    //   throw Exception("Unexpected error: $e");
    // }
  }

  /********************** Payment Success Response Amount Data ***********************/

  Future totalSuccessPayment(
      {required String orderId,
      required String transactionID,
      required String razorpayId,
      required String razorpaySecretId,
      String? remedy_id,
      String? suggestedAstrologerId,
      String? astroProductId}) async {
    
    String endPointUrl = UrlConstants.orderPaymentComplete;
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      String status = await getPaymentStatus(
          transactionId: transactionID,
          razorpayId: razorpayId,
          razorpaySecretId: razorpaySecretId);
      Map<String, String> body = {
        'order_id': orderId,
        'transaction_id': transactionID,
        'is_success': "1",
        "payment_status": status
      };

      body['remedy_order_id'] = remedy_id ?? "";
      body['astro_pooja_id'] = astroProductId ?? "";
      body['astro_product_id'] = astroProductId ?? "";

      print(body.toString());
      final headers = await getHeaders();
      final http.Response response =
          await http.post(uri, headers: headers, body: body);
      var successRBody = response.body;
      var jsonData = jsonDecode(successRBody);
      developer.log("Payment Success Res =======> ${successRBody}");
      return TotalSuccessPaymentModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      print("Success Res Error --> $e");
    }
  }

  Future<String> getPaymentStatus({
    required String transactionId,
    required String razorpayId,
    required String razorpaySecretId,
  }) async {
    // Encode keyId and secretKey
    String basicAuth =
        'Basic ${base64Encode(utf8.encode('${razorpayId}:${razorpaySecretId}'))}';
    var headers = {
      'Authorization': basicAuth,
    };
    var request = http.Request('GET',
        Uri.parse('https://api.razorpay.com/v1/payments/$transactionId'));

    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      Map<String, dynamic> parseData =
          await jsonDecode(await response.stream.bytesToString());
      return parseData['status'].toString();
    } else {
      return "created";
    }
  }

  // Future totalSuccessPayment(
  //     {required String amountID,
  //     required String userID,
  //     required String orderId,
  //     required String transactionID,
  //     String? remedy_order_id}) async {
  //   String endPointUrl = "order-payment";
  //   final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
  //   try {
  //     Map body = {};
  //
  //     body['order_id'] = orderId;
  //     body['transaction_id'] = transactionID;
  //     body['is_success'] = "1";
  //     body['remedy_order_id'] = remedy_order_id;
  //     final http.Response response =
  //         await client.post(uri, headers: headers, body: body);
  //     var successRBody = response.body;
  //     var jsonData = jsonDecode(successRBody);
  //     log("Success Recharge =======> ${successRBody}");
  //     return TotalSuccessPaymentModel.fromJson(jsonData);
  //   } on SocketException catch (e) {
  //     debugPrint("You Need Better Network Connection $e");
  //   } catch (e) {
  //     print("Success Res Error --> $e");
  //   }
  // }

  /**************************** Wallet Transaction *******************************/

  Future walletTransaction({required String userID}) async {
    String endPointUrl =UrlConstants.walletTranscation;

    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map body = {};
      body['user_id'] = userID;
      final headers = await getHeaders();
      final http.Response response =
          await client.post(uri, body: body, headers: headers);
      var data = response.body;
      var jsonData = jsonDecode(data);
      print("Wallet Json --> $data");
      return WalletTransactionModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      throw Exception('Error fetching wallet transactions: $e');
    }
  }

  /**************************** Search Api *******************************/

  Future search({required String name}) async {
    String endPointUrl = "search";

    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map body = {};
      body['name'] = name;
      final headers = await getHeaders();
      final http.Response response =
          await client.post(uri, body: body, headers: headers);
      var data = response.body;
      var jsonData = jsonDecode(data);
      print("Search --> ${response.body}");
      return SearchModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      throw Exception('Error fetching search: $e');
    }
  }

  /**************************** Follower Api *******************************/

  Future followerNew(
      {required String astrologerID, required bool status}) async {
    String endPointUrl = "follower";
    // final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map<String, String> body = {};
      body['customer_id'] = await THelperFunctions.getUserId();
      body['astrologer_id'] = astrologerID;
      body['status'] = status ? "1" : "0";
      TLoggerHelper.debug(body.entries.toString());
      return await THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      print("Follower Res Error --> $e");
    }
  }

  Future follower(
      {required String customerID,
      required String astrologerID,
      required String status}) async {
    String endPointUrl = "follower";
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map body = {};
      body['customer_id'] = customerID;
      body['astrologer_id'] = astrologerID;
      body['status'] = status;
      final headers = await getHeaders();
      final http.Response response =
          await client.post(uri, headers: headers, body: body);
      var data = response.body;
      print(" Follow Provider--> $data");
      var jsonData = jsonDecode(data);
      return FollowModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      print("Follower Res Error --> $e");
    }
  }

  /********************** Send Call Duration *********************************/
  Future sendCallDuration(
      {required String communicationId, required int duration}) async {
    const endPointUrl = UrlConstants.userEndCall ;
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map<String, String> body = {};
      body['communication_id'] = communicationId;
      body['user_type'] = "customer";

      TLoggerHelper.debug(
          body.entries.toString() + "   ------Send Call Duration");
      final headers = await getHeaders();
      final http.Response response =
          await client.post(uri, headers: headers, body: body);
      var sendDataRes = response.body;
      TLoggerHelper.debug("Send Call Duration Response: ${sendDataRes}");
      var jsonData = jsonDecode(sendDataRes);
      return AcceptCustomerStartCallModel.fromJson(jsonData);
    } catch (e) {
      TLoggerHelper.debug(e.toString());
    }
  }

  // Future sendCallDuration(
  //     {required String communicationId, required int duration}) async {
  //   const endPointUrl = "accept-customer-end-call-live";
  //   final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
  //   try {
  //     Map<String, String> body = {};
  //     body['communication_id'] = communicationId;
  //     body['call_duration'] = duration.toString();
  //     body['accept_by_customer'] = "1";
  //     TLoggerHelper.debug(
  //         body.entries.toString() + "   ------Send Call Duration");
  //     final http.Response response =
  //         await client.post(uri, headers: headers, body: body);
  //     var sendDataRes = response.body;
  //     TLoggerHelper.debug("Send Call Duration Response: ${sendDataRes}");
  //     var jsonData = jsonDecode(sendDataRes);
  //     return AcceptCustomerStartCallModel.fromJson(jsonData);
  //   } catch (e) {
  //     TLoggerHelper.debug(e.toString());
  //   }
  // }

  // /************************************ Send Review *******************************/
  Future sendReview(
      {required String astrologer_id,
      required String customer_id,
      required String description,
      required String rate,
      required String isLike,
      required int reviewId,
      required String type,
      required String orderID,
      required String infoId}) async {
    final endPointUrl = UrlConstants.sendReview ;

    try {
      Map<String, String> body = {
        'astrologer_id': astrologer_id,
        'customer_id': customer_id,
        'description': description,
        'rate': rate,
        'type': type,
        'order_id': orderID,
        "info_id": infoId
      };
      if (reviewId != -1) {
        body['id'] = reviewId.toString();
      }

      return await THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    } catch (e) {
      TLoggerHelper.debug(e.toString());
    }
  }

  /************************** Customer Start Chat ******************************/
  Future acceptCustomerStartChat({
    required String communicationid,
  }) async {
    const subUrl = UrlConstants.acceptCustomerStartChat ;

    final Uri uri = Uri.parse(UrlConstants.baseUrl + subUrl);
    Map body = {
      "eventName": "chatStart",
      "channelName": "chat.info_id_${communicationid}"
    };
    body['communication_id'] = communicationid;
    TLoggerHelper.debug(subUrl + "   ----------   " + body.entries.toString());
    final headers = await getHeaders();
    final http.Response response =
        await client.post(uri, headers: headers, body: body);
    final dynamic bodyy = response.body;
    print("StartChatbody${bodyy.toString()}");
    return bodyy;
  }

  // Future acceptCustomerStartChatNew({
  //   required String communicationid,
  // }) async {
  //   const subUrl = 'accept-customer-chat-start-new';

  //   final Uri uri = Uri.parse(UrlConstants.baseUrl + subUrl);
  //   Map body = {};
  //   body['communication_id'] = communicationid;
  //   TLoggerHelper.debug(subUrl + "   ----------   " + body.entries.toString());
  //   final headers = await getHeaders();
  //   final http.Response response =
  //       await client.post(uri, headers: headers, body: body);
  //   final dynamic bodyy = response.body;
  //   print("StartChatbody${bodyy.toString()}");
  //   return bodyy;
  // }

  /********************** Jyotish Mall ****************************/

  Future getJyotishMallNew() async {
    String endPointUrl = UrlConstants.jyotishMallCategory;
    return await THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  /********************** Jyotish Mall Category Detail ****************************/
  getJyotishMallDetail(String categoryId, String suggestedType) async {
    String endPointUrl =
        "${UrlConstants.mallItemDetails}?category=$categoryId&suggested_type=$suggestedType";
    TLoggerHelper.debug(endPointUrl);
    return THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  /**************************** Following Api *******************************/

  Future<FollowingModel?> following({required String customerID}) async {
    String endPointUrl = UrlConstants.followerList;
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      Map body = {};
      body['customer_id'] = customerID;
      final headers = await getHeaders();
      final http.Response response =
          await client.post(uri, headers: headers, body: body);
      var data = response.body;
      var jsonData = jsonDecode(data);
      print("Following Json ---> ${jsonData}");
      return FollowingModel.fromJson(jsonData);
    } on SocketException catch (e) {
      debugPrint("You Need Better Network Connection $e");
    } catch (e) {
      print("Following Res Error --> $e");
    }
  }

  /*********************** Blog **************************/
  // getBlog() async {
  //   String endPointUrl = "blog";
  //   final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
  //   try {
  //     final headers = await getHeaders();
  //     final http.Response response = await client.get(uri, headers: headers);
  //     debugPrint("Blog Data${response.statusCode}");
  //     if (response.statusCode == 200) {
  //       var jsonData = jsonDecode(response.body);
  //       // print("Blog Detail ====> $jsonData");
  //       return BlogModel.fromJson(jsonData);
  //     }
  //     return null;
  //   } catch (e) {
  //     // Log any errors that occur
  //     TLoggerHelper.debug(e.toString());
  //   }
  // }

  /*********************** Banner **************************/
  getBanner() async {
    String endPointUrl = UrlConstants.banner;
    return THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  /*********************** Jyotish Mall Category Slider **************************/
  Future getSpellSliderNew({required String id}) async {
    String endPointUrl = "${UrlConstants.slider}?product_category=$id";
    return await THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  // Future getSpellSlider(String id) async {
  //   String endPointUrl = "slider?product_category=$id";
  //   final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
  //   final http.Response response = await client.get(uri);
  //   debugPrint("Spell Data ${response.statusCode}");
  //   if (response.statusCode == 200) {
  //     var jsonData = jsonDecode(response.body);
  //     print("Spell Detail ====> $jsonData");
  //     return SpellSliderModel.fromJson(jsonData);
  //   }
  //   return null;
  // }

  /*********************** Mall Product Slider **************************/
  getMallProductSlider(String id) async {
    String endPointUrl = "slider?product_category=$id";
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      final headers = await getHeaders();
      final http.Response response = await client.get(uri, headers: headers);
      debugPrint("Product Slider Data ${response.statusCode}");
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print("Product Slider Detail ====> $jsonData");
        return MallProductSliderModel.fromJson(jsonData);
      }
      return null;
    } catch (e) {
      // Log any errors that occur
      TLoggerHelper.debug(e.toString());
    }
  }

  /*********************** Book a Pooja Slider **************************/
  getBookPooja() async {
    String endPointUrl = UrlConstants.getPoojasHome;
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      final headers = await getHeaders();
      final http.Response response = await client.get(uri, headers: headers);
      debugPrint("Pooja Data ${response.statusCode}");
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print("Book Pooja Detail ====> $jsonData");
        return BookPoojaModel.fromJson(jsonData);
      }
      return null;
    } catch (e) {
      // Log any errors that occur
      TLoggerHelper.debug(e.toString());
    }
  }

  /*********************** Newly Launched Slider **************************/
  // getNewlyLaunched() async {
  //   String endPointUrl = "newLaunch";
  //   final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
  //   try {
  //     final headers = await getHeaders();
  //     final http.Response response = await client.get(uri, headers: headers);
  //     debugPrint("Newly Launched Data ${response.body}");
  //     var jsonRes = jsonDecode(response.body);
  //     print("Json Response Of NewLaunch===> $jsonRes");
  //     return NewlyLaunchedModel.fromJson(jsonRes);
  //   } catch (e) {
  //     debugPrint("Error ------> $e");
  //   }
  // }

  Future getNewlyLauchedNew() async {
    String endPointUrl = UrlConstants.newlyLauched;
    return await THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  /********************** Gift Api *****************************/
  getGiftData(String customerID) async {
    String endPointUrl = "${UrlConstants.getGiftData}?user_id=$customerID";
    final Uri uri = Uri.parse(UrlConstants.baseUrl + endPointUrl);
    try {
      final headers = await getHeaders();
      final http.Response response = await client.get(uri, headers: headers);
      print("Response Body =========> ${response.body}");
      var giftRes = jsonDecode(response.body);
      print("Gift Response ====> $giftRes");
      return GiftApiModel.fromJson(giftRes);
    } catch (e) {
      print("Error----> $e");
    }
  }

  Future getProductList(String productId) async {
    const subUrl = "${UrlConstants.getAstrologerProductList}?product_id=";
    
    final Uri uri = Uri.parse('${UrlConstants.baseUrl}$subUrl$productId');
    final headers = await getHeaders();
    final http.Response response = await client.get(uri, headers: headers);
    final dynamic body = response.body;
    return body;
  }
//TODO:*****************************************************
  Future goLive() async {
    var endPointUrl = "astrologer-live";

    final Uri uri = Uri.parse('${UrlConstants.baseUrl}$endPointUrl');
    try {
      final headers = await getHeaders();
      final http.Response response = await client.get(uri, headers: headers);
      return response.body;
    } catch (e) {
      // Log any errors that occur
      TLoggerHelper.debug(e.toString());
    }
  }

  Future getReelsLiveList() async {
    var endPointUrl = UrlConstants.reelsLiveList;
    final Map<String, String> body = {
      "customer_id": await THelperFunctions.getUserId()
    };
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future fetchWaitListLiveUsers({required String astrologerId}) async {
    var endPointUrl = UrlConstants.waitListUserLive;
    final Map<String, String> body = {"astrologer_id": astrologerId};
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future getAllMessageList({required String liveId}) async {
    var endPointUrl = UrlConstants.getAllMessageList;
    Map<String, String> body = {"live_id": liveId};
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future endGoLive({required String id, required String liveId}) async {
    var endPointUrl = UrlConstants.goLive;
    Map<String, String> bodyPublic = {
      "user_id": await THelperFunctions.getUserId(),
      "is_online": "2",
      "live_id": liveId
    };
    Map<String, String> bodySession = {
      "user_id": await THelperFunctions.getUserId(),
      "is_online": "2",
      "id": id,
      "live_id": liveId
    };
    return THelperFunctions.httpPostHandler(
        client: client,
        endPointUrl: endPointUrl,
        body: id.isEmpty ? bodyPublic : bodySession);
  }

  Future sendLiveComment(
      {required String liveId, required String comment}) async {
    var endPointUrl = UrlConstants.sendLiveComment;
    Map<String, String> body = {
      "live_id": liveId,
      "comment": comment,
      "user_id": await THelperFunctions.getUserId()
    };
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future getGiftListData() async {
    String endPointUrl = UrlConstants.getGiftData;
    return THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  Future acceptCallStart(
      {required String communicationId, required String astrologerId}) async {
    var endPointUrl = UrlConstants.acceptCallStart;
    Map<String, String> body = {
      "communication_id": communicationId,
      "astrologer_id": astrologerId
    };
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future callDismiss({required String communicationId}) async {
    var endPointUrl = UrlConstants.callDismiss;
    Map<String, String> body = {
      "communication_id": communicationId,
      "user_type": "astrologer"
    };
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future sendGiftWallet(
      {required String astrologerId, required String giftId}) async {
    var endPointUrl = UrlConstants.giftWalletPayment;
    Map<String, String> body = {
      "user_id": await THelperFunctions.getUserId(),
      "astrologer_id": astrologerId,
      "gift_id": giftId
    };
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future sendCallLiveDuration(
      {required String communcationId,
      required int duration,
      required String liveId}) async {
    const endPointUrl = UrlConstants.endCallLive;
    final Map<String, String> body = {
      "communication_id": communcationId,
      "call_duration": duration.toString(),
      "live_id": liveId,
      "user_type": "customer"
    };
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future userEndCall({required String communicationId}) async {
    var endPointUrl = UrlConstants.endCall;
    final Map<String, String> body = {
      "communication_id": communicationId.toString(),
      // "call_duration": minutesCompleted,
      "user_type": "customer"
    };
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future astrologerDetailsLive({required String astrologerId}) async {
    var endPointUrl = UrlConstants.astrologerDetailsLive;
    final Map<String, String> body = {"astrologer_id": astrologerId};
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }
// /***************************** Jyotish Remedy Detail *****************************/
//
// Future customerRemedy({required String userID}) async {
//   String endPointUrl = "customer-remedy-list";
//
//   Map<String, String> body = {"user_id": await THelperFunctions.getUserId()};
//   TLoggerHelper.debug(endPointUrl);
//   TLoggerHelper.debug(body.entries.toString());
//   return THelperFunctions.httpPostHandler(
//       client: client, endPointUrl: endPointUrl, body: body);
// }
}
