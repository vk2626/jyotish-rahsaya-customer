import 'package:http/http.dart' as http;
import '../Key/api_keys.dart';
import '../helper_functions.dart';
import '../logger_helper.dart';

class KundliApi {
  final client = http.Client();

  Future addKundliDetails(
      {required String name,
      required String gender,
      required String dob,
      required String dobTime,
      required String pob,
      required String lat,
      required String lon,
      required String id,
      required String orderId,
      required String infoId}) async {
    var endPointUrl = "kundli-info";
    final Map<String, String> body = {
      "user_id": await THelperFunctions.getUserId(),
      "name": name,
      "gender": gender,
      "dob": dob,
      "dob_time": dobTime,
      "pob": pob,
      "lng": lon,
      "lat": lat,
    };
    if (id.isNotEmpty) {
      body['id'] = id;
    }
    if (orderId.isNotEmpty) {
      body['order_id'] = orderId;
    }
    if (infoId.isNotEmpty) {
      body['info_id'] = infoId;
    }
    return await THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future getAllKundliesList() async {
    var endPointUrl = "kundli-info-list-customer";
    final Map<String, String> body = {
      "user_id": await THelperFunctions.getUserId()
    };
    return await THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future deleteUserKundli({required String kundliId}) async {
    var endPointUrl = "kundli-info-list-delete";
    final Map<String, String> body = {"id": kundliId};
    return await THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future getPanchangDetails(
      {required String date,
      required String lat,
      required String lon,
      required String time,
      required String lang}) async {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/panchang/panchang?api_key=${TAPIKeys.vedicAstrologer}&date=$date&tz=5.5&lat=$lat&lon=$lon&time=$time&lang=$lang";
    TLoggerHelper.info(endPointUrl);
    return await THelperFunctions.httpGetHandlerNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getPlanetDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/horoscope/planet-details?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    TLoggerHelper.info(endPointUrl);
    return THelperFunctions.httpGetHandlerNormal(
      client: client,
      endPointUrl: endPointUrl,
      isUtf8: lang == 'en' ? false : true
    );
  }

  Future getAshtakVargaDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/horoscope/ashtakvarga?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    return THelperFunctions.httpGetHandlerNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getKPCuspsDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lon,
      required String lang}) {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/extended-horoscope/kp-houses?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    return THelperFunctions.httpGetHandlerNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getKPPlanetsDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lon,
      required String lang}) {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/extended-horoscope/kp-planets?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    return THelperFunctions.httpGetHandlerNormal(
      client: client,
      endPointUrl: endPointUrl,
      isUtf8: lang == 'en' ? false : true,
    );
  }

  Future getManglikAnalysis(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/dosha/manglik-dosh?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    return THelperFunctions.httpGetHandlerNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getKalsarpaAnalysis(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/dosha/kaalsarp-dosh?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    return THelperFunctions.httpGetHandlerNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getSadeSatiDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/extended-horoscope/sade-sati-table?dob=$dob&tob=$tob&lat=$lat&lon=$lon&tz=5.5&api_key=${TAPIKeys.vedicAstrologer}&lang=$lang";
    return THelperFunctions.httpGetHandlerNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getMatchMakingDetails({
    required String boyDob,
    required String boyTob,
    required String boyLat,
    required String boyLon,
    required String girlDob,
    required String girlTob,
    required String girlLat,
    required String girlLon,
  }) async {
    var endPointUrl =
        "https://api.vedicastroapi.com/v3-json/matching/ashtakoot?boy_dob=$boyDob&boy_tob=$boyTob&boy_tz=5.5&boy_lat=$boyLat&boy_lon=$boyLon&girl_dob=$girlDob&girl_tob=$girlTob&girl_tz=5.5&girl_lat=$girlLat&girl_lon=$girlLon&api_key=${TAPIKeys.vedicAstrologer}&lang=en";
    return THelperFunctions.httpGetHandlerNormal(
        client: client, endPointUrl: endPointUrl);
  }

  Future getChart(
      {required String lat,
      required String lon,
      required String dateTime,
      required String lang,
      required String chartType,
      bool isSouthIndian = false}) async {
    var endPointUrl =
        "https://api.prokerala.com/v2/astrology/chart?ayanamsa=5&coordinates=$lat,$lon&datetime=$dateTime%2B05:30&chart_type=$chartType&chart_style=${((isSouthIndian) ? ("south-indian") : ("north-indian"))}&format=svg&la=$lang";
    TLoggerHelper.info(endPointUrl);
    return THelperFunctions.httpGetHandlerProKeralaNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }

  Future getDashaData(
      {required String lat,
      required String lon,
      required String lang,
      required String dateTime}) async {
    var endPointUrl =
        "https://api.prokerala.com/v2/astrology/dasha-periods?ayanamsa=5&coordinates=$lat,$lon&datetime=$dateTime%2B05:30&la=$lang";
    TLoggerHelper.info(endPointUrl);
    return THelperFunctions.httpGetHandlerProKeralaNormal(
        client: client,
        endPointUrl: endPointUrl,
        isUtf8: lang == 'en' ? false : true);
  }
}
