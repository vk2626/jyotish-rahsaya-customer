import 'dart:convert'; // For utf8 encoding
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../logger_helper.dart';


class KundliApiService {
  static const String username = "633945"; // Your username
  static const String password =
      "09b2d7f96a34ec85c4154c74590ce7fc9046b0d2"; // Your password
  static const String url = 'https://json.astrologyapi.com/v1/';

  // Function for Basic Auth header
  static Map<String, String> get _basicAuthHeaders {
    String basicAuth =
        'Basic ${base64Encode(utf8.encode('$username:$password'))}';
    return {
      'Authorization': basicAuth,
      'Accept': 'application/json',
      //Add Language
      // "Accept-language": "hi"
    };
  }

  // GET handler with Basic Auth
  static Future<String?> httpGetHandlerNormal({
    required String endPointUrl,
    Map<String, dynamic>? queryParams,
    bool isUtf8 = false,
  }) async {
    try {
      Uri uri =
          Uri.parse(url + endPointUrl).replace(queryParameters: queryParams);

      final http.Response response =
          await http.Client().get(uri, headers: _basicAuthHeaders);

      // Log the response body (assuming TLoggerHelper is defined elsewhere)
      TLoggerHelper.info(response.body.toString());
      return isUtf8 ? utf8.decode(response.bodyBytes) : response.body;
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  // POST handler with Basic Auth
  static Future<String?> httpPostHandlerNormal(
      {required String endPointUrl,
      Map<String, dynamic>? body,
      bool isUtf8 = false,
      String lang = "en"}) async {
    try {
      final headers = _basicAuthHeaders;
      headers['Accept-Language'] =
          lang; // Correctly set the Accept-Language header
      final http.Response response = await http.Client().post(
        Uri.parse(url + endPointUrl),
        headers: headers,
        body: body, // Assuming the body is JSON
      );

      // Log the response body
      TLoggerHelper.info(_basicAuthHeaders.entries.toString());
      return isUtf8 ? utf8.decode(response.bodyBytes) : response.body;
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }
}
