import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import 'Constants.dart';


class HttpClient {
  static final HttpClient _singleton = HttpClient._internal();

  factory HttpClient() {
    return _singleton;
  }

  HttpClient._internal();
  final String baseUrl = UrlConstants.baseUrl;
  Future<Map<String, dynamic>?> getData(String endpoint) async {
    try {
      final response = await http.get(Uri.parse('$endpoint'));

      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        // Handle errors
        print('Error: ${response.statusCode}');
        print('Response body: ${response.body}');
        return null;
      }
    } catch (e) {
      // Handle exceptions
      print('Exception: $e');
      return null;
    }
  }
  Future<Map<String, dynamic>?> postData(String endpoint, Map<String, dynamic> data) async {
    try {
      final response = await http.post(
        Uri.parse('$endpoint'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(data),
      );

      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        // Handle errors
        print('Error: ${response.statusCode}');
        print('Response body: ${response.body}');
        return null;
      }
    } catch (e) {
      // Handle exceptions
      print('Exception: $e');
      return null;
    }
  }


}