import 'package:http/http.dart' as http;
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../../handler_classes/secure_storage.dart';
import '../helper_functions.dart';
import '../logger_helper.dart';
import 'Constants.dart';

class ChattingAPIs {
  final client = http.Client();

  Future sendMessage(
      {required String textBody,
      required String fromId,
      required String toId,
      required String imagePath,
      required String infoId,
      String? referenceChatId}) async {
    // var endPointUrl = "send-chat-message1";
    var endPointUrl = UrlConstants.sendMessage;

    Map<String, String> body = {
      "from_id": fromId,
      "to_id": toId,
      "body": textBody,
      "chat_info_id": infoId
    };

    if (referenceChatId != null) {
      body.addAll({"reference_id": referenceChatId});
    }

    // TLoggerHelper.info(body.entries.toString());
    if (imagePath.isNotEmpty) {
      TLoggerHelper.info(imagePath);
      var request = http.MultipartRequest(
          'POST', Uri.parse("${UrlConstants.baseUrl}$endPointUrl"));
      request.fields.addAll(body);
      // Set the authorization header
      request.headers['Accept'] = 'application/json';
      request.headers['Authorization'] =
          "Bearer ${await SecureStorage().getAccessToken(endPointUrl: endPointUrl)}";

      request.files
          .add(await http.MultipartFile.fromPath('attachment', imagePath));
      http.StreamedResponse response = await request.send();
      String responseBody = "";
      if (response.statusCode == 200) {
        responseBody = await response.stream.bytesToString();
        TLoggerHelper.info(responseBody);
      } else {
        print(response.reasonPhrase);
      }
      return responseBody;
    } else {
      return THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    }
  }

  // Future sendMessageNew(
  //     {required String textBody,
  //     required String fromId,
  //     required String toId,
  //     required String imagePath,
  //     required String infoId,
  //     String? referenceChatId}) async {
  //   var endPointUrl = "send-chat-message";

  //   Map<String, String> body = {
  //     "from_id": fromId,
  //     "to_id": toId,
  //     "body": textBody,
  //     "chat_info_id": infoId,
  //     "eventName": "customer",
  //     "channelName": "chat.info_id_$infoId"
  //   };

  //   if (referenceChatId != null) {
  //     body.addAll({"reference_id": referenceChatId});
  //   }

  //   // TLoggerHelper.info(body.entries.toString());
  //   if (imagePath.isNotEmpty) {
  //     TLoggerHelper.info(imagePath);
  //     var request = http.MultipartRequest(
  //         'POST', Uri.parse("${Constants.baseUrl}$endPointUrl"));
  //     request.fields.addAll(body);
  //     // Set the authorization header
  //     request.headers['Accept'] = 'application/json';
  //     request.headers['Authorization'] =
  //         "Bearer ${await SecureStorage().getAccessToken(endPointUrl: endPointUrl)}";

  //     request.files
  //         .add(await http.MultipartFile.fromPath('attachment', imagePath));
  //     http.StreamedResponse response = await request.send();
  //     String responseBody = "";
  //     if (response.statusCode == 200) {
  //       responseBody = await response.stream.bytesToString();
  //       TLoggerHelper.info(responseBody);
  //     } else {
  //       print(response.reasonPhrase);
  //     }
  //     return responseBody;
  //   } else {
  //     return THelperFunctions.httpPostHandler(
  //         client: client, endPointUrl: endPointUrl, body: body);
  //   }
  // }

  Future getMessages(
      {required String fromId,
      required String toId,
      required String infoId,
      required int pageNumber}) async {
    var endPointUrl =
        "${UrlConstants.getAllChatMessages}?to_id=$toId&from_id=$fromId&page=$pageNumber&chat_info_id=${int.parse(infoId)}";
    TLoggerHelper.debug(endPointUrl);
    return THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  Future getMessageFromID({required String chatId}) async {
    var endPointUrl = "${UrlConstants.getFirstMessageFromID}?id=$chatId";
    TLoggerHelper.debug(endPointUrl);
    return THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  /********************* Resume Chat Api ************************/
  Future resumeChat(
      {required String communicationid, required bool isResume}) async {
    const endPointUrl = UrlConstants.resumeChat;
    Map<String, dynamic> body = {};
    body['communication_id'] = communicationid;
    body['is_resume'] = isResume.toString();

    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future sendPoojaMessage(
      {required String textBody,
      required String fromId,
      required String toId,
      required String orderId,
      required bool isPooja,
      required String imagePath,
      String? referenceChatId}) async {
    var endPointUrl = UrlConstants.sendPoojaMessage;

    Map<String, String> body = {
      "from_id": fromId,
      "to_id": toId,
      "body": textBody,
    };
    if (isPooja) {
      body['order_pooja_id'] = orderId;
    } else {
      body['remedy_order_id'] = orderId;
      body['message_type'] = "remedy";
    }
    TLoggerHelper.info(" infoId----->$orderId");
    if (referenceChatId != null) {
      body.addAll({"reference_id": referenceChatId});
    }

    TLoggerHelper.info(body.entries.toString());
    if (imagePath.isNotEmpty) {
      TLoggerHelper.info(imagePath);
      var request = http.MultipartRequest(
          'POST', Uri.parse("${UrlConstants.baseUrl}$endPointUrl"));
      request.fields.addAll(body);
      request.files
          .add(await http.MultipartFile.fromPath('attachment', imagePath));
      http.StreamedResponse response = await request.send();
      String responseBody = "";
      if (response.statusCode == 200) {
        responseBody = await response.stream.bytesToString();
        TLoggerHelper.info(responseBody);
      } else {
        TLoggerHelper.error(response.reasonPhrase.toString());
      }
      return responseBody;
    } else {
      return THelperFunctions.httpPostHandler(
          client: client, endPointUrl: endPointUrl, body: body);
    }
  }

  Future getPoojaMessages(
      {required String fromId,
      required String toId,
      required String orderId,
      required bool isPooja,
      required int pageNumber}) async {
    var endPointUrl = isPooja
        ? "${UrlConstants.getPoojaOrRemedyMessages}?to_id=$toId&from_id=$fromId&page=$pageNumber&order_pooja_id=$orderId"
        : "${UrlConstants.getPoojaOrRemedyMessages}?to_id=$toId&from_id=$fromId&page=$pageNumber&remedy_order_id=$orderId";
    TLoggerHelper.debug(endPointUrl);
    return THelperFunctions.httpGetHandler(
        client: client, endPointUrl: endPointUrl);
  }

  Future sendSeenStatus(
      {required String fromId,
      required String toId,
      required String chatInfoId}) async {
    var endPointUrl = UrlConstants.sendMessageSeen;
    final Map<String, String> body = {
      "from_id": fromId,
      "to_id": toId,
      "chat_info_id": chatInfoId
    };
    TLoggerHelper.debug(body.entries.toString());
    return await THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  // Future sendSeenStatusNew(
  //     {required String fromId,
  //     required String toId,
  //     required String chatInfoId}) async {
  //   var endPointUrl = "seen-message";
  //   final Map<String, String> body = {
  //     "from_id": fromId,
  //     "to_id": toId,
  //     "chat_info_id": chatInfoId,
  //     "user_type": "customer",
  //     "channelName": "chat.info_id_$chatInfoId",
  //     "eventName": "seen"
  //   };
  //   TLoggerHelper.debug(body.entries.toString());
  //   return await THelperFunctions.httpPostHandler(
  //       client: client, endPointUrl: endPointUrl, body: body);
  // }

  Future sendSeenStatusPooja(
      {required String fromId,
      required String toId,
      required bool isPooja,
      required String orderId}) async {
    var endPointUrl = UrlConstants.sendMessageSeenPooja ;
    final Map<String, String> body = {
      "from_id": fromId,
      "to_id": toId,
      // "order_pooja_id": poojaId
    };
    if (isPooja) {
      body['order_pooja_id'] = orderId;
    } else {
      body['remedy_order_id'] = orderId;
      body['message_type'] = "remedy";
    }
    TLoggerHelper.debug(body.entries.toString());
    return await THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  Future isNotAttended({required String communicationId}) async {
    var endPointUrl = UrlConstants.recall;
    Map<String, String> body = {"communication_id": communicationId};
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  // Future endChatByAstrologer({required String communicationId}) async {
  //   var endPointUrl = "user-end-chat";
  //   final Map<String, String> body = {
  //     "communication_id": communicationId,
  //     "user_type": "customer"
  //   };
  //   TLoggerHelper.debug(body.entries.toString());
  //   return THelperFunctions.httpPostHandler(
  //       client: client, endPointUrl: endPointUrl, body: body);
  // }

  Future sendChatDuration({required String communicationId}) async {
    var endPointUrl = UrlConstants.chatEnd;
    final Map<String, dynamic> body = {
      "communication_id": communicationId,
      "user_type": "customer"
    };
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  // Future sendChatDurationNew(
  //     {required String communicationId, required String infoId}) async {
  //   var endPointUrl = "user-end-chat-new";
  //   final Map<String, dynamic> body = {
  //     "communication_id": communicationId,
  //     "user_type": "customer",
  //     "channelName": "chat.info_id_${infoId}",
  //     "eventName": "chat_end"
  //   };
  //   TLoggerHelper.debug(body.entries.toString());
  //   return THelperFunctions.httpPostHandler(
  //       client: client, endPointUrl: endPointUrl, body: body);
  // }

  Future sendChatDurationResume(
      {required String communicationId,
      required bool isResume,
      required String chatDuration}) async {
    var endPointUrl = UrlConstants.communicationRequestResume;
    final Map<String, dynamic> body = {
      "communication_id": communicationId,
      "is_resume": isResume.toString(),
      "chat_duration": chatDuration
    };
    TLoggerHelper.debug(body.entries.toString());
    return THelperFunctions.httpPostHandler(
        client: client, endPointUrl: endPointUrl, body: body);
  }

  // Future sendChatDurationResumeNew(
  //     {required String communicationId,
  //     required bool isResume,
  //     required String chatDuration}) async {
  //   var endPointUrl = "communication-request-resume";
  //   final Map<String, dynamic> body = {
  //     "communication_id": communicationId,
  //     "is_resume": isResume.toString(),
  //     "chat_duration": chatDuration,
  //     "channelName": "chat.info_id_${communicationId}",
  //     "eventName": "resume"
  //   };
  //   TLoggerHelper.debug(body.entries.toString());
  //   return THelperFunctions.httpPostHandler(
  //       client: client, endPointUrl: endPointUrl, body: body);
  // }
}
