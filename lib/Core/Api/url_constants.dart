import '../firebaseremoteconfig_service.dart';

class UrlConstants {
  static final _remoteConfig = FirebaseRemoteConfigService();

  static late final String baseUrl_login = _getBaseUrlOld();
  static late final String baseUrl = _getBaseUrl();

  static String _getBaseUrlOld() {
    try {
      final loginUrl = _remoteConfig.getString('login_url');
      return loginUrl.isNotEmpty
          ? loginUrl
          : 'https://admin.jyotishrahsya.com/api/';
    } catch (e) {
      print("Error fetching login_url: $e");
      return 'https://admin.jyotishrahsya.com/api/';
    }
  }

  static String _getBaseUrl() {
    try {
      final baseUrl = _remoteConfig.getString('base_url');
      return baseUrl.isNotEmpty
          ? baseUrl
          : 'https://admin.jyotishrahsya.com/api/auth/';
    } catch (e) {
      print("Error fetching base_url: $e");
      return 'https://admin.jyotishrahsya.com/api/auth/';
    }
  }

  static const String loginUrl = "customer-otp";
  static const String verifyOTP = "verify-otp";
  static const String sendMessage = "send-message";
  static const String getAllChatMessages = "customer-astrologer-chat";
  static const String getFirstMessageFromID = "customer-astrologer-chat-first";
  static const String resumeChat = "communication-request-resume";
  static const String sendPoojaMessage = "send-pooja-message";
  static const String getPoojaOrRemedyMessages =
      "pooja-customer-astrologer-chat";
  static const String sendMessageSeen = "send-seen";
  static const String sendMessageSeenPooja = "send-pooja-seen";
  static const String recall = "recall";
  static const String chatEnd = "user-end-chat-new1";
  static const String communicationRequestResume =
      "communication-request-resume-new";
  static const String recentOrderChatList =
      "user-chat-complete-list-by-astrologer-new";
  static const String similarAstrologerlist = "astrologer-similar-list-new1";
  static const String blog = "blog";
  static const String jyotishMallCategory = "jyotish-mall-category";
  static const String astrologerCategory = "astrologer-category";
  static const String slider = "slider";
  static const String celebrityCustomer = "celebrity-customer";
  static const String astrologerProfile = "astrologer-detail-new";
  static const String getProductDetails = "product-details-newly";
  static const String blockAstrologer = "astrologer-block";
  static const String getPoojaDetails = "book-a-pooja-details";
  static const String panditDetails = "pandit-details";
  static const String astrologerChatCallList = "astrologer-new";
  static const String recharge = "recharge";
  //TODO:Enable "payment-new" when doing actual payment
  static const String payment = "payment-new";

  static const String giftWalletPayment = "gift-wallet-payment";
  static const String createOrderPooja = "create-order-pooja-payment";
  static const String createOrderProduct = "create-order-product-payment";
  static const String orderPaymentComplete = "order-payment";
  static const String walletTranscation = "wallet-transaction";
  static const String search = "search";
  static const String follower = "follower";
  static const String userEndCall = "user-end-call";
  static const String sendReview = "send-review";
  static const String acceptCustomerStartChat = 'accept-customer-chat-start';
  static const String mallItemDetails = "category-product-details-new";
  static const String followerList = "follower-list";
  static const String banner = "banner";
  static const String getPoojasHome = "book-a-pooja";
  static const String newlyLauched = "newLaunch";
  static const String getGiftData = "gift";
  static const String getAstrologerProductList = "astrologer-product-list";
  static const String reelsLiveList = "slide-astrologer-live-new";
  static const String waitListUserLive = "wait-list-user-live";
  static const String getAllMessageList = "comment-live-list";
  static const String goLive = "go-live";
  static const String sendLiveComment = "comment-live";
  static const String acceptCallStart = "accept-customer-start-call-new";
  static const String callDismiss = "user-dismiss";
  static const String endCallLive = "user-end-call-live";
  static const String endCall = "user-end-call-new";
  static const String astrologerDetailsLive = "astrologer-price-list-live-call";
  static const String rejectCall = "reject-call";
  static const String showTyping = "typing";
  static const String sendCustomerOnlineOffline = "customer-online-offline";
  static const String waitChatList = "wait-chat-list-new";
  static const String wallet = "wallet";
  static const String getSuggestedPoojaOrderList = "customer-pooja-list-new";
  static const String getPoojaOrderList = "customer-order-book-a-pooja-list";
  static const String walletAmountTrasaction = "wallet-amount-transaction";
  static const String order = "order-new";
  static const String astrologerLive = "astrologer-new";
  static const String submitChatIntakeForm = "user-info";
  static const String submitChatIntakeFormLive = "user-info-live";
  static const String submitChatIntakeFormRandom = "random-chat-by-customer";
  static const String submitChatIntakeFormUsers = "user-info-list";
  static const String recentSearch = "recent-search";
  static const String astrologerRecentList = "astrologer-recent-list";
  static const String profileCustomerDetail = "profile-customer-detail";
  static const String updateProfileCustomer = "update-profile-customer";
  static const String cancelChatRequest = "cancel-chat-request";
  static const String skills = "skill?type=all";
  static const String language = "language";
  static const String qualification = "qualification";
  static const String incomeSource = "source-income";
  static const String userLogout = 'user-logout';
  static const String paymentLog = "payment-log";
  static const String astroMallOrder = "astro-mall-order-new";
  static const String remedyDetails = "customer-remedy-list-new";
  static const String remedyPurchasedDetails =
      "customer-remedy-purchased-list-new";
}
