// static String baseUrl_old =
//     "https://jyotish.techsaga.live/api/"; //old api base url without auth

//   static String baseUrl_old = kReleaseMode
//       ? "https://jyotish.techsaga.live/api/"
//       : "https://admin.jyotishrahsya.com/api/"; //old api base url without auth

//   // static String baseUrl = "https://jyotish.techsaga.live/api/auth/";
//   static String baseUrl = kReleaseMode
//       ? "https://jyotish.techsaga.live/api/auth/"
//       : "https://admin.jyotishrahsya.com/api/auth/";
import 'package:flutter/foundation.dart';

import '../firebaseremoteconfig_service.dart';

class Constants {
  // // Singleton instance of FirebaseRemoteConfigService
  // static final _remoteConfig = FirebaseRemoteConfigService();

  // // Lazy-loaded static URLs with error handling
  // static late final String baseUrl_old = _getBaseUrlOld();
  // static late final String baseUrl = _getBaseUrl();

  // // Private helper methods for lazy initialization
  // static String _getBaseUrlOld() {
  //   try {
  //     final loginUrl = _remoteConfig.getString('login_url');
  //     return loginUrl.isNotEmpty
  //         ? loginUrl
  //         : 'https://admin.jyotishrahsya.com/api/'; // Fallback value
  //   } catch (e) {
  //     print("Error fetching login_url: $e");
  //     return 'https://admin.jyotishrahsya.com/api/'; // Fallback on error
  //   }
  // }

  // static String _getBaseUrl() {
  //   try {
  //     final baseUrl = _remoteConfig.getString('base_url');
  //     return baseUrl.isNotEmpty
  //         ? baseUrl
  //         : 'https://admin.jyotishrahsya.com/api/auth/'; // Fallback value
  //   } catch (e) {
  //     print("Error fetching base_url: $e");
  //     return 'https://admin.jyotishrahsya.com/api/auth/'; // Fallback on error
  //   }
  // }

  // static String generateOTP = '${baseUrl_old}generate-otp';
  // static String verifyOTP = '${baseUrl_old}verify-otp';
  // static String skill = '${baseUrl}skill?type=';
  // static String language = '${baseUrl}language';
  // static String qualification = '${baseUrl}qualification';
  // static String incomeSource = '${baseUrl}source-income';
  // static String registration = '${baseUrl}registration-customer';
  // static String reviews = '${baseUrl}review?astrologer_id=';
  // // static String chat = '${baseUrl}customer-astrologer-chat?from_id=';
  // static String sendMessage = '${baseUrl}send-message';
  // static String order = '${baseUrl}order-new';
  // static String walletTransaction = '${baseUrl}wallet-transaction';

  // static String submitChatIntakeForm = '${baseUrl}user-info';
  // static String submitChatIntakeFormLive = '${baseUrl}user-info-live';
  // static String submitChatIntakeFormUsers = '${baseUrl}user-info-list';
  // static String updateProfile = '${baseUrl}update-profile-customer';
  // static String liveAstrologer = '${baseUrl}astrologer-live';
  // static String profileDetail = '${baseUrl}profile-customer-detail';
  // static String recentSearch = '${baseUrl}recent-search';
  // static String paymentLog = '${baseUrl}payment-log';
  // static String skills = '${baseUrl}skill?type=all';
  // static String filter = '${baseUrl}filter';

  // //TODO: Always enable the "create-order-product-payment"
  // static String createOrder = '${baseUrl}create-order-product-payment';
  // // static String createOrder = '${baseUrl}create-order-product-payment-test';

  // //TODO: Always enable the "payment-new" for production
  // static String payment = '${baseUrl}payment-new';
  // // static String payment = '${baseUrl}payment-test';

  // //TODO: Always enable the "create-order-pooja-payment"
  // static String createOrderPoojaProductSuggested =
  //     '${baseUrl}create-order-pooja-payment';
  // // static String createOrderPoojaProductSuggested =
  // //     '${baseUrl}create-order-pooja-payment-test';

  // static String astroMallOrders = '${baseUrl}astro-mall-order-new';
  // static String deleteRecentSearch = '${baseUrl}recent-delete';
  // static String recentList = '${baseUrl}astrologer-recent-list';
  // static String block_unblock = '${baseUrl}astrologer-block';
  // static String unblock = '${baseUrl}astrologer-unblock';
  // static String cancelChatRequest = '${baseUrl}cancel-chat-request';
  // static String waitList = '${baseUrl}wait-chat-list-new';
  // static String astroDetails = '${baseUrl}astrologer-detail-new';
  // static String remedyDetails = '${baseUrl}customer-remedy-list-new';
  // static String remedyPurchasedDetails =
  //     '${baseUrl}customer-remedy-purchased-list-new';

  static String fullAttendanceAccess = "is_full_attendance_access";
  static String fullName = "fullname";
  static String userID = "id";
  static String isFreeFlag = "isFreeFlag";
  static String mobile = "mobile";
  static String tempUserID = "tempUserID";
  static String profileImage = "profileImage";
  static String firstName = "firstName";
  static String lastName = "lastName";
  static String email = "email";
  static String address = "location";
  static String latitude = "latitude";
  static String longitude = "longitude";
  static String officeLocation = "officeLocation";

  static String isCalling = "isCalling";

  static String languageId = "languageId";
  static String primarySkill = "primarySkill";
  static String allSkill = "allSkill";
  static String maritalStatus = "maritalStatus";
  static String otherPlatform = "otherPlatform";
  static String gender = "gender";
  static String whereDidYou = "whereDidYou";
  static String dailyHour = "dailyHour";
  static String experience = "experience";
  static String dob_time = "dob_time";
  static String dob_place = "dob_place";
  static String dob = "dob";
  static String city = "city";
  static String user_address = "address";
  static String state = "state";
  static String country = "country";
  static String pincode = "pincode";
  static String current_address = "current_address";

  static String reasonToOnboard = "reasonToOnboard";
  static String bio = "bio";
  static String minEarning = "minEarning";
  static String website = "website";
  static String youtube = "youtube";
  static String linkedin = "linkedin";
  static String facebook = "facebook";
  static String instagram = "instagram";
  static String learnAstrology = "learnAstrology";
  static String qualificationId = "qualificationId";
  static String incomeSourceID = "incomeSourceID";
  static String isReferred = "isReferred";

  static String challenge = "challenge";
  static String travel = "travel";
  static String working = "working";
  static String currency = "₹ ";
  static String CurrentScreen = "CurrentScreen";
  static Map<String, String> zodiacSignsList = {
    "aquarius": "asset/images/aquarius.png",
    "aries": "asset/images/aries.png",
    "cancer": "asset/images/cancer.png",
    "capricorn": "asset/images/capricorn.png",
    "gemini": "asset/images/gemini.png",
    "leo": "asset/images/leo.png",
    "libra": "asset/images/libra.png",
    "pisces": "asset/images/pisces.png",
    "sagatarius": "asset/images/sagatarius.png",
    "scorpio": "asset/images/scorpio.png",
    "taurus": "asset/images/taurus.png",
    "virgo": "asset/images/virgo.png"
  };
}
