import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../utils/AppColor.dart';

class TimerNotifier extends ChangeNotifier {
  final int timerMaxSeconds = 60;
  int currentSeconds = 0;
  late Timer _timer;

  String get timerText =>
      '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')}: ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  final Duration interval = const Duration(seconds: 1);

  void startTimeout(BuildContext context) {
    _timer = Timer.periodic(interval, (timer) {
      currentSeconds = timer.tick;
      if (timer.tick >= timerMaxSeconds) {
        timer.cancel();
        _ratingQuestionDialog(context);
      }
      notifyListeners();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

/***************************** Rating Question ******************************/
  Future<void> _ratingQuestionDialog(BuildContext context1) async {
    await showDialog(
      context: context1,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: AppColor.whiteColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(

              width: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Align(alignment: Alignment.topRight, child: CloseButton()),
                  Container(
                      height: 60,
                      width: 70,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("asset/images/person.png")),
                          shape: BoxShape.circle,
                          border: Border.all(color: AppColor.appColor))),
                  SizedBox(height: 10),
                  Text(
                    'Astrologer Name',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),
                  const Text(
                      'Would you like to write a few words about Astrologer Name on PlatStore?',
                      textAlign: TextAlign.center),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 100,
                        child: ElevatedButton(
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            backgroundColor: AppColor.appBackgroundLightGrey,
                            textStyle: Theme.of(context).textTheme.labelLarge,
                          ),
                          child: const Text('Cancel'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      SizedBox(
                        width: 100,
                        child: ElevatedButton(
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            backgroundColor: AppColor.appColor,
                            textStyle: Theme.of(context).textTheme.labelLarge,
                          ),
                          child: const Text('Yes'),
                          onPressed: () {
                            Navigator.pop(context);
                            _ratingDialog(context);
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  /********** Feedback Dialog Box **********/
  Future<void> _ratingDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Dialog(
            backgroundColor: AppColor.whiteColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(alignment: Alignment.topRight, child: CloseButton()),
                Text(
                  "Rate {User Name}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Container(
                    height: 70,
                    width: 80,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("asset/images/person.png")),
                        shape: BoxShape.circle,
                        border: Border.all(color: AppColor.appColor))),
                SizedBox(height: 10),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                            border: Border.all(color: AppColor.appColor),
                            image:
                                DecorationImage(image: AssetImage("assetName")),
                            shape: BoxShape.circle),
                      ),
                    ),
                    RichText(
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                          text: "User Name\n",
                          style:
                              TextStyle(fontSize: 11, color: AppColor.darkGrey),
                          children: <TextSpan>[
                            TextSpan(
                                text:
                                    "Review are public if you are not anonymous",
                                style: TextStyle(color: Colors.red)),
                          ]),
                    )
                  ],
                ),
                CheckboxListTile(
                  controlAffinity: ListTileControlAffinity.leading,
                  value: true,
                  onChanged: (value) {},
                  title: Text(
                    "Hide my name from all public reviews",
                    style: TextStyle(fontSize: 13),
                  ),
                ),
                const Text(
                    'Would you like to write a few words about Astrologer Name on PlatStore?',
                    textAlign: TextAlign.center),
                RatingBarIndicator(
                    itemCount: 5,
                    itemSize: 25.0,
                    itemBuilder: (context, _) => const Icon(
                          Icons.star,
                        )),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    height: 90, // set desired height here
                    child: TextFormField(
                      maxLength: 160,
                      maxLengthEnforcement: MaxLengthEnforcement.enforced,
                      decoration: InputDecoration(
                        labelText: 'Describe your experience (Optional)',
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColor.borderColor)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 250,
                  child: ElevatedButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      backgroundColor: AppColor.appColor,
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Submit'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      _promotingDialog(context);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /********************* Promoting Rating ************************/
  Future<void> _promotingDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Dialog(
            insetPadding: EdgeInsets.all(2),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            backgroundColor: AppColor.whiteColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Align(alignment: Alignment.topRight, child: CloseButton()),
                  const Text(
                    'How likely are you to promote the expert?',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: 250,
                    child: Column(
                      children: <Widget>[
                        RatingBar.builder(
                            glowColor: AppColor.whiteColor,
                            itemCount: 10,
                            itemSize: 25,
                            itemBuilder: (context, index) {
                              switch (index) {
                                case 0:
                                  return Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.orangeAccent,
                                    ),
                                  );
                                case 1:
                                  return Text(
                                    "2",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.orangeAccent,
                                    ),
                                  );
                                case 2:
                                  return Text(
                                    "3",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.orangeAccent,
                                    ),
                                  );
                                case 3:
                                  return Text(
                                    "4",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.orangeAccent,
                                    ),
                                  );
                                case 4:
                                  return Text(
                                    "5",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.orangeAccent,
                                    ),
                                  );
                                case 5:
                                  return Text(
                                    "6",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.orangeAccent,
                                    ),
                                  );
                                case 6:
                                  return Text(
                                    "7",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.lightBlueAccent,
                                    ),
                                  );
                                case 7:
                                  return Text(
                                    "8",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.lightBlueAccent,
                                    ),
                                  );
                                case 8:
                                  return Text(
                                    "9",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.greenAccent,
                                    ),
                                  );
                                case 9:
                                  return Text(
                                    "10",
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.greenAccent,
                                    ),
                                  );
                                default:
                                  return Container();
                              }
                            },
                            onRatingUpdate: (rating) {}),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Least Likely"),
                            Text("Most Likely"),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    "The data is confidential & your rating is not shown to the astrologer",
                    style: TextStyle(fontSize: 8),
                  ),
                  SizedBox(
                    width: 250,
                    child: ElevatedButton(
                      style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        backgroundColor: AppColor.appColor,
                        textStyle: Theme.of(context).textTheme.labelLarge,
                      ),
                      child: const Text('Submit'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
