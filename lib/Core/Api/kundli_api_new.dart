import 'dart:convert';

import 'package:flutter/material.dart';

import 'kundli_api_service.dart';

class KundliApiNew {
  Future getBasicDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "birth_details";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5", // You can change this to a dynamic value if needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getPanchangDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "astro_details";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5", // You can change this to a dynamic value if needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getKPPlanets(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "kp_planets";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5", // You can change this to a dynamic value if needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getKPHouses(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "kp_house_cusps";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5", // You can change this to a dynamic value if needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getBhavChalitChart(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "kp_house_cusps";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5", // You can change this to a dynamic value if needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getAshtakvargaChart(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String planetName,
      required String long}) async {
    final String endPointUrl = "planet_ashtak/:$planetName";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getGeneralDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required String type}) async {
    String endPointUrl = "";
    if (type == "nakshatra") {
      endPointUrl = "general_nakshatra_report";
    } else if (type == "ascendant") {
      endPointUrl = "general_ascendant_report";
    }
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5", // You can change this to a dynamic value if needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getRemediesDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required String type}) async {
    String endPointUrl = "";
    if (type == "rudraksha") {
      endPointUrl = "rudraksha_suggestion";
    } else if (type == "gemstone") {
      endPointUrl = "basic_gem_suggestion";
    }
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getManglikDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "manglik";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getKalsarpDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "kalsarpa_details";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getSadesatiDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "sadhesati_current_status";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getChartPlanetsDoshaReportDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    const String endPointUrl = "planets";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getChartLagna(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required bool isNorth}) async {
    const String endPointUrl = "horo_chart_image/D1";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5",
      "planetColor": "#000000",
      "signColor": "#000000",
      "lineColor": "#000000",
      "chartType": isNorth ? "north" : "south"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getChartNavasma(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required bool isNorth}) async {
    const String endPointUrl = "horo_chart_image/D9";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5",
      "planetColor": "#000000",
      "signColor": "#000000",
      "lineColor": "#000000",
      "chartType": isNorth ? "north" : "south"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getDivisionalChart(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required String type,
      required bool isNorth}) async {
    String endPointUrl = "horo_chart_image/$type";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5",
      "planetColor": "#000000",
      "signColor": "#000000",
      "lineColor": "#000000",
      "chartType": isNorth ? "north" : "south"
    };
    return await KundliApiService.httpPostHandlerNormal(
        lang: lang, endPointUrl: endPointUrl, body: body);
  }

  Future getDashaDetails({
    required DateTime dateTime,
    required String lat,
    required String lang,
    required String long,
  }) async {
    String endPointUrl = "current_vdasha_all";
    final Map<String, dynamic> body = {
      "day": dateTime.day.toString(),
      "month": dateTime.month.toString(),
      "year": dateTime.year.toString(),
      "hour": dateTime.hour.toString(),
      "min": dateTime.minute.toString(),
      "lat": lat,
      "lon": long,
      "tzone": "5.5"
    };
    return await KundliApiService.httpPostHandlerNormal(
        endPointUrl: endPointUrl, body: body, lang: lang);
  }

  Future getMatchMakingResult(
      {required DateTime boyDOB,
      required TimeOfDay boyTOB,
      required String boyLat,
      required String boyLong,
      required DateTime girlDOB,
      required TimeOfDay girlTOB,
      required String girlLat,
      required String girlLong}) async {
    String endPointUrl = "match_making_detailed_report";
    final Map<String, String> body = {
      "m_day": boyDOB.day.toString(),
      "m_month": boyDOB.month.toString(),
      "m_year": boyDOB.year.toString(),
      "m_hour": boyTOB.hour.toString(),
      "m_min": boyTOB.minute.toString(),
      "m_lat": boyLat,
      "m_lon": boyLong,
      "m_tzone": "5.5", // Example timezone, update as needed
      "f_day": girlDOB.day.toString(),
      "f_month": girlDOB.month.toString(),
      "f_year": girlDOB.year.toString(),
      "f_hour": girlTOB.hour.toString(),
      "f_min": girlTOB.minute.toString(),
      "f_lat": girlLat,
      "f_lon": girlLong,
      "f_tzone": "5.5" // Example timezone, update as needed
    };
    return await KundliApiService.httpPostHandlerNormal(
        endPointUrl: endPointUrl, body: body);
  }
}
