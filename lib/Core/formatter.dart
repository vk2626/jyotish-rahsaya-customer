import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'logger_helper.dart';

class TFormatter {
  static String? getYouTubeVideoId(String url) {
    // Find the index of '?' in the URL
    int questionMarkIndex = url.indexOf('?');

    // Extract substring from index 17 (start of video ID) to just before the '?'
    String substring = url.substring(
        17, questionMarkIndex != -1 ? questionMarkIndex : url.length);

    // TLoggerHelper.debug(substring);
    return substring;
  }

  static String formatDate(DateTime dateTime) {
    // Define the date format to match your desired output
    DateFormat dateFormat = DateFormat('dd-MMM-yyyy');

    // Format the DateTime object into a string
    String formattedDate = dateFormat.format(dateTime);

    return formattedDate;
  }

  
  static String convertToReadableFormat(String text) {
    // Split the string by underscores, capitalize each word, and join them with spaces
    return text
        .split('_') // Split by underscore
        .map((word) =>
            word[0].toUpperCase() +
            word.substring(1)) // Capitalize first letter
        .join(' '); // Join with space
  }

  static String convertTo12HourFormat(String timeString) {
    // Parse the time string into a DateTime object (ignoring date part)
    DateTime parsedDate = DateFormat('HH:mm').parse(timeString);

    // Format the DateTime object into 12-hour format string
    String formattedTime = DateFormat('h:mm a').format(parsedDate);

    return formattedTime;
  }

  static String convertTo24HourFormat(String time) {
    // Split the time into components
    final parts = time.split(' ');
    if (parts.length != 2) {
      throw FormatException('Invalid time format');
    }

    // Further split the time and period (AM/PM)
    final timeParts = parts[0].split(':');
    if (timeParts.length != 2) {
      throw FormatException('Invalid time format');
    }

    int hour = int.parse(timeParts[0]);
    final minute = timeParts[1];

    // Convert to 24-hour format
    if (parts[1].toUpperCase() == 'PM' && hour != 12) {
      hour += 12;
    } else if (parts[1].toUpperCase() == 'AM' && hour == 12) {
      hour = 0;
    }

    return '${hour.toString().padLeft(2, '0')}:$minute';
  }

  static String parseAndFormatDate(String dateString) {
    // Parse the date string into a DateTime object
    DateTime parsedDate = DateFormat('dd MMMM yyyy').parse(dateString);

    // Format the DateTime object into "dd/MM/yyyy" format
    String formattedDate = DateFormat('dd/MM/yyyy').format(parsedDate);

    return formattedDate;
  }

  static String formatTimeOfDay(TimeOfDay timeOfDay) {
    // Extract hour and minute from TimeOfDay object
    final hour = timeOfDay.hour;
    final minute = timeOfDay.minute;

    // Determine AM or PM
    final isPM = hour >= 12;
    final formattedHour =
        (hour % 12 == 0) ? 12 : hour % 12; // Convert to 12-hour format
    final formattedMinute = minute.toString().padLeft(2, '0');
    final amPm = isPM ? 'PM' : 'AM';

    // Combine hour, minute, and AM/PM into a formatted time string
    return '${formattedHour.toString().padLeft(2, '0')}:$formattedMinute $amPm';
  }

  static TimeOfDay parseTimeString(String timeString) {
    List<String> parts = timeString.split(' '); // Split timeString by space

    String timePart = parts[0]; // '7:45'
    String periodPart = parts[1]; // 'AM' or 'PM'

    List<String> timeParts = timePart.split(':'); // Split timePart by colon
    int hour = int.parse(timeParts[0]); // Get hour part and convert to integer
    int minute =
        int.parse(timeParts[1]); // Get minute part and convert to integer

    // Adjust hour based on period (AM or PM)
    if (periodPart == 'PM' && hour < 12) {
      hour += 12; // Convert PM hour to 24-hour format
    } else if (periodPart == 'AM' && hour == 12) {
      hour = 0; // Convert 12 AM to 0 hour in 24-hour format
    }

    // Create TimeOfDay object
    TimeOfDay timeOfDay = TimeOfDay(hour: hour, minute: minute);

    return timeOfDay;
  }
 static TimeOfDay parseDobTime(String dobTime) {
  // Use DateFormat from intl package if necessary for complex formats
  final timeParts = dobTime.split(" ");
  final isPm = timeParts[1].toUpperCase() == "PM";
  final hourMinute = timeParts[0].split(":");

  int hour = int.parse(hourMinute[0]);
  int minute = int.parse(hourMinute[1]);

  // Adjust for PM time (not 12 PM or 12 AM)
  if (isPm && hour != 12) {
    hour += 12;
  } else if (!isPm && hour == 12) {
    hour = 0;
  }

  return TimeOfDay(hour: hour, minute: minute);
}

  static String formatLiveCount(int count) {
    if (count < 1000) {
      return count.toString();
    } else if (count < 10000) {
      // Display as X.Xk
      double kValue = count / 1000;
      return "${kValue.toStringAsFixed(1)}k";
    } else {
      // Display as Xk
      return "${(count / 1000).toStringAsFixed(0)}k";
    }
  }

  static String formatDegree(double degree) {
    int deg = degree.floor();
    double minDouble = (degree - deg) * 60;
    int min = minDouble.floor();
    double secDouble = (minDouble - min) * 60;
    int sec = secDouble.round();

    return '$deg° $min\' $sec"';
  }

  static String formatTimeOfDayNew(TimeOfDay timeOfDay) {
    final now = DateTime.now();
    final DateTime dateTime = DateTime(
        now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
    return DateFormat.jm().format(dateTime);
  }

  static String formatTimeFromMinutes(int minutes) {
    int totalSeconds = minutes * 60;
    Duration duration = Duration(seconds: totalSeconds);
    String twoDigits(int n) => n.toString().padLeft(2, '0');

    String minutesPart = twoDigits(duration.inMinutes.remainder(60));
    String secondsPart = twoDigits(duration.inSeconds.remainder(60));

    return '${duration.inMinutes}m $secondsPart s';
  }

  static String formatTimeFromDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');

    String minutesPart = twoDigits(duration.inMinutes.remainder(60));
    String secondsPart = twoDigits(duration.inSeconds.remainder(60));

    return '${duration.inMinutes}m ${secondsPart}s';
  }

  static String capitalize(String text) {
    if (text.isEmpty) return "";
    return "${text[0].toUpperCase()}${text.substring(1).toLowerCase()}";
  }

  static String capitalizeSentence(String sentence) {
    List<String> words = sentence.split(' ');

    List<String> capitalizedWords = words.map((word) {
      if (word.isEmpty) {
        return '';
      }
      return '${word[0].toUpperCase()}${word.substring(1).toLowerCase()}';
    }).toList();

    return capitalizedWords.join(' ');
  }

  static Duration dateTimeToDuration(DateTime dateTime) {
    DateTime now = DateTime.now();
    Duration difference = now.difference(dateTime);
    return difference;
  }

  static DateTime durationToDateTime(Duration duration) {
    DateTime now = DateTime.now();
    DateTime result = now.subtract(duration);
    return result;
  }

  static String formatChatDuration(Duration duration) {
    // Calculate total minutes and remaining seconds
    int totalMinutes = duration.inMinutes;
    int seconds = duration.inSeconds % 60;

    // Format minutes and seconds with leading zeros if needed
    String minutesStr = totalMinutes.toString();
    String secondsStr = seconds.toString().padLeft(2, '0');

    // Return formatted duration
    return '$minutesStr mins: $secondsStr secs';
  }

  static String formatDuration(Duration duration) {
    // Calculate hours, minutes, and seconds
    int hours = duration.inHours;
    int minutes = (duration.inMinutes % 60);
    int seconds = (duration.inSeconds % 60);

    // Format hours, minutes, and seconds with leading zeros if needed
    String hoursStr = hours.toString().padLeft(2, '0');
    String minutesStr = minutes.toString().padLeft(2, '0');
    String secondsStr = seconds.toString().padLeft(2, '0');

    // Return formatted duration
    return '$hoursStr:$minutesStr:$secondsStr';
  }

  static Color hexToColor(String hexColor) {
    // Remove '#' if present
    if (hexColor.startsWith('#')) {
      hexColor = hexColor.substring(1);
    }

    // Parse hex color code
    if (hexColor.length == 6) {
      hexColor = 'FF$hexColor'; // Add alpha if missing
    }

    // Convert hex to int
    int parsedColor = int.parse(hexColor, radix: 16);

    // Return Color object
    return Color(parsedColor);
  }

  static String formatDateTime(DateTime dateTime) {
    final formatter = DateFormat('dd MMMM yyyy hh : mm a');
    return formatter.format(dateTime);
  }

  static String formatDateOnly(DateTime dateTime) {
    final formatter = DateFormat('dd MMMM yyyy');
    return formatter.format(dateTime);
  }
  static String formatDateOnlyKundli(DateTime dateTime) {
  final formatter = DateFormat('yyyy-MM-dd');
  return formatter.format(dateTime);
}
}
