import 'dart:developer';

import 'package:flutter/cupertino.dart';

import '../../dialog/showLoaderDialog.dart';
import '../Api/list.of.apis.dart';
import '../Model/astrologer.list.model.dart';

class FilterNotifier with ChangeNotifier {
  final List<int> _values = <int>[];
  List<int> get values => _values.toList();

  List<String> get valuesSelectedSortBy => selectedSortBy.toList();
  List<String> get valuesCountry => selectedCountry.toList();
  List<String> get valuesGender => selectedGender.toList();
  List<String> get valuesLng => selectedLng.toList();
  List<String> get valuesSkills => selectedSkills.toList();

  List<String> selectedSortBy = [];
  List<String> selectedCountry = [];
  List<String> selectedGender = [];
  List<String> selectedLng = [];
  List<String> selectedSkills = [];
  ListOfApi listOfApi = ListOfApi();
  AstrologerListModel _astrologerListModel = AstrologerListModel();
  AstrologerListModel get astrologerList => _astrologerListModel;
  bool isApplyCall = false;

  void add(int value) {
    _values.add(value);
    notifyListeners();
    print("Notify my filter--> $_values");
  }

  // getFilteredAstrologerListProvider(
  //     {required BuildContext context,
  //     required int UserId,
  //     required List<String> selectedSortBy,
  //     required List<String> selectedSkills,
  //     required List<String> selectedLng,
  //     required List<String> selectedGender,
  //     required List<String> selectedCountry}) async {
  //   isApplyCall = true;
  //   try {
  //     final dataList = await listOfApi.getFilteredAstrologerList(
  //         UserId,
  //         selectedSortBy,
  //         selectedSkills,
  //         selectedLng,
  //         selectedGender,
  //         selectedCountry);
  //     _astrologerListModel = dataList;
  //     //Navigator.pop(context);
  //     log("FiltersAstro List Status Provider======> ${_astrologerListModel.status}");
  //     notifyListeners();
  //   } catch (e) {
  //     debugPrint(e.toString());
  //     notifyListeners();
  //   }
  // }

  void applyFilter(
      List<String> selectedSortBy,
      List<String> selectedCountry,
      List<String> selectedGender,
      List<String> selectedLng,
      List<String> selectedSkills,
      BuildContext context,
      int userId) {
    this.selectedSortBy = selectedSortBy;
    this.selectedCountry = selectedCountry;
    this.selectedGender = selectedGender;
    this.selectedLng = selectedLng;
    this.selectedSkills = selectedSkills;
    print("selectedGender--->${this.selectedGender}");
    // getFilteredAstrologerListProvider(
    //   context: context,
    //   UserId: userId,
    //   selectedSortBy: selectedSortBy,
    //   selectedSkills: selectedSkills,
    //   selectedLng: selectedLng,
    //   selectedGender: selectedGender,
    //   selectedCountry: selectedCountry,
    // );
    showLoaderDialog(context);
  }
}
