import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import '../Model/astrologer_details_model.dart';
import '../Model/astrologer_short_details_model.dart';
import '../Model/jyotish_mall_model_new.dart';
import '../Model/newly_launched_model_new.dart';
import '../Model/pooja_details_model.dart';
import '../Model/product_details_model_new.dart';
import '../Model/similar_consultant_model.dart';
import '../Model/spell_slider_model.dart';
import '../helper_functions.dart';
import '../../utils/snack_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Api/ApiServices.dart';
import '../Api/Constants.dart';
import '../Api/list.of.apis.dart';
import '../Model/OrderHistoryModel.dart';
import '../Model/SearchModel.dart';
import '../Model/accept_cutomer_call_model.dart';
import '../Model/astrologer.category.list.model.dart';
import '../Model/astrologer.list.model.dart';
import '../Model/astrologer.slider.category.model.dart';
import '../Model/astrologer_live_details_model.dart';
import '../Model/banner.model.dart';
import '../Model/blog.model.dart';
import '../Model/book.pooja.model.dart';
import '../Model/celebrity.customer.model.dart';
import '../Model/create_order_pooja_model.dart';
import '../Model/follower.model.dart';
import '../Model/gift.api.model.dart';
import '../Model/gift.wallet.payment.model.dart';
import '../Model/jyotish.mall.detail.model.dart';
import '../Model/jyotish.mall.model.dart';
import '../Model/mall.product.slider.dart';
import '../Model/my.following.model.dart';
import '../Model/newly.launched.model.dart';
import '../Model/recharge.amount.model.dart';
import '../Model/recharge.total.amount.model.dart';
import '../Model/send_review_model.dart';
import '../Model/total.success.payment.model.dart';
import '../Model/wallet.transection.model.dart';
import '../logger_helper.dart';

class ListOfApisProvider with ChangeNotifier {
  ListOfApi listOfApi = ListOfApi();

  /**************************** Astrologer List Provider *************************/
  AstrologerListModel _astrologerListModel = AstrologerListModel();

  AstrologerListModel get astrologerList => _astrologerListModel;
  bool isLoading = false;

  getAstrologerListProvider(
      {required BuildContext context,
      required int UserId,
      required String type,
      required int pageNumber}) async {
    isLoading = true;
    try {
      final dataList = await listOfApi.getAstrologerList(
          UserId, type, pageNumber, {},
          selectedCountry: [],
          selectedGender: [],
          selectedLng: [],
          selectedSkills: [],
          selectedSortBy: []);
      _astrologerListModel =
          AstrologerListModel(status: true, astrologers: dataList!.astrologers);
      log("Astro List Status Provider======> ${_astrologerListModel.status}");
      isLoading = false;
      notifyListeners();
    } catch (e) {
      debugPrint(e.toString());
      isLoading = false;
      notifyListeners();
    }
  }

  Future<AstrologerDetailsModel?> getAstrologerDetailsNew(
      {required String astrologerId, required String type}) async {
    try {
      var getAstrologerDetailsNew = await listOfApi.getAstrologerDetails(
          astrologerId: astrologerId, type: type);
      final Map<String, dynamic> parseData =
          await jsonDecode(getAstrologerDetailsNew);
      // TLoggerHelper.warning(parseData['astrologers'][0]['astrologer']
      //         ['following_user']
      //     .toString());
      return AstrologerDetailsModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<ProductDetailsModelNew?> getProductDetailsNew(
      {required String productId}) async {
    try {
      var getProductDetailsNew =
          await listOfApi.getProductDetailsItem(productId: productId);
      final Map<String, dynamic> parseData =
          await jsonDecode(getProductDetailsNew);
      if (parseData['status']) {
        return ProductDetailsModelNew.fromJson(parseData);
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<PoojaDetailsModel?> getPoojaDetails({required String poojaId}) async {
    // try {
    var getPoojaDetails = await listOfApi.getPoojaDetails(poojaId: poojaId);
    final Map<String, dynamic> parseData = await jsonDecode(getPoojaDetails);
    TLoggerHelper.debug(parseData.entries.toString());
    return PoojaDetailsModel.fromJson(parseData);
    // } catch (e) {
    //   TLoggerHelper.error(e.toString());
    // }
  }

  Future<AstrologerShortDetailsModel?> getPanditDetails(
      {required String astrologerId}) async {
    try {
      var getPoojaDetails =
          await listOfApi.getPanditDetails(astrologerId: astrologerId);
      final Map<String, dynamic> parseData = await jsonDecode(getPoojaDetails);
      TLoggerHelper.debug(parseData.entries.toString());
      return AstrologerShortDetailsModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }

    return null;
  }

  Future<bool> submitBlockReport(
      {required String astrologerId,
      required String reason,
      required bool blockStatus}) async {
    try {
      var submitBlockReport = await listOfApi.submitBlockReport(
          astrologerId: astrologerId, reason: reason, blockStatus: blockStatus);
      final Map<String, dynamic> parseData =
          await jsonDecode(submitBlockReport);
      if (parseData['status'] == true) {
        return true;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return false;
  }

  Future<bool> followerNew(
      {required String astrologerID, required bool status}) async {
    try {
      var followerNew = await listOfApi.followerNew(
          astrologerID: astrologerID, status: status);
      final Map<String, dynamic> parseData = await jsonDecode(followerNew);
      if (parseData['status']) {
        return true;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return false;
  }

  Future<SimilarConsultantsModel?> getSimilarConsulants(
      {required String primarySkillsList}) async {
    try {
      var getSimilarConsultants = await listOfApi.getSimilarConsultants(
          primarySkillIds: primarySkillsList);
      final Map<String, dynamic> parseData =
          await jsonDecode(getSimilarConsultants);
      return SimilarConsultantsModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<BlogModel?> getBlogNew() async {
    try {
      var data = await listOfApi.getBlogNew();
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return BlogModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<AstrologerCategoryListModel?>
      getAstrologerCategoryListProvider() async {
    try {
      var data = await listOfApi.getAstrologerCategoryList();
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return AstrologerCategoryListModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<SliderAstrologerCategoryModel?>
      getSliderAstrologerCategoryListProvider({required String id}) async {
    try {
      var data = await listOfApi.getSliderAstrologerCategoryList(id: id);
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return SliderAstrologerCategoryModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<CelebrityCustomerModel?> getCelebrityCustomerNew() async {
    try {
      var data = await listOfApi.getCelebrityCustomer();
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return CelebrityCustomerModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<AstrologerListModel?> getAstrologerListNew() async {
    // try {
    final dataList = await listOfApi.getAstrologerList(
        int.parse(await THelperFunctions.getUserId()), "chat", 0, {},
        selectedSortBy: [],
        selectedCountry: [],
        selectedGender: [],
        selectedLng: [],
        selectedSkills: []);
    return dataList;
    // } catch (e) {
    //   TLoggerHelper.error(e.toString());
    //   return null;
    // }
  }

  // getFilteredAstrologerListProvider(
  //     {required BuildContext context,
  //     required int UserId,
  //     required List<String> selectedSortBy,
  //     required List<String> selectedSkills,
  //     required List<String> selectedLng,
  //     required List<String> selectedGender,
  //     required List<String> selectedCountry}) async {
  //   isLoading = true;
  //   try {
  //     final dataList = await listOfApi.getFilteredAstrologerList(
  //         UserId,
  //         selectedSortBy,
  //         selectedSkills,
  //         selectedLng,
  //         selectedGender,
  //         selectedCountry);
  //     _astrologerListModel = dataList;
  //     log("FiltersAstro List Status Provider======> ${_astrologerListModel.status}");
  //     isLoading = false;
  //     notifyListeners();
  //   } catch (e) {
  //     debugPrint(e.toString());
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  /******************************* Create Order Pooja Provider **************************/

  CreateOrderPoojaModel _createOrderPoojaModel = CreateOrderPoojaModel();

  CreateOrderPoojaModel get createOrderPoojaGetter => _createOrderPoojaModel;

  Future<CreateOrderPoojaModel?> createOrderPoojaListProvider(
      {required BuildContext context,
      
      required String amount,
      required String userInfoId,
      required String astrologerID,
      required String productID,
      required String isWallet,
      required List<String> productPrice,
      required String realPricePoojaWithoutGST,
      required String suggestedAstrologerId}) async {
    isLoading = true;
    notifyListeners();
    // try {
    final orderPoojaProductResponse = await listOfApi.createOrderPooja(
        userInfoId: userInfoId,
        realPricePoojaWithoutGST: realPricePoojaWithoutGST,
        astrologerID: astrologerID,
        isWallet: isWallet,
        amount: amount,
        productID: productID,
        productPrice: productPrice,
        suggestedAstrologerId: suggestedAstrologerId);
    _createOrderPoojaModel = orderPoojaProductResponse;
    log("Create Order Status Provider======> ${_createOrderPoojaModel.status} \n<<<<---->>>\n${{
      _createOrderPoojaModel.payment!.orderPoojaId!:
          _createOrderPoojaModel.payment!.orderId!
    }}");
    isLoading = false;
    notifyListeners();
    return _createOrderPoojaModel;
    // } catch (e) {
    //   TLoggerHelper.error(e.toString());
    //   isLoading = false;
    //   notifyListeners();
    //   return null;
    // }
  }

  /**************************** Astrologer Category List Provider *************************/
  WalletTransactionModel _transactionWalletModel = WalletTransactionModel();

  WalletTransactionModel get getWalletTransaction => _transactionWalletModel;

  walletTransactionProvider({required String userID}) async {
    isLoading = true;
    //notifyListeners();
    try {
      final walletTransaction = await listOfApi.walletTransaction(
          userID: await THelperFunctions.getUserId());
      _transactionWalletModel = walletTransaction;
      print("Provider--> $walletTransaction");
      isLoading = false;
      notifyListeners();
    } catch (e) {
      debugPrint("Wallet Provider Error --> ${e.toString()}");
      isLoading = false;
      notifyListeners();
    }
  }

  /***************************** Search Provider ********************************/
  SearchModel _searchModel = SearchModel();

  SearchModel get searchModel => _searchModel;

  searchProvider({required String name}) async {
    isLoading = true;
    notifyListeners();
    try {
      final searchData = await listOfApi.search(name: name);

      _searchModel = searchData;

      print(" Search Provider--> $searchData");
      isLoading = false;
      notifyListeners();
    } catch (e) {
      debugPrint("Search Provider Error --> ${e.toString()}");
      isLoading = false;
      notifyListeners();
    }
  }

  /***************************** Follower Provider ********************************/
  FollowModel _followModel = FollowModel();

  FollowModel get followModel => _followModel;

  var isApiHit = false;

  followerProvider(
      {required String customerID,
      required String astrologerID,
      required String status}) async {
    isLoading = true;
    notifyListeners();
    try {
      final followData = await listOfApi.follower(
          customerID: customerID, astrologerID: astrologerID, status: status);
      _followModel = followData;
      print("state.followModel.status---> ${_followModel.status}");
      isLoading = false;
      isApiHit = true;
      notifyListeners();
    } catch (e) {
      debugPrint("Follow Provider Error --> ${e.toString()}");
      isLoading = false;
      isApiHit = true;
      notifyListeners();
    }
  }

  /***************************** Gift Wallet Provider ********************************/
  GiftWalletPaymentModel _giftWalletApiModel = GiftWalletPaymentModel();

  GiftWalletPaymentModel get giftWalletModel => _giftWalletApiModel;
  bool giftWalletApi = false;

  Future giftWalletProvider(
      {required String customerID,
      required String astrologerID,
      required BuildContext context,
      required String giftID}) async {
    isLoading = true;
    try {
      final giftWalletPaymentData = await listOfApi.giftWalletPayment(
          userID: customerID, astrologerID: astrologerID, giftID: giftID);
      _giftWalletApiModel = giftWalletPaymentData;
      print("Gift Wallet Payment Provider--> $giftWalletPaymentData");
      isLoading = false;
      giftWalletApi = true;
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
      Fluttertoast.showToast(msg: "Gift Purchased Successfully !!");
      notifyListeners();
    } catch (e) {
      debugPrint("Gift Wallet Payment Provider Error --> ${e.toString()}");
      isLoading = false;
      notifyListeners();
    }
  }

  /**************************** Astrologer Category List Provider *************************/

  // AstrologerCategoryListModel _astrologerCategoryListModel =
  //     AstrologerCategoryListModel();

  // AstrologerCategoryListModel get astrologerCategoryList =>
  //     _astrologerCategoryListModel;

  // getAstrologerCategoryListProvider({required BuildContext context}) async {
  //   isLoading = true;
  //   //notifyListeners();
  //   try {
  //     final dataList = await listOfApi.getAstrologerCategoryList();
  //     if (dataList != null) {
  //       debugPrint(
  //           "Astrologer Category Provider List Length=======> ${dataList.data!.length}");
  //       _astrologerCategoryListModel = dataList;
  //       if (_astrologerCategoryListModel.data != null)
  //         getSliderAstrologerCategoryListProvider(
  //             context: context,
  //             id: _astrologerCategoryListModel.data![0].id.toString());
  //     } else {
  //       debugPrint("DataList or Data is null");
  //       // Handle the case where dataList or dataList.data is null
  //     }
  //     isLoading = false;
  //     notifyListeners();
  //   } catch (e) {
  //     debugPrint(e.toString());
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  /**************************** Slider Astrologer Category List Provider *************************/

  // SliderAstrologerCategoryModel _sliderAstrologerCategoryModel =
  //     SliderAstrologerCategoryModel();

  // SliderAstrologerCategoryModel get sliderAstrologerCategoryList =>
  //     _sliderAstrologerCategoryModel;

  // getSliderAstrologerCategoryListProvider(
  //     {required BuildContext context, }) async {
  //   isLoading = true;
  //   // notifyListeners();

  //   try {
  //     final sliderDataList =
  //         await listOfApi.getSliderAstrologerCategoryList(id);

  //     log("Slider Astrologer Category Provider List Length=======> ${sliderDataList.data.length}");
  //     _sliderAstrologerCategoryModel = sliderDataList;

  //     notifyListeners();
  //     log("Now Here");
  //   } catch (e) {
  //     debugPrint("Error fetching data: $e");
  //     // Handle specific errors or log more details if necessary
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  /************************* Recharge Amount Provider **************************/

  RechargeAmountModel _rechargeAmountModel = RechargeAmountModel();

  RechargeAmountModel get rechargeAmountModel => _rechargeAmountModel;

  getRechargeAmountProvider({required BuildContext context}) async {
    isLoading = true;
    try {
      final amountData = await listOfApi.getRechargeData();
      log("Recharge Amount Provider ====> ${amountData.recharge.length}");
      _rechargeAmountModel = amountData;
      notifyListeners();
    } catch (e) {
      log("Error Show ----> $e");
    } finally {
      isLoading = false;
      notifyListeners();
    }
  }

  Future getRechargeAmountPProvider({required BuildContext context}) async {
    try {
      final amountData = await listOfApi.getRechargeData();
      log("Recharge Amount Provider ====> ${amountData.recharge.length}");
      RechargeAmountModel _rechargeAmountModel = amountData;
      return _rechargeAmountModel;
    } catch (e) {
      log("Error Show ----> $e");
    }
  }
  /************************* Success Amount Provider **************************/

  TotalSuccessPaymentModel _successFullModel = TotalSuccessPaymentModel();

  TotalSuccessPaymentModel get successRechargeAmountModel => _successFullModel;

  Future getSuccessProvider(
      {required BuildContext context,
      
      required String orderId,
      
      required String razorpayId,
      required String razorpaySecretId,
      String? remedy_id,
      String? astroProductId,
      String? suggestedAstrologerId,
      required String transactionID}) async {
    isLoading = true;
    notifyListeners();
    try {
      final successAmountData = await listOfApi.totalSuccessPayment(
          razorpayId: razorpayId,
          razorpaySecretId: razorpaySecretId,
          
          transactionID: transactionID,
          orderId: orderId,
          suggestedAstrologerId: suggestedAstrologerId,
          astroProductId: astroProductId,
          remedy_id: remedy_id);
      log("Success Provider ====> ${successAmountData.payment!.gstAmount}");
      _successFullModel = successAmountData;
      notifyListeners();
      print("Success Amount");
    } catch (e) {
      log("Error Show ----> $e");
    } finally {
      isLoading = false;
      notifyListeners();
    }
  }

  /************************* Recharge Amount Provider **************************/

  RechargeTotalAmountModel _totalRechargeAmountModel =
      RechargeTotalAmountModel();

  RechargeTotalAmountModel get totalRechargeAmountModel =>
      _totalRechargeAmountModel;

  getTotalRechargeAmountProvider(
      {required BuildContext context, required String amountID}) async {
    isLoading = true;
    //notifyListeners();
    try {
      final totalAmountData = await listOfApi.totalPayment(amountID: amountID);
      log("Payment Amount Provider ====> ${totalAmountData.recharge!.gstAmount}");
      _totalRechargeAmountModel = totalAmountData;
      notifyListeners();
      TLoggerHelper.debug("Recharge Total Amount Model!!");
    } catch (e) {
      TLoggerHelper.error(e.toString());
    } finally {
      isLoading = false;
      notifyListeners();
    }
  }

  // /************************** Celebrity Customer Provider **********************/
  // CelebrityCustomerModel _celebrityCustomerModel = CelebrityCustomerModel();

  // CelebrityCustomerModel get celebrity => _celebrityCustomerModel;

  // getCelebrityCustomerProvider({required BuildContext context}) async {
  //   isLoading = true;
  //   try {
  //     final celebrityData = await listOfApi.getCelebrityCustomer();

  //     if (celebrityData != null && celebrityData.data != null) {
  //       debugPrint(
  //           "Celebrity Provider List Length=======> ${celebrityData.data.length}");
  //       _celebrityCustomerModel = celebrityData;
  //     }
  //   } catch (e) {
  //     debugPrint("Error fetching data : $e");
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  /************************** Jyotish Mall Provider **********************/
  // JyotishMallModel _jyotishMallModel = JyotishMallModel();

  // JyotishMallModel get jyotishModel => _jyotishMallModel;

  // getJyotishMallProvider(
  //     {required BuildContext context, required String customText}) async {
  //   isLoading = true;
  //   try {
  //     final jyotishData = await listOfApi.getJyotishMall();

  //     if (jyotishData != null && jyotishData.data != null) {
  //       log("Jyotish Mall Provider List Length=======> ${jyotishData.data.length}");

  //       _jyotishMallModel = jyotishData;
  //     }
  //   } catch (e) {
  //     debugPrint("Error fetching data : $e");
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  Future<JyotishMallModel?> getJyotishMallProvider() async {
    try {
      var followerNew = await listOfApi.getMallDataNew();
      final Map<String, dynamic> parseData = await jsonDecode(followerNew);
      JyotishMallModel jyotishMallModel = JyotishMallModel.fromJson(parseData);
      if (parseData['status']) {
        return jyotishMallModel;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<JyotishMallModelNew?> getJyotishMallProviderNew() async {
    try {
      var getJyotishMallProvider = await listOfApi.getJyotishMallNew();
      final Map<String, dynamic> parseData =
          await jsonDecode(getJyotishMallProvider);
      TLoggerHelper.debug(parseData.entries.toString());
      if (parseData['status'] == true) {
        return JyotishMallModelNew.fromJson(parseData);
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  /********************** Jyotish Mall Detail Provider ****************************/

  Future<JyotishMallDetailModel?> getJyotishMallDetailProvider(
      {required BuildContext context, required String categoryId,required String suggestedType}) async {
    try {
      var getjyotishmalldetail = await listOfApi.getJyotishMallDetail(categoryId,suggestedType);
      Map<String, dynamic> parseData = await jsonDecode(getjyotishmalldetail);
      JyotishMallDetailModel jyotishMallDetailModel =
          JyotishMallDetailModel.fromJson(parseData);
      TLoggerHelper.info(parseData.entries.toString());
      if (parseData['status']) {
        return jyotishMallDetailModel;
      } else {
        return null;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  /****************************** Send Call Duration ********************************/

  AcceptCustomerStartCallModel _acceptCustomerStartCallModel =
      AcceptCustomerStartCallModel();

  AcceptCustomerStartCallModel get sendDuration =>
      _acceptCustomerStartCallModel;

  Future sendCallDurationProvider(
      {required String communicationID, required int duration}) async {
    try {
      listOfApi.userEndCall(communicationId: communicationID);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  /************************** Send Review Provider *******************/
  Future<SendReviewModel?> sendReviewProvider(
      {required String astrologer_id,
      required String customer_id,
      required int reviewId,
      required String description,
      required String rate,
      required String isLike,
      required String type,
      required String orderID,
      required String infoId}) async {
    try {
      var sendReviewResponse = await listOfApi.sendReview(
          astrologer_id: astrologer_id,
          customer_id: customer_id,
          description: description,
          rate: rate,
          isLike: isLike,
          type: type,
          reviewId: reviewId,
          orderID: orderID,
          infoId: infoId);

      final Map<String, dynamic> parseData =
          await jsonDecode(sendReviewResponse);
      return SendReviewModel.fromJson(parseData);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  /***************************** Following Provider ********************************/

  Future<FollowingModel?> followingProvider(
      {required String customerID}) async {
    try {
      final followingData = await listOfApi.following(customerID: customerID);
      FollowingModel followingModel = followingData!;
      if (followingData.status!) {
        return followingModel;
      } else {
        return null;
      }
    } catch (e) {
      TLoggerHelper.error("Following Provider Error --> ${e.toString()}");
    }
    return null;
  }

  // /**************************** Remedy Provider *********************************/
  //
  // Future<CustomerRemedyModel?> customerRemedyProvider(
  //     {required String userID}) async {
  //   try {
  //     final remedyData = await listOfApi.customerRemedy(userID: userID);
  //
  //     CustomerRemedyModel customerRemedyModel = remedyData!;
  //
  //     if (remedyData.status!) {
  //       return customerRemedyModel;
  //     } else {
  //       return null;
  //     }
  //   } catch (e) {
  //     TLoggerHelper.error("Remedy Provider Error --> ${e.toString()}");
  //   }
  //   return null;
  // }

  /********************** Blog Provider ****************************/

  // BlogModel _blogModel = BlogModel();

  // BlogModel get blogDetail => _blogModel;

  // getBlogProvider({required BuildContext context}) async {
  //   isLoading = true;
  //   try {
  //     final blogDetail = await listOfApi.getBlog();
  //     if (blogDetail != null && blogDetail.data != null) {
  //       // debugPrint("Blog Detail Length=======> ${blogDetail.data.length}");
  //       _blogModel = blogDetail;
  //     }
  //   } catch (e) {
  //     debugPrint("Error fetching data : $e");
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  /********************** My Orders Provider ****************************/

  Future<List<OrderHistory>?> getMyOrderList() async {
    DioClient? client;
    isLoading = true;
    final prefs = await SharedPreferences.getInstance();
    var userId = prefs.getInt(Constants.userID).toString();

    client = DioClient();
    Dio dio = await client.getClient();

    String api =UrlConstants.baseUrl + UrlConstants.order;

    var params = jsonEncode({'user_id': userId});
    TLoggerHelper.debug(params);
    OrderHistoryModel? orderHistoryModel =
        await client.orderHistory(dio, api, params);

    if (orderHistoryModel.status == true) {
      return orderHistoryModel.orderHistory;
    }
    return [];
  }

  /********************** Banner Provider ****************************/
  // BannerModel _bannerModel = BannerModel();

  // BannerModel get bannerDetail => _bannerModel;

  // getBannerProvider({required BuildContext context}) async {
  //   isLoading = true;
  //   try {
  //     final bannerDetail = await listOfApi.getBanner();
  //     if (bannerDetail != null && bannerDetail.data != null) {
  //       debugPrint("Banner Detail Length=======> ${bannerDetail.data.length}");
  //       _bannerModel = bannerDetail;
  //     }
  //   } catch (e) {
  //     debugPrint("Error fetching data : $e");
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }
  Future<BannerModel?> getBannerProvider() async {
    try {
      final data = await listOfApi.getBanner();
      final Map<String, dynamic> parseData = await jsonDecode(data);
      BannerModel bannerData = BannerModel.fromJson(parseData);
      if (bannerData.status!) {
        return bannerData;
      } else {
        return null;
      }
    } catch (e) {
      TLoggerHelper.error("Following Provider Error --> ${e.toString()}");
    }
    return null;
  }

  /********************** Spell Slider Provider ****************************/
  // SpellSliderModel _spellSliderModel = SpellSliderModel();

  // SpellSliderModel get spellSlider => _spellSliderModel;

  // getSpellSliderProvider(
  //     {required BuildContext context, required String id}) async {
  //   isLoading = true;
  //   try {
  //     final spellSliderData = await listOfApi.getSpellSlider(id);
  //     if (spellSliderData != null && spellSliderData.data != null) {
  //       debugPrint(
  //           "Spell Slider Length=======> ${spellSliderData.data.length}");
  //       _spellSliderModel = spellSliderData;
  //     }
  //   } catch (e) {
  //     debugPrint("Error fetching data : $e");
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  Future<SpellSliderModelNew?> getSpellSliderProviderNew(
      {required String id}) async {
    try {
      var getSpellSliderNew = await listOfApi.getSpellSliderNew(id: id);
      final Map<String, dynamic> parseData =
          await jsonDecode(getSpellSliderNew);
      if (parseData['status'] == true) {
        return SpellSliderModelNew.fromJson(parseData);
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  /********************** Spell Slider Provider ****************************/
  MallProductSliderModel _mallProductSliderModel = MallProductSliderModel();

  MallProductSliderModel get mallPSlider => _mallProductSliderModel;

  getMallProductSliderProvider(
      {required BuildContext context, required String id}) async {
    isLoading = true;
    try {
      final productSliderData = await listOfApi.getMallProductSlider(id);
      if (productSliderData != null && productSliderData.data != null) {
        debugPrint(
            "Spell Slider Length=======> ${productSliderData.data.length}");
        _mallProductSliderModel = productSliderData;
      }
    } catch (e) {
      debugPrint("Error fetching data : $e");
    } finally {
      isLoading = false;
      notifyListeners();
    }
  }


  /********************** Book Pooja Provider ****************************/
  BookPoojaModel _bookPoojaModel = BookPoojaModel();

  BookPoojaModel get poojaModel => _bookPoojaModel;

  getBookPoojaProvider({required BuildContext context}) async {
    isLoading = true;
    notifyListeners();
    try {
      final bookPoojaData = await listOfApi.getBookPooja();
      if (bookPoojaData != null && bookPoojaData.data != null) {
        debugPrint("Book Pooja Length=======> ${bookPoojaData.data.length}");
        _bookPoojaModel = bookPoojaData;
      }
    } catch (e) {
      debugPrint("Error fetching data : $e");
    } finally {
      isLoading = false;
      notifyListeners();
    }
  }

  /********************** Book Pooja Provider ****************************/
  // NewlyLaunchedModel _newlyLaunchedModel = NewlyLaunchedModel();

  // NewlyLaunchedModel get newLaunchModel => _newlyLaunchedModel;

  // getNewlyLaunchedProvider({required BuildContext context}) async {
  //   isLoading = true;
  //   notifyListeners();
  //   try {
  //     final newLaunchedData = await listOfApi.getNewlyLaunched();
  //     if (newLaunchedData != null && newLaunchedData.data != null) {
  //       _newlyLaunchedModel = newLaunchedData;
  //     }
  //   } catch (e) {
  //     debugPrint("Error fetching data : $e");
  //   } finally {
  //     isLoading = false;
  //     notifyListeners();
  //   }
  // }

  Future<NewlyLauchedModelNew?> getNewlyLauchedProviderNew() async {
    try {
      var getNewlyLauchedItems = await listOfApi.getNewlyLauchedNew();
      final Map<String, dynamic> parseData =
          await jsonDecode(getNewlyLauchedItems);
      if (parseData['status'] == true) {
        return NewlyLauchedModelNew.fromJson(parseData);
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  /********************* Gift Data Provider ***********************/
  GiftApiModel _giftApiModel = GiftApiModel();

  GiftApiModel get giftModel => _giftApiModel;

  giftDataProvider({
    required BuildContext context,
    required String customerID,
  }) async {
    isLoading = true;
    try {
      final getGiftDataResponse = await listOfApi.getGiftData(customerID);
      _giftApiModel = getGiftDataResponse;
      print("Provider Gift Status ---> ${_giftApiModel.status}");

      isLoading = false;
      notifyListeners();
    } catch (e) {
      log("Error---> $e");
    }
  }

  Future acceptCallStart(
      {required String communicationId, required String astrologerId}) async {
    try {
      await listOfApi.acceptCallStart(
          communicationId: communicationId, astrologerId: astrologerId);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future callDismiss({required String communicationId}) async {
    try {
      await listOfApi.callDismiss(communicationId: communicationId);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future sendGiftWallet(
      {required BuildContext context,
      required String astrologerId,
      required String giftId}) async {
    try {
      var sendGiftWallet = await listOfApi.sendGiftWallet(
          astrologerId: astrologerId, giftId: giftId);
      final Map<String, dynamic> parseData = await jsonDecode(sendGiftWallet);
      if (parseData['status']) {
        Navigator.of(context).pop();
      } else {
        TLoggerHelper.error("Some error occured !!");
        showSnackBarWidget(context, "Gift isn't get sent");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<AstrologerLiveDetailsModel?> astrologerDetailsLive(
      {required String id}) async {
    try {
      var astrologerDetailsLive =
          await listOfApi.astrologerDetailsLive(astrologerId: id);
      final Map<String, dynamic> parseData =
          await jsonDecode(astrologerDetailsLive);
      TLoggerHelper.debug(parseData.entries.toString());

      if (parseData['status']) {
        AstrologerLiveDetailsModel astrologerLiveDetailsModel =
            AstrologerLiveDetailsModel.fromJson(parseData);
        return astrologerLiveDetailsModel;
      } else {
        TLoggerHelper.error("Some error occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }
}
