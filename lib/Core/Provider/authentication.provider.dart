import 'dart:convert';

import 'package:flutter/material.dart';
import '../helper_functions.dart';
import '../../handler_classes/secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../CustomNavigator/SlideRightRoute.dart';
import '../../Screens/OTPScreen.dart';
import '../Api/Constants.dart';
import '../Api/authentication.apis.dart';
import '../logger_helper.dart';

class AuthenticationProvider with ChangeNotifier {
  final AuthenticationAPI _authenticationAPI = AuthenticationAPI();
  static SecureStorage secureStorage = SecureStorage();
  Future login(
      {required BuildContext context,
      required String mobileNum,
      required String token}) async {
    try {
      var userLogin = await _authenticationAPI.loginAccount(
          phoneNum: mobileNum, deviceToken: token);
      debugPrint("Login Response Provider ==========> $userLogin");
      final Map<String, dynamic> parseData = await jsonDecode(userLogin);
      debugPrint("Login Response ==========> $parseData");

      bool isAuthentication = parseData['status'];
      dynamic authData = parseData['data'];
      if (isAuthentication) {
        SharedPreferences prefs = await SharedPreferences.getInstance();

        var userID = authData["id"];
        var mobileNumm = authData["mobile"];
        var userName = authData["name"];
        var userEmail = authData["email"];
        print("================== Shared Preferences==================");
        await prefs.setInt(Constants.tempUserID, userID);
        await prefs.setString(Constants.mobile, mobileNumm.toString());
        await prefs.setString(Constants.fullName, userName);
        await prefs.setString(Constants.profileImage, authData["image"]);
        await prefs.setString(Constants.email, userEmail ?? "");
        await prefs.setString(Constants.gender, authData["customer"]["gender"]);
        await prefs.setString(Constants.dob, authData["customer"]["dob"]);
        await prefs.setString(
            Constants.dob_time, authData["customer"]["dob_time"]);
        await prefs.setString(
            Constants.dob_place, authData["customer"]["dob_place"]);
        await prefs.setString(
            Constants.pincode, authData["customer"]["pincode"]);
        await prefs.setString(
            Constants.current_address, authData["customer"]["current_address"]);
        await prefs.setString(
            Constants.address, authData["customer"]["address"]);
        int? tempUserID = prefs.getInt(Constants.tempUserID);
        print("My User Save ID -> $tempUserID");
        print("My User Save Name -> ${prefs.getString("name")}");

        Navigator.push(
            context,
            SlideRightRoute(
                page: ListenableProvider(
                    create: (context) => AuthenticationProvider(),
                    child: OTPScreen(mobileNum, token))));
      } else {
        SnackBar(content: Text("Authentication"));
      }
    } catch (e) {
      debugPrint("Error Catch ----------> ${e.toString()}");
    }
  }

/* Verifies OTP*/
  Future<void> verifyOTPProvider({
    required BuildContext context,
    required String mobileNumber,
    required String otp,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      var verifyOTP = await _authenticationAPI.verifyOTP(
        mobileNumber: mobileNumber,
        otp: otp,
      );

      final Map<String, dynamic> parseData = jsonDecode(verifyOTP);

      if (parseData.isNotEmpty) {
        final bool? isAuthentication = parseData['status'];
        dynamic authData1 = parseData['data'];

        if (isAuthentication == true) {
          var userID = authData1["id"];
          var userName = authData1["name"];

          // Save data locally
          await secureStorage
              .setAccessToken(authData1['access_token'].toString());
          await prefs.setInt(Constants.userID, userID);
          await prefs.setInt(Constants.isFreeFlag, authData1['is_flag_free']);

          await prefs.setString(Constants.fullName, userName);

          // Retry until userID exists
          while (true) {
            String? storedUserID = await THelperFunctions.getUserId();

            if (storedUserID != null &&
                storedUserID.isNotEmpty &&
                storedUserID != "null") {
              TLoggerHelper.debug(
                  "User ID found: $storedUserID. Restarting the app.");
              await Future.delayed(const Duration(
                  seconds: 1)); // Small delay to avoid tight loop
              THelperFunctions.restartApp();
              break;
            } else {
              TLoggerHelper.debug(
                  "User ID not found. Retrying to store data...");
              // Re-store the data
              await prefs.setInt(Constants.userID, userID);
              await prefs.setString(Constants.fullName, userName);
              await Future.delayed(const Duration(
                  milliseconds: 500)); // Small delay to avoid tight loop
            }
          }
        } else {
          // Show a SnackBar on the main UI thread for wrong OTP
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("Wrong OTP")),
          );
        }
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      // Optionally show an error SnackBar or dialog
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("An error occurred: ${e.toString()}")),
      );
    }
  }
}
