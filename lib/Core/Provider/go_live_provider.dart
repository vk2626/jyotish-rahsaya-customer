import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../Model/waitlist_user_live_model.dart';
import '../Api/list.of.apis.dart';
import '../Model/accept_cutomer_call_model.dart';
import '../Model/astrologer_live_reels_model.dart';

import '../Model/gift.api.model.dart';
import '../Model/golive_messages_model.dart';
import '../logger_helper.dart';

class GoLiveProvider with ChangeNotifier {
  final ListOfApi _listOfApi = ListOfApi();
  Future<List<String>> goLive({required BuildContext context}) async {
    try {
      var goLiveSingle = await _listOfApi.goLive();
      final Map<String, dynamic> parseData = await jsonDecode(goLiveSingle);
      bool isHttpStatus = parseData['status'];
      TLoggerHelper.debug(parseData.toString());
      if (!isHttpStatus) {
        showSnackBarWidget(context, "May be some error !!");
      } else {
        showSnackBarWidget(context, "Fetching Details!!", isTrue: true);
        return [
          parseData['is_online'][0]['live_token'].toString(),
          parseData['is_online'][0]['channel_name'].toString(),
          parseData['is_online'][0]['live_id'].toString(),
        ];
      }
    } on HttpException catch (e) {
      TLoggerHelper.error(e.message);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return [];
  }

  Future<GoLiveMessagesModel?> getAllMessageList(
      {required BuildContext context, required String liveId}) async {
    try {
      var getAllMessageList =
          await _listOfApi.getAllMessageList(liveId: liveId);
      final Map<String, dynamic> parseData =
          await jsonDecode(getAllMessageList);
      GoLiveMessagesModel goLiveMessagesModel =
          GoLiveMessagesModel.fromJson(parseData);
      if (goLiveMessagesModel.status) {
        return goLiveMessagesModel;
      } else {
        showSnackBarWidget(context, "Some error occured");
        return null;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future endGoLive(
      {required BuildContext context,
      required String sessionId,
      required String liveId}) async {
    try {
      var endGoLive = await _listOfApi.endGoLive(id: sessionId, liveId: liveId);
      final Map<String, dynamic> parseData = await jsonDecode(endGoLive);

      bool isHttpStatus = parseData['status'];
      if (!isHttpStatus) {
        showSnackBarWidget(context, "May be some error !!");
      } else {
        showSnackBarWidget(context, "Completed Meeting Successfully !!",
            isTrue: true);
        Navigator.of(context).pop();
      }
    } on HttpException catch (e) {
      TLoggerHelper.error(e.message);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<bool> sendCallDurationCallProvider(
      {required BuildContext context,
      required String communcationId,
      required int duration}) async {
    var sendCallDurationCall = await _listOfApi.sendCallDuration(
        communicationId: communcationId, duration: duration);
    final AcceptCustomerStartCallModel parseData = sendCallDurationCall;
    bool isHttpStatus = parseData.status!;
    if (isHttpStatus) {
      showSnackBarWidget(context, "Leaved Successfully !!");
      return true;
    }
    return false;
  }

  Future<bool> sendCallDurationLive(
      {required BuildContext context,
      required String communcationId,
      required String liveId,
      required int duration}) async {
    try {
      var sendCallDurationLive = await _listOfApi.sendCallLiveDuration(
        liveId: liveId,
          communcationId: communcationId, duration: duration);
      final Map<String, dynamic> parseData =
          await jsonDecode(sendCallDurationLive);
      if (parseData['status']) {
        return true;
      } else {
        TLoggerHelper.error("Some error occured !");
        return false;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return false;
    }
  }

  Future<AstrologerLiveReelsModel?> getReelsLiveList() async {
    try {
      var getReelsLiveList = await _listOfApi.getReelsLiveList();
      final Map<String, dynamic> parseData = await jsonDecode(getReelsLiveList);
      TLoggerHelper.info(parseData.entries.toString());
      if (parseData['status']) {
        return AstrologerLiveReelsModel.fromJson(parseData);
      } else {
        TLoggerHelper.error("Some Error Occured");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<List<WaitListUserLive>> fetchWaitListLiveUsers(
      {required String astrologerId}) async {
    try {
      var fetchWaitListLiveUsers =
          await _listOfApi.fetchWaitListLiveUsers(astrologerId: astrologerId);
      final Map<String, dynamic> parseData =
          await jsonDecode(fetchWaitListLiveUsers);
      if (parseData['status']) {
        WaitListUserLiveModel waitListUserLiveModel =
            WaitListUserLiveModel.fromJson(parseData);
        return waitListUserLiveModel.users;
      } else {
        TLoggerHelper.error("Some error occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return [];
  }

  Future<bool> sendLiveComment(
      {required BuildContext context,
      required String liveId,
      required String comment}) async {
    try {
      var sendLiveComment =
          await _listOfApi.sendLiveComment(liveId: liveId, comment: comment);
      TLoggerHelper.debug("LiveID:- $liveId and Comment :- $comment");

      final Map<String, dynamic> parseData = await jsonDecode(sendLiveComment);
      if (parseData['status']) {
        TLoggerHelper.info("Message Sent Successfully!!");
        return true;
      } else {
        TLoggerHelper.error("Some error occured!!");
        return false;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return false;
  }

  Future<GiftApiModel?> getGiftListData({required BuildContext context}) async {
    try {
      var getGiftListData = await _listOfApi.getGiftListData();
      final Map<String, dynamic> parseData = await jsonDecode(getGiftListData);
      if (parseData['status']) {
        return GiftApiModel.fromJson(parseData);
      } else {
        showSnackBarWidget(context, "Some error occured");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  showSnackBarWidget(context, String message, {bool isTrue = false}) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
      ),
      backgroundColor: isTrue ? Colors.green : Colors.red,
    ));
  }
}
