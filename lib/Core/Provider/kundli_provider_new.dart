import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:html/parser.dart';

import '../Api/kundli_api_new.dart';

class KundliProviderNew with ChangeNotifier {
  final KundliApiNew _kundliApiNew = KundliApiNew();
  Future<Map<String, dynamic>> getBasicDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getBasicDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    final Map<String, dynamic> parseData = await jsonDecode(data);
    if (data != null) {
      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getPanchangDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getPanchangDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    final Map<String, dynamic> parseData = await jsonDecode(data);
    if (data != null) {
      return parseData;
    }
    return {};
  }

  Future<List<Map<String, dynamic>>> getKPPlanetsDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getKPPlanets(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    if (data != null) {
      final List<dynamic> decodedData = jsonDecode(data);

      final List<Map<String, dynamic>> parseData =
          List<Map<String, dynamic>>.from(decodedData);
      return parseData;
    }
    return [];
  }

  Future<List<Map<String, dynamic>>> getKPCuspsDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getKPHouses(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    if (data != null) {
      final List<dynamic> decodedData = await jsonDecode(data);

      final List<Map<String, dynamic>> parseData =
          List<Map<String, dynamic>>.from(decodedData);
      return parseData;
    }
    return [];
  }

  Future<Map<String, dynamic>> getAshtakvargaChart(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String planetName,
      required String long}) async {
    var data = await _kundliApiNew.getAshtakvargaChart(
        lang: lang,
        dateTime: dateTime,
        lat: lat,
        long: long,
        planetName: planetName);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);

      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getGeneralReportDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required String type}) async {
    var data = await _kundliApiNew.getGeneralDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long, type: type);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getRemediesReportDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required String type}) async {
    var data = await _kundliApiNew.getRemediesDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long, type: type);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getManglikDoshaReportDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getManglikDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getKalsarpDoshaReportDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getKalsarpDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getSadesatiDoshaReportDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getSadesatiDetails(
        lang: lang, dateTime: dateTime, lat: lat, long: long);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }

  Future<List<Map<String, dynamic>>> getChartPlanetsDoshaReportDetails({
    required DateTime dateTime,
    required String lat,
    required String lang,
    required String long,
  }) async {
    var data = await _kundliApiNew.getChartPlanetsDoshaReportDetails(
      lang: lang,
      dateTime: dateTime,
      lat: lat,
      long: long,
    );

    if (data != null) {
      final decodedData = jsonDecode(data);

      if (decodedData is List) {
        return List<Map<String, dynamic>>.from(decodedData);
      } else {
        throw Exception('Expected a List but got ${decodedData.runtimeType}');
      }
    }

    return [];
  }

  Future<String> getChartLagna(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      bool isNorth = true}) async {
    var data = await _kundliApiNew.getChartLagna(
        lang: lang, dateTime: dateTime, lat: lat, long: long, isNorth: isNorth);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData['svg'];
    }
    return "";
  }

  Future<String> getChartNavasma(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required bool isNorth}) async {
    var data = await _kundliApiNew.getChartNavasma(
        lang: lang, dateTime: dateTime, lat: lat, long: long, isNorth: isNorth);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData['svg'];
    }
    return "";
  }

  Future<String> getChartsDivisional(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long,
      required String type,
      required bool isNorth}) async {
    var data = await _kundliApiNew.getDivisionalChart(
        lang: lang,
        dateTime: dateTime,
        lat: lat,
        long: long,
        isNorth: isNorth,
        type: type);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData['svg'];
    }
    return "";
  }

  Future<Map<String, dynamic>> getDashaDetails(
      {required DateTime dateTime,
      required String lat,
      required String lang,
      required String long}) async {
    var data = await _kundliApiNew.getDashaDetails(
        dateTime: dateTime, lat: lat, lang: lang, long: long);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }

  Future<Map<String, dynamic>> getMatchMakingResult(
      {required DateTime boyDOB,
      required TimeOfDay boyTOB,
      required String boyLat,
      required String boyLong,
      required DateTime girlDOB,
      required TimeOfDay girlTOB,
      required String girlLat,
      required String girlLong}) async {
    var data = await _kundliApiNew.getMatchMakingResult(
        boyDOB: boyDOB,
        boyTOB: boyTOB,
        boyLat: boyLat,
        boyLong: boyLong,
        girlDOB: girlDOB,
        girlTOB: girlTOB,
        girlLat: girlLat,
        girlLong: girlLong);
    if (data != null) {
      final Map<String, dynamic> parseData = await jsonDecode(data);
      return parseData;
    }
    return {};
  }
}
