import 'dart:convert';

import 'package:flutter/foundation.dart';
import '../Model/dasha_model.dart';
import '../Model/kundli_list_model.dart';
import '../Model/match_making_model.dart';
import '../Model/sadesatidetailsmodel.dart';
import '../formatter.dart';
import '../logger_helper.dart';
import 'package:provider/provider.dart';
import '../Api/kundli_api.dart';
import '../Model/ashtakvarga_chart_model.dart';

class KundliProvider with ChangeNotifier {
  final KundliApi kundliApi = KundliApi();

  Future<bool> addKundliDetails(
      {required String name,
      required String gender,
      required String dob,
      required String dobTime,
      required String pob,
      required String lat,
      required String lon,
      String id = "",
      String orderId = "",
      String infoId = ""}) async {
    try {
      var addKundliDetails = await kundliApi.addKundliDetails(
          name: name,
          gender: gender,
          dob: dob,
          dobTime: dobTime,
          pob: pob,
          lat: lat,
          lon: lon,
          id: id,
          infoId: infoId,
          orderId: orderId);
      final Map<String, dynamic> parseData = await jsonDecode(addKundliDetails);
      bool isHttpStatus = parseData['status'];
      if (isHttpStatus) {
        return true;
      } else {
        TLoggerHelper.debug(parseData.entries.toString());
        TLoggerHelper.error("Some error occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return false;
  }

  Future<List<KundliInformationList>?> getAllKundliesList() async {
    try {
      var getAllKundliesList = await kundliApi.getAllKundliesList();
      final Map<String, dynamic> parseData =
          await jsonDecode(getAllKundliesList);
      bool isHttpStatus = parseData['status'];
      if (isHttpStatus) {
        KundliListModel kundliListModel = KundliListModel.fromJson(parseData);
        return kundliListModel.kundliInformationList;
      } else {
        TLoggerHelper.error("Some error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<bool> deleteUserKundli({required String kundliId}) async {
    try {
      var deleteUserKundli =
          await kundliApi.deleteUserKundli(kundliId: kundliId);
      final Map<String, dynamic> parseData = await jsonDecode(deleteUserKundli);
      bool isHttpStatus = parseData['status'];
      if (isHttpStatus) {
        return true;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return false;
  }

  Future<Map<String, String>?> getPanchangDetails(
      {required String date,
      required String lat,
      required String lon,
      required String lang,
      required String time}) async {
    try {
      var getPanchangDetails = await kundliApi.getPanchangDetails(
          date: date, lat: lat, lon: lon, time: time, lang: lang);
      final Map<String, dynamic> parseData =
          await jsonDecode(getPanchangDetails);
      TLoggerHelper.info(parseData.entries.toString());
      if (parseData['status'] == 200) {
        final Map<String, String> result = {};
        result['tithi'] = parseData['response']['tithi']['name'];
        result['nakshatra'] = parseData['response']['nakshatra']['name'];
        result['karana'] = parseData['response']['karana']['name'];
        result['yoga'] = parseData['response']['yoga']['name'];
        result['ayanamsa'] = parseData['response']['ayanamsa']['name'];
        result['rasi'] = parseData['response']['rasi']['name'];
        result['sunrise'] =
            parseData['response']['advanced_details']['sun_rise'];
        result['sunset'] = parseData['response']['advanced_details']['sun_set'];
        return result;
      } else {
        TLoggerHelper.error("Some Error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<List<Map<String, String>>?> getPlanetDetails({
    required String dob,
    required String tob,
    required String lat,
    required String lon,
    required String lang
  }) async {
    try {
      var getPlanetDetails = await kundliApi.getPlanetDetails(
        dob: dob,
        tob: tob,
        lat: lat,
        lon: lon,
        lang:lang
      );

      final Map<String, dynamic> parseData = jsonDecode(getPlanetDetails);
      TLoggerHelper.info(parseData.entries.toString());

      if (parseData['status'] == 200) {
        final Map<String, dynamic> response = parseData['response'];
        final List<Map<String, String>> result = [];

        for (int i = 0; i < 10; i++) {
          String degreeFormatted =
              TFormatter.formatDegree(response[i.toString()]['global_degree']);
          result.add({
            "name": response[i.toString()]['full_name'].toString(),
            "sign": response[i.toString()]['zodiac'].toString(),
            "sign\nlord": response[i.toString()]['zodiac_lord'].toString(),
            "degree": degreeFormatted,
            "house": response[i.toString()]['house'].toString(),
          });
        }

        return result;
      } else {
        TLoggerHelper.error("Some error occurred !!");
        return null; // Return null or handle error as per your requirement
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return null; // Return null or handle error as per your requirement
    }
  }

  Future<List<Map<String, String>>?> getKPPlanetsDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lon,
      required String lang}) async {
    try {
      var getPlanetDetails = await kundliApi.getKPPlanetsDetails(
        dob: dob,
        lang: lang,
        tob: tob,
        lat: lat,
        lon: lon,
      );

      final Map<String, dynamic> parseData = jsonDecode(getPlanetDetails);
      TLoggerHelper.info(parseData.entries.toString());

      if (parseData['status'] == 200) {
        final Map<String, dynamic> response = parseData['response'];
        final List<Map<String, String>> result = [];

        for (int i = 0; i < 10; i++) {
          result.add({
            "planet": response[i.toString()]['pseudo_rasi_lord'].toString(),
            "cusp": response[i.toString()]['house'].toString(),
            "sign": response[i.toString()]['pseudo_rasi'].toString(),
            "sign\nlord":
                response[i.toString()]['pseudo_nakshatra_lord'].toString(),
            "star\nlord": response[i.toString()]['sub_lord'].toString(),
            "sub\nlord": response[i.toString()]['sub_sub_lord'].toString(),
          });
        }

        return result;
      } else {
        TLoggerHelper.error("Some error occurred !!");
        return null; // Return null or handle error as per your requirement
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return null; // Return null or handle error as per your requirement
    }
  }

  Future<List<Map<String, String>>?> getKPCuspsDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lon,
      required String lang}) async {
    try {
      var getPlanetDetails = await kundliApi.getKPCuspsDetails(
          dob: dob, tob: tob, lat: lat, lon: lon, lang: lang);

      final Map<String, dynamic> parseData = await jsonDecode(getPlanetDetails);
      TLoggerHelper.info(parseData.entries.toString());

      if (parseData['status'] == 200) {
        final List<dynamic> response = parseData['response'];
        final List<Map<String, String>> result = [];

        for (int i = 0; i < response.length; i++) {
          String degreeFormatted =
              double.parse(response[i]['local_start_degree'].toString())
                  .toStringAsFixed(2);
          result.add({
            "cusp": response[i]['house'].toString(),
            "degree": degreeFormatted,
            "sign": response[i]['start_rasi'].toString(),
            "sign\nlord": response[i]['start_nakshatra_lord'].toString(),
            "star\nlord": response[i]['cusp_sub_lord'].toString(),
            "sub\nlord": response[i]['cusp_sub_sub_lord'].toString()
          });
        }

        return result;
      } else {
        TLoggerHelper.error("Some error occurred !!");
        return null; // Return null or handle error as per your requirement
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return null; // Return null or handle error as per your requirement
    }
  }

  Future<AshtakVargaData?> getAshtakvargaDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    try {
      var getAshtakvargaDetails = await kundliApi.getAshtakVargaDetails(
          dob: dob, tob: tob, lat: lat, lon: lon,lang: lang);
      final Map<String, dynamic> parseData =
          await jsonDecode(getAshtakvargaDetails);
      TLoggerHelper.info(parseData.entries.toString());
      if (parseData['status'] == 200) {
        AshtakvargaChartModel ashtakvargaChartModel =
            AshtakvargaChartModel.fromJson(parseData);
        return ashtakvargaChartModel.response;
      } else {
        TLoggerHelper.error("Some error occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<Map<String, String>?> getManglikAnalysis(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    try {
      var getManglikAnalysis = await kundliApi.getManglikAnalysis(
          dob: dob, tob: tob, lat: lat, lon: lon,lang: lang);
      final Map<String, dynamic> parseData =
          await jsonDecode(getManglikAnalysis);
      TLoggerHelper.info(parseData.entries.toString());
      TLoggerHelper.info(parseData.entries.toString());
      if (parseData['status'] == 200) {
        String data = "";
        parseData['response']['factors']
            .forEach((element) => data += element + " ");
        data += parseData['response']['bot_response'];
        return {parseData['response']['manglik_by_mars'] ? "Yes" : "No": data};
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<Map<String, String>?> getKalsarpaAnalysis(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    try {
      var getManglikAnalysis = await kundliApi.getKalsarpaAnalysis(
          dob: dob, tob: tob, lat: lat, lon: lon,lang: lang);
      final Map<String, dynamic> parseData =
          await jsonDecode(getManglikAnalysis);
      TLoggerHelper.info(parseData.entries.toString());
      if (parseData['status'] == 200) {
        return {
          parseData['response']['is_dosha_present'] ? "Yes" : "No":
              parseData['response']['bot_response'].toString()
        };
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<List<SadesatiTableList>?> getSadeSatiDetails(
      {required String dob,
      required String tob,
      required String lat,
      required String lang,
      required String lon}) async {
    try {
      var getSadeSatiDetails = await kundliApi.getSadeSatiDetails(
          dob: dob, tob: tob, lat: lat, lon: lon,lang: lang);
      final Map<String, dynamic> parseData =
          await jsonDecode(getSadeSatiDetails);
      if (parseData['status'] == 200) {
        SadeSatiDetailsModel sadeSatiDetailsModel =
            SadeSatiDetailsModel.fromJson(parseData);
        return sadeSatiDetailsModel.sadesatiTableList;
      } else {
        TLoggerHelper.error("Some Error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<MatchMakingModel?> getMatchMakingDetails({
    required String boyDob,
    required String boyTob,
    required String boyLat,
    required String boyLon,
    required String girlDob,
    required String girlTob,
    required String girlLat,
    required String girlLon,
  }) async {
    try {
      var getMatchMakingDetails = await kundliApi.getMatchMakingDetails(
          boyDob: boyDob,
          boyTob: boyTob,
          boyLat: boyLat,
          boyLon: boyLon,
          girlDob: girlDob,
          girlTob: girlTob,
          girlLat: girlLat,
          girlLon: girlLon);
      final Map<String, dynamic> parseData =
          await jsonDecode(getMatchMakingDetails);
      bool isHttpStatus = parseData['status'];
      if (isHttpStatus) {
        MatchMakingModel matchMakingModel =
            MatchMakingModel.fromJson(getMatchMakingDetails);
        return matchMakingModel;
      } else {
        TLoggerHelper.error("Some error Occured !!");
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<String?> getChart(
      {required String lat,
      required String lon,
      required String lang,
      required String dateTime,
      required String chartType,
      bool isSouthIndian = false}) async {
    try {
      var getChart = await kundliApi.getChart(
          lat: lat,
          lon: lon,
          dateTime: dateTime,
          chartType: chartType,
          isSouthIndian: isSouthIndian,
          lang: lang);

      return getChart;
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<List<DashaPeriod>?> getDashaData(
      {required String lat,
      required String lon,
      required String lang,
      required String dateTime}) async {
    try {
      var getDashaData =
          await kundliApi.getDashaData(lat: lat, lon: lon, dateTime: dateTime,lang:lang);
      final Map<String, dynamic> parseData = await jsonDecode(getDashaData);
      DashaModel dashaModel = DashaModel.fromJson(parseData);
      return dashaModel.data.dashaPeriods;
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }
}
