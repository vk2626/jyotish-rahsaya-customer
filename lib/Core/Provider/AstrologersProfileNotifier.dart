// import 'dart:convert';
//
// import 'package:dio/dio.dart';
// import 'package:flutter/cupertino.dart';
// import '../Model/astrologersDetails.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// import '../Api/ApiServices.dart';
// import '../Api/Constants.dart';
//
// class AstrologersProfileNotifier with ChangeNotifier {
//   List<Astrologers>? astrologers = [];
//
//   List<Astrologers>? get getAstrologerDetail => astrologers;
//   var isApiHit = false;
//   bool noUserFound = true;
//
//   void getProfileDetails(BuildContext context, String astroId) async {
//     DioClient? client;
//     client = DioClient();
//     Dio dio = await client!.getClient();
//     final pref = await SharedPreferences.getInstance();
//     var params = jsonEncode({
//       'user_id': pref.getInt(Constants.tempUserID),
//       'astrologer_id': astroId
//     });
//     AstrologersDetails? astrologersDetails =
//         await client?.astrologerDetails(dio, Constants.astroDetails, params);
//     print("astrologersDetails--> $astrologersDetails");
//     if (astrologersDetails!.status == true) {
//       astrologers = astrologersDetails.astrologers;
//       notifyListeners();
//     }
//   }
// }
