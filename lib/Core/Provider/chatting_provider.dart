import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import '../helper_functions.dart';
import '../../handler_classes/secure_storage.dart';

import '../../utils/snack_bar.dart';

import '../Api/chatting_api.dart';
import '../Model/ChatModel.dart';
import '../logger_helper.dart';

class ChattingProvider with ChangeNotifier {
  final ChattingAPIs _chattingAPIs = ChattingAPIs();
  final _secureStorage = SecureStorage();
  var headers = {'Accept': 'application/json'};

  Future<ChatModel?> getMessages(
      {required BuildContext context,
      required String fromId,
      required String toId,
      required String infoId,
      required int pageNumber}) async {
    try {
      var getMessages = await _chattingAPIs.getMessages(
          fromId: fromId, toId: toId, pageNumber: pageNumber, infoId: infoId);
      final Map<String, dynamic> parseData = await jsonDecode(getMessages);
      TLoggerHelper.info(parseData.toString());
      ChatModel chatModel = ChatModel.fromJson(parseData);
      bool isHttpStatus = chatModel.status;
      if (isHttpStatus) {
        return chatModel;
      } else {
        showSnackBarWidget(context, "Sever Error");
      }
      return null;
    } on HttpException catch (e) {
      TLoggerHelper.error(e.message);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<Message?> getMessageFromID({required String chatId}) async {
    try {
      var getmessagefromId =
          await _chattingAPIs.getMessageFromID(chatId: chatId);
      final Map<String, dynamic> parseData = await jsonDecode(getmessagefromId);
      TLoggerHelper.info(parseData.entries.toString());
      bool isHttpStatus = parseData['status'];
      if (isHttpStatus) {
        return Message.fromJson(parseData['message']);
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  // Future<bool> endChatByAstrologer({required String communicationId}) async {
  //   try {
  //     var endChatByAstrologer = await _chattingAPIs.endChatByAstrologer(
  //         communicationId: communicationId);
  //     final Map<String, dynamic> parseData =
  //         await jsonDecode(endChatByAstrologer);
  //     TLoggerHelper.info(parseData.entries.toString());
  //     if (parseData['status']) {
  //       return true;
  //     }
  //   } catch (e) {
  //     TLoggerHelper.error(e.toString());
  //   }
  //   return false;
  // }

  Future<Map<bool, String>> sendPoojaMessage(
      {required BuildContext context,
      required String textBody,
      required String fromId,
      bool isPooja = true,
      required String toId,
      required String imagePath,
      required String orderId,
      required dynamic referenceChatId}) async {
    try {
      var sendMessage = await _chattingAPIs.sendPoojaMessage(
          textBody: textBody,
          toId: toId,
          fromId: fromId,
          orderId: orderId,
          isPooja: isPooja,
          imagePath: imagePath,
          referenceChatId: referenceChatId);
      print('params${sendMessage}');
      final Map<String, dynamic> parseData = await jsonDecode(sendMessage);
      String isHttpStatus = parseData['status'];
      TLoggerHelper.debug(parseData.entries.toString());
      if (isHttpStatus == "200") {
        return {true: parseData['chatId'].toString()};
      } else {
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        showSnackBarWidget(context, "Server Error occured !!");
      }
      return {false: ""};
    } on HttpException catch (e) {
      TLoggerHelper.error(e.message);
    } catch (e) {
      print("object" + e.toString());
      TLoggerHelper.error(e.toString());
    }
    return {false: ""};
  }

  Future<ChatModel?> getPoojaMessages(
      {required BuildContext context,
      required String fromId,
      required String toId,
      required String orderId,
      required bool isPooja,
      required int pageNumber}) async {
    try {
      var getMessages = await _chattingAPIs.getPoojaMessages(
          isPooja: isPooja,
          fromId: fromId,
          toId: toId,
          pageNumber: pageNumber,
          orderId: orderId);
      final Map<String, dynamic> parseData = await jsonDecode(getMessages);
      TLoggerHelper.info(parseData.toString());
      ChatModel chatModel = ChatModel.fromJson(parseData);
      bool isHttpStatus = chatModel.status;
      if (isHttpStatus) {
        return chatModel;
      } else {
        showSnackBarWidget(context, "Sever Error");
      }
      return null;
    } on HttpException catch (e) {
      TLoggerHelper.error(e.message);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return null;
  }

  Future<bool> sendSeenStatus(
      {required String fromId,
      required String toId,
      required String chatInfoId}) async {
    try {
      var sendSeenStatus = await _chattingAPIs.sendSeenStatus(
          fromId: fromId, toId: toId, chatInfoId: chatInfoId);
      final Map<String, dynamic> parseData = await jsonDecode(sendSeenStatus);
      if (parseData['status']) {
        return true;
      } else {
        TLoggerHelper.error(parseData['message']);
        return false;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return false;
    }
  }

  // Future<bool> sendSeenStatusNew(
  //     {required String fromId,
  //     required String toId,
  //     required String chatInfoId}) async {
  //   try {
  //     var sendSeenStatus = await _chattingAPIs.sendSeenStatusNew(
  //         fromId: fromId, toId: toId, chatInfoId: chatInfoId);
  //     final Map<String, dynamic> parseData = await jsonDecode(sendSeenStatus);
  //     if (parseData['status']) {
  //       return true;
  //     } else {
  //       TLoggerHelper.error(parseData['message']);
  //       return false;
  //     }
  //   } catch (e) {
  //     TLoggerHelper.error(e.toString());
  //     return false;
  //   }
  // }

  Future<bool> sendSeenPoojaStatus(
      {required String fromId,
      required String toId,
      required bool isPooja,
      required String orderId}) async {
    try {
      var sendSeenStatus = await _chattingAPIs.sendSeenStatusPooja(
          fromId: fromId, toId: toId, orderId: orderId,isPooja: isPooja);
      final Map<String, dynamic> parseData = await jsonDecode(sendSeenStatus);
      if (parseData['status']) {
        return true;
      } else {
        TLoggerHelper.error(parseData['message']);
        return false;
      }
    } catch (e) {
      TLoggerHelper.error(e.toString());
      return false;
    }
  }

  Future<Map<bool, String>> sendMessage(
      {required BuildContext context,
      required String textBody,
      required String fromId,
      required String toId,
      required String imagePath,
      required String infoId,
      required dynamic referenceChatId}) async {
    try {
      // TLoggerHelper.debug(
      //     "TextBody:- $textBody\nFromId:- $fromId\nToId:- $toId\nImagePath:- $imagePath\nReferenceId:- $referenceChatId");
      var sendMessage = await _chattingAPIs.sendMessage(
          textBody: textBody,
          toId: toId,
          fromId: fromId,
          imagePath: imagePath,
          infoId: infoId,
          referenceChatId: referenceChatId);
      final Map<String, dynamic> parseData = await jsonDecode(sendMessage);
      String isHttpStatus = parseData['status'];
      TLoggerHelper.debug(parseData.entries.toString());
      if (isHttpStatus == "200") {
        return {true: parseData['chatId'].toString()};
      } else if (isHttpStatus == "403" && parseData['is_chat_ended'] == "1") {
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }

        showSnackBarWidget(context, "Chat Ended !!", isTrue: true);
      } else {
        showSnackBarWidget(context, "Server Error occured !!");
      }
      return {false: ""};
    } on HttpException catch (e) {
      TLoggerHelper.error(e.message);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
    return {false: ""};
  }

  // Future<Map<bool, String>> sendMessageNew(
  //     {required BuildContext context,
  //     required String textBody,
  //     required String fromId,
  //     required String toId,
  //     required String imagePath,
  //     required String infoId,
  //     required dynamic referenceChatId}) async {
  //   try {
  //     // TLoggerHelper.debug(
  //     //     "TextBody:- $textBody\nFromId:- $fromId\nToId:- $toId\nImagePath:- $imagePath\nReferenceId:- $referenceChatId");
  //     var sendMessage = await _chattingAPIs.sendMessageNew(
  //         textBody: textBody,
  //         toId: toId,
  //         fromId: fromId,
  //         imagePath: imagePath,
  //         infoId: infoId,
  //         referenceChatId: referenceChatId);
  //     final Map<String, dynamic> parseData = await jsonDecode(sendMessage);

  //     String isHttpStatus = parseData['status'];
  //     TLoggerHelper.debug(parseData.entries.toString());
  //     if (isHttpStatus == "200") {
  //       return {true: parseData['chatId'].toString()};
  //     } else if (isHttpStatus == "403" && parseData['is_chat_ended'] == "1") {
  //       if (Navigator.of(context).canPop()) {
  //         Navigator.of(context).pop();
  //       }
  //       if (Navigator.of(context).canPop()) {
  //         Navigator.of(context).pop();
  //       }

  //       showSnackBarWidget(context, "Chat Ended !!", isTrue: true);
  //     } else {
  //       showSnackBarWidget(context, "Server Error occured !!");
  //     }
  //     return {false: ""};
  //   } on HttpException catch (e) {
  //     TLoggerHelper.error(e.message);
  //   } catch (e) {
  //     TLoggerHelper.error(e.toString());
  //   }
  //   return {false: ""};
  // }

  // Future sendMessageNew(String message, String toId) async {
  //   try {
  //     // Fetch Bearer token from SecureStorage
  //     final accessToken = await _secureStorage.getAccessToken();

  //     if (accessToken == null || accessToken.isEmpty) {
  //       TLoggerHelper.error("Access Token is not available.");
  //       return;
  //     }

  //     final Map<String, String> body = {
  //       "from_id": await THelperFunctions.getUserId(),
  //       "to_id": toId,
  //       "body": message,
  //       "chat_info_id": "1406"
  //     };

  //     // Set headers with Bearer token
  //     final headers = {
  //       "Accept": "application/json",
  //       "Authorization": "Bearer $accessToken"
  //     };

  //     // Use Dio to send the POST request
  //     final dio = Dio();
  //     final response = await dio.post(
  //       "https://jyotish.techsaga.live/api/send-message1",
  //       data: body,
  //       options: Options(headers: headers),
  //     );

  //     // Log success message
  //     if (response.statusCode == 200) {
  //       TLoggerHelper.info("Message sent successfully: $message");
  //     } else {
  //       TLoggerHelper.error(
  //           "Failed to send message: ${response.statusMessage}");
  //     }
  //   } catch (e) {
  //     TLoggerHelper.error("Error sending message: $e");
  //   }
  // }

  Future<bool> resumeChat(
      {required String communication_id, required bool isResumed}) async {
    try {
      final resumeData = await _chattingAPIs.resumeChat(
          communicationid: communication_id, isResume: isResumed);
      final Map<String, dynamic> parseData = jsonDecode(resumeData);
      TLoggerHelper.info(
          " Chat Resume Response ${parseData.entries.toString()}");
      return parseData['status'];
    } catch (e) {
      TLoggerHelper.error("Resume Chat Error${e.toString()}");
    }
    return false;
  }

  Future isNotAttended(
      {required BuildContext context, required String communicationId}) async {
    try {
      await _chattingAPIs.isNotAttended(communicationId: communicationId);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future sendChatDuration({required String communicationId}) async {
    try {
      await _chattingAPIs.sendChatDuration(communicationId: communicationId);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  // Future sendChatDurationNew(
  //     {required String communicationId, required String infoId}) async {
  //   try {
  //     await _chattingAPIs.sendChatDurationNew(
  //         communicationId: communicationId, infoId: infoId);
  //   } catch (e) {
  //     TLoggerHelper.error(e.toString());
  //   }
  // }

  Future sendChatDurationResume(
      {required String communicationId,
      required bool isResume,
      required String chatDuration}) async {
    try {
      await _chattingAPIs.sendChatDurationResume(
          communicationId: communicationId,
          isResume: isResume,
          chatDuration: chatDuration);
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  // Future sendChatDurationResumeNew(
  //     {required String communicationId,
  //     required bool isResume,
  //     required String chatDuration}) async {
  //   try {
  //     await _chattingAPIs.sendChatDurationResumeNew(
  //         communicationId: communicationId,
  //         isResume: isResume,
  //         chatDuration: chatDuration);
  //   } catch (e) {
  //     TLoggerHelper.error(e.toString());
  //   }
  // }
}
