class Language {
  final int id;
  final String name;
  final String languageCode;

  Language(this.id, this.name, this.languageCode);

  static List<Language> languageList() {
    return <Language>[
      Language(0, "English", "en"),
      Language(1, "हिंदी", "hi"),
      Language(2, "ਤੇਲਗੂ", "pa"),
      Language(3, "मराठी", "mr"),
      Language(4, "తెలుగు", "te"),
      Language(5, "ಕನ್ನಡ", "kn"),
      Language(6, "বাংলা", "bn"),
    ];
  }
}
