import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String LAGUAGE_CODE = 'languageCode';
const String LAGUAGE_NAME = 'languageName';

//languages code
const String ENGLISH = 'en';
const String HINDI = 'hi';
const String BENGALI = 'bn';
const String KANNADA = 'kn';
const String MARATHI = 'mr';
const String PUNJABI = 'pa';
const String TELUGU = 'te';

Future<Locale> setLocale(String languageCode) async {
  try {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString(LAGUAGE_CODE, languageCode);
  } catch (e) {
    print("Error initializing SharedPreferences: $e");
  }
  return _locale(languageCode);
}

Future<Locale> getLocale() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String? languageCode = _prefs.getString(LAGUAGE_CODE);
  print('Retrieved language code: $languageCode');
  return _locale(languageCode??'en');
}

Locale _locale(String languageCode) {
  switch (languageCode) {
    case ENGLISH:
      return const Locale(ENGLISH, '');
    case HINDI:
      return const Locale(HINDI, '');
    case PUNJABI:
      return const Locale(PUNJABI, '');
    case MARATHI:
      return const Locale(MARATHI, '');
    case TELUGU:
      return const Locale(TELUGU, '');
    case KANNADA:
      return const Locale(KANNADA, '');
    case BENGALI:
      return const Locale(BENGALI, '');
    default:
      return const Locale(ENGLISH, '');
  }
}

AppLocalizations translation(BuildContext context) {
  return AppLocalizations.of(context)!;
}
  