import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../Api/HttpClient.dart';
import '../helper_functions.dart';
import '../logger_helper.dart';
import 'package:provider/provider.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../Api/Constants.dart';
import '../Key/app.key.dart';
import '../Provider/list.of.api.provider.dart';

class PaymentServices {
  Razorpay razorpay = Razorpay();

  PaymentServices(
      {required BuildContext context,
      required String this.orderId,
      required this.razorpayId,
      required this.razorpaySecretId,
      this.astroPoojaId,
      this.suggestedAstrologerId,
      this.remedy_id});

  String orderId;

  String razorpayId;
  String razorpaySecretId;
  String? remedy_id;

  String? astroPoojaId;
  String? suggestedAstrologerId;
  initializeRazorPay(
      {required BuildContext context,
      String? astroPoojaId,
      String? suggestedAstrologerId}) {
    razorpay.on(
        Razorpay.EVENT_PAYMENT_SUCCESS,
        (response) => _handlePaymentSuccess(
            response, context, astroPoojaId, suggestedAstrologerId));
    razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response, context,
      String? astroPoojaId, String? suggestedAstrologerId) {
    Fluttertoast.showToast(msg: "SUCCESS: " + response.paymentId!)
        .whenComplete(() {
      Provider.of<ListOfApisProvider>(context, listen: false)
          .getSuccessProvider(
              context: context,
              razorpayId: razorpayId,
              razorpaySecretId: razorpaySecretId,
              // amountID: amountID.toString(),

              orderId: orderId,
              suggestedAstrologerId: suggestedAstrologerId,
              remedy_id: remedy_id,
              astroProductId: astroPoojaId,
              transactionID: "${response.paymentId.toString()}");
    }).then((value) {
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).pop();
      }
    });
  }

  void _handlePaymentError(PaymentFailureResponse response) async {
    log(response.error.toString());
    log(response.message.toString());

    Fluttertoast.showToast(msg: "Payment Failed");
    await THelperFunctions.httpPostHandler(
        client: http.Client(),
        endPointUrl: "order-payment",
        body: {
          "order_id": orderId,
          "payment_status": "failed",
          "is_success": "2"
        });
  }

  Future checkMeOUT({
    required BuildContext context,
    required String rechargeAmount,
    required String astroName,
    required String orderId,
    required String razorypayDesc,
  }) async {
    TLoggerHelper.error(
        "${remedy_id} ${remedy_id} SuggestedAstroID ${suggestedAstrologerId}");
    initializeRazorPay(
        context: context,
        astroPoojaId: suggestedAstrologerId != null ? astroPoojaId : remedy_id,
        suggestedAstrologerId: suggestedAstrologerId);
    final prefs = await SharedPreferences.getInstance();
    TLoggerHelper.info(rechargeAmount);
    int amount = double.parse(rechargeAmount).toInt();
    var options = {
      'key': razorpayId,
      'amount': amount * 100,
      'name': astroName.toLowerCase(),
      'description': razorypayDesc,
      'prefill': {
        'contact': '${prefs.getString(Constants.mobile)}',
        'email': '${prefs.getString(Constants.mobile)}'
      },
      "order_id": orderId
    };
    TLoggerHelper.info(options.entries.toString());
    try {
      razorpay.open(options);
    } catch (e) {
      debugPrint("RazorPay Error---->${e.toString()}");
    }
  }

  disposeRazorPay() {
    razorpay.clear();
  }

  showResponse({required BuildContext context, required String response}) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox(
            height: 100,
            width: double.infinity,
            child: Text(
              'This Is Response $response',
              style: const TextStyle(),
            ),
          );
        });
  }
}
