import 'package:firebase_remote_config/firebase_remote_config.dart';

import 'logger_helper.dart';

class FirebaseRemoteConfigService {
  FirebaseRemoteConfigService._()
      : _remoteConfig = FirebaseRemoteConfig.instance;

  static FirebaseRemoteConfigService? _instance;
  factory FirebaseRemoteConfigService() =>
      _instance ??= FirebaseRemoteConfigService._();

  final FirebaseRemoteConfig _remoteConfig;

  // Initialize Remote Config
  Future<void> initialize() async {
    try {
      await _remoteConfig.setConfigSettings(RemoteConfigSettings(
        fetchTimeout: const Duration(seconds: 10),
        minimumFetchInterval: const Duration(seconds: 0),
      ));
      await _remoteConfig.fetchAndActivate().then((value) {
        TLoggerHelper.debug(value.toString());
      });
      _setDefaults();
    } catch (e) {
      TLoggerHelper.error(e.toString());
    }
  }

  Future<void> _setDefaults() async => _remoteConfig.setDefaults(
        const {
          "login_url": "https://admin.jyotishrahsya.com/api/",
          "base_url": "https://admin.jyotishrahsya.com/api/auth/",
        },
      );

  // Helper methods to get values
  String getString(String key) => _remoteConfig.getString(key);
  bool getBool(String key) => _remoteConfig.getBool(key);
  int getInt(String key) => _remoteConfig.getInt(key);
  double getDouble(String key) => _remoteConfig.getDouble(key);
}
