// To parse this JSON data, do
//
//     final sendReviewModel = sendReviewModelFromJson(jsonString);

import 'dart:convert';

SendReviewModel sendReviewModelFromJson(String str) =>
    SendReviewModel.fromJson(json.decode(str));

String sendReviewModelToJson(SendReviewModel data) =>
    json.encode(data.toJson());

class SendReviewModel {
  bool status;
  String message;
  Data data;

  SendReviewModel({
    required this.status,
    required this.message,
    required this.data,
  });

  factory SendReviewModel.fromJson(Map<String, dynamic> json) =>
      SendReviewModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  String orderId;
  String rate;
  String description;
  String customerId;
  String astrologerId;
  // String isLike;
  String type;
  int isHide;
  int status;
  DateTime createdAt;
  DateTime updatedAt;
  int id;

  Data({
    required this.orderId,
    required this.rate,
    required this.description,
    required this.customerId,
    required this.astrologerId,
    // required this.isLike,
    required this.type,
    required this.isHide,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.id,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        orderId: json["order_id"],
        rate: json["rate"],
        description: json["description"],
        customerId: json["customer_id"],
        astrologerId: json["astrologer_id"],
        // isLike: json["is_like"],
        type: json["type"],
        isHide: json["is_hide"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "order_id": orderId,
        "rate": rate,
        "description": description,
        "customer_id": customerId,
        "astrologer_id": astrologerId,
        // "is_like": isLike,
        "type": type,
        "is_hide": isHide,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "id": id,
      };
}
