// To parse this JSON data, do
//
//     final poojaOrderModel = poojaOrderModelFromJson(jsonString);

import 'dart:convert';

PoojaOrderModel poojaOrderModelFromJson(String str) =>
    PoojaOrderModel.fromJson(json.decode(str));

String poojaOrderModelToJson(PoojaOrderModel data) =>
    json.encode(data.toJson());

class PoojaOrderModel {
  bool status;
  List<PoojaList> poojaList;

  PoojaOrderModel({
    required this.status,
    required this.poojaList,
  });

  factory PoojaOrderModel.fromJson(Map<String, dynamic> json) =>
      PoojaOrderModel(
        status: json["status"],
        poojaList: List<PoojaList>.from(
            json["poojaList"].map((x) => PoojaList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "poojaList": List<dynamic>.from(poojaList.map((x) => x.toJson())),
      };
}

class PoojaList {
  int id;
  String userId;
  // String productPrice;
  // String customerDId;
  String astrologerId;
  int poojaId;
  String paymentId;
  String paymentType;
  String cashback;
  String value;
  String totalValue;
  String walletDeducted;
  String gstAmount;
  String remark;
  int isSuccess;
  String title;
  String image;
  String name;
  String avatar;
  DateTime? poojaTime;
  int orderPoojaStatus;
  DateTime orderDate;
  String orderPoojaId;
  String connectToken;
  String channelName;

  PoojaList({
    required this.id,
    required this.userId,
    // required this.productPrice,
    // required this.customerDId,
    required this.astrologerId,
    required this.poojaId,
    required this.paymentId,
    required this.paymentType,
    required this.cashback,
    required this.value,
    required this.totalValue,
    required this.walletDeducted,
    required this.gstAmount,
    required this.remark,
    required this.isSuccess,
    required this.title,
    required this.image,
    required this.name,
    required this.avatar,
    required this.poojaTime,
    required this.orderPoojaStatus,
    required this.connectToken,
    required this.channelName,
    required this.orderDate,
    required this.orderPoojaId,
  });

  factory PoojaList.fromJson(Map<String, dynamic> json) => PoojaList(
        id: json["id"],
        userId: json["user_id"],
        // productPrice: json["product_price"],
        // customerDId: json["customer_d_id"],
        astrologerId: json["astrologer_id"],
        poojaId: json["pooja_id"],
        paymentId: json["payment_id"],
        paymentType: json["payment_type"],
        cashback: json["cashback"],
        value: json["value"],
        totalValue: json["total_value"],
        walletDeducted: json["wallet_deducted"],
        gstAmount: json["gst_amount"],
        remark: json["remark"],
        isSuccess: json["is_success"],
        title: json["product_name"],
        image: json["image"] ?? "",
        name: json["name"],
        orderPoojaStatus: json['order_pooja_status'] ?? 0,
        avatar: json["avatar"],
        poojaTime: json['pooja_time'] != null
            ? DateTime.parse(json["pooja_time"])
            : null,
        connectToken: json["connect_token"] ?? "",
        channelName: json["channel_name"] ?? "",
        orderDate: DateTime.parse(json["order_date"]),
        orderPoojaId: json["order_pooja_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        // "product_price": productPrice,
        // "customer_d_id": customerDId,
        "astrologer_id": astrologerId,
        "pooja_id": poojaId,
        "payment_id": paymentId,
        "payment_type": paymentType,
        "cashback": cashback,
        "value": value,
        "total_value": totalValue,
        "wallet_deducted": walletDeducted,
        "gst_amount": gstAmount,
        "remark": remark,
        "is_success": isSuccess,
        "title": title,
        "image": image,
        "name": name,
        "avatar": avatar,
        "pooja_time": poojaTime!.toIso8601String(),
        "connect_token": connectToken,
        "channel_name": channelName,
        "order_date": orderDate.toIso8601String(),
        "order_pooja_id": orderPoojaId,
      };
}
