// To parse this JSON data, do
//
//     final goLiveModel = goLiveModelFromJson(jsonString);

import 'dart:convert';

GoLiveModel goLiveModelFromJson(String str) => GoLiveModel.fromJson(json.decode(str));

String goLiveModelToJson(GoLiveModel data) => json.encode(data.toJson());

class GoLiveModel {
    bool status;
    IsOnline isOnline;
    String message;

    GoLiveModel({
        required this.status,
        required this.isOnline,
        required this.message,
    });

    factory GoLiveModel.fromJson(Map<String, dynamic> json) => GoLiveModel(
        status: json["status"],
        isOnline: IsOnline.fromJson(json["is_online"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "is_online": isOnline.toJson(),
        "message": message,
    };
}

class IsOnline {
    int id;
    String astrologerId;
    String liveToken;
    String channelName;
    DateTime createdAt;
    String isOnline;

    IsOnline({
        required this.id,
        required this.astrologerId,
        required this.liveToken,
        required this.channelName,
        required this.createdAt,
        required this.isOnline,
    });

    factory IsOnline.fromJson(Map<String, dynamic> json) => IsOnline(
        id: json["id"],
        astrologerId: json["astrologer_id"],
        liveToken: json["live_token"],
        channelName: json["channel_name"],
        createdAt: DateTime.parse(json["created_at"]),
        isOnline: json["is_online"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "astrologer_id": astrologerId,
        "live_token": liveToken,
        "channel_name": channelName,
        "created_at": createdAt.toIso8601String(),
        "is_online": isOnline,
    };
}
