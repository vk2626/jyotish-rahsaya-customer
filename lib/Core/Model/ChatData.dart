


class ChatData{
  String messageText;
  String imageURL;
  String time;
  int fromId;
  String attachmentFile;
  ChatData({required this.messageText,required this.imageURL,required this.time, required this.fromId, required this.attachmentFile});
}