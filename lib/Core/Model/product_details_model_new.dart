// To parse this JSON data, do
//
//     final productDetailsModelNew = productDetailsModelNewFromJson(jsonString);

import 'dart:convert';

ProductDetailsModelNew productDetailsModelNewFromJson(String str) =>
    ProductDetailsModelNew.fromJson(json.decode(str));

String productDetailsModelNewToJson(ProductDetailsModelNew data) =>
    json.encode(data.toJson());

class ProductDetailsModelNew {
  bool status;
  List<Datum> data;

  ProductDetailsModelNew({
    required this.status,
    required this.data,
  });

  factory ProductDetailsModelNew.fromJson(Map<String, dynamic> json) =>
      ProductDetailsModelNew(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  // String astrologerId;
  String title;
  String category;
  String image;
  String productLabel;
  String price;
  String shortDescription;
  String youtubeUrl;
  List<Heading> heading;
  int status;

  Datum({
    required this.id,
    // required this.astrologerId,
    required this.title,
    required this.category,
    required this.image,
    required this.productLabel,
    required this.price,
    required this.shortDescription,
    required this.youtubeUrl,
    required this.heading,
    required this.status,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        // astrologerId: json["astrologer_id"],
        title: json["title"],
        category: json["category"],
        image: json["image"],
        productLabel: json["product_label"],
        price: json["price"],
        shortDescription: json["short_description"],
        youtubeUrl: json["youtube_url"],
        heading:
            List<Heading>.from(json["heading"].map((x) => Heading.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        // "astrologer_id": astrologerId,
        "title": title,
        "category": category,
        "image": image,
        "product_label": productLabel,
        "price": price,
        "short_description": shortDescription,
        "youtube_url": youtubeUrl,
        "heading": List<dynamic>.from(heading.map((x) => x.toJson())),
        "status": status,
      };
}

class Heading {
  String heading;
  String description;

  Heading({
    required this.heading,
    required this.description,
  });

  factory Heading.fromJson(Map<String, dynamic> json) => Heading(
        heading: json["heading"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "heading": heading,
        "description": description,
      };
}
