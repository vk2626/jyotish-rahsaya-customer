// To parse this JSON data, do
//
//     final jyotishMallModel = jyotishMallModelFromJson(jsonString);

import 'dart:convert';

JyotishMallModel jyotishMallModelFromJson(String str) =>
    JyotishMallModel.fromJson(json.decode(str));

String jyotishMallModelToJson(JyotishMallModel data) =>
    json.encode(data.toJson());

class JyotishMallModel {
  bool status;
  List<Datum> data;

  JyotishMallModel({
    required this.status,
    required this.data,
  });

  factory JyotishMallModel.fromJson(Map<String, dynamic> json) =>
      JyotishMallModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  int parent;
  String name;
  int jyotishStatus;
  String slug;
  String image;
  String labelCategory;
  String suggestedType;
  int status;
  DateTime createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  Datum({
    required this.id,
    required this.parent,
    required this.name,
    required this.jyotishStatus,
    required this.slug,
    required this.image,
    required this.suggestedType,
    required this.labelCategory,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        parent: json["parent"],
        name: json["name"],
        jyotishStatus: json["jyotish_status"],
        slug: json["slug"],
        suggestedType: json['suggested_type'] ?? "",
        image: json["image"],
        labelCategory: json["label_category"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "name": name,
        "jyotish_status": jyotishStatus,
        "slug": slug,
        "image": image,
        "label_category": labelCategory,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
