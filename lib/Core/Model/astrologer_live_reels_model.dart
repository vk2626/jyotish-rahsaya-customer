// To parse this JSON data, do
//
//     final astrologerLiveReelsModel = astrologerLiveReelsModelFromJson(jsonString);

import 'dart:convert';

AstrologerLiveReelsModel astrologerLiveReelsModelFromJson(String str) =>
    AstrologerLiveReelsModel.fromJson(json.decode(str));

String astrologerLiveReelsModelToJson(AstrologerLiveReelsModel data) =>
    json.encode(data.toJson());

class AstrologerLiveReelsModel {
  bool status;
  List<AstrologerOnline> astrologerOnline;

  AstrologerLiveReelsModel({
    required this.status,
    required this.astrologerOnline,
  });

  factory AstrologerLiveReelsModel.fromJson(Map<String, dynamic> json) =>
      AstrologerLiveReelsModel(
        status: json["status"],
        astrologerOnline: List<AstrologerOnline>.from(
            json["is_online"].map((x) => AstrologerOnline.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "is_online":
            List<dynamic>.from(astrologerOnline.map((x) => x.toJson())),
      };
}

class AstrologerOnline {
  int id;
  String name;
  int label;
  String avatar;
  int liveId;
  int liveCount;
  String liveToken;
  String channelName;
  bool followingUser;
  AstrologerList astrologerList;

  AstrologerOnline({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.liveId,
    required this.liveCount,
    required this.liveToken,
    required this.channelName,
    required this.followingUser,
    required this.astrologerList,
  });

  factory AstrologerOnline.fromJson(Map<String, dynamic> json) =>
      AstrologerOnline(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        liveId: json["live_id"],
        liveCount: json['live_count'],
        liveToken: json["live_token"],
        channelName: json["channel_name"],
        followingUser: json["following_user"],
        astrologerList: AstrologerList.fromJson(json["astrologerList"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "live_id": liveId,
        "live_count": liveCount,
        "live_token": liveToken,
        "channel_name": channelName,
        "following_user": followingUser,
        "astrologerList": astrologerList.toJson(),
      };
}

class AstrologerList {
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;

  AstrologerList({
    required this.callPrice,
    required this.callDiscountPrice,
    required this.videoPrice,
    required this.videoDiscountPrice,
  });

  factory AstrologerList.fromJson(Map<String, dynamic> json) => AstrologerList(
        callPrice: json["call_price"],
        callDiscountPrice: json["call_discount_price"],
        videoPrice: json["video_price"],
        videoDiscountPrice: json["video_discount_price"],
      );

  Map<String, dynamic> toJson() => {
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
      };
}
