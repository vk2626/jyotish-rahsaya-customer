// To parse this JSON data, do
//
//     final sadeSatiDetailsModel = sadeSatiDetailsModelFromJson(jsonString);

import 'dart:convert';

SadeSatiDetailsModel sadeSatiDetailsModelFromJson(String str) =>
    SadeSatiDetailsModel.fromJson(json.decode(str));

String sadeSatiDetailsModelToJson(SadeSatiDetailsModel data) =>
    json.encode(data.toJson());

class SadeSatiDetailsModel {
  int status;
  List<SadesatiTableList> sadesatiTableList;

  SadeSatiDetailsModel({
    required this.status,
    required this.sadesatiTableList,
  });

  factory SadeSatiDetailsModel.fromJson(Map<String, dynamic> json) =>
      SadeSatiDetailsModel(
        status: json["status"],
        sadesatiTableList: List<SadesatiTableList>.from(
            json["response"].map((x) => SadesatiTableList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "response":
            List<dynamic>.from(sadesatiTableList.map((x) => x.toJson())),
      };
}

class SadesatiTableList {
  bool retro;
  String startDate;
  String zodiac;
  String type;
  String dhaiya;
  String direction;
  String endDate;

  SadesatiTableList({
    required this.retro,
    required this.startDate,
    required this.zodiac,
    required this.type,
    required this.dhaiya,
    required this.direction,
    required this.endDate,
  });

  factory SadesatiTableList.fromJson(Map<String, dynamic> json) =>
      SadesatiTableList(
        retro: json["retro"],
        startDate: json["start_date"],
        zodiac: json["zodiac"]!,
        type: json["type"]!,
        dhaiya: json["dhaiya"]!,
        direction: json["direction"]!,
        endDate: json["end_date"],
      );

  Map<String, dynamic> toJson() => {
        "retro": retro,
        "start_date": startDate,
        "zodiac": zodiac,
        "type": type,
        "dhaiya": dhaiya,
        "direction": direction,
        "end_date": endDate,
      };
}
