// To parse this JSON data, do
//
//     final jyotishMallModelNew = jyotishMallModelNewFromJson(jsonString);

import 'dart:convert';

JyotishMallModelNew jyotishMallModelNewFromJson(String str) =>
    JyotishMallModelNew.fromJson(json.decode(str));

String jyotishMallModelNewToJson(JyotishMallModelNew data) =>
    json.encode(data.toJson());

class JyotishMallModelNew {
  bool status;
  List<ProductItem> data;

  JyotishMallModelNew({
    required this.status,
    required this.data,
  });

  factory JyotishMallModelNew.fromJson(Map<String, dynamic> json) =>
      JyotishMallModelNew(
        status: json["status"],
        data: List<ProductItem>.from(
            json["data"].map((x) => ProductItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ProductItem {
  int id;
  int parent;
  String name;
  String suggestedType;
  int jyotishStatus;
  String slug;
  String image;
  String labelCategory;
  int status;
  DateTime createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  ProductItem({
    required this.id,
    required this.parent,
    required this.name,
    required this.jyotishStatus,
    required this.slug,
    required this.suggestedType,
    required this.image,
    required this.labelCategory,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
  });

  factory ProductItem.fromJson(Map<String, dynamic> json) => ProductItem(
        id: json["id"],
        parent: json["parent"],
        name: json["name"],
        jyotishStatus: json["jyotish_status"],
        suggestedType: json['suggested_type']??"",
        slug: json["slug"],
        image: json["image"],
        labelCategory: json["label_category"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "name": name,
        "jyotish_status": jyotishStatus,
        "slug": slug,
        "image": image,
        "label_category": labelCategory,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
