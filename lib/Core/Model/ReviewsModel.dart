import 'dart:convert';
/// status : true
/// data : [{"id":7,"rate":"5","customer_id":"204","astrologer_id":"188","is_hide":0,"is_like":"10","description":"sdfg","reply":"","status":1,"created_at":"2024-02-06 19:16:27","updated_at":"2024-02-06 19:16:27","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024020211445161.jpg","customer_name":"Francesca Bass"},{"id":8,"rate":"5","customer_id":"204","astrologer_id":"188","is_hide":0,"is_like":"10","description":"sdfg","reply":"","status":1,"created_at":"2024-03-07 15:17:50","updated_at":"2024-03-07 15:17:50","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024020211445161.jpg","customer_name":"Francesca Bass"}]

ReviewsModel reviewsModelFromJson(String str) => ReviewsModel.fromJson(json.decode(str));
String reviewsModelToJson(ReviewsModel data) => json.encode(data.toJson());
class ReviewsModel {
  ReviewsModel({
      bool? status, 
      List<Data>? data,}){
    _status = status;
    _data = data;
}

  ReviewsModel.fromJson(dynamic json) {
    _status = json['status'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  bool? _status;
  List<Data>? _data;
ReviewsModel copyWith({  bool? status,
  List<Data>? data,
}) => ReviewsModel(  status: status ?? _status,
  data: data ?? _data,
);
  bool? get status => _status;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 7
/// rate : "5"
/// customer_id : "204"
/// astrologer_id : "188"
/// is_hide : 0
/// is_like : "10"
/// description : "sdfg"
/// reply : ""
/// status : 1
/// created_at : "2024-02-06 19:16:27"
/// updated_at : "2024-02-06 19:16:27"
/// customer_image : "https://jyotish.techsaga.live/uploads/customer/2024020211445161.jpg"
/// customer_name : "Francesca Bass"

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      num? id, 
      String? rate, 
      String? customerId, 
      String? astrologerId, 
      num? isHide, 
      String? isLike, 
      String? description, 
      String? reply, 
      num? status, 
      String? createdAt, 
      String? updatedAt, 
      String? customerImage, 
      String? customerName,}){
    _id = id;
    _rate = rate;
    _customerId = customerId;
    _astrologerId = astrologerId;
    _isHide = isHide;
    _isLike = isLike;
    _description = description;
    _reply = reply;
    _status = status;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _customerImage = customerImage;
    _customerName = customerName;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _rate = json['rate'];
    _customerId = json['customer_id'];
    _astrologerId = json['astrologer_id'];
    _isHide = json['is_hide'];
    _isLike = json['is_like'];
    _description = json['description'];
    _reply = json['reply'];
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _customerImage = json['customer_image'];
    _customerName = json['customer_name'];
  }
  num? _id;
  String? _rate;
  String? _customerId;
  String? _astrologerId;
  num? _isHide;
  String? _isLike;
  String? _description;
  String? _reply;
  num? _status;
  String? _createdAt;
  String? _updatedAt;
  String? _customerImage;
  String? _customerName;
Data copyWith({  num? id,
  String? rate,
  String? customerId,
  String? astrologerId,
  num? isHide,
  String? isLike,
  String? description,
  String? reply,
  num? status,
  String? createdAt,
  String? updatedAt,
  String? customerImage,
  String? customerName,
}) => Data(  id: id ?? _id,
  rate: rate ?? _rate,
  customerId: customerId ?? _customerId,
  astrologerId: astrologerId ?? _astrologerId,
  isHide: isHide ?? _isHide,
  isLike: isLike ?? _isLike,
  description: description ?? _description,
  reply: reply ?? _reply,
  status: status ?? _status,
  createdAt: createdAt ?? _createdAt,
  updatedAt: updatedAt ?? _updatedAt,
  customerImage: customerImage ?? _customerImage,
  customerName: customerName ?? _customerName,
);
  num? get id => _id;
  String? get rate => _rate;
  String? get customerId => _customerId;
  String? get astrologerId => _astrologerId;
  num? get isHide => _isHide;
  String? get isLike => _isLike;
  String? get description => _description;
  String? get reply => _reply;
  num? get status => _status;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  String? get customerImage => _customerImage;
  String? get customerName => _customerName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['rate'] = _rate;
    map['customer_id'] = _customerId;
    map['astrologer_id'] = _astrologerId;
    map['is_hide'] = _isHide;
    map['is_like'] = _isLike;
    map['description'] = _description;
    map['reply'] = _reply;
    map['status'] = _status;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['customer_image'] = _customerImage;
    map['customer_name'] = _customerName;
    return map;
  }

}