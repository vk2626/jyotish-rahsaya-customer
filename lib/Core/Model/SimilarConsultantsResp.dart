// To parse this JSON data, do
//
//     final similarConsultantsResp = similarConsultantsRespFromJson(jsonString);

import 'dart:convert';

SimilarConsultantsResp similarConsultantsRespFromJson(String str) =>
    SimilarConsultantsResp.fromJson(json.decode(str));

String similarConsultantsRespToJson(SimilarConsultantsResp data) =>
    json.encode(data.toJson());

class SimilarConsultantsResp {
  bool status;
  List<User> users;

  SimilarConsultantsResp({
    required this.status,
    required this.users,
  });

  factory SimilarConsultantsResp.fromJson(Map<String, dynamic> json) =>
      SimilarConsultantsResp(
        status: json["status"],
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}

class User {
  int id;
  String name;
  int label;
  String avatar;
  int isOnlineAstrologer;
  Astrologer astrologer;

  User({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.astrologer,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isOnlineAstrologer: json['is_online_astrologer'],
        astrologer: Astrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "astrologer": astrologer.toJson(),
      };
}

class Astrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  String chatPrice;
  String chatDiscountPrice;
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;
  String displayName;
  String totalDurationChat;
  String totalDurationCall;
  int rating;
  String primarySkillName;
  String allSkillName;
  String languageName;
  List<ConsultantsGallery> gallery;

  Astrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.chatPrice,
    required this.chatDiscountPrice,
    required this.callPrice,
    required this.callDiscountPrice,
    required this.videoPrice,
    required this.videoDiscountPrice,
    required this.displayName,
    required this.totalDurationChat,
    required this.totalDurationCall,
    required this.rating,
    required this.primarySkillName,
    required this.allSkillName,
    required this.languageName,
    required this.gallery,
  });

  factory Astrologer.fromJson(Map<String, dynamic> json) => Astrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        bio: json["bio"],
        freeChat: json["free_chat"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        chatPrice: json["chat_price"],
        chatDiscountPrice: json["chat_discount_price"],
        callPrice: json["call_price"],
        callDiscountPrice: json["call_discount_price"],
        videoPrice: json["video_price"],
        videoDiscountPrice: json["video_discount_price"],
        displayName: json["display_name"],
        totalDurationChat: json["total_duration_chat"],
        totalDurationCall: json["total_duration_call"],
        rating: json["rating"],
        primarySkillName: json["primary_skill_name"],
        allSkillName: json["all_skill_name"],
        languageName: json["language_name"],
        gallery: List<ConsultantsGallery>.from(
            json["gallery"].map((x) => ConsultantsGallery.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "total_duration_chat": totalDurationChat,
        "total_duration_call": totalDurationCall,
        "rating": rating,
        "primary_skill_name": primarySkillName,
        "all_skill_name": allSkillName,
        "language_name": languageName,
        "gallery": List<dynamic>.from(gallery.map((x) => x.toJson())),
      };
}

class ConsultantsGallery {
  String astrologerId;
  String image;

  ConsultantsGallery({
    required this.astrologerId,
    required this.image,
  });

  factory ConsultantsGallery.fromJson(Map<String, dynamic> json) =>
      ConsultantsGallery(
        astrologerId: json["astrologer_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "astrologer_id": astrologerId,
        "image": image,
      };
}
