// To parse this JSON data, do
//
//     final chatModel = chatModelFromJson(jsonString);

import 'dart:convert';

ChatModel chatModelFromJson(String str) => ChatModel.fromJson(json.decode(str));

String chatModelToJson(ChatModel data) => json.encode(data.toJson());

class ChatModel {
  bool status;
  Messages messages;

  ChatModel({
    required this.status,
    required this.messages,
  });

  factory ChatModel.fromJson(Map<String, dynamic> json) => ChatModel(
        status: json["status"],
        messages: Messages.fromJson(json["messages"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "messages": messages.toJson(),
      };
}

class Messages {
  int currentPage;
  List<Message> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  Messages({
    required this.currentPage,
    required this.data,
    required this.firstPageUrl,
    required this.from,
    required this.lastPage,
    required this.lastPageUrl,
    required this.nextPageUrl,
    required this.path,
    required this.perPage,
    required this.prevPageUrl,
    required this.to,
    required this.total,
  });

  factory Messages.fromJson(Map<String, dynamic> json) => Messages(
        currentPage: json["current_page"],
        data: List<Message>.from(json["data"].map((x) => Message.fromJson(x))),
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        nextPageUrl: json["next_page_url"] ?? "",
        path: json["path"] ?? "",
        perPage: json["per_page"] ?? "",
        prevPageUrl: json["prev_page_url"] ?? "",
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "next_page_url": nextPageUrl,
        "path": path,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}

class Message {
  String id;
  int fromId;
  int toId;
  dynamic? referenceChatId;
  String? body;
  String? attachment;
  int seen;
  String? messageType;
  DateTime createdAt;
  DateTime updatedAt;

  Message({
    required this.id,
    required this.fromId,
    required this.toId,
    this.referenceChatId,
    required this.body,
    required this.messageType,
    required this.attachment,
    required this.seen,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Message.fromJson(Map<String, dynamic> json) => Message(
        id: json["id"],
        fromId: json["from_id"],
        toId: json["to_id"],
        referenceChatId: json['reference_id'],
        body: json["body"] ?? "",
        attachment: json["attachment"] ?? "",
        messageType: json['message_type']??"chat",
        seen: json["seen"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "from_id": fromId,
        "to_id": toId,
        "reference_id": referenceChatId,
        "body": body,
        "attachment": attachment,
        "seen": seen,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

enum Body { BODY, EMPTY, HELLO_FROM_HEMANT, MESSAGE }

final bodyValues = EnumValues({
  "..": Body.BODY,
  ".": Body.EMPTY,
  "hello from hemant": Body.HELLO_FROM_HEMANT,
  "message": Body.MESSAGE
});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
