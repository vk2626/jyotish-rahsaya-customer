// To parse this JSON data, do
//
//     final totalSuccessPaymentModel = totalSuccessPaymentModelFromJson(jsonString);

import 'dart:convert';

TotalSuccessPaymentModel totalSuccessPaymentModelFromJson(String str) =>
    TotalSuccessPaymentModel.fromJson(json.decode(str));

String totalSuccessPaymentModelToJson(TotalSuccessPaymentModel data) =>
    json.encode(data.toJson());

class TotalSuccessPaymentModel {
  bool? status;
  Payment? payment;

  TotalSuccessPaymentModel({
    this.status,
    this.payment,
  });

  factory TotalSuccessPaymentModel.fromJson(Map<String, dynamic> json) =>
      TotalSuccessPaymentModel(
        status: json["status"],
        payment:
            json["payment"] == null ? null : Payment.fromJson(json["payment"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "payment": payment?.toJson(),
      };
}

class Payment {
  String? userId;
  int? rechargeId;
  String? transactionId;
  int? gstAmount;
  int? totalAmount;
  String? rechargeAmount;
  DateTime? currentDateTime;
  DateTime? updatedAt;
  DateTime? createdAt;
  int? id;

  Payment({
    this.userId,
    this.rechargeId,
    this.transactionId,
    this.gstAmount,
    this.totalAmount,
    this.rechargeAmount,
    this.currentDateTime,
    this.updatedAt,
    this.createdAt,
    this.id,
  });

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        userId: json["user_id"],
        rechargeId: json["recharge_id"],
        transactionId: json["transaction_id"],
        gstAmount: json["gst_amount"],
        totalAmount: json["total_amount"],
        rechargeAmount: json["recharge_amount"],
        currentDateTime: json["current_date_time"] == null
            ? null
            : DateTime.parse(json["current_date_time"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "recharge_id": rechargeId,
        "transaction_id": transactionId,
        "gst_amount": gstAmount,
        "total_amount": totalAmount,
        "recharge_amount": rechargeAmount,
        "current_date_time": currentDateTime?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "created_at": createdAt?.toIso8601String(),
        "id": id,
      };
}
