import 'dart:convert';
/// status : true
/// RecentList : {"customer_id":"185","astrologer_id":"45","product_id":"7","product_pooja_id":"4","seen":1,"updated_at":"2024-03-20T14:36:12.000000Z","created_at":"2024-03-20T14:36:12.000000Z","id":10}

AddToRecentListModel addToRecentListModelFromJson(String str) => AddToRecentListModel.fromJson(json.decode(str));
String addToRecentListModelToJson(AddToRecentListModel data) => json.encode(data.toJson());
class AddToRecentListModel {
  AddToRecentListModel({
      bool? status, 
      RecentList? recentList,}){
    _status = status;
    _recentList = recentList;
}

  AddToRecentListModel.fromJson(dynamic json) {
    _status = json['status'];
    _recentList = json['RecentList'];
  }
  bool? _status;
  RecentList? _recentList;
AddToRecentListModel copyWith({  bool? status,
  RecentList? recentList,
}) => AddToRecentListModel(  status: status ?? _status,
  recentList: recentList ?? _recentList,
);
  bool? get status => _status;
  RecentList? get recentList => _recentList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['RecentList'] = _recentList;
    return map;
  }

}

/// customer_id : "185"
/// astrologer_id : "45"
/// product_id : "7"
/// product_pooja_id : "4"
/// seen : 1
/// updated_at : "2024-03-20T14:36:12.000000Z"
/// created_at : "2024-03-20T14:36:12.000000Z"
/// id : 10

RecentList recentListFromJson(String str) => RecentList.fromJson(json.decode(str));
String recentListToJson(RecentList data) => json.encode(data.toJson());
class RecentList {
  RecentList({
      String? customerId, 
      String? astrologerId, 
      String? productId, 
      String? productPoojaId, 
      num? seen, 
      String? updatedAt, 
      String? createdAt, 
      num? id,}){
    _customerId = customerId;
    _astrologerId = astrologerId;
    _productId = productId;
    _productPoojaId = productPoojaId;
    _seen = seen;
    _updatedAt = updatedAt;
    _createdAt = createdAt;
    _id = id;
}

  RecentList.fromJson(dynamic json) {
    _customerId = json['customer_id'];
    _astrologerId = json['astrologer_id'];
    _productId = json['product_id'];
    _productPoojaId = json['product_pooja_id'];
    _seen = json['seen'];
    _updatedAt = json['updated_at'];
    _createdAt = json['created_at'];
    _id = json['id'];
  }
  String? _customerId;
  String? _astrologerId;
  String? _productId;
  String? _productPoojaId;
  num? _seen;
  String? _updatedAt;
  String? _createdAt;
  num? _id;
RecentList copyWith({  String? customerId,
  String? astrologerId,
  String? productId,
  String? productPoojaId,
  num? seen,
  String? updatedAt,
  String? createdAt,
  num? id,
}) => RecentList(  customerId: customerId ?? _customerId,
  astrologerId: astrologerId ?? _astrologerId,
  productId: productId ?? _productId,
  productPoojaId: productPoojaId ?? _productPoojaId,
  seen: seen ?? _seen,
  updatedAt: updatedAt ?? _updatedAt,
  createdAt: createdAt ?? _createdAt,
  id: id ?? _id,
);
  String? get customerId => _customerId;
  String? get astrologerId => _astrologerId;
  String? get productId => _productId;
  String? get productPoojaId => _productPoojaId;
  num? get seen => _seen;
  String? get updatedAt => _updatedAt;
  String? get createdAt => _createdAt;
  num? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['customer_id'] = _customerId;
    map['astrologer_id'] = _astrologerId;
    map['product_id'] = _productId;
    map['product_pooja_id'] = _productPoojaId;
    map['seen'] = _seen;
    map['updated_at'] = _updatedAt;
    map['created_at'] = _createdAt;
    map['id'] = _id;
    return map;
  }

}