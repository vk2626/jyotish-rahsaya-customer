// To parse this JSON data, do
//
//     final astrologerLiveDetailsModel = astrologerLiveDetailsModelFromJson(jsonString);

import 'dart:convert';

AstrologerLiveDetailsModel astrologerLiveDetailsModelFromJson(String str) =>
    AstrologerLiveDetailsModel.fromJson(json.decode(str));

String astrologerLiveDetailsModelToJson(AstrologerLiveDetailsModel data) =>
    json.encode(data.toJson());

class AstrologerLiveDetailsModel {
  bool status;
  AstrlogerPriceList astrlogerPriceList;

  AstrologerLiveDetailsModel({
    required this.status,
    required this.astrlogerPriceList,
  });

  factory AstrologerLiveDetailsModel.fromJson(Map<String, dynamic> json) =>
      AstrologerLiveDetailsModel(
        status: json["status"],
        astrlogerPriceList:
            AstrlogerPriceList.fromJson(json["astrlogerPriceList"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "astrlogerPriceList": astrlogerPriceList.toJson(),
      };
}

class AstrlogerPriceList {
  String userId;
  String name;
  String avatar;
  int isOnlineAstrologer;
  String audioCall;
  String anonymousCall;
  String privateCall;
  String videoCall;
  String discountVideoCall;
  String discountAudioCall;
  String discountAnonymousCall;
  String discountPrivateCall;
  int statusVideoCall;
  int statusAudioCall;
  int statusAnonymousCall;
  int statusPrivateCall;

  AstrlogerPriceList({
    required this.userId,
    required this.name,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.audioCall,
    required this.anonymousCall,
    required this.privateCall,
    required this.videoCall,
    required this.discountVideoCall,
    required this.discountAudioCall,
    required this.discountAnonymousCall,
    required this.discountPrivateCall,
    required this.statusVideoCall,
    required this.statusAudioCall,
    required this.statusAnonymousCall,
    required this.statusPrivateCall,
  });

  factory AstrlogerPriceList.fromJson(Map<String, dynamic> json) =>
      AstrlogerPriceList(
        userId: json["user_id"],
        name: json["name"],
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        audioCall: json["audio_call"],
        anonymousCall: json["anonymous_call"],
        privateCall: json["private_call"],
        videoCall: json["video_call"],
        discountVideoCall: json["discount_video_call"],
        discountAudioCall: json["discount_audio_call"],
        discountAnonymousCall: json["discount_anonymous_call"],
        discountPrivateCall: json["discount_private_call"],
        statusVideoCall: json["status_video_call"],
        statusAudioCall: json["status_audio_call"],
        statusAnonymousCall: json["status_anonymous_call"],
        statusPrivateCall: json["status_private_call"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "name": name,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "audio_call": audioCall,
        "anonymous_call": anonymousCall,
        "private_call": privateCall,
        "video_call": videoCall,
        "discount_video_call": discountVideoCall,
        "discount_audio_call": discountAudioCall,
        "discount_anonymous_call": discountAnonymousCall,
        "discount_private_call": discountPrivateCall,
        "status_video_call": statusVideoCall,
        "status_audio_call": statusAudioCall,
        "status_anonymous_call": statusAnonymousCall,
        "status_private_call": statusPrivateCall,
      };
}
