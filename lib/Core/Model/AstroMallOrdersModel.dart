// To parse this JSON data, do
//
//     final astroMallOrdersModel = astroMallOrdersModelFromJson(jsonString);

import 'dart:convert';

AstroMallOrdersModel astroMallOrdersModelFromJson(String str) =>
    AstroMallOrdersModel.fromJson(json.decode(str));

String astroMallOrdersModelToJson(AstroMallOrdersModel data) =>
    json.encode(data.toJson());

class AstroMallOrdersModel {
  bool status;
  List<ProductList> productList;

  AstroMallOrdersModel({
    required this.status,
    required this.productList,
  });

  factory AstroMallOrdersModel.fromJson(Map<String, dynamic> json) =>
      AstroMallOrdersModel(
        status: json["status"],
        productList: List<ProductList>.from(
            json["productList"].map((x) => ProductList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "productList": List<dynamic>.from(productList.map((x) => x.toJson())),
      };
}

class ProductList {
  int id;
  String orderId;
  String userId;
  String? productPrice;
  String astrologerId;
  String suggestedAstroId;
  String productId;
  String poojaId;
  String transactionId;
  String amount;
  String gstAmount;
  String walletDeducted;
  String totalAmount;
  dynamic isSuccess;
  String paymentType;
  String remark;
  DateTime currentDateTime;
  DateTime createdAt;
  // DateTime updatedAt;
  String productTitle;
  String productImage;
  String suggestedAstroName;
  String categoryName;

  ProductList({
    required this.id,
    required this.orderId,
    required this.userId,
    this.productPrice,
    required this.astrologerId,
    required this.suggestedAstroId,
    required this.productId,
    required this.poojaId,
    required this.transactionId,
    required this.amount,
    required this.gstAmount,
    required this.walletDeducted,
    required this.totalAmount,
    required this.isSuccess,
    required this.paymentType,
    required this.remark,
    required this.currentDateTime,
    required this.createdAt,
    // required this.updatedAt,
    required this.productTitle,
    required this.productImage,
    required this.suggestedAstroName,
    required this.categoryName,
  });

  factory ProductList.fromJson(Map<String, dynamic> json) => ProductList(
        id: json["id"],
        orderId: json["order_id"],
        userId: json["user_id"],
        productPrice: json["product_price"],
        astrologerId: json["astrologer_id"],
        suggestedAstroId: json["suggested_astro_id"],
        productId: json["product_id"],
        poojaId: json["pooja_id"],
        transactionId: json["transaction_id"],
        amount: json["amount"],
        gstAmount: json["gst_amount"],
        walletDeducted: json["wallet_deducted"],
        totalAmount: json["total_amount"],
        isSuccess: json["is_success"],
        paymentType: json["payment_type"],
        remark: json["remark"],
        currentDateTime: DateTime.parse(json["current_date_time"]),
        createdAt: DateTime.parse(json["created_at"]),
        // updatedAt: DateTime.parse(json["updated_at"]),
        productTitle: json["product_title"],
        productImage: json["product_image"] ?? "",
        suggestedAstroName: json["suggested_astro_name"],
        categoryName: json["category_name"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "user_id": userId,
        "product_price": productPrice,
        "astrologer_id": astrologerId,
        "suggested_astro_id": suggestedAstroId,
        "product_id": productId,
        "pooja_id": poojaId,
        "transaction_id": transactionId,
        "amount": amount,
        "gst_amount": gstAmount,
        "wallet_deducted": walletDeducted,
        "total_amount": totalAmount,
        "is_success": isSuccess,
        "payment_type": paymentType,
        "remark": remark,
        "current_date_time": currentDateTime.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        // "updated_at": updatedAt.toIso8601String(),
        "product_title": productTitle,
        "product_image": productImage,
        "suggested_astro_name": suggestedAstroName,
        "category_name": categoryName,
      };
}
