// To parse this JSON data, do
//
//     final blockUnblockModel = blockUnblockModelFromJson(jsonString);

import 'dart:convert';

BlockUnblockModel blockUnblockModelFromJson(String str) =>
    BlockUnblockModel.fromJson(json.decode(str));

String blockUnblockModelToJson(BlockUnblockModel data) =>
    json.encode(data.toJson());

class BlockUnblockModel {
  bool? status;
  String? message;
  Data? data;

  BlockUnblockModel({
    this.status,
    this.message,
    this.data,
  });

  factory BlockUnblockModel.fromJson(Map<String, dynamic> json) =>
      BlockUnblockModel(
        status: json["status"],
        message: json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data?.toJson(),
      };
}

class Data {
  String? userId;
  String? astrologerId;
  int? statusBlock;
  String? report;
  DateTime? updatedAt;
  DateTime? createdAt;
  int? id;

  Data({
    this.userId,
    this.astrologerId,
    this.statusBlock,
    this.report,
    this.updatedAt,
    this.createdAt,
    this.id,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userId: json["user_id"],
        astrologerId: json["astrologer_id"],
        statusBlock: json["status_block"],
        report: json["report"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "astrologer_id": astrologerId,
        "status_block": statusBlock,
        "report": report,
        "updated_at": updatedAt?.toIso8601String(),
        "created_at": createdAt?.toIso8601String(),
        "id": id,
      };
}
