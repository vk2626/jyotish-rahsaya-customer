// To parse this JSON data, do
//
//     final giftWalletPaymentModel = giftWalletPaymentModelFromJson(jsonString);

import 'dart:convert';

GiftWalletPaymentModel giftWalletPaymentModelFromJson(String str) =>
    GiftWalletPaymentModel.fromJson(json.decode(str));

String giftWalletPaymentModelToJson(GiftWalletPaymentModel data) =>
    json.encode(data.toJson());

class GiftWalletPaymentModel {
  bool? status;
  String? message;
  Payment? payment;

  GiftWalletPaymentModel({
    this.status,
    this.message,
    this.payment,
  });

  factory GiftWalletPaymentModel.fromJson(Map<String, dynamic> json) =>
      GiftWalletPaymentModel(
        status: json["status"],
        message: json["message"],
        payment:
            json["payment"] == null ? null : Payment.fromJson(json["payment"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "payment": payment?.toJson(),
      };
}

class Payment {
  String? userId;
  String? astrologerId;
  int? giftId;
  String? paymentType;
  String? value;
  int? totalValue;
  String? paymentId;
  String? remark;
  DateTime? updatedAt;
  DateTime? createdAt;
  int? id;

  Payment({
    this.userId,
    this.astrologerId,
    this.giftId,
    this.paymentType,
    this.value,
    this.totalValue,
    this.paymentId,
    this.remark,
    this.updatedAt,
    this.createdAt,
    this.id,
  });

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        userId: json["user_id"],
        astrologerId: json["astrologer_id"],
        giftId: json["gift_id"],
        paymentType: json["payment_type"],
        value: json["value"],
        totalValue: json["total_value"],
        paymentId: json["payment_id"],
        remark: json["remark"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "astrologer_id": astrologerId,
        "gift_id": giftId,
        "payment_type": paymentType,
        "value": value,
        "total_value": totalValue,
        "payment_id": paymentId,
        "remark": remark,
        "updated_at": updatedAt?.toIso8601String(),
        "created_at": createdAt?.toIso8601String(),
        "id": id,
      };
}
