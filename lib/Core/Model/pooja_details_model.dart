// To parse this JSON data, do
//
//     final poojaDetailsModel = poojaDetailsModelFromJson(jsonString);

import 'dart:convert';

PoojaDetailsModel poojaDetailsModelFromJson(String str) =>
    PoojaDetailsModel.fromJson(json.decode(str));

String poojaDetailsModelToJson(PoojaDetailsModel data) =>
    json.encode(data.toJson());

class PoojaDetailsModel {
  bool status;
  Data data;

  PoojaDetailsModel({
    required this.status,
    required this.data,
  });

  factory PoojaDetailsModel.fromJson(Map<String, dynamic> json) =>
      PoojaDetailsModel(
        status: json["status"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class Data {
  int id;
  String title;
  String category;
  String image;
  List<Price> price;
  String shortDescription;
  String description;
  String astrologerId;
  DateTime? time;
  int status;
  DateTime createdAt;

  String astrologerImage;
  String astrologerName;
  String astrologerBio;
  String astrologerDisplayName;
  num rating;
  int isPoojaBooked;
  int astroPoojaId;
  String suggestedAstroId;
  String poojaOrderId;

  Data(
      {required this.id,
      required this.title,
      required this.category,
      required this.image,
      required this.price,
      required this.shortDescription,
      required this.description,
      required this.astrologerId,
      required this.time,
      required this.status,
      required this.createdAt,
      required this.astrologerImage,
      required this.astrologerName,
      required this.astrologerBio,
      required this.astrologerDisplayName,
      required this.rating,
      required this.isPoojaBooked,
      required this.astroPoojaId,
      required this.suggestedAstroId,
      required this.poojaOrderId});

  factory Data.fromJson(Map<String, dynamic> json) => Data(
      id: json["id"],
      title: json["title"],
      category: json["category"],
      image: json["image"],
      price: List<Price>.from(json["price"].map((x) => Price.fromJson(x))),
      shortDescription: json["short_description"] ?? "",
      description: json["description"],
      astrologerId: json["astrologer_id"],
      time: json['time'] != null ? DateTime.parse(json["time"]) : null,
      status: json["status"],
      createdAt: DateTime.parse(json["created_at"]),
      astrologerImage: json["astrologer_image"],
      astrologerName: json["astrologer_name"],
      astrologerBio: json["astrologer_bio"],
      astrologerDisplayName: json["astrologer_display_name"] == null ||
              json["astrologer_display_name"].toString().isEmpty
          ? json["astrologer_name"]
          : json['astrologer_display_name'],
      rating: json["rating"] ?? 0,
      isPoojaBooked: json["is_pooja_booked"],
      astroPoojaId: json['astro_pooja_id'],
      poojaOrderId: json['pooja_order_id'],
      suggestedAstroId: json['suggested_astro_id']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "category": category,
        "image": image,
        "price": List<dynamic>.from(price.map((x) => x.toJson())),
        "short_description": shortDescription ?? "",
        "description": description,
        "astrologer_id": astrologerId,
        "time": time!.toIso8601String(),
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "astrologer_image": astrologerImage,
        "astrologer_name": astrologerName,
        "astrologer_bio": astrologerBio,
        "astrologer_display_name": astrologerDisplayName,
        "rating": rating,
        "is_pooja_booked": isPoojaBooked,
      };
}

class Price {
  String price;
  String priceDescription;
  String priceTitle;
  String priceDiscount;

  Price({
    required this.price,
    required this.priceDescription,
    required this.priceTitle,
    required this.priceDiscount,
  });

  factory Price.fromJson(Map<String, dynamic> json) => Price(
        price: json["price"] ?? "0",
        priceDescription: json["price_description"] ?? "",
        priceTitle: json["price_title"] ?? "",
        priceDiscount: json["price_discount"] ?? "0",
      );

  Map<String, dynamic> toJson() => {
        "price": price,
        "price_description": priceDescription,
        "price_title": priceTitle,
        "price_discount": priceDiscount,
      };
}
