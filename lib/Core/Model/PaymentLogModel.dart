import 'dart:convert';

PaymentLogModel paymentLogModelFromJson(String str) =>
    PaymentLogModel.fromJson(json.decode(str));

String paymentLogModelToJson(PaymentLogModel data) =>
    json.encode(data.toJson());

class PaymentLogModel {
  bool status;
  List<PaymentList> paymentList;

  PaymentLogModel({
    required this.status,
    required this.paymentList,
  });

  factory PaymentLogModel.fromJson(Map<String, dynamic> json) =>
      PaymentLogModel(
        status: json["status"],
        paymentList: List<PaymentList>.from(
            json["paymentList"].map((x) => PaymentList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "paymentList": List<dynamic>.from(paymentList.map((x) => x.toJson())),
      };
}

class PaymentList {
  int id;
  String orderId;

  String? transactionId;
  String amount;
  String gstAmount;

  int isSuccess;
  String paymentType;
  String paymentStatus;
  String remark;
  DateTime currentDateTime;

  PaymentList({
    required this.id,
    required this.orderId,
    required this.transactionId,
    required this.amount,
    required this.gstAmount,
    required this.isSuccess,
    required this.paymentType,
    required this.paymentStatus,
    required this.remark,
    required this.currentDateTime,
  });

  factory PaymentList.fromJson(Map<String, dynamic> json) => PaymentList(
        id: json["id"],
        orderId: json["order_id"],
        transactionId: json["transaction_id"],
        amount: json["amount"],
        gstAmount: json["gst_amount"],
        isSuccess: json["is_success"],
        paymentType: json["payment_type"] ?? "",
        paymentStatus: json['payment_status'],
        remark: json["remark"],
        currentDateTime: DateTime.parse(json["current_date_time"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "transaction_id": transactionId,
        "amount": amount,
        "gst_amount": gstAmount,
        "is_success": isSuccess,
        "payment_type": paymentType,
        "payment_status": paymentStatus,
        "remark": remark,
        "current_date_time": currentDateTime.toIso8601String(),
      };
}
