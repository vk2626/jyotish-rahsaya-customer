// To parse this JSON data, do
//
//     final ashtakvargaChartModel = ashtakvargaChartModelFromJson(jsonString);

import 'dart:convert';

AshtakvargaChartModel ashtakvargaChartModelFromJson(String str) => AshtakvargaChartModel.fromJson(json.decode(str));

String ashtakvargaChartModelToJson(AshtakvargaChartModel data) => json.encode(data.toJson());

class AshtakvargaChartModel {
    int status;
    AshtakVargaData response;

    AshtakvargaChartModel({
        required this.status,
        required this.response,
    });

    factory AshtakvargaChartModel.fromJson(Map<String, dynamic> json) => AshtakvargaChartModel(
        status: json["status"],
        response: AshtakVargaData.fromJson(json["response"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "response": response.toJson(),
    };
}

class AshtakVargaData {
    List<String> ashtakvargaOrder;
    List<List<int>> ashtakvargaPoints;
    List<int> ashtakvargaTotal;

    AshtakVargaData({
        required this.ashtakvargaOrder,
        required this.ashtakvargaPoints,
        required this.ashtakvargaTotal,
    });

    factory AshtakVargaData.fromJson(Map<String, dynamic> json) => AshtakVargaData(
        ashtakvargaOrder: List<String>.from(json["ashtakvarga_order"].map((x) => x)),
        ashtakvargaPoints: List<List<int>>.from(json["ashtakvarga_points"].map((x) => List<int>.from(x.map((x) => x)))),
        ashtakvargaTotal: List<int>.from(json["ashtakvarga_total"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "ashtakvarga_order": List<dynamic>.from(ashtakvargaOrder.map((x) => x)),
        "ashtakvarga_points": List<dynamic>.from(ashtakvargaPoints.map((x) => List<dynamic>.from(x.map((x) => x)))),
        "ashtakvarga_total": List<dynamic>.from(ashtakvargaTotal.map((x) => x)),
    };
}
