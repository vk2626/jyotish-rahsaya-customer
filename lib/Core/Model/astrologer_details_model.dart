// To parse this JSON data, do
//
//     final astrologerDetailsModel = astrologerDetailsModelFromJson(jsonString);

import 'dart:convert';

AstrologerDetailsModel astrologerDetailsModelFromJson(String str) =>
    AstrologerDetailsModel.fromJson(json.decode(str));

String astrologerDetailsModelToJson(AstrologerDetailsModel data) =>
    json.encode(data.toJson());

class AstrologerDetailsModel {
  bool status;
  List<AstrologerElement> astrologers;

  AstrologerDetailsModel({
    required this.status,
    required this.astrologers,
  });

  factory AstrologerDetailsModel.fromJson(Map<String, dynamic> json) =>
      AstrologerDetailsModel(
        status: json["status"],
        astrologers: List<AstrologerElement>.from(
            json["astrologers"].map((x) => AstrologerElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "astrologers": List<dynamic>.from(astrologers.map((x) => x.toJson())),
      };
}

class AstrologerElement {
  int id;
  String name;
  int label;
  String avatar;
  int isFreeChatCompleted;
  int isOnlineAstrologer;
  AstrologerAstrologer astrologer;

  AstrologerElement({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isFreeChatCompleted,
    required this.isOnlineAstrologer,
    required this.astrologer,
  });

  factory AstrologerElement.fromJson(Map<String, dynamic> json) =>
      AstrologerElement(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        isFreeChatCompleted: json["is_free_enable"] ??
            1, //Free Chat Enable 1 Means FreeChat is completed for the user
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        astrologer: AstrologerAstrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "astrologer": astrologer.toJson(),
      };
}

class AstrologerAstrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String labelText;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  String chatPrice;
  String chatDiscountPrice;
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;
  String displayName;
  int statusCall;
  int statusChat;
  int statusVideo;
  int waitTime;
  num rating;
  String totalDurationChat;
  String totalDurationCall;
  bool followingUser;
  bool blockedUser;
  String primarySkillName;
  String allSkillName;
  String languageName;
  List<Review> reviews;
  List<Gallery> gallery;
  Nextonlines nextonlines;

  AstrologerAstrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.labelText,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.chatPrice,
    required this.chatDiscountPrice,
    required this.callPrice,
    required this.callDiscountPrice,
    required this.videoPrice,
    required this.videoDiscountPrice,
    required this.displayName,
    required this.statusCall,
    required this.statusChat,
    required this.statusVideo,
    required this.waitTime,
    required this.rating,
    required this.totalDurationChat,
    required this.totalDurationCall,
    required this.followingUser,
    required this.blockedUser,
    required this.primarySkillName,
    required this.allSkillName,
    required this.languageName,
    required this.reviews,
    required this.gallery,
    required this.nextonlines,
  });

  factory AstrologerAstrologer.fromJson(Map<String, dynamic> json) =>
      AstrologerAstrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        labelText: json['label_text'],
        bio: json["bio"],
        freeChat: json["free_chat"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        chatPrice: json["chat_price"],
        chatDiscountPrice: json["chat_discount_price"],
        callPrice: json["call_price"],
        callDiscountPrice: json["call_discount_price"],
        videoPrice: json["video_price"],
        videoDiscountPrice: json["video_discount_price"],
        displayName: json["display_name"],
        statusCall: json["status_call"],
        statusChat: json["status_chat"],
        statusVideo: json["status_video"],
        waitTime: json["wait_time"],
        rating: json["rating"],
        totalDurationChat: json["total_duration_chat"],
        totalDurationCall: json["total_duration_call"],
        followingUser: json["following_user"],
        blockedUser: json["blocked_user"],
        primarySkillName: json["primary_skill_name"],
        allSkillName: json["all_skill_name"],
        languageName: json["language_name"],
        reviews:
            List<Review>.from(json["reviews"].map((x) => Review.fromJson(x))),
        gallery:
            List<Gallery>.from(json["gallery"].map((x) => Gallery.fromJson(x))),
        nextonlines: Nextonlines.fromJson(json["nextonlines"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "label_text": labelText,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "status_call": statusCall,
        "status_chat": statusChat,
        "status_video": statusVideo,
        "wait_time": waitTime,
        "rating": rating,
        "total_duration_chat": totalDurationChat,
        "total_duration_call": totalDurationCall,
        "following_user": followingUser,
        "blocked_user": blockedUser,
        "primary_skill_name": primarySkillName,
        "all_skill_name": allSkillName,
        "language_name": languageName,
        "reviews": List<dynamic>.from(reviews.map((x) => x.toJson())),
        "gallery": List<dynamic>.from(gallery.map((x) => x.toJson())),
        "nextonlines": nextonlines.toJson(),
      };
}

class Gallery {
  String astrologerId;
  String image;

  Gallery({
    required this.astrologerId,
    required this.image,
  });

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
        astrologerId: json["astrologer_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "astrologer_id": astrologerId,
        "image": image,
      };
}

class Review {
  int id;
  String infoId;
  String orderId;
  String type;
  String rate;
  String customerId;
  String astrologerId;
  int isHide;
  String isLike;
  String description;
  String reply;
  int isPin;
  String commentRDelete;
  int status;
  DateTime createdAt;
  DateTime updatedAt;
  String customerImage;
  String customerName;

  Review({
    required this.id,
    required this.infoId,
    required this.orderId,
    required this.type,
    required this.rate,
    required this.customerId,
    required this.astrologerId,
    required this.isHide,
    required this.isLike,
    required this.description,
    required this.reply,
    required this.isPin,
    required this.commentRDelete,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.customerImage,
    required this.customerName,
  });

  factory Review.fromJson(Map<String, dynamic> json) => Review(
        id: json["id"],
        infoId: json["info_id"],
        orderId: json["order_id"],
        type: json["type"],
        rate: json["rate"],
        customerId: json["customer_id"],
        astrologerId: json["astrologer_id"],
        isHide: json["is_hide"],
        isLike: json["is_like"],
        description: json["description"],
        reply: json["reply"],
        isPin: json["is_pin"],
        commentRDelete: json["comment_r_delete"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        customerImage: json["customer_image"],
        customerName: json["customer_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "info_id": infoId,
        "order_id": orderId,
        "type": type,
        "rate": rate,
        "customer_id": customerId,
        "astrologer_id": astrologerId,
        "is_hide": isHide,
        "is_like": isLike,
        "description": description,
        "reply": reply,
        "is_pin": isPin,
        "comment_r_delete": commentRDelete,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "customer_image": customerImage,
        "customer_name": customerName,
      };
}

class Nextonlines {
  String chatNODate;
  String chatNOTime;
  String callNODate;
  String callNOTime;
  String videoNODate;
  String videoNOTime;

  Nextonlines({
    required this.chatNODate,
    required this.chatNOTime,
    required this.callNODate,
    required this.callNOTime,
    required this.videoNODate,
    required this.videoNOTime,
  });

  factory Nextonlines.fromJson(Map<String, dynamic> json) => Nextonlines(
        chatNODate: json["chat_n_o_date"],
        chatNOTime: json["chat_n_o_time"],
        callNODate: json["call_n_o_date"],
        callNOTime: json["call_n_o_time"],
        videoNODate: json["video_n_o_date"],
        videoNOTime: json["video_n_o_time"],
      );

  Map<String, dynamic> toJson() => {
        "chat_n_o_date": chatNODate,
        "chat_n_o_time": chatNOTime,
        "call_n_o_date": callNODate,
        "call_n_o_time": callNOTime,
        "video_n_o_date": videoNODate,
        "video_n_o_time": videoNOTime,
      };
}
