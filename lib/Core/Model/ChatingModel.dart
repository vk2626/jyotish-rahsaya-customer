// To parse this JSON data, do
//
//     final chatingModel = chatingModelFromJson(jsonString);

import 'dart:convert';

ChatingModel chatingModelFromJson(String str) =>
    ChatingModel.fromJson(json.decode(str));

String chatingModelToJson(ChatingModel data) => json.encode(data.toJson());

class ChatingModel {
  bool status;
  ChatMessage messages;
  List<Review> reviews;

  ChatingModel({
    required this.status,
    required this.messages,
    required this.reviews,
  });

  factory ChatingModel.fromJson(Map<String, dynamic> json) => ChatingModel(
        status: json["status"],
        messages: ChatMessage.fromJson(json["messages"]),
        reviews:
            List<Review>.from(json["reviews"].map((x) => Review.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "messages": messages.toJson(),
        "reviews": List<dynamic>.from(reviews.map((x) => x.toJson())),
      };
}

class ChatMessage {
  int currentPage;
  List<Message> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  List<Link> links;
  String? nextPageUrl;
  String path;
  int perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  ChatMessage({
    required this.currentPage,
    required this.data,
    required this.firstPageUrl,
    required this.from,
    required this.lastPage,
    required this.lastPageUrl,
    required this.links,
    required this.nextPageUrl,
    required this.path,
    required this.perPage,
    required this.prevPageUrl,
    required this.to,
    required this.total,
  });

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
        currentPage: json["current_page"],
        data: List<Message>.from(json["data"].map((x) => Message.fromJson(x))),
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        links: List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
        nextPageUrl: json["next_page_url"]??"",
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "links": List<dynamic>.from(links.map((x) => x.toJson())),
        "next_page_url": nextPageUrl,
        "path": path,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}

class Message {
  String id;
  int fromId;
  int toId;
  String? body;
  String attachment;
  int seen;
  DateTime createdAt;
  DateTime updatedAt;

  Message({
    required this.id,
    required this.fromId,
    required this.toId,
    required this.body,
    required this.attachment,
    required this.seen,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Message.fromJson(Map<String, dynamic> json) => Message(
        id: json["id"],
        fromId: json["from_id"],
        toId: json["to_id"],
        body: json["body"] ?? "",
        attachment: json["attachment"],
        seen: json["seen"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "from_id": fromId,
        "to_id": toId,
        "body": body,
        "attachment": attachment,
        "seen": seen,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class Link {
  String? url;
  String label;
  bool active;

  Link({
    required this.url,
    required this.label,
    required this.active,
  });

  factory Link.fromJson(Map<String, dynamic> json) => Link(
        url: json["url"],
        label: json["label"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "label": label,
        "active": active,
      };
}

class Review {
  int id;
  String orderId;
  String type;
  String rate;
  String customerId;
  String astrologerId;
  int isHide;
  String isLike;
  String description;
  String reply;
  int isPin;
  String commentRDelete;
  int status;
  DateTime createdAt;
  DateTime updatedAt;
  String customerImage;
  String customerName;

  Review({
    required this.id,
    required this.orderId,
    required this.type,
    required this.rate,
    required this.customerId,
    required this.astrologerId,
    required this.isHide,
    required this.isLike,
    required this.description,
    required this.reply,
    required this.isPin,
    required this.commentRDelete,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.customerImage,
    required this.customerName,
  });

  factory Review.fromJson(Map<String, dynamic> json) => Review(
        id: json["id"],
        orderId: json["order_id"],
        type: json["type"],
        rate: json["rate"],
        customerId: json["customer_id"],
        astrologerId: json["astrologer_id"],
        isHide: json["is_hide"],
        isLike: json["is_like"],
        description: json["description"],
        reply: json["reply"],
        isPin: json["is_pin"],
        commentRDelete: json["comment_r_delete"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        customerImage: json["customer_image"],
        customerName: json["customer_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "type": type,
        "rate": rate,
        "customer_id": customerId,
        "astrologer_id": astrologerId,
        "is_hide": isHide,
        "is_like": isLike,
        "description": description,
        "reply": reply,
        "is_pin": isPin,
        "comment_r_delete": commentRDelete,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "customer_image": customerImage,
        "customer_name": customerName,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
