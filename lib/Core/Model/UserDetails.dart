// To parse this JSON data, do
//
//     final userDetails = userDetailsFromJson(jsonString);

import 'dart:convert';

UserDetails userDetailsFromJson(String str) =>
    UserDetails.fromJson(json.decode(str));

String userDetailsToJson(UserDetails data) => json.encode(data.toJson());

class UserDetails {
  bool status;
  Data data;

  UserDetails({
    required this.status,
    required this.data,
  });

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
        status: json["status"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class Data {
  int id;
  String name;
  String image;
  int mobile;
  int userType;
  int status;
  String isComplete;
  String deviceToken;
  Customer customer;

  Data({
    required this.id,
    required this.name,
    required this.image,
    required this.mobile,
    required this.userType,
    required this.status,
    required this.isComplete,
    required this.deviceToken,
    required this.customer,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        image: json["image"],
        mobile: json["mobile"],
        userType: json["user_type"],
        status: json["status"],
        isComplete: json["is_complete"],
        deviceToken: json["device_token"],
        customer: Customer.fromJson(json["customer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "mobile": mobile,
        "user_type": userType,
        "status": status,
        "is_complete": isComplete,
        "device_token": deviceToken,
        "customer": customer.toJson(),
      };
}

class Customer {
  int id;
  String userId;
  dynamic rashi;
  String gender;
  String dob;
  String dobPlace;
  String dobTime;
  String currentAddress;
  String lat;
  String lon;
  String address;
  String pincode;

  Customer({
    required this.id,
    required this.userId,
    required this.rashi,
    required this.gender,
    required this.dob,
    required this.dobPlace,
    required this.dobTime,
    required this.currentAddress,
    required this.lat,
    required this.lon,
    required this.address,
    required this.pincode,
  });
  factory Customer.fromJson(Map<String, dynamic> json) {
    int id = json["id"] is int
        ? json["id"]
        : int.tryParse(json["id"]?.toString() ?? "0") ?? 0;
    String lat = json["lat"] ?? "";
    String lon = json["lon"] ?? "";

    if (lat.isEmpty || lon.isEmpty) {
      lat = "0";
      lon = "0";
    }

    return Customer(
      id: id,
      userId: json["user_id"],
      rashi: json["rashi"],
      gender: json["gender"],
      dob: json["dob"],
      dobPlace: json["dob_place"],
      dobTime: json["dob_time"],
      currentAddress: json["current_address"],
      lat: lat,
      lon: lon,
      address: json["address"],
      pincode: json["pincode"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "rashi": rashi,
        "gender": gender,
        "dob": dob,
        "dob_place": dobPlace,
        "dob_time": dobTime,
        "current_address": currentAddress,
        "lat": lat,
        "lon": lon,
        "address": address,
        "pincode": pincode,
      };
}
