import 'dart:convert';

/// status : true
/// astrologers : [{"id":119,"name":"Abhinav Kumar Singh","label":1,"avatar":"https://jyotish.techsaga.live/uploads/astrologer/2024010514220453.png","astrologer":{"id":75,"user_id":"119","primary_skill_id":"3","all_skill_id":"3","language_id":"2,1","experience_year":"2","price":20,"discount_price":10,"bio":"fdg","free_chat":1,"order_count":"10","is_online":1,"rating":4.5,"total_duration_chat":"5.0 k mins","total_duration_call":"0.0 mins","following_user":true,"blocked_user":false,"primary_skill_name":"Prashana","all_skill_name":"Prashana","language_name":"Hindi,English","reviews":[{"id":1,"rate":"5","customer_id":"185","astrologer_id":"119","is_hide":1,"is_like":"10","description":"abhinav","reply":"","status":1,"created_at":"2024-03-11 11:08:03","updated_at":"2024-03-11 11:08:25","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024011515405334.jpg","customer_name":"Prabhakar Pandey"},{"id":2,"rate":"4","customer_id":"204","astrologer_id":"119","is_hide":1,"is_like":"10","description":"rahul","reply":"","status":1,"created_at":"2024-03-21 12:11:34","updated_at":"2024-03-21 12:11:34","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024020211445161.jpg","customer_name":"Francesca Bass"}],"gallery":[{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474914.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474920.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474941.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474909.jpg"}]}}]

AstrologersDetails astrologersDetailsFromJson(String str) =>
    AstrologersDetails.fromJson(json.decode(str));

String astrologersDetailsToJson(AstrologersDetails data) =>
    json.encode(data.toJson());

class AstrologersDetails {
  AstrologersDetails({
    bool? status,
    List<Astrologers>? astrologers,
  }) {
    _status = status;
    _astrologers = astrologers;
  }

  AstrologersDetails.fromJson(dynamic json) {
    _status = json['status'];
    if (json['astrologers'] != null) {
      _astrologers = [];
      json['astrologers'].forEach((v) {
        _astrologers?.add(Astrologers.fromJson(v));
      });
    }
  }

  bool? _status;
  List<Astrologers>? _astrologers;

  AstrologersDetails copyWith({
    bool? status,
    List<Astrologers>? astrologers,
  }) =>
      AstrologersDetails(
        status: status ?? _status,
        astrologers: astrologers ?? _astrologers,
      );

  bool? get status => _status;

  List<Astrologers>? get astrologers => _astrologers;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    if (_astrologers != null) {
      map['astrologers'] = _astrologers?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 119
/// name : "Abhinav Kumar Singh"
/// label : 1
/// avatar : "https://jyotish.techsaga.live/uploads/astrologer/2024010514220453.png"
/// astrologer : {"id":75,"user_id":"119","primary_skill_id":"3","all_skill_id":"3","language_id":"2,1","experience_year":"2","price":20,"discount_price":10,"bio":"fdg","free_chat":1,"order_count":"10","is_online":1,"rating":4.5,"total_duration_chat":"5.0 k mins","total_duration_call":"0.0 mins","following_user":true,"blocked_user":false,"primary_skill_name":"Prashana","all_skill_name":"Prashana","language_name":"Hindi,English","reviews":[{"id":1,"rate":"5","customer_id":"185","astrologer_id":"119","is_hide":1,"is_like":"10","description":"abhinav","reply":"","status":1,"created_at":"2024-03-11 11:08:03","updated_at":"2024-03-11 11:08:25","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024011515405334.jpg","customer_name":"Prabhakar Pandey"},{"id":2,"rate":"4","customer_id":"204","astrologer_id":"119","is_hide":1,"is_like":"10","description":"rahul","reply":"","status":1,"created_at":"2024-03-21 12:11:34","updated_at":"2024-03-21 12:11:34","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024020211445161.jpg","customer_name":"Francesca Bass"}],"gallery":[{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474914.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474920.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474941.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474909.jpg"}]}

Astrologers astrologersFromJson(String str) =>
    Astrologers.fromJson(json.decode(str));

String astrologersToJson(Astrologers data) => json.encode(data.toJson());

class Astrologers {
  Astrologers({
    num? id,
    String? name,
    num? label,
    String? avatar,
    Astrologer? astrologer,
    int? isOnlineAstrologer, // New parameter
  }) {
    _id = id;
    _name = name;
    _label = label;
    _avatar = avatar;
    _astrologer = astrologer;
    _isOnlineAstrologer = isOnlineAstrologer; // Assign the new parameter
  }

  Astrologers.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _label = json['label'];
    _avatar = json['avatar'];
    _astrologer = json['astrologer'] != null
        ? Astrologer.fromJson(json['astrologer'])
        : null;
    _isOnlineAstrologer = json['isOnlineAstrologer']; // Parse the new parameter
  }

  num? _id;
  String? _name;
  num? _label;
  String? _avatar;
  Astrologer? _astrologer;
  int? _isOnlineAstrologer; // New instance variable

  Astrologers copyWith({
    num? id,
    String? name,
    num? label,
    String? avatar,
    Astrologer? astrologer,
    int? isOnlineAstrologer, // Include the new parameter in copyWith
  }) =>
      Astrologers(
        id: id ?? _id,
        name: name ?? _name,
        label: label ?? _label,
        avatar: avatar ?? _avatar,
        astrologer: astrologer ?? _astrologer,
        isOnlineAstrologer: isOnlineAstrologer ??
            _isOnlineAstrologer, // Assign the new parameter in copyWith
      );

  num? get id => _id;

  String? get name => _name;

  num? get label => _label;

  String? get avatar => _avatar;

  Astrologer? get astrologer => _astrologer;

  int? get isOnlineAstrologer =>
      _isOnlineAstrologer; // Getter for the new parameter

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['label'] = _label;
    map['avatar'] = _avatar;
    if (_astrologer != null) {
      map['astrologer'] = _astrologer?.toJson();
    }
    map['isOnlineAstrologer'] =
        _isOnlineAstrologer; // Serialize the new parameter
    return map;
  }
}

/// id : 75
/// user_id : "119"
/// primary_skill_id : "3"
/// all_skill_id : "3"
/// language_id : "2,1"
/// experience_year : "2"
/// price : 20
/// discount_price : 10
/// bio : "fdg"
/// free_chat : 1
/// order_count : "10"
/// is_online : 1
/// rating : 4.5
/// total_duration_chat : "5.0 k mins"
/// total_duration_call : "0.0 mins"
/// following_user : true
/// blocked_user : false
/// primary_skill_name : "Prashana"
/// all_skill_name : "Prashana"
/// language_name : "Hindi,English"
/// reviews : [{"id":1,"rate":"5","customer_id":"185","astrologer_id":"119","is_hide":1,"is_like":"10","description":"abhinav","reply":"","status":1,"created_at":"2024-03-11 11:08:03","updated_at":"2024-03-11 11:08:25","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024011515405334.jpg","customer_name":"Prabhakar Pandey"},{"id":2,"rate":"4","customer_id":"204","astrologer_id":"119","is_hide":1,"is_like":"10","description":"rahul","reply":"","status":1,"created_at":"2024-03-21 12:11:34","updated_at":"2024-03-21 12:11:34","customer_image":"https://jyotish.techsaga.live/uploads/customer/2024020211445161.jpg","customer_name":"Francesca Bass"}]
/// gallery : [{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474914.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474920.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474941.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474909.jpg"}]

Astrologer astrologerFromJson(String str) =>
    Astrologer.fromJson(json.decode(str));

String astrologerToJson(Astrologer data) => json.encode(data.toJson());

class Astrologer {
  Astrologer({
    num? id,
    String? userId,
    String? primarySkillId,
    String? allSkillId,
    String? languageId,
    String? experienceYear,
    num? price,
    num? discountPrice,
    String? bio,
    num? freeChat,
    String? orderCount,
    num? isOnline,
    num? rating,
    String? totalDurationChat,
    String? totalDurationCall,
    bool? followingUser,
    bool? blockedUser,
    String? primarySkillName,
    String? allSkillName,
    String? languageName,
    List<Reviews>? reviews,
    List<Gallery>? gallery,
  }) {
    _id = id;
    _userId = userId;
    _primarySkillId = primarySkillId;
    _allSkillId = allSkillId;
    _languageId = languageId;
    _experienceYear = experienceYear;
    _price = price;
    _discountPrice = discountPrice;
    _bio = bio;
    _freeChat = freeChat;
    _orderCount = orderCount;
    _isOnline = isOnline;
    _rating = rating;
    _totalDurationChat = totalDurationChat;
    _totalDurationCall = totalDurationCall;
    _followingUser = followingUser;
    _blockedUser = blockedUser;
    _primarySkillName = primarySkillName;
    _allSkillName = allSkillName;
    _languageName = languageName;
    _reviews = reviews;
    _gallery = gallery;
  }

  Astrologer.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _primarySkillId = json['primary_skill_id'];
    _allSkillId = json['all_skill_id'];
    _languageId = json['language_id'];
    _experienceYear = json['experience_year'];
    _price = json['price'];
    _discountPrice = json['discount_price'];
    _bio = json['bio'];
    _freeChat = json['free_chat'];
    _orderCount = json['order_count'];
    _isOnline = json['is_online'];
    _rating = json['rating'];
    _totalDurationChat = json['total_duration_chat'];
    _totalDurationCall = json['total_duration_call'];
    _followingUser = json['following_user'];
    _blockedUser = json['blocked_user'];
    _primarySkillName = json['primary_skill_name'];
    _allSkillName = json['all_skill_name'];
    _languageName = json['language_name'];
    if (json['reviews'] != null) {
      _reviews = [];
      json['reviews'].forEach((v) {
        _reviews?.add(Reviews.fromJson(v));
      });
    }
    if (json['gallery'] != null) {
      _gallery = [];
      json['gallery'].forEach((v) {
        _gallery?.add(Gallery.fromJson(v));
      });
    }
  }

  num? _id;
  String? _userId;
  String? _primarySkillId;
  String? _allSkillId;
  String? _languageId;
  String? _experienceYear;
  num? _price;
  num? _discountPrice;
  String? _bio;
  num? _freeChat;
  String? _orderCount;
  num? _isOnline;
  num? _rating;
  String? _totalDurationChat;
  String? _totalDurationCall;
  bool? _followingUser;
  bool? _blockedUser;
  String? _primarySkillName;
  String? _allSkillName;
  String? _languageName;
  List<Reviews>? _reviews;
  List<Gallery>? _gallery;

  Astrologer copyWith({
    num? id,
    String? userId,
    String? primarySkillId,
    String? allSkillId,
    String? languageId,
    String? experienceYear,
    num? price,
    num? discountPrice,
    String? bio,
    num? freeChat,
    String? orderCount,
    num? isOnline,
    num? rating,
    String? totalDurationChat,
    String? totalDurationCall,
    bool? followingUser,
    bool? blockedUser,
    String? primarySkillName,
    String? allSkillName,
    String? languageName,
    List<Reviews>? reviews,
    List<Gallery>? gallery,
  }) =>
      Astrologer(
        id: id ?? _id,
        userId: userId ?? _userId,
        primarySkillId: primarySkillId ?? _primarySkillId,
        allSkillId: allSkillId ?? _allSkillId,
        languageId: languageId ?? _languageId,
        experienceYear: experienceYear ?? _experienceYear,
        price: price ?? _price,
        discountPrice: discountPrice ?? _discountPrice,
        bio: bio ?? _bio,
        freeChat: freeChat ?? _freeChat,
        orderCount: orderCount ?? _orderCount,
        isOnline: isOnline ?? _isOnline,
        rating: rating ?? _rating,
        totalDurationChat: totalDurationChat ?? _totalDurationChat,
        totalDurationCall: totalDurationCall ?? _totalDurationCall,
        followingUser: followingUser ?? _followingUser,
        blockedUser: blockedUser ?? _blockedUser,
        primarySkillName: primarySkillName ?? _primarySkillName,
        allSkillName: allSkillName ?? _allSkillName,
        languageName: languageName ?? _languageName,
        reviews: reviews ?? _reviews,
        gallery: gallery ?? _gallery,
      );

  num? get id => _id;

  String? get userId => _userId;

  String? get primarySkillId => _primarySkillId;

  String? get allSkillId => _allSkillId;

  String? get languageId => _languageId;

  String? get experienceYear => _experienceYear;

  num? get price => _price;

  num? get discountPrice => _discountPrice;

  String? get bio => _bio;

  num? get freeChat => _freeChat;

  String? get orderCount => _orderCount;

  num? get isOnline => _isOnline;

  num? get rating => _rating;

  String? get totalDurationChat => _totalDurationChat;

  String? get totalDurationCall => _totalDurationCall;

  bool? get followingUser => _followingUser;

  bool? get blockedUser => _blockedUser;

  String? get primarySkillName => _primarySkillName;

  String? get allSkillName => _allSkillName;

  String? get languageName => _languageName;

  List<Reviews>? get reviews => _reviews;

  List<Gallery>? get gallery => _gallery;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['primary_skill_id'] = _primarySkillId;
    map['all_skill_id'] = _allSkillId;
    map['language_id'] = _languageId;
    map['experience_year'] = _experienceYear;
    map['price'] = _price;
    map['discount_price'] = _discountPrice;
    map['bio'] = _bio;
    map['free_chat'] = _freeChat;
    map['order_count'] = _orderCount;
    map['is_online'] = _isOnline;
    map['rating'] = _rating;
    map['total_duration_chat'] = _totalDurationChat;
    map['total_duration_call'] = _totalDurationCall;
    map['following_user'] = _followingUser;
    map['blocked_user'] = _blockedUser;
    map['primary_skill_name'] = _primarySkillName;
    map['all_skill_name'] = _allSkillName;
    map['language_name'] = _languageName;
    if (_reviews != null) {
      map['reviews'] = _reviews?.map((v) => v.toJson()).toList();
    }
    if (_gallery != null) {
      map['gallery'] = _gallery?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// astrologer_id : "75"
/// image : "https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"

Gallery galleryFromJson(String str) => Gallery.fromJson(json.decode(str));

String galleryToJson(Gallery data) => json.encode(data.toJson());

class Gallery {
  Gallery({
    String? astrologerId,
    String? image,
  }) {
    _astrologerId = astrologerId;
    _image = image;
  }

  Gallery.fromJson(dynamic json) {
    _astrologerId = json['astrologer_id'];
    _image = json['image'];
  }

  String? _astrologerId;
  String? _image;

  Gallery copyWith({
    String? astrologerId,
    String? image,
  }) =>
      Gallery(
        astrologerId: astrologerId ?? _astrologerId,
        image: image ?? _image,
      );

  String? get astrologerId => _astrologerId;

  String? get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['astrologer_id'] = _astrologerId;
    map['image'] = _image;
    return map;
  }
}

/// id : 1
/// rate : "5"
/// customer_id : "185"
/// astrologer_id : "119"
/// is_hide : 1
/// is_like : "10"
/// description : "abhinav"
/// reply : ""
/// status : 1
/// created_at : "2024-03-11 11:08:03"
/// updated_at : "2024-03-11 11:08:25"
/// customer_image : "https://jyotish.techsaga.live/uploads/customer/2024011515405334.jpg"
/// customer_name : "Prabhakar Pandey"

Reviews reviewsFromJson(String str) => Reviews.fromJson(json.decode(str));

String reviewsToJson(Reviews data) => json.encode(data.toJson());

class Reviews {
  Reviews({
    num? id,
    String? rate,
    String? customerId,
    String? astrologerId,
    num? isHide,
    String? isLike,
    String? description,
    String? reply,
    num? status,
    String? createdAt,
    String? updatedAt,
    String? customerImage,
    String? customerName,
  }) {
    _id = id;
    _rate = rate;
    _customerId = customerId;
    _astrologerId = astrologerId;
    _isHide = isHide;
    _isLike = isLike;
    _description = description;
    _reply = reply;
    _status = status;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _customerImage = customerImage;
    _customerName = customerName;
  }

  Reviews.fromJson(dynamic json) {
    _id = json['id'];
    _rate = json['rate'];
    _customerId = json['customer_id'];
    _astrologerId = json['astrologer_id'];
    _isHide = json['is_hide'];
    _isLike = json['is_like'];
    _description = json['description'];
    _reply = json['reply'];
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _customerImage = json['customer_image'];
    _customerName = json['customer_name'];
  }

  num? _id;
  String? _rate;
  String? _customerId;
  String? _astrologerId;
  num? _isHide;
  String? _isLike;
  String? _description;
  String? _reply;
  num? _status;
  String? _createdAt;
  String? _updatedAt;
  String? _customerImage;
  String? _customerName;

  Reviews copyWith({
    num? id,
    String? rate,
    String? customerId,
    String? astrologerId,
    num? isHide,
    String? isLike,
    String? description,
    String? reply,
    num? status,
    String? createdAt,
    String? updatedAt,
    String? customerImage,
    String? customerName,
  }) =>
      Reviews(
        id: id ?? _id,
        rate: rate ?? _rate,
        customerId: customerId ?? _customerId,
        astrologerId: astrologerId ?? _astrologerId,
        isHide: isHide ?? _isHide,
        isLike: isLike ?? _isLike,
        description: description ?? _description,
        reply: reply ?? _reply,
        status: status ?? _status,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        customerImage: customerImage ?? _customerImage,
        customerName: customerName ?? _customerName,
      );

  num? get id => _id;

  String? get rate => _rate;

  String? get customerId => _customerId;

  String? get astrologerId => _astrologerId;

  num? get isHide => _isHide;

  String? get isLike => _isLike;

  String? get description => _description;

  String? get reply => _reply;

  num? get status => _status;

  String? get createdAt => _createdAt;

  String? get updatedAt => _updatedAt;

  String? get customerImage => _customerImage;

  String? get customerName => _customerName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['rate'] = _rate;
    map['customer_id'] = _customerId;
    map['astrologer_id'] = _astrologerId;
    map['is_hide'] = _isHide;
    map['is_like'] = _isLike;
    map['description'] = _description;
    map['reply'] = _reply;
    map['status'] = _status;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['customer_image'] = _customerImage;
    map['customer_name'] = _customerName;
    return map;
  }
}
