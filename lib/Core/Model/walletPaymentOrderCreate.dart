// ignore_for_file: public_member_api_docs, sort_constructors_first
// To parse this JSON data, do
//
//     final walletPaymentOrderCreate = walletPaymentOrderCreateFromJson(jsonString);

import 'dart:convert';

WalletPaymentOrderCreate walletPaymentOrderCreateFromJson(String str) =>
    WalletPaymentOrderCreate.fromJson(json.decode(str));

String walletPaymentOrderCreateToJson(WalletPaymentOrderCreate data) =>
    json.encode(data.toJson());

class WalletPaymentOrderCreate {
  bool? status;
  Payment? payment;

  WalletPaymentOrderCreate({
    this.status,
    this.payment,
  });

  factory WalletPaymentOrderCreate.fromJson(Map<String, dynamic> json) =>
      WalletPaymentOrderCreate(
        status: json["status"],
        payment:
            json["payment"] == null ? null : Payment.fromJson(json["payment"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "payment": payment?.toJson(),
      };
}

class Payment {
  String? userId;
  String? orderId;
  String? value;
  String? gstAmount;
  String? totalValue;
  DateTime? currentDateTime;
  int? isSuccess;
  String? remark;
  DateTime? createdAt;
  DateTime? updatedAt;
  String razorpayId;
  String razorpaySecretKey;
  int? id;

  Payment({
    this.userId,
    this.orderId,
    this.value,
    this.gstAmount,
    this.totalValue,
    this.currentDateTime,
    this.isSuccess,
    this.remark,
    this.createdAt,
    this.updatedAt,
    required this.razorpayId,
    required this.razorpaySecretKey,
    this.id,
  });

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        userId: json["user_id"],
        orderId: json["order_id"],
        value: json["value"],
        gstAmount: json["gst_amount"],
        totalValue: json["total_value"],
        currentDateTime: json["current_date_time"] == null
            ? null
            : DateTime.parse(json["current_date_time"]),
        isSuccess: json["is_success"],
        remark: json["remark"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        // updatedAt: json["updated_at"] == null
        //     ? null
        //     : DateTime.parse(json["updated_at"]),
        razorpayId: json['RAZORPAY_KEY_ID'],
        razorpaySecretKey: json['RAZORPAY_KEY_SECRET'],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "order_id": orderId,
        "value": value,
        "gst_amount": gstAmount,
        "total_value": totalValue,
        "current_date_time": currentDateTime?.toIso8601String(),
        "is_success": isSuccess,
        "remark": remark,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "id": id,
      };
}
