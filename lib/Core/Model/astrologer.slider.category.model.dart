// To parse this JSON data, do
//
//     final sliderAstrologerCategoryModel = sliderAstrologerCategoryModelFromJson(jsonString);

import 'dart:convert';

SliderAstrologerCategoryModel sliderAstrologerCategoryModelFromJson(
        String str) =>
    SliderAstrologerCategoryModel.fromJson(json.decode(str));

String sliderAstrologerCategoryModelToJson(
        SliderAstrologerCategoryModel data) =>
    json.encode(data.toJson());

class SliderAstrologerCategoryModel {
  bool status;
  List<Datum> data;

  SliderAstrologerCategoryModel({
    required this.status,
    required this.data,
  });

  factory SliderAstrologerCategoryModel.fromJson(Map<String, dynamic> json) =>
      SliderAstrologerCategoryModel(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  int? id;
  String? astrologerCategory;
  String? title;
  String? image;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;

  Datum({
    this.id,
    this.astrologerCategory,
    this.title,
    this.image,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        astrologerCategory: json["astrologer_category"],
        title: json["title"],
        image: json["image"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "astrologer_category": astrologerCategory,
        "title": title,
        "image": image,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}
