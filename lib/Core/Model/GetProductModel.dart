// To parse this JSON data, do
//
//     final getProductModel = getProductModelFromJson(jsonString);

import 'dart:convert';

GetProductModel getProductModelFromJson(String str) => GetProductModel.fromJson(json.decode(str));

String getProductModelToJson(GetProductModel data) => json.encode(data.toJson());

class GetProductModel {
  bool status;
  List<AstrologerElement> astrologers;

  GetProductModel({
    required this.status,
    required this.astrologers,
  });

  factory GetProductModel.fromJson(Map<String, dynamic> json) => GetProductModel(
    status: json["status"],
    astrologers: List<AstrologerElement>.from(json["astrologers"].map((x) => AstrologerElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "astrologers": List<dynamic>.from(astrologers.map((x) => x.toJson())),
  };
}

class AstrologerElement {
  int id;
  String name;
  int label;
  String avatar;
  AstrologerAstrologer astrologer;

  AstrologerElement({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.astrologer,
  });

  factory AstrologerElement.fromJson(Map<String, dynamic> json) => AstrologerElement(
    id: json["id"],
    name: json["name"],
    label: json["label"],
    avatar: json["avatar"],
    astrologer: AstrologerAstrologer.fromJson(json["astrologer"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "label": label,
    "avatar": avatar,
    "astrologer": astrologer.toJson(),
  };
}

class AstrologerAstrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  String productPrice;
  String productDiscountPrice;
  List<Gallery> gallery;

  AstrologerAstrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.productPrice,
    required this.productDiscountPrice,
    required this.gallery,
  });

  factory AstrologerAstrologer.fromJson(Map<String, dynamic> json) => AstrologerAstrologer(
    id: json["id"],
    userId: json["user_id"],
    primarySkillId: json["primary_skill_id"],
    allSkillId: json["all_skill_id"],
    languageId: json["language_id"],
    experienceYear: json["experience_year"],
    bio: json["bio"],
    freeChat: json["free_chat"],
    orderCount: json["order_count"],
    isOnline: json["is_online"],
    productPrice: json["price"],
    productDiscountPrice: json["discount_price"],
    gallery: List<Gallery>.from(json["gallery"].map((x) => Gallery.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "primary_skill_id": primarySkillId,
    "all_skill_id": allSkillId,
    "language_id": languageId,
    "experience_year": experienceYear,
    "bio": bio,
    "free_chat": freeChat,
    "order_count": orderCount,
    "is_online": isOnline,
    "price": productPrice,
    "discount_price": productDiscountPrice,
    "gallery": List<dynamic>.from(gallery.map((x) => x.toJson())),
  };
}

class Gallery {
  String astrologerId;
  String image;

  Gallery({
    required this.astrologerId,
    required this.image,
  });

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
    astrologerId: json["astrologer_id"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "astrologer_id": astrologerId,
    "image": image,
  };
}
