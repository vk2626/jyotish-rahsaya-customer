// To parse this JSON data, do
//
//     final rechargeAmountModel = rechargeAmountModelFromJson(jsonString);

import 'dart:convert';

RechargeAmountModel rechargeAmountModelFromJson(String str) =>
    RechargeAmountModel.fromJson(json.decode(str));

String rechargeAmountModelToJson(RechargeAmountModel data) =>
    json.encode(data.toJson());

class RechargeAmountModel {
  bool? status;
  List<Recharge>? recharge;

  RechargeAmountModel({
    this.status,
    this.recharge,
  });

  factory RechargeAmountModel.fromJson(Map<String, dynamic> json) =>
      RechargeAmountModel(
        status: json["status"],
        recharge: json["recharge"] == null
            ? []
            : List<Recharge>.from(
                json["recharge"]!.map((x) => Recharge.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "recharge": recharge == null
            ? []
            : List<dynamic>.from(recharge!.map((x) => x.toJson())),
      };
}

class Recharge {
  int? id;
  String? discountPercentage;
  String? price;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  Recharge({
    this.id,
    this.discountPercentage,
    this.price,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory Recharge.fromJson(Map<String, dynamic> json) => Recharge(
        id: json["id"],
        discountPercentage: json["discount_percentage"],
        price: json["price"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "discount_percentage": discountPercentage,
        "price": price,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
