// To parse this JSON data, do
//
//     final orderHistoryModel = orderHistoryModelFromJson(jsonString);

import 'dart:convert';

OrderHistoryModel orderHistoryModelFromJson(String str) =>
    OrderHistoryModel.fromJson(json.decode(str));

String orderHistoryModelToJson(OrderHistoryModel data) =>
    json.encode(data.toJson());

class OrderHistoryModel {
  bool status;
  List<OrderHistory> orderHistory;

  OrderHistoryModel({
    required this.status,
    required this.orderHistory,
  });

  factory OrderHistoryModel.fromJson(Map<String, dynamic> json) =>
      OrderHistoryModel(
        status: json["status"],
        orderHistory: List<OrderHistory>.from(
            json["orderHistory"].map((x) => OrderHistory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "orderHistory": List<dynamic>.from(orderHistory.map((x) => x.toJson())),
      };
}

class OrderHistory {
  int id;
  String infoId;
  String orderId;
  String userId;
  String astrologerId;
  int orderCount;
  String type;
  int acceptByAstrologer;
  int acceptByCustomer;
  DateTime startTime;
  DateTime? endTime;
  String durationTime;
  String waitTime;
  String totalPrice;
  int isFreechat;
  int callCount;
  DateTime createdAt;
  DateTime updatedAt;
  Review review;
  Messages messages;
  User user;

  OrderHistory({
    required this.id,
    required this.infoId,
    required this.orderId,
    required this.userId,
    required this.astrologerId,
    required this.orderCount,
    required this.type,
    required this.acceptByAstrologer,
    required this.acceptByCustomer,
    required this.startTime,
    required this.endTime,
    required this.durationTime,
    required this.waitTime,
    required this.totalPrice,
    required this.isFreechat,
    required this.callCount,
    required this.createdAt,
    required this.updatedAt,
    required this.review,
    required this.messages,
    required this.user,
  });

  factory OrderHistory.fromJson(Map<String, dynamic> json) => OrderHistory(
        id: json["id"],
        infoId: json["info_id"],
        orderId: json["order_id"],
        userId: json["user_id"],
        astrologerId: json["astrologer_id"],
        orderCount: json["order_count"],
        type: json["type"],
        acceptByAstrologer: json["accept_by_astrologer"],
        acceptByCustomer: json["accept_by_customer"],
        startTime: DateTime.parse(json["start_time"]),
        endTime: json["end_time"] == null
            ? DateTime.now()
            : DateTime.parse(json["end_time"]),
        durationTime: json["duration_time"],
        waitTime: json["wait_time"],
        totalPrice: json["total_price"],
        isFreechat: json["is_freechat"],
        callCount: json["call_count"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        messages: Messages.fromJson(json["messages"]),
        review: Review.fromJson(json["review"]),
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "info_id": infoId,
        "order_id": orderId,
        "user_id": userId,
        "astrologer_id": astrologerId,
        "order_count": orderCount,
        "type": type,
        "accept_by_astrologer": acceptByAstrologer,
        "accept_by_customer": acceptByCustomer,
        "start_time": startTime.toIso8601String(),
        "end_time": endTime!.toIso8601String(),
        "duration_time": durationTime,
        "wait_time": waitTime,
        "total_price": totalPrice,
        "is_freechat": isFreechat,
        "call_count": callCount,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "review": review.toJson(),
        "messages": messages.toJson(),
        "user": user.toJson(),
      };
}

class Messages {
  String? body;
  String? attachment;

  Messages({
    required this.body,
    required this.attachment,
  });

  factory Messages.fromJson(Map<String, dynamic> json) => Messages(
        body: json["body"]??"",
        attachment: json["attachment"] ?? "Attachment",
      );

  Map<String, dynamic> toJson() => {
        "body": body,
        "attachment": attachment,
      };
}

class Review {
  int? id;
  String? infoId;
  String? orderId;
  String? type;
  String? rate;
  String? customerId;
  String? astrologerId;
  int? isHide;
  String? isLike;
  String? description;
  String? reply;
  int? isPin;
  String? commentRDelete;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;

  Review({
    this.id,
    this.infoId,
    this.orderId,
    this.type,
    this.rate,
    this.customerId,
    this.astrologerId,
    this.isHide,
    this.isLike,
    this.description,
    this.reply,
    this.isPin,
    this.commentRDelete,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory Review.fromJson(Map<String, dynamic> json) => Review(
        id: json["id"],
        infoId: json["info_id"],
        orderId: json["order_id"],
        type: json["type"],
        rate: json["rate"],
        customerId: json["customer_id"],
        astrologerId: json["astrologer_id"],
        isHide: json["is_hide"],
        isLike: json["is_like"],
        description: json["description"],
        reply: json["reply"],
        isPin: json["is_pin"],
        commentRDelete: json["comment_r_delete"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "info_id": infoId,
        "order_id": orderId,
        "type": type,
        "rate": rate,
        "customer_id": customerId,
        "astrologer_id": astrologerId,
        "is_hide": isHide,
        "is_like": isLike,
        "description": description,
        "reply": reply,
        "is_pin": isPin,
        "comment_r_delete": commentRDelete,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}

class User {
  int id;
  String name;
  int label;
  String avatar;
  int isAstrologerOnline;
  Astrologer astrologer;

  User({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isAstrologerOnline,
    required this.astrologer,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isAstrologerOnline: json['is_online_astrologer'],
        astrologer: Astrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isAstrologerOnline,
        "astrologer": astrologer.toJson(),
      };
}

class Astrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  String chatPrice;
  String chatDiscountPrice;
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;
  String displayName;
  List<Gallery> gallery;

  Astrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.chatPrice,
    required this.chatDiscountPrice,
    required this.callPrice,
    required this.callDiscountPrice,
    required this.videoPrice,
    required this.videoDiscountPrice,
    required this.displayName,
    required this.gallery,
  });

  factory Astrologer.fromJson(Map<String, dynamic> json) => Astrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        bio: json["bio"],
        freeChat: json["free_chat"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        chatPrice: json["chat_price"],
        chatDiscountPrice: json["chat_discount_price"],
        callPrice: json["call_price"],
        callDiscountPrice: json["call_discount_price"],
        videoPrice: json["video_price"],
        videoDiscountPrice: json["video_discount_price"],
        displayName: json["display_name"],
        gallery:
            List<Gallery>.from(json["gallery"].map((x) => Gallery.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "gallery": List<dynamic>.from(gallery.map((x) => x.toJson())),
      };
}

class Gallery {
  String astrologerId;
  String image;

  Gallery({
    required this.astrologerId,
    required this.image,
  });

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
        astrologerId: json["astrologer_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "astrologer_id": astrologerId,
        "image": image,
      };
}
