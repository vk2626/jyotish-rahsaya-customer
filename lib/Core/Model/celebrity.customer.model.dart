// To parse this JSON data, do
//
//     final celebrityCustomerModel = celebrityCustomerModelFromJson(jsonString);

import 'dart:convert';

CelebrityCustomerModel celebrityCustomerModelFromJson(String str) =>
    CelebrityCustomerModel.fromJson(json.decode(str));

String celebrityCustomerModelToJson(CelebrityCustomerModel data) =>
    json.encode(data.toJson());

class CelebrityCustomerModel {
  bool status;
  List<Datum> data;

  CelebrityCustomerModel({
    required this.status,
    required this.data,
  });

  factory CelebrityCustomerModel.fromJson(Map<String, dynamic> json) =>
      CelebrityCustomerModel(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int? id;
  String? title;
  String? url;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  Datum({
    this.id,
    this.title,
    this.url,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        title: json["title"],
        url: json["url"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "url": url,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
