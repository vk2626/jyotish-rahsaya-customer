// To parse this JSON data, do
//
//     final astrologerListModel = astrologerListModelFromJson(jsonString);

import 'dart:convert';

AstrologerListModel astrologerListModelFromJson(String str) =>
    AstrologerListModel.fromJson(json.decode(str));

String astrologerListModelToJson(AstrologerListModel data) =>
    json.encode(data.toJson());

class AstrologerListModel {
  bool? status;
  AstrologersAstrologers? astrologers;

  AstrologerListModel({
    this.status,
    this.astrologers,
  });

  factory AstrologerListModel.fromJson(Map<String, dynamic> json) =>
      AstrologerListModel(
        status: json["status"],
        astrologers: AstrologersAstrologers.fromJson(json["astrologers"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "astrologers": astrologers!.toJson(),
      };
}

class AstrologersAstrologers {
  List<AstrologerElement> data;

  AstrologersAstrologers({
    required this.data,
  });

  factory AstrologersAstrologers.fromJson(Map<String, dynamic> json) =>
      AstrologersAstrologers(
        data: List<AstrologerElement>.from(
            json["data"].map((x) => AstrologerElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class AstrologerElement {
  int id;
  String name;
  int label;
  String avatar;
  double rating;
  int isFreeChatCompleted;
  String totalOrderCount;
  AstrologerAstrologer astrologer;
  int isAstrologerOnline;

  AstrologerElement({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.rating,
    required this.isFreeChatCompleted,
    required this.totalOrderCount,
    required this.astrologer,
    required this.isAstrologerOnline,
  });

  factory AstrologerElement.fromJson(Map<String, dynamic> json) =>
      AstrologerElement(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isFreeChatCompleted: json["is_free_enable"] ??
            1, //Free Chat Enable 1 Means FreeChat is completed for the user
        rating: json["rating"]?.toDouble(),
        totalOrderCount: json['total_order_count'],
        isAstrologerOnline: json['is_online_astrologer'],
        astrologer: AstrologerAstrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "rating": rating,
        "is_free_enable": isFreeChatCompleted,
        "is_online_astrologer": isAstrologerOnline,
        "total_order_count": totalOrderCount,
        "avatar": avatar,
        "astrologer": astrologer.toJson(),
      };
}

class AstrologerAstrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  int chatPrice;
  int chatDiscountPrice;
  int callPrice;
  int callDiscountPrice;
  int videoPrice;
  int videoDiscountPrice;
  String displayName;
  int statusCall;
  int statusChat;
  int statusVideo;
  num? waitTime;
  String labelText;
  String primarySkillName;
  String allSkillName;
  String languageName;
  Nextonlines nextonlines;

  AstrologerAstrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.chatPrice,
    required this.chatDiscountPrice,
    required this.callPrice,
    required this.callDiscountPrice,
    required this.videoPrice,
    required this.videoDiscountPrice,
    required this.displayName,
    required this.statusCall,
    required this.statusChat,
    required this.statusVideo,
    required this.waitTime,
    required this.labelText,
    required this.primarySkillName,
    required this.allSkillName,
    required this.languageName,
    required this.nextonlines,
  });

  factory AstrologerAstrologer.fromJson(Map<String, dynamic> json) =>
      AstrologerAstrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        bio: json["bio"],
        freeChat: json["free_chat"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        chatPrice: int.parse(json["chat_price"]),
        chatDiscountPrice: int.parse(json["chat_discount_price"]),
        callPrice: int.parse(json["call_price"]),
        callDiscountPrice: int.parse(json["call_discount_price"]),
        videoPrice: int.parse(json["video_price"]),
        videoDiscountPrice: int.parse(json["video_discount_price"]),
        displayName: json["display_name"]!,
        statusCall: json["status_call"],
        statusChat: json["status_chat"],
        statusVideo: json["status_video"],
        waitTime: json["wait_time"] ?? 0,
        labelText: json['label_text'],
        primarySkillName: json["primary_skill_name"],
        allSkillName: json["all_skill_name"],
        languageName: json["language_name"]!,
        nextonlines: Nextonlines.fromJson(json["nextonlines"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "status_call": statusCall,
        "status_chat": statusChat,
        "status_video": statusVideo,
        "label_text": labelText,
        "primary_skill_name": primarySkillName,
        "all_skill_name": allSkillName,
        "language_name": languageName,
        "nextonlines": nextonlines.toJson(),
      };
}

class Nextonlines {
  String chatNODate;
  String chatNOTime;
  String callNODate;
  String callNOTime;
  String videoNODate;
  String videoNOTime;

  Nextonlines({
    required this.chatNODate,
    required this.chatNOTime,
    required this.callNODate,
    required this.callNOTime,
    required this.videoNODate,
    required this.videoNOTime,
  });

  factory Nextonlines.fromJson(Map<String, dynamic> json) => Nextonlines(
        chatNODate: json["chat_n_o_date"],
        chatNOTime: json["chat_n_o_time"],
        callNODate: json["call_n_o_date"],
        callNOTime: json["call_n_o_time"],
        videoNODate: json["video_n_o_date"],
        videoNOTime: json["video_n_o_time"],
      );

  Map<String, dynamic> toJson() => {
        "chat_n_o_date": chatNODate,
        "chat_n_o_time": chatNOTime,
        "call_n_o_date": callNODate,
        "call_n_o_time": callNOTime,
        "video_n_o_date": videoNODate,
        "video_n_o_time": videoNOTime,
      };
}
