import 'dart:convert';
/// status : true
/// data : [{"id":1,"language":"Hindi","status":1,"created_at":"2023-12-04T21:51:05.000000Z","updated_at":"2023-12-04T21:52:21.000000Z"},{"id":2,"language":"English","status":1,"created_at":"2023-12-04T22:20:56.000000Z","updated_at":null},{"id":3,"language":"Urdu","status":1,"created_at":"2023-12-26T16:23:42.000000Z","updated_at":null},{"id":4,"language":"gujrati","status":1,"created_at":"2023-12-26T16:28:01.000000Z","updated_at":null},{"id":5,"language":"marathi","status":1,"created_at":"2023-12-26T16:28:23.000000Z","updated_at":null}]

LanguageModel languageModelFromJson(String str) => LanguageModel.fromJson(json.decode(str));
String languageModelToJson(LanguageModel data) => json.encode(data.toJson());
class LanguageModel {
  LanguageModel({
      bool? status, 
      List<Data>? data,}){
    _status = status;
    _data = data;
}

  LanguageModel.fromJson(dynamic json) {
    _status = json['status'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  bool? _status;
  List<Data>? _data;
LanguageModel copyWith({  bool? status,
  List<Data>? data,
}) => LanguageModel(  status: status ?? _status,
  data: data ?? _data,
);
  bool? get status => _status;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1
/// language : "Hindi"
/// status : 1
/// created_at : "2023-12-04T21:51:05.000000Z"
/// updated_at : "2023-12-04T21:52:21.000000Z"

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      num? id, 
      String? language, 
      num? status, 
      String? createdAt, 
      String? updatedAt,}){
    _id = id;
    _language = language;
    _status = status;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _language = json['language'];
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }
  num? _id;
  String? _language;
  num? _status;
  String? _createdAt;
  String? _updatedAt;
Data copyWith({  num? id,
  String? language,
  num? status,
  String? createdAt,
  String? updatedAt,
}) => Data(  id: id ?? _id,
  language: language ?? _language,
  status: status ?? _status,
  createdAt: createdAt ?? _createdAt,
  updatedAt: updatedAt ?? _updatedAt,
);
  num? get id => _id;
  String? get language => _language;
  num? get status => _status;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['language'] = _language;
    map['status'] = _status;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }

}