// To parse this JSON data, do
//
//     final followingModel = followingModelFromJson(jsonString);

import 'dart:convert';

FollowingModel followingModelFromJson(String str) =>
    FollowingModel.fromJson(json.decode(str));

String followingModelToJson(FollowingModel data) => json.encode(data.toJson());

class FollowingModel {
  bool? status;
  List<FollowerDetail>? followerDetails;

  FollowingModel({
    this.status,
    this.followerDetails,
  });

  factory FollowingModel.fromJson(Map<String, dynamic> json) => FollowingModel(
        status: json["status"],
        followerDetails: json["followerDetails"] == null
            ? []
            : List<FollowerDetail>.from(json["followerDetails"]!
                .map((x) => FollowerDetail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "followerDetails": followerDetails == null
            ? []
            : List<dynamic>.from(followerDetails!.map((x) => x.toJson())),
      };
}

class FollowerDetail {
  int id;
  String name;
  int label;
  String avatar;
  int isOnlineAstrologer;
  Astrologer astrologer;

  FollowerDetail({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.astrologer,
  });

  factory FollowerDetail.fromJson(Map<String, dynamic> json) => FollowerDetail(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        astrologer: Astrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "astrologer": astrologer.toJson(),
      };
}

class Astrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String orderCount;
  int isOnline;
  String displayName;
  String labelText;
  String primarySkillName;
  String allSkillName;
  String languageName;
  int rating;

  Astrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.orderCount,
    required this.isOnline,
    required this.displayName,
    required this.labelText,
    required this.primarySkillName,
    required this.allSkillName,
    required this.languageName,
    required this.rating,
  });

  factory Astrologer.fromJson(Map<String, dynamic> json) => Astrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        displayName: json["display_name"],
        labelText: json["label_text"],
        primarySkillName: json["primary_skill_name"],
        allSkillName: json["all_skill_name"],
        languageName: json["language_name"],
        rating: json["rating"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "order_count": orderCount,
        "is_online": isOnline,
        "display_name": displayName,
        "label_text": labelText,
        "primary_skill_name": primarySkillName,
        "all_skill_name": allSkillName,
        "language_name": languageName,
        "rating": rating,
      };
}
