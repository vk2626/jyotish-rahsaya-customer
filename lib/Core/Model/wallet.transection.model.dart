// To parse this JSON data, do
//
//     final walletTransactionModel = walletTransactionModelFromJson(jsonString);

import 'dart:convert';

WalletTransactionModel walletTransactionModelFromJson(String str) =>
    WalletTransactionModel.fromJson(json.decode(str));

String walletTransactionModelToJson(WalletTransactionModel data) =>
    json.encode(data.toJson());

class WalletTransactionModel {
  bool? status;
  String? walletBalance;
  List<WalletList>? walletList;

  WalletTransactionModel({
    this.status,
    this.walletBalance,
    this.walletList,
  });

  factory WalletTransactionModel.fromJson(Map<String, dynamic> json) =>
      WalletTransactionModel(
        status: json["status"],
        walletBalance: json['walletBalance'] ?? "0",
        walletList: json["walletList"] == null
            ? []
            : List<WalletList>.from(
                json["walletList"]!.map((x) => WalletList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "walletBalance": walletBalance,
        "walletList": walletList == null
            ? []
            : List<dynamic>.from(walletList!.map((x) => x.toJson())),
      };
}

class WalletList {
  int? id;
  String? userId;
  String? paymentId;
  dynamic? amount;
  dynamic? balance;
  DateTime? createdAt;
  String? paymentType;
  String? remark;
  dynamic? cashback;
  int? isSuccess;
  String? gst_amount;
  String? invoice;

  WalletList({
    this.id,
    this.userId,
    this.paymentId,
    this.amount,
    this.balance,
    this.createdAt,
    this.paymentType,
    this.remark,
    this.isSuccess,
    this.cashback,
    this.gst_amount,
    this.invoice,
  });

  factory WalletList.fromJson(Map<String, dynamic> json) => WalletList(
        id: json["id"],
        userId: json["user_id"],
        paymentId: json["payment_id"],
        amount: json["amount"],
        balance: json["balance"],
        isSuccess: json["is_success"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        paymentType: json["payment_type"],
        remark: json["remark"],
        cashback: json["cashback"],
        gst_amount: json["gst_amount"],
        invoice: json["invoice"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "payment_id": paymentId,
        "amount": amount,
        "balance": balance,
        "created_at": createdAt!.toLocal(),
        "payment_type": paymentType,
        "remark": remark,
        "is_success": isSuccess,
        "cashback": cashback,
        "gst_amount": gst_amount,
        "invoice": invoice,
      };
}
