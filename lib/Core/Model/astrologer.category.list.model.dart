// To parse this JSON data, do
//
//     final astrologerCategoryListModel = astrologerCategoryListModelFromJson(jsonString);

import 'dart:convert';

AstrologerCategoryListModel astrologerCategoryListModelFromJson(String str) =>
    AstrologerCategoryListModel.fromJson(json.decode(str));

String astrologerCategoryListModelToJson(AstrologerCategoryListModel data) =>
    json.encode(data.toJson());

class AstrologerCategoryListModel {
  bool status;
  List<Datum> data;

  AstrologerCategoryListModel({
    required this.status,
    required this.data,
  });

  factory AstrologerCategoryListModel.fromJson(Map<String, dynamic> json) =>
      AstrologerCategoryListModel(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  String? id;
  String? name;
  String? slug;
  String? image;
  String color;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  Datum({
    this.id,
    this.name,
    this.slug,
    this.image,
    required this.color,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        image: json["image"],
        status: json["status"],
        color: json['color'],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "image": image,
        "color": color,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
