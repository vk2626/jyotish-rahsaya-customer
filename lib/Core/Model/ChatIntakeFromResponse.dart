import 'dart:convert';
/// status : true
/// message : "UserInfo registered successfully!"
/// data : {"user_id":"185","first_name":"Abhinav","last_name":"singh","gender":"Male","dob":"2023-12-06","not_exact_dob_time":"0","dob_time":"17.58 PM","dob_place":"Delhi","relationship_status":"","occupation":"software","topic_concern":"","other_text":"","created_at":"2024-03-11T06:40:03.000000Z","updated_at":"2024-03-11T06:40:03.000000Z","id":16}

ChatIntakeFromResponse chatIntakeFromResponseFromJson(String str) => ChatIntakeFromResponse.fromJson(json.decode(str));
String chatIntakeFromResponseToJson(ChatIntakeFromResponse data) => json.encode(data.toJson());
class ChatIntakeFromResponse {
  ChatIntakeFromResponse({
      bool? status, 
      String? message, 
      Data? data,}){
    _status = status;
    _message = message;
    _data = data;
}

  ChatIntakeFromResponse.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  bool? _status;
  String? _message;
  Data? _data;
ChatIntakeFromResponse copyWith({  bool? status,
  String? message,
  Data? data,
}) => ChatIntakeFromResponse(  status: status ?? _status,
  message: message ?? _message,
  data: data ?? _data,
);
  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// user_id : "185"
/// first_name : "Abhinav"
/// last_name : "singh"
/// gender : "Male"
/// dob : "2023-12-06"
/// not_exact_dob_time : "0"
/// dob_time : "17.58 PM"
/// dob_place : "Delhi"
/// relationship_status : ""
/// occupation : "software"
/// topic_concern : ""
/// other_text : ""
/// created_at : "2024-03-11T06:40:03.000000Z"
/// updated_at : "2024-03-11T06:40:03.000000Z"
/// id : 16

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      String? userId, 
      String? firstName, 
      String? lastName, 
      String? gender, 
      String? dob, 
      int? notExactDobTime,
      String? dobTime, 
      String? dobPlace, 
      String? relationshipStatus, 
      String? occupation, 
      String? topicConcern, 
      String? otherText, 
      String? createdAt, 
      String? updatedAt, 
      num? id,}){
    _userId = userId;
    _firstName = firstName;
    _lastName = lastName;
    _gender = gender;
    _dob = dob;
    _notExactDobTime = notExactDobTime;
    _dobTime = dobTime;
    _dobPlace = dobPlace;
    _relationshipStatus = relationshipStatus;
    _occupation = occupation;
    _topicConcern = topicConcern;
    _otherText = otherText;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _id = id;
}

  Data.fromJson(dynamic json) {
    _userId = json['user_id'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _gender = json['gender'];
    _dob = json['dob'];
    _notExactDobTime = json['not_exact_dob_time'];
    _dobTime = json['dob_time'];
    _dobPlace = json['dob_place'];
    _relationshipStatus = json['relationship_status'];
    _occupation = json['occupation'];
    _topicConcern = json['topic_concern'];
    _otherText = json['other_text'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _id = json['id'];
  }
  String? _userId;
  String? _firstName;
  String? _lastName;
  String? _gender;
  String? _dob;
  int? _notExactDobTime;
  String? _dobTime;
  String? _dobPlace;
  String? _relationshipStatus;
  String? _occupation;
  String? _topicConcern;
  String? _otherText;
  String? _createdAt;
  String? _updatedAt;
  num? _id;
Data copyWith({  String? userId,
  String? firstName,
  String? lastName,
  String? gender,
  String? dob,
  int? notExactDobTime,
  String? dobTime,
  String? dobPlace,
  String? relationshipStatus,
  String? occupation,
  String? topicConcern,
  String? otherText,
  String? createdAt,
  String? updatedAt,
  num? id,
}) => Data(  userId: userId ?? _userId,
  firstName: firstName ?? _firstName,
  lastName: lastName ?? _lastName,
  gender: gender ?? _gender,
  dob: dob ?? _dob,
  notExactDobTime: notExactDobTime ?? _notExactDobTime,
  dobTime: dobTime ?? _dobTime,
  dobPlace: dobPlace ?? _dobPlace,
  relationshipStatus: relationshipStatus ?? _relationshipStatus,
  occupation: occupation ?? _occupation,
  topicConcern: topicConcern ?? _topicConcern,
  otherText: otherText ?? _otherText,
  createdAt: createdAt ?? _createdAt,
  updatedAt: updatedAt ?? _updatedAt,
  id: id ?? _id,
);
  String? get userId => _userId;
  String? get firstName => _firstName;
  String? get lastName => _lastName;
  String? get gender => _gender;
  String? get dob => _dob;
  int? get notExactDobTime => _notExactDobTime;
  String? get dobTime => _dobTime;
  String? get dobPlace => _dobPlace;
  String? get relationshipStatus => _relationshipStatus;
  String? get occupation => _occupation;
  String? get topicConcern => _topicConcern;
  String? get otherText => _otherText;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user_id'] = _userId;
    map['first_name'] = _firstName;
    map['last_name'] = _lastName;
    map['gender'] = _gender;
    map['dob'] = _dob;
    map['not_exact_dob_time'] = _notExactDobTime;
    map['dob_time'] = _dobTime;
    map['dob_place'] = _dobPlace;
    map['relationship_status'] = _relationshipStatus;
    map['occupation'] = _occupation;
    map['topic_concern'] = _topicConcern;
    map['other_text'] = _otherText;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['id'] = _id;
    return map;
  }

}