import 'dart:convert';

// Function to deserialize UserList JSON string
UserList userListFromJson(String str) => UserList.fromJson(json.decode(str));

// Function to serialize UserList object to JSON string
String userListToJson(UserList data) => json.encode(data.toJson());

// UserList class definition
class UserList {
  UserList({
    this.status,
    this.userListInfo,
  });

  bool? status;
  List<UserListInfo>? userListInfo;

  // Factory method to create UserList object from JSON
  factory UserList.fromJson(Map<String, dynamic> json) => UserList(
        status: json['status'],
        userListInfo: json['userListInfo'] != null
            ? List<UserListInfo>.from(
                json['userListInfo'].map((x) => UserListInfo.fromJson(x)))
            : null,
      );

  // Method to create a copy of UserList object with optional parameters
  UserList copyWith({
    bool? status,
    List<UserListInfo>? userListInfo,
  }) =>
      UserList(
        status: status ?? this.status,
        userListInfo: userListInfo ?? this.userListInfo,
      );

  // Method to convert UserList object to JSON
  Map<String, dynamic> toJson() => {
        'status': status,
        'userListInfo': userListInfo != null
            ? List<dynamic>.from(userListInfo!.map((x) => x.toJson()))
            : null,
      };
}

// UserListInfo class definition
class UserListInfo {
  UserListInfo({
    this.id,
    this.userId,
    this.firstName,
    this.lastName,
    this.image,
    this.gender,
    this.dob,
    this.dobPlace,
    this.dobTime,
    this.notExactDobTime,
    this.occupation,
    this.topicConcern,
    this.otherText,
    this.relationshipStatus,
    this.createdAt,
    this.updatedAt,
    this.lat, // Added latitude field
    this.lon, // Added longitude field
  });

  num? id;
  String? userId;
  String? firstName;
  dynamic lastName;
  dynamic image;
  String? gender;
  String? dob;
  String? dobPlace;
  String? dobTime;
  num? notExactDobTime;
  dynamic occupation;
  String? topicConcern;
  String? otherText;
  String? relationshipStatus;
  String? createdAt;
  String? updatedAt;
  String? lat; // Latitude field
  String? lon; // Longitude field

  // Factory method to create UserListInfo object from JSON
  factory UserListInfo.fromJson(Map<String, dynamic> json) => UserListInfo(
        id: json['id'],
        userId: json['user_id'],
        firstName: json['first_name'],
        lastName: json['last_name'],
        image: json['image'],
        gender: json['gender'],
        dob: json['dob'],
        dobPlace: json['dob_place'],
        dobTime: json['dob_time'],
        notExactDobTime: json['not_exact_dob_time'],
        occupation: json['occupation'],
        topicConcern: json['topic_concern'],
        otherText: json['other_text'],
        relationshipStatus: json['relationship_status'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        lat: json['lat'], // Deserialize latitude
        lon: json['lon'], // Deserialize longitude
      );

  // Method to convert UserListInfo object to JSON
  Map<String, dynamic> toJson() => {
        'id': id,
        'user_id': userId,
        'first_name': firstName,
        'last_name': lastName,
        'image': image,
        'gender': gender,
        'dob': dob,
        'dob_place': dobPlace,
        'dob_time': dobTime,
        'not_exact_dob_time': notExactDobTime,
        'occupation': occupation,
        'topic_concern': topicConcern,
        'other_text': otherText,
        'relationship_status': relationshipStatus,
        'created_at': createdAt,
        'updated_at': updatedAt,
        'lat': lat, // Serialize latitude
        'lon': lon, // Serialize longitude
      };

  // Method to create a copy of UserListInfo object with optional parameters
  UserListInfo copyWith({
    num? id,
    String? userId,
    String? firstName,
    dynamic lastName,
    dynamic image,
    String? gender,
    String? dob,
    String? dobPlace,
    String? dobTime,
    num? notExactDobTime,
    dynamic occupation,
    String? topicConcern,
    String? otherText,
    String? relationshipStatus,
    String? createdAt,
    String? updatedAt,
    String? lat,
    String? lon,
  }) =>
      UserListInfo(
        id: id ?? this.id,
        userId: userId ?? this.userId,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        image: image ?? this.image,
        gender: gender ?? this.gender,
        dob: dob ?? this.dob,
        dobPlace: dobPlace ?? this.dobPlace,
        dobTime: dobTime ?? this.dobTime,
        notExactDobTime: notExactDobTime ?? this.notExactDobTime,
        occupation: occupation ?? this.occupation,
        topicConcern: topicConcern ?? this.topicConcern,
        otherText: otherText ?? this.otherText,
        relationshipStatus: relationshipStatus ?? this.relationshipStatus,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        lat: lat ?? this.lat,
        lon: lon ?? this.lon,
      );
}
