import 'dart:convert';
/// status : true
/// message : " Customer Registration  successfully !"
/// data : {"user_id":"167","gender":"Male","dob":"2023-12-06","dob_time":"17.58 PM","dob_place":"Delhi","address":"india","current_address":"noida","pincode":"232123","created_at":"2024-03-11T09:48:48.000000Z","updated_at":"2024-03-11T09:48:48.000000Z","id":7}

UpdateProfileResponse updateProfileResponseFromJson(String str) => UpdateProfileResponse.fromJson(json.decode(str));
String updateProfileResponseToJson(UpdateProfileResponse data) => json.encode(data.toJson());
class UpdateProfileResponse {
  UpdateProfileResponse({
      bool? status, 
      String? message, 
      Data? data,}){
    _status = status;
    _message = message;
    _data = data;
}

  UpdateProfileResponse.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  bool? _status;
  String? _message;
  Data? _data;
UpdateProfileResponse copyWith({  bool? status,
  String? message,
  Data? data,
}) => UpdateProfileResponse(  status: status ?? _status,
  message: message ?? _message,
  data: data ?? _data,
);
  bool? get status => _status;
  String? get message => _message;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// user_id : "167"
/// gender : "Male"
/// dob : "2023-12-06"
/// dob_time : "17.58 PM"
/// dob_place : "Delhi"
/// address : "india"
/// current_address : "noida"
/// pincode : "232123"
/// created_at : "2024-03-11T09:48:48.000000Z"
/// updated_at : "2024-03-11T09:48:48.000000Z"
/// id : 7

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      String? userId, 
      String? gender, 
      String? dob, 
      String? dobTime, 
      String? dobPlace, 
      String? address, 
      String? currentAddress, 
      String? pincode, 
      String? createdAt, 
      String? updatedAt, 
      num? id,}){
    _userId = userId;
    _gender = gender;
    _dob = dob;
    _dobTime = dobTime;
    _dobPlace = dobPlace;
    _address = address;
    _currentAddress = currentAddress;
    _pincode = pincode;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _id = id;
}

  Data.fromJson(dynamic json) {
    _userId = json['user_id'];
    _gender = json['gender'];
    _dob = json['dob'];
    _dobTime = json['dob_time'];
    _dobPlace = json['dob_place'];
    _address = json['address'];
    _currentAddress = json['current_address'];
    _pincode = json['pincode'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _id = json['id'];
  }
  String? _userId;
  String? _gender;
  String? _dob;
  String? _dobTime;
  String? _dobPlace;
  String? _address;
  String? _currentAddress;
  String? _pincode;
  String? _createdAt;
  String? _updatedAt;
  num? _id;
Data copyWith({  String? userId,
  String? gender,
  String? dob,
  String? dobTime,
  String? dobPlace,
  String? address,
  String? currentAddress,
  String? pincode,
  String? createdAt,
  String? updatedAt,
  num? id,
}) => Data(  userId: userId ?? _userId,
  gender: gender ?? _gender,
  dob: dob ?? _dob,
  dobTime: dobTime ?? _dobTime,
  dobPlace: dobPlace ?? _dobPlace,
  address: address ?? _address,
  currentAddress: currentAddress ?? _currentAddress,
  pincode: pincode ?? _pincode,
  createdAt: createdAt ?? _createdAt,
  updatedAt: updatedAt ?? _updatedAt,
  id: id ?? _id,
);
  String? get userId => _userId;
  String? get gender => _gender;
  String? get dob => _dob;
  String? get dobTime => _dobTime;
  String? get dobPlace => _dobPlace;
  String? get address => _address;
  String? get currentAddress => _currentAddress;
  String? get pincode => _pincode;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user_id'] = _userId;
    map['gender'] = _gender;
    map['dob'] = _dob;
    map['dob_time'] = _dobTime;
    map['dob_place'] = _dobPlace;
    map['address'] = _address;
    map['current_address'] = _currentAddress;
    map['pincode'] = _pincode;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['id'] = _id;
    return map;
  }

}