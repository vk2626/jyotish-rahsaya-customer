// To parse this JSON data, do
//
//     final dashaModel = dashaModelFromJson(jsonString);

import 'dart:convert';

DashaModel dashaModelFromJson(String str) =>
    DashaModel.fromJson(json.decode(str));

String dashaModelToJson(DashaModel data) => json.encode(data.toJson());

class DashaModel {
  String status;
  DashaData data;

  DashaModel({
    required this.status,
    required this.data,
  });

  factory DashaModel.fromJson(Map<String, dynamic> json) => DashaModel(
        status: json["status"],
        data: DashaData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class DashaData {
  List<DashaPeriod> dashaPeriods;
  DashaBalance dashaBalance;

  DashaData({
    required this.dashaPeriods,
    required this.dashaBalance,
  });

  factory DashaData.fromJson(Map<String, dynamic> json) => DashaData(
        dashaPeriods: List<DashaPeriod>.from(
            json["dasha_periods"].map((x) => DashaPeriod.fromJson(x))),
        dashaBalance: DashaBalance.fromJson(json["dasha_balance"]),
      );

  Map<String, dynamic> toJson() => {
        "dasha_periods":
            List<dynamic>.from(dashaPeriods.map((x) => x.toJson())),
        "dasha_balance": dashaBalance.toJson(),
      };
}

class DashaBalance {
  Lord lord;
  String duration;
  String description;

  DashaBalance({
    required this.lord,
    required this.duration,
    required this.description,
  });

  factory DashaBalance.fromJson(Map<String, dynamic> json) => DashaBalance(
        lord: Lord.fromJson(json["lord"]),
        duration: json["duration"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "lord": lord.toJson(),
        "duration": duration,
        "description": description,
      };
}

class Lord {
  int id;
  String name;
  String vedicName;

  Lord({
    required this.id,
    required this.name,
    required this.vedicName,
  });

  factory Lord.fromJson(Map<String, dynamic> json) => Lord(
        id: json["id"],
        name: json["name"],
        vedicName: json["vedic_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "vedic_name": vedicName,
      };
}

class DashaPeriod {
  int id;
  String name;
  DateTime start;
  DateTime end;
  List<DashaPeriod>? antardasha;
  List<DashaPeriod>? pratyantardasha;

  DashaPeriod({
    required this.id,
    required this.name,
    required this.start,
    required this.end,
    this.antardasha,
    this.pratyantardasha,
  });

  factory DashaPeriod.fromJson(Map<String, dynamic> json) => DashaPeriod(
        id: json["id"],
        name: json["name"],
        start: DateTime.parse(json["start"]),
        end: DateTime.parse(json["end"]),
        antardasha: json["antardasha"] == null
            ? []
            : List<DashaPeriod>.from(
                json["antardasha"]!.map((x) => DashaPeriod.fromJson(x))),
        pratyantardasha: json["pratyantardasha"] == null
            ? []
            : List<DashaPeriod>.from(
                json["pratyantardasha"]!.map((x) => DashaPeriod.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "start": start.toIso8601String(),
        "end": end.toIso8601String(),
        "antardasha": antardasha == null
            ? []
            : List<dynamic>.from(antardasha!.map((x) => x.toJson())),
        "pratyantardasha": pratyantardasha == null
            ? []
            : List<dynamic>.from(pratyantardasha!.map((x) => x.toJson())),
      };
}
