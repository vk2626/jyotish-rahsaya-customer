// To parse this JSON data, do
//
//     final kundliListModel = kundliListModelFromJson(jsonString);

import 'dart:convert';

KundliListModel kundliListModelFromJson(String str) =>
    KundliListModel.fromJson(json.decode(str));

String kundliListModelToJson(KundliListModel data) =>
    json.encode(data.toJson());

class KundliListModel {
  bool status;
  List<KundliInformationList> kundliInformationList;

  KundliListModel({
    required this.status,
    required this.kundliInformationList,
  });

  factory KundliListModel.fromJson(Map<String, dynamic> json) =>
      KundliListModel(
        status: json["status"],
        kundliInformationList: List<KundliInformationList>.from(
            json["kundliInformationList"]
                .map((x) => KundliInformationList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "kundliInformationList":
            List<dynamic>.from(kundliInformationList.map((x) => x.toJson())),
      };
}

class KundliInformationList {
  int id;
  String orderId;
  String infoId;
  String userId;
  String name;
  String gender;
  String dobTime;
  String dob;
  String pob;
  String lng;
  String lat;
  String kundliDetails;

  KundliInformationList(
      {required this.id,
      required this.orderId,
      required this.infoId,
      required this.userId,
      required this.name,
      required this.gender,
      required this.dobTime,
      required this.dob,
      required this.pob,
      required this.lng,
      required this.lat,
      required this.kundliDetails});

  factory KundliInformationList.fromJson(Map<String, dynamic> json) =>
      KundliInformationList(
          id: json["id"],
          orderId: json["order_id"],
          infoId: json["info_id"],
          userId: json["user_id"],
          name: json["name"],
          gender: json["gender"],
          dobTime: json["dob_time"],
          dob: json["dob"],
          pob: json["pob"],
          lng: json["lng"],
          lat: json["lat"],
          kundliDetails: json["kundli_details"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "info_id": infoId,
        "user_id": userId,
        "name": name,
        "gender": gender,
        "dob_time": dobTime,
        "dob": dob,
        "pob": pob,
        "lng": lng,
        "lat": lat,
        "kundli_details": kundliDetails
      };
}
