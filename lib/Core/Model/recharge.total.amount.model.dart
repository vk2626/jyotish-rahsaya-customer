// To parse this JSON data, do
//
//     final rechargeTotalAmountModel = rechargeTotalAmountModelFromJson(jsonString);

import 'dart:convert';

RechargeTotalAmountModel rechargeTotalAmountModelFromJson(String str) =>
    RechargeTotalAmountModel.fromJson(json.decode(str));

String rechargeTotalAmountModelToJson(RechargeTotalAmountModel data) =>
    json.encode(data.toJson());

class RechargeTotalAmountModel {
  bool? status;
  Recharge? recharge;

  RechargeTotalAmountModel({
    this.status,
    this.recharge,
  });

  factory RechargeTotalAmountModel.fromJson(Map<String, dynamic> json) =>
      RechargeTotalAmountModel(
        status: json["status"],
        recharge: Recharge.fromJson(json["recharge"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "recharge": recharge!.toJson(),
      };
}

class Recharge {
  String price;
  String discountPercentage;
  num cashback;
  num gstAmount;
  num totalPriceWithGst;

  Recharge({
    required this.price,
    required this.discountPercentage,
    required this.cashback,
    required this.gstAmount,
    required this.totalPriceWithGst,
  });

  factory Recharge.fromJson(Map<String, dynamic> json) => Recharge(
        price: json["price"],
        discountPercentage: json["discount_percentage"],
        cashback: json["cashback"],
        gstAmount: json["gst_amount"]?.toDouble(),
        totalPriceWithGst: json["total_price_with_gst"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "price": price,
        "discount_percentage": discountPercentage,
        "cashback": cashback,
        "gst_amount": gstAmount,
        "total_price_with_gst": totalPriceWithGst,
      };
}
