// To parse this JSON data, do
//
//     final customerRemedyPurchasedListModel = customerRemedyPurchasedListModelFromJson(jsonString);

import 'dart:convert';

CustomerRemedyPurchasedListModel customerRemedyPurchasedListModelFromJson(
        String str) =>
    CustomerRemedyPurchasedListModel.fromJson(json.decode(str));

String customerRemedyPurchasedListModelToJson(
        CustomerRemedyPurchasedListModel data) =>
    json.encode(data.toJson());

class CustomerRemedyPurchasedListModel {
  bool status;
  List<ProductList> productList;

  CustomerRemedyPurchasedListModel({
    required this.status,
    required this.productList,
  });

  factory CustomerRemedyPurchasedListModel.fromJson(
          Map<String, dynamic> json) =>
      CustomerRemedyPurchasedListModel(
        status: json["status"],
        productList: List<ProductList>.from(
            json["productList"].map((x) => ProductList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "productList": List<dynamic>.from(productList.map((x) => x.toJson())),
      };
}

class ProductList {
  int id;
  String orderId;
  String userId;
  String productPrice;
  String astrologerId;
  String suggestedAstroId;
  String remedyOrderId;
  String remedyId;
  String poojaId;
  String transactionId;
  String amount;
  String gstAmount;
  String walletDeducted;
  String totalAmount;

  int isSuccess;
  int orderStatus;

  String paymentType;
  String remark;
  DateTime currentDateTime;
  DateTime createdAt;
  DateTime updatedAt;
  String productTitle;
  String productImage;
  String suggestedAstroName;
  String categoryName;

  ProductList({
    required this.id,
    required this.orderId,
    required this.userId,
    required this.productPrice,
    required this.astrologerId,
    required this.suggestedAstroId,
    required this.remedyId,
    required this.remedyOrderId,
    required this.poojaId,
    required this.transactionId,
    required this.amount,
    required this.gstAmount,
    required this.walletDeducted,
    required this.totalAmount,
    required this.isSuccess,
    required this.orderStatus,
    required this.paymentType,
    required this.remark,
    required this.currentDateTime,
    required this.createdAt,
    required this.updatedAt,
    required this.productTitle,
    required this.productImage,
    required this.suggestedAstroName,
    required this.categoryName,
  });

  factory ProductList.fromJson(Map<String, dynamic> json) => ProductList(
        id: json["id"],
        orderId: json["order_id"],
        userId: json["user_id"],
        productPrice: json["product_price"],
        astrologerId: json["astrologer_id"],
        suggestedAstroId: json["suggested_astro_id"],
        remedyId: json["remedy_id"],
        poojaId: json["pooja_id"],
        transactionId: json["transaction_id"],
        amount: json["amount"],
        remedyOrderId: json['remedy_order_id'],
        gstAmount: json["gst_amount"],
        walletDeducted: json["wallet_deducted"],
        totalAmount: json["total_amount"],
        isSuccess: json["is_success"],
        orderStatus: json['order_status'],
        paymentType: json["payment_type"],
        remark: json["remark"],
        currentDateTime: DateTime.parse(json["current_date_time"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        productTitle: json["product_title"],
        productImage: json["product_image"] ?? "",
        suggestedAstroName: json["suggested_astro_name"],
        categoryName: json["category_name"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "user_id": userId,
        "product_price": productPrice,
        "astrologer_id": astrologerId,
        "suggested_astro_id": suggestedAstroId,
        "product_id": remedyId,
        "pooja_id": poojaId,
        "transaction_id": transactionId,
        "amount": amount,
        "remedy_order_id": remedyOrderId,
        "gst_amount": gstAmount,
        "wallet_deducted": walletDeducted,
        "total_amount": totalAmount,
        "is_success": isSuccess,
        "payment_type": paymentType,
        "remark": remark,
        "current_date_time": currentDateTime.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "product_title": productTitle,
        "product_image": productImage,
        "suggested_astro_name": suggestedAstroName,
        "category_name": categoryName,
      };
}
