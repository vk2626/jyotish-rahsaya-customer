// To parse this JSON data, do
//
//     final customerRemedyModel = customerRemedyModelFromJson(jsonString);

import 'dart:convert';

CustomerRemedyModel customerRemedyModelFromJson(String str) =>
    CustomerRemedyModel.fromJson(json.decode(str));

String customerRemedyModelToJson(CustomerRemedyModel data) =>
    json.encode(data.toJson());

class CustomerRemedyModel {
  bool status;
  List<Customer> customer;

  CustomerRemedyModel({
    required this.status,
    required this.customer,
  });

  factory CustomerRemedyModel.fromJson(Map<String, dynamic> json) =>
      CustomerRemedyModel(
        status: json["status"],
        customer: List<Customer>.from(
            json["customer"].map((x) => Customer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "customer": List<dynamic>.from(customer.map((x) => x.toJson())),
      };
}

class Customer {
  int id;
  String infoId;
  String orderId;
  String userId;
  String astrologerId;
  String suggestedAstroId;
  String remedyId;
  String poojaId;
  String transactionId;
  String amount;
  String gstAmount;
  String walletDeducted;
  String totalAmount;
  String isSuccess;
  String paymentType;
  String remark;
  DateTime currentDateTime;
  DateTime createdAt;
  // DateTime updatedAt;
  String productTitle;
  String productImage;
  String type;
  String suggestedAstroName;
  String categoryName;

  Customer({
    required this.id,
    required this.infoId,
    required this.orderId,
    required this.userId,
    required this.astrologerId,
    required this.suggestedAstroId,
    required this.remedyId,
    required this.poojaId,
    required this.transactionId,
    required this.amount,
    required this.gstAmount,
    required this.walletDeducted,
    required this.totalAmount,
    required this.isSuccess,
    required this.paymentType,
    required this.remark,
    required this.currentDateTime,
    required this.createdAt,
    // required this.updatedAt,
    required this.productTitle,
    required this.productImage,
    required this.type,
    required this.suggestedAstroName,
    required this.categoryName,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        infoId: json['user_info_id'],
        orderId: json["order_id"],
        userId: json["user_id"],
        astrologerId: json["astrologer_id"],
        suggestedAstroId: json["suggested_astro_id"],
        remedyId: json["remedy_id"],
        poojaId: json["pooja_id"],
        transactionId: json["transaction_id"],
        amount: json["amount"],
        gstAmount: json["gst_amount"],
        walletDeducted: json["wallet_deducted"],
        totalAmount: json["total_amount"],
        isSuccess: json["is_success"],
        paymentType: json["payment_type"],
        remark: json["remark"],
        currentDateTime: DateTime.parse(json["current_date_time"]),
        createdAt: DateTime.parse(json["created_at"]),
        // updatedAt: DateTime.parse(json["updated_at"]),
        productTitle: json["product_title"],
        productImage: json["product_image"] ?? "",
        type: json['type'] ?? "",
        suggestedAstroName: json["suggested_astro_name"] ?? "",
        categoryName: json["category_name"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "user_id": userId,
        "astrologer_id": astrologerId,
        "suggested_astro_id": suggestedAstroId,
        "remedy_id": remedyId,
        "pooja_id": poojaId,
        "transaction_id": transactionId,
        "amount": amount,
        "gst_amount": gstAmount,
        "wallet_deducted": walletDeducted,
        "total_amount": totalAmount,
        "is_success": isSuccess,
        "payment_type": paymentType,
        "remark": remark,
        "current_date_time": currentDateTime.toIso8601String(),
        // "updated_at": updatedAt.toIso8601String(),
        "product_title": productTitle,
        "product_image": productImage,
        "suggested_astro_name": suggestedAstroName,
        "category_name": categoryName,
      };
}
