// To parse this JSON data, do
//
//     final similarConsultantsModel = similarConsultantsModelFromJson(jsonString);

import 'dart:convert';

SimilarConsultantsModel similarConsultantsModelFromJson(String str) =>
    SimilarConsultantsModel.fromJson(json.decode(str));

String similarConsultantsModelToJson(SimilarConsultantsModel data) =>
    json.encode(data.toJson());

class SimilarConsultantsModel {
  bool status;
  List<User> users;

  SimilarConsultantsModel({
    required this.status,
    required this.users,
  });

  factory SimilarConsultantsModel.fromJson(Map<String, dynamic> json) =>
      SimilarConsultantsModel(
        status: json["status"],
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}

class User {
  int id;
  String name;
  int label;
  String avatar;
  int isOnlineAstrologer;
  Astrologer astrologer;

  User({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.astrologer,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        astrologer: Astrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "astrologer": astrologer.toJson(),
      };
}

class Astrologer {
  int id;
  String userId;
  int isOnline;
  String chatPrice;
  String chatDiscountPrice;
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;
  String displayName;
  String labelText;

  Astrologer(
      {required this.id,
      required this.userId,
      required this.isOnline,
      required this.chatPrice,
      required this.chatDiscountPrice,
      required this.callPrice,
      required this.callDiscountPrice,
      required this.videoPrice,
      required this.videoDiscountPrice,
      required this.displayName,
      required this.labelText});

  factory Astrologer.fromJson(Map<String, dynamic> json) => Astrologer(
      id: json["id"],
      userId: json["user_id"],
      isOnline: json["is_online"],
      chatPrice: json["chat_price"],
      chatDiscountPrice: json["chat_discount_price"],
      callPrice: json["call_price"],
      callDiscountPrice: json["call_discount_price"],
      videoPrice: json["video_price"],
      videoDiscountPrice: json["video_discount_price"],
      displayName: json["display_name"],
      labelText: json['label_text']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "label_text": labelText
      };
}
