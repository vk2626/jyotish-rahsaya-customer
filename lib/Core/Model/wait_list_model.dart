// To parse this JSON data, do
//
//     final waitListModel = waitListModelFromJson(jsonString);

import 'dart:convert';

WaitListModel waitListModelFromJson(String str) =>
    WaitListModel.fromJson(json.decode(str));

String waitListModelToJson(WaitListModel data) => json.encode(data.toJson());

class WaitListModel {
  bool status;
  Users users;

  WaitListModel({
    required this.status,
    required this.users,
  });

  factory WaitListModel.fromJson(Map<String, dynamic> json) => WaitListModel(
        status: json["status"],
        users: Users.fromJson(json["users"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "users": users.toJson(),
      };
}

class Users {
  int id;
  String name;
  int label;
  String avatar;
  Astrologer astrologer;

  Users({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.astrologer,
  });

  factory Users.fromJson(Map<String, dynamic> json) => Users(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        astrologer: Astrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "astrologer": astrologer.toJson(),
      };
}

class Astrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  String chatPrice;
  String chatDiscountPrice;
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;
  String displayName;
  String waitTime;
  String durationTime;
  String startTime;
  String endTime;
  String type;
  int acceptByAstrologer;
  int acceptByCustomer;
  int chatRequestId;
  int status;
  String dob;
  String dobPlace;
  String dobTime;
  String infoId;
  String lat;
  String lon;
  String rashi;

  Astrologer(
      {required this.id,
      required this.userId,
      required this.primarySkillId,
      required this.allSkillId,
      required this.languageId,
      required this.experienceYear,
      required this.bio,
      required this.freeChat,
      required this.orderCount,
      required this.isOnline,
      required this.chatPrice,
      required this.chatDiscountPrice,
      required this.callPrice,
      required this.callDiscountPrice,
      required this.videoPrice,
      required this.videoDiscountPrice,
      required this.displayName,
      required this.waitTime,
      required this.durationTime,
      required this.startTime,
      required this.endTime,
      required this.type,
      required this.acceptByAstrologer,
      required this.acceptByCustomer,
      required this.chatRequestId,
      required this.status,
      required this.dob,
      required this.dobPlace,
      required this.dobTime,
      required this.infoId,
      required this.lat,
      required this.lon,
      required this.rashi});

  factory Astrologer.fromJson(Map<String, dynamic> json) => Astrologer(
      id: json["id"],
      userId: json["user_id"],
      primarySkillId: json["primary_skill_id"],
      allSkillId: json["all_skill_id"],
      languageId: json["language_id"],
      experienceYear: json["experience_year"],
      bio: json["bio"],
      freeChat: json["free_chat"],
      orderCount: json["order_count"],
      isOnline: json["is_online"],
      chatPrice: json["chat_price"],
      chatDiscountPrice: json["chat_discount_price"],
      callPrice: json["call_price"],
      callDiscountPrice: json["call_discount_price"],
      videoPrice: json["video_price"],
      videoDiscountPrice: json["video_discount_price"],
      displayName: json["display_name"],
      waitTime: json["wait_time"],
      durationTime: json["duration_time"],
      startTime: json["start_time"],
      endTime: json["end_time"],
      type: json["type"],
      acceptByAstrologer: json["accept_by_astrologer"],
      acceptByCustomer: json["accept_by_customer"],
      chatRequestId: json["chatRequestId"],
      status: json["status"],
      dob: json["dob"],
      dobPlace: json["dob_place"],
      dobTime: json["dob_time"],
      infoId: json["info_id"],
      lat: json["lat"],
      lon: json["lon"],
      rashi: json["rashi"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "wait_time": waitTime,
        "duration_time": durationTime,
        "start_time": startTime,
        "end_time": endTime,
        "type": type,
        "accept_by_astrologer": acceptByAstrologer,
        "accept_by_customer": acceptByCustomer,
        "chatRequestId": chatRequestId,
        "status": status,
        "dob": dob,
        "dob_place": dobPlace,
        "dob_time": dobTime,
        "info_id": infoId,
        "lat": lat,
        "lon": lon,
        "rashi": rashi
      };
}
