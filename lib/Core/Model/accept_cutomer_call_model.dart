// To parse this JSON data, do
//
//     final acceptCustomerStartCallModel = acceptCustomerStartCallModelFromJson(jsonString);

import 'dart:convert';

AcceptCustomerStartCallModel acceptCustomerStartCallModelFromJson(String str) =>
    AcceptCustomerStartCallModel.fromJson(json.decode(str));

String acceptCustomerStartCallModelToJson(AcceptCustomerStartCallModel data) =>
    json.encode(data.toJson());

class AcceptCustomerStartCallModel {
  bool? status;
  Communication? communication;

  AcceptCustomerStartCallModel({
    this.status,
    this.communication,
  });

  factory AcceptCustomerStartCallModel.fromJson(Map<String, dynamic> json) =>
      AcceptCustomerStartCallModel(
        status: json["status"],
        communication: Communication.fromJson(json["communication"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "communication": communication!.toJson(),
      };
}

class Communication {
  int id;
  String orderId;
  String userId;
  String astrologerId;
  int orderCount;
  String type;
  int acceptByAstrologer;
  int acceptByCustomer;
  DateTime startTime;
  DateTime endTime;
  String durationTime;
  String waitTime;
  String totalPrice;
  DateTime createdAt;
  DateTime updatedAt;

  Communication({
    required this.id,
    required this.orderId,
    required this.userId,
    required this.astrologerId,
    required this.orderCount,
    required this.type,
    required this.acceptByAstrologer,
    required this.acceptByCustomer,
    required this.startTime,
    required this.endTime,
    required this.durationTime,
    required this.waitTime,
    required this.totalPrice,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Communication.fromJson(Map<String, dynamic> json) => Communication(
        id: json["id"],
        orderId: json["order_id"],
        userId: json["user_id"],
        astrologerId: json["astrologer_id"],
        orderCount: json["order_count"],
        type: json["type"],
        acceptByAstrologer: json["accept_by_astrologer"],
        acceptByCustomer: json["accept_by_customer"],
        startTime: DateTime.parse(json["start_time"]),
        endTime: DateTime.parse(json["end_time"]),
        durationTime: json["duration_time"],
        waitTime: json["wait_time"],
        totalPrice: json["total_price"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "user_id": userId,
        "astrologer_id": astrologerId,
        "order_count": orderCount,
        "type": type,
        "accept_by_astrologer": acceptByAstrologer,
        "accept_by_customer": acceptByCustomer,
        "start_time": startTime.toIso8601String(),
        "end_time": endTime.toIso8601String(),
        "duration_time": durationTime,
        "wait_time": waitTime,
        "total_price": totalPrice,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
