import 'dart:convert';

NewlyLauchedModelNew newlyLauchedModelNewFromJson(String str) =>
    NewlyLauchedModelNew.fromJson(json.decode(str));

String newlyLauchedModelNewToJson(NewlyLauchedModelNew data) =>
    json.encode(data.toJson());

class NewlyLauchedModelNew {
  bool status;
  List<Datum> data;

  NewlyLauchedModelNew({
    required this.status,
    required this.data,
  });

  factory NewlyLauchedModelNew.fromJson(Map<String, dynamic> json) =>
      NewlyLauchedModelNew(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  String title;
  String image;
  String time;
  String activity;
  int productLabel;
  String productLableText;

  Datum({
    required this.id,
    required this.title,
    required this.image,
    required this.time,
    required this.activity,
    required this.productLabel,
    required this.productLableText,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        time: json["time"],
        activity: json["activity"],
        productLabel: json["product_label"],
        productLableText: json["product_lable_text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "time": time,
        "activity": activity,
        "product_label": productLabel,
        "product_lable_text": productLableText,
      };
}
