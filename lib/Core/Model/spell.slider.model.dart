// To parse this JSON data, do
//
//     final spellSliderModel = spellSliderModelFromJson(jsonString);

import 'dart:convert';

SpellSliderModel spellSliderModelFromJson(String str) =>
    SpellSliderModel.fromJson(json.decode(str));

String spellSliderModelToJson(SpellSliderModel data) =>
    json.encode(data.toJson());

class SpellSliderModel {
  bool? status;
  List<Datum>? data;

  SpellSliderModel({
    this.status,
    this.data,
  });

  factory SpellSliderModel.fromJson(Map<String, dynamic> json) =>
      SpellSliderModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  dynamic astrologerCategory;
  String productCategory;
  String title;
  String image;
  int status;
  DateTime createdAt;
  DateTime updatedAt;

  Datum({
    required this.id,
    required this.astrologerCategory,
    required this.productCategory,
    required this.title,
    required this.image,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        astrologerCategory: json["astrologer_category"],
        productCategory: json["product_category"],
        title: json["title"],
        image: json["image"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "astrologer_category": astrologerCategory,
        "product_category": productCategory,
        "title": title,
        "image": image,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
