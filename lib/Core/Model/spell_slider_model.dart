// To parse this JSON data, do
//
//     final spellSliderModelNew = spellSliderModelNewFromJson(jsonString);

import 'dart:convert';

SpellSliderModelNew spellSliderModelNewFromJson(String str) => SpellSliderModelNew.fromJson(json.decode(str));

String spellSliderModelNewToJson(SpellSliderModelNew data) => json.encode(data.toJson());

class SpellSliderModelNew {
    bool status;
    List<Datum> data;

    SpellSliderModelNew({
        required this.status,
        required this.data,
    });

    factory SpellSliderModelNew.fromJson(Map<String, dynamic> json) => SpellSliderModelNew(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    int id;
    dynamic astrologerCategory;
    String productCategory;
    String title;
    String image;
    int status;
    DateTime createdAt;
    DateTime? updatedAt;

    Datum({
        required this.id,
        required this.astrologerCategory,
        required this.productCategory,
        required this.title,
        required this.image,
        required this.status,
        required this.createdAt,
        required this.updatedAt,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        astrologerCategory: json["astrologer_category"],
        productCategory: json["product_category"],
        title: json["title"],
        image: json["image"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "astrologer_category": astrologerCategory,
        "product_category": productCategory,
        "title": title,
        "image": image,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
    };
}
