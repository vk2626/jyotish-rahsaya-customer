// import 'dart:convert';
// /// status : true
// /// WaitChatList : {"id":37,"user_id":"321","astrologer_id":"211","order_count":0,"type":"call","accept_by_astrologer":0,"accept_by_customer":0,"start_time":"","end_time":"","duration_time":"5","wait_time":0,"total_price":75,"created_at":"2024-03-19T13:16:42.000000Z","updated_at":"2024-03-19T13:16:42.000000Z"}

// WaitListModel waitListModelFromJson(String str) => WaitListModel.fromJson(json.decode(str));
// String waitListModelToJson(WaitListModel data) => json.encode(data.toJson());
// class WaitListModel {
//   WaitListModel({
//       bool? status, 
//       WaitChatList? waitChatList,}){
//     _status = status;
//     _waitChatList = waitChatList;
// }

//   WaitListModel.fromJson(dynamic json) {
//     _status = json['status'];
//     _waitChatList = json['WaitChatList'];
//   }
//   bool? _status;
//   WaitChatList? _waitChatList;
// WaitListModel copyWith({  bool? status,
//   WaitChatList? waitChatList,
// }) => WaitListModel(  status: status ?? _status,
//   waitChatList: waitChatList ?? _waitChatList,
// );
//   bool? get status => _status;
//   WaitChatList? get waitChatList => _waitChatList;

//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['status'] = _status;
//     map['WaitChatList'] = _waitChatList;
//     return map;
//   }

// }

// /// id : 37
// /// user_id : "321"
// /// astrologer_id : "211"
// /// order_count : 0
// /// type : "call"
// /// accept_by_astrologer : 0
// /// accept_by_customer : 0
// /// start_time : ""
// /// end_time : ""
// /// duration_time : "5"
// /// wait_time : 0
// /// total_price : 75
// /// created_at : "2024-03-19T13:16:42.000000Z"
// /// updated_at : "2024-03-19T13:16:42.000000Z"

// WaitChatList waitChatListFromJson(String str) => WaitChatList.fromJson(json.decode(str));
// String waitChatListToJson(WaitChatList data) => json.encode(data.toJson());
// class WaitChatList {
//   WaitChatList({
//       num? id, 
//       String? userId, 
//       String? astrologerId, 
//       num? orderCount, 
//       String? type, 
//       num? acceptByAstrologer, 
//       num? acceptByCustomer, 
//       String? startTime, 
//       String? endTime, 
//       String? durationTime, 
//       num? waitTime, 
//       num? totalPrice, 
//       String? createdAt, 
//       String? updatedAt,}){
//     _id = id;
//     _userId = userId;
//     _astrologerId = astrologerId;
//     _orderCount = orderCount;
//     _type = type;
//     _acceptByAstrologer = acceptByAstrologer;
//     _acceptByCustomer = acceptByCustomer;
//     _startTime = startTime;
//     _endTime = endTime;
//     _durationTime = durationTime;
//     _waitTime = waitTime;
//     _totalPrice = totalPrice;
//     _createdAt = createdAt;
//     _updatedAt = updatedAt;
// }

//   WaitChatList.fromJson(dynamic json) {
//     _id = json['id'];
//     _userId = json['user_id'];
//     _astrologerId = json['astrologer_id'];
//     _orderCount = json['order_count'];
//     _type = json['type'];
//     _acceptByAstrologer = json['accept_by_astrologer'];
//     _acceptByCustomer = json['accept_by_customer'];
//     _startTime = json['start_time'];
//     _endTime = json['end_time'];
//     _durationTime = json['duration_time'];
//     _waitTime = json['wait_time'];
//     _totalPrice = json['total_price'];
//     _createdAt = json['created_at'];
//     _updatedAt = json['updated_at'];
//   }
//   num? _id;
//   String? _userId;
//   String? _astrologerId;
//   num? _orderCount;
//   String? _type;
//   num? _acceptByAstrologer;
//   num? _acceptByCustomer;
//   String? _startTime;
//   String? _endTime;
//   String? _durationTime;
//   num? _waitTime;
//   num? _totalPrice;
//   String? _createdAt;
//   String? _updatedAt;
// WaitChatList copyWith({  num? id,
//   String? userId,
//   String? astrologerId,
//   num? orderCount,
//   String? type,
//   num? acceptByAstrologer,
//   num? acceptByCustomer,
//   String? startTime,
//   String? endTime,
//   String? durationTime,
//   num? waitTime,
//   num? totalPrice,
//   String? createdAt,
//   String? updatedAt,
// }) => WaitChatList(  id: id ?? _id,
//   userId: userId ?? _userId,
//   astrologerId: astrologerId ?? _astrologerId,
//   orderCount: orderCount ?? _orderCount,
//   type: type ?? _type,
//   acceptByAstrologer: acceptByAstrologer ?? _acceptByAstrologer,
//   acceptByCustomer: acceptByCustomer ?? _acceptByCustomer,
//   startTime: startTime ?? _startTime,
//   endTime: endTime ?? _endTime,
//   durationTime: durationTime ?? _durationTime,
//   waitTime: waitTime ?? _waitTime,
//   totalPrice: totalPrice ?? _totalPrice,
//   createdAt: createdAt ?? _createdAt,
//   updatedAt: updatedAt ?? _updatedAt,
// );
//   num? get id => _id;
//   String? get userId => _userId;
//   String? get astrologerId => _astrologerId;
//   num? get orderCount => _orderCount;
//   String? get type => _type;
//   num? get acceptByAstrologer => _acceptByAstrologer;
//   num? get acceptByCustomer => _acceptByCustomer;
//   String? get startTime => _startTime;
//   String? get endTime => _endTime;
//   String? get durationTime => _durationTime;
//   num? get waitTime => _waitTime;
//   num? get totalPrice => _totalPrice;
//   String? get createdAt => _createdAt;
//   String? get updatedAt => _updatedAt;

//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['id'] = _id;
//     map['user_id'] = _userId;
//     map['astrologer_id'] = _astrologerId;
//     map['order_count'] = _orderCount;
//     map['type'] = _type;
//     map['accept_by_astrologer'] = _acceptByAstrologer;
//     map['accept_by_customer'] = _acceptByCustomer;
//     map['start_time'] = _startTime;
//     map['end_time'] = _endTime;
//     map['duration_time'] = _durationTime;
//     map['wait_time'] = _waitTime;
//     map['total_price'] = _totalPrice;
//     map['created_at'] = _createdAt;
//     map['updated_at'] = _updatedAt;
//     return map;
//   }

// }