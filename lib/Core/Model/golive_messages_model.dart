// To parse this JSON data, do
//
//     final goLiveMessagesModel = goLiveMessagesModelFromJson(jsonString);

import 'dart:convert';

GoLiveMessagesModel goLiveMessagesModelFromJson(String str) =>
    GoLiveMessagesModel.fromJson(json.decode(str));

String goLiveMessagesModelToJson(GoLiveMessagesModel data) =>
    json.encode(data.toJson());

class GoLiveMessagesModel {
  bool status;
  List<Comment> comments;

  GoLiveMessagesModel({
    required this.status,
    required this.comments,
  });

  factory GoLiveMessagesModel.fromJson(Map<String, dynamic> json) =>
      GoLiveMessagesModel(
        status: json["status"],
        comments: List<Comment>.from(
            json["comments"].map((x) => Comment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "comments": List<dynamic>.from(comments.map((x) => x.toJson())),
      };
}

class Comment {
  int commentId;
  String liveId;
  String comment;
  String userId;
  String userName;
  String userImage;
  DateTime createdAt;

  Comment({
    required this.commentId,
    required this.liveId,
    required this.comment,
    required this.userId,
    required this.userName,
    required this.userImage,
    required this.createdAt,
  });

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        commentId: json["comment_id"],
        liveId: json["live_id"],
        comment: json["comment"] ?? "",
        userId: json["user_id"],
        userName: json["user_name"] ?? "",
        userImage: json["user_image"] ?? "",
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "comment_id": commentId,
        "live_id": liveId,
        "comment": comment,
        "user_id": userId,
        "user_name": userName,
        "user_image": userImage,
        "created_at": createdAt.toIso8601String(),
      };
}
