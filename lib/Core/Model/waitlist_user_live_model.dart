// To parse this JSON data, do
//
//     final waitListUserLiveModel = waitListUserLiveModelFromJson(jsonString);

import 'dart:convert';

WaitListUserLiveModel waitListUserLiveModelFromJson(String str) =>
    WaitListUserLiveModel.fromJson(json.decode(str));

String waitListUserLiveModelToJson(WaitListUserLiveModel data) =>
    json.encode(data.toJson());

class WaitListUserLiveModel {
  bool status;
  List<WaitListUserLive> users;

  WaitListUserLiveModel({
    required this.status,
    required this.users,
  });

  factory WaitListUserLiveModel.fromJson(Map<String, dynamic> json) =>
      WaitListUserLiveModel(
        status: json["status"],
        users: List<WaitListUserLive>.from(
            json["users"].map((x) => WaitListUserLive.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}

class WaitListUserLive {
  int id;
  String name;
  String avatar;
  int userType;
  Customer customer;

  WaitListUserLive({
    required this.id,
    required this.name,
    required this.avatar,
    required this.userType,
    required this.customer,
  });

  factory WaitListUserLive.fromJson(Map<String, dynamic> json) =>
      WaitListUserLive(
        id: json["id"],
        name: json["name"],
        avatar: json["avatar"],
        userType: json["user_type"],
        customer: Customer.fromJson(json["customer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "avatar": avatar,
        "user_type": userType,
        "customer": customer.toJson(),
      };
}

class Customer {
  int id;
  String userId;
  String gender;
  DateTime dob;
  String dobPlace;
  String dobTime;
  String currentAddress;
  String address;
  String pincode;
  String duration;
  DateTime startTime;
  String type;
  int isOnline;
  DateTime createdAt;
  int tokenNo;
  int status;
  String infoId;

  Customer({
    required this.id,
    required this.userId,
    required this.gender,
    required this.dob,
    required this.dobPlace,
    required this.dobTime,
    required this.currentAddress,
    required this.address,
    required this.pincode,
    required this.startTime,
    required this.duration,
    required this.type,
    required this.isOnline,
    required this.createdAt,
    required this.tokenNo,
    required this.status,
    required this.infoId,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        userId: json["user_id"],
        gender: json["gender"],
        dob: DateTime.parse(json["dob"]),
        dobPlace: json["dob_place"],
        dobTime: json["dob_time"],
        currentAddress: json["current_address"],
        address: json["address"],
        pincode: json["pincode"],
        startTime: json['start_time'] != null
            ? DateTime.parse(json['start_time'])
            : DateTime.now(),
        duration: json["duration_time"],
        type: json["type"],
        isOnline: json["is_online"],
        createdAt: DateTime.parse(json["created_at"]),
        tokenNo: json["token_no"],
        status: json["status"],
        infoId: json["info_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "gender": gender,
        "dob":
            "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
        "dob_place": dobPlace,
        "dob_time": dobTime,
        "current_address": currentAddress,
        "address": address,
        "pincode": pincode,
        "duration_time": duration,
        "type": type,
        "is_online": isOnline,
        "created_at": createdAt.toIso8601String(),
        "token_no": tokenNo,
        "status": status,
        "info_id": infoId,
      };
}
