// To parse this JSON data, do
//
//     final blogModel = blogModelFromJson(jsonString);

import 'dart:convert';

BlogModel blogModelFromJson(String str) => BlogModel.fromJson(json.decode(str));

String blogModelToJson(BlogModel data) => json.encode(data.toJson());

class BlogModel {
  bool status;
  List<Datum> data;

  BlogModel({
    required this.status,
    required this.data,
  });

  factory BlogModel.fromJson(Map<String, dynamic> json) => BlogModel(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int? id;
  String? userId;
  String? title;
  String? lType;
  String? view;
  String? slug;
  String? image;
  String? bloggerName;
  String? longDescription;
  int? status;
  DateTime? updatedAt;
  dynamic deletedAt;
  DateTime? createTime;

  Datum({
    this.id,
    this.userId,
    this.title,
    this.lType,
    this.slug,
    this.view,
    this.image,
    this.bloggerName,
    this.longDescription,
    this.status,
    this.updatedAt,
    this.deletedAt,
    this.createTime,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        userId: json["user_id"],
        title: json["title"],
        lType: json["l_type"],
        slug: json["slug"],
        view: json["view"],
        image: json["image"],
        bloggerName: json["blogger_name"],
        longDescription: json["long_description"],
        status: json["status"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
        createTime: json["create_time"] == null
            ? null
            : DateTime.parse(json["create_time"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "title": title,
        "l_type": lType,
        "slug": slug,
        "image": image,
        "view": view,
        "blogger_name": bloggerName,
        "long_description": longDescription,
        "status": status,
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
        "create_time":
            "${createTime!.year.toString().padLeft(4, '0')}-${createTime!.month.toString().padLeft(2, '0')}-${createTime!.day.toString().padLeft(2, '0')}",
      };
}
