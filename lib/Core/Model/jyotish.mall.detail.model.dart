// To parse this JSON data, do
//
//     final jyotishMallDetailModel = jyotishMallDetailModelFromJson(jsonString);

import 'dart:convert';

JyotishMallDetailModel jyotishMallDetailModelFromJson(String str) =>
    JyotishMallDetailModel.fromJson(json.decode(str));

String jyotishMallDetailModelToJson(JyotishMallDetailModel data) =>
    json.encode(data.toJson());

class JyotishMallDetailModel {
  bool? status;
  List<Datum>? data;

  JyotishMallDetailModel({
    this.status,
    this.data,
  });

  factory JyotishMallDetailModel.fromJson(Map<String, dynamic> json) =>
      JyotishMallDetailModel(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  int? id;
  String? title;
  String? category;
  String? image;
  String? price;
  String? shortDescription;
  String? youtubeUrl;
  List<Heading>? heading;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  Datum({
    this.id,
    this.title,
    this.category,
    this.image,
    this.price,
    this.shortDescription,
    this.youtubeUrl,
    this.heading,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        title: json["title"],
        category: json["category"],
        image: json["image"],
        price: json["price"],
        shortDescription: json["short_description"],
        youtubeUrl: json["youtube_url"],
        heading: json["heading"] == null
            ? []
            : List<Heading>.from(
                json["heading"]!.map((x) => Heading.fromJson(x))),
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "category": category,
        "image": image,
        "price": price,
        "short_description": shortDescription,
        "youtube_url": youtubeUrl,
        "heading": heading == null
            ? []
            : List<dynamic>.from(heading!.map((x) => x.toJson())),
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

class Heading {
  String? heading;
  String? description;
  String? descrption;

  Heading({
    this.heading,
    this.description,
    this.descrption,
  });

  factory Heading.fromJson(Map<String, dynamic> json) => Heading(
        heading: json["heading"],
        description: json["description"],
        descrption: json["descrption"],
      );

  Map<String, dynamic> toJson() => {
        "heading": heading,
        "description": description,
        "descrption": descrption,
      };
}
