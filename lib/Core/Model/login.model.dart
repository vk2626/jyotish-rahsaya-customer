// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  bool? status;
  Data? data;

  LoginModel({
    this.status,
    this.data,
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        status: json["status"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data?.toJson(),
      };
}

class Data {
  int? id;
  dynamic name;
  String? image;
  int? mobile;
  int? userType;
  int? status;
  String? isComplete;
  String? deviceToken;
  Customer? customer;

  Data({
    this.id,
    this.name,
    this.image,
    this.mobile,
    this.userType,
    this.status,
    this.isComplete,
    this.deviceToken,
    this.customer,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        image: json["image"],
        mobile: json["mobile"],
        userType: json["user_type"],
        status: json["status"],
        isComplete: json["is_complete"],
        deviceToken: json["device_token"],
        customer: json["customer"] == null
            ? null
            : Customer.fromJson(json["customer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "mobile": mobile,
        "user_type": userType,
        "status": status,
        "is_complete": isComplete,
        "device_token": deviceToken,
        "customer": customer?.toJson(),
      };
}

class Customer {
  String? id;
  String? userId;
  String? image;
  String? gender;
  String? dob;
  String? dobPlace;
  String? currentAddress;
  String? dobTime;
  String? address;
  String? pincode;

  Customer({
    this.id,
    this.userId,
    this.image,
    this.gender,
    this.dob,
    this.dobPlace,
    this.currentAddress,
    this.dobTime,
    this.address,
    this.pincode,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        userId: json["user_id"],
        image: json["image"],
        gender: json["gender"],
        dob: json["dob"],
        dobPlace: json["dob_place"],
        currentAddress: json["current_address"],
        dobTime: json["dob_time"],
        address: json["address"],
        pincode: json["pincode"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "image": image,
        "gender": gender,
        "dob": dob,
        "dob_place": dobPlace,
        "current_address": currentAddress,
        "dob_time": dobTime,
        "address": address,
        "pincode": pincode,
      };
}
