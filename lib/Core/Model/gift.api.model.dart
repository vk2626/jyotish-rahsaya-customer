// To parse this JSON data, do
//
//     final giftApiModel = giftApiModelFromJson(jsonString);

import 'dart:convert';

GiftApiModel giftApiModelFromJson(String str) =>
    GiftApiModel.fromJson(json.decode(str));

String giftApiModelToJson(GiftApiModel data) => json.encode(data.toJson());

class GiftApiModel {
  bool? status;
  dynamic? balance;
  List<Datum>? data;

  GiftApiModel({
    this.status,
    this.balance,
    this.data,
  });

  factory GiftApiModel.fromJson(Map<String, dynamic> json) => GiftApiModel(
        status: json["status"],
      balance : json["balance"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "balance": balance,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  dynamic? id;
  String? title;
  dynamic? price;
  String? image;
  dynamic? status;
  DateTime? createdAt;
  DateTime? updatedAt;

  Datum({
    this.id,
    this.title,
    this.price,
    this.image,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        title: json["title"],
        price: json["price"],
        image: json["image"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "price": price,
        "image": image,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}
