import 'dart:convert';
/// status : true
/// data : [{"id":1,"skill":"Vedic","p_skill":"all","status":1,"created_at":"2023-12-04T20:26:00.000000Z","updated_at":"2023-12-04T21:54:36.000000Z"}]

SkillsModel skillsModelFromJson(String str) => SkillsModel.fromJson(json.decode(str));
String skillsModelToJson(SkillsModel data) => json.encode(data.toJson());
class SkillsModel {
  SkillsModel({
      bool? status, 
      List<Data>? data,}){
    _status = status;
    _data = data;
}

  SkillsModel.fromJson(dynamic json) {
    _status = json['status'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  bool? _status;
  List<Data>? _data;
SkillsModel copyWith({  bool? status,
  List<Data>? data,
}) => SkillsModel(  status: status ?? _status,
  data: data ?? _data,
);
  bool? get status => _status;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1
/// skill : "Vedic"
/// p_skill : "all"
/// status : 1
/// created_at : "2023-12-04T20:26:00.000000Z"
/// updated_at : "2023-12-04T21:54:36.000000Z"

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      num? id, 
      String? skill, 
      String? pSkill, 
      num? status, 
      String? createdAt, 
      String? updatedAt,}){
    _id = id;
    _skill = skill;
    _pSkill = pSkill;
    _status = status;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _skill = json['skill'];
    _pSkill = json['p_skill'];
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }
  num? _id;
  String? _skill;
  String? _pSkill;
  num? _status;
  String? _createdAt;
  String? _updatedAt;
Data copyWith({  num? id,
  String? skill,
  String? pSkill,
  num? status,
  String? createdAt,
  String? updatedAt,
}) => Data(  id: id ?? _id,
  skill: skill ?? _skill,
  pSkill: pSkill ?? _pSkill,
  status: status ?? _status,
  createdAt: createdAt ?? _createdAt,
  updatedAt: updatedAt ?? _updatedAt,
);
  num? get id => _id;
  String? get skill => _skill;
  String? get pSkill => _pSkill;
  num? get status => _status;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['skill'] = _skill;
    map['p_skill'] = _pSkill;
    map['status'] = _status;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }

}