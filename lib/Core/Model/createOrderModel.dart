import 'dart:convert';

CreateOrderModel createOrderModelFromJson(String str) =>
    CreateOrderModel.fromJson(json.decode(str));

String createOrderModelToJson(CreateOrderModel data) =>
    json.encode(data.toJson());

class CreateOrderModel {
  bool status;
  Payment payment;

  CreateOrderModel({
    required this.status,
    required this.payment,
  });

  factory CreateOrderModel.fromJson(Map<String, dynamic> json) =>
      CreateOrderModel(
        status: json["status"],
        payment: Payment.fromJson(json["payment"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "payment": payment.toJson(),
      };
}

class Payment {
  String userId;
  String astrologerId;
  String astroRemedyId;
  String? orderPoojaId;
  String? orderId;

  String razorpayId;
  String razorpaySecretKey;
  int isSuccess;

  Payment({
    required this.userId,
    required this.astrologerId,
    required this.orderPoojaId,
    required this.astroRemedyId,
    required this.orderId,
    required this.razorpayId,
    required this.razorpaySecretKey,
    required this.isSuccess,
  });

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        userId: json["user_id"],
        astrologerId: json["astrologer_id"].toString() ?? "",
        astroRemedyId: json['astro_remedy_id'].toString() ?? "",
        orderPoojaId: json['order_pooja_id'].toString() ?? "",
        orderId: json["order_id"],
        isSuccess: json["is_success"],
        razorpayId: json['RAZORPAY_KEY_ID'],
        razorpaySecretKey: json['RAZORPAY_KEY_SECRET'],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "astrologer_id": astrologerId,
        "order_id": orderId,
        "is_success": isSuccess,
      };
}
