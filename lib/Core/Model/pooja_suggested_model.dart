// To parse this JSON data, do
//
//     final poojaSuggestedModel = poojaSuggestedModelFromJson(jsonString);

import 'dart:convert';

PoojaSuggestedModel poojaSuggestedModelFromJson(String str) => PoojaSuggestedModel.fromJson(json.decode(str));

String poojaSuggestedModelToJson(PoojaSuggestedModel data) => json.encode(data.toJson());

class PoojaSuggestedModel {
    bool status;
    List<Customer> customer;

    PoojaSuggestedModel({
        required this.status,
        required this.customer,
    });

    factory PoojaSuggestedModel.fromJson(Map<String, dynamic> json) => PoojaSuggestedModel(
        status: json["status"],
        customer: List<Customer>.from(json["customer"].map((x) => Customer.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "customer": List<dynamic>.from(customer.map((x) => x.toJson())),
    };
}

class Customer {
    int id;
    String orderId;
    String userId;
    String userInfoId;
    String astrologerId;
    String suggestedAstroId;
    String productId;
    String poojaId;
    String transactionId;
    String amount;
    String gstAmount;
    String walletDeducted;
    String totalAmount;
    String isSuccess;
    String paymentType;
    String remark;
    String type;
    DateTime currentDateTime;
    dynamic createdAt;
    dynamic updatedAt;
    String productTitle;
    String? productImage;
    String suggestedAstroName;
    String categoryName;

    Customer({
        required this.id,
        required this.orderId,
        required this.userId,
        required this.userInfoId,
        required this.astrologerId,
        required this.suggestedAstroId,
        required this.productId,
        required this.poojaId,
        required this.transactionId,
        required this.amount,
        required this.gstAmount,
        required this.walletDeducted,
        required this.totalAmount,
        required this.isSuccess,
        required this.paymentType,
        required this.remark,
        required this.type,
        required this.currentDateTime,
        required this.createdAt,
        required this.updatedAt,
        required this.productTitle,
        required this.productImage,
        required this.suggestedAstroName,
        required this.categoryName,
    });

    factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        orderId: json["order_id"],
        userId: json["user_id"],
        userInfoId: json['user_info_id']??"",
        astrologerId: json["astrologer_id"],
        suggestedAstroId: json["suggested_astro_id"],
        productId: json["product_id"],
        poojaId: json["pooja_id"],
        transactionId: json["transaction_id"],
        amount: json["amount"],
        gstAmount: json["gst_amount"],
        walletDeducted: json["wallet_deducted"],
        totalAmount: json["total_amount"],
        isSuccess: json["is_success"],
        paymentType: json["payment_type"],
        remark: json["remark"],
        type: json["type"],
        currentDateTime: DateTime.parse(json["current_date_time"]),
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        productTitle: json["product_title"],
        productImage: json["product_image"],
        suggestedAstroName: json["suggested_astro_name"],
        categoryName: json["category_name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "user_id": userId,
        "astrologer_id": astrologerId,
        "suggested_astro_id": suggestedAstroId,
        "product_id": productId,
        "pooja_id": poojaId,
        "transaction_id": transactionId,
        "amount": amount,
        "gst_amount": gstAmount,
        "wallet_deducted": walletDeducted,
        "total_amount": totalAmount,
        "is_success": isSuccess,
        "payment_type": paymentType,
        "remark": remark,
        "type": type,
        "current_date_time": currentDateTime.toIso8601String(),
        "created_at": createdAt,
        "updated_at": updatedAt,
        "product_title": productTitle,
        "product_image": productImage,
        "suggested_astro_name": suggestedAstroName,
        "category_name": categoryName,
    };
}
