// To parse this JSON data, do
//
//     final mallProductSliderModel = mallProductSliderModelFromJson(jsonString);

import 'dart:convert';

MallProductSliderModel mallProductSliderModelFromJson(String str) => MallProductSliderModel.fromJson(json.decode(str));

String mallProductSliderModelToJson(MallProductSliderModel data) => json.encode(data.toJson());

class MallProductSliderModel {
  bool? status;
  List<Datum>? data;

  MallProductSliderModel({
    this.status,
    this.data,
  });

  factory MallProductSliderModel.fromJson(Map<String, dynamic> json) => MallProductSliderModel(
    status: json["status"],
    data: json["data"] == null ? [] : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class Datum {
  int? id;
  dynamic astrologerCategory;
  String? productCategory;
  String? title;
  String? image;
  int? status;
  DateTime? createdAt;
  dynamic updatedAt;

  Datum({
    this.id,
    this.astrologerCategory,
    this.productCategory,
    this.title,
    this.image,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    astrologerCategory: json["astrologer_category"],
    productCategory: json["product_category"],
    title: json["title"],
    image: json["image"],
    status: json["status"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "astrologer_category": astrologerCategory,
    "product_category": productCategory,
    "title": title,
    "image": image,
    "status": status,
    "created_at": createdAt?.toIso8601String(),
    "updated_at": updatedAt,
  };
}
