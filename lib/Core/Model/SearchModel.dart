// To parse this JSON data, do
//
//     final searchModel = searchModelFromJson(jsonString);

import 'dart:convert';

SearchModel searchModelFromJson(String str) =>
    SearchModel.fromJson(json.decode(str));

String searchModelToJson(SearchModel data) => json.encode(data.toJson());

class SearchModel {
  bool? status;
  Filter? filter;

  SearchModel({
    this.status,
    this.filter,
  });

  factory SearchModel.fromJson(Map<String, dynamic> json) => SearchModel(
        status: json["status"],
        filter: Filter.fromJson(json["filter"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "filter": filter!.toJson(),
      };
}

class Filter {
  List<AstrologerElement> astrologers;
  List<Product> products;

  Filter({
    required this.astrologers,
    required this.products,
  });

  factory Filter.fromJson(Map<String, dynamic> json) => Filter(
        astrologers: List<AstrologerElement>.from(
            json["astrologers"].map((x) => AstrologerElement.fromJson(x))),
        products: List<Product>.from(
            json["products"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "astrologers": List<dynamic>.from(astrologers.map((x) => x.toJson())),
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
      };
}

class AstrologerElement {
  int id;
  String name;
  int label;
  String avatar;
  int isOnlineAstrologer;
  AstrologerAstrologer astrologer;

  AstrologerElement({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.astrologer,
  });

  factory AstrologerElement.fromJson(Map<String, dynamic> json) =>
      AstrologerElement(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        astrologer: AstrologerAstrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "astrologer": astrologer.toJson(),
      };
}

class AstrologerAstrologer {
  int id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  int freeChat;
  String orderCount;
  int isOnline;
  String chatPrice;
  String chatDiscountPrice;
  String callPrice;
  String callDiscountPrice;
  String videoPrice;
  String videoDiscountPrice;
  String? displayName;
  String totalDurationChat;
  String totalDurationCall;
  double rating;
  String primarySkillName;
  String labelText;
  String allSkillName;
  String languageName;
  List<Gallery> gallery;

  AstrologerAstrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.chatPrice,
    required this.chatDiscountPrice,
    required this.callPrice,
    required this.callDiscountPrice,
    required this.videoPrice,
    required this.videoDiscountPrice,
    required this.displayName,
    required this.labelText,
    required this.totalDurationChat,
    required this.totalDurationCall,
    required this.rating,
    required this.primarySkillName,
    required this.allSkillName,
    required this.languageName,
    required this.gallery,
  });

  factory AstrologerAstrologer.fromJson(Map<String, dynamic> json) =>
      AstrologerAstrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        bio: json["bio"],
        freeChat: json["free_chat"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        chatPrice: json["chat_price"],
        chatDiscountPrice: json["chat_discount_price"],
        labelText: json['label_text'],
        callPrice: json["call_price"],
        callDiscountPrice: json["call_discount_price"],
        videoPrice: json["video_price"],
        videoDiscountPrice: json["video_discount_price"],
        displayName: json["display_name"],
        totalDurationChat: json["total_duration_chat"],
        totalDurationCall: json["total_duration_call"],
        rating: json["rating"]?.toDouble(),
        primarySkillName: json["primary_skill_name"],
        allSkillName: json["all_skill_name"],
        languageName: json["language_name"],
        gallery:
            List<Gallery>.from(json["gallery"].map((x) => Gallery.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "chat_price": chatPrice,
        "label_text":labelText,
        "chat_discount_price": chatDiscountPrice,
        "call_price": callPrice,
        "call_discount_price": callDiscountPrice,
        "video_price": videoPrice,
        "video_discount_price": videoDiscountPrice,
        "display_name": displayName,
        "total_duration_chat": totalDurationChat,
        "total_duration_call": totalDurationCall,
        "rating": rating,
        "primary_skill_name": primarySkillName,
        "all_skill_name": allSkillName,
        "language_name": languageName,
        "gallery": List<dynamic>.from(gallery.map((x) => x.toJson())),
      };
}

class Gallery {
  String astrologerId;
  String image;

  Gallery({
    required this.astrologerId,
    required this.image,
  });

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
        astrologerId: json["astrologer_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "astrologer_id": astrologerId,
        "image": image,
      };
}

class Product {
  int id;
  String astrologerId;
  String title;
  String category;
  String image;
  String productLabel;
  String price;
  String? shortDescription;
  String? youtubeUrl;
  List<Heading>? heading;
  int status;
  DateTime createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;
  String activity;

  Product({
    required this.id,
    required this.astrologerId,
    required this.title,
    required this.category,
    required this.image,
    required this.productLabel,
    required this.price,
    required this.shortDescription,
    required this.youtubeUrl,
    required this.heading,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
    required this.activity,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        astrologerId: json["astrologer_id"],
        title: json["title"],
        category: json["category"],
        image: json["image"],
        productLabel: json["product_label"],
        price: json["price"],
        shortDescription: json["short_description"],
        youtubeUrl: json["youtube_url"],
        heading: json["heading"] == null
            ? []
            : List<Heading>.from(
                json["heading"]!.map((x) => Heading.fromJson(x))),
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
        activity: json["activity"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "astrologer_id": astrologerId,
        "title": title,
        "category": category,
        "image": image,
        "product_label": productLabel,
        "price": price,
        "short_description": shortDescription,
        "youtube_url": youtubeUrl,
        "heading": heading == null
            ? []
            : List<dynamic>.from(heading!.map((x) => x.toJson())),
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
        "activity": activity,
      };
}

class Heading {
  String heading;
  String description;

  Heading({
    required this.heading,
    required this.description,
  });

  factory Heading.fromJson(Map<String, dynamic> json) => Heading(
        heading: json["heading"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "heading": heading,
        "description": description,
      };
}
