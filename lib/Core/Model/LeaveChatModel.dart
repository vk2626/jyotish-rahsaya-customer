import 'dart:convert';
/// status : false

LeaveChatModel leaveChatModelFromJson(String str) => LeaveChatModel.fromJson(json.decode(str));
String leaveChatModelToJson(LeaveChatModel data) => json.encode(data.toJson());
class LeaveChatModel {
  LeaveChatModel({
      bool? status,}){
    _status = status;
}

  LeaveChatModel.fromJson(dynamic json) {
    _status = json['status'];
  }
  bool? _status;
LeaveChatModel copyWith({  bool? status,
}) => LeaveChatModel(  status: status ?? _status,
);
  bool? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    return map;
  }

}