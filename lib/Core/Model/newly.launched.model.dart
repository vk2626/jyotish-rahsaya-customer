// To parse this JSON data, do
//
//     final newlyLaunchedModel = newlyLaunchedModelFromJson(jsonString);

import 'dart:convert';

NewlyLaunchedModel newlyLaunchedModelFromJson(String str) =>
    NewlyLaunchedModel.fromJson(json.decode(str));

String newlyLaunchedModelToJson(NewlyLaunchedModel data) =>
    json.encode(data.toJson());

class NewlyLaunchedModel {
  bool? status;
  List<Datum>? data;

  NewlyLaunchedModel({
    this.status,
    this.data,
  });

  factory NewlyLaunchedModel.fromJson(Map<String, dynamic> json) =>
      NewlyLaunchedModel(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  int? id;
  String? title;
  String? category;
  String? image;
  List<LaunchedPrice>? price;
  String? shortDescription;
  String? description;
  String? astrologerId;
  DateTime? time;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;
  String? astrologerImage;
  String? astrologerName;
  String? astrologerBio;
  String? activity;

  Datum({
    this.id,
    this.title,
    this.category,
    this.image,
    this.price,
    this.shortDescription,
    this.description,
    this.astrologerId,
    this.time,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.astrologerImage,
    this.astrologerName,
    this.astrologerBio,
    this.activity,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        title: json["title"],
        category: json["category"],
        image: json["image"],
        price: json["price"] == null
            ? []
            : List<LaunchedPrice>.from(
                json["price"]!.map((x) => LaunchedPrice.fromJson(x))),
        shortDescription: json["short_description"],
        description: json["description"],
        astrologerId: json["astrologer_id"],
        time: json["time"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
        astrologerImage: json["astrologer_image"],
        astrologerName: json["astrologer_name"],
        astrologerBio: json["astrologer_bio"],
        activity: json["activity"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "category": category,
        "image": image,
        "price": price == null
            ? []
            : List<dynamic>.from(price!.map((x) => x.toJson())),
        "short_description": shortDescription,
        "description": description,
        "astrologer_id": astrologerId,
        "time": time,
        "status": status,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "deleted_at": deletedAt,
        "astrologer_image": astrologerImage,
        "astrologer_name": astrologerName,
        "astrologer_bio": astrologerBio,
        "activity": activity,
      };
}

class LaunchedPrice {
  String? price;
  String? priceDescription;
  String? priceTitle;
  String? priceDiscount;

  LaunchedPrice({
    this.price,
    this.priceDescription,
    this.priceTitle,
    this.priceDiscount,
  });

  factory LaunchedPrice.fromJson(Map<String, dynamic> json) => LaunchedPrice(
        price: json["price"],
        priceDescription: json["price_description"],
        priceTitle: json["price_title"],
        priceDiscount: json["price_discount"],
      );

  Map<String, dynamic> toJson() => {
        "price": price,
        "price_description": priceDescription,
        "price_title": priceTitle,
        "price_discount": priceDiscount,
      };
}
