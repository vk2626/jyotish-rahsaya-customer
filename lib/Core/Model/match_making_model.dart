// To parse this JSON data, do
//
//     final matchMakingModel = matchMakingModelFromJson(jsonString);

import 'dart:convert';

MatchMakingModel matchMakingModelFromJson(String str) =>
    MatchMakingModel.fromJson(json.decode(str));

String matchMakingModelToJson(MatchMakingModel data) =>
    json.encode(data.toJson());

class MatchMakingModel {
  int status;
  Response response;

  MatchMakingModel({
    required this.status,
    required this.response,
  });

  factory MatchMakingModel.fromJson(Map<String, dynamic> json) =>
      MatchMakingModel(
        status: json["status"],
        response: Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "response": response.toJson(),
      };
}

class Response {
  Tara tara;
  Gana gana;
  Yoni yoni;
  Bhakoot bhakoot;
  Grahamaitri grahamaitri;
  Vasya vasya;
  Nadi nadi;
  Varna varna;
  int score;
  String botResponse;

  Response({
    required this.tara,
    required this.gana,
    required this.yoni,
    required this.bhakoot,
    required this.grahamaitri,
    required this.vasya,
    required this.nadi,
    required this.varna,
    required this.score,
    required this.botResponse,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        tara: Tara.fromJson(json["tara"]),
        gana: Gana.fromJson(json["gana"]),
        yoni: Yoni.fromJson(json["yoni"]),
        bhakoot: Bhakoot.fromJson(json["bhakoot"]),
        grahamaitri: Grahamaitri.fromJson(json["grahamaitri"]),
        vasya: Vasya.fromJson(json["vasya"]),
        nadi: Nadi.fromJson(json["nadi"]),
        varna: Varna.fromJson(json["varna"]),
        score: json["score"],
        botResponse: json["bot_response"],
      );

  Map<String, dynamic> toJson() => {
        "tara": tara.toJson(),
        "gana": gana.toJson(),
        "yoni": yoni.toJson(),
        "bhakoot": bhakoot.toJson(),
        "grahamaitri": grahamaitri.toJson(),
        "vasya": vasya.toJson(),
        "nadi": nadi.toJson(),
        "varna": varna.toJson(),
        "score": score,
        "bot_response": botResponse,
      };
}

class Bhakoot {
  int boyRasi;
  int girlRasi;
  String boyRasiName;
  String girlRasiName;
  int bhakoot;
  String description;
  int fullScore;

  Bhakoot({
    required this.boyRasi,
    required this.girlRasi,
    required this.boyRasiName,
    required this.girlRasiName,
    required this.bhakoot,
    required this.description,
    required this.fullScore,
  });

  factory Bhakoot.fromJson(Map<String, dynamic> json) => Bhakoot(
        boyRasi: json["boy_rasi"],
        girlRasi: json["girl_rasi"],
        boyRasiName: json["boy_rasi_name"],
        girlRasiName: json["girl_rasi_name"],
        bhakoot: json["bhakoot"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_rasi": boyRasi,
        "girl_rasi": girlRasi,
        "boy_rasi_name": boyRasiName,
        "girl_rasi_name": girlRasiName,
        "bhakoot": bhakoot,
        "description": description,
        "full_score": fullScore,
      };
}

class Gana {
  String boyGana;
  String girlGana;
  int gana;
  String description;
  int fullScore;

  Gana({
    required this.boyGana,
    required this.girlGana,
    required this.gana,
    required this.description,
    required this.fullScore,
  });

  factory Gana.fromJson(Map<String, dynamic> json) => Gana(
        boyGana: json["boy_gana"],
        girlGana: json["girl_gana"],
        gana: json["gana"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_gana": boyGana,
        "girl_gana": girlGana,
        "gana": gana,
        "description": description,
        "full_score": fullScore,
      };
}

class Grahamaitri {
  String boyLord;
  String girlLord;
  int grahamaitri;
  String description;
  int fullScore;

  Grahamaitri({
    required this.boyLord,
    required this.girlLord,
    required this.grahamaitri,
    required this.description,
    required this.fullScore,
  });

  factory Grahamaitri.fromJson(Map<String, dynamic> json) => Grahamaitri(
        boyLord: json["boy_lord"],
        girlLord: json["girl_lord"],
        grahamaitri: json["grahamaitri"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_lord": boyLord,
        "girl_lord": girlLord,
        "grahamaitri": grahamaitri,
        "description": description,
        "full_score": fullScore,
      };
}

class Nadi {
  String boyNadi;
  String girlNadi;
  int nadi;
  String description;
  int fullScore;

  Nadi({
    required this.boyNadi,
    required this.girlNadi,
    required this.nadi,
    required this.description,
    required this.fullScore,
  });

  factory Nadi.fromJson(Map<String, dynamic> json) => Nadi(
        boyNadi: json["boy_nadi"],
        girlNadi: json["girl_nadi"],
        nadi: json["nadi"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_nadi": boyNadi,
        "girl_nadi": girlNadi,
        "nadi": nadi,
        "description": description,
        "full_score": fullScore,
      };
}

class Tara {
  String boyTara;
  String girlTara;
  int tara;
  String description;
  int fullScore;

  Tara({
    required this.boyTara,
    required this.girlTara,
    required this.tara,
    required this.description,
    required this.fullScore,
  });

  factory Tara.fromJson(Map<String, dynamic> json) => Tara(
        boyTara: json["boy_tara"],
        girlTara: json["girl_tara"],
        tara: json["tara"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_tara": boyTara,
        "girl_tara": girlTara,
        "tara": tara,
        "description": description,
        "full_score": fullScore,
      };
}

class Varna {
  String boyVarna;
  String girlVarna;
  int varna;
  String description;
  int fullScore;

  Varna({
    required this.boyVarna,
    required this.girlVarna,
    required this.varna,
    required this.description,
    required this.fullScore,
  });

  factory Varna.fromJson(Map<String, dynamic> json) => Varna(
        boyVarna: json["boy_varna"],
        girlVarna: json["girl_varna"],
        varna: json["varna"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_varna": boyVarna,
        "girl_varna": girlVarna,
        "varna": varna,
        "description": description,
        "full_score": fullScore,
      };
}

class Vasya {
  String boyVasya;
  String girlVasya;
  int vasya;
  String description;
  int fullScore;

  Vasya({
    required this.boyVasya,
    required this.girlVasya,
    required this.vasya,
    required this.description,
    required this.fullScore,
  });

  factory Vasya.fromJson(Map<String, dynamic> json) => Vasya(
        boyVasya: json["boy_vasya"],
        girlVasya: json["girl_vasya"],
        vasya: json["vasya"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_vasya": boyVasya,
        "girl_vasya": girlVasya,
        "vasya": vasya,
        "description": description,
        "full_score": fullScore,
      };
}

class Yoni {
  String boyYoni;
  String girlYoni;
  int yoni;
  String description;
  int fullScore;

  Yoni({
    required this.boyYoni,
    required this.girlYoni,
    required this.yoni,
    required this.description,
    required this.fullScore,
  });

  factory Yoni.fromJson(Map<String, dynamic> json) => Yoni(
        boyYoni: json["boy_yoni"],
        girlYoni: json["girl_yoni"],
        yoni: json["yoni"],
        description: json["description"],
        fullScore: json["full_score"],
      );

  Map<String, dynamic> toJson() => {
        "boy_yoni": boyYoni,
        "girl_yoni": girlYoni,
        "yoni": yoni,
        "description": description,
        "full_score": fullScore,
      };
}
