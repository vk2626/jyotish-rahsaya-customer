import 'dart:convert';
/// status : true
/// is_online : [{"id":119,"name":"Abhinav Kumar Singh","label":1,"avatar":"https://jyotish.techsaga.live/uploads/astrologer/2024010514220453.png","astrologer":{"id":75,"user_id":"119","primary_skill_id":"3","all_skill_id":"3","language_id":"2","experience_year":2,"price":20,"discount_price":10,"bio":"fdg","is_online":1,"order_count":"35","gallery":[{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474914.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474920.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474941.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474909.jpg"}]}},{"id":45,"name":"Astro Guru Prabhakar ji","label":0,"avatar":"https://jyotish.techsaga.live/uploads/astrologer/2023122716353320.jpg","astrologer":{"id":51,"user_id":"45","primary_skill_id":"4,1","all_skill_id":"1","language_id":"1","experience_year":15,"price":30,"discount_price":0,"bio":"astro guru Prabhakar ji from ayodhya I read in falit jyotish from Sanskrit mahavidyalay ayodhya and my hobby reading in Book and vaidik puja and karm kand","is_online":1,"order_count":"3","gallery":[{"astrologer_id":"51","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024030814401910.jpg"},{"astrologer_id":"51","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024030814401924.jpg"}]}}]

LiveAstrologerModel liveAstrologerModelFromJson(String str) => LiveAstrologerModel.fromJson(json.decode(str));
String liveAstrologerModelToJson(LiveAstrologerModel data) => json.encode(data.toJson());
class LiveAstrologerModel {
  LiveAstrologerModel({
      bool? status, 
      List<IsOnline>? isOnline,}){
    _status = status;
    _isOnline = isOnline;
}

  LiveAstrologerModel.fromJson(dynamic json) {
    _status = json['status'];
    if (json['is_online'] != null) {
      _isOnline = [];
      json['is_online'].forEach((v) {
        _isOnline?.add(IsOnline.fromJson(v));
      });
    }
  }
  bool? _status;
  List<IsOnline>? _isOnline;
LiveAstrologerModel copyWith({  bool? status,
  List<IsOnline>? isOnline,
}) => LiveAstrologerModel(  status: status ?? _status,
  isOnline: isOnline ?? _isOnline,
);
  bool? get status => _status;
  List<IsOnline>? get isOnline => _isOnline;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    if (_isOnline != null) {
      map['is_online'] = _isOnline?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 119
/// name : "Abhinav Kumar Singh"
/// label : 1
/// avatar : "https://jyotish.techsaga.live/uploads/astrologer/2024010514220453.png"
/// astrologer : {"id":75,"user_id":"119","primary_skill_id":"3","all_skill_id":"3","language_id":"2","experience_year":2,"price":20,"discount_price":10,"bio":"fdg","is_online":1,"order_count":"35","gallery":[{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474914.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474920.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474941.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474909.jpg"}]}

IsOnline isOnlineFromJson(String str) => IsOnline.fromJson(json.decode(str));
String isOnlineToJson(IsOnline data) => json.encode(data.toJson());
class IsOnline {
  IsOnline({
      num? id, 
      String? name, 
      num? label, 
      String? avatar, 
      Astrologer? astrologer,}){
    _id = id;
    _name = name;
    _label = label;
    _avatar = avatar;
    _astrologer = astrologer;
}

  IsOnline.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _label = json['label'];
    _avatar = json['avatar'];
    _astrologer = json['astrologer'] != null ? Astrologer.fromJson(json['astrologer']) : null;
  }
  num? _id;
  String? _name;
  num? _label;
  String? _avatar;
  Astrologer? _astrologer;
IsOnline copyWith({  num? id,
  String? name,
  num? label,
  String? avatar,
  Astrologer? astrologer,
}) => IsOnline(  id: id ?? _id,
  name: name ?? _name,
  label: label ?? _label,
  avatar: avatar ?? _avatar,
  astrologer: astrologer ?? _astrologer,
);
  num? get id => _id;
  String? get name => _name;
  num? get label => _label;
  String? get avatar => _avatar;
  Astrologer? get astrologer => _astrologer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['label'] = _label;
    map['avatar'] = _avatar;
    if (_astrologer != null) {
      map['astrologer'] = _astrologer?.toJson();
    }
    return map;
  }

}

/// id : 75
/// user_id : "119"
/// primary_skill_id : "3"
/// all_skill_id : "3"
/// language_id : "2"
/// experience_year : 2
/// price : 20
/// discount_price : 10
/// bio : "fdg"
/// is_online : 1
/// order_count : "35"
/// gallery : [{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474914.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474920.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474941.jpg"},{"astrologer_id":"75","image":"https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474909.jpg"}]

Astrologer astrologerFromJson(String str) => Astrologer.fromJson(json.decode(str));
String astrologerToJson(Astrologer data) => json.encode(data.toJson());
class Astrologer {
  Astrologer({
      num? id, 
      String? userId, 
      String? primarySkillId, 
      String? allSkillId, 
      String? languageId, 
      num? experienceYear, 
      num? price, 
      num? discountPrice, 
      String? bio, 
      num? isOnline, 
      String? orderCount, 
      List<Gallery>? gallery,}){
    _id = id;
    _userId = userId;
    _primarySkillId = primarySkillId;
    _allSkillId = allSkillId;
    _languageId = languageId;
    _experienceYear = experienceYear;
    _price = price;
    _discountPrice = discountPrice;
    _bio = bio;
    _isOnline = isOnline;
    _orderCount = orderCount;
    _gallery = gallery;
}

  Astrologer.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _primarySkillId = json['primary_skill_id'];
    _allSkillId = json['all_skill_id'];
    _languageId = json['language_id'];
    _experienceYear = json['experience_year'];
    _price = json['price'];
    _discountPrice = json['discount_price'];
    _bio = json['bio'];
    _isOnline = json['is_online'];
    _orderCount = json['order_count'];
    if (json['gallery'] != null) {
      _gallery = [];
      json['gallery'].forEach((v) {
        _gallery?.add(Gallery.fromJson(v));
      });
    }
  }
  num? _id;
  String? _userId;
  String? _primarySkillId;
  String? _allSkillId;
  String? _languageId;
  num? _experienceYear;
  num? _price;
  num? _discountPrice;
  String? _bio;
  num? _isOnline;
  String? _orderCount;
  List<Gallery>? _gallery;
Astrologer copyWith({  num? id,
  String? userId,
  String? primarySkillId,
  String? allSkillId,
  String? languageId,
  num? experienceYear,
  num? price,
  num? discountPrice,
  String? bio,
  num? isOnline,
  String? orderCount,
  List<Gallery>? gallery,
}) => Astrologer(  id: id ?? _id,
  userId: userId ?? _userId,
  primarySkillId: primarySkillId ?? _primarySkillId,
  allSkillId: allSkillId ?? _allSkillId,
  languageId: languageId ?? _languageId,
  experienceYear: experienceYear ?? _experienceYear,
  price: price ?? _price,
  discountPrice: discountPrice ?? _discountPrice,
  bio: bio ?? _bio,
  isOnline: isOnline ?? _isOnline,
  orderCount: orderCount ?? _orderCount,
  gallery: gallery ?? _gallery,
);
  num? get id => _id;
  String? get userId => _userId;
  String? get primarySkillId => _primarySkillId;
  String? get allSkillId => _allSkillId;
  String? get languageId => _languageId;
  num? get experienceYear => _experienceYear;
  num? get price => _price;
  num? get discountPrice => _discountPrice;
  String? get bio => _bio;
  num? get isOnline => _isOnline;
  String? get orderCount => _orderCount;
  List<Gallery>? get gallery => _gallery;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['primary_skill_id'] = _primarySkillId;
    map['all_skill_id'] = _allSkillId;
    map['language_id'] = _languageId;
    map['experience_year'] = _experienceYear;
    map['price'] = _price;
    map['discount_price'] = _discountPrice;
    map['bio'] = _bio;
    map['is_online'] = _isOnline;
    map['order_count'] = _orderCount;
    if (_gallery != null) {
      map['gallery'] = _gallery?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// astrologer_id : "75"
/// image : "https://jyotish.techsaga.live/uploads/astrologer_gallery/2024012512474939.jpg"

Gallery galleryFromJson(String str) => Gallery.fromJson(json.decode(str));
String galleryToJson(Gallery data) => json.encode(data.toJson());
class Gallery {
  Gallery({
      String? astrologerId, 
      String? image,}){
    _astrologerId = astrologerId;
    _image = image;
}

  Gallery.fromJson(dynamic json) {
    _astrologerId = json['astrologer_id'];
    _image = json['image'];
  }
  String? _astrologerId;
  String? _image;
Gallery copyWith({  String? astrologerId,
  String? image,
}) => Gallery(  astrologerId: astrologerId ?? _astrologerId,
  image: image ?? _image,
);
  String? get astrologerId => _astrologerId;
  String? get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['astrologer_id'] = _astrologerId;
    map['image'] = _image;
    return map;
  }

}