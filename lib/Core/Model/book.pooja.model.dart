// To parse this JSON data, do
//
//     final bookPoojaModel = bookPoojaModelFromJson(jsonString);

import 'dart:convert';

BookPoojaModel bookPoojaModelFromJson(String str) =>
    BookPoojaModel.fromJson(json.decode(str));

String bookPoojaModelToJson(BookPoojaModel data) => json.encode(data.toJson());

class BookPoojaModel {
  bool? status;
  List<PoojaListItem>? data;

  BookPoojaModel({
    this.status,
    this.data,
  });

  factory BookPoojaModel.fromJson(Map<String, dynamic> json) => BookPoojaModel(
        status: json["status"],
        data: List<PoojaListItem>.from(json["data"].map((x) => PoojaListItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class PoojaListItem {
  int id;
  String astrologerId;
  String shortDescription;
  String title;
  String image;
  DateTime time;
  int status;
  dynamic type;
  String astrologerImage;

  PoojaListItem({
    required this.id,
    required this.astrologerId,
    required this.shortDescription,
    required this.title,
    required this.image,
    required this.time,
    required this.status,
    required this.type,
    required this.astrologerImage,
  });

  factory PoojaListItem.fromJson(Map<String, dynamic> json) => PoojaListItem(
        id: json["id"],
        astrologerId: json["astrologer_id"],
        shortDescription: json["short_description"],
        title: json["title"],
        image: json["image"],
        time: DateTime.parse(json["time"]),
        status: json["status"],
        type: json["type"],
        astrologerImage: json["astrologer_image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "astrologer_id": astrologerId,
        "short_description": shortDescription,
        "title": title,
        "image": image,
        "time": time.toIso8601String(),
        "status": status,
        "type": type,
        "astrologer_image": astrologerImage,
      };
}
