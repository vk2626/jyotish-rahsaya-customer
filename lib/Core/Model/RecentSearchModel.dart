// To parse this JSON data, do
//
//     final recentSearchModel = recentSearchModelFromJson(jsonString);

import 'dart:convert';

RecentSearchModel recentSearchModelFromJson(String str) =>
    RecentSearchModel.fromJson(json.decode(str));

String recentSearchModelToJson(RecentSearchModel data) =>
    json.encode(data.toJson());

class RecentSearchModel {
  bool status;
  List<AstrologerElement> astrologers;
  List<dynamic> poojaProduct;
  List<Product> products;

  RecentSearchModel({
    required this.status,
    required this.astrologers,
    required this.poojaProduct,
    required this.products,
  });

  factory RecentSearchModel.fromJson(Map<String, dynamic> json) =>
      RecentSearchModel(
        status: json["status"],
        astrologers: List<AstrologerElement>.from(
            json["astrologers"].map((x) => AstrologerElement.fromJson(x))),
        poojaProduct: List<dynamic>.from(json["poojaProduct"].map((x) => x)),
        products: List<Product>.from(
            json["products"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "astrologers": List<dynamic>.from(astrologers.map((x) => x.toJson())),
        "poojaProduct": List<dynamic>.from(poojaProduct.map((x) => x)),
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
      };
}

class AstrologerElement {
  int id;
  String name;
  int label;
  String avatar;
  int isOnlineAstrologer;
  String labelText;

  AstrologerElement({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.labelText,
  });

  factory AstrologerElement.fromJson(Map<String, dynamic> json) =>
      AstrologerElement(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        labelText: json["label_text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "label_text": labelText,
      };
}

class Product {
  int id;
  String title;
  String image;
  String activity;
  String productLabel;
  String productLableText;

  Product({
    required this.id,
    required this.title,
    required this.image,
    required this.activity,
    required this.productLabel,
    required this.productLableText,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        activity: json["activity"],
        productLabel: json["product_label"],
        productLableText: json["product_lable_text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "activity": activity,
        "product_label": productLabel,
        "product_lable_text": productLableText,
      };
}
