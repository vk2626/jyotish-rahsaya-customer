// To parse this JSON data, do
//
//     final jyotishMallAstrologersModel = jyotishMallAstrologersModelFromJson(jsonString);

import 'dart:convert';

JyotishMallAstrologersModel jyotishMallAstrologersModelFromJson(String str) =>
    JyotishMallAstrologersModel.fromJson(json.decode(str));

String jyotishMallAstrologersModelToJson(JyotishMallAstrologersModel data) =>
    json.encode(data.toJson());

class JyotishMallAstrologersModel {
  bool status;
  List<Astrologers> astrologers;

  JyotishMallAstrologersModel({
    required this.status,
    required this.astrologers,
  });

  factory JyotishMallAstrologersModel.fromJson(Map<String, dynamic> json) =>
      JyotishMallAstrologersModel(
        status: json["status"],
        astrologers: List<Astrologers>.from(
            json["astrologers"].map((x) => Astrologers.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "astrologers": List<dynamic>.from(astrologers.map((x) => x.toJson())),
      };
}

class Astrologers {
  num id;
  String name;
  num label;
  String avatar;
  num isOnlineAstrologer;
  AstrologerAstrologer astrologer;

  Astrologers({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.isOnlineAstrologer,
    required this.astrologer,
  });

  factory Astrologers.fromJson(Map<String, dynamic> json) => Astrologers(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        isOnlineAstrologer: json["is_online_astrologer"],
        astrologer: AstrologerAstrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "is_online_astrologer": isOnlineAstrologer,
        "astrologer": astrologer.toJson(),
      };
}

class AstrologerAstrologer {
  num id;
  String userId;
  String primarySkillId;
  String allSkillId;
  String languageId;
  String experienceYear;
  String bio;
  num freeChat;
  String orderCount;
  num isOnline;
  String displayName;
  String productId;
  num rating;
  String labelText;
  String price;
  String discountPrice;
  String primarySkillName;
  String allSkillName;
  String languageName;
  List<dynamic> gallery;

  AstrologerAstrologer({
    required this.id,
    required this.userId,
    required this.primarySkillId,
    required this.allSkillId,
    required this.languageId,
    required this.experienceYear,
    required this.bio,
    required this.freeChat,
    required this.orderCount,
    required this.isOnline,
    required this.displayName,
    required this.productId,
    required this.rating,
    required this.labelText,
    required this.price,
    required this.discountPrice,
    required this.primarySkillName,
    required this.allSkillName,
    required this.languageName,
    required this.gallery,
  });

  factory AstrologerAstrologer.fromJson(Map<String, dynamic> json) =>
      AstrologerAstrologer(
        id: json["id"],
        userId: json["user_id"],
        primarySkillId: json["primary_skill_id"],
        allSkillId: json["all_skill_id"],
        languageId: json["language_id"],
        experienceYear: json["experience_year"],
        bio: json["bio"],
        freeChat: json["free_chat"],
        orderCount: json["order_count"],
        isOnline: json["is_online"],
        displayName: json["display_name"],
        productId: json["product_id"],
        rating: json["rating"],
        labelText: json["label_text"],
        price: json["price"],
        discountPrice:
            (json["discount_price"].isEmpty || json['discount_price'] == null)
                ? "0"
                : json['discount_price'],
        primarySkillName: json["primary_skill_name"],
        allSkillName: json["all_skill_name"],
        languageName: json["language_name"],
        gallery: List<dynamic>.from(json["gallery"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "primary_skill_id": primarySkillId,
        "all_skill_id": allSkillId,
        "language_id": languageId,
        "experience_year": experienceYear,
        "bio": bio,
        "free_chat": freeChat,
        "order_count": orderCount,
        "is_online": isOnline,
        "display_name": displayName,
        "product_id": productId,
        "rating": rating,
        "label_text": labelText,
        "price": price,
        "discount_price": discountPrice,
        "primary_skill_name": primarySkillName,
        "all_skill_name": allSkillName,
        "language_name": languageName,
        "gallery": List<dynamic>.from(gallery.map((x) => x)),
      };
}
