// To parse this JSON data, do
//
//     final createOrderPoojaModel = createOrderPoojaModelFromJson(jsonString);

import 'dart:convert';

CreateOrderPoojaModel createOrderPoojaModelFromJson(String str) =>
    CreateOrderPoojaModel.fromJson(json.decode(str));

String createOrderPoojaModelToJson(CreateOrderPoojaModel data) =>
    json.encode(data.toJson());

class CreateOrderPoojaModel {
  bool? status;
  Payment? payment;

  CreateOrderPoojaModel({
    this.status,
    this.payment,
  });

  factory CreateOrderPoojaModel.fromJson(Map<String, dynamic> json) =>
      CreateOrderPoojaModel(
        status: json["status"],
        payment:
            json["payment"] == null ? null : Payment.fromJson(json["payment"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "payment": payment?.toJson(),
      };
}

class Payment {
  String? userId;
  String? astrologerId;
  String? productId;
  String? orderPoojaId;
  String? orderId;
  String? value;
  String? gstAmount;
  String? totalValue;
  String? paymentType;
  String razorpayId;
  String razorpaySecretKey;

  int? isSuccess;
  String? remark;
  String? productPrice;
  // String? walletDeducted;
  DateTime? createdAt;
  int? id;

  Payment({
    this.userId,
    this.astrologerId,
    this.productId,
    this.orderId,
    this.orderPoojaId,
    this.value,
    this.gstAmount,
    this.totalValue,
    this.paymentType,
    this.isSuccess,
    required this.razorpayId,
    required this.razorpaySecretKey,
    this.remark,
    this.productPrice,
    // this.walletDeducted,
    this.createdAt,
    this.id,
  });

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        userId: json["user_id"],
        astrologerId: json["astrologer_id"],
        productId: json["product_id"],
        orderId: json["order_id"],
        orderPoojaId: json['order_pooja_id'].toString(),
        value: json["value"],
        gstAmount: json["gst_amount"],
        totalValue: json["total_value"],
        paymentType: json["payment_type"],
        isSuccess: json["is_success"],
        razorpayId: json['RAZORPAY_KEY_ID'],
        razorpaySecretKey: json['RAZORPAY_KEY_SECRET'],
        remark: json["remark"],
        productPrice: json["product_price"],
        // walletDeducted: json["wallet_deducted"] ?? "",
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "astrologer_id": astrologerId,
        "product_id": productId,
        "order_id": orderId,
        "value": value,
        "gst_amount": gstAmount,
        "total_value": totalValue,
        "payment_type": paymentType,
        "is_success": isSuccess,
        "remark": remark,
        "product_price": productPrice,
        // "wallet_deducted": walletDeducted,
        "created_at": createdAt?.toIso8601String(),
        "id": id,
      };
}
