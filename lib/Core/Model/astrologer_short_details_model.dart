// To parse this JSON data, do
//
//     final astrologerShortDetailsModel = astrologerShortDetailsModelFromJson(jsonString);

import 'dart:convert';

AstrologerShortDetailsModel astrologerShortDetailsModelFromJson(String str) =>
    AstrologerShortDetailsModel.fromJson(json.decode(str));

String astrologerShortDetailsModelToJson(AstrologerShortDetailsModel data) =>
    json.encode(data.toJson());

class AstrologerShortDetailsModel {
  bool status;
  List<AstrologerElement> astrologers;

  AstrologerShortDetailsModel({
    required this.status,
    required this.astrologers,
  });

  factory AstrologerShortDetailsModel.fromJson(Map<String, dynamic> json) =>
      AstrologerShortDetailsModel(
        status: json["status"],
        astrologers: List<AstrologerElement>.from(
            json["astrologers"].map((x) => AstrologerElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "astrologers": List<dynamic>.from(astrologers.map((x) => x.toJson())),
      };
}

class AstrologerElement {
  int id;
  String name;
  int label;
  String avatar;
  num rating;
  AstrologerAstrologer astrologer;

  AstrologerElement({
    required this.id,
    required this.name,
    required this.label,
    required this.avatar,
    required this.rating,
    required this.astrologer,
  });

  factory AstrologerElement.fromJson(Map<String, dynamic> json) =>
      AstrologerElement(
        id: json["id"],
        name: json["name"],
        label: json["label"],
        avatar: json["avatar"],
        rating: json["rating"] ?? 0,
        astrologer: AstrologerAstrologer.fromJson(json["astrologer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "label": label,
        "avatar": avatar,
        "rating": rating,
        "astrologer": astrologer.toJson(),
      };
}

class AstrologerAstrologer {
  int id;
  String userId;
  String gender;
  String bio;

  AstrologerAstrologer({
    required this.id,
    required this.userId,
    required this.gender,
    required this.bio,
  });

  factory AstrologerAstrologer.fromJson(Map<String, dynamic> json) =>
      AstrologerAstrologer(
        id: json["id"],
        userId: json["user_id"],
        gender: json["gender"],
        bio: json["bio"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "gender": gender,
        "bio": bio,
      };
}
