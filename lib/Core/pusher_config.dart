// import 'dart:developer';
// import 'package:pusher_channels_flutter/pusher_channels_flutter.dart';

// class PusherConfig {
//   late PusherChannelsFlutter _pusher;

//   String API_KEY = "8e0231d05f06cc62323f";
//   String API_CLUSTER = "ap2";
//   String APP_ID = "1912323";
//   String SECRET = "131d9f331937f9afc6aa";

//   Future<void> initPusher(onEvent, {channelName = "chat"}) async {
//     _pusher = PusherChannelsFlutter.getInstance();

//     try {
//       await _pusher.init(
//         apiKey: API_KEY,
//         cluster: API_CLUSTER,
//         onEvent: onEvent,
//         onError: onError,
//         onConnectionStateChange: onConnectionStateChange,
//         onSubscriptionSucceeded: onSubscriptionSucceeded,
//         onSubscriptionError: onSubscriptionError,
//         onDecryptionFailure: onDecryptionFailure,
//         onMemberAdded: onMemberAdded,
//         onMemberRemoved: onMemberRemoved,
//       );
//       try {
//         await _pusher.subscribe(channelName: "$channelName").whenComplete(
//               () => log("Done"),
//             );
//         await _pusher.connect();

//         log("trying to subscribe to :  " "$channelName");
//       } catch (e) {
//         log(e.toString());
//       }
//     } catch (e) {
//       log("error in initialization: $e");
//     }
//   }

//   // void sendMessage() async {
//   //   await _pusher.trigger(PusherEvent(channelName: PrivateChannel(name, pusher), eventName: "Varun"));
//   // }

//   void disconnect() {
//     _pusher.disconnect();
//   }

//   void onConnectionStateChange(dynamic currentState, dynamic previousState) {
//     log("Connection: $currentState");
//   }

//   void onError(String message, int? code, dynamic e) {
//     log("onError: $message code: $code exception: $e");
//   }

//   void onEvent(PusherEvent event) {
//     log("onEvent: $event");
//     log("Event channel: ${event.channelName}, Event name: ${event.eventName}, Event data: ${event.data}");

//     // Parse and log the event data (if JSON)
//     try {
//       final eventData = event.data != null ? event.data : "{}";
//       log("Parsed data: $eventData");
//     } catch (e) {
//       log("Error parsing event data: $e");
//     }
//   }

//   void onSubscriptionSucceeded(String channelName, dynamic data) {
//     log("onSubscriptionSucceeded: $channelName data: $data");
//     final me = _pusher.getChannel(channelName)?.me;
//     log("Me: $me");
//   }

//   void onSubscriptionError(String message, dynamic e) {
//     log("onSubscriptionError: $message Exception: $e");
//   }

//   void onDecryptionFailure(String event, String reason) {
//     log("onDecryptionFailure: $event reason: $reason");
//   }

//   void onMemberAdded(String channelName, PusherMember member) {
//     log("onMemberAdded: $channelName user: $member");
//   }

//   void onMemberRemoved(String channelName, PusherMember member) {
//     log("onMemberRemoved: $channelName user: $member");
//   }

//   void onSubscriptionCount(String channelName, int subscriptionCount) {
//     log("onSubscriptionCount: $channelName subscriptionCount: $subscriptionCount");
//   }
// }
