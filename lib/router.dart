import 'dart:convert';
import 'dart:developer';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:go_router/go_router.dart';
import 'package:jyotish_rahsaya/Core/Api/url_constants.dart';
import 'Core/Api/Constants.dart';
import 'Core/Provider/authentication.provider.dart';
import 'Core/Provider/chatting_provider.dart';
import 'Core/Provider/go_live_provider.dart';
import 'Core/Provider/list.of.api.provider.dart';
import 'Core/formatter.dart';
import 'Core/logger_helper.dart';
import 'Screens/AstrologerProfileNew.dart';
import 'Screens/IncomingChatRequest.dart';
import 'Screens/LoginScreen.dart';
import 'Screens/NavigationScreen.dart';
import 'Screens/flutter_chat_screen.dart';
import 'Screens/jyotish_mall/live_pooja_screen.dart';
import 'Screens/jyotish_mall/one_one_direct_call_screen.dart';
import 'Screens/jyotish_mall/one_one_voip_screen.dart';
import 'Screens/jyotish_mall/pooja_details_screen_new.dart';
import 'router_constants.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:upgrader/upgrader.dart';

import 'Core/helper_functions.dart';
import 'Screens/IncomingCallRequest.dart';

class AppRouter {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  late SharedPreferences prefs;
  Map<String, dynamic> _currentUuid = {};
  Future<void> rejectCallFunction({required String communicationId}) async {
    var endPointUrl = UrlConstants.rejectCall;
    final Map<String, String> body = {'communication_id': communicationId};
    THelperFunctions.clearCallingPrefs();
    await THelperFunctions.httpPostHandler(
        client: http.Client(), endPointUrl: endPointUrl, body: body);
  }

  flutterCallKitListener() async {
    FlutterCallkitIncoming.onEvent.listen((CallEvent? event) {
      switch (event!.event) {
        case Event.actionCallIncoming:
          getCurrentCall(isNeedtoDecline: false);
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallStart:
          break;
        case Event.actionCallAccept:
          TLoggerHelper.info(event.event.name);
          handleCallAccept().whenComplete(() {
            // String route = _currentUuid['type'] == "chat"
            //     ? "incoming_chat"
            //     : "incoming_call";
            String route;

            switch (_currentUuid['type']) {
              case 'chat':
                route = 'incoming_chat';
                break;
              case 'call':
                route = "incoming_call";
                break;
              case 'pooja-call':
                route = 'pooja_call';
                break;
              case 'pooja-video':
                route = 'pooja_video';
                break;
              case 'pooja-video-call':
                route = 'pooja-video-call';
                break;

              default:
                route = 'unknown_type'; // Handle unknown types if necessary
                break;
            }
            GoRouter.of(navigatorKey.currentContext!).go('/home/$route');
          });
          break;
        case Event.actionCallDecline:
          TLoggerHelper.info(event.event.name);
          TLoggerHelper.info(event.event.name);
          // log("This is the body " + event.body);
          rejectCallFunction(
              communicationId: _currentUuid['communicationId'].toString());
          break;
        case Event.actionDidUpdateDevicePushTokenVoip:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallEnded:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallTimeout:
          TLoggerHelper.info(event.event.name);
          // log("This is the body " + event.body);
          rejectCallFunction(
              communicationId: _currentUuid['communicationId'].toString());
          break;
        case Event.actionCallCallback:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleHold:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleMute:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleDmtf:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleGroup:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallToggleAudioSession:
          TLoggerHelper.info(event.event.name);
          break;
        case Event.actionCallCustom:
          TLoggerHelper.info(event.event.name);
          break;
      }
    });
  }

  GoRouter buildRouter() {
    flutterCallKitListener();
    return GoRouter(
      navigatorKey: navigatorKey,
      initialLocation: "/home",
      observers: [GoRouterObserver()],
      routes: [
        GoRoute(
          path: '/auth',
          name: RouteConstants.authScreen,
          builder: (context, state) {
            return ListenableProvider(
                create: (context) => AuthenticationProvider(),
                child: LoginScreen());
          },
        ),
        GoRoute(
            path: '/home',
            name: RouteConstants.mainhomescreen,
            builder: (context, state) => UpgradeAlert(
                  showIgnore: false,
                  showLater: false,
                  barrierDismissible: false,
                  shouldPopScope: () {
                    return false;
                  },
                  upgrader: Upgrader(
                      debugLogging: true,
                      messages: UpgraderMessages(),
                      durationUntilAlertAgain: Duration.zero),
                  child: MultiProvider(providers: [
                    ListenableProvider(
                        create: (context) => ListOfApisProvider()),
                    // ListenableProvider(create: (context) => WaitListNotifier()),
                  ], child: NavigationScreen(pageIndex: 0)),
                ),
            routes: [
              GoRoute(
                name: RouteConstants.astrologerProfile,
                path: 'astrologer-profile',
                builder: (context, state) {
                  final astrologerId =
                      state.uri.queryParameters['astrologerId'].toString();
                  final type = state.uri.queryParameters['type'] ?? 'chat';
                  final bool isAstrologerOnline =
                      state.uri.queryParameters['isAstrologerOnline'] == "1";
                  return MultiProvider(
                    providers: [
                      ListenableProvider(create: (context) => GoLiveProvider()),
                      ListenableProvider(
                          create: (context) => ListOfApisProvider())
                    ],
                    child: AstrologerProfileNew(
                        isAstrologerOnline: isAstrologerOnline,
                        astrologerId: astrologerId ?? "",
                        type: type.toString()),
                  );
                },
              ),
              GoRoute(
                name: RouteConstants.poojaScreen,
                path: 'pooja',
                builder: (context, state) {
                  final poojaId =
                      state.uri.queryParameters['poojaId'].toString();
                  final userInfoId =
                      state.uri.queryParameters['user_info_id'].toString();
                  final astrologerId =
                      state.uri.queryParameters['astrologerId'];
                  final isDirectPooja =
                      state.uri.queryParameters['isDirectPooja'].toString();
                  return MultiProvider(
                      providers: [
                        // ListenableProvider(create: (context) => GoLiveProvider()),
                        ListenableProvider(
                            create: (context) => ListOfApisProvider())
                      ],
                      child: PoojaDetailsScreenNew(
                        userInfoId: userInfoId,
                        poojaId: poojaId,
                        astrologerId: astrologerId ?? "",
                        isDirectPooja: isDirectPooja == "1" ? true : false,
                      ));
                },
              ),
              GoRoute(
                path: 'pooja_video',
                builder: (context, state) {
                  return OneOneVoipScreen(
                    token: _currentUuid['token'],
                    channelName: _currentUuid['channel'],
                    astrologerName: _currentUuid['name'],
                    astrologerImage: _currentUuid['image'],
                  );
                },
              ),
              GoRoute(
                path: 'pooja_call',
                builder: (context, state) {
                  return OneOneDirectCallScreen(
                    token: _currentUuid['token'],
                    channelName: _currentUuid['channel'],
                    astrologerName: _currentUuid['name'],
                    astrologerImage: _currentUuid['image'],
                  );
                },
              ),
              GoRoute(
                  path: 'pooja-video-call',
                  builder: (context, state) {
                    return LivePoojaScreen(
                        astrologerId: _currentUuid['astrologerId'],
                        token: _currentUuid['token'],
                        channelName: _currentUuid['channelName'],
                        astrologerName: _currentUuid['astrologerName'],
                        astrologerImage: _currentUuid['astrologerImage'],
                        poojaId: _currentUuid['poojaId']);
                  }),
              GoRoute(
                  path: 'incoming_call',
                  builder: (context, state) {
                    return ListenableProvider(
                      create: (context) => ListOfApisProvider(),
                      child: IncomingCallRequest(
                        userName: _currentUuid['callerName']!,
                        astrologerId: _currentUuid['astrologerId']!,
                        imageUrl: _currentUuid['callerAvatar']!,
                        channelName: _currentUuid['channel']!,
                        token: _currentUuid['token']!,
                        communication_id:
                            _currentUuid['communicationId'].toString(),
                        totalCallDuration: Duration(
                            minutes:
                                int.parse(_currentUuid['duration'].toString())),
                      ),
                    );
                  }),
              GoRoute(
                  path: 'incoming_chat',
                  builder: (context, state) {
                    return ListenableProvider(
                      create: (context) => ChattingProvider(),
                      child: IncomingChatRequest(
                        _currentUuid["communicationId"],
                        _currentUuid["image"],
                        dob: _currentUuid['message_info']['dob'].toString(),
                        tob:
                            _currentUuid['message_info']['dob_time'].toString(),
                        lat: _currentUuid['message_info']['lat'].toString(),
                        lon: _currentUuid['message_info']['lon'].toString(),
                        _currentUuid["name"],
                        _currentUuid["astrologerId"],
                        _currentUuid['chat_price'],
                        _currentUuid['orderId'],
                        infoId: _currentUuid['infoId'].toString(),
                        isFreeChat:
                            _currentUuid['is_freechat'].toString() == "1",
                        chatDuration: _currentUuid['durationTime'],
                        messageInfo: {
                          'name':
                              _currentUuid['message_info']['name'].toString(),
                          'dob': _currentUuid['message_info']['dob'].toString(),
                          'dob_place': _currentUuid['message_info']['dob_place']
                              .toString(),
                          'gender':
                              _currentUuid['message_info']['gender'].toString(),
                          'dob_time': _currentUuid['message_info']['dob_time']
                              .toString(),
                        },
                      ),
                    );
                  },
                  routes: [
                    GoRoute(
                        path: 'chat_screen',
                        name: RouteConstants.chatScreen,
                        builder: (context, state) {
                          return MultiProvider(
                            providers: [
                              ListenableProvider(
                                  create: (context) => ChattingProvider()),
                              ListenableProvider(
                                  create: (context) => ListOfApisProvider()),
                            ],
                            child: FlutterChatScreen(
                                orderID: _currentUuid['orderId'],
                                cummID: _currentUuid["communicationId"],
                                chat_price:
                                    int.parse(_currentUuid['chat_price']),
                                fromId: _currentUuid["user_id"],
                                toId: _currentUuid["astrologerId"],
                                timerDuration: Duration(
                                    minutes: int.parse(
                                        _currentUuid['durationTime'])),
                                astrologerName: TFormatter.capitalizeSentence(
                                    _currentUuid['name'].toString()),
                                astrologerImage: _currentUuid["image"],
                                isFreeChat:
                                    _currentUuid['is_freechat'].toString() ==
                                        "1",
                                dob: _currentUuid['message_info']['dob']
                                    .toString(),
                                tob: _currentUuid['message_info']['dob_time']
                                    .toString(),
                                messageInfo: {
                                  'name': _currentUuid['message_info']['name']
                                      .toString(),
                                  'dob': _currentUuid['message_info']['dob']
                                      .toString(),
                                  'dob_place': _currentUuid['message_info']
                                          ['dob_place']
                                      .toString(),
                                  'gender': _currentUuid['message_info']
                                          ['gender']
                                      .toString(),
                                  'dob_time': _currentUuid['message_info']
                                          ['dob_time']
                                      .toString(),
                                },
                                infoId: _currentUuid['infoId'].toString(),
                                lat: _currentUuid['message_info']['lat']
                                    .toString(),
                                lon: _currentUuid['message_info']['lon']
                                    .toString()),
                          );
                        })
                  ]),
            ])
      ],
      redirect: (context, state) async {
        prefs = await SharedPreferences.getInstance();
        if (prefs.containsKey(Constants.isCalling)) {
          String? savedData = prefs.getString(Constants.isCalling);
          if (savedData != null) {
            _currentUuid = jsonDecode(savedData);
            String route;

            switch (_currentUuid['type']) {
              case 'chat':
                route = 'incoming_chat';
                break;
              case 'call':
                route = 'incoming_call';
                break;
              case 'pooja-call':
                route = 'pooja_call';
                break;
              case 'pooja-video':
                route = 'pooja_video';
                break;
              case 'pooja-video-call':
                route = 'pooja-video-call';
                break;
              default:
                route = 'unknown_type';
                break;
            }

            return '/home/$route';
          }
        }
        return null; // No redirection needed, proceed to the requested route
      },
      // redirect: (context, state) async {
      //   prefs = await SharedPreferences.getInstance();
      //   // // Check for incoming calls
      //   // var currentCall = await getCurrentCall();
      //   // if (currentCall != null) {
      //   //   return '/incoming_call'; // Redirect to incoming call screen
      //   // }

      //   if (!prefs.containsKey(Constants.userID)) {
      //     return '/auth'; // Redirect to auth if user is not logged in
      //   }
      //   return null; // Allow navigation to the requested route
      // },
    );
  }

  Future<void> handleCallAccept() async {
    if (prefs.containsKey(Constants.isCalling)) {
      String? savedData = prefs.getString(Constants.isCalling);
      if (savedData != null) {
        _currentUuid = jsonDecode(savedData);
      }
    } else {
      await getCurrentCall(isNeedtoDecline: true);
      log("Exists ${_currentUuid}");
    }
  }

  Future<dynamic> getCurrentCall({required bool isNeedtoDecline}) async {
    var calls = await FlutterCallkitIncoming.activeCalls();
    if (calls is List) {
      if (isNeedtoDecline) {
        await FlutterCallkitIncoming.endAllCalls();
      }
      TLoggerHelper.debug("Checking Calls" + calls.toString());
      if (calls.isNotEmpty) {
        TLoggerHelper.debug('DATA: $calls');

        calls[0]['extra'].forEach((key, value) {
          if (key is String) {
            _currentUuid[key] = value;
            _currentUuid.addEntries({key: value}.entries);
          } else {}
          log("Logging in if currentUUID ${_currentUuid}");
        });
        TLoggerHelper.info(_currentUuid.entries.toString());
        return calls[0]['extra'];
      } else {
        _currentUuid = {};
        log("Logging in else currentUUID ${_currentUuid}");
        return null;
      }
    }
    log("Logging outside currentUUID ${_currentUuid}");
  }
}

class GoRouterObserver extends NavigatorObserver {
  final FirebaseAnalytics _analytics = FirebaseAnalytics.instance;
  @override
  void didPush(Route route, Route? previousRoute) {
    super.didPush(route, previousRoute);
    if (route.settings.name != null) {
      _logScreenView(route.settings.name ?? "");
    }
  }

  void _logScreenView(String screenName) async {
    await _analytics.logEvent(
      name: 'screen_view',
      parameters: {
        'screen_name': screenName.toLowerCase(),
        'screen_class': 'GoRouter',
      },
    );
    print('Screen view logged: $screenName');
  }
}
