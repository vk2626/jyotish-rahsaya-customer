import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jyotish_rahsaya/Core/notification/local_notice_service.dart';
import 'package:jyotish_rahsaya/Screens/AstrologerProfileNew.dart';
import 'package:jyotish_rahsaya/Screens/chatSupport/support_chat_screen.dart';
import 'package:jyotish_rahsaya/Screens/gemstone/gemstone_screen.dart';
import 'package:jyotish_rahsaya/SubScreens/free_chat_available_screen.dart';
import 'Core/Provider/kundli_provider_new.dart';
import 'Core/helper_functions.dart';
import 'Core/notification/notification_handler_ios.dart';
import 'Screens/NavigationScreen.dart';
import 'Screens/horoScopeScreens/kundliScreens/create_new_kundli_matching_screen.dart';
import 'Screens/setting.screen.dart';
import 'Screens/walletScreens/wallet.screen.dart';
import 'package:provider/provider.dart';
// import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Core/Api/Constants.dart';
import 'Core/Provider/chatting_provider.dart';
import 'Core/Provider/list.of.api.provider.dart';
import 'Core/firebaseremoteconfig_service.dart';
import 'Core/langConstant/language.constant.dart';
import 'Core/notification/notification_handler_android.dart';
import 'Screens/flutter_chat_screen.dart';
import 'Screens/order.history.dart';
import 'firebase_options.dart';
import 'router.dart';
import 'utils/AppColor.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? callingPrefs = prefs.getString(Constants.isCalling);

  String defaultLanguage = prefs.getString(LAGUAGE_CODE) ?? 'en';

  Widget myApp;
  try {
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);
  } catch (e) {
    await Firebase.initializeApp(
        name: "jyotish_rahsaya_customer",
        options: DefaultFirebaseOptions.currentPlatform);
  }
  await FirebaseRemoteConfigService().initialize(); // Initialize Remote Config
  _initializeFirebaseMessaging();
  Platform.isIOS
      ? FirebaseMessaging.onBackgroundMessage(
          NotificationHandlerIOS.firebaseMessagingBackgroundHandler)
      : FirebaseMessaging.onBackgroundMessage(
          NotificationHandlerAndroid.firebaseMessagingBackgroundHandler);

  FirebaseAnalytics.instance.setAnalyticsCollectionEnabled(true);
  if (callingPrefs != null) {
    Map<String, dynamic> prefsBody = jsonDecode(callingPrefs);
    myApp = MyApp(prefsBody: prefsBody);
  } else {
    myApp = const MyApp();
  }
  HttpOverrides.global = MyHttpOverrides();
  NotificationHandler().initializeNotification();
  runApp(EasyLocalization(
    supportedLocales: const [Locale('en'), Locale('hi')],
    path: 'asset/lang',
    fallbackLocale: const Locale('en'),
    startLocale: Locale(defaultLanguage),
    child: myApp,
  ));
}

final FirebaseMessaging _messaging = FirebaseMessaging.instance;
void _initializeFirebaseMessaging() async {
  await _messaging.requestPermission();

  String? apnsToken = await _messaging.getAPNSToken();
  print("APNs Token: $apnsToken");

  String? fcmToken = await _messaging.getToken();
  print("FCM Token: $fcmToken");
}

class MyApp extends StatefulWidget {
  final Map<String, dynamic>? prefsBody;

  const MyApp({Key? key, this.prefsBody}) : super(key: key);
  // static final facebookAppEvents = FacebookAppEvents();
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  @override
  void initState() {
    super.initState();

    if (Platform.isIOS) {
      NotificationHandlerIOS.initializeNotifications(context);
    } else {
      NotificationHandlerAndroid.initializeNotifications(context);
    }
    // MyApp.facebookAppEvents.setAdvertiserTracking(enabled: true);
    THelperFunctions.setUserId();
    THelperFunctions.logEvent(eventName: "app_launch_event");
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: AppColor.appColor,
        statusBarBrightness: Brightness.dark,
      ),
    );

    // return MaterialApp.router(
    //   debugShowCheckedModeBanner: false,
    //   builder: (context, child) => ResponsiveBreakpoints.builder(
    //     child: child!,
    //     breakpoints: [
    //       const Breakpoint(start: 0, end: 480, name: MOBILE),
    //       const Breakpoint(start: 481, end: 800, name: TABLET),
    //       const Breakpoint(start: 801, end: 1920, name: DESKTOP),
    //       const Breakpoint(start: 1921, end: double.infinity, name: '4K'),
    //     ],
    //   ),
    //   theme: ThemeData(
    //     appBarTheme: AppBarTheme(
    //       color: AppColor.appColor,
    //       systemOverlayStyle: const SystemUiOverlayStyle(
    //         statusBarColor: AppColor.appColor,
    //         statusBarIconBrightness: Brightness.dark,
    //         statusBarBrightness: Brightness.light,
    //       ),
    //     ),
    //     primaryColor: AppColor.appColor,
    //     textSelectionTheme: TextSelectionThemeData(
    //       cursorColor: AppColor.appColor,
    //       selectionHandleColor: AppColor.appColor,
    //     ),
    //   ),
    //   routerConfig: AppRouter().buildRouter(),
    //   localizationsDelegates: context.localizationDelegates,
    //   supportedLocales: context.supportedLocales,
    //   locale: context.locale,
    // );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) => ResponsiveBreakpoints.builder(
        child: child!,
        breakpoints: [
          const Breakpoint(start: 0, end: 480, name: MOBILE),
          const Breakpoint(start: 481, end: 800, name: TABLET),
          const Breakpoint(start: 801, end: 1920, name: DESKTOP),
          const Breakpoint(start: 1921, end: double.infinity, name: '4K'),
        ],
      ),
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: AppColor.appColor,
          systemOverlayStyle: const SystemUiOverlayStyle(
            statusBarColor: AppColor.appColor,
            statusBarIconBrightness: Brightness.dark,
            statusBarBrightness: Brightness.light,
          ),
        ),
        primaryColor: AppColor.appColor,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: AppColor.appColor,
          selectionHandleColor: AppColor.appColor,
        ),
      ),
      // home: FreeChatAvailableScreen(),
      home: SupportChatScreen(),
      // home: GemstoneScreen(),
      // home: MultiProvider(
      //   providers: [
      //     ListenableProvider(create: (context) => KundliProviderNew()),
      //     ListenableProvider(create: (context) => ListOfApisProvider()),
      //   ],
      //   child:
      //       AstrologerProfileNew(astrologerId: "716", isAstrologerOnline: true),
      // ),
      // home: ListenableProvider(
      //   create: (context) => ListOfApisProvider(),
      //   child: OrderHistoryScreen(selected_index: 0),
      // ),
      // home: MultiProvider(
      //   providers: [
      //     ListenableProvider(create: (context) => ChattingProvider()),
      //     ListenableProvider(create: (context) => ListOfApisProvider()),
      //   ],
      //   child: FlutterChatScreen(
      //       orderID: "00000000384",
      //       cummID: "1527",
      //       chat_price: 5,
      //       isToView: true,
      //       fromId: "2774",
      //       toId: "1521",
      //       timerDuration: Duration(minutes: int.parse("5")),
      //       astrologerName: "Abhinav",
      //       astrologerImage: "",
      //       isFreeChat: false,
      //       dob: "1992-09-01",
      //       tob: "8:21 AM",
      //       messageInfo: {},
      //       infoId: "5",
      //       lat: "",
      //       lon: ""),
      // ),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
