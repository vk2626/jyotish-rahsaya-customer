// import 'dart:async';
// import 'package:record/record.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:flutter/material.dart';

// class RecordManager {
//   final AudioRecorder _record = AudioRecorder();
//   String? _recordedFilePath;

//   /// Returns the recorded file path if available.
//   String? get recordedFilePath => _recordedFilePath;

//   /// Check and request microphone permission.
//   Future<bool> checkPermission() async {
//     return await _record.hasPermission();
//   }

//   /// Start recording audio.
//   Future<void> startRecording() async {
//     if (await checkPermission()) {
//       final directory = await getApplicationDocumentsDirectory();
//       _recordedFilePath = '${directory.path}/recordedAudio.m4a';

//       try {
//         await _record.start(
//           const RecordConfig(),
//           path: _recordedFilePath!,
//         );
//       } catch (e) {
//         print('Error starting recording: $e');
//       }
//     } else {
//       print('Permission denied');
//     }
//   }

//   /// Stop recording audio and return the file path.
//   Future<String?> stopRecording() async {
//     try {
//       final path = await _record.stop();
//       _recordedFilePath = path;
//       return path;
//     } catch (e) {
//       print('Error stopping recording: $e');
//       return null;
//     }
//   }

//   /// Cancel the recording.
//   Future<void> cancelRecording() async {
//     try {
//       await _record.cancel();
//       _recordedFilePath = null; // Reset the path
//     } catch (e) {
//       print('Error canceling recording: $e');
//     }
//   }

//   /// Dispose of the record resources.
//   void dispose() {
//     _record.dispose();
//   }
// }
