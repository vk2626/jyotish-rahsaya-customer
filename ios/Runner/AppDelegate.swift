import UIKit
import flutter_foreground_task
import Flutter
import GoogleMaps  // Ensure this line is there
import UserNotifications
import CallKit
import AVFAudio
import PushKit
import flutter_callkit_incoming
import FBSDKCoreKit
import AppTrackingTransparency
import AdSupport


@main
@objc class AppDelegate: FlutterAppDelegate, PKPushRegistryDelegate, CallkitIncomingAppDelegate  {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyB4Bec1p6cCz6VvI3oRvWAyh0VBI9FOmw4")  // Replace with your key
    
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }
    
    GeneratedPluginRegistrant.register(with: self)
    SwiftFlutterForegroundTaskPlugin.setPluginRegistrantCallback { registry in
      GeneratedPluginRegistrant.register(with: registry)
    }
    //Setup VOIP
        let mainQueue = DispatchQueue.main
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    // let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
    // let restartChannel = FlutterMethodChannel(name: "com.jyotishrahsayacustomer.jyotishRahsaya/restartApp",
    //                                           binaryMessenger: controller.binaryMessenger)
        // Settings.isAutoLogAppEventsEnabled =  true
        // Settings.setAdvertiserTrackingEnabled(true)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }

  
  // Handle updated push credentials
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        print(credentials.token)
        let deviceToken = credentials.token.map { String(format: "%02x", $0) }.joined()
        print(deviceToken)
        //Save deviceToken to your server
        SwiftFlutterCallkitIncomingPlugin.sharedInstance?.setDevicePushTokenVoIP(deviceToken)
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("didInvalidatePushTokenFor")
        SwiftFlutterCallkitIncomingPlugin.sharedInstance?.setDevicePushTokenVoIP("")
    }

    // Handle incoming pushes
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
    print("didReceiveIncomingPushWith")
    
    // Ensure the push type is VoIP
    guard type == .voIP else {
        print("Push type is not VoIP.")
        return
    }
    
    // Log the entire payload as JSON
    if let jsonData = try? JSONSerialization.data(withJSONObject: payload.dictionaryPayload, options: .prettyPrinted),
       let jsonString = String(data: jsonData, encoding: .utf8) {
        print("Payload JSON: \(jsonString)")
    } else {
        print("Failed to serialize payload into JSON.")
    }
    
    // Extract required fields from the payload
    let communicationId = payload.dictionaryPayload["communicationId"] as? String ?? ""
    let nameCaller = payload.dictionaryPayload["astrologerName"] as? String ?? "Unknown Caller"
    let handle = payload.dictionaryPayload["user_id"] as? String ?? "Unknown Handle"
    let isVideo = payload.dictionaryPayload["isVideo"] as? Bool ?? false
    
    // Validate the UUID
    guard !communicationId.isEmpty else {
        print("Invalid or missing communicationId in payload.")
        return
    }
    
    // Create the CallkitIncoming data object
    let data = flutter_callkit_incoming.Data(id: communicationId, nameCaller: nameCaller, handle: handle, type: isVideo ? 1 : 0)
    
    // Set additional data fields
    data.extra = ["user": "abc@123", "platform": "ios"]
    
    // Display the incoming call
    SwiftFlutterCallkitIncomingPlugin.sharedInstance?.showCallkitIncoming(data, fromPushKit: true)
    
    // Ensure the completion handler is called
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        completion()
    }
}

    
    func onAccept(_ call: Call, _ action: CXAnswerCallAction) {
        print("LOG: onAccept - \(call.data)")
    }

    func onDecline(_ call: Call, _ action: CXEndCallAction) {
        print("LOG: onDecline - \(call.data)")
    }

    func onEnd(_ call: Call, _ action: CXEndCallAction) {
        print("LOG: onEnd - \(call.data)")
    }

    func onTimeOut(_ call: Call) {
        print("LOG: onTimeOut - \(call.data)")
    }

    func didActivateAudioSession(_ audioSession: AVAudioSession) {
        print("Audio session activated")
    }

    func didDeactivateAudioSession(_ audioSession: AVAudioSession) {
        print("Audio session deactivated")
    }
}
